<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
?>
 <script type="text/javascript">
	 /*删选*/
	 $(function(){
		 $("#index-help-sell").validate({ 
		 });
		 
		  $(".index-sell-brand-list li").click(function(){
  
		  $(".carsell-brand-con1-right").scrollTop(100);
		  });
		  
		  
		  $(".carsell-sell-brand").click(function(event){
		  //取消事件冒泡  
		  event.stopPropagation();  
		  $(".carsell-sell-brand-con1").toggle();
		   return false;
		  })
		  
		  //点击空白处隐藏弹
	   $(document).click(function(event){
			var _con = $(".carsell-sell-brand-con1");   // 设置目标区域
			if(!_con.is(event.target) && _con.has(event.target).length == 0){ // Mark 1
			  //$('#buycarmore-b').slideUp('slow');   //滑动消失
			  $(".carsell-sell-brand-con1").hide();          //淡出消失
			}
	  });
							
	  //汽车品牌显示
	  show_brand(".carsell-brand-con1-right");
		 
 	 })
  
		function get_brand(brand_name) {
			$(".carsell-sell-brand-content").val($(brand_name).html());
			$(".carsell-sell-brand-con1").hide();
			}
		function show_brand(container) {
			var content ='';
			for(var s in brand_a) {
					content+= '<span class="carsell-brand-flag">'+s+'</span>';
					$(brand_a[s]).each(function(i,dom){
						var aArray = dom.split(','),
						q1 = aArray[0],
						q2 = aArray[1];
						content+='<p class="brand-item"><a href="javascript:void(0)" onClick="get_brand(this)">'+q1+'</a></p>';
						});
					
			}
			
			$(container).html(content);
		}

</script>
<script src="js/car_brand_data.js"></script>
 <!-- banner-->
	  <div class="sellcar-banner">
      	<div class="carsell-message-pos">
      		<div class="carsell-message-list">
            	<div class="carsell-message-a">出售爱车</div>
                <form id="index-help-sell" class="carsellform-css">
            		
                    <!--车型选择-->
                    <div>
                    <div class="carsell-sell-brand">
                        <input id="brand_name" type="text" class="form-control sell-carico carsell-sell-brand-content valid" readonly="readonly" placeholder="车辆品牌" aria-invalid="false"><i class="fa fa-angle-down "></i>

                    </div>
                    
                      <!--隐藏的车辆品牌一级页面-->
                    <div class="carsell-sell-brand-con1">
                        <!--<div class="index-brand-con1-left">
                            <ul class="index-sell-brand-list">
                                <li>A</li>
                                <li>B</li>
                                <li>C</li>
                                <li>D</li>
                                <li>E</li>
                                <li>F</li>
                                <li>G</li>
                                <li>H</li>
                                <li>I</li>
                            </ul>
                        </div>-->
                        <!--右半部分-->
                         <div class="carsell-brand-con1-right"></div>
                    </div>
                    </div>
                    <div class="carsellform-input">
                    	<input id="appointment_mobile" type="mobile" required="required"  class="form-control sell-telico" placeholder="手机号码"/>
                    </div>
                 	<button type="submit" class="carsell-sell">免费卖车</button>  
                </form>

                <div class="carsell-consult"><a href="<?php echo $SER_ADD;?>/appraise">估价计算器 <i class="fa fa-angle-double-right"></i></a></div>
            </div>
          </div>  
      </div>	
    
	  <ul class="sellcar-bg">
      	<li>
        	<span>第一步 免费预约代理</span>
            <p>填写电话号码在线预约</p>
        </li>
      	<li>
        	<span>第二步 客服联系您 </span>
            <p>车辆检测、棚拍、发布官网</p>
        </li>
        
        <li>
        	<span>第三步 专人陪同看车 </span>
            <p>填写电话号码在线预约</p>
        </li>
        
        <li>
        	<span>第四步 交易完成 </span>
            <p>填写电话号码在线预约</p>
        </li>
      </ul>		
    	
       <div class="sellcar-allbg"> 
          <div class="sellcar-a">
          	 <div class="sellcar-aa">
                <span>边开边卖  无需寄存</span>
                <p>不用寄存车辆，代售期间可以一直开着爱车，直到交易成功，高效不误事。</p>
          	 </div>
          </div>
          	
          <div class="sellcar-b">
             <div class="sellcar-bb">
                <span>直面个人买家 好车卖出好价</span>
                <p>拒绝黄牛车商，避免重重压价，每车多卖20%。全程免费，一对一服务。</p>
          	 </div>
          </div>
          <div class="sellcar-c">
         	 <div class="sellcar-cc">
                <span>海量买家 极速成交</span>
                <p>每天超过百万人次浏览，一站式服务，无需担忧流程复杂，安心轻松卖车。</p>
          	 </div>
          </div>
   	   </div>
    <!--底部-->
<?php
	require_once('footer.php');
?>