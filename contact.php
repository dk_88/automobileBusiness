<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
 <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=ZKhbc8UjM4f3eF3qcWEx8WFT"></script>
<div class="introduction-bgb"></div>

        <div class="introduction-item">
            <div class="introduction-left">
                <div class="introduction-left-top"><i class="fa fa-files-o"></i>关于我们</div>
                <?php
					require_once('about_navbar.php');
				?>
 
            </div>
            
            <div class="introduction-right">
                <div class="introduction-right-locatin">关于我们>加入我们</div>
                <div class="introduction-right-content">
                	<div id="map" class="baidu-map"></div>
                    	
                        <ul class="baidu-address">
                        	
                            <li><i class="mapa"></i>杭州市凤起东路358号五福天星龙大厦B座17楼</li>
                            <li><i class="mapb"></i>服务热线：400-0975-588</li>
                            <li><i class="mapc"></i>官网网址：<a href="http://www.zjdaopu.com">www.zjdaopu.com</a></li>
                            <li><i class="mapc"></i>车宇宙网址：<a href="http://www.yuzhouche.com">www.yuzhouche.com</a></li>
                            
                        </ul>
                    </div>
                </div>     
            </div>
            <script type="text/javascript">
				//创建和初始化地图函数：
				function initMap(){
				  createMap();//创建地图
				  setMapEvent();//设置地图事件
				  addMapControl();//向地图添加控件
				  addMapOverlay();//向地图添加覆盖物
				}
				function createMap(){ 
				  map = new BMap.Map("map"); 
				  map.centerAndZoom(new BMap.Point(120.215366,30.268794),18);
				}
				function setMapEvent(){
				  map.enableScrollWheelZoom();
				  map.enableKeyboard();
				  map.enableDragging();
				  map.enableDoubleClickZoom()
				}
				function addClickHandler(target,window){
				  target.addEventListener("click",function(){
					target.openInfoWindow(window);
				  });
				}
				function addMapOverlay(){
				  var markers = [
					{content:"杭州市凤起东路358号五福天星龙大厦B座17楼",title:"浙江稻普控股集团",imageOffset: {width:0,height:3},position:{lat:30.268607,lng:120.215178}}
				  ];
			
				  for(var index = 0; index < markers.length; index++ ){
					var point = new BMap.Point(markers[index].position.lng,markers[index].position.lat);
					var marker = new BMap.Marker(point,{icon:new BMap.Icon("http://api.map.baidu.com/lbsapi/createmap/images/icon.png",new BMap.Size(20,25),{
					  imageOffset: new BMap.Size(markers[index].imageOffset.width,markers[index].imageOffset.height)
					})});
					var label = new BMap.Label(markers[index].title,{offset: new BMap.Size(25,5)});
					var opts = {
					  width: 200,
					  title: markers[index].title,
					  enableMessage: false
					};
					var infoWindow = new BMap.InfoWindow(markers[index].content,opts);
					marker.setLabel(label);
					addClickHandler(marker,infoWindow);
					map.addOverlay(marker);
				  };
				}
				//向地图添加控件
				function addMapControl(){
				  var scaleControl = new BMap.ScaleControl({anchor:BMAP_ANCHOR_BOTTOM_LEFT});
				  scaleControl.setUnit(BMAP_UNIT_IMPERIAL);
				  map.addControl(scaleControl);
				  var navControl = new BMap.NavigationControl({anchor:BMAP_ANCHOR_TOP_LEFT,type:BMAP_NAVIGATION_CONTROL_LARGE});
				  map.addControl(navControl);
				  var overviewControl = new BMap.OverviewMapControl({anchor:BMAP_ANCHOR_BOTTOM_RIGHT,isOpen:true});
				  map.addControl(overviewControl);
				}
				var map;
				  initMap();
			  </script>

        </div>
<?php
	require_once('footer.php'); 
?> 
		
 