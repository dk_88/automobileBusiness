<!DOCTYPE html>
<html lang="zh-CN"><head>
	<meta charset="utf-8">
   	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/unslider.js"></script>
    <script src="js/animateBackground-plugin.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/messages_zh.js"></script>
    <script src="js/placeholder.js"></script>
    <script type="text/javascript" src="js/fly.js"></script>
    <script type="text/javascript" src="js/requestAnimationFrame.js"></script> 
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <link rel="stylesheet" href="fontawesome/css/font-awesome.min.css">
    <title>车宇宙</title>
   
</head>

<body>