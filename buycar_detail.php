<?php
	if(!isset($_GET['id'])){
		header("Location: 404");
	}
?>
<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
?>
<script type="text/javascript" src="js/collect.js"></script>
<!--若在用户登录情况下，需记录最近浏览的二手车-->
<?php
	//获取车id
	$car_id = $_GET['id'];
	$user_collect = '';
	if(isset($_SESSION['user_id'])) {
		$conn = db_connect();
		$conn->query("set names utf8");
		$user_id = $_SESSION['user_id'];
		$result = $conn->query("select collect_cars,view_cars from ".$users_info." where user_id='".$user_id."'");
		if(!$result) {
			exit();
		}
		$row = $result->fetch_assoc();
		$user_collect = $row['collect_cars'];
		if($row['view_cars']=="") {
			$arr =array();
		}
		else
			$arr = explode(",",$row['view_cars']);
		//先查找该数组中是否已经有该车的id，若有则无需进行后续
		if(!in_array($car_id, $arr)) {
			if(count($arr)>=8) {
			array_shift($arr);
		}
		array_push($arr,$car_id);
		$str=implode(",",$arr);
		$result = $conn->query("update ".$users_info." set view_cars='".$str."' where user_id='".$user_id."'");
		
		}
		//若$arr长度大于6,则需要删除第一个数组并push新的进去
		
		
	}
	
?>
<div class="model-dialog" id="model_dialog_order" style="display:none;">
	<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>
	<div class="make_order_model">您正在预约看车~请留下联系方式，购车顾问会第一时间与您联系，并沟通看车事宜!</div>
    <form id="make_order_Form"><div class="passwordcenter-list has-feedback make_order_phone"><span class="glyphicon glyphicon-phone form-control-feedback"></span><input type="mobile" class="phone-control" required id="make_order_mobile" placeholder="请输入手机号" value="<?php echo isset($_SESSION['user_id'])?$_SESSION['user_id']:'';?>"></div><button class="btn make_order_submit" type="submit">确认预约</button></form>
</div>
<?php
	//获取车辆的信息
	$conn = db_connect();
	$conn->query("set names utf8");
	$result1 = $conn->query("select * from ".$car_dataset." where id='".$car_id."'");
	$row1 = $result1->fetch_assoc();
	$img_src=explode(";",$row1['img_src']);
	$img_src[0]=$img_src[0]==""?"upload/lost.jpg":$img_src[0];
	$create_time = explode("-",$row1['Ctime']);
?>

<script>
	function make_order(obj) {
		var bh = $("body").height(); 
		var bw = $("body").width(); 
		$(".fullbg").css({ 
		height:bh, 
		width:bw, 
		display:"block" 
		}); 
		$("#model_dialog_order").show(); 
		
	}
	//加入购物车
	function add_to_cart(obj,event){
		$.ajax({
			type:"POST",
			url:"configure/add_to_cart.php",
			cache:false,
			data:{"car_id":$(obj).val(),
			  },
			success:function(data) {
				var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				//成功
				if(data==1) {
					//购物车动画
					$(".shopping_number").html(parseInt($(".shopping_number").html())+1);
					var offset = $('#end').offset(), flyer = $('<img src="images/cart_fly.png" width="40" height="40">');
					flyer.fly({
						start: {
							left: event.pageX,
							top: event.pageY
						},
						end: {
							left: offset.left+10,
							top: offset.top,
							width:0, //结束时宽度 
							height:0, //结束时高度
						}
					});
					//改变购物车中的东西
					//增加购物车的东西,判断是否是空
					if($(".cart_num").html()=="0") {
						$(".ibar_car_list").html('<li><div class="cart_item_pic"><a href="buycar_detail?id=<?php echo $row1['id'];?>"><img src="../cheyuzhou_bg/<?php echo $img_src[0];?>" /></a></div><div class="cart_item_desc"><a href="buycar_detail?id=<?php echo $row1['id'];?>" class="cart_item_name"><?php echo $row1['brand_name'];?></a><span class="cart_price">￥<?php echo $row1['current_price'];?>万元</span></div><div class="cart_item_delete"><a href="javascript:void(0)" onclick="sidebar_goods_delete(\'<?php echo $row1['id'];?>\')">删除</a></div></li>');
					}
					else {
						$(".ibar_car_list").append('<li><div class="cart_item_pic"><a href="buycar_detail?id=<?php echo $row1['id'];?>"><img src="../cheyuzhou_bg/<?php echo $img_src[0];?>" /></a></div><div class="cart_item_desc"><a href="buycar_detail?id=<?php echo $row1['id'];?>" class="cart_item_name"><?php echo $row1['brand_name'];?></a><span class="cart_price">￥<?php echo $row1['current_price'];?>万元</span></div><div class="cart_item_delete"><a href="javascript:void(0)" onclick="sidebar_goods_delete(\'<?php echo $row1['id'];?>\')">删除</a></div></li>');
					}
					//改变数字
					$(".cart_num").html(parseInt($(".cart_num").html())+1);
					$(".cart_number").html(parseInt($(".cart_number").html())+1);
					$(".cart_handler_detail").html(parseFloat($(".cart_handler_detail").html())+parseFloat(<?php echo $row1['current_price'];?>));
				}
				//未登录
				else if(data==2) {
					show_model(0);
				}
				//
				//加入购物车失败
				else if(data==0) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">加入购物车失败！刷新重试~</p>';
				    show_clue(content);
					
				}
				//该车子已经在购物车
				else if(data==3) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">该车已经在购物车里啦~</p>';
				    show_clue(content);
				}
			},
			});
	}
	//点击确认预约按钮
	$(function(){
		$("#make_order_Form").validate({
			submitHandler: function (form) {
				$.ajax({
					type:"POST",
			  	  	url:"configure/make_order_check.php",
			      	cache:false,
					data:{
						car_id:$(".cardetails-buttonab").val(),
						user_id:$("#make_order_mobile").val(),
						user_type:"1",
					},
					beforeSend:function(){
					 	
				    	$(".make_order_submit").html("预约中...").attr('disabled',"true");
						  
					  },
					success:function(data){
						
						var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
						$(".fullbg,#model_dialog_order").hide(); 
						//预约成功
						if(data==1) {
							content+= '<p class="helpbuy-demand-success">预约成功，请保持手机畅通~</p>';
							show_clue(content);
						}
						
						//预约失败
						else if(data==0) {
							content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">预约失败，刷新页面重试~</p>';
							show_clue(content);
						}
						//该车已卖掉/下架
						else if(data==2) {
							content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">太不巧啦~该二手车已卖出！</p>';
							show_clue(content);
							
						}
						else if(data==3) {
							content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">请勿重复预约该车！可至个人中心查看</p>';
							show_clue(content);
						}
						$(".make_order_submit").html("确认预约").removeAttr('disabled');
					}
					});
			}
			});
		
		})
		
</script>
<div class="cardetails-carinfor">
        <div class="cardetails-carinfor-left">
          <img src="../cheyuzhou_bg/<?php echo $img_src[0];?>" />
        </div>
        
        <div class="cardetails-carinfor-right">
            <div class="cardetails-carname">
                <?php echo $row1['brand_name'];?>  
           </div>
            <div class="cardetails-carinfor-price">
                <div class="cardetails-carinfor-price-left">
                    <span>￥<?php echo $row1['current_price'];?>万</span>
                    <p>(含过户费)</p>
                </div>
                
                <div class="cardetails-carinfor-price-right">
                    <span>¥<?php echo $row1['new_price'];?>万</span>
                    <p>同型新车价格(含税)</p>
                </div>
            </div>
            
            <ul class="cardetails-carinfor-list">
                    <li>
                        <span>上架时间</span>
                        <p><?php echo $create_time[0]."-".$create_time[1];?></p>
                    </li>

                    <li>
                        <span>上牌时间</span>
                        <p><?php echo $row1['plate_date'];?></p>
                    </li>
                    
                    <li>
                        <span>表显里程</span>
                        <p><?php echo $row1['driving_distance'];?>万公里</p>
                    </li>
                    
                    <li>
                        <span>排放标准</span>
                        <p><?php echo $standard_change[$row1['e_standard']];?></p>
                    </li>
                    
                    <li style="border: none;">
                        <span>销售城市</span>
                        <p><?php echo $city_change[$row1['sell_city']];?></p>
                    </li>
            </ul>
            
            <div class="cardetails-button">
                <div class="cardetails-buttona"><button type="button" class="cardetails-buttonab" onclick="make_order(this)" value="<?php echo $car_id;?>">预约看车</button></div>
                <div class="cardetails-buttonb"><button type="submit" class="cardetails-buttonbb cardetails-button-c" onclick="add_to_cart(this,event)" value="<?php echo $car_id;?>">加入购物车</button></div>
                <div id="flyItem" class="fly_item"><img src="images/item-pic.jpg" width="40" height="40"></div>
                <div class="cardetails-buttonc">
                    <span>预约电话</span>
                    <p>400-097-5588</p>
                </div>
            </div>


            <div class="cardetails-icons">
                <ul>
                    <li>
                        <img src="images/house.png" />
                        <a href="<?php echo $SER_ADD?>/businessshop?id=<?php echo $row1['owner_phone'];?>&sellcity=<?php echo $row1['sell_city'];?>">进入店铺</a>
                    </li>
                    <li>
                         <?php
						 	$collect_content = '<img class="collect-img" src="images/star.png" /><button class="cardetails-col" onclick="car_collect(this)" value="'.$car_id.'">收藏车辆</button>';
							if($user_collect!='') {
									$collect_arr = explode(",",$user_collect);
									$key = in_array($row1['id'],$collect_arr);
								    if($key) {
									    $collect_content = '<img class="collect-img" src="images/star_full.png" /><button class="cardetails-col" onclick="car_collect(this)" value="'.$car_id.'">已收藏</button>';
								    }
									else 
										$collect_content = '<img class="collect-img" src="images/star.png" /><button class="cardetails-col" onclick="car_collect(this)" value="'.$car_id.'">收藏车辆</button>';
							}
							echo $collect_content;
						 ?>
                         

                    </li>
                   <li>
                        <img src="images/share.png" />
                        <a href="javascript:void(0)" onmouseover="$('#ckepop').show();" onmouseout="$('#ckepop').hide()" class="shareto_button">分享</a>

                    </li>

                </ul>
                <div class="share-car"  id="ckepop" onmouseover="$('#ckepop').show();" onmouseout="$('#ckepop').hide()">
						<b class="d-icon"></b>
						
						<ul>
							<li><a href="http://service.weibo.com/share/share.php?title=帮我出谋划策一下吧~~<?php echo $row1['brand_name']?>二手车 - 车宇宙二手车&amp;url=http://www.yuzhouche.com/buycar_detail?id=<?php echo $row1['id']?>&amp;source=bookmark&amp;appkey=&amp;pic=http://www.yuzhouche.com/cheyuzhou_bg/<?php echo $img_src[0];?>&amp;ralateUid=" target="_blank" class="d-icon xl-ico ">新浪微博</a></li>
                            <!--这里分享到QQ空间中时，无法准确读取id的值-->
							<li><a href="http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?title=帮我出谋划策一下吧~~<?php echo $row1['brand_name']?>二手车 - 车宇宙二手车&amp;url=http://www.yuzhouche.com&amp;pics=http://www.yuzhouche.com/cheyuzhou_bg/<?php echo $img_src[0];?>&amp;summary=" class="d-icon qq-ico " target="_blank">QQ空间</a></li>
							<li><a class="d-icon wx-ico " onclick="$('#title_word').html('分享到微信');$('#des_word').html('打开微信，点击底部的发现，使用扫一扫 即可将网页分享到我的微信。');$('#weixin_share').show()">微信好友</a></li>
							<li><a href="javascript:void(0)" class="d-icon pyq-ico " onclick="$('#title_word').html('分享到朋友圈');$('#des_word').html('打开微信，点击底部的发现，使用 扫一扫 即可将网页分享到我的朋友圈。');$('#weixin_share').show()">朋友圈</a></li>
						</ul>
					</div>
            </div>

        </div>  
    </div>
     <div class="cardetails-check-item">
        <div class="cardetails-check-title">
            <span>车宇宙质量检测合格证</span>
            <p>检测对象：<?php echo $row1['brand_name'];?>   检测项目：154项国家标准检测及95项车宇宙专项检测</p>
        </div>
        
        <div class="cardetails-check-content">
            <p>
            <strong>检验结果 :</strong> 经检测，本车全车均为原厂原漆，后保险杠可见伤，加装导航、倒车影像，空调及车内其他电器设备均运行正常。
车身骨架完好无损伤，变速箱、发动机工况良好无渗油，怠速正常，无抖动现象，换挡平顺。个人一手车。
            </p>
        </div>
        
        <div class="cardetails-table">
            <table>
                <tr>
                    <td><strong>事故检测 :</strong></td>
                    <td></td>
                </tr>
                <tr>
                    <td>排除重大撞击</td>
                    <td>28项</td>        
                     <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>排除火烧车</td>
                    <td>4项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>排除水泡车</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
            </table>
            
            <table>
                <tr>
                    <td><strong>外观内饰检测 :</strong></td>
                    <td></td>
                </tr>
                <tr>
                    <td>外观检测</td>
                    <td>28项</td>   
                    <td> <img src="images/ok.png" /></td>     
                </tr>         
          
                <tr>
                    <td>内饰检测</td>
                    <td>37项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
            </table>
            
            <table>
                <tr>
                    <td><strong>安全检测 :</strong></td>
                    <td></td>
                </tr>
                <tr>
                    <td>刹车系统</td>
                    <td>28项</td>  
                    <td> <img src="images/ok.png" /></td>      
                </tr>
                <tr>
                    <td>轮胎</td>
                    <td>4项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>被动安全系统</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>排查起火隐患</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>指示灯检测</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
            </table>

            <table>
                <tr>
                    <td><strong>驾驶检测 :</strong></td>
                    <td></td>
                </tr>
                <tr>
                    <td>启动检测</td>
                    <td>28项</td>  
                    <td> <img src="images/ok.png" /></td>      
                </tr>
                <tr>
                    <td>怠速检测</td>               
                    <td>4项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>加速检测</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>匀速检测</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>减速检测</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
                <tr>
                    <td>制动检测</td>
                    <td>8项</td>
                    <td> <img src="images/ok.png" /></td>
                </tr>
            </table>
        </div>

        <div class="cardetails-in">以上为当前车况，交易前会做二次深度检测。</div>
    </div>
    
    <div class="cardetails-more-infor">
        <div class="cardetails-more-bg">
            车辆信息
        </div>
        
        <table class="cardetails-more-tablea">
            <tr>
                <td><span>手续状况</span></td>
                <td></td>
            </tr>
            <tr>
                <td>过户次数</td>
                <td><?php echo $transfer_num_change[$row1['transfer_num']];?></td>
            </tr>
            <tr>
                <td>使用性质</td>
                <td><?php echo $property_change[$row1['property']];?></td>
            </tr>
            <tr>
                <td>过户手续</td>
                <td><?php echo $car_procedure_change[$row1['car_procedure']];?></td>
            </tr>
            <tr>
                <td>保险到期时间</td>
                <td><?php echo $row1['insurance']==''?"未知":$row1['insurance'];?></td>
            </tr>
            <tr>
                <td>年检有效期</td>
                <td><?php echo $row1['survey']==''?"未知":$row1['survey'];?></td>
            </tr>
            <tr style="border: none;">
                <td>保养情况</td>
                <td><?php echo $maintenance_change[$row1['maintenance']];?></td>
            </tr>
        </table>
        
        <table class="cardetails-more-tablea">
            <tr>
                <td><span>参数配置</span></td>
                <td></td>
            </tr>
            <tr>
                <td>变速箱</td>
                <td><?php echo $gearbox_change[$row1['gearbox']];?></td>
            </tr>
            <tr>
                <td>车辆颜色</td>
                <td><?php echo $carcolor_change[$row1['carcolor']];?></td>
            </tr>
            <tr>
                <td>环保标准</td>
                <td><?php echo $e_standard_change[$row1['e_standard']];?></td>
            </tr>
            <tr>
                <td>工信部综合油耗（L/100km）</td>
                <td><?php echo $row1['oil_wear']=='0'?"未知":$row1['oil_wear'];?></td>
            </tr>
            
            <tr style="border: none;">
                <td>生产厂商</td>
                <td><?php echo $row1['manufacturer']==''?"未知":$row1['manufacturer'];?></td>
            </tr>
        </table>
    </div>
    
    <div class="cardetails-more-bgb">车辆外观</div>
    <div class="cardetails-listtitl-small">
    	<?php
			
			$content = '';
			for($i=1;$i<sizeof($img_src)-1;$i++) {
				$content='<div class="cardetails-listtitl-small-left"><img src="../cheyuzhou_bg/'.$img_src[$i].'" /></div>';
				echo $content;
			}
		?>
        <div class="clear"></div>  
    </div>
    

    
    
    
    
    
    
    
    <div class="cardetails-more-c">
        <div class="cardetails-more-bgc">车况检测</div>
        <div class="cardetails-list-a">
            <p>事故排查</p>
            <span>车体骨架结构无变形、无扭曲、无更换、无烧焊、无褶皱；无火烧痕迹，无水泡痕迹。</span>
        </div>
        
        <div class="cardetails-list-b">
            <ul>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>舱内保险丝盒无火烧熏黑痕迹</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>车辆覆盖件无火烧痕迹</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>发动机线束无火烧痕迹</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>防火墙有无火烧或熏黑痕迹</span>
                </li>
            </ul>
            
            <ul>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>发动机缸盖无发霉点</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>保险盒无泥沙</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>散热片及擎旁零件无有异味、泥沙</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>水箱及水箱前板无异味、泥沙</span>
                </li>
            </ul>
            
            
            <ul>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>安全带根部无异味、泥沙</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>座椅弹簧和内套绒布无异味、泥沙</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>座椅底部金属件无锈蚀</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>仪表台座内电线和接头无异味、泥沙</span>
                </li>
            </ul>
        </div>

        <div class="cardetails-list-a">
            <p>安全检测</p>
            <span>经检测，刹车系统、被动系统、指示灯系统正常。</span>
        </div>
        
        <div class="cardetails-list-c">
            <ul>
                <li><strong>指示灯检测</strong></li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>远近光灯</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>前雾灯</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>前转向灯</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>前示廓灯</span>
                </li>
                
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>刹车灯</span>
                </li>
                
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>后雾灯</span>
                </li>
                
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>后转向灯</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>后示廓灯</span>
                </li>
            </ul>
            
            <ul>
                <li><strong>起火隐患排查</strong></li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>油管水管无老化、裂痕</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>线束无老化、破损</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>进油管无损坏、渗油</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>回油管无坏损、渗油</span>
                </li>
                <br />
                <li>
                    <strong>被动安全</strong>
                </li>

                
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>安全带结构完整、功能正常</span>
                </li>
                
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>安全气囊正常、无故障报警</span>
                </li>
            
            </ul>
            
            
            <ul>
                <li><strong>轮胎检测</strong></li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>左前轮无修补断线鼓包割裂</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>左后轮无修补断线鼓包割裂</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>右前轮无修补断线鼓包割裂</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>右后轮无修补断线鼓包割裂</span>
                </li>
                <br />
                <li><strong>刹车系统</strong></li>
                
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>制动防抱死系统（ABS）正常</span>
                </li>
                
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>驻车制动系统结构完整</span>
                </li>
            </ul>
        </div>
        
        <div class="clear"></div>
        
        <div class="cardetails-list-a">
            <p>驾驶检测</p>
            <span>经试驾员专业测试，发动机、变速箱正常，无怠速抖动，变速时无闯档顿挫，转向无乏力感。</span>
        </div>
        
        <div class="cardetails-list-b">
            <ul>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>发动机怠速平稳无异响</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>发动机运转顺畅无异响</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>泡水车辆检测项检测</span>
                </li>
            </ul>
            
            <ul>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>车辆转向方向回位灵敏</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>制动性能正常</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>过火车辆检测项检测</span>
                </li>
            </ul>
            
            
            <ul>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>加速性能与车型排量相符</span>
                </li>
                <li>
                    <div class="cardetails-yes"><img src="images/cardetails-yes.png"/></div>
                    <span>车辆制动时不跑偏</span>
                </li>
            </ul>
        </div>
    </div>

<?php
	require_once('footer.php'); 
?>