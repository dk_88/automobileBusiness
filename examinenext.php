<?php
	if (!session_id()) session_start();
	//判断城市
	$city = isset($_GET['city'])?$_GET['city']:"hangzhou";
	
	//默认情况的话将从ip地址获取到城市,前期的话用杭州
	
?>
<?php
	require_once('configure/parameter.php');
	require_once('header.php');
	
?>
 <div class="header-three-bg">
    	<div class="header-logo">
        	<a href="<?php echo $SER_ADD;?>/index"><img src="images/index_logo.png" /></a>
        </div>
    </div>
     <div class="navbar-container">
           <div class="nav-bar">
                <ul>
                    
                    <?php
						
						$links = array();
						$links[]=array("首页","index.php","index");//或者这边直接改成根据ip地址获取
						$links[]=array("买车","buy_car.php","buy?city=".$city);
						$links[]=array("卖车","sell_car.php","sell_car");
						$links[]=array("帮买","helpbuy.php","helpbuy");
						$links[]=array("服务保障","service.php","service");
						$self_page = basename($_SERVER['PHP_SELF']);
						foreach($links as $link){
							printf('<a href="%s"><li %s>%s</li></a>' , $link[2],$self_page==$link[1]?'class="navbar-current"':'class=""',$link[0]);
								
							}
					?>
                                 
                </ul>
            </div>
      </div>
    <div class="examine-bg">
    	 <div class="examine-processbg">
            <ul class="examine-processb">
                <li>填写公司账号信息</li>
                <li class="examine-native">待审核状态</li>
                <li>审核结果</li>
            </ul>
   		</div>
        
        <div class="examinestate">
   			<img src="images/pass.png" />
            <div class="examinestateok">
            	<span>恭喜您申请成功！</span>
                <p>
                您的认证正在审核，请耐心等待。您可根据手机号实时查询申请进度。
如有疑问，请致电：400-0975-588
				</p>
            </div>
        </div>
     </div> 
   <!--尾部-->
   <?php
       require_once('footer.php');
   ?>