//JavaScriptDocument
//不同字母对应不同汽车牌子
var brand_a={"A":["安驰,anchi","奥迪,audi","阿斯顿马丁,astonmartin","AC Schnitzer,acs","阿尔法罗密欧,alfa"],
			 "B":["巴博斯,barbus","宝龙,baolong","保时捷,porsche","别克,bieke","宝马,bmw", "奔驰,benz","北汽制造,beiqi","本田,bentian","北京汽车,bjqc","奔腾,benteng","比亚迪,byd","宝骏,baojun","宾利,binli","布加迪,bjd","标致,biaozhi","北汽幻速,bqhs"],
			 "C":["长丰,changfeng","长安,changan","长城,changcheng","昌河,changhe"],
			 "D":["道奇,daoqi","大宇,dayu","大迪,dadi","东南,dongnan","大众,dazhong","大通,datong","帝豪,dihao","东风,dongfeng"],
			 "F":["菲斯克,fsike","福田,futian","菲亚特,fyate","福迪,fudi","丰田,fengtian", "福特,ford","法拉利,ferray"],
			 "G":["光冈,mitsuoka","GMC,gmc","广汽,guangqi","观致,guanzhi","GUMPERT,gumpert"],
			 "H":["悍马,hanma","华泰,huatai","黑豹,heibao","黄海,huanghai","哈飞,hafei","红旗,flag","华翔,huaxiang","华北,huabei","海马,haima","汇众,huizhong","华普,huapu","华阳,huayang","恒天,hengtian","航天,hangtian","海格,haige","华颂,huasong"],
			 "J":["金龙,jinlong","金程,jincheng","吉利,geely","金杯,jinbei","江淮,jianghuai","吉奥,jiao","江铃,jiangling","吉普,jeep","江南,jiangnan","九龙,jiulong","捷豹,jiebao"],
			 "K":["开瑞,kairui","克莱斯勒,chrysler","柯尼赛格,koenigsegg","卡尔森,kers","凯迪拉克,cadillac","凯佰赫,kaibaihe","KTM,ktm","卡威,kawei","凯马汽车,kaima","凯翼,kaiyi","康迪,kangdi"],
			 "L":["雷诺,renault","莲花,lianhua","雷克萨斯,lexus","路特斯,lutesi","林肯,lincoln","铃木,suzuki","力帆,lifan","路虎,landrove","兰博基尼,lamborghini","劳斯莱斯,rollsroyce","陆风,lufeng","陆地方舟,ludiship","罗孚,luofu"],
			 "M":["摩根,mogen","迷你,mini","迈巴赫,maybach","迈凯伦,maikailun","名爵,mingjue","马自达,mazda","玛莎拉蒂,maserati","美亚,meiya"],
			 "N":["纳智捷,nazhijie","南汽,nanqi"],
			 "O":["讴歌,ouge","欧宝,opel"],
			 "P":["帕加尼,pajiani"],
			 "Q":["起亚,kia","奇瑞,qirui"],
			 "R":["日产,richan","荣威,rongwei","瑞麒,ruilin"],
			 "S":["陕汽通家,sqtjia","双环,shuanghuang","双龙,shuanglong","斯柯达,skoda","Smart,smart","世爵,shijue","三菱,mitsubishi","斯巴鲁,subaru","萨博,saab"],
			 "T":["天马,tianma","通宝,tongbao","天津一汽,tjyq","特斯拉,tesla"],
			 "W":["威麟,weilin","威兹曼,weiziman","沃尔沃,volvo"],
			 "X":["新凯,xinkai","西雅特,seat","现代,hyundai","雪铁龙,citroen","雪佛兰,chevrolet"],
			 "Y":["永源,yongyuan","一汽,yiqi","英菲尼迪,infiniti","英伦,englon","野马,yema","英致,yingzhi"],
			 "Z":["中誉,zhongyu","中顺,zhongshun","中兴,zhongxing","中华,zhonghua","众泰,zhongtai","中欧,zhongou"]};
//首页买车栏车型配置数组
var car_type={"SUV":["途观,dazhongh","奔驰M级,benzv","奥迪Q5,audiq5","汉兰达,fengtiand","本田CR-V,bentianc","别克昂科威,biekeg"],
"紧凑车":["卡罗拉,fengtiani","思域,bentiana","朗逸,dazhongd","悦动,hyundaiab1","宝来,dazhongo","凯越,biekea"],
"中型车":["天籁,richang","迈腾,dazhongr","锐志,fengtiank","帕萨特,dazhongg","宝马3系,bmwa","君越,biekee"],
"中大型车":["奔驰E系,benzb","克莱斯勒300C,chryslera","宝马5系,bmwb","奥迪A6L,audia6l"],
"跑车":["法拉利458,ferrayh","欧陆,binlia","保时捷Cayman,porschee","奥迪TT,auditt"]
};
var brand_class_ad= { 

                "安驰": ["小公主,anchixgz","雪豹,anchixb","威豹,anchiwb","瑞驰征途,anchirczt","瑞驰K3,anchirck3","瑞驰K5,anchirck5"],
				
				"奥迪": ["奥迪A3,audia3","奥迪A4L,audia4l","奥迪A6L,audia6l","奥迪Q3,audiq3","奥迪Q5,audiq5","奥迪A4,audia4","奥迪A6,audia6","奥迪A1,audia1","奥迪A3(进口),audia3jk","奥迪S3,audis3",
				         "奥迪A4(进口),audisa4jk","奥迪A5,audisa5","奥迪S5,audis5","奥迪A6(进口),audia6jk","奥迪S6,audis6","奥迪A7,audia7","奥迪S7,audis7","奥迪A8,audia8","奥迪S8,audis8","奥迪Q3(进口),audiq3jk",
				         "奥迪Q5(进口),audiq5jk","奥迪SQ5,audisq5","奥迪Q7,audiq7","奥迪TT,auditt","奥迪TTS,auditts","奥迪R8,audir8","奥迪RS5,audirs5"
				        ],
				        
				"阿斯顿马丁":["Rapide,astonmartina","V8 Vantage,kastonmartinb","V12 Vantage,astonmartinc","阿斯顿马丁DB9,astonmartind",
				             "Vanquish,astonmartine","Virage,astonmartinf","阿斯顿马丁DBS,astonmarting"
				             ],
				             
				"阿尔法罗密欧":["ALFA GT,alfaa"],
				
				"巴博斯":["巴博斯S级,barbusa","巴博斯M级,barbusb"],
				
				"宝龙":["霸道,baolonga","天马座,baolongb","菱惠,baolongc","菱麒,baolongd","菱骏,baolonge"],
				
				"保时捷":["Panamera,porschea","Macan,porscheb","卡宴,porschec","Boxster,porsched","Cayman,porschee","保时捷911,porschef"],
				
				"别克":["凯越,biekea","威朗,biekeb","英朗,biekec","君威,bieked","君越,biekee","昂科拉,biekef","昂科威,biekeg","别克GL8,biekeh",
				       "林荫大道,biekei","荣御,biekej","昂科雷,biekek"],
				
				"宝马":["宝马3系,bmwa","宝马5系,bmwb","宝马X1,bmwc","宝马1系,bmwd","宝马2系运动旅行车(进口),bmwjke","宝马3系(进口),bmwjkf","宝马3系GT,bmwg","宝马4系,bmwh",
						"宝马5系(进口),bmwjki","宝马5系GT,bmwj","宝马6系,bmwk","宝马7系,bmwl","宝马X3,bmwm","宝马X4,bmwn","宝马X5,bmwo","宝马X6,bmwp","宝马2系,bmwq",
						"宝马Z4,bmwr","宝马X1(进口),bmwjks","宝马M3,bmwt","宝马M4,bmwu","宝马M5,bmwv","宝马M6,bmww","宝马X5 M,bmwx","宝马X6 M,bmwy",
						"宝马1系M,bmwz"
						],
						
				"奔驰":["奔驰C级,benza","奔驰E级,benzb","奔驰GLA级,benzc","奔驰GLK级,benzd","奔驰威霆,benze","唯雅诺,benzf",
						"奔驰凌特,benzg","奔驰A级,benzh","奔驰B级,benzi","奔驰CLA级,benzj","奔驰C级(进口),benzk","奔驰E级(进口),benzl",
						"奔驰CLS级,benzm","奔驰S级,benzn","奔驰G级,benzp","奔驰GL级,benzq","奔驰R级,benzr",
						"奔驰SLK级,benzs","奔驰SL级,benzt","奔驰GLA级(进口),benzu","奔驰M级,benzv","Sprinter,benzw","奔驰威霆(进口),benzo",
						"奔驰GLK级(进口),benzx","唯雅诺(进口),benzy","奔驰CLK级,benzz","奔驰A级AMG,benzab","奔驰CLA级AMG,benzac","奔驰C级AMG,benzad","奔驰CLS级AMG,benz1ae",
						"奔驰S级AMG,benz1af","奔驰M级AMG,benz1ah","奔驰G级AMG,benz1ai","奔驰GL级AMG,benz1aj","奔驰SL级AMG,benz1ak","奔驰SLS级AMG,benz1am","奔驰E级AMG,benz1an",
						"奔驰SLK级AMG,benz1ao","迈巴赫S级,benz1ap"
						],
				
              	"北汽制造":["旋风,beiqia","骑士,beiqib","陆霸,beiqic","战旗,beiqid","雷驰,beiqie","陆铃,beiqif","御虎,beiqig",
              			  "京城海狮,beiqih","勇士,beiqii","域胜007,beiqij","北京212,beiqik","角斗士,beiqil","越铃,beiqim",
              			  "锐铃,beiqin","北京BW007,beiqio"],
              			  
              	"本田":["思域,bentiana","思迪,bentianb","CR-V,bentianc","奥德赛,bentiand","飞度,bentiane","雅阁,bentianf","锋范,bentiang",
					   "Element,bentiank","Insight,bentianm","里程,bentiann","时韵,bentiano",
						"思铂睿,bentianp","歌诗图,bentianr","CR-Z,bentians","理念,bentiant","思铭,bentianw","艾力绅,bentianx",
						"凌派,bentiany","杰德,bentianz","缤智,bentian1aa","XR-V,bentian1ab","哥瑞,bentian1ac"
						],
						
				"北京汽车":["E系列,bjqca","威旺306,bjqcb","威旺205,bjqcc","威旺M20,bjqcd","威旺206,bjqce","威旺307,bjqcf","威旺T205-D,bjqcg",
						"威旺007,bjqch","威旺M30,bjqci","北京40,bjqcj","E150EV,bjqck","EV200,bjqcl",
					   "ES210,bjqcm","EV160,bjqcn","绅宝D70,bjqco","绅宝D50,bjqcp","绅宝D60,bjqcq","绅宝D20,bjqcr","绅宝X65,bjqcs",
					   "绅宝CC,bjqct","绅宝D80,bjqcu"
						],
						
				"奔腾":["B70,bentenga","B50,bentengb","B90,bentengc","X80,bentengd","奔腾B30,bentenge"],
				
				"比亚迪":["比亚迪S8,byda","比亚迪F3R,bydb","福莱尔,bydc","比亚迪F6,bydd","比亚迪F3,byde","比亚迪F0,bydf","比亚迪F3DM,bydg","比亚迪G3,bydh","比亚迪M6,bydi",
						"比亚迪L3,bydj","比亚迪G3R,bydk","比亚迪S6,bydl","比亚迪G6,bydm","比亚迪E6,byd1n","速锐,bydo","思锐,bydp","秦,bydq","腾势,bydr",
						"比亚迪G5,byds","比亚迪S7,bydt","唐,bydu","宋,bydv"
						],
				
				"宝骏":["宝骏630,baojuna","宝骏乐驰,baojunb","宝骏610,baojunc","宝骏730,baojund","宝骏560,baojune"],
				
				"宾利":["欧陆,binlia","雅致,binlib","慕尚,binlic","雅骏,binlid","飞驰,binlie","添越,binlif"],
				
				"布加迪":["威航,bjda"],
				
				"标致":["标致301,biaozhia","标致308,biaozhib","标致308S,biaozhic","标致408,biaozhid","标致508,biaozhie","标致3008,biaozhif","标致2008,biaozhig",
						"标致206,biaozhih","标致207,biaozhii","标致307,biaozhij","标致RCZ,biaozhik","标致308(进口),biaozhijkl","标致3008(进口),biaozhijkm",
						"标致206(进口),biaozhijkn","标致207(进口),biaozhijko","标致307(进口),biaozhijkp","307旅行车,biaozhiq","标致407,biaozhir","407旅行车,biaozhis",
						"标致607,biaozhit","307Cross,biaozhiu","207Cross,biaozhiv"
						],
						
				"北汽幻速":["幻速S2,bqhsa","幻速S3,bqhsb","幻速H2,bqhsc","幻速H3,bqhsd"],
				
				"长丰":["猎豹CS6,changfenga","骐菱,changfengb","黑金刚,changfengc","飞腾,changfengd","长丰奇兵,changfenge",
						"DUV,changfengf","飞铃,changfengg","猎豹CS7,changfengh","飞扬,changfengi",
						"猎豹CT5,changfengk","飞腾C5,changfengl","猎豹Q6,changfengm","猎豹CS10,changfengn"
						],
						
				"长安":["CM8,changana","CS35,changanb","CS75,changanc","CX20,changand","CX30,changane","奔奔,changanf","尊行,changang",
						"志翔,changanh","悦翔,changani","星光,changanj","星光小卡,changank","星韵,changanl","杰勋,changanm","欧力威,changann",
						"欧诺,changano","长安欧雅,changanp","睿行,changanq","睿骋,changanr","绿色新星,changans","致尚XT,changant","运动星,changanu",
						"运通,changanv","逸动,changanw","都市彩虹,changanx","金牛星,changany",
						"长安之星,changanz","跨越新豹,changanaa1","星卡,changanab1","镭蒙,changanac1","雪虎,changanad1","神琪,changanae1"
						],
				
				"长城":["长城M2,changchenga","长城C30,changchengb","长城C50,changchengc","长城M4,changchengd","风骏5,changchenge",
						"风骏3,changchengf","长城精灵,changchengg","酷熊,changchengh","炫丽,changchengi","长城M1,changchengj",
						"赛弗,changchengk","长城V80,changchengl","嘉誉,changchengm"
						],
						
				"昌河":["海豚,changhea","海象,changheb","福瑞达,changhec","爱迪尔,changhed","骏马,changhee","福运,changhef",
						"福瑞达M50,changheg"
						],
				
				"道奇":["凯领,daoqia","锋哲,daoqib","酷搏,daoqic","蝰蛇,daoqid","酷威,daoqie","Challenger,daoqif",
					   "翼龙,daoqig","公羊,daoqih","Ram,daoqii"
					  ],
					  
				"大宇":["典雅,dayua","旅行家,dayub","马蒂兹,dayuc"],
				
				"大迪":["都市威菱,dadia","都市骏马,dadib","大迪霸道,dadic","顺驰,dadid"],
				
				"东南":["菱利,dongnana","富利卡,dongnanb","菱帅,dongnanc","得利卡,dongnand","菱动,dongnane",
					   "菱悦,dongnanf","希旺,dongnang","菱致,dongnanh","菱仕,dongnani","东南DX7,dongnanj"
					   ],
				
				"大众":["POLO,dazhonga","桑塔纳·尚纳,dazhongb","朗行,dazhongc","朗逸,dazhongd","朗境,dazhonge","凌渡,dazhongf",
						"帕萨特,dazhongg","途观,dazhongh","途安,dazhongi","Passat领驭,dazhongj","桑塔纳经典,dazhongk",
						"桑塔纳志俊,dazhongm","捷达,dazhongn","宝来,dazhongo","高尔夫,dazhongp","速腾,dazhongq","迈腾,dazhongr",
						"一汽-大众CC,dazhongs","开迪,dazhongt","甲壳虫,dazhongu",
						"高尔夫(进口),dazhongv","辉腾,dazhongw","迈腾(进口),dazhongx","途锐,dazhongy",
						"夏朗,dazhongz","迈特威,dazhongaa1","凯路威,dazhongab1","尚酷,dazhongac1",
						"大众CC,dazhongad1","大众Eos,dazhongae1","桑塔纳3000,dazhongaf1","UP!,dazhongag1"
						],

				"大通":["V80,datonga","G10,datongb"],
				
				"帝豪":["EC7,dihaoa","EC8,dihaob"],
				
				"东风":["小康K07,dongfenga","小康K07-ii,dongfengb","小康K17,dongfengc","小康K06,dongfengd","小康V27,dongfenge",
						"小康V29,dongfengf","小康C37,dongfengg","小康V26,dongfengh","小康V07S,dongfengi","小康风光,dongfengj",
						"小康C35,dongfengk","小康K02,dongfengl","小康K05,dongfengm","小康K01,dongfengn","小康V21,dongfengo",
						"小康V22,dongfengp","小康C36,dongfengq","小康C32,dongfengr","小康K07S,dongfengs",
						"景逸,dongfengt","风行,dongfengu","小王子,dongfengv","风神水星,dongfengw","菱智,dongfengx","菱越,dongfengy",
						"菱通,dongfengz","风神S30,dongfengaa1","风神H30,dongfengab1","EQ7240BP,dongfengac1","猛士,dongfengad1",
						"风神H30 Cross,dongfengae1","景逸Cross,dongfengaf1","途逸,dongfengag1","风神A60,dongfengah1","景逸SUV,dongfengai1",
						"俊风CV03,dongfengaj","御风,dongfengak","景逸X5,dongfengal","虎视,dongfengam","信天游,dongfengan1",
						"景逸X3,dongfengap1","风行CM7,dongfengaq1","景逸S50,dongfengar1","风神A30,dongfengas1","风神AX7,dongfengat1","风神L60,dongfengau1",
						"景逸XV,dongfengav1","风度MX6,dongfengaw1","风光360,dongfengax1","风光330,dongfengay1"
						],
				
				"菲斯克":["Karma,fsikea"],
				
				"福田":["风景G7,futiana","福田风景,futianb","蒙派克E,futianc","蒙派克S,futiand","迷迪,futiane"],
				
				"菲亚特":["菲翔,fyatea","致悦,fyateb","派朗,fyatec","派力奥,fyated","西耶那,fyatee","周末风,fyatef","菲跃,fyateg","博悦,fyateh"],
				
				"福迪":["小超人,fudia","探索者,fudib","揽福,fudic","雄狮,fudid","飞越,fudie"],   
				
				"丰田":["致炫,fengtiana","雷凌,fengtianb","凯美瑞,fengtianc","汉兰达,fengtiand","逸致,fengtiane","雅力士,fengtianf",
						"威驰,fengtiang","花冠,fengtianh",
						"卡罗拉,fengtiani","普锐斯,fengtianj","锐志,fengtiank","皇冠,fengtianl","一汽丰田RAV4,fengtianm","兰德酷路泽,fengtiann",
						"普拉多,fengtiano","柯斯达,fengtianp","FJ 酷路泽,fengtianr",
						"威飒,fengtiant","红杉,fengtianv","埃尔法,fengtianw","普瑞维亚,fengtianx",
						"丰田86,fengtianz","杰路驰,fengtianaa1","坦途,fengtianac1","普拉多(进口),fengtianad1",
						"丰田RAV4(进口),fengtianae1","汉兰达(进口),fengtianaf1","丰田佳美,fengtianag1","Wish,fengtians","奔跑者,fengtianq"
						],

				"福特":["S-MAX,forda","嘉年华,fordb","福克斯,fordc","福睿斯,fordd","翼搏,forde","翼虎,fordf","蒙迪欧,fordg","锐界,fordh",
						"全顺,fordi","嘉年华(进口),fordk","房车E系列,fordl","探险者,fordm","爱虎,fordn","猛禽F系,fordo",
						"福克斯(进口),fordp","翼虎(进口),fordr","蒙迪欧(进口),fords","野马,fordt","锐界,fordu","金牛座,,fordv"
						],
				

				
				"法拉利":["488,ferraya","575,ferrayb","599,ferrayc","612,ferrayd","360,ferraye","F430,ferrayf","F149,ferrayg",
						 "458,ferrayh","FF,ferrayii","F12,ferrayj","LaFerrari,ferrayk"
						],
						
				"光冈":["大蛇,mitsuokaa","女王,mitsuokab","嘉路,mitsuokac"
						],
			
				"GMC":["阿卡迪亚,gmca","使节,gmcb","Savana,gmcc","育空河,gmcd","西拉,gmce","Terrain,gmcf"],
				
				"广汽":["传祺GA5,guangqia","传祺GS5,guangqib","传祺GA3,guangqic","传祺GA3S视界,guangqid",
						"传祺GA6,guangqie","传祺GS4,guangqif"],
				
				"观致":["观致3,guanzhia","观致3都市SUV,guanzhib"],
				
				
				"GUMPERT":["阿波罗,gumperta"],
				
				"悍马":["悍马H2,hanmaa","悍马H3,hanmaab"],
				
				"华泰":["圣达菲,huataia","特拉卡,huataib","吉田,huataic","B11,huataid","宝利格,huataie","路盛E70,huataif"],
				
				"黑豹":["朗杰,heibaoa","多功能车,heibaob"],
				
				
			   "黄海":["傲羚,huanghaia","傲骏,huanghaib","傲龙CUV,huanghaic","大柴神,huanghaid","小柴神,huanghai2d","挑战者,huanghaie",
			   		"旗胜F1,huanghaif","旗胜V3,huanghaig","翱龙CUV,huanghaih","领航者CUV,huanghaii","黄海N1,huanghaij","黄海N2,huanghaik"
			   		],

			   
			   "哈飞":["中意,hafeia","松花江,hafeib","民意,hafeic","赛豹,hafeie","赛马,hafeif",
			          "路宝,hafeig","路尊大霸王,hafeibh","路尊小霸王,hafeibi","骏意,hafeij"
			          ],
			   
			   "一汽红旗":["H7,flaga","HQ3,flagb","L5,flagc","世纪星,flagd","旗舰,flage","明仕,flagf","盛世,flagg","红旗,flagh"
			   		 ],
			   
			   "华翔":["驭虎,huaxianga","富奇,huaxiangb"],
			   
			   "华北":["腾狮RV,huabeia","骏霸,huabeib","超赛,huabeic","醒狮,huabeid"],
			   
			   "海马":["323,haimaa","海马M3,haimab","海马M6,haimac","海马M8,haimad","海马S5,haimae","海马S7,haimaf","丘比特,haimag","普力马,haimah",
			   		  "欢动,haimai","海福星,haimaj","海马3,haimak","福美来,haimal","福美来M5,haimam","海马骑士,haimao",
			   		  "爱尚,haimap","王子,haimaq","福仕达,haimar"
			   		],
       
			   "汇众":["伊斯坦纳,huizhonga","德驰,huizhongb"],
			   
			   "华普":["M203,huapua","朗风,huapub","杰士达美鹿,huapuc","海域,huapud","海尚,huapue","海悦,huapuf","海景,huapug","海炫,huapuh",
			   		   "海迅,huapui","海锋,huapuj","飓风,huapuk","飙风,huapul"
			   		   ],
			   
			              
			   "华阳":["五菱,huayanga"],
			   
			   "恒天":["途腾T1,hengtiana","途腾T2,hengtianb","途腾T3,hengtianc"],
			   

		   	   "航天":["成功一号,hangtiana"],
		   		
		   		"海格":["H5C,haigea","H6C,haigeb","H6V,haigec","御骏,haiged","龙威,haigee"],
		   		
		   		"华颂":["华颂7,huasonga"],
		   		
		   		"金龙":["金威,jinlonga","金龙海狮,jinlongb"],
		   		  
		   		"金程":["先锋,jinchenga","横行,jinchengb","赛风,jinchengc","金程之星,jinchengd","金程海狮,jinchenge","领跑,jinchengf"],
		   		     
		   		"吉利":["中国龙,geelya","优利欧,geelyb","博瑞,geelyc","吉利SC3,geelyd","吉利帝豪,geelye","吉利海景,geelyf","美人豹,geelyg",
		   				"美日,geelyh","自由舰,geelyi","豪情SUV,geelyj","豹风GT,geelyk","远景,geelyl","金鹰,geelym","金刚,geelyn",
		   				"豪情,geelyo","雳靓,geelyp","GC7,geelyq","GX2,geelyr","GX7,geelys","熊猫,geelyt"
		   				],
		   		

		   		"金杯":["大力神,jinbeia","智尚S30,jinbeib","小海狮X30,jinbeic","海星,jinbeid","金典,jinbeie","金杯750,jinbeif","金杯S50,jinbeig",
		   				"金杯T30,jinbeih","金杯T32,jinbeii","金杯海狮,jinbeij","金杯霸道,jinbeik","锐驰,jinbeil","阁瑞斯,jinbeim",
		   				"雷龙,jinbein"
		   				],
		   		
		   		             
		   		"江淮":["iEV5,jianghuaia","凌铃,jianghuaib","同悦,jianghuaic","和悦,jianghuaid","和悦A13Cross,jianghuaie",
		   				"和悦A13,jianghuaie2","和悦A30,jianghuaif",
		   				"宾悦,jianghuaig","帅铃T6,jianghuaih","悦悦,jianghuaii","星锐,jianghuaij","瑞铃,jianghuaik","瑞风,jianghuail",
		   				"瑞风M3,jianghuaim","瑞风M5,jianghuain","瑞风S3,jianghuaio","瑞风S5,jianghuaip","瑞鹰,jianghuaiq"
		   				],
		   		
		   		                
		   		
		   		"吉奥":["E美,jiaoa","GP150,jiaob","GS50,jiaoc","GX6,jiaod","伊美,jiaoe","吉奥凯旋,jiaof","凯睿,jiaog","奥腾,jiaoh",
		   				"奥轩G3,jiaoi","奥轩G5,jiaoj","奥轩GX5,jiaok","帅凌,jiaol","帅威,jiaom","帅舰,jiaon",
		   				"帅豹,jiaoo","帅驰,jiaop","星旺,jiaoq","星朗,jiaor","猛将旅,jiaos","财运,jiaot"
		   				],
		   		                   
		   		"江铃":["域虎,jianglinga","宝典,jianglingb","宝威,jianglingc","江铃轻卡,jianglingd","运霸,jianglinge","驭胜,jianglingf","骐铃,jianglingg","骐铃T7,jianglingh"],
		   		      
		   		
			   "吉普":["JEEP2500,jeepa","JEEP2700,jeepb","大切诺基(进口),jeepjkc","城市猎人,jeepd","大切诺基,jeepe","切诺基(进口),jeepjkf","指南者,jeepg",
			   			"指挥官,jeeph","牧马人,jeepi","自由人,jeepj","自由光,jeepk","自由客,jeepl"	
			   		 ],
    
			   "江南":["江南TT,jiangnana","传奇,jiangnanb","奥拓,jiangnanc","精灵,jiangnand","风光,jiangnane"],
			       
			   "九龙":["天马商务车,jiulonga","艾菲,jiulongb"],
			    
			   "捷豹":["F-Type,jiebaoa","S-Type,jiebaob","X-Type,jiebaoc","XE,jiebaod","XF,jiebaoe","XJ,jiebaof","XK,jiebaog"],

			   			   
			   "开瑞":["K50,kairuia","优优,kairuib","优劲,kairuic","优派,kairuid","优翼,kairuie","优胜,kairuif","优雅,kairuig",
			   		   "杰虎,kairuih","绿卡,kairuii"
			   		],

			   "克莱斯勒":["300C,chryslera","铂锐,chryslerb","大捷龙,chryslerc","300C(进口),chryslerjkd","300S,chryslere","PT漫步者,chryslerf",
  		   			      "交叉火力,chryslerg","大捷龙(进口),chryslerjkh","彩虹,chrysleri","赛百灵,chryslerj"
			   			],

			   "柯尼赛格":["Agera,koenigsegga","CCR,koenigseggb","CCX,koenigseggc","CCXR,koenigseggd"],
 
			   "卡尔森":["GL级,kersa","S级,kersb"],

			   "凯迪拉克":["ATS-L,cadillaca","XTS,cadillacb","CTS,cadillacc","赛威SLS,cadillacd","ATS,cadillace","CTS(进口),cadillajkcf",
			   			  "SRX,cadillacg","XLR,cadillach","凯雷德,cadillaci"
			   			 ], 
			   

			   "凯佰赫":["战盾,kaibaihea"],
			   
			   "KTM":["X-BOW,ktma"],
			   
			   "卡威":["K1,kaweia","W1,kaweib"],

			   "凯马汽车":["凯马皮卡,kaimaa"],
			   
			   "凯翼":["C3,kaiyia","C3R,kaiyib"],
			    
			   "康迪":["康迪小电跑,kangdia","康迪熊猫,kangdib"],
			    
			   "雷诺":["卡缤,renaulta","塔利斯曼,renaultb","威赛帝,renaultc","拉古那,renaultd","拉古那Couple,renaulte",
        			   "梅甘娜,renaultf","梅甘娜,renaulth","梅甘娜CC,renaulti","梅甘娜Couple,renaultj",
					  "科雷傲,renaultk","纬度,renaultl","风景,renaultm","风朗,renaultn"
			   		 ],
			           
  
			   "莲花":["L3两厢,lianhuaa","L3三厢,lianhuab","L5两厢,lianhuac","L5三厢,lianhuad"
			   			],
			   
			   
			   "雷克萨斯":["雷克萨斯CT,lexusa","雷克萨斯ES,lexusb","雷克萨斯GS,lexusc","雷克萨斯GX,lexusd","雷克萨斯IS,lexuse","雷克萨斯LFA,lexusf",
			   				"雷克萨斯LS,lexusg","雷克萨斯LX,lexush","雷克萨斯NX,lexusi","雷克萨斯RC,lexusj",
			   			   "雷克萨斯RX,lexusk","雷克萨斯SC,lexusl"
			   			   ],
			   
			              
			   "路特斯":["Elise,lutesia","Evora,lutesib","Exige,lutesic","竞悦,lutesid","竞速,lutesie"
			   			],
			   
			       
			   "林肯":["Blackwood,lincolna","LS,lincolnb","MKC,lincolnc","MKS,lincolnd","MKT,lincolne","MKX,lincolnf",
			   		   "MKZ,lincolng","城市,lincolnh","领航员,lincolni"
			   			],
			           
			   "铃木":["启悦,suzukia","天语SX4两厢,suzukib","天语SX4三厢,suzukic","铃木奥拓,suzukid","尚悦,suzukie","羚羊,suzukif","锋驭,suzukig",
			   			"雨燕,suzukih","利亚纳两厢,suzukii","利亚纳三厢,suzukij","利亚纳A6两厢,suzukik","利亚纳A6三厢,suzukil","昌铃王,suzukim",
			   			"浪迪,suzukin","派喜,suzukio","凯泽西,suzukip","吉姆尼,suzukiq","超级维特拉,suzukir","速翼特,suzukis","北斗星,suzukit"
			   			],
			   
         
			    "力帆":["320,lifana","330,lifanb","520两厢,lifanc","520三厢,lifand","530,lifane","620,lifanf","630,lifang","720,lifanh","T21,lifani",
			   		   "X50,lifanj","X60,lifank","丰顺,lifanl","乐途,lifanm","兴顺,lifann","力帆820,lifano","福顺,lifanp"	
			   		   ],
			                 
			   "路虎":["卫士,landrovea","发现,landroveb","发现神行,landrovec","揽胜,landroved","极光,landrovee","神行者,landrovef",
			   		   "揽胜极光,landroveg","揽胜运动版,landroveh"
			   		  ],
			         
			   "兰博基尼":["Aventador,lamborghinia","Huracan,lamborghinib","Reventon,lamborghinic","盖拉多,lamborghinid","蝙蝠,lamborghinie"
			   			],
			       
			   "劳斯莱斯":["古思特,rollsroycea","幻影,rollsroyceb","银刺,rollsroycec","银影,rollsroyced","银灵,rollsroycee","魅影,rollsroycef"
			   			],

			   "陆风":["X6,lufenga","X8,lufengb","X9,lufengc","新饰界,lufengd","X7,lufenge","风华,lufengf","风尚,lufengg","陆风X5,lufengh"
			   		 ],
			          
			   "陆地方舟":["陆地方舟V5,ludishipa","艾威,ludishipb","陆地方舟陆风,ludishipc"],
 
			   "罗孚":["罗孚75,luofua"],
			   
			   "摩根":["4/4,mogena","Aero Coupe,mogenb","Aero SuperSports,mogenc","Plus 4,mogend","Plus 8,mogene","摩根Roadster,mogenf","三轮车,mogenh"
			   			],
			   
			   "迷你":["Clubman,minia","Roadster,minib","Countryman,minic","Paceman,minid","Cooper,minie","Couple,minif","One,minig","敞篷版,minih"],
    
			   "迈巴赫":["57,maybacha","62,maybachb"],
			   
			   "迈凯伦":["540C,maikailuna","650S,maikailunb","MP4-12C,maikailunc","P1,maikailund"],
			      
			   "名爵":["MG 3SW,mingjuea","MG 7,mingjueb","MG TF,mingjuec","TF,mingjued","MG 3,mingjuee","MG 5,mingjuef","MG 6 两厢,mingjueg",
			   			"MG 6 三厢,mingjueh","锐行GT,mingjuei","锐腾,mingjuej","名爵3系SW,mingjuek"],

			   
			   "马自达":["马自达2,mazdaa","马自达3 Axela昂克赛拉,mazdab","马自达3星骋,mazdac","马自达CX-5,mazdad","马自达2劲翔,mazdae","马自达3,mazdaf",
			   			"阿特兹,mazdag","马自达6,mazdah","睿翼,mazdai","马自达CX-7,mazdaj","马自达8,mazdak","马自达CX-7(进口),mazdal","马自达CX-9,mazdam",
			   			"马自达5,mazdan","马自达MX-5,mazdao","马自达3(进口),mazdap",
						"ATENZA,mazdaq","马自达RX-8,mazdar"
						],
			   
			   "玛莎拉蒂":["3200GT,maseratia","Coupe,maseratib","Ghibli,maseratic","GranCabrio,maseratid","GranSport,maseratie","GranTurismo,maseratif",
			   				"总裁,maseratig"
			   				],
			         
			   "美亚":["奇兵,meiyaa","美亚奇骏,meiyab","海狮,meiyac","陆程,meiyad"],
			       
			   "纳智捷":["5 Sedan,nazhijiea","CEO,nazhijieb","优6,nazhijiec","大7 MPV,nazhijied","大7 SUV,nazhijiee"
			   			],
			        
			   "南汽":["优尼柯,nanqia","君达,nanqib","新雅途,nanqic"],  
		   	   
		   	   "讴歌":["ILX,ougea","MDX,ougeb","RDX,ougec","RL,ouged","RLX,ougee","TL,ougef","TLX,ougeg","ZDX,ougeh"
		   	   			],
		   	          
		   	   "欧宝":["威达两厢,opela","威达三厢,opelb","安德拉,opelc","欧捷利,opeld","欧美佳两厢,opele","欧美佳三厢,opelf",
		   	   			"英速亚两厢,opelg","英速亚三厢,opelh","英速亚旅行版,opeli","赛飞利,opelj",
		   	   			"雅特两厢,opelm","雅特三厢,opeln","雅特敞篷车,opelo","麦瑞纳,opelp"
		   	   		 ],
			   	               
	   	   "帕加尼":["Zonda,pajiania"],
		   	   
		   	  
		   		"起亚":["起亚K2,kiaa","秀尔,kiab","赛拉图,kiac","福瑞迪,kiad","起亚K3,kiae","起亚K4,kiaf","起亚K5,kiag","狮跑,kiah","智跑,kiai",
		   				"千里马,kiaj","锐欧,kiak","远舰,kial","嘉华,kiam","凯尊,kian","索兰托,kiao","霸锐,kiap","佳乐,kiaq","起亚VQ,kiar",
		   				"速迈,kias","欧菲莱斯,kiat"
		   				],
		   		
		   		"奇瑞":["奇瑞A1,qiruia","奇瑞A3,qiruib","奇瑞A5,qiruic","奇瑞E3,qiruid","奇瑞E5,qiruie","奇瑞eQ,qiruif","奇瑞QQ3,qiruig","奇瑞QQ6,qiruih","奇瑞QQme,qiruii",
		   				"奇瑞V5,qiruij","东方之子,qiruik","旗云,qiruil","旗云1,qiruim","旗云2,qiruin","旗云3,qiruio","旗云5,qiruip","爱卡,qiruiq",
		   				"瑞虎,qiruir","瑞虎3,qiruis","瑞虎5,qiruit","艾瑞泽3,qiruiu","艾瑞泽7,qiruiv","艾瑞泽M7,qiruiw","风云,qiruix",
		   				"风云2,qiruiy"
		   				],
		   		
		   		                        
		   		
		   		"日产":["启辰D50,richana","启辰R30,richanb","启辰R50,richanc","启辰R50X,richand","启辰T70,richane","启辰晨风,richanf","天籁,richang",
		   				"东风奇骏,richanh","楼兰,richani","玛驰,richanj","蓝鸟,richank","轩逸,richanl","逍客,richanm","阳光,richann",
		   				"颐达,richano","骊威,richanp","骏逸,richanq","骐达,richanr","NV200,richans","俊风,richant","多功能商用车,richanu",
		   				"奥丁,richanv","帅客,richanw","帕拉丁,richanx","帕拉骐,richany","御轩,richanz","风度MX6,richan1aa",
						"350Z,richan1ab","370Z,richan1ac","GT-R,richan1ad","Juke,richan1ae","Murano,richan1af","奇骏(进口),richan1ah",
		   				"奥蒂玛,richan1ai","桂冠,richan1aj","西玛,richan1ak","贵士,richan1al","途乐,richan1am",
		   				"风雅,richan1ap","马克西马,richan1aq","骐达(进口),richan1ar"
		   				],
                 

		   		"荣威":["荣威350,rongweia","荣威550,rongweib","荣威750,rongweic","荣威950,rongweid","荣威E50,rongweie","荣威W5,rongweif"
		   				],
		   		     
		   		"瑞麒":["瑞麒G3,ruilina","瑞麒G5,ruilinb","瑞麒G6,ruilinc","瑞麒M1,ruilind","瑞麒M5,ruiline","瑞麒X1,ruilinf","派拉蒙,ruiling"
		   				],
		   		      
		   		"陕汽通家":["福家,sqtjiaa","通家和瑞,sqtjiab"],
		   		 
		   		"双环":["双环SCEO,shuanghuanga","来宝S-RV,shuanghuangb","来旺,shuanghuangc","小贵族,shuanghuangd"],

		   		
		   		"双龙":["主席,shuanglonga","享御,shuanglongb","柯兰多,shuanglongc","爱腾,shuanglongd","路帝,shuanglonge","雷斯特,shuanglongf"
		   				],

		   		"斯柯达":["明锐,skodaa","晶锐,skodab","欧雅,skodac","速派,skodad","昊锐,skodae","Yeti,skodaf","明锐RS,skodag",
		   				  "法比亚三厢,skodah","晶锐Scout,skodai","昊锐旅行车,skodaj","昕锐,skodak","速派(进口),skodal","Yeti(进口),skodam",
		   				  "速尊旅行版,skodan","昕动,skodao","明锐旅行版,skodap"
		   				  ],

		   		
		   		"Smart":["ForTwo,smarta"],
		   
		 		"世爵":["C12,shijuea","C8,shijueb","D12,shijuec","D8,shijued"],
		 		   
		 		"三菱":["欧蓝德(进口),mitsubishib","帕杰罗(进口),mitsubishic","君阁,mitsubishid","戈蓝,mitsubishie",
		 				"翼神,mitsubishif","菱绅,mitsubishig","蓝瑟,mitsubishih","风迪思,mitsubishii","ASX劲炫,mitsubishij",
		 				"欧蓝德,mitsubishik","蓝瑟(进口),mitsubishil","帕杰罗,mitsubishim","蓝瑟翼豪陆神,mitsubishin",
		 				"劲炫,mitsubishio","帕杰罗劲畅,mitsubiship"
		 				],
		 				 		
		 		"斯巴鲁":["BRZ,subarua","XV,subarub","傲虎,subaruc","力狮,subarud","森林人,subarue","翼豹,subaruf","驰鹏,subarug"],
		 		      
		 		"萨博":["9月3日,saaba","9月5日,saabb"],
		 		    
		 		"天马":["英雄,tianmaa","风锐,tianmab","骏驰,tianmac"],
		 		
		 		"通宝":["宗申通宝,tongbaoa"],
			   
			   "天津一汽":["夏利,tjyqa","夏利A+,tjyqb","夏利N3,tjyqc","夏利N5,tjyqd","夏利N7,tjyqe","威乐,tjyqf","威姿,tjyqg","威志,tjyqh",
			   				"威志V2,tjyqi","威志V5,tjyqj","骏派D60,tjyqk"
			   				],
			              
			   
			   "特斯拉":["MODEL S,teslaa"],
			   
			   "威麟":["威麟H3,weilina","威麟H5,weilinb","威麟V5,weilinc","威麟X5,weilind"],
			      
			   "威兹曼":["威兹曼GT,weizimana","威兹曼敞篷车,weizimanb"],
			    			   
			   "沃尔沃":["S40,volvoa","S80L,volvob","C30,volvof","C70,volvog","S40(进口),volvoh",
						"S60,volvoi","S80,volvok","V40,volvom","V60,volvon","XC60(进口),volvoo","XC90,volvop",
						"S60L,volvoq","XC Classic,volvor","XC60,volvos"	
					   ],

			   "新凯":["凯胜,xinkaia","新凯之星,xinkaib","新凯靓星,xinkaic","都市之星,xinkaid"],
			   
			      
		   	   "西雅特":["伊比飒,seata","利昂,seatb","欧悦博,seatc"],
		   		
		   		  
		   		"现代":["Trajet,hyundaia","劳恩斯,hyundaic","劳恩斯酷派,hyundaid",
		   				"捷恩斯,hyundaig","格锐,hyundaih","维拉克斯,hyundail",
		   				"美佳,hyundaim","新胜达,hyundain","辉翼,hyundaio","酷派,hyundaiq","雅尊,hyundair",
		   				"雅科仕,hyundais","飞思,hyundait","i30,hyundaiu","ix25,hyundaiv","ix35,hyundaiw","伊兰特,hyundaix",
		   				"名图,hyundaiy","名驭,hyundaiz","御翔,hyundaiaa1","悦动,hyundaiab1","朗动,hyundaiac1","瑞奕,hyundaiad1",
		   				"瑞纳,hyundaiae1","索纳塔,hyundaiaf1","胜达,hyundaiag1","途胜,hyundaiah1","雅绅特,hyundaiai1","领翔,hyundaiaj1"
		   				],
		   		
		   		"雪铁龙":["C2,citroena","C3-XR,citroenb","C4L,citroenc","C5,citroend","世嘉,citroene","凯旋,citroenf","富康,citroeng","毕加索,citroenv",
		   				  "爱丽舍,citroenh","赛纳,citroeni","C4,citroenj","C4 Aircross,citroenk","C4毕加索,citroenl","C5(进口),citroen","C6,citroenm",
		   				  "DS3,citroenn","DS4,citroeno","DS5,citroenp","DS 5LS,citroens","DS5(进口),citroent","DS6,citroenu","雪铁龙C4世嘉,citroenv"
		   				  ],
		   		
		   		"雪佛兰":["乐风,chevroleta","乐骋,chevroletb","创酷,chevroletc","景程,chevroletd","爱唯欧,chevrolete","科帕奇,chevroletf",
						 "科鲁兹,chevroletg","赛欧,chevroleth","迈锐宝,chevroleti","乐驰,chevroletj",
						 "克尔维特,chevroletm","斯帕可,chevroletn","沃蓝达,chevroleto","特使,chevroletp","科帕奇,chevroletq","科迈罗,chevroletr",
						 "蒙特卡洛,chevrolets","豪放,chevrolett"
						 ],

		   		"永源":["永源五星,yongyuana","猎鹰,yongyuanb","飞碟UFO,yongyuanc"],
		   		  
			   "一汽":["特锐,yiqia","自由风,yiqib","森雅M80,yiqic","森雅S80,yiqid","欧朗,yiqie"],
			   
			   
			   "英菲尼迪":["英菲尼迪ESQ,infinitia","英菲尼迪EX,infinitib","英菲尼迪FX,infinitic","英菲尼迪G,infinitid","英菲尼迪JX,infinitie","英菲尼迪M,infinitif","英菲尼迪Q50,infinitig",
			   				"英菲尼迪Q60,infinitih","英菲尼迪Q60S,infinitii","英菲尼迪Q70L,infinitij","英菲尼迪QX,infinitik","英菲尼迪QX50(进口),infinitil","英菲尼迪QX60,infinitim",
			   				"英菲尼迪QX70,infinitin","英菲尼迪QX80,infinitio","英菲尼迪Q50L,infinitip","英菲尼迪QX50,infinitiq"
			   				],

   
			   "英伦":["英伦SC3,englona","英伦SC5,englonb","英伦SC6,englonc","英伦SC7,englond","英伦SX7,englone","英伦TX4,englonf"],
			        
			   "野马":["野马F10,yemaa","野马F12,yemab","野马F16,yemac","野马F99,yemad","野马T70,yemae"],	
			       
			   "英致":["英致G3,yingzhia","英致737,yingzhib"],
			   
			   "中誉":["凌特,zhongyua","威霆,zhongyub","假日风情,zhongyuc","迷你巴士,zhongyud"],
			     
			   "中顺":["世纪,zhongshuna"],
	
			   "中兴":["中兴C3,zhongxinga","无限,zhongxingb"],
			   
			   "中华":["H220,zhonghuaa","H230,zhonghuab","H320,zhonghuac","H330,zhonghuad","H530,zhonghuae","中华V5,zhonghuaf","华晨中华,zhonghuag",
			   			"中华V3,zhonghuah","中华豚,zhonghuai","尊驰,zhonghuaj","酷宝,zhonghuak","骏捷,zhonghual","骏捷Cross,zhonghuam",
			   			"骏捷FRV,zhonghuan","骏捷FSV,zhonghuao","骏捷Wagon,zhonghuap"
			   			],
			                  
			   "众泰":["2008,zhongtaia","梦迪博朗,zhongtaib","5008,zhongtaic","M300朗悦,zhongtaid","朗骏Z200,zhongtaie",
			   			"郎朗Z200HB,zhongtaif","Z300,zhongtaig","T200,zhongtaih","V10,zhongtaii","Z100,zhongtaij","T600,zhongtaik",
			   			"知豆,zhongtail","云100,zhongtaim","Z500,zhongtain","大迈X5,zhongtaio"
			   			],

			    "中欧":["尊逸,zhongoua"],
           };
