/*用户收藏*/
function car_collect(obj) {
	

	if($(obj).html()=="收藏车辆") {
		//未收藏
		var collect_type = 0;
		
	}
	else {
		//已收藏
		var collect_type = 1;
		
	}
	$.ajax({
			  type:"POST",
			  url:"configure/car_collect.php",
			  cache:false,
			  data:{"car_id":$(obj).val(),
			  		"collect_type":collect_type,
			  },
			  
			  success:function(data) {
				  
				  var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				  //失败
				  if(collect_type==0) {
					  if(data==0) {
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">收藏失败！请刷新后重试~</p>';
						  show_clue(content);
					  }
					  //未登录
					  else if(data==2) {  
						  show_model(0);
					  }
					  //成功
					  else if(data==1) {
						  $(".collect-img").attr('src','images/star_full.png');
						  $(obj).html("已收藏");
					  }
				  }
				  else {
					  if(data==0) {
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">取消失败！请刷新后重试~</p>';
						  show_clue(content);
					  }
					  //未登录
					  else if(data==2) {  
						  show_model(0);
					  }
					  //成功
					  else if(data==1) {
						  $(".collect-img").attr('src','images/star.png');
						  $(obj).html("收藏车辆");
					  }
				  }
			  },
		});
}
//用户分享