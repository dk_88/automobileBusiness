// JavaScript Document

$(function(){
	
	//登录框验证
	$.validator.addMethod('mobile', function( value, element ){
	//addmethod之后记得去validate中绑定mobile的焦点获得/keyup等事件
   
    return this.optional( element ) || /^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test( value );

}, '* 请输入正确的手机号码');
	$.validator.addMethod('password', function( value, element ){
    return this.optional( element ) || /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,20}$/.test( value );

}, '* 密码必须同时包含数字和字母（8-20位）');
$.validator.addMethod("username", function( value, element ){
			return this.optional( element ) || /^[\u4e00-\u9fa5][\u4e00-\u9fa5][\u4e00-\u9fa5]?[\u4e00-\u9fa5]?$/.test( value );
		}, '*负责人姓名有误');





$.validator.addMethod('code', function( value, element ){

    
    return this.optional( element ) || /^\d{4}$/.test( value );

}, '* 输入正确的4位验证码');
$.validator.addMethod('img_code', function( value, element ){

    
    return this.optional( element ) || /^\d{4}$/.test( value );

}, '* 请输入图片中的4位验证码');
$.validator.addMethod("email", function (value, element) {
	var email = /^[a-z0-9._%-]+@([a-z0-9-]+\.)+[a-z]{2,4}$/;
	return this.optional(element) || (email.test(value));
}, "* 邮箱格式有误！");

	$("#login-form").validate({
	
	
	  submitHandler: function (form) {
		 
		  var user_id = $("#login-phone").val();
		
		  $.ajax({
			  type:"POST",
			  url:"http://localhost/cheyuzhou_new/configure/login_check.php",
			  cache:false,
			  data:{
				  "user_id":user_id,
				  "passwd":$("#login-password").val(),
				  },
			  beforeSend:function(){
					 
				   $(".login-submit").html("登录中...").attr('disabled',"true");
						  
					  },
			  success:function(data) {
				 
				  //登录成功
				  if(data==1) {
					  if(window.location.pathname=="/cheyuzhou_new/404") {
						  window.location.href="index";
					  }
					  else if(window.location.pathname=="/cheyuzhou_new/user_login") {
						   window.location.href="user_order";
					  }
					  else
					  window.location.reload();
				  }
				  //账号不存在的情况
				  else if(data==2) {
					  $("#login-phone").after('<lable class="error register-exist">* 该手机号暂未注册，请先注册！</lable>');
					 
				  }
				  //密码错误
				  else if(data==3) {
					  $("#login-password").after('<lable class="error register-exist">* 密码错误！</lable>');
				  }
				  else
				  	alert("暂时无法连接服务器，请稍后再试");
				  $(".login-submit").html("登录").removeAttr('disabled');
				  
			  },
			  });
	  },
	  
	});
	
	
	//注册框验证
	
 $("#register-form").validate({
	
	  submitHandler: function (form) {
		  var user_id = $("#register-phone").val();
		  var rel_url = window.location.href;
		  $.ajax({
			  type:"POST",
			  url:"configure/register_check.php",
			  cache:false,
			  data:{
				  "user_id":user_id,
				  "passwd":$("#register-password").val(),
				  },
			  beforeSend:function(){
					 
				   $(".register-submit").html("提交中...").attr('disabled',"true");
						  
					  },
			  success:function(data) {
				  alert(data);
				  //账号已存在	
			  		if(data==2) {
						$("#register-phone").after('<lable class="error register-exist">* 该用户名已注册，请直接登录！</lable>');
					
					}
				  //注册成功
					else if(data==1) {
						$("#register-form").html('<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div><p class="register-success">恭喜您~注册成功！马上去<a href="buy_car.php">逛逛</a></p>');
						
						$(".login_register_li").html('<img src="images/header_login.png">&nbsp;&nbsp;<a href="user_center.php?id='+user_id+'">'+user_id+'</a>/<a href="user_logout.php?rel_url='+rel_url+'&id='+user_id+'">注销</a>');
						
					}
  					$(".register-submit").html("注册").removeAttr('disabled');
			  },
			  });
	  },
	});
	//登录框密码处理
	
	//城市选择
			  
	$(".header-city, .header-city-container").hover(function(){
		$(".header-city-container").show();
	  
		},
	function(){
		$(".header-city-container").hide();
		});
	//只要有鼠标操作就移除提示
	$("body").mousedown(function(){
		$(".register-exist").remove();
		});
	$(".navbar-list").hover(function(){
		
		$(this).addClass("navbar-select");
		},function(){
			$(this).removeClass("navbar-select");
			});
		
	 //隐藏新闻部分多余字函数
	function limitwords(classname,maxlength) {
		$(classname).each(function(){
        if($(this).text().length>maxlength){
            $(this).text($(this).text().substring(0,maxlength));
            $(this).html($(this).html()+'...');
        }
		});
		}
	//调用函数限制长度
	//news页面
	//limitwords(".news-content",40);
//	limitwords(".news-content-sp",105);
//	
//
//	limitwords(".news-title-length",20);
//	limitwords(".news-title-con",22);
//	limitwords(".news-list-content>p",190);
//	limitwords(".link-news",30);
	limitwords(".usercenter-location-middle",20);
	
	//登录框表单验证
	//登录注册表单切换
	$(".login-register").click(function(){
		$(".login-form").hide();
		$(".register-form").fadeIn(400);
		});
	$(".register-login").click(function(){
		$(".register-form").hide();
		$(".login-form").fadeIn(400);
		});
	//卖车栏验证
	$("#index-help-sell").validate({
		errorLabelContainer: "#summary",
		errorPlacement: function(error, element) {                             //错误信息位置设置方法  
                             //这里的element是录入数据的对象
 		element.parent().after(error);  
 },
 		submitHandler: function (form) {
			
			var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
			$.ajax({
				  type:"POST",
			  	  url:"configure/sell_car_check.php",
			      cache:false,
				  data:{
					  "brand_name":$("#brand_name").val(),
					  "user_id":$("#appointment_mobile").val(),
				  },
				  beforeSend:function(){
					  $(".appoint-now").html("预约中...").attr('disabled',"true");
					  },
				  success:function(data) {
					  
					  //提交成功
					  if(data==1) {
						  //调用模态对话框,提示成功
						  content+= '<p class="helpbuy-demand-success">您的卖车信息提交成功</p><p class="helpbuy-contact">工作人员会第一时间与您取得联系~</p><p class="helpbuy-around"><a href="buy">您也可以到处逛逛<<</a></p>';
						 
					  }
					 //失败
					  else if(data==0){
						  //调用模态对话框,提示失败
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">提交失败~请稍候重试!</p>';
						  
					  }
					  
					   //若该用户已提交过卖车信息且该信息未被处理，则不能再提交
					  else if(data==2) {
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">处理中，请勿重复提交！</p>';
					  }
					  //限制条件，若同一ip或者同一session提交次数过多，则提示其不能再提交
					  else {
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">请求失败，提交次数过多！</p>';
					  }
					  show_clue(content);
					  $(".appoint-now").html("马上预约").removeAttr('disabled');
					  },
				  error:function(){
					  alert("请求超时！");
				  },
			})
		},
	});
	
	
	
});
//导航栏登录注册框效果
 function show_model(state) {
	$("#model_dialog_clue").hide();
	var bh = $("body").height(); 
	var bw = $("body").width(); 
	$(".fullbg").css({ 
	height:bh, 
	width:bw, 
	display:"block" 
	}); 
	$("#login_out_model").show(); 
	if(state == 0) {
		$(".register-form").hide();
		$(".login-form").fadeIn(400);
	}
	else {
		$(".login-form").hide();
		$(".register-form").fadeIn(400);
	}
	
 }
//模态框提示栏效果
function show_clue(content) {
	var bh = $("body").height(); 
	var bw = $("body").width(); 
	$(".fullbg").css({ 
	height:bh, 
	width:bw, 
	display:"block" 
	});
	$("#model_dialog_clue").html(content);
	$("#model_dialog_clue").show();
}
 function close_dialog() {
	 $(".fullbg, #login_out_model,#model_dialog_clue,#model_dialog_order").hide(); 
 }
  function close_dialog_sp() {
	 $(".fullbg,#model_dialog_clue").hide(); 
 }
 //设置重新发送的时间
var wait=60;
function time(o) {
	
		if (wait == 0) {
			o.removeAttr("disabled");			
			o.html("获取验证码");
			wait = 60;
		} else {
			o.attr("disabled","disabled");
			o.html("重新发送(" + wait + ")");
			wait--;
			setTimeout(function() {
				time(o)
			},
			1000)
		}
	}
 function get_code() {
	
	 $.ajax({
				  type:"POST",
				  url:"test.php",   //接入短信接口
				  cache:false,
				  data:{
					  "phone":$("#register-phone").val(),
					  "password":$("#register-password").val(),
					  
					  },
				  beforeSend:function(){
					  $(".register-phone-code").html("验证码获取中....");
					  $(".register-phone-code").attr("disabled","disabled");
					  if(!/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test($("#register-phone").val())||!/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{6,20}$/.test($("#register-password").val())) {
						  alert("获取验证码失败！输入的手机号或密码不符合规定");
						  $(".register-phone-code").html("获取验证码");
						  $(".register-phone-code").removeAttr("disabled");
						  return false;
					  }
					  
					  },
				  success:function(data){
						 alert("获取成功,验证码已发送至您手机，请查收");
					  	 time($(".register-phone-code"));
					
					   },
				  
				  });
 }
 
//搜索框进行搜索
function car_navbar_search(city) {
	var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
	$.ajax({
			type:"POST",
			url:"configure/car_search.php",
			cache:false,
			
			data:{
				"search_content":$(".header-input").val(),
			},
			beforeSend:function(){
				
				$(".header-btn").html("搜索中...").attr("disabled",true).addClass("header-btn-disabled").removeClass("header-btn");
				},
			success:function(data) {
				if($(".header-input").val()=='') {
					;
					window.location.href="buy?city="+city;
				}
				else {
					if(data==0) {
						content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">未查询到相关车辆，请重新查询！</p>';
						show_clue(content);
						
					}
					else {
						var data_arr = data.split(",");
						if(data_arr.length>1) 
							window.location.href="buy?city="+city+"&brand="+data_arr[0]+"&class="+data_arr[1]+"&searchtitle="+$(".header-input").val();
						else
							window.location.href="buy?city="+city+"&brand="+data_arr[0]+"&searchtitle="+$(".header-input").val();
						
					}
					$(".header-btn-disabled").html("搜索").removeAttr("disabled").removeClass("header-btn-disabled").addClass("header-btn");
					
				}
			}
		});
}