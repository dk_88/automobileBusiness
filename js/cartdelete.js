/*删除购物车中的数据*/
function cart_goods_delete(car_id) {
	
	$.ajax({
			type:"POST",
			url:"cart_goods_delete.php",
			cache:false,
			data:{"car_id":car_id,
			  },
			success:function(data) {
				var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				//成功
				if(data==1) {
					//删除成功，重新加载页面
					 window.location.reload();
				}
				//未登录
				else if(data==2) {
					show_model(0);
				}
				//
				//删除失败
				else if(data==0) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">删除失败！请刷新后重试~</p>';
				    show_clue(content);
					
				}
				//该车子已经不在购物车了
				else if(data==3) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">该二手车早已不在购物车啦~</p>';
				    show_clue(content);
				}
			},
		});
}