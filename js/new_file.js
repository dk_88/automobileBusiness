// JavaScript Document
//(c) Copyright 2015 dpcs. All Rights Reserved. 
var brand_brand = ["安驰,anchi","奥迪,audi","阿斯顿马丁,astonmartin","AC Schnitzer,acs","阿尔法罗密欧,alfa","巴博斯,barbus","宝龙,baolong","保时捷,porsche",
               "别克,bieke","宝马,bmw", "奔驰,benz","北汽制造,beiqi","本田,bentian","北京汽车,bjqc","奔腾,benteng","比亚迪,byd","宝骏,baojun","宾利,binli",
			   "布加迪,bjd","标致,biaozhi","北汽幻速,bqhs","长丰,changfeng","长安,changan","长城,changcheng","昌河,changhe","道奇,daoqi","大宇,dayu","大迪,dadi",
			   "东南,dongnan","大众,dazhong","大通,datong","帝豪,dihao","东风,dongfeng","菲斯克,fsike","福田,futian","菲亚特,fyate","福迪,fudi","丰田,fengtian",
			   "福特,ford","法拉利,ferray","光冈,mitsuoka","GMC,gmc","广汽,guangqi","观致,guanzhi","GUMPERT,gumpert","悍马,hanma","华泰,huatai","黑豹,heibao",
			   "黄海,huanghai","哈飞,hafei","红旗,flag","华翔,huaxiang","华北,huabei","海马,haima","汇众,huizhong","华普,huapu","华阳,huayang","恒天,hengtian",
			   "航天,hangtian","海格,haige","华颂,huasong","金龙,jinlong","金程,jincheng","吉利,geely","金杯,jinbei","江淮,jianghuai","吉奥,jiao","江铃,jiangling",
			   "吉普,jeep","江南,jiangnan","九龙,jiulong","捷豹,jiebao","开瑞,kairui","克莱斯勒,chrysler","柯尼赛格,koenigsegg","卡尔森,kers","凯迪拉克,cadillac",
			   "凯佰赫,kaibaihe","KTM,ktm","卡威,kawei","凯马汽车,kaima","凯翼,kaiyi","康迪,kangdi","雷诺,renault","莲花,lianhua","雷克萨斯,lexus","路特斯,lutesi",
			   "林肯,lincoln","铃木,suzuki","力帆,lifan","路虎,landrove","兰博基尼,lamborghini","劳斯莱斯,rollsroyce","陆风,lufeng","陆地方舟,ludiship","罗孚,luofu",
			   "摩根,mogen","迷你,mini","迈巴赫,maybach","迈凯伦,maikailun","名爵,mingjue","马自达,mazda","玛莎拉蒂,maserati","美亚,meiya","纳智捷,nazhijie","南汽,nanqi",
			   "讴歌,ouge","欧宝,opel","帕加尼,pajiani","起亚,kia","奇瑞,qirui","日产,richan","荣威,rongwei","瑞麒,ruilin","陕汽通家,sqtjia","双环,shuanghuang",
			   "双龙,shuanglong","斯柯达,skoda","Smart,smart","世爵,shijue","三菱,mitsubishi","斯巴鲁,subaru","萨博,saab","天马,tianma","通宝,tongbao",
			   "天津一汽,tjyq","特斯拉,tesla","威麟,weilin","威兹曼,weiziman","沃尔沃,volvo","新凯,xinkai","西雅特,seat","现代,hyundai","雪铁龙,citroen","雪佛兰,chevrolet",
			   "永源,yongyuan","一汽,yiqi","英菲尼迪,infiniti","英伦,englon","野马,yema","英致,yingzhi","中誉,zhongyu","中顺,zhongshun","中兴,zhongxing","中华,zhonghua",
			   "众泰,zhongtai","中欧,zhongou"
			   ];

//"赛宝,庆龄,通田,五十铃,"五菱,"万丰,"依维柯,"新大地, "新龙马,"云豹,"英格尔,"云雀,
var brand_class= { 

                "安驰": ["小公主,anchixgz","雪豹,anchixb","威豹,anchiwb","瑞驰征途,anchirczt","瑞驰K3,anchirck3","瑞驰K5,anchirck5"],
				
				"奥迪": ["奥迪A3,audia3","奥迪A4L,audia4l","奥迪A6L,audia6l","奥迪Q3,audiq3","奥迪Q5,audiq5","奥迪A4,audia4","奥迪A6,audia6","奥迪A1,audia1","奥迪A3(进口),audia3jk","奥迪S3,audis3",
				         "奥迪A4(进口),audisa4jk","奥迪A5,audisa5","奥迪S5,audis5","奥迪A6(进口),audia6jk","奥迪S6,audis6","奥迪A7,audia7","奥迪S7,audis7","奥迪A8,audia8","奥迪S8,audis8","奥迪Q3(进口),audiq3jk",
				         "奥迪Q5(进口),audiq5jk","奥迪SQ5,audisq5","奥迪Q7,audiq7","奥迪TT,auditt","奥迪TTS,auditts","奥迪R8,audir8","奥迪RS5,audirs5"
				        ],
				        
				"阿斯顿马丁":["Rapide,astonmartina","V8 Vantage,kastonmartinb","V12 Vantage,astonmartinc","阿斯顿马丁DB9,astonmartind",
				             "Vanquish,astonmartine","Virage,astonmartinf","阿斯顿马丁DBS,astonmarting"
				             ],
				             
				"阿尔法罗密欧":["ALFA GT,alfaa"],
				
				"巴博斯":["巴博斯S级,barbusa","巴博斯M级,barbusb"],
				
				"宝龙":["霸道,baolonga","天马座,baolongb","菱惠,baolongc","菱麒,baolongd","菱骏,baolonge"],
				
				"保时捷":["Panamera,porschea","Macan,porscheb","卡宴,porschec","Boxster,porsched","Cayman,porschee","保时捷911,porschef"],
				
				"别克":["凯越,biekea","威朗,biekeb","英朗,biekec","君威,bieked","君越,biekee","昂科拉,biekef","昂科威,biekeg","别克GL8,biekeh",
				       "林荫大道,biekei","荣御,biekej","昂科雷,biekek"],
				
				"宝马":["宝马3系,bmwa","宝马5系,bmwb","宝马X1,bmwc","宝马1系,bmwd","宝马2系运动旅行车(进口),bmwjke","宝马3系(进口),bmwjkf","宝马3系GT,bmwg","宝马4系,bmwh",
						"宝马5系(进口),bmwjki","宝马5系GT,bmwj","宝马6系,bmwk","宝马7系,bmwl","宝马X3,bmwm","宝马X4,bmwn","宝马X5,bmwo","宝马X6,bmwp","宝马2系,bmwq",
						"宝马Z4,bmwr","宝马X1(进口),bmwjks","宝马M3,bmwt","宝马M4,bmwu","宝马M5,bmwv","宝马M6,bmww","宝马X5 M,bmwx","宝马X6 M,bmwy",
						"宝马1系M,bmwz"
						],
						
				"奔驰":["奔驰C级,benza","奔驰E级,benzb","奔驰GLA级,benzc","奔驰GLK级,benzd","奔驰威霆,benze","唯雅诺,benzf",
						"奔驰凌特,benzg","奔驰A级,benzh","奔驰B级,benzi","奔驰CLA级,benzj","奔驰C级(进口),benzk","奔驰E级(进口),benzl",
						"奔驰CLS级,benzm","奔驰S级,benzn","奔驰G级,benzp","奔驰GL级,benzq","奔驰R级,benzr",
						"奔驰SLK级,benzs","奔驰SL级,benzt","奔驰GLA级(进口),benzu","奔驰M级,benzv","Sprinter,benzw","奔驰威霆(进口),benzo",
						"奔驰GLK级(进口),benzx","唯雅诺(进口),benzy","奔驰CLK级,benzz","奔驰A级AMG,benzab","奔驰CLA级AMG,benzac","奔驰C级AMG,benzad","奔驰CLS级AMG,benz1ae",
						"奔驰S级AMG,benz1af","奔驰M级AMG,benz1ah","奔驰G级AMG,benz1ai","奔驰GL级AMG,benz1aj","奔驰SL级AMG,benz1ak","奔驰SLS级AMG,benz1am","奔驰E级AMG,benz1an",
						"奔驰SLK级AMG,benz1ao","迈巴赫S级,benz1ap"
						],
				
              	"北汽制造":["旋风,beiqia","骑士,beiqib","陆霸,beiqic","战旗,beiqid","雷驰,beiqie","陆铃,beiqif","御虎,beiqig",
              			  "京城海狮,beiqih","勇士,beiqii","域胜007,beiqij","北京212,beiqik","角斗士,beiqil","越铃,beiqim",
              			  "锐铃,beiqin","北京BW007,beiqio"],
              			  
              	"本田":["思域,bentiana","思迪,bentianb","CR-V,bentianc","奥德赛,bentiand","飞度,bentiane","雅阁,bentianf","锋范,bentiang",
					   "Element,bentiank","Insight,bentianm","里程,bentiann","时韵,bentiano",
						"思铂睿,bentianp","歌诗图,bentianr","CR-Z,bentians","理念,bentiant","思铭,bentianw","艾力绅,bentianx",
						"凌派,bentiany","杰德,bentianz","缤智,bentian1aa","XR-V,bentian1ab","哥瑞,bentian1ac"
						],
						
				"北京汽车":["E系列,bjqca","威旺306,bjqcb","威旺205,bjqcc","威旺M20,bjqcd","威旺206,bjqce","威旺307,bjqcf","威旺T205-D,bjqcg",
						"威旺007,bjqch","威旺M30,bjqci","北京40,bjqcj","E150EV,bjqck","EV200,bjqcl",
					   "ES210,bjqcm","EV160,bjqcn","绅宝D70,bjqco","绅宝D50,bjqcp","绅宝D60,bjqcq","绅宝D20,bjqcr","绅宝X65,bjqcs",
					   "绅宝CC,bjqct","绅宝D80,bjqcu"
						],
						
				"奔腾":["B70,bentenga","B50,bentengb","B90,bentengc","X80,bentengd","奔腾B30,bentenge"],
				
				"比亚迪":["比亚迪S8,byda","比亚迪F3R,bydb","福莱尔,bydc","比亚迪F6,bydd","比亚迪F3,byde","比亚迪F0,bydf","比亚迪F3DM,bydg","比亚迪G3,bydh","比亚迪M6,bydi",
						"比亚迪L3,bydj","比亚迪G3R,bydk","比亚迪S6,bydl","比亚迪G6,bydm","比亚迪E6,byd1n","速锐,bydo","思锐,bydp","秦,bydq","腾势,bydr",
						"比亚迪G5,byds","比亚迪S7,bydt","唐,bydu","宋,bydv"
						],
				
				"宝骏":["宝骏630,baojuna","宝骏乐驰,baojunb","宝骏610,baojunc","宝骏730,baojund","宝骏560,baojune"],
				
				"宾利":["欧陆,binlia","雅致,binlib","慕尚,binlic","雅骏,binlid","飞驰,binlie","添越,binlif"],
				
				"布加迪":["威航,bjda"],
				
				"标致":["标致301,biaozhia","标致308,biaozhib","标致308S,biaozhic","标致408,biaozhid","标致508,biaozhie","标致3008,biaozhif","标致2008,biaozhig",
						"标致206,biaozhih","标致207,biaozhii","标致307,biaozhij","标致RCZ,biaozhik","标致308(进口),biaozhijkl","标致3008(进口),biaozhijkm",
						"标致206(进口),biaozhijkn","标致207(进口),biaozhijko","标致307(进口),biaozhijkp","307旅行车,biaozhiq","标致407,biaozhir","407旅行车,biaozhis",
						"标致607,biaozhit","307Cross,biaozhiu","207Cross,biaozhiv"
						],
						
				"北汽幻速":["幻速S2,bqhsa","幻速S3,bqhsb","幻速H2,bqhsc","幻速H3,bqhsd"],
				
				"长丰":["猎豹CS6,changfenga","骐菱,changfengb","黑金刚,changfengc","飞腾,changfengd","长丰奇兵,changfenge",
						"DUV,changfengf","飞铃,changfengg","猎豹CS7,changfengh","飞扬,changfengi",
						"猎豹CT5,changfengk","飞腾C5,changfengl","猎豹Q6,changfengm","猎豹CS10,changfengn"
						],
						
				"长安":["CM8,changana","CS35,changanb","CS75,changanc","CX20,changand","CX30,changane","奔奔,changanf","尊行,changang",
						"志翔,changanh","悦翔,changani","星光,changanj","星光小卡,changank","星韵,changanl","杰勋,changanm","欧力威,changann",
						"欧诺,changano","长安欧雅,changanp","睿行,changanq","睿骋,changanr","绿色新星,changans","致尚XT,changant","运动星,changanu",
						"运通,changanv","逸动,changanw","都市彩虹,changanx","金牛星,changany",
						"长安之星,changanz","跨越新豹,changanaa1","星卡,changanab1","镭蒙,changanac1","雪虎,changanad1","神琪,changanae1"
						],
				
				"长城":["长城M2,changchenga","长城C30,changchengb","长城C50,changchengc","长城M4,changchengd","风骏5,changchenge",
						"风骏3,changchengf","长城精灵,changchengg","酷熊,changchengh","炫丽,changchengi","长城M1,changchengj",
						"赛弗,changchengk","长城V80,changchengl","嘉誉,changchengm"
						],
						
				"昌河":["海豚,changhea","海象,changheb","福瑞达,changhec","爱迪尔,changhed","骏马,changhee","福运,changhef",
						"福瑞达M50,changheg"
						],
				
				"道奇":["凯领,daoqia","锋哲,daoqib","酷搏,daoqic","蝰蛇,daoqid","酷威,daoqie","Challenger,daoqif",
					   "翼龙,daoqig","公羊,daoqih","Ram,daoqii"
					  ],
					  
				"大宇":["典雅,dayua","旅行家,dayub","马蒂兹,dayuc"],
				
				"大迪":["都市威菱,dadia","都市骏马,dadib","大迪霸道,dadic","顺驰,dadid"],
				
				"东南":["菱利,dongnana","富利卡,dongnanb","菱帅,dongnanc","得利卡,dongnand","菱动,dongnane",
					   "菱悦,dongnanf","希旺,dongnang","菱致,dongnanh","菱仕,dongnani","东南DX7,dongnanj"
					   ],
				
				"大众":["POLO,dazhonga","桑塔纳·尚纳,dazhongb","朗行,dazhongc","朗逸,dazhongd","朗境,dazhonge","凌渡,dazhongf",
						"帕萨特,dazhongg","途观,dazhongh","途安,dazhongi","Passat领驭,dazhongj","桑塔纳经典,dazhongk",
						"桑塔纳志俊,dazhongm","捷达,dazhongn","宝来,dazhongo","高尔夫,dazhongp","速腾,dazhongq","迈腾,dazhongr",
						"一汽-大众CC,dazhongs","开迪,dazhongt","甲壳虫,dazhongu",
						"高尔夫(进口),dazhongv","辉腾,dazhongw","迈腾(进口),dazhongx","途锐,dazhongy",
						"夏朗,dazhongz","迈特威,dazhongaa1","凯路威,dazhongab1","尚酷,dazhongac1",
						"大众CC,dazhongad1","大众Eos,dazhongae1","桑塔纳3000,dazhongaf1","UP!,dazhongag1"
						],

				"大通":["V80,datonga","G10,datongb"],
				
				"帝豪":["EC7,dihaoa","EC8,dihaob"],
				
				"东风":["小康K07,dongfenga","小康K07-ii,dongfengb","小康K17,dongfengc","小康K06,dongfengd","小康V27,dongfenge",
						"小康V29,dongfengf","小康C37,dongfengg","小康V26,dongfengh","小康V07S,dongfengi","小康风光,dongfengj",
						"小康C35,dongfengk","小康K02,dongfengl","小康K05,dongfengm","小康K01,dongfengn","小康V21,dongfengo",
						"小康V22,dongfengp","小康C36,dongfengq","小康C32,dongfengr","小康K07S,dongfengs",
						"景逸,dongfengt","风行,dongfengu","小王子,dongfengv","风神水星,dongfengw","菱智,dongfengx","菱越,dongfengy",
						"菱通,dongfengz","风神S30,dongfengaa1","风神H30,dongfengab1","EQ7240BP,dongfengac1","猛士,dongfengad1",
						"风神H30 Cross,dongfengae1","景逸Cross,dongfengaf1","途逸,dongfengag1","风神A60,dongfengah1","景逸SUV,dongfengai1",
						"俊风CV03,dongfengaj","御风,dongfengak","景逸X5,dongfengal","虎视,dongfengam","信天游,dongfengan1",
						"景逸X3,dongfengap1","风行CM7,dongfengaq1","景逸S50,dongfengar1","风神A30,dongfengas1","风神AX7,dongfengat1","风神L60,dongfengau1",
						"景逸XV,dongfengav1","风度MX6,dongfengaw1","风光360,dongfengax1","风光330,dongfengay1"
						],
				
				"菲斯克":["Karma,fsikea"],
				
				"福田":["风景G7,futiana","福田风景,futianb","蒙派克E,futianc","蒙派克S,futiand","迷迪,futiane"],
				
				"菲亚特":["菲翔,fyatea","致悦,fyateb","派朗,fyatec","派力奥,fyated","西耶那,fyatee","周末风,fyatef","菲跃,fyateg","博悦,fyateh"],
				
				"福迪":["小超人,fudia","探索者,fudib","揽福,fudic","雄狮,fudid","飞越,fudie"],   
				
				"丰田":["致炫,fengtiana","雷凌,fengtianb","凯美瑞,fengtianc","汉兰达,fengtiand","逸致,fengtiane","雅力士,fengtianf",
						"威驰,fengtiang","花冠,fengtianh",
						"卡罗拉,fengtiani","普锐斯,fengtianj","锐志,fengtiank","皇冠,fengtianl","一汽丰田RAV4,fengtianm","兰德酷路泽,fengtiann",
						"普拉多,fengtiano","柯斯达,fengtianp","FJ 酷路泽,fengtianr",
						"威飒,fengtiant","红杉,fengtianv","埃尔法,fengtianw","普瑞维亚,fengtianx",
						"丰田86,fengtianz","杰路驰,fengtianaa1","坦途,fengtianac1","普拉多(进口),fengtianad1",
						"丰田RAV4(进口),fengtianae1","汉兰达(进口),fengtianaf1","丰田佳美,fengtianag1","Wish,fengtians","奔跑者,fengtianq"
						],

				"福特":["S-MAX,forda","嘉年华,fordb","福克斯,fordc","福睿斯,fordd","翼搏,forde","翼虎,fordf","蒙迪欧,fordg","锐界,fordh",
						"全顺,fordi","嘉年华(进口),fordk","房车E系列,fordl","探险者,fordm","爱虎,fordn","猛禽F系,fordo",
						"福克斯(进口),fordp","翼虎(进口),fordr","蒙迪欧(进口),fords","野马,fordt","锐界,fordu","金牛座,,fordv"
						],
				

				
				"法拉利":["488,ferraya","575,ferrayb","599,ferrayc","612,ferrayd","360,ferraye","F430,ferrayf","F149,ferrayg",
						 "458,ferrayh","FF,ferrayii","F12,ferrayj","LaFerrari,ferrayk"
						],
						
				"光冈":["大蛇,mitsuokaa","女王,mitsuokab","嘉路,mitsuokac"
						],
			
				"GMC":["阿卡迪亚,gmca","使节,gmcb","Savana,gmcc","育空河,gmcd","西拉,gmce","Terrain,gmcf"],
				
				"广汽":["传祺GA5,guangqia","传祺GS5,guangqib","传祺GA3,guangqic","传祺GA3S视界,guangqid",
						"传祺GA6,guangqie","传祺GS4,guangqif"],
				
				"观致":["观致3,guanzhia","观致3都市SUV,guanzhib"],
				
				
				"GUMPERT":["阿波罗,gumperta"],
				
				"悍马":["悍马H2,hanmaa","悍马H3,hanmaab"],
				
				"华泰":["圣达菲,huataia","特拉卡,huataib","吉田,huataic","B11,huataid","宝利格,huataie","路盛E70,huataif"],
				
				"黑豹":["朗杰,heibaoa","多功能车,heibaob"],
				
				
			   "黄海":["傲羚,huanghaia","傲骏,huanghaib","傲龙CUV,huanghaic","大柴神,huanghaid","小柴神,huanghai2d","挑战者,huanghaie",
			   		"旗胜F1,huanghaif","旗胜V3,huanghaig","翱龙CUV,huanghaih","领航者CUV,huanghaii","黄海N1,huanghaij","黄海N2,huanghaik"
			   		],

			   
			   "哈飞":["中意,hafeia","松花江,hafeib","民意,hafeic","赛豹,hafeie","赛马,hafeif",
			          "路宝,hafeig","路尊大霸王,hafeibh","路尊小霸王,hafeibi","骏意,hafeij"
			          ],
			   
			   "一汽红旗":["H7,flaga","HQ3,flagb","L5,flagc","世纪星,flagd","旗舰,flage","明仕,flagf","盛世,flagg","红旗,flagh"
			   		 ],
			   
			   "华翔":["驭虎,huaxianga","富奇,huaxiangb"],
			   
			   "华北":["腾狮RV,huabeia","骏霸,huabeib","超赛,huabeic","醒狮,huabeid"],
			   
			   "海马":["323,haimaa","海马M3,haimab","海马M6,haimac","海马M8,haimad","海马S5,haimae","海马S7,haimaf","丘比特,haimag","普力马,haimah",
			   		  "欢动,haimai","海福星,haimaj","海马3,haimak","福美来,haimal","福美来M5,haimam","海马骑士,haimao",
			   		  "爱尚,haimap","王子,haimaq","福仕达,haimar"
			   		],
       
			   "汇众":["伊斯坦纳,huizhonga","德驰,huizhongb"],
			   
			   "华普":["M203,huapua","朗风,huapub","杰士达美鹿,huapuc","海域,huapud","海尚,huapue","海悦,huapuf","海景,huapug","海炫,huapuh",
			   		   "海迅,huapui","海锋,huapuj","飓风,huapuk","飙风,huapul"
			   		   ],
			   
			              
			   "华阳":["五菱,huayanga"],
			   
			   "恒天":["途腾T1,hengtiana","途腾T2,hengtianb","途腾T3,hengtianc"],
			   

		   	   "航天":["成功一号,hangtiana"],
		   		
		   		"海格":["H5C,haigea","H6C,haigeb","H6V,haigec","御骏,haiged","龙威,haigee"],
		   		
		   		"华颂":["华颂7,huasonga"],
		   		
		   		"金龙":["金威,jinlonga","金龙海狮,jinlongb"],
		   		  
		   		"金程":["先锋,jinchenga","横行,jinchengb","赛风,jinchengc","金程之星,jinchengd","金程海狮,jinchenge","领跑,jinchengf"],
		   		     
		   		"吉利":["中国龙,geelya","优利欧,geelyb","博瑞,geelyc","吉利SC3,geelyd","吉利帝豪,geelye","吉利海景,geelyf","美人豹,geelyg",
		   				"美日,geelyh","自由舰,geelyi","豪情SUV,geelyj","豹风GT,geelyk","远景,geelyl","金鹰,geelym","金刚,geelyn",
		   				"豪情,geelyo","雳靓,geelyp","GC7,geelyq","GX2,geelyr","GX7,geelys","熊猫,geelyt"
		   				],
		   		

		   		"金杯":["大力神,jinbeia","智尚S30,jinbeib","小海狮X30,jinbeic","海星,jinbeid","金典,jinbeie","金杯750,jinbeif","金杯S50,jinbeig",
		   				"金杯T30,jinbeih","金杯T32,jinbeii","金杯海狮,jinbeij","金杯霸道,jinbeik","锐驰,jinbeil","阁瑞斯,jinbeim",
		   				"雷龙,jinbein"
		   				],
		   		
		   		             
		   		"江淮":["iEV5,jianghuaia","凌铃,jianghuaib","同悦,jianghuaic","和悦,jianghuaid","和悦A13Cross,jianghuaie",
		   				"和悦A13,jianghuaie2","和悦A30,jianghuaif",
		   				"宾悦,jianghuaig","帅铃T6,jianghuaih","悦悦,jianghuaii","星锐,jianghuaij","瑞铃,jianghuaik","瑞风,jianghuail",
		   				"瑞风M3,jianghuaim","瑞风M5,jianghuain","瑞风S3,jianghuaio","瑞风S5,jianghuaip","瑞鹰,jianghuaiq"
		   				],
		   		
		   		                
		   		
		   		"吉奥":["E美,jiaoa","GP150,jiaob","GS50,jiaoc","GX6,jiaod","伊美,jiaoe","吉奥凯旋,jiaof","凯睿,jiaog","奥腾,jiaoh",
		   				"奥轩G3,jiaoi","奥轩G5,jiaoj","奥轩GX5,jiaok","帅凌,jiaol","帅威,jiaom","帅舰,jiaon",
		   				"帅豹,jiaoo","帅驰,jiaop","星旺,jiaoq","星朗,jiaor","猛将旅,jiaos","财运,jiaot"
		   				],
		   		                   
		   		"江铃":["域虎,jianglinga","宝典,jianglingb","宝威,jianglingc","江铃轻卡,jianglingd","运霸,jianglinge","驭胜,jianglingf","骐铃,jianglingg","骐铃T7,jianglingh"
		   				],
		   		      
		   		
			   "吉普":["JEEP2500,jeepa","JEEP2700,jeepb","大切诺基(进口),jeepjkc","城市猎人,jeepd","大切诺基,jeepe","切诺基(进口),jeepjkf","指南者,jeepg",
			   			"指挥官,jeeph","牧马人,jeepi","自由人,jeepj","自由光,jeepk","自由客,jeepl"	
			   		 ],
    
			   "江南":["江南TT,jiangnana","传奇,jiangnanb","奥拓,jiangnanc","精灵,jiangnand","风光,jiangnane"],
			       
			   "九龙":["天马商务车,jiulonga","艾菲,jiulongb"],
			    
			   "捷豹":["F-Type,jiebaoa","S-Type,jiebaob","X-Type,jiebaoc","XE,jiebaod","XF,jiebaoe","XJ,jiebaof","XK,jiebaog"],

			   			   
			   "开瑞":["K50,kairuia","优优,kairuib","优劲,kairuic","优派,kairuid","优翼,kairuie","优胜,kairuif","优雅,kairuig",
			   		   "杰虎,kairuih","绿卡,kairuii"
			   		],

			   "克莱斯勒":["300C,chryslera","铂锐,chryslerb","大捷龙,chryslerc","300C(进口),chryslerjkd","300S,chryslere","PT漫步者,chryslerf",
  		   			      "交叉火力,chryslerg","大捷龙(进口),chryslerjkh","彩虹,chrysleri","赛百灵,chryslerj"
			   			],

			   "柯尼赛格":["Agera,koenigsegga","CCR,koenigseggb","CCX,koenigseggc","CCXR,koenigseggd"],
 
			   "卡尔森":["GL级,kersa","S级,kersb"],

			   "凯迪拉克":["ATS-L,cadillaca","XTS,cadillacb","CTS,cadillacc","赛威SLS,cadillacd","ATS,cadillace","CTS(进口),cadillajkcf",
			   			  "SRX,cadillacg","XLR,cadillach","凯雷德,cadillaci"
			   			 ], 
			   

			   "凯佰赫":["战盾,kaibaihea"],
			   
			   "KTM":["X-BOW,ktma"],
			   
			   "卡威":["K1,kaweia","W1,kaweib"],

			   "凯马汽车":["凯马皮卡,kaimaa"],
			   
			   "凯翼":["C3,kaiyia","C3R,kaiyib"],
			    
			   "康迪":["康迪小电跑,kangdia","康迪熊猫,kangdib"],
			    
			   "雷诺":["卡缤,renaulta","塔利斯曼,renaultb","威赛帝,renaultc","拉古那,renaultd","拉古那Couple,renaulte",
        			   "梅甘娜,renaultf","梅甘娜,renaulth","梅甘娜CC,renaulti","梅甘娜Couple,renaultj",
					  "科雷傲,renaultk","纬度,renaultl","风景,renaultm","风朗,renaultn"
			   		 ],
			           
  
			   "莲花":["L3两厢,lianhuaa","L3三厢,lianhuab","L5两厢,lianhuac","L5三厢,lianhuad"
			   			],
			   
			   
			   "雷克萨斯":["雷克萨斯CT,lexusa","雷克萨斯ES,lexusb","雷克萨斯GS,lexusc","雷克萨斯GX,lexusd","雷克萨斯IS,lexuse","雷克萨斯LFA,lexusf",
			   				"雷克萨斯LS,lexusg","雷克萨斯LX,lexush","雷克萨斯NX,lexusi","雷克萨斯RC,lexusj",
			   			   "雷克萨斯RX,lexusk","雷克萨斯SC,lexusl"
			   			   ],
			   
			              
			   "路特斯":["Elise,lutesia","Evora,lutesib","Exige,lutesic","竞悦,lutesid","竞速,lutesie"
			   			],
			   
			       
			   "林肯":["Blackwood,lincolna","LS,lincolnb","MKC,lincolnc","MKS,lincolnd","MKT,lincolne","MKX,lincolnf",
			   		   "MKZ,lincolng","城市,lincolnh","领航员,lincolni"
			   			],
			           
			   "铃木":["启悦,suzukia","天语SX4两厢,suzukib","天语SX4三厢,suzukic","铃木奥拓,suzukid","尚悦,suzukie","羚羊,suzukif","锋驭,suzukig",
			   			"雨燕,suzukih","利亚纳两厢,suzukii","利亚纳三厢,suzukij","利亚纳A6两厢,suzukik","利亚纳A6三厢,suzukil","昌铃王,suzukim",
			   			"浪迪,suzukin","派喜,suzukio","凯泽西,suzukip","吉姆尼,suzukiq","超级维特拉,suzukir","速翼特,suzukis","北斗星,suzukit"
			   			],
			   
         
			    "力帆":["320,lifana","330,lifanb","520两厢,lifanc","520三厢,lifand","530,lifane","620,lifanf","630,lifang","720,lifanh","T21,lifani",
			   		   "X50,lifanj","X60,lifank","丰顺,lifanl","乐途,lifanm","兴顺,lifann","力帆820,lifano","福顺,lifanp"	
			   		   ],
			                 
			   "路虎":["卫士,landrovea","发现,landroveb","发现神行,landrovec","揽胜,landroved","极光,landrovee","神行者,landrovef",
			   		   "揽胜极光,landroveg","揽胜运动版,landroveh"
			   		  ],
			         
			   "兰博基尼":["Aventador,lamborghinia","Huracan,lamborghinib","Reventon,lamborghinic","盖拉多,lamborghinid","蝙蝠,lamborghinie"
			   			],
			       
			   "劳斯莱斯":["古思特,rollsroycea","幻影,rollsroyceb","银刺,rollsroycec","银影,rollsroyced","银灵,rollsroycee","魅影,rollsroycef"
			   			],

			   "陆风":["X6,lufenga","X8,lufengb","X9,lufengc","新饰界,lufengd","X7,lufenge","风华,lufengf","风尚,lufengg","陆风X5,lufengh",	
			   		 ],
			          
			   "陆地方舟":["陆地方舟V5,ludishipa","艾威,ludishipb","陆地方舟陆风,ludishipc"],
 
			   "罗孚":["罗孚75,luofua"],
			   
			   "摩根":["4/4,mogena","Aero Coupe,mogenb","Aero SuperSports,mogenc","Plus 4,mogend","Plus 8,mogene","摩根Roadster,mogenf","三轮车,mogenh"
			   			],
			   
			   "迷你":["Clubman,minia","Roadster,minib","Countryman,minic","Paceman,minid","Cooper,minie","Couple,minif","One,minig","敞篷版,minih"
			   			],
    
			   "迈巴赫":["57,maybacha","62,maybachb"],
			   
			   "迈凯伦":["540C,maikailuna","650S,maikailunb","MP4-12C,maikailunc","P1,maikailund"],
			      
			   "名爵":["MG 3SW,mingjuea","MG 7,mingjueb","MG TF,mingjuec","TF,mingjued","MG 3,mingjuee","MG 5,mingjuef","MG 6 两厢,mingjueg",
			   			"MG 6 三厢,mingjueh","锐行GT,mingjuei","锐腾,mingjuej","名爵3系SW,mingjuek"
			   			],

			   
			   "马自达":["马自达2,mazdaa","马自达3 Axela昂克赛拉,mazdab","马自达3星骋,mazdac","马自达CX-5,mazdad","马自达2劲翔,mazdae","马自达3,mazdaf",
			   			"阿特兹,mazdag","马自达6,mazdah","睿翼,mazdai","马自达CX-7,mazdaj","马自达8,mazdak","马自达CX-7(进口),mazdal","马自达CX-9,mazdam",
			   			"马自达5,mazdan","马自达MX-5,mazdao","马自达3(进口),mazdap",
						"ATENZA,mazdaq","马自达RX-8,mazdar"
						],
			   
			   "玛莎拉蒂":["3200GT,maseratia","Coupe,maseratib","Ghibli,maseratic","GranCabrio,maseratid","GranSport,maseratie","GranTurismo,maseratif",
			   				"总裁,maseratig"
			   				],
			         
			   "美亚":["奇兵,meiyaa","美亚奇骏,meiyab","海狮,meiyac","陆程,meiyad"],
			       
			   "纳智捷":["5 Sedan,nazhijiea","CEO,nazhijieb","优6,nazhijiec","大7 MPV,nazhijied","大7 SUV,nazhijiee"
			   			],
			        
			   "南汽":["优尼柯,nanqia","君达,nanqib","新雅途,nanqic"],  
		   	   
		   	   "讴歌":["ILX,ougea","MDX,ougeb","RDX,ougec","RL,ouged","RLX,ougee","TL,ougef","TLX,ougeg","ZDX,ougeh"
		   	   			],
		   	          
		   	   "欧宝":["威达两厢,opela","威达三厢,opelb","安德拉,opelc","欧捷利,opeld","欧美佳两厢,opele","欧美佳三厢,opelf",
		   	   			"英速亚两厢,opelg","英速亚三厢,opelh","英速亚旅行版,opeli","赛飞利,opelj",
		   	   			"雅特两厢,opelm","雅特三厢,opeln","雅特敞篷车,opelo","麦瑞纳,opelp"
		   	   		 ],
			   	               
	   	   "帕加尼":["Zonda,pajiania"],
		   	   
		   	  
		   		"起亚":["起亚K2,kiaa","秀尔,kiab","赛拉图,kiac","福瑞迪,kiad","起亚K3,kiae","起亚K4,kiaf","起亚K5,kiag","狮跑,kiah","智跑,kiai",
		   				"千里马,kiaj","锐欧,kiak","远舰,kial","嘉华,kiam","凯尊,kian","索兰托,kiao","霸锐,kiap","佳乐,kiaq","起亚VQ,kiar",
		   				"速迈,kias","欧菲莱斯,kiat"
		   				],
		   		
		   		"奇瑞":["奇瑞A1,qiruia","奇瑞A3,qiruib","奇瑞A5,qiruic","奇瑞E3,qiruid","奇瑞E5,qiruie","奇瑞eQ,qiruif","奇瑞QQ3,qiruig","奇瑞QQ6,qiruih","奇瑞QQme,qiruii",
		   				"奇瑞V5,qiruij","东方之子,qiruik","旗云,qiruil","旗云1,qiruim","旗云2,qiruin","旗云3,qiruio","旗云5,qiruip","爱卡,qiruiq",
		   				"瑞虎,qiruir","瑞虎3,qiruis","瑞虎5,qiruit","艾瑞泽3,qiruiu","艾瑞泽7,qiruiv","艾瑞泽M7,qiruiw","风云,qiruix",
		   				"风云2,qiruiy"
		   				],
		   		
		   		                        
		   		
		   		"日产":["启辰D50,richana","启辰R30,richanb","启辰R50,richanc","启辰R50X,richand","启辰T70,richane","启辰晨风,richanf","天籁,richang",
		   				"东风奇骏,richanh","楼兰,richani","玛驰,richanj","蓝鸟,richank","轩逸,richanl","逍客,richanm","阳光,richann",
		   				"颐达,richano","骊威,richanp","骏逸,richanq","骐达,richanr","NV200,richans","俊风,richant","多功能商用车,richanu",
		   				"奥丁,richanv","帅客,richanw","帕拉丁,richanx","帕拉骐,richany","御轩,richanz","风度MX6,richan1aa",
						"350Z,richan1ab","370Z,richan1ac","GT-R,richan1ad","Juke,richan1ae","Murano,richan1af","奇骏(进口),richan1ah",
		   				"奥蒂玛,richan1ai","桂冠,richan1aj","西玛,richan1ak","贵士,richan1al","途乐,richan1am",
		   				"风雅,richan1ap","马克西马,richan1aq","骐达(进口),richan1ar"
		   				],
                 

		   		"荣威":["荣威350,rongweia","荣威550,rongweib","荣威750,rongweic","荣威950,rongweid","荣威E50,rongweie","荣威W5,rongweif"
		   				],
		   		     
		   		"瑞麒":["瑞麒G3,ruilina","瑞麒G5,ruilinb","瑞麒G6,ruilinc","瑞麒M1,ruilind","瑞麒M5,ruiline","瑞麒X1,ruilinf","派拉蒙,ruiling"
		   				],
		   		      
		   		"陕汽通家":["福家,sqtjiaa","通家和瑞,sqtjiab"],
		   		 
		   		"双环":["双环SCEO,shuanghuanga","来宝S-RV,shuanghuangb","来旺,shuanghuangc","小贵族,shuanghuangd"],

		   		
		   		"双龙":["主席,shuanglonga","享御,shuanglongb","柯兰多,shuanglongc","爱腾,shuanglongd","路帝,shuanglonge","雷斯特,shuanglongf"
		   				],

		   		"斯柯达":["明锐,skodaa","晶锐,skodab","欧雅,skodac","速派,skodad","昊锐,skodae","Yeti,skodaf","明锐RS,skodag",
		   				  "法比亚三厢,skodah","晶锐Scout,skodai","昊锐旅行车,skodaj","昕锐,skodak","速派(进口),skodal","Yeti(进口),skodam",
		   				  "速尊旅行版,skodan","昕动,skodao","明锐旅行版,skodap"
		   				  ],

		   		
		   		"Smart":["ForTwo,smarta"],
		   
		 		"世爵":["C12,shijuea","C8,shijueb","D12,shijuec","D8,shijued"],
		 		   
		 		"三菱":["欧蓝德(进口),mitsubishib","帕杰罗(进口),mitsubishic","君阁,mitsubishid","戈蓝,mitsubishie",
		 				"翼神,mitsubishif","菱绅,mitsubishig","蓝瑟,mitsubishih","风迪思,mitsubishii","ASX劲炫,mitsubishij",
		 				"欧蓝德,mitsubishik","蓝瑟(进口),mitsubishil","帕杰罗,mitsubishim","蓝瑟翼豪陆神,mitsubishin",
		 				"劲炫,mitsubishio","帕杰罗劲畅,mitsubiship"
		 				],
		 				 		
		 		"斯巴鲁":["BRZ,subarua","XV,subarub","傲虎,subaruc","力狮,subarud","森林人,subarue","翼豹,subaruf","驰鹏,subarug"],
		 		      
		 		"萨博":["9月3日,saaba","9月5日,saabb"],
		 		    
		 		"天马":["英雄,tianmaa","风锐,tianmab","骏驰,tianmac"],
		 		
		 		"通宝":["宗申通宝,tongbaoa"],
			   
			   "天津一汽":["夏利,tjyqa","夏利A+,tjyqb","夏利N3,tjyqc","夏利N5,tjyqd","夏利N7,tjyqe","威乐,tjyqf","威姿,tjyqg","威志,tjyqh",
			   				"威志V2,tjyqi","威志V5,tjyqj","骏派D60,tjyqk"
			   				],
			              
			   
			   "特斯拉":["MODEL S,teslaa"],
			   
			   "威麟":["威麟H3,weilina","威麟H5,weilinb","威麟V5,weilinc","威麟X5,weilind"],
			      
			   "威兹曼":["威兹曼GT,weizimana","威兹曼敞篷车,weizimanb"],
			    			   
			   "沃尔沃":["S40,volvoa","S80L,volvob","C30,volvof","C70,volvog","S40(进口),volvoh",
						"S60,volvoi","S80,volvok","V40,volvom","V60,volvon","XC60(进口),volvoo","XC90,volvop",
						"S60L,volvoq","XC Classic,volvor","XC60,volvos"
					   ],

			   "新凯":["凯胜,xinkaia","新凯之星,xinkaib","新凯靓星,xinkaic","都市之星,xinkaid"],
			   
			      
		   	   "西雅特":["伊比飒,seata","利昂,seatb","欧悦博,seatc"],
		   		
		   		  
		   		"现代":["Trajet,hyundaia","劳恩斯,hyundaic","劳恩斯酷派,hyundaid",
		   				"捷恩斯,hyundaig","格锐,hyundaih","维拉克斯,hyundail",
		   				"美佳,hyundaim","新胜达,hyundain","辉翼,hyundaio","酷派,hyundaiq","雅尊,hyundair",
		   				"雅科仕,hyundais","飞思,hyundait","i30,hyundaiu","ix25,hyundaiv","ix35,hyundaiw","伊兰特,hyundaix",
		   				"名图,hyundaiy","名驭,hyundaiz","御翔,hyundaiaa1","悦动,hyundaiab1","朗动,hyundaiac1","瑞奕,hyundaiad1",
		   				"瑞纳,hyundaiae1","索纳塔,hyundaiaf1","胜达,hyundaiag1","途胜,hyundaiah1","雅绅特,hyundaiai1","领翔,hyundaiaj1"
		   				],
		   		
		   		"雪铁龙":["C2,citroena","C3-XR,citroenb","C4L,citroenc","C5,citroend","世嘉,citroene","凯旋,citroenf","富康,citroeng","毕加索,citroenv",
		   				  "爱丽舍,citroenh","赛纳,citroeni","C4,citroenj","C4 Aircross,citroenk","C4毕加索,citroenl","C5(进口),citroen","C6,citroenm",
		   				  "DS3,citroenn","DS4,citroeno","DS5,citroenp","DS 5LS,citroens","DS5(进口),citroent","DS6,citroenu","雪铁龙C4世嘉,citroenv"
		   				  ],
		   		
		   		"雪佛兰":["乐风,chevroleta","乐骋,chevroletb","创酷,chevroletc","景程,chevroletd","爱唯欧,chevrolete","科帕奇,chevroletf",
						 "科鲁兹,chevroletg","赛欧,chevroleth","迈锐宝,chevroleti","乐驰,chevroletj",
						 "克尔维特,chevroletm","斯帕可,chevroletn","沃蓝达,chevroleto","特使,chevroletp","科帕奇,chevroletq","科迈罗,chevroletr",
						 "蒙特卡洛,chevrolets","豪放,chevrolett"
						 ],

		   		"永源":["永源五星,yongyuana","猎鹰,yongyuanb","飞碟UFO,yongyuanc"],
		   		  
			   "一汽":["特锐,yiqia","自由风,yiqib","森雅M80,yiqic","森雅S80,yiqid","欧朗,yiqie"],
			   
			   
			   "英菲尼迪":["英菲尼迪ESQ,infinitia","英菲尼迪EX,infinitib","英菲尼迪FX,infinitic","英菲尼迪G,infinitid","英菲尼迪JX,infinitie","英菲尼迪M,infinitif","英菲尼迪Q50,infinitig",
			   				"英菲尼迪Q60,infinitih","英菲尼迪Q60S,infinitii","英菲尼迪Q70L,infinitij","英菲尼迪QX,infinitik","英菲尼迪QX50(进口),infinitil","英菲尼迪QX60,infinitim",
			   				"英菲尼迪QX70,infinitin","英菲尼迪QX80,infinitio","英菲尼迪Q50L,infinitip","英菲尼迪QX50,infinitiq"
			   				],

   
			   "英伦":["英伦SC3,englona","英伦SC5,englonb","英伦SC6,englonc","英伦SC7,englond","英伦SX7,englone","英伦TX4,englonf"],
			        
			   "野马":["野马F10,yemaa","野马F12,yemab","野马F16,yemac","野马F99,yemad","野马T70,yemae"],	
			       
			   "英致":["英致G3,yingzhia","英致737,yingzhib"],
			   
			   "中誉":["凌特,zhongyua","威霆,zhongyub","假日风情,zhongyuc","迷你巴士,zhongyud"],
			     
			   "中顺":["世纪,zhongshuna"],
	
			   "中兴":["中兴C3,zhongxinga","无限,zhongxingb"],
			   
			   "中华":["H220,zhonghuaa","H230,zhonghuab","H320,zhonghuac","H330,zhonghuad","H530,zhonghuae","中华V5,zhonghuaf","华晨中华,zhonghuag",
			   			"中华V3,zhonghuah","中华豚,zhonghuai","尊驰,zhonghuaj","酷宝,zhonghuak","骏捷,zhonghual","骏捷Cross,zhonghuam",
			   			"骏捷FRV,zhonghuan","骏捷FSV,zhonghuao","骏捷Wagon,zhonghuap"
			   			],
			                  
			   "众泰":["2008,zhongtaia","梦迪博朗,zhongtaib","5008,zhongtaic","M300朗悦,zhongtaid","朗骏Z200,zhongtaie",
			   			"郎朗Z200HB,zhongtaif","Z300,zhongtaig","T200,zhongtaih","V10,zhongtaii","Z100,zhongtaij","T600,zhongtaik",
			   			"知豆,zhongtail","云100,zhongtaim","Z500,zhongtain","大迈X5,zhongtaio"
			   			],

			    "中欧":["尊逸,zhongoua"],
           };


var brand_style= { 
/*安驰*/
                "小公主": ["1999款0.8手动,anchixgza","2001款0.8手动,anchixgzb","2001款0.8自动,anchixgzc","2006款0.8自动,anchixgzd", "2006款0.8手动,anchixgze"
                           ],
			    
				"雪豹": ["2009款 2.0 手动 前驱,anchixba","2009款 2.8T 手动 前驱4JB1TC 柴油,anchixbb","2009款 2.8T 手动 前驱 柴油,anchixbc",
				         "2009款 2.2 手动 前驱,anchixbd","2009款 2.0T 手动 前驱 柴油,anchixbf","2012款 2.8T 手动 标准版 柴油,anchixbg"],
						 
				"威豹": ["2009款 2.8T 手动 前驱 柴油,anchiwba","2009款 2.2 手动 前驱,anchiwbb","2009款 2.0T 手动 前驱 柴油,anchiwbc","2009款 2.0 手动 前驱,anchiwbd"],
			    
				"瑞驰征途": ["2013款 2.4 手动,anchirczta","2014款 2.5T 手动 后驱 柴油,anchircztb"],
				
				"瑞驰K3": ["2013款 2.8T 手动 标准版4JB1TC 柴油,anchirck3a","2013款 2.8T 手动 标准版HFC4DA12B1 柴油,anchirck3b","2013款 2.2 手动 标准版JM491QME,anchirck3c",
						  "2014款 2.8T 手动 豪华版HFC4DA11C 柴油,anchirck3d","2013款 2.5T 手动 豪华版后驱 柴油,anchirck3e","2013款 2.5T 手动 标准版4D25TC 柴油,anchirck3f"
				          ],
			    
				"瑞驰K5": ["2011款 2.2 手动 精英版后驱,anchirck5a","2013款 2.5T 手动 豪华型后驱 柴油,anchirck5b","2013款 2.5T 手动 标准版4D25TC 柴油,anchirck5c",
				          "2013款 2.8T 手动 标准版4JB1TC 柴油,anchirck5d","2013款 2.8T 手动 标准版HFC4DA12B1 柴油,anchirck5e",
				           "2013款 2.0 手动 标准版HFC4GA3C,anchirck5f","2013款 2.2 手动 标准版JM491QME,anchirck5g","2014款 2.5T 手动 至尊版短轴后驱 柴油,anchirck5h",
				           "2014款 2.8T 手动 豪华版长轴后驱HFC4DA11C,anchirck5i","2014款 2.8T 手动 精英版短轴后驱 柴油,anchirck5j",
				           "2014款 2.5T 手动 精英版短轴后驱 柴油,anchirck5k","2014款 2.5T 手动 豪华版长轴后驱 柴油,anchirck5l","2014款 2.2 手动 豪华版长轴后驱 油气混合,anchirck5m",
				           "2014款 2.2 手动 精英版短轴后驱 油气混合,anchirck5n","2014款 2.8T 手动 豪华版长轴后驱4JB1T4B,anchirck5o"
				           ],
/*奥迪*/				
				 "奥迪A3": ["2014款 Sportback 35 TFSI 自动豪华型,audia3a","2014款 Sportback 35 TFSI 自动进取型,audia3b","2014款 Sportback 35 TFSI 自动时尚型,audia3c",
				        "2014款 Sportback 35 TFSI 自动舒适型,audia3d","2014款 Limousine 35 TFSI 自动豪华型,audia3e","2014款 Limousine 35 TFSI 自动进取型,audia3f",
				        "2014款 Limousine 35 TFSI 自动时尚型 ,audia3g","2015款 Sportback 40 TFSI 自动舒适型,audia3h","2015款 Sportback 40 TFSI 自动豪华型,audia3i",
				        "2015款 Limousine 40 TFSI 自动舒适型,audia3j","2015款 Limousine 40 TFSI 自动豪华型,audia3k"
				       ],
				
				
				 "奥迪A4L": ["2009款 2.0 TFSI 标准型,audia4la","2009款 3.2 FSI quattro 旗舰型,audia4lb","2009款 2.0 TFSI 豪华型,audia4lc","2009款 2.0 TFSI 舒适型,audia4ld",
				         "2009款 2.0 TFSI 技术型,audia4le","2010款 1.8 TFSI 舒适型,audia4lf","2010款 2.0 TFSI 运动型 ,audia4lg","2010款 2.0 TFSI 舒适型,audia4lh",
				         "2010款 2.0 TFSI 标准型,audia4li","2010款 2.0 TFSI 技术型,audia4lj","2010款 2.0 TFSI 豪华型,audia4lk","2011款 1.8 TFSI 舒适型,audia4ll",
				         "2011款 2.0 TFSI 标准型,audia4lm","2011款 2.0 TFSI 技术型,audia4ln","2011款 2.0 TFSI 豪华型,audia4lo","2011款 2.0 TFSI 舒适型,audia4lp",
				         "2011款 2.0 TFSI 运动型,audia4lq","2011款 2.0 TFSI 尊享型,audia4lr","2012款 1.8 TFSI 手动舒适型,audia4ls","2012款 1.8 TFSI 自动舒适型,audia4lt",
				         "2012款 2.0 TFSI 自动标准型,audia4lu","2012款 2.0 TFSI 自动舒适型,audia4lv","2012款 2.0 TFSI 自动技术型,audia4lw","2012款 2.0 TFSI 自动豪华型,audia4lx",
				         "2012款 2.0 TFSI 自动运动型,audia4ly","2013款 35 TFSI 自动标准型,audia4lz","2013款 30 TFSI 手动舒适型,audia4laa","2013款 30 TFSI 自动舒适型,audia4lab",
				         "2013款 35 TFSI 自动舒适型,audia4lac","2013款 35 TFSI 自动技术型,audia4lad","2013款 35 TFSI 自动豪华型,audia4lae","2013款 40 TFSI quattro运动型,audia4laf",
				         "2013款 40 TFSI quattro个性运动型,audia4lag","2015款 30 TFSI 手动舒适型,audia4lah","2015款 30 TFSI 自动舒适型,audia4lai",
				         "2015款 35 TFSI 自动标准型,audia4laj","2015款 35 TFSI 自动技术型,audia4lak","2015款 35 TFSI 自动舒适型,audia4lal","2015款 35 TFSI 自动豪华型,audia4lam",
				         "2015款 45 TFSI quattro个性运动型,audia4lan","2015款 45 TFSI quattro运动型,audia4lao"
				       ],
				       
				       
				 "奥迪A6L": ["2005款 2.0T 基本型,audia6la","2005款 2.4L 技术型,audia6lb","2005款 2.0T 手动标准型,audia6lc","2005款 3.0L quattro 领先尊享型,audia6ld",
				 		 "2005款 2.4L 舒适型,audia6le","2005款 2.4L 技术领先型,audia6lf","2005款 3.0L 技术领先型,audia6lg","2005款 2.0T 自动标准型,audia6lh","2005款 2.4L 尊贵型,audia6li","2006款 3.2 FSI 舒适娱乐型,audia6lj",
				         "2006款 3.2 FSI quattro 领先尊享型,audia6lk","2006款 4.2 FSI quattro 至尊旗舰,audia6ll","2006款 3.2 FSI 技术领先型,audia6lm","2006款 3.2 FSI 尊享型,audia6lo","2008款 2.0T 手动标准型,audia6lp","2008款 2.0T 自动标准型,audia6lq",
				         "2008款 2.4L 技术型,audia6lr","2008款 2.4L 舒适型,audia6ls","2008款 2.4L 尊贵型,audia6lt","2008款 2.8 FSI 舒适娱乐型,audia6lu","2008款 2.8 FSI 尊享型,audia6lv","2008款 3.2 FSI quattro 舒适娱乐型,audia6lw",
				         "2008款 3.2 FSI quattro 豪华型,audia6lx","2008款 3.2 FSI 尊享型,audia6ly","2008款 2.0T 奥运限量版,audia6lz","2009款 2.8 FSI quattro 豪华型,audia6laa","2009款 2.0 TFSI 基本型,audia6lab",
				         "2009款 2.0 TFSI 手动标准型,audia6lac","2009款 2.0 TFSI 手动标准型,audia6lad","2009款 2.0 TFSI 自动标准型,audia6lae","2009款 2.4L 技术型,audia6laf","2009款 2.4L 舒适型,audia6lag",
				         "2009款 2.4L 豪华型,audia6lah","2009款 2.8 FSI 舒适娱乐型,audia6lai","2009款 2.8 FSI 豪华型,audia6laj","2009款 3.0 TFSI quattro 豪华型,audia6lak","2010款 2.0 TFSI 基本型,audia6lal","2010款 2.0 TFSI 手动标准型,audia6lam",
				         "2010款 3.0 TFSI quattro 豪华型,audia6lao","2010款 2.8 FSI quattro 豪华型,audia6lap","2010款 2.8 FSI 豪华型,audia6laq","2010款 2.0 TFSI 自动标准型,audia6lar","2010款 2.8 FSI 舒适型,audia6las",
				         "2010款 2.4L 豪华型,audia6lat","2010款 2.4L 技术型,audia6lau","2010款 2.4L 舒适型,audia6lav","2011款 2.0 TFSI 手动基本型,audia6law","2011款 2.0 TFSI 手动标准型,audia6lax","2011款 2.0 TFSI 自动标准型,audia6lay",
				         "2011款 2.4L 技术型,audia6laz","2011款 2.4L 舒适型,audia6lba","2011款 2.4L 豪华型,audia6lbb","2011款 2.8 FSI 舒适型,audia6lbc","2011款 2.8 FSI 豪华型,audia6lbd",
				         "2011款 2.8 FSI quattro 豪华型,audia6lbe","2011款 3.0 TFSI quattro 豪华型,audia6lbf", "2011款 2.0 TFSI 自动舒适型,audia6lbg","2012款 30 FSI 技术型,audia6lbh","2012款 50 TFSI quattro 豪华型,audia6lbi","2012款 TFSI 手动基本型,audia6lbj",
				         "2012款 TFSI 标准型,audia6lbk","2012款 TFSI 舒适型,audia6lbl","2012款 35 FSI 舒适型,audia6lbm","2012款 35 FSI 豪华型,audia6lbn","2012款 35 FSI quattro 豪华型,audia6lbo",
				         "2012款 30 FSI 舒适型,audia6lbp","2012款 30 FSI 豪华型,audia6lbq","2014款 TFSI 标准型,audia6lbr","2014款 TFSI 舒适型,audia6lbs","2014款 30 FSI 技术型,audia6lbt","2014款 30 FSI 舒适型,audia6lbu",
				         "2014款 30 FSI 豪华型,audia6lbv","2014款 35 FSI 舒适型,audia6lbw","2014款 35 FSI 豪华型,audia6lbx","2014款 35 FSI quattro 豪华型,audia6lby","2014款 50 TFSI quattro 豪华型,audia6lbz","2015款 35 FSI quattro 舒适型,audia6lca",
				         "2015款 30 FSI 百万纪念舒享型,audia6lcb"
				       ], 
				     

				   "奥迪Q3":["2013款 40 TFSI quattro 豪华型,audiq3a","2013款 35 TFSI quattro 舒适型,audiq3b","2013款 35 TFSI 进取型,audiq3c","2013款 35 TFSI 舒适型,audiq3d",
				   		 "2013款 35 TFSI quattro 技术型,audiq3e","2013款 35 TFSI quattro 豪华型,audiq3f","2015款 30 TFSI 进取型,audiq3g","2015款 30 TFSI 舒适型,audiq3h",
				   		 "2015款 35 TFSI 进取型,audiq3i","2015款 35 TFSI 舒适型,audiq3j","2015款 35 TFSI quattro 技术型,audiq3k","2015款 35 TFSI quattro 舒适型,audiq3l","2015款 35 TFSI quattro 豪华型,audiq3m"
				        ],
				        
				        
				   "奥迪Q5":["2010款 2.0TFSI 进取型,audiq5a","2010款 2.0TFSI 技术型,audiq5b","2010款 2.0TFSI 舒适型,audiq5c","2010款 2.0TFSI 豪华型,audiq5d",
				   		 "2011款 2.0TFSI 进取型,audiq5e","2011款 2.0TFSI 技术型,audiq5f","2011款 2.0TFSI 舒适型,audiq5g","2011款 2.0TFSI 动感型,audiq5h","2011款 2.0TFSI 豪华型,audiq5i",
				         "2012款 2.0TFSI 进取型,audiq5hj","2012款 2.0TFSI 技术型,audiq5k","2012款 2.0TFSI 舒适型,audiq5l","2012款 2.0TFSI 动感型,audiq5m","2012款 2.0TFSI 豪华型,audiq5n",
				   		 "2013款  40 TFSI 进取型,audiq5o","2013款  40 TFSI 技术型,audiq5p","2013款  40 TFSI 舒适型,audiq5q","2013款  40 TFSI 豪华型,audiq5r",
				   		 "2015款  40 TFSI 进取型,audiq5s","2015款  40 TFSI 技术型,audiq5t","2015款  40 TFSI 舒适型,audiq5u","2015款  40 TFSI 豪华型,audiq5v"
				        ],
				        
				   "奥迪A4":["2003款 1.8T 自动 ,audia4a","2003款 2.4 自动,audia4b",
				         "2004款 3.0L 娱乐型,audia4c","2004款 1.8T 标准型,audia4d","2004款 1.8T 技术型,audia4e","2004款 1.8T 舒适型,audia4f","2004款 1.8T,audia4g",
				         "2004款 2.4L 舒适型,audia4h","2005款 1.8T 自动基本型,audia4i","2005款 1.8T 6挡自动舒适型,audia4j","2005款 1.8T 技术型,audia4k",
				         "2005款 2.4L 舒适型,audia4l","2005款 2.4L 运动型,audia4m","2005款 2.4L 尊贵型,audia4n","2006款 2.0T 尊享型,audia4o","2006款 1.8T 手动基本,audia4p",
				         "2006款 1.8T 自动基本,audia4q","2006款 1.8T 7挡自动舒适型,audia4r","2006款 1.8T 豪华型,audia4s","2006款 1.8T 领先型,audia4t","2007款 1.8T 手动舒适型,audia4u",
				         "2007款 1.8T 自动标准型,audia4v","2007款 1.8T 自动舒适型,audia4w","2007款 1.8T 自动豪华型,audia4x","2007款 2.0T 自动标准型,audia4y",
				         "2007款 2.0T 自动舒适型,audia4z","2007款 2.0T 自动豪华型,audia4aa","2008款 1.8T 手动舒适型,audia4ab","2008款 1.8T 自动舒适型,audia4ac","2008款 1.8T 个性风格版,audia4ad","2008款 1.8T 自动豪华型,audia4ae",
				         "2008款 2.0T 个性风格版,audia4af","2008款 2.0T 自动豪华型,audia4ag"
                         ],
                         
                         
                    "奥迪A6":["2000款 1.8T,audia6a","2001款 2.4L,audia6b","2001款 2.8L,audia6c","2002款 1.8L,audia6d","2002款 1.8T,audia6e","2002款 2.4L,audia6f",
                          "2002款 2.8L,audia6g","2003款 2.8L,audia6h","2003款 2.4L,audia6i","2003款 1.8L,audia6j","2003款 1.8T,audia6k",
                          "2004款 2.4L 豪华舒适,audia16l","2004款 1.8T 手动基本,audia6m","2004款 1.8T 自动基本,audia6n","2004款 1.8T 自动舒适,audia6o",
                          "2004款 2.4L 技术领先,audia6p","2004款 2.8L 豪华型,audia6q","2004款 2.8L 技术领先,audia6r","2004款 2.4L 自动基本,audia6s","2004款 2.8L 行政版,audia6t","2004款 2.4L 行政版,audia6u",
                          "2004款 2.5（TDI）手自一体,audia6v","2005款 1.8T 舒适型,audia6w","2005款 2.5（TDI）手自一体,audia6x"
                         ],
                         
                    "奥迪A1":["2012款 1.4 TFSI Ego,audia1a","2012款 1.4 TFSI Ego plus,audia1b","2012款 1.4 TFSI Urban,audia1c","2013款 30 TFSI Sportback Ego,audia1d",
                          "2013款 30 TFSI Sportback Urban,audia1e","2014款 30 TFSI 时尚型,audia1f","2014款 30 TFSI 技术型,audia1g","2014款 30 TFSI 舒适型,audia1h","2014款 30 TFSI 豪华型,audia1i",
                          "2014款 30 TFSI Sportback时尚型,audia1j","2014款 30 TFSI Sportback技术型,audia1k","2014款 30 TFSI Sportback舒适型,audia1l","2014款 30 TFSI Sportback豪华型,audia1m"
                         ],
                         
                         
                    "奥迪A3(进口)":["2010款 Sportback 1.4T 舒适型,audia3jka","2010款 Sportback 1.4T 豪华型,audia3jkb","2010款 Sportback 1.8T 豪华型,audia3jkc",
                                "2010款 Sportback 1.8T 尊贵型,audia3jkd","2012款 Sportback 1.8T 尊贵型,audia3jke","2012款 Sportback 1.4T 豪华型,audia3jkf",
                                "2012款 Sportback 1.4T 舒适型,audia3jkg","2012款 Sportback 1.4T 技术型,audia3jkh","2013款 Sportback 30 TFSI 技术型,audia3jki",
                                "2013款 Sportback 30 TFSI 舒适型,audia3jkj","2013款 Sportback 30 TFSI 豪华型,audia3jkk","2013款 Sportback 35 TFSI 豪华型,audia3jkl",
                                "2014款 Sportback 40 TFSI S line 豪华型,audia3jkm","2014款 Limousine 40 TFSI S line 豪华型,audia3jkn","2014款 Sportback 40 TFSI S line 舒适型,audia3jko",
                                "2014款 Limousine 40 TFSI S line 舒适型,audia3jkp","2015款 Cabriolet 40 TFSI,audia3jkq"
                             ],
                    
                    
                    "奥迪S3":["2015款 S3 2.0T Limousine,audis3a"],
                    
                    
                    "奥迪A4(进口)":["2013款 40 TFSI allroad quattro 豪华型,audia4jka","2013款 40 TFSI allroad quattro 舒适型,audia4jkb","2014款 40 TFSI allroad quattro,audia4jkc",
                                "2014款 40 TFSI allroad quattro plus版,audia4jkd"],
                                
                     
                    "奥迪A5":["2008款 3.2 coupe quattro,audia5a","2009款 2.0TFSI coupe,audia5b","2010款 2.0TFSI Cabriolet,audia5c","2010款 2.0TFSI Sportback 舒适型,audia5d",
                          "2010款 2.0TFSI Coupe,audia5e","2010款 2.0TFSI Coupe 风尚版,audia5f","2010款 2.0TFSI Sportback 技术型,audia5g","2010款 2.0TFSI Sportback 豪华型,audia5h",
                          "2012款 2.0TFSI Coupe,audia5i","2012款 2.0TFSI Cabriolet,audia5j","2012款 2.0TFSI Sportback,audia5k","2012款 2.0TFSI Coupe quattro,audia5l",
                          "2012款 3.0TFSI Coupe quattro,audia5m","2012款 2.0TFSI Cabriolet quattro,audia5n","2012款 2.0TFSI Sportback quattro,audia5o",
                          "2012款 3.0TFSI Sportback quattro,audia5p","2013款 Coupe 40 TFSI,audia5q","2013款 Coupe 40 TFSI quattro,audia5r","2013款 Coupe 50 TFSI quattro,audia5s",
                          "2013款 Cabriolet 40 TFSI,audia5t","2013款 Cabriolet 40 TFSI quattro,audia5u","2013款 Sportback 40 TFSI,audia5v","2013款 Sportback 40 TFSI quattro,audia5w",
                          "2013款 Sportback 50 TFSI quattro,audia5x","2013款 Coupe 40 TFSI风尚版,audia5y","2013款 Cabriolet 40 TFSI风尚版,audia5z","2013款 Sportback 40 TFSI风尚版,audia5aa",
                          "2014款 Coupe 45 TFSI,audia5ab","2014款 Coupe 45 TFSI风尚版,audia5ac","2014款 Coupe 45 TFSI quattro,audia5ad","2014款 Cabriolet 45 TFSI quattro风尚版,audia5ae",
                          "2014款 Sportback 45 TFSI,audia5af","2014款 Sportback 45 TFSI风尚版,audia5ag","2014款 Sportback 45 TFSI quattro风尚版,audia5ah"
                         ],
				    
				    "奥迪S5":["2009款 S5 4.2 Coupe,audis5a","2010款 S5 3.0T Cabriolet,audis5b","2010款 S5 3.0T Sportback,audis5c","2012款 S5 3.0T Sportback,audis5d","2012款 S5 3.0T Cabriolet,audis5e","2012款 S5 3.0T Coupe,audis5f"],
				    
				    "奥迪A6(进口)":["2005款 2.4 AT,audia6jka","2013款 40 hybrid,audia6jkb"],
  
				    "奥迪S6":["2013款 S6 4.0TFSI,audis6a"],
				    
				    "奥迪A7":["2012款 3.0TFSI quattro豪华型,audia7a","2012款 2.8FSI quattro进取型,audia7b","2012款 3.0TFSI quattro舒适型,audia7c","2013款 35 FSI quattro进取型,audia7d",
				          "2013款 50 TFSI quattro舒适型,audia7e","2013款 50 TFSI quattro豪华型,audia7f","2014款 30 FSI 时尚型,audia7g","2014款 30 FSI 进取型,audia7h","2014款 35 FSI quattro 技术型,audia7i",
				          "2014款 50 TFSI quattro 舒适型,audia7j","2014款 50 TFSI quattro 豪华型,audia7k"],
				    
				    "奥迪S7":["2013款 4.0 TFSI quattro,audis7a"],
				    				    
				    "奥迪A8":["2002款 2.8L 自动,audia8a","2003款 4.2L 自动,audia8b","2004款 A8L 3.0L,audia8c","2005款 A8L 3.0L,audia8d","2005款 A8L 4.2L,audia8e",
				          "2006款 A8L 6.0 W12 quattro旗舰型,audia8f","2007款 A8L 3.2 FSI 标准型,audia8g","2007款 A8L 3.2 FSI 豪华型,audia8h","2007款 A8L 3.2 FSI 技术型,audia8i",
				          "2007款 A8L 3.2 FSI 尊享型,audia8j","2008款 A8L 6.0 W12至尊旗舰型,audia8k","2008款 A8L 4.2 FSI尊贵型,audia8l","2008款 A8L 3.2 FSI标准型,audia8m",
				          "2008款 A8L 3.2 FSI豪华型,audia8n","2008款 A8L 2.8 FSI标准型,audia8o","2008款 A8L 2.8 FSI豪华型,audia8p","2009款 A8L 2.8 FSI 标准型,audia8q",
				          "2009款 A8L 3.0 FSI 标准型,audia8r","2009款 A8L 3.0 FSI 豪华型,audia8s","2009款 A8L 3.0 FSI 尊贵型,audia8t","2010款 A8L 3.0 FSI 百年纪念版,audia8u","2011款 A8L 3.0 TFSI quattro尊贵型(245kW),audia8v",
				          "2011款 A8L 3.0 TFSI quattro豪华型(245kW),audia8w","2011款 A8L 3.0 TFSI quattro舒适型(245kW),audia8x","2011款 A8L 3.0 TFSI quattro舒适型(213kW),audia8y",
				          "2011款 A8L 3.0 TFSI quattro豪华型(213kW),audia8z","2012款 A8L 45 TFSI quattro豪华型,audia8aa","2012款 A8L 45 TFSI quattro豪华型,audia8ab",
				          "2012款 A8L 50 TFSI quattro舒适型,audia8ac","2012款 A8L 50 TFSI quattro豪华型,audia8ad","2012款 A8L 50 TFSI quattro尊贵型,audia8ae","2013款 A8L 40 hybrid,audia8af",
				          "2013款 A8L 30 FSI 舒适型,audia8ag","2013款 A8L 30 FSI 专享型,audia8ah","2013款 A8L 55 TFSI quattro豪华型,audia8ai","2013款 A8L 55 TFSI quattro尊贵型,audia8aj",
				          "2013款 A8L 55 TFSI quattro专享型,audia8ak","2013款 A8L 50 TFSI quattro专享型,audia8al","2013款 A8L 45 TFSI quattro专享型,audia8am","2013款 A8L 45 TFSI quattro舒适型,audia8an",
				          "2013款 A8L 45 TFSI quattro豪华型,audia8ao","2013款 A8L 50 TFSI quattro舒适型,audia8ap","2013款 A8L 50 TFSI quattro豪华型,audia8aq","2013款 A8L 50 TFSI quattro尊贵型,audia8ar",
				          "2014款 A8L 30 FSI 舒适型,audia8as","2014款 A8L 45 TFSI quattro舒适型,audia8at","2014款 A8L 45 TFSI quattro豪华型,audia8au","2014款 A8L 45 TFSI quattro专享型,audia8av",
				          "2014款 A8L 50 TFSI quattro豪华型,audia8aw","2014款 A8L 60 TFSI quattro豪华型,audia8ax","2014款 A8L 6.3 FSI W12 quattro旗舰型,audia8zy","2014款 A8L 40 hybrid,audia8az"
				         ],
				    
				    
				    "奥迪S8":["2013款 S8 4.0TFSI quattro,audis8a","2014款 S8 4.0TFSI quattro,audis8b"],
				    
				    "奥迪Q3(进口)":["2012款 35 TFSI quattro 舒适型,audiq3jka","2012款 40 TFSI quattro 越野型,audiq3jkb"],
				    
				    "奥迪Q5(进口)":["2010款 2.0TFSI 运动版,audiq5jka","2010款 3.2FSI 运动版,audiq5jkb","2010款 2.0TFSI 越野版,audiq5jkc","2010款 3.2FSI 越野版,audiq5jkd",
				               "2012款 2.0TFSI hybrid,audiq5jke","2013款 40 TFSI hybrid,audiq5jkf","2013款 45 TFSI quattro 运动型,audiq5jkg"
				              ],
				    
				    "奥迪SQ5":["2014款 SQ5 3.0TFSI quattro,audisq5a"],
				    
				    "奥迪Q7": [ "2006款 4.2 FSI quattro 技术型,audiq7a","2006款 3.6 FSI quattro 基本型,audiq7b","2007款 3.6 FSI quattro 技术型,audiq7c","2007款 3.6 FSI quattro 舒适型,audiq7d",
							"2007款 3.6 FSI quattro 豪华型,audiq7e","2007款 4.2 FSI quattro 舒适型,audiq7f","2007款 4.2 FSI quattro 豪华型,audiq7g","2010款 6.0 V12 TDI旗舰型,audiq7h",
							"2010款 3.6 FSI quattro 基本型,audiq7i","2010款 3.6 FSI quattro 技术型,audiq7j","2010款 3.6 FSI quattro 舒适型,audiq7k","2010款 3.6 FSI quattro 豪华型,audiq7l",
							"2010款 3.0 TDI quattro 领先型,audiq7m","2010款 3.6 FSI舒适型越野典藏版,audiq7n","2010款 3.6 FSI技术型越野典藏版,audiq7o","2011款 3.0 TFSI 专享型(200kW),audiq7p",
							"2011款 3.0 TFSI 进取型(200kW),audiq7q","2011款 3.0 TFSI 技术型(200kW),audiq7r","2011款 3.0 TFSI 舒适型(200kW),audiq7s","2011款 3.0 TFSI 技术型(245kW),audiq7t",
							"2011款 3.0 TFSI 舒适型(245kW),audiq7u","2011款 3.0 TFSI 专享型(245kW),audiq7v","2012款 3.0 TDI 领先型,audiq7w","2012款 3.0 TFSI 进取型(200kW),audiq7x",
							"2012款 3.0 TFSI 技术型(200kW),audiq7y","2012款 3.0 TFSI 舒适型(200kW),audiq7z","2012款 3.0 TFSI 专享型(200kW),audiq7aa","2012款 3.0 TFSI 技术型(245kW),audiq7ab",
							"2012款 3.0 TFSI 专享型(245kW),audiq7ac","2013款 35 TFSI 进取型,audiq7ad","2013款 35 TFSI 技术型,audiq7ae","2013款 35 TFSI 舒适型,audiq7af",
							"2013款 35 TFSI 专享型,audiq7ag","2013款 40 TFSI 技术型,audiq7ah","2013款 40 TFSI 舒适型,audiq7ai","2013款 40 TFSI 专享型,audiq7aj","2014款 35 TFSI 进取型,audiq7ak",
							"2014款 35 TFSI 运动型,audiq7al","2014款 35 TFSI 越野型,audiq7am","2014款 35 TDI 运动型,audiq7an","2014款 35 TDI 越野型,audiq7ao","2015款 35 TFSI 进取型,audiq7ap",
							"2015款 35 TFSI 运动型,audiq7aq"
				    		],
				    		
				    "奥迪TT":["2002款 1.8T,auditta","2004款 TT Coupe 1.8T,audittb","2004款 TT Roadster 1.8T,audittc","2008款 TT 3.2 Quattro,audittd","2008款 TT Coupe 2.0TFSI,auditte",
							"2008款 TT Roadster 2.0TFSI,audittf","2010款 TT 2.0TFSI典藏版,audittg","2011款 TT Coupe 2.0TFSI,auditth","2011款 TT Roadster 2.0TFSI,auditti",
							"2011款 TT Coupe 2.0TFSI quattro,audittj","2011款 TT Roadster 2.0TFSI quattro,audittk","2012款 TT Roadster 2.0TFSI典雅版,audittl",
							"2012款 TT Roadster 2.0TFSI quattro典雅版,audittm","2013款 TT Coupe 45 TFSI,audi1tt","2013款 TT Roadster 45 TFSI,audittn",
							"2013款 TT Roadster 45 TFSI quattro,auditto","2014款 TT Coupe 45 TFSI 劲动型,audittp","2014款 TT Coupe 45 TFSI 悦尚型,audittq",
							"2014款 TT Coupe 45 TFSI quattro 劲动型,audittr"
							],
		    		
				    "奥迪TTS":["2008款 TTS Roadster 2.0TFSI quattro,audittsa","2008款 TTS Coupe 2.0TFSI quattro,audittsb","2011款 TTS Coupe 2.0TFSI quattro,audittsc",
				           "2013款 TTS Coupe 2.0TFSI quattro,audittsd","2013款 TTS Roadster 2.0TFSI quattro,audittse"],
				    		
				    		
				    
				    "奥迪R8":["2007款 4.2 FSI quattro,audir8a","2011款 Spyder 5.2 FSI quattro,audir8b","2010款 5.2 FSI quattro,audir8c","2012款 5.2 FSI quattro 限量版,audir8d",
				          "2013款 5.2 FSI quattro 中国专享型,audir8e","2014款 5.2 FSI quattro,audir8f","2014款 Spyder 5.2 FSI quattro,audir8g","2014款 4.2 FSI quattro,audir8h",
				          "2014款 Spyder 4.2 FSI quattro,audir8i"],
				    		
				    		
				    "奥迪RS5":["2012款 RS 5 Coupe,audirs5a","2013款 RS 5 Cabriolet,audirs5b","2014款 RS 5 Coupe 特别版,audirs5c"],
				    
				    
//阿斯顿马丁				    
				    "Rapide":["2010款 6.0L,astonmartinaa","2013款 6.0L S,astonmartinab"],
				    
				    "V8 Vantage":["2008款 4.7 Sportshift Roadster,astonmartinba","2011款 4.7L Sportshift Coupe,astonmartinbb","2011款 4.7L Sportshift Roadster,astonmartinbc",
				                  "2012款 4.7L S Coupe,astonmartinbd","2012款 4.7L S Roadster,astonmartinbe"
				                 ],
				                 
				    
				    "V12 Vantage":["2014款 6.0L S,astonmartinca"
				                 ],
				                 
				     
				    "阿斯顿马丁DB9":["2007款 6.0L Touchtronic Coupe,astonmartinda","2011款 6.0L Touchtronic Coupe,astonmartindb","2013款 6.0L Coupe,astonmartindc","2014款 6.0L Carbon White Coupe,astonmartindd"
				                    ],
				                    
				                    
				    "Vanquish":["2013款 6.0L 标准型,astonmartinea"
				                 ],                
				                    
				    "Virage":["2012款 6.0 Coupe,astonmartinfa"
				                 ],                    
				                    
				                    
				    "阿斯顿马丁DBS":["2009款 6.0 Touchtronic Volante,astonmartinga","2009款 6.0 Touchtronic Coupe,astonmartingb"
				                  ],

//阿尔法罗密欧
				    "ALFA GT":["2004款 3.2,alfaaa"],             
				                    
//巴博斯				                    
				    "巴博斯S级":["2011款 38S,barbusaa"],             
				                    
				    "巴博斯M级":["2013款 35MR,barbusab"],
	
//宝龙	
				    "霸道":["2004款 3.4 自动 四驱,baolongaa","2004款 2.7 手动 四驱,baolongab","2004款 2.4 自动 四驱,baolongac"],
				    
				    "天马座":["2004款 2.0 手动 环保型7-11座,baolongba","2004款 2.0 手动 经济型7-11座,baolongbb","2004款 2.0 手动 精致型,baolongbc","2004款 2.4 手动 标准型,baolongbd"],
				    
				    "菱惠":["2003款 2.0 手动,baolongca"],
				    
				    "菱麒":["2003款 2.4 手动,baolongda"],
				    
				    "菱骏":["2003款 2.4 手动 豪华型,baolongea","2003款 2.4 手动 标准型,baolongeb"],
               
				    "Panamera":["2010款 Panamera Turbo 4.8T,porscheaa","2010款 Panamera 4S 4.8L,porscheab","2010款 Panamera S 4.8L,porscheac",
								"2010款 Panamera 4 3.6L,porschead","2010款 Panamera 3.6L,porscheae","2012款 Panamera S Hybrid 3.0T,porscheaf","2012款 Panamera GTS 4.8L,porscheag",
								"2013款 Panamera Platinum Edition 3.6L,porscheah","2013款 Panamera 4 Platinum Edition 3.6L,porscheai","2014款 Panamera 4S Executive 3.0T,porscheaj",
								"2014款 Panamera S E-Hybrid 3.0T,porscheak","2014款 Panamera Turbo Executive 4.8T,porscheal","2014款 Panamera 3.0T,porscheam",
								"2014款 Panamera 4 3.0T,porschean","2014款 Panamera S Executive 3.0T,porscheao","2014款 Panamera Executive 3.0T,porscheap",
								"2014款 Panamera 4 Executive 3.0T,porscheaq"
								],
				                    
				    "Macan":["2014款 Macan Turbo 3.6T,porscheba","2014款 Macan S 3.0T,porschebb","2014款 Macan 2.0T,porschebc","2016款 Macan 2.0T,porschebd"],    
				                    
				                    
				    "卡宴":["2006款 Cayenne 3.2L AT,porscheca","2006款 Cayenne Turbo 4.5T,porschecb","2006款 Cayenne S 4.5L AT,porschecc","2007款 Cayenne 3.6L,porschecd",
							"2007款 Cayenne S 4.8L,porschece","2007款 Cayenne Turbo 4.8T,porschecf","2008款 Cayenne GTS 4.8L,porschecg","2008款 Cayenne Turbo S 4.8T,porschech",
							"2009款 Cayenne S Transsyberia 4.8L,porscheci","2011款 Cayenne S Hybrid 3.0T,porschecj","2011款 Cayenne Turbo 4.8T,porscheck",
							"2011款 Cayenne 3.0T,porschecl","2011款 Cayenne S 4.8L,porschecm","2013款 Cayenne Turbo S 4.8T,porschecn","2012款 Cayenne GTS 4.8L,porscheco",
							"2014款 Cayenne Platinum Edition 3.0T,porschecp","2015款 Cayenne S 3.6T,porschecq","2015款 Cayenne Turbo 4.8T,porschecr",
							"2015款 Cayenne S E-Hybrid 3.0T,porschecs","2015款 Cayenne 3.0T,porschect","2015款 Cayenne GTS 3.6T,porschecu" 
				          ],
				                    
				    "Boxster":["2004款 Boxster S 3.2L,porscheda","2006款 Boxster 2.7L,porschedb","2006款 Boxster S 3.2L,porschedc",
							  "2009款 Boxster S 3.4L Porsche Edition 2,porschedd","2009款 Boxster 2.9L,porschede",
							  "2009款 Boxster S 3.4L,porschedf","2010款 Boxster Spyder 3.4L,porschedg",
							  "2011款 Boxster Black Edition 2.9L,porschedh","2013款 Boxster 2.7L,porschedi","2013款 Boxster S 3.4L,porschedj",
							  "2014款 Boxster GTS 3.4L,porschedk","2015款 Boxster Style Edition 2.7L,porschedl"
							  ],
				                    
				    "Cayman":["2006款 Cayman S AT 3.4L,porscheea","2006款 Cayman 2.7L,porscheeb","2008款 Cayman S Sport 3.4L,porscheec",
				              "2009款 Cayman 2.9L,porscheed","009款 Cayman S 3.4L,porscheee","2012款 Cayman R 3.4L,porscheef",
				              "2012款 Cayman Black Edition 2.9L,porscheeg","2013款 Cayman S 3.4L,porscheeh","2013款 Cayman 2.7L,porscheei","2.7L AT,porscheej"
				     		 ],
				                    
				    "保时捷911":["2004款 Carrera S Coupe 3.6L,porschefa","2006款 Turbo 3.6T,porschefb",
								"2006款 Targa 4 3.6L,porschefc","2006款 Carrera AT 3.6L,porschefd","2008款 Carrera 4S Cabriolet 3.8L,porschefe",
								"2008款 Carrera 4 Cabriolet 3.6L,porscheff","2008款 Carrera 4S 3.8L,porschefg","2008款 Carrera 4 3.6L,porschefh",
								"2008款 Carrera Cabriolet 3.6L,porschefi","2008款 Carrera S 3.8L,porschefj","2008款 Carrera 3.6L,porschefk",
								"2010款 GT2 3.6T,porschefl","2010款 Turbo 3.8T,porschefm","2010款 Turbo Cabriolet 3.8T,porschefn",
								"2010款 Carrera 3.6L,porschefo","2010款 Carrera S 3.8L,porschefp","2010款 Carrera 4 3.6L,porschefq",
								"2010款 Carrera 4S 3.8L,porschefr","2010款 Carrera Cabriolet 3.6L,porschefs","2010款 Carrera S Cabriolet 3.8L,porscheft",
								"2010款 Carrera 4S Cabriolet 3.8L,porschefu","2010款 Targa 4 3.6L,porschefv","2010款 Targa 4S 3.8L,porschefw",
								"2010款 Turbo S 3.8T,porschefx","2010款 Turbo S Cabriolet 3.8T,porschefy","2011款 Carrera GTS 3.8L,porschefz",
								"2011款 Edition Style 3.6L 敞篷版,porschefaa","2011款 Edition Style 3.6L 硬顶版,porschefab","2011款 Carrera 4 GTS Coupe 3.8L,porschefac",
								"2012款 Carrera S 3.8L,porschefad","2012款 Carrera 3.4L,porschefae","2012款 Carrera S Cabriolet 3.8L,porschefaf",
								"2012款 Carrera Cabriolet 3.4L,porschefag","2013款 Carrera 4S 3.8L,porschefah","2013款 Carrera 4 Cabriolet 3.4L,porschefai",
								"2013款 Carrera 4 3.4L,porschefaj","2013款 Carrera 4S Cabriolet 3.8L,porschefak","2013款 GT3 3.8L,porschefal",
								"2014款 Turbo S 3.8T,porschefam","2014款 Turbo 3.8T,porschefan","2014款 Turbo Cabriolet 3.8T,porschefao",
								"2014款 Turbo S Cabriolet 3.8T,porschefap","2014款 Targa 4 3.4L,porschefaq","2014款 Targa 4S 3.8L,porschefar",
								"2015款 3.8 自动 Carrera GTS,porschefas","2015款 3.8 自动 Carrera4 GTS四驱,porschefat","2015款 4.0 自动 GTS RS,porschefau","2015款 3.4 自动 Carrera,porschefav",
								"2015款 3.4 自动 Carrera4-Style-Edition,porschefaw","2016款 3.8T 自动 Turbo四驱,porschefax","2016款 3.8T 自动 Turbo S四驱 ,porschefaz",
								],

//别克
					"凯越":["2003款 1.6L 手动,biekea03a","2003款 1.8L 自动,biekea03b","2004款 1.8 自动顶级版,biekeaa","2004款 1.6 手动豪华版,biekeab",
					       "2004款 1.8 自动豪华版,biekeac","2004款 1.6 手动舒适版,biekead","2004款 HRV 1.6 自动豪华版,biekeae",
						"2005款 1.6 手动舒适型,biekeaf","2005款 1.6 自动豪华型,biekeag","2005款 1.6 手动豪华型,biekeah","2005款 1.8 自动顶级型,biekeai",
						"2005款 HRV 1.6LE-AT,biekeaj","2005款 HRV 1.6LE-MT,biekeak","2005款 1.8 自动豪华型,biekeal","2005款 旅行版 1.8 自动,biekeam",
						"2005款 旅行版 1.8 自动舒适,biekean","2005款 旅行版 1.8 自动顶级,biekeao","2005款 1.8 手动豪华型,biekeap","2006款 HRV 1.6 手动标准型,biekeaq",
						"2006款 1.6 自动舒适型,biekear","2007款 旅行版 1.8LE-MT,biekeas","2007款 旅行版 1.8LE-AT,biekeat","2007款 旅行版 1.6LX-MT,biekeau",
						"2007款 旅行版 1.6LX-AT,biekeav","2008款 HRV 1.6LE-MT运动版,biekeaw","2008款 HRV 1.6LE-AT运动版,biekeax","2008款 1.6LX-MT,biekeay",
						"2008款 1.6LE-MT,biekeaz","2008款 1.6LE-MTNavi,biekeaaa","2008款 1.6LE-AT,biekeaab","2008款 1.6LE-ATNavi,biekeaac","2008款 1.8LE-MT,biekeaad",
						"2008款 1.8LE-AT,biekeaae","2008款 HRV 1.6LE-ATNavi,biekeaaf","2008款 旅行版 1.8LE-ATNavi,biekeaag","2008款 1.8LE-MTNavi,biekeaah",
						"2011款 1.6LX-MT,biekeaai","2011款 1.6LX-AT,biekeaaj","2011款 1.6LE-MT,biekeaak","2011款 1.6LE-AT,biekeaao","2013款 1.5L 手动经典型,biekeaap",
						"2013款 1.5L 自动经典型,biekeaaq","2013款 1.5L 手动尊享型,biekeaar","2013款 1.5L 自动尊享型,biekeaas","2015款 1.5L 手动经典型,biekeaat",
						"2015款 1.5L 自动经典型,biekeaau","2015款 1.5L 手动尊享型,biekeaav","2015款 1.5L 自动尊享型,biekeaaw"
						],
						
					"威朗":["2015款 20T 双离合豪华型,biekeba"],
					
					"英朗":["2010款 XT 1.6L 手动进取版,biekeca","2010款 XT 1.8L 自动豪华版,biekecb","2010款 XT 1.6T 新锐运动版,biekecc","2010款 XT 1.8L 自动时尚版,biekecd",
							"2010款 XT 1.6T 时尚运动版,biekece","2010款 XT 1.6L 自动时尚版,biekecf","2010款 GT 1.8L 自动豪华版,biekecg","2010款 GT 1.6L 手动进取版,biekech",
							"2010款 GT 1.6L 自动时尚版,biekeci","2010款 GT 1.8L 自动时尚版,biekecj","2010款 GT 1.6T 时尚运动版,biekeck","2010款 GT 1.6T 新锐运动版,biekecl",
							"2011款 GT 1.8L 自动时尚版真皮款,biekecm","2011款 GT 1.6T 时尚运动版真皮款,biekecn","2012款 XT 1.6L 手动进取版,biekeco","2012款 XT 1.6L 自动时尚版,biekecp",
							"2012款 XT 1.6T 时尚运动版,biekecq","2012款 XT 1.6T 新锐运动版,biekecr","2012款 GT 1.6L 手动进取版,biekecs","2012款 GT 1.6L 自动时尚版,biekect",
							"2012款 GT 1.8L 自动时尚真皮版,biekecu","2012款 GT 1.6T 时尚运动真皮版,biekecv","2012款 GT 1.6T 新锐运动版,biekecw","2012款 XT 1.6L 手动舒适版,biekecx",
							"2012款 XT 1.6L 自动舒适版,biekecy","2012款 GT 1.6L 手动舒适版,biekecz","2012款 GT 1.6L 自动舒适版,biekecaa","2013款 XT 1.6L 手动舒适版,biekecab",
							"2013款 XT 1.6L 手动进取版,biekecac","2013款 XT 1.6L 自动舒适版,biekecad","2013款 XT 1.6L 自动时尚版,biekecae","2013款 XT 1.6T 自动时尚运动版,biekecaf",
							"2013款 GT 1.6L 手动舒适版,biekecag","2013款 GT 1.6L 手动进取版,biekecah","2013款 GT 1.6L 自动舒适版,biekecai","2013款 GT 1.6L 自动时尚版,biekecaj",
							"2013款 GT 1.8L 自动时尚版,biekecak","2013款 GT 1.6T 自动时尚运动版,biekecao","2014款 XT 1.6L 自动舒适版,biekecap","2014款 XT 1.6L 手动进取版,biekecaq",
							"2014款 XT 1.6L 自动时尚版,biekecar","2014款 XT 1.6T 自动时尚运动版,biekecas","2014款 XT 1.6T 自动新锐运动版,biekecat","2015款 18T 双离合精英型,biekecau",
							"2015款 15N 自动进取型,biekecav","2015款 15N 手动豪华型,biekecaw","2015款 15N 自动豪华型,biekecax"
							],
					
					
					"君威":["2002款 3.0 自动,biekeda","2003款 3.0 自动,biekedb","2003款 2.5 自动,biekedc","2003款 2.0 手动,biekedd",
							"2004款 3.0L 标准型,biekede","2004款 3.0L 旗舰版,biekedf","2004款 2.5L 豪华型,biekedg","2004款 2.0L 标准型,biekedh","2004款 2.0L 豪华型,biekedi",
							"2004款 2.5L 标准型,biekedj","2005款 2.0L 豪华版,biekedk","2005款 2.5L G基本型,biekedl","2005款 2.5L G豪华版,biekedm","2005款 3.0L GL豪华版,biekedn",
							"2006款 2.0L 手动舒适,biekedo","2006款 2.5L 自动豪华,biekedp","2009款 2.0L 精英版,biekedq","2009款 2.0T 豪华运动版,biekedr","2009款 2.4L 旗舰版,biekeds",
							"2009款 2.0L 舒适版,biekedt","2009款 2.4L 精英版,biekedu","2009款 1.6T 精英运动版,biekedv","2010款 2.4L 舒适版,biekedw","2010款 2.0T 旗舰运动版,biekedx",
							"2010款 1.6T 精英运动版,biekedy","2010款 2.0L 世博版,biekedz","2010款 2.0T 豪华运动版,biekedaa","2010款 2.4L 精英版,biekedab","2010款 2.4L 旗舰版,biekedac",
							"2011款 2.0T GS超级运动版,biekedad","2011款 2.4L SIDI旗舰版,biekedae","2011款 2.4L SIDI精英版,biekedaf","2011款 2.4L SIDI舒适版,biekedag",
							"2011款 2.0L 舒适版,biekedah","2012款 2.0L 舒适版,biekedai","2012款 1.6T 手动精英运动版,biekedaj","2012款 2.0L 豪华版,biekedak","2012款 2.4L SIDI精英版,biekedal",
							"2012款 2.4L SIDI旗舰版,biekedam","2012款 2.0T 豪华运动版,biekedan","2012款 2.0T GS超级运动版,biekedao","2013款 2.0T GS豪情运动版,biekedap",
							"2013款 2.0T GS纵情运动版,biekedaq","2013款 1.6T 自动精英运动版,biekedar","2014款 2.0T GS纵情运动版,biekedas","2014款 2.4L SIDI豪华时尚型,biekedat",
							"2014款 2.0T GS燃情运动版,biekedau","2014款 2.0T GS豪情运动版,biekedav","2014款 2.4L SIDI精英时尚型,biekedaw","2014款 1.6T 时尚技术型,biekedax",
							"2014款 2.0L 精英时尚型,biekeday","2014款 2.0L 领先时尚型,biekedaz","2015款 1.6T 精英技术型,biekedba","2015款 1.6T 领先技术型,biekedbb","2015款 2.0L 领先时尚型,biekedbc",
							"2015款 2.0L 精英时尚型,biekedbd","2015款 1.6T 时尚技术型,biekedbe","2015款 2.0T GS豪情运动版,biekedbf","2015款 2.0T GS燃情运动版,biekedbg",
							"2015款 2.0T GS纵情运动版,biekedbh"
							],
												
					
					"君越":["2006款 2.4 精英型,biekeea","2006款 2.4 豪华型,biekeeb","2006款 3.0 旗舰型,biekeec","2006款 2.4 舒适型,biekeed","2006款 3.0 豪华型,biekeee",
							"2007款 2.4 豪华导航版,biekeef","2007款 2.4 标准型,biekeeg","2008款 2.4 舒适型,biekeeh","2008款 2.4 豪华导航版,biekeei","2008款 2.4 精英版,biekeej",
							"2008款 2.4 精英爵士版,biekeek","2008款 2.4 标准版,biekeel","2008款 2.4 豪华导航爵士版,biekeem","2009款 3.0L旗舰版,biekeen",
							"2009款 2.4L舒适版,biekeeo","2009款 2.4L雅致版,biekeep","2009款 2.4L豪雅版,biekeeq","2009款 2.4L豪华版,biekeer","2010款 2.4L雅致版,biekees",
							"2010款 2.4L豪雅版,biekeet","2010款 2.4L豪华版,biekeeu","2010款 3.0L旗舰版,biekeev","2010款 2.0T舒适版,biekeew","2010款 2.0T豪雅版,biekeex",
							"2011款 2.0T旗舰版,biekeey","2011款 2.4L SIDI舒适版,biekeez","2011款 2.4L SIDI雅致版,biekeeaa","2011款 2.4L SIDI豪雅版,biekeeab",
							"2011款 2.4L SIDI豪华版,biekeeac","2011款 eAssist 2.4L雅致版,biekeead","2011款 eAssist 2.4L豪雅版,biekeeae","2011款 2.4L SIDI舒适天窗版,biekeeaf",
							"2012款 2.4L SIDI舒适版,biekeeag","2012款 2.4L SIDI雅致版,biekeeah","2012款 2.4L SIDI豪雅版,biekeeai","2012款 2.4L SIDI豪华版,biekeeaj",
							"2012款 2.0T舒适版,biekeeak","2012款 2.0T豪雅版,biekeeal","2012款 2.0T旗舰版,biekeeam","2013款 2.4L SIDI精英舒适型,biekeean",
							"2013款 2.4L SIDI豪华舒适型,biekeeao","2013款 3.0L SIDI V6智享旗舰型,biekeeap","2013款 2.0T SIDI 精英技术型,biekeeaq",
							"2013款 2.0T SIDI 智享旗舰型,biekeear","2013款 2.0T SIDI 技术型,biekeeas","2013款 2.4L SIDI领先舒适型,biekeeat",
							"2014款 2.0T SIDI 精英技术型,biekeeau","2014款 2.0T SIDI 智享旗舰型,biekeeav"
							],
					
					"昂科拉":["2013款 1.4T 自动四驱旗舰型,biekefa","2013款 1.4T 手动两驱进取型,biekefb","2013款 1.4T 自动两驱精英型,biekefc","d",
							"2014款 1.4T 手动两驱都市运动型,biekefe","2014款 1.4T 手动两驱都市进取型,biekeff","2014款 1.4T 自动两驱都市精英型,biekefg",
							"2014款 1.4T 自动两驱都市领先型,biekefh","2014款 1.4T 自动四驱全能旗舰型,biekefi","2015款 1.4T 自动两驱都市精英型,biekefj",
							"2015款 1.4T 自动四驱全能旗舰型,biekefk"
							],
					
					
					"昂科威":["2014款 28T 四驱全能运动旗舰型,biekega","2014款 28T 四驱精英型,biekegb","2014款 28T 四驱豪华型,biekegc","2014款 28T 四驱全能旗舰型,biekegd"
					       ],
					
					
					"别克GL8":[
							"2004款 3.0L CT2 舒适版,biekeha","2004款 3.0L LT 豪华版,biekehb","2004款 3.0L GT 旗舰剧院版,biekehc","2004款 3.0L CT3 舒适型,biekehd",
							"2005款 3.0L CT0 标准版,biekehe","2005款 陆尊 3.0L CT2 舒适型,biekehf","2005款 陆尊 3.0L LT 豪华型,biekehg","2005款 陆尊 3.0L GT 旗舰版,biekehh",
							"2005款 2.5L CT1 舒适型,biekehi",
							"2005款 2.5L CT2 舒适型,biekehj","2005款 2.5L LT 豪华型,biekehk","2006款 陆尊 3.0L CT 舒适版,biekehl","2006款 陆尊 3.0L LT 豪华版,biekehm",
							"2006款 陆尊 3.0L GT 精英版,biekehn","2006款 陆尊 3.0L XT 导航版,biekeho","2007款 2.5L GT 精英版,biekehp","2011款 3.0L XT豪华商务旗舰版,biekehq",
							"2011款 2.4L LT豪华商务行政版,biekehr","2011款 2.4L CT豪华商务舒适版,biekehs","2011款 3.0L GT豪华商务豪雅版,biekeht","2011款 2.4L CT舒适版,biekehu",
							"2011款 2.4L LT行政版,biekehv","2012款 2.4L BASE舒适版,biekehw","2013款 2.4L CT豪华商务舒适版,biekehx","2013款 2.4L LT豪华商务行政版,biekehy",
							"2013款 3.0L GT豪华商务豪雅版,biekehz","2013款 3.0L XT豪华商务旗舰版,biekehaa","2013款 2.4L 舒适版,biekehab","2013款 2.4L 行政版,biekehac",
							"2013款 2.4L 经典版,biekehad","2014款 2.4L 经典版,biekehae","2014款 2.4L 舒适版,biekehaf","2014款 2.4L CT豪华商务舒适版,biekehag","2014款 2.4L LT豪华商务行政版,biekehah",
							"2014款 3.0L GT豪华商务豪雅版,biekehai","2014款 3.0L XT豪华商务旗舰版,biekehaj","2015款 2.4L 豪华商务尊享版,biekehak"
							],
					
					"林荫大道":["2007款 2.8 舒适型,biekeia","2007款 3.6 精英型,biekeib","2007款 2.8 精英型,biekeic","2007款 3.6 旗舰型,biekeid","2009款 2.8 舒适型,biekeie","2009款 2.8 精英型,biekeif",
							  "2009款 2.8 豪华型,biekeig","2010款 3.0 舒适版,biekeih","2010款 3.0 豪华版,biekeii","2010款 3.0 旗舰版,biekeij"
							 ],
					
					
					"荣御":["2004款 GS 3.6 豪华版,biekeja","2005款 GL 2.8 舒适型,biekejb","2005款 GS 3.6 豪华运动版,biekejc"],
					
					"昂科雷":["2010款 3.6L 精英版,biekeka","2010款 3.6L 旗舰版,biekekb","2009款 3.6L CXL1精英版,biekekc","2009款 3.6L CXL2旗舰版,biekekd",
							 "2013款 3.6L 两驱雅致版,biekeke","2014款 3.6L 四驱智享旗舰型,biekekf"
							],
					
//宝马

					"宝马3系":["2003款 318i,bmwaa","2004款 325i,bmwab","2004款 318i,bmwac","2005款 320i 领先型,bmwad","2005款 325i 领先型,bmwae","2005款 320i 时尚型,bmwaf",
							"2005款 325i 时尚型,bmwag","2005款 320i 时尚型木内饰,bmwah","2007款 325i 豪华运动型,bmwai","2007款 320i 豪华型,bmwaj","2008款 325i 时尚型,bmwak",
							"2008款 325i 领先型,bmwal","2008款 320i 进取型,bmwam","2009款 318i 领先型,bmwan","2009款 320i 豪华型,bmwao","2009款 325i 豪华型,bmwap",
							"2009款 325i 时尚型,bmwaq","2009款 320i 时尚型,bmwar","2009款 325i M 运动型,bmwas","2009款 318i 进取型,bmwat","2010款 318i 进取型,bmwau",
							"2010款 318i 领先型,bmwav","2010款 320i 时尚型,bmwaw","2010款 320i 豪华型,bmwax","2010款 325i 时尚型,bmway","2010款 325i 豪华型,bmwaz",
							"2010款 325i M 运动型,bmwaaa","2010款 320i 世博版,bmwaab","2011款 318i 领先型,bmwaac","2011款 320i 时尚型,bmwaad","2011款 320i 豪华型,bmwaae",
							"2011款 325i 时尚型,bmwaaf","2011款 325i 豪华型,bmwaag","2011款 325i M 运动型,bmwaah","2012款 320i 豪华型,bmwaai","2012款 318i 领先型,bmwaaj",
							"2012款 320i 时尚型,bmwaak","2012款 325i 时尚型,bmwaal","2012款 325i 豪华型,bmwaam","2013款 335Li 风尚设计套装,bmwaan","2013款 320Li 风尚设计套装,bmwaao",
							"2013款 328Li 豪华设计套装,bmwaap","2013款 320Li 豪华设计套装,bmwaaq","2013款 320Li 时尚型,bmwaar","2013款 328Li 时尚型,bmwaas",
							"2013款 316i 进取型,bmwaat","2013款 320i 时尚型,bmwaau","2013款 改款 320Li 豪华设计套装,bmwaav","2013款 改款 320Li 风尚设计套装,bmwaaw",
							"2013款 320i 进取型,bmwaax","2013款 320i 运动设计套装,bmwaay","2013款 328i 运动设计套装,bmwaaz","2013款 328i M运动型,bmwaba",
							"2013款 316i 运动设计套装,bmwabb","2014款 320Li 时尚型,bmwabc","2014款 320Li 豪华设计套装,bmwabd","2014款 320Li 风尚设计套装,bmwabe",
							"2014款 328Li 时尚型,bmwabf","2014款 328Li 豪华设计套装,bmwabg","2014款 328Li 风尚设计套装,bmwabh","2014款 320i 进取型,bmwabi",
							"2014款 320i 时尚型,bmwabj","2014款 320i 运动设计套装,bmwabk","2014款 328i 运动设计套装,bmwabl","2014款 328i M运动型,bmwabm",
							"2014款 328i xDrive运动设计套装,bmwabn","2014款 328Li xDrive豪华设计套装,bmwabo","2015款 316Li 手动型,bmwabp","2015款 316Li 时尚型,bmwabq",
							"2015款 320Li 时尚型,bmwabr","2015款 320Li 豪华设计套装,bmwabs","2015款 328Li 时尚型,bmwabt","2015款 316i 进取型,bmwabu","2015款 320i 时尚型,bmwabv",
							"2015款 320i 运动设计套装,bmwabw","2015款 328i 运动设计套装,bmwabx","2015款 328i M运动型,bmwaby","2015款 320Li 超悦版时尚型,bmwabz",
							"2015款 320i 超悦版运动设计套装,bmwaca"
							],
							
							
					"宝马5系":["2004款 520i,bmwba","2004款 525i,bmwbb","2005款 520i,bmwbc","2005款 525i,bmwbd","2005款 530i,bmwbe","2006款 530i,bmwbf","2006款 523i 豪华型,bmwbg",
							"2006款 523i 典雅型,bmwbh","2006款 530Li 豪华型,bmwbi","2006款 523Li 豪华型,bmwbj","2006款 525Li 豪华型,bmwbk","2007款 523Li 典雅型,bmwbl",
							"2007款 525Li 典雅型,bmwbm","2007款 530Li 典雅型,bmwbn","2008款 525Li 豪华型,bmwbo","2008款 530Li 豪华型,bmwbp","2008款 523Li 典雅型,bmwbq",
							"2008款 523Li 豪华型,bmwbr","2008款 523Li 领先型,bmwbs","2008款 525Li 典雅型,bmwbt","2008款 530Li 领先型,bmwbu","2008款 530Li 典雅型,bmwbv",
							"2008款 523Li 标准型,bmwbw","2009款 523Li 标准型,bmwbx","2009款 523Li 领先型,bmwby","2009款 523Li 豪华型,bmwbz","2009款 525Li 领先型,bmwbaa",
							"2009款 525Li 豪华型,bmwbab","2009款 530Li 领先型,bmwbac","2009款 530Li 豪华型,bmwbad","2009款 520Li 领先型,bmwbae","2010款 520Li 领先型,bmwbaf",
							"2010款 520Li 豪华型,bmwbag","2010款 523Li 标准型,bmwbah","2010款 523Li 领先型,bmwbai","2010款 523Li 豪华型,bmwbaj","2010款 525Li 领先型,bmwbak",
							"2010款 525Li 豪华型,bmwbal","2010款 530Li 领先型,bmwbam","2010款 530Li 豪华型,bmwban","2011款 528Li 豪华型,bmwbao","2011款 535Li 豪华型,bmwbap",
							"2011款 523Li 豪华型,bmwbaq","2011款 535Li 行政型,bmwbar","2011款 523Li 领先型,bmwbas","2011款 528Li 领先型,bmwbat","2011款 535Li 领先型,bmwbau",
							"2011款 520Li 典雅型,bmwbav","2012款 520Li 典雅型,bmwbaw","2012款 523Li 领先型,bmwbax","2012款 523Li 豪华型,bmwbay","2012款 530Li 领先型,bmwbaz",
							"2012款 530Li 豪华型,bmwbba","2012款 535Li 领先型,bmwbbb","2012款 535Li 豪华型,bmwbbc","2012款 535Li 行政型,bmwbbd","2013款 520Li 典雅型,bmwbbe",
							"2013款 525Li 领先型,bmwbbf","2013款 525Li 豪华型,bmwbbg","2013款 530Li 领先型,bmwbbh","2013款 530Li 豪华型,bmwbbi","2013款 535Li 领先型,bmwbbj",
							"2013款 535Li 豪华型,bmwbbk","2013款 535Li 行政型,bmwbbl","2013款 535Li 卓乐版,bmwbbm","2013款 525Li 卓乐版,bmwbbn","2014款 535Li 行政型 豪华设计套装,bmwbbo",
							"2014款 520Li 典雅型,bmwbbp","2014款 525Li 领先型,bmwbbq","2014款 525Li 豪华设计套装,bmwbbr","2014款 525Li 风尚设计套装,bmwbbs",
							"2014款 530Li 领先型,bmwbbt","2014款 535Li 领先型,bmwbbu","2014款 535Li 豪华设计套装,bmwbbv","2014款 535Li 行政型 风尚设计套装,bmwbbw",
							"2014款 528Li xDrive领先型,bmwbbx","2014款 528Li xDrive豪华设计套装,bmwbby","2014款 528Li 领先型,bmwbbz","2014款 528Li 豪华设计套装,bmwbca",
							"2014款 528Li 风尚设计套装,bmwbcb"
							],	
					
					
					"宝马X1":["2012款 xDrive20i 豪华型,bmwca","2012款 xDrive28i 豪华型,bmwcb","2012款 sDrive18i 时尚型,bmwcc","2012款 sDrive18i 豪华型,bmwcd",
							"2012款 sDrive20i 领先型,bmwce","2013款 sDrive18i 运动设计套装,bmwcf","2013款 xDrive20i X设计套装,bmwcg","2013款 sDrive18i 手动型,bmwch",
							"2013款 sDrive18i 时尚型,bmwch","2013款 sDrive18i 领先型,bmwci","2013款 sDrive20i 运动设计套装,bmwcj","2013款 xDrive20i 探索版,bmwck",
							"2014款 sDrive18i 时尚型,bmwcl","2014款 sDrive18i 领先型,bmwcm","2014款 sDrive20i 运动设计套装,bmwcn","2014款 sDrive18i 运动设计套装,bmwco",
							"2014款 xDrive20i X设计套装,bmwcp","2014款 xDrive28i X设计套装,bmwcq","2014款 sDrive18i X设计套装,bmwcr","2014款 sDrive20i X设计套装,bmwcs",
							"2014款 xDrive20i 运动设计套装,bmwct","2015款 xDrive20i 时尚型,bmwcu","2015款 xDrive20i X套装晋级版,bmwcv","2015款 xDrive20i 时尚晋级版,bmwcw",
							"2015款 sDrive18i 领先晋级版,bmwcx","2015款 sDrive18i 时尚晋级版,bmwcy"
							],
							
					"宝马1系":["2008款 120i 自动挡,bmwda","2008款 130i 自动挡,bmwdb","2008款 120i 手动挡,bmwdc","2010款 120i巧克力限量版,bmwdd","2010款 120i运动限量版,bmwde",
							"2011款 120i 双门轿跑车,bmwdf","2011款 120i 敞篷轿跑车,bmwdg","2012款 118i 运动型,bmwdh","2012款 116i 都市型,bmwdi","2012款 118i 领先型,bmwdj",
							"2012款 116i 领先型,bmwdk","2013款 M135i 5门版,bmwdl","2013款 改款 116i 领先型,bmwdm","2013款 改款 116i 都市型,bmwdn","2013款 改款 118i 领先型,bmwdo",
							"2013款 改款 118i 运动型,bmwdp","2013款 改款 125i 运动型,bmwdq","2013款 改款 M135i 5门版,bmwdr","2015款 125i M 运动型,bmwds"
							],
							
					"宝马2系运动旅行车(进口)":["2014款 220i 豪华设计套装,bmwjkea","2015款 218i 运动设计套装,bmwjkeb"],	
					

					"宝马3系(进口)":["2003款 318i,bmwjkfa","2004款 330Ci,bmwjkfb","2005款 330i,bmwjkfc","2006款 320i典雅型,bmwjkfd","2007款 325i敞篷轿跑车,bmwjkfe",
								"2007款 330i敞篷轿跑车,bmwjkff","2007款 325i双门轿跑车,bmwjkfg","2007款 330i双门轿跑车,bmwjkfh","2008款 325i 6MT,bmwjkfi",
								"2008款 325i典雅型,bmwjkfj","2008款 330i双门轿跑车,bmwjkfk","2009款 335i,bmwjkfl","2009款 320i双门轿跑车,bmwjkfm",
								"2009款 320i敞篷轿跑版,bmwjkfn","2009款 325i双门轿跑车,bmwjkfo","2009款 325i敞篷轿跑车,bmwjkfp","2009款 330i双门轿跑车,bmwjkfq",
								"2009款 330i敞篷轿跑车,bmwjkfr","2010款 325i豪华型,bmwjkfs","2010款 320i豪华型,bmwjkft","2010款 320i时尚型,bmwjkfu",
								"2011款 325i敞篷轿跑版,bmwjkfv","2011款 330i敞篷轿跑版,bmwjkfw","2011款 335i双门轿跑车,bmwjkfx",
								"2011款 335i敞篷轿跑车,bmwjkfy","2011款 320i双门轿跑车,bmwjkfz","2011款 320i敞篷轿跑版,bmwjkfaa","2011款 325i双门轿跑车,bmwjkfab",
								"2011款 330i双门轿跑车,bmwjkfac","2012款 335i,bmwjkfad","2012款 318i领先型,bmwjkfae","2013款 328i运动设计套装,bmwjkfaf",
								"2013款 320i运动设计套装,bmwjkfag","2013款 320i时尚型,bmwjkfah","2013款 320i进取型,bmwjkfai","2013款 328i M运动型,bmwjkfaj",
								"2013款 改款 320i运动设计套装,bmwjkfak","2013款 320i运动设计套装 旅行版,bmwjkfal","2014款 320i运动设计套装,bmwjkfam","2014款 328i M运动型,bmwjkfan"
								],
													
					"宝马3系GT":["2013款 320i 领先型,bmwga","2013款 328i 风尚设计套装,bmwgb","2013款 335i 风尚设计套装,bmwgc","2013款 320i 风尚设计套装,bmwgd","2014款 328i 设计套装型,bmwge",
								"2014款 320i 设计套装型,bmwgf","2014款 320i 领先型,bmwgg"
								],
					
					"宝马4系":["2014款 428i 豪华设计套装,bmwha","2014款 428i 运动设计套装,bmwhb","2014款 428i Gran Coupe 设计套装型,bmwhc","2014款 428i 敞篷豪华设计套装,bmwhd",
							"2014款 428i 敞篷运动设计套装,bmwhe","2014款 435i 敞篷豪华设计套装,bmwhf","2014款 420i Gran Coupe 时尚型,bmwhg","2014款 420i 时尚型,bmwhh",
							"2014款 420i 设计套装型,bmwhi","2014款 428i xDrive 敞篷设计套装型,bmwhj","2014款 420i Gran Coupe 运动设计套装,bmwhk",
							"2014款 420i 敞篷风尚设计套装,bmwhl","2014款 420i 敞篷运动设计套装,bmwhm","2015款 428i 敞篷限量版,bmwhn"
							],
						
					"宝马5系(进口)":["2011款 535i 豪华运动型,bmwjkia","2011款 535i 领先运动型,bmwjkib","2011款 523i 领先型,bmwjkic","2011款 535i xDrive豪华型,bmwjkid",
								"2013款 530i 领先型 旅行版,bmwjkie","2013款 520i 典雅型,bmwjkif","2013款 535i 领先运动型,bmwjkig","2013款 528i xDrive豪华型,bmwjkih",
								"2012款 530i 领先型 旅行版,bmwjkii","2012款 520i 典雅型,bmwjkij","2012款 528i xDrive豪华型,bmwjkik","2014款 520i 典雅型,bmwjkil",
								"2014款 535i 设计套装型,bmwjkim","2014款 535i xDrive M运动型,bmwjkin"
								],	
														
					"宝马5系GT":["2010款 535i 领先型,bmwja","2010款 535i 豪华型,bmwjb","2011款 535i 典雅型,bmwjc","2011款 535i xDrive豪华型,bmwjd","2013款 535i 典雅型,bmwje",
								"2013款 535i 领先型,bmwjf","2013款 535i 豪华型,bmwjg","2013款 535i xDrive豪华型,bmwjh","2014款 528i 领先型,bmwji","2014款 528i 豪华型,bmwjj",
								"2014款 535i 领先型,bmwjk","2014款 535i 豪华型,bmwjl","2015款 528i 豪华型,bmwjm"
								],	
	
					"宝马6系":["2004款 645Ci,bmwka","2006款 630i敞篷轿跑车,bmwkb","2007款 630i双门轿跑车,bmwkc","2007款 630i敞篷轿跑车,bmwkd","2012款 640i双门轿跑车,bmwke",
							"2012款 640i Gran Coupe,bmwkf","2011款 650i敞篷轿跑车,bmwkg","2011款 640i敞篷轿跑车,bmwkh","2013款 640i敞篷轿跑车,bmwki",
							"2013款 640i双门轿跑车,bmwkj","2013款 改款 640i敞篷轿跑车,bmwkk","2013款 改款 640i Gran Coupe,bmwkl","2013款 改款 640i xDrive Gran Coupe,bmwkm"
							],
					
					"宝马7系":["2004款 730Li,bmwla","2004款 735Li,bmwlb","2004款 745Li,bmwlc","2004款 760Li,bmwld","2005款 730Li,bmwle","2005款 740Li,bmwlf",
							"2005款 750Li,bmwlg","2005款 760Li,bmwlh","2007款 730Li减配版,bmwli","2008款 730Li经典版,bmwlj","2008款 730Li领先型,bmwlk",
							"2009款 750Li领先型,bmwll","2009款 740Li豪华型,bmwlm","2009款 740Li尊贵型,bmwln","2009款 750Li豪华型,bmwlo","2009款 740Li领先型,bmwlp",
							"2009款 730Li领先型,bmwlq","2009款 730Li豪华型,bmwlr","2010款 760Li,bmwls","2010款 4.4T 混合动力版,bmwlt","2010款 750Li,bmwlu",
							"2011款 740Li 施坦威限量版,bmwlv","2011款 750Li xDrive,bmwlw","2011款 730Li典雅型,bmwlx","2013款 750Li xDrive,bmwly","2013款 740Li 混合动力版,bmwlz",
							"2013款 730Li 领先型,bmwlaa","2013款 730Li 豪华型,bmwlab","2013款 740Li 领先型,bmwlac","2013款 740Li 豪华型,bmwlad","2013款 740Li xDrive,bmwlae","2013款 760Li,bmwlaf",
							"2014款 730Li 臻享型,bmwlag"
							],
							
					"宝马X3":["2006款 3.0i,bmwma","2006款 2.5i,bmwmb","2006款 xDrive30i,bmwmc","2006款 xDrive25i,bmwmd","2006款 xDrive25si,bmwme",
							"2008款 xDrive25i领先型,bmwmf","2008款 xDrive25i豪华型,bmwmg","2009款 xDrive30i,bmwmh","2009款 xDrive25i豪华增配型,bmwmi",
							"2011款 xDrive35i 豪华型,bmwmj","2011款 xDrive28i 领先型,bmwmk","2011款 xDrive28i 豪华型,bmwml","2012款 xDrive28i 领先型,bmwmm",
							"2012款 xDrive20i 豪华型,bmwmn","2012款 xDrive28i 豪华型,bmwmo","2013款 xDrive20i 豪华型,bmwmp","2013款 xDrive28i 领先型,bmwmq",
							"2013款 xDrive28i 豪华型,bmwmr","2013款 xDrive35i 豪华型,bmwms","2013款 改款 xDrive20i 豪华型,bmwmt","2013款 改款 xDrive28i 领先型,bmwmu",
							"2013款 改款 xDrive28i 豪华型,bmwmv","2014款 xDrive20i X设计套装,bmwmw","2014款 xDrive28i 领先型,bmwmx","2014款 xDrive28i X设计套装,bmwmy",
							"2014款 xDrive35i M运动型,bmwmz","2014款 xDrive20i 领先型,bmwmaa"
							],
							
					"宝马X4":["2014款 xDrive35i M运动型,bmwna","2014款 xDrive28i M运动型,bmwnb","2014款 xDrive20i X设计套装,bmwnc",
							],
							
					"宝马X5":["2004款 4.4L,bmwoa","2004款 4.8L,bmwob","2004款 3.0L,bmwoc","2006款 4.4i,bmwod","2006款 3.0i,bmwoe","2006款 4.8is,bmwof",
							"2008款 xDrive48i豪华型,bmwog","2008款 xDrive30i豪华型,bmwoh",
							"2008款 xDrive30i领先型,bmwoi","2008款 xDrive48i领先型,bmwoj","2008款 xDrive30i典雅型,bmwok","2009款 xDrive30i领先型,bmwol","2009款 xDrive30i豪华型,bmwom",
							"2009款 xDrive30i尊贵型,bmwon","2009款 xDrive48i领先型,bmwoo","2009款 xDrive48i豪华型,bmwop","2010款 xDrive30i豪华型十周年纪念版,bmwoq",
							"2010款 xDrive30i尊贵型十周年纪念版,bmwor","2011款  xDrive35i 尊贵型,bmwos","2011款 xDrive35i 领先型,bmwot","2011款 xDrive35i 豪华型,bmwou","2011款 xDrive35i M运动型,bmwov",
							"2013款 xDrive35i 领先型,bmwow","2013款 xDrive35i 豪华型,bmwox","2013款 xDrive35i M运动型,bmwoy","2013款 xDrive50i 豪华型,bmwoz","2013款 xDrive35i 臻享版,bmwoaa",
							"2014款 xDrive35i 尊享型,bmwoab","2014款 xDrive35i 豪华型,bmwoac","2014款 xDrive35i 领先型,bmwoad","2014款 xDrive30d,bmwoae",
							"2014款 xDrive35i 典雅型,bmwoaf","2015款 xDrive28i,bmwoag"
							],
							
							
					"宝马X6":["2008款 xDrive35i,bmwpa","2008款 xDrive50i,bmwpb","2009款 xDrive35i,bmwpc","2009款 xDrive50i,bmwpd","2011款 xDrive35i,bmwpe",
							"2012款 xDrive35i,bmwpf","2013款 xDrive40i,bmwpg","2013款 xDrive35i,bmwph","2013款 xDrive50i,bmwpi","2014款 xDrive35i 运动型,bmwpj",
							"2015款 xDrive35i 豪华型,bmwpk"
							],		
														
														
					"宝马2系":["2014款 M235i,bmwqa","2014款 220i 领先型,bmwqb","2014款 220i 运动设计套装,bmwqc","2015款 220i 敞篷轿跑车 M运动型,bmwqd"
							],
														
					"宝马Z4":["2004款 3.0i,bmwra","2006款 2.5si敞篷跑车,bmwrb","2006款 3.0si敞篷跑车,bmwrc","2007款 3.0si双门跑车,bmwrd","2009款 sDrive35i锋尚型,bmwre",
							"2009款 sDrive30i锋尚型,bmwrf","2009款 sDrive23i领先型,bmwrg","2011款 sDrive35is,bmwrh","2011款 sDrive23i烈焰限量版,bmwri",
							"2010款 sDrive35i豪华型,bmwrj","2010款 sDrive30i领先型,bmwrk","2012款 sDrive28i领先型,bmwrl","2012款 sDrive20i领先型,bmwrm",
							"2012款 sDrive35i豪华型,bmwrn","2012款 sDrive35is,bmwro","2012款 sDrive20i十周年版,bmwrp","2013款 sDrive20i领先型,bmwrq",
							"2013款 sDrive28i领先型,bmwrr","2013款 sDrive35i豪华型,bmwrs"
							],	
							
					"宝马X1(进口)":["2010款 xDrive28i,bmwjksa","2010款 sDrive18i豪华型,bmwjksb","2010款 xDrive25i,bmwjksc","2012款 xDrive28i,bmwjksd","2012款 xDrive20i,bmwjkse"
								],
							
							
					"宝马M3":["2009款 M3四门轿车,bmwta","2009款 M3双门轿跑车,bmwtb","2009款 M3敞篷轿跑车,bmwtc","2010款 M3 25周年限量珍藏版,bmwtd","2011款 M3双门轿跑磨砂版,bmwte",
							 "2011款 M3碳纤限量版,bmwtf","2011款 M3双门碳纤顶版,bmwtg","2013款 M3敞篷轿跑车磨砂限量版,bmwth"
							],
							
					"宝马M4":["2014款 M4双门轿跑车,bmwua","2014款 M4敞篷轿跑车,bmwub"
							],
							
					"宝马M5":["2012款 M5,bmwva","2014款 M5,bmwvb","2014款 M5 马年限量版,bmwvc","2005款 M5,bmwvd"
							],	
							
					"宝马M6":["2006款 M6,bmwwa","2013款 M6 Gran Coupe,bmwwb","2013款 改款 M6 Coupe,bmwwc"
							],
							
							
					"宝马X5 M":["2010款 X5 M,bmwxa","2013款 X5 M,bmwxb"
							],
							
					"宝马X6 M":["2010款 X6 M,bmwya","2013款 X6 M,bmwyb"
							],			
									
							
					"宝马1系M":["2011款 1M Coupe,bmwza"],				
							
//奔驰	
				
					"奔驰C级":["2008款 C 200K 优雅型,benzaa","2008款 C 200K 时尚型,ben1zab","2008款 C 280 时尚型,benz1ac","2008款 C 230 时尚型,benz1ad",
							"2008款 C 200K 标准型,benzae","2010款 C 260 时尚型,benzaf","2010款 C 200K 标准型,benzag","2010款 C 300 时尚型,benzah",
							"2010款 C 200K 时尚型,benzai","2010款 C 300 运动型,benzaj","2010款 C 180K 经典型,benzak","2010款 C 200 CGI 优雅型,benzal",
							"2010款 C 200 CGI 时尚型,benzam","2010款 C 260 CGI 时尚型,benzan","2011款 C 200 CGI 优雅型,benzao","2011款 C 300 时尚型,benzap",
							"2011款 C 180K 经典型,benzaq","2011款 C 200 CGI 时尚型,benzar","2011款 C 260 CGI 时尚型,benzas","2013款 C 260 CGI 时尚型,benzat",
							"2013款 C 260 CGI 优雅型,benzau","2013款 C 180 CGI 经典型,benzav","2013款 C 260 时尚型 Grand Edition,benzaw",
							"2013款 C 180 经典型 Grand Edition,benzax","2013款 C 260 优雅型 Grand Edition,benzay","2013款 C 300 运动型 Grand Edition,benzaz",
							"2015款 C 260 L 运动型,benzaaa","2015款 C 180 L,benzaab","2015款 C 200 L 运动型,benzaac","2015款 C 200 L,benzaag","2015款 C 260 L,benzaad",
							"2015款 C 180 L 运动型,benzaae","2015款 改款 C 200 L 运动型,benzaaf","2015款 C 200 运动版,benzaah"
							],		
							
					"奔驰E级":["2005款 E 280,benzba","2005款 E 200K,benzbb","2006款 E 350 时尚型,benzbc","2006款 E 280 优雅型,benzbd","2007款 E 200K 优雅型,benzbe",
							"2007款 E 350 时尚型,benzbf","2007款 E 230 时尚型,benzbg","2007款 E 280 时尚型,benzbh","2007款 E 230 优雅型,benzbi",
							"2008款 E 280 个性运动版,benzbj","2008款 E 230 时尚型,benzbk","2010款 E 300 L 时尚型,benzbl","2010款 E 260 L CGI优雅型,benzbm",
							"2011款 E 260 L CGI时尚型,benzbn","2011款 E 300 L 时尚尊贵型,benzbo","2011款 E 200 L CGI优雅型,benzbp","2011款 E 300 L 优雅型,benzbq",
							"2012款 E 200 L CGI优雅型,benzbr","2012款 E 300 L 优雅型,benzbs","2012款 E 260 L CGI时尚型,benzbt","2012款 E 300 L 时尚型,benzbu",
							"2012款 E 300 L 时尚豪华型,benzbv","2013款 E 260 L CGI优雅型,benzbw","2013款 E 260 L CGI时尚型,benzbx","2013款 E 300 L 优雅型,benzby",
							"2013款 E 300 L 时尚型,benzbz","2013款 E 300 L 时尚豪华型,benzbaa","2014款 E 300 L,benzbab","2014款 E 260 L 豪华型,benzbac",
							"2014款 E 260 L 运动型,benzbad","2014款 E 260 L 运动豪华型,benzbae","2014款 E 300 L 运动型,benzbaf","2014款 E 400 L Hybrid,benzbag",
							"2014款 E 260 L,benzbah","2014款 改款 E 260 L 豪华型,benzbai","2014款 改款 E 300 L,benzbaj","2014款 改款 E 260 L 运动型,benzbak",
							"2014款 改款 E 400 L 运动豪华型,benzbal","2015款 E 200 L,benzbam","2015款 E 260 L,benzban","2015款 E 260 L 豪华型,benzbao",
							"2015款 E 320 L,benzbap","2015款 E 320 L 4MATIC,benzbaq","2015款 E 200 L 运动型,benzbar","2015款 E 260 L 运动型,benzbas",
							"2015款 E 260 L 运动时尚型,benzbat","2015款 E 260 L 运动豪华型,benzbau","2015款 E 320 L 运动型 4MATIC,benzbav",
							"2015款 E 320 L 运动豪华型,benzbaw","2015款 E 400 L 运动豪华型 4MATIC,benzbax","2015款 改款 E 260 L 豪华型,benzbay",
							"2015款 改款 E 200 L 运动型,benzbaz","2015款 改款 E 260 L 运动豪华型,benzbba"
							],
					
					
					"奔驰GLA级":["2015款 GLA 220 4MATIC 豪华型,benzca"],
					

					"奔驰GLK级":["2012款 GLK 300 4MATIC 动感型,benzda","2012款 GLK 300 4MATIC 豪华型,benzdb","2012款 GLK 300 4MATIC 时尚型,benzdc",
							"2013款 GLK 300 4MATIC 动感型,benzdd","2013款 GLK 300 4MATIC 时尚型,benzde","2013款 GLK 300 4MATIC 豪华型,benzdf",
							"2013款 GLK 300 4MATIC 动感天窗型,benzdg","2013款 改款 GLK 300 4MATIC 时尚型,benzdh","2013款 改款 GLK 300 4MATIC 豪华型,benzdi",
							"2014款 GLK 260 4MATIC 动感型,benzdj","2014款 GLK 200 标准型,benzdk","2015款 GLK 260 4MATIC 动感型 极致版,benzdl",
							"2015款 GLK 260 4MATIC 时尚型 极致版,benzdm","2015款 GLK 300 4MATIC 时尚型 极致版,benzdn","2015款 GLK 300 4MATIC 豪华型 极致版,benzdo"
							],
					
					"奔驰威霆":["2010款 2.5L 商务版,benzea","2010款 2.5L 精英版,benzeb","2011款 2.5L 精英版,benzec","2011款 2.5L 7座行政版,benzed",
							"2011款 2.5L 商务版,benzee","2013款 3.0L 精英版,benzef","2013款 3.0L 商务版,benzeg"
							],
					
					"唯雅诺":["2010款 2.5L 豪华版,benzfa","2010款 2.5L 尊贵版,benzfb","2011款 2.5L 限量版,benzfc","2011款 2.5L 豪华版,benzfd",
							"2011款 2.5L 尊贵版,benzfe","2011款 2.5L 领航版,benzff","2012款 2.5L 舒适版,benzfg","2012款 2.5L 礼遇版,benzfh",
							"2012款 2.5L 尊贵版,benzfi","2012款 2.5L 领航版,benzfj","2013款 3.5L 皓驰版,benzfk","2013款 3.5L 劲驰版,benzfl",
							"2013款 3.0L 领航版,benzfm","2013款 3.0L 舒适版,benzfn","2013款 3.0L 礼遇版,benzfo","2014款 3.5L 卓越版,benzfp","2015款 3.0L 合伙人版,benzfq"
							],
					
					"奔驰凌特":["2012款 2.1T尊旅 中轴版,benzga"],
					
					"奔驰A级":["2011款 A 160,benzha","2013款 A 180 时尚型,benzhb","2013款 A 200 都市型,benzhc","2013款 A 260 运动型,benzhd","2015款 A 180,benzhe","2015款 A 180 标准型,benzhf"
							],
					
					"奔驰B级":["2009款 B 200 动感型,benzia","2009款 B 200 时尚型,benzib","2009款 B 200 豪华型,benzic","2012款 B 200","2012款 B 180,benzid",
							 "2013款 B 260,benzie","2015款 B 200 豪华型,benzif","2015款 B 260 运动型,benzig"
							],
					
					"奔驰CLA级":["2014款 CLA 260 4MATIC,benzja","2015款 CLA 200,benzjb","2015款 CLA 220 4MATIC,benzjc"],
					
					"奔驰C级(进口)":["2004款 C 200K,benzka","2006款 C 230 时尚型,benzkb","2006款 C 230 优雅型,benzkc","2007款 C 200K 时尚型,benzkd","2007款 C 200K 优雅型,benzke",
								"2008款 C 230,benzkf","2010款 C 200 CGI旅行版,benzkg","2010款 C 300 旅行版,benzkh","2011款 C 200 豪华运动旅行版,benzki",
								"2011款 C 200 时尚旅行版,benzkj","2013款 C 180 轿跑版,benzkk"
								],
					
					"奔驰E级(进口)":["2004款 E 240,benz04a","2009款 E 350 Coupe,benzla","2009款 E 300 时尚型,benzlb","2009款 E 300 优雅型,benzlc","2010款 E 200 CGI 优雅型,benzld",
									"2010款 E 260 CGI 时尚型,benzle","2010款 E 260 CGI Coupe,benzlf","2010款 E 350 敞篷版,benzlg","2010款 E 300 优雅型豪华版,benzlh",
									"2010款 E 300 时尚型豪华版,benzli","2010款 E 260 CGI 敞篷版,benzlj","2011款 E 260 CGI Coupe,benzlk",
									"2011款 E 350 CGI 敞篷版,benzll","2012款 E 200 CGI Coupe,benzlm","2012款 E 260 CGI Coupe,benzln","2012款 E 350 Coupe,benzlo",
									"2014款 E 260 Coupe,benzlp","2014款 E 200 Coupe,benzlq"
									],
					
					"奔驰CLS级":["2008款 CLS 350,benzma","2009款 CLS 300,benzmb","2009款 CLS 350,benzmc","2007款 CLS 350,benzmd","2012款 CLS 350 CGI,benzme",
								"2012款 CLS 300 CGI,benzmf","2013款 CLS 350 猎装豪华型,benzmg","2013款 CLS 350 猎装时尚型,benzmh","2015款 CLS 260,benzmi",
								"2015款 CLS 320,benzmj"
								],
								
					"奔驰S级":["2004款 S 350,benzna","2004款 S 500,benznb","2004款 S 600,benznc","2004款 S 280,benznd","2006款 S 300,benzne","2008款 S 350 L 豪华型,benznf",
							"2008款 S 500 L,benzng","2008款 S 600 L,benznh","2008款 S 300 L 商务型,benzni","2008款 S 300 L 尊贵型,benznj","2008款 S 350 L 4MATIC,benznk",
							"2008款 S 500 L 4MATIC,benznl","2008款 S 300 L 豪华型,benznm","2010款 S 400 L HYBRID,benznn","2010款 S 300 L 商务型,benzno",
							"2010款 S 300 L 尊贵型,benznp","2010款 S 300 L 豪华型,benznq","2010款 S 350 L 豪华型,benznr","2010款 S 350 L 4MATIC,benzns",
							"2010款 S 600 L,benznt","2011款 S 500 L CGI 4MATIC,benznu","2011款 S 350 L CGI 4MATIC,benznv","2011款 S 350 L CGI,benznw",
							"2012款 S 350 L Grand Edition,benznx","2012款 S 300 L 商务型 Grand Edition,benzny","2012款 S 300 L 尊贵型 Grand Edition,benznz",
							"2012款 S 300 L 豪华型 Grand Edition,benznaa","2012款 S 350 L 4MATIC Grand Edition,benznab","2012款 S 400 L HYBRID Grand Edition,benznac",
							"2012款 S 500 L 4MATIC Grand Edition,benznad","2012款 S 600 L Grand Edition,benznae","2012款 S 300 L 商务简配型,benznaf",
							"2012款 S 600 L Grand Edition designo,benznag","2014款 S 500 L,benznah","2014款 S 400 L HYBRID,benznai","2014款 S 400 L 豪华型,benznaj",
							"2014款 S 400 L 尊贵型,benznak","2014款 S 500 L 4MATIC,benznal","2014款 S 320 L 商务型,benznam","2014款 S 320 L 豪华型,benznan",
							"2015款 S 500 4MATIC Coupe,benznao","2015款 S 400 L 4MATIC,benznap"
							],
							
					"奔驰G级":["2004款 G 500,benzpa","2009款 G 55 AMG,benzpb","2010款 G 500,benzpc","2013款 G 500,benzpd"],
					
					"奔驰GL级":["2008款 GL 450 4MATIC经典型,benzqa","2008款 GL 450 4MATIC尊贵型,benzqb","2011款 GL 450 4MATIC尊贵型,benzqc",
							  "2011款 GL 450 尊贵型 Grand Edition,benzqd","2013款 GL 500 4MATIC,benzqe","2014款 GL 400 4MATIC动感型,benzqf",
							   "2014款 GL 400 4MATIC豪华型,benzqg","2015款 GL 500 4MATIC,benzqh"],
					"奔驰R级":["2006款 R 350 4MATIC,benzra","2007款 R 350 L 4MATIC,benzrb","2007款 R 500 L 4MATIC,benzrc","2009款 R 300 L 豪华型,benzrd",
							"2009款 R 300 L 商务型,benzre","2010款 R 350 L 4MATIC Grand Edition,benzrf","2010款 R 300 L 商务型,benzrg",
							"2010款 R 300 L 豪华型,benzrh","2010款 R 350 L 4MATIC,benzri","2011款 R 300 L 商务型,benzrj","2011款 R 300 L 豪华型,benzrk",
							"2011款 R 350 L 4MATIC,benzrl","2014款 R 320 4MATIC商务型,benzrm","2014款 R 320 4MATIC豪华型,benzrn","2014款 R 400 4MATIC商务型,benzro",
							"2014款 R 400 4MATIC豪华型,benzrp","2015款 R 320 4MATIC豪华型,benzrq"
							],
					
					"奔驰SLK级":["2004款 SLK 200K,benzsa","2004款 SLK 350,benzsb","2008款 SLK 200K PASSION,benzsc","2008款 SLK 280 PASSION,benzsd",
							"2008款 SLK 350 PASSION,benzse","2009款 SLK 200K,benzsf","2009款 SLK 300,benzsg","2010款 SLK 300 Grand Edition,benzsh",
							"2010款 SLK 200K,benzsi","2011款 SLK 350,benzsj","2011款 SLK 200 时尚型,benzsk","2011款 SLK 200 豪华运动型,benzsl"
							],
					
					"奔驰SL级":["2010款 SL 300,benzta","2010款 SL 350,benztb","2011款 SL 300 Grand Edition,benztc","2013款 SL 350 时尚型,benztd"],
					
					"奔驰GLA级(进口)":["2015款 GLA 260 4MATIC,benzua"],
					
					"奔驰M级":["2005款 ML 350,benzva","2005款 ML 500,benzvb","2006款 ML 350 尊贵型,benzvc","2006款 ML 500,benzvd","2006款 ML 350 运动型,benzve",
							"2008款 ML 350 4MATIC动感型,benzvf","2008款 ML 350 4MATIC豪华型,benzvg","2008款 ML 500 4MATIC,benzvh","2010款 ML 300 4MATIC,benzvi",
							"2010款 ML 350 4MATIC豪华型,benzvj","2010款 ML 350 4MATIC豪华型特别版,benzvk","2012款 ML 350 动感型,benzvl",
							"2012款 ML 350 豪华型,benzvm","2012款 ML 300,benzvn","2014款 ML 320 4MATIC,benzvo","2014款 ML 400 4MATIC动感型,benzvp",
							"2014款 ML 400 4MATIC豪华型,benzvq","2015款 ML 320 4MATIC,benzvr","2015款 ML 400 4MATIC动感型,benzvs"
							],
												
					"Sprinter":["2009款 增配版,benzwa"],
					
					"奔驰威霆(进口)":["2010款 VITO 3.2L 紧凑型,benzoa"],
					
					"奔驰GLK级(进口)":["2008款 GLK 300 4MATIC 豪华型,benzxa","2008款 GLK 350 4MATIC,benzxb","2010款 GLK 300 4MATIC 时尚型,benzxc","2011款 GLK 350 4MATIC,benzxd",
					 				 "2011款 GLK 300 4MATIC 豪华型,benzxe","2011款 GLK 300 4MATIC 动感型,benzxf","2011款 GLK 300 4MATIC 时尚型,benzxg"
					 				 ],
					
					"唯雅诺(进口)":["2004款 Viano 3.2L,benzya","2006款 Viano 3.2L,benzyb","2010款 Viano 3.2L,benzyc","2010款 Viano 3.5L,benzyd",
					 			 "2011款 3.5L 125周年纪念版 6座,benzye","2011款 3.5L 125周年纪念版 7座,benzyf"
					 			],
					
					"奔驰CLK级":["2006款 CLK 200K 双门轿跑车,benzza","2006款 CLK 280 双门轿跑车,benzzb","2006款 CLK 350 双门轿跑车,benzzc","2006款 CLK 200K 敞篷跑车,benzzd",
					 			"2006款 CLK 280 敞篷跑车,benzze"
					 			],
					
					"奔驰A级AMG":["2014款 A 45 AMG 4MATIC,benzaba"],
					
					"奔驰CLA级AMG":["2014款 CLA 45 AMG 4MATIC,benzaca","2015款 CLA 45 AMG 4MATIC,benzacb"],
					
					"奔驰C级AMG":["2009款 C 63 AMG 动感型,benzada","2010款 C 63 AMG 动感型增强版,benzadb","2010款 C 63 AMG 高性能版,benzadc","2012款 C 63 AMG 动感型,benzadd",
								"2014款 C 63 AMG Coupe Edition 507,benzade","2014款 C 63 AMG Edition 507,benzadf","2015款 AMG C 63,benzadg"
								],
					
					"奔驰CLS级AMG":["2008款 CLS 63 AMG,benzaea","2012款 CLS 63 AMG,benzaeb","2013款 CLS 63 AMG,benzaec"
								],
					
					"奔驰S级AMG":["2007款 S 65 AMG,benzafa","2010款 S 65 AMG,benzafb","2014款 S 65 L AMG,benzaec,benzafc","2014款 S 63 L AMG 4MATIC,benzafd"
								],
					
					
					"奔驰M级AMG":["2007款 ML 63 AMG,benzaha","2011款 ML 63 AMG,benzahb","2012款 ML 63 AMG,benzahc","2014款 ML 63 AMG,benzahd"
								],
					
					
					"奔驰G级AMG":["2007款 G 55 AMG,benzaia","2009款 G 55 AMG,benzaib","2013款 G 63 AMG,benzaic","2013款 G 65 AMG,benzaid"
								],
					
					"奔驰GL级AMG":["2013款 GL 63 AMG,benzaja","2014款 GL 63 AMG,benzajb"],
					
					"奔驰SL级AMG":["2009款 SL 63 AMG,benzaka"],
					
					"奔驰SLS级AMG":["2011款 SLS AMG,benzama","2013款 SLS AMG 45周年中国限量版,benzamb"],
					
					"奔驰E级AMG":["2007款 E 63 AMG,benzana"],
					
					"奔驰SLK级AMG":["2004款 5.4 自动 SLK55,benzaoa","2006款 5.4 自动 SLK55 BlackSeries,benzaob","2008款 5.4 自动 SLK55,benzaoc","2012款 5.4 自动 SLK55,benzaod"],
					
					"迈巴赫S级":["2015款 4.0T 自动 S500四驱,benzapa","2015款 6.0T 自动 S600,benzapb","2015款 3.0T 自动 S400四驱,benzapc"],
		
//北汽制造

					"旋风":["2002款 2.2 手动 2代四驱,beiqiaa","2003款 2.2 手动 2代后驱,beiqiab","2005款 2.2 手动 金旋风四驱,beiqiac"],
		
					"旋风":["2007款 2.0 手动 金骑士四驱,beiqiba","2007款 2.2 手动 金骑士四驱,beiqibc","2007款 2.0 手动 金骑士后驱,beiqibd",
						   "2007款 2.0 手动 S12舒适型后驱,beiqibe","2007款 2.0 手动 S12豪华型后驱,beiqibf","2007款 2.2 手动 金骑士后驱,beiqibg",
						   "2008款 2.0 手动 金骑士四驱,beiqibh","2008款 2.2 手动 金骑士四驱,beiqibi","2008款 2.2 手动 S12豪华型四驱,beiqibj",
						   "2009款 2.2 手动 金骑士后驱,beiqibk","2009款 2.2 手动 金骑士四驱,beiqibl","2010款 2.0 手动 金骑士四驱,beiqibm",
						    "2011款 2.0 手动 S12舒适型后驱,beiqibn","2011款 2.0 手动 S12豪华型后驱,beiqibo","2011款 2.2 手动 S12豪华型四驱,beiqibp"
							],

					"陆霸":["2003款 2.7 手动 四驱,beiqicx","2003款 3.0 手动 后驱,beiqicy","2003款 3.0 手动 豪华型四驱,beiqicz","2003款 3.4 自动 四驱,beiqicaa",
							"2004款 2.4 手动 豪华型后驱,beiqicu","2004款 2.4 手动 四驱,beiqicv","2004款 2.4 手动 标准型后驱,beiqicw",
							"2005款 3.0 手动 豪华型四驱,beiqict","2006款 2.4 手动 四驱,beiqicl",
							"2006款 3.4 自动 四驱,beiqicm","2006款 3.2 手动 豪华型四驱,beiqicn","2006款 2.7 手动 标准型后驱,beiqico","2006款 2.7 手动 标准型四驱,beiqicp",
							"2006款 2.4 手动 豪华型后驱,beiqicq","2006款 2.4 手动 标准型后驱,beiqicr","2006款 3.0 手动 豪华型四驱,beiqics","2007款 2.7 手动 标准型四驱,beiqici",
							"2007款 2.4 手动 标准型后驱,beiqicj","2007款 2.4 手动 标准型四驱,beiqick","2007款 3.4 自动 四驱,beiqicg","2007款 2.7 手动 标准型后驱,beiqich","2008款 3.4 自动 四驱,beiqicb","2008款 2.7 手动 标准型后驱,beiqicc","2008款 2.7 手动 标准型四驱,beiqicd",
							"2008款 2.4 手动 标准型后驱,beiqice","2008款 2.4 手动 标准型四驱,beiqicf","2009款 3.0 手动 豪华型四驱,beiqica"
							//下一个数据应该是开始//beiqicab
							],
							
					"战旗":[	"2000款 2.2 手动 加长加宽硬顶型四驱,beiqidj","2000款 2.8T 手动 四驱 柴油,beiqidk","2000款 2.8T 手动 柴油,beiqidl","2003款 2.8T 手动 四驱 柴油,beiqidi",
							"2005款 2.2 手动 软顶四驱,beiqidh","2006款 2.8T 手动 四驱 柴油,beiqidg","2007款 2.2 手动 软顶四驱,beiqidf","2008款 2.2 手动 2023四驱,beiqide",
							"2008款 2.0 手动 2023四驱,beiqidd","2010款 2.0 手动 加长加宽硬顶型四驱,beiqidc","2011款 2.0 手动 CK四驱,beiqidb","2013款 2.2 手动 铁双排四驱,beiqida"
							//下一个数据应该是beiqidm开始//
							],	
							
					"雷驰":["2005款 2.4 手动 标准型短轴后驱,beiqiea","2005款 2.4 手动 豪华型短轴后驱,beiqieb","2005款 2.4 手动 标准型长轴后驱,beiqiec","2005款 2.4 手动 豪华型长轴后驱,beiqied",
							"2005款 2.2 手动 标准型长轴后驱,beiqiee","2005款 2.2 手动 豪华型长轴后驱,beiqief","2005款 2.2 手动 标准型短轴后驱,beiqieg","2005款 2.2 手动 豪华型短轴后驱,beiqieh",
							"2005款 2.2 手动 标准型短轴四驱,beiqiei","2005款 2.2 手动 豪华型短轴四驱,beiqiej","2005款 2.2 手动 标准型长轴四驱,beiqiek","2005款 2.2 手动 豪华型长轴四驱,beiqiel",
							"2006款 2.4 手动 标准型短轴后驱,beiqiem","2006款 2.4 手动 豪华型短轴后驱,beiqien","2006款 2.0 手动 标准型长轴四驱,beiqieo","2006款 2.0 手动 豪华型长轴四驱,beiqiep"
							],		
					
					"陆铃":["2006款 2.2 手动 豪华型,beiqifa","2006款 2.2 手动 标准型,beiqifb","2008款 2.2 手动 标准型,beiqifc",
						   "2008款 2.2 手动 豪华型,beiqifd","2010款 2.2 手动 标准型低顶后驱,beiqife","2010款 2.2 手动 经济型低顶后驱,beiqiff",
						   "2010款 2.2 手动 标准型高顶后驱,beiqifg"
						   ],
						   
					"御虎":["2003款 2.2 手动 经济型,beiqiga","2003款 2.2 手动 豪华型,beiqigb","2003款 2.2 手动 标准型,beiqigc",
						   "2003款 2.2 手动 经济型高顶,beiqigd","2003款 2.2 手动 豪华型高顶,beiqige","2003款 2.2 手动 标准型高顶,beiqigf"
						   ],
						   
					"京城海狮":["2006款 2.7 手动 9座 柴油,beiqiha","2006款 2.2 手动 标准型中顶,beiqihb","2006款 2.2 手动 豪华型中顶,beiqihc",
							"2006款 2.2 手动 舒适型中顶,beiqihd","2006款 2.0 手动 9座,beiqihe","2006款 2.2 手动 9座 柴油,beiqihf","2006款 2.2 手动 舒适型高顶11座,beiqihg",
							"2006款 2.2 手动 豪华型高顶11座,beiqihh","2006款 2.2 手动 标准型高顶11座,beiqihi","2006款 2.2 手动 豪华型高顶,beiqihj","2006款 2.2 手动 舒适型高顶,beiqihk",
							"2006款 2.2 手动 舒适型,beiqihl","2006款 2.2 手动 豪华型,beiqihm","2006款 2.2 手动 标准型高顶,beiqihn","2006款 2.2 手动 标准型,beiqiho",
							"2008款 2.2 手动 标准型,beiqihp"
							],	
							
					"勇士":["2007款 3.0T 手动 三门四驱 柴油,beiqiia","2008款 3.0T 手动 三门四驱 柴油,beiqiib","2008款 3.0T 手动 五门四驱 柴油,beiqiic",
						   "2008款 2.7 手动 五门四驱,beiqiid","2008款 2.7 手动 三门四驱 ,beiqiie","2014款 2.5T 手动 三门四驱 柴油,beiqiif",
						   "2014款 2.5T 手动 五门四驱 柴油,beiqiig"
						   ],
					
					"域胜007":["2011款 2.4 手动 运动版舒适型后驱,beiqija","2011款 2.4 手动 运动版豪华型后驱,beiqijb","2011款 2.4 手动 精英版舒适型四驱,beiqijc",
						   "2011款 2.4 手动 精英版豪华型四驱,beiqijd","2011款 2.0 手动 都市版豪华型后驱 ,beiqije","2011款 2.0 手动 都市版舒适型后驱,beiqijf",
						   "2012款 2.5T 手动 运动版舒适型后驱 柴油,beiqijg","2012款 2.5T 手动 运动版豪华型后驱 柴油,beiqiji","2012款 2.5T 手动 精英版舒适型四驱 柴油,beiqijj",
						   "2012款 2.5T 手动 精英版豪华型四驱 柴油,beiqijk"
						   ],
						   
					"北京212":["2006款 2.0 手动,beiqika","2006款 2.2 手动,beiqikb","2007款 2.0 手动,beiqikc","2012款 2.0 手动,beiqikd"],	   
					
					"角斗士":["2005款 2.2 手动,beiqila","2006款 2.0 手动,beiqilb","2006款 2.2 手动,beiqilc",
						   "2007款 2.0 手动,beiqild","2007款 2.2 手动 ,beiqile","2008款 2.0 手动,beiqilf",
						   "2008款 2.2 手动,beiqilg","2009款 2.0 手动,beiqili","2009款 2.2 手动,beiqilj",
						   "2010款 2.0 手动,beiqilk","2010款 2.2 手动,beiqill"
						   ],
					
					"越铃":["2012款 2.8T 手动 基本型后驱 柴油,beiqima","2013款 2.5T 手动 豪华型四驱R425 柴油,beiqimb","2013款 2.5 手动 豪华型四驱G4CA,beiqimc",
							"2013款 2.5 手动 舒适型四驱G4CA,beiqimd","2013款 2.5T 手动 标准型两驱YC4FB90P40C 柴油,beiqime","2013款 2.2 手动 豪华型两驱4G22D4,beiqimf",
							"2013款 2.2 手动 经济型两驱4G22D4,beiqimg","2013款 2.2 手动 标准型两驱4G22B,beiqimh","2013款 2.2 手动 标准型两驱4G22D4,beiqimi",
							"2014款 2.5T 手动 标准型柴油后驱,beiqimj","2014款 2.8T 手动 加长型后驱 柴油,beiqimk","2014款 2.2 手动 豪华型加长后驱,beiqiml",
							"2014款 2.5T 手动 标准型柴油后驱 柴油,beiqimm","2014款 2.5T 手动 标准型加长后驱 柴油,beiqimn","2015款 2.2 手动 创业版后驱,beiqimo",
							"2015款 2.2 手动 财富版后驱,beiqimp","2015款 2.5T 手动 创业版后驱 柴油,beiqimq",
							"2015款 2.5T 手动 财富版后驱 柴油,beiqimr","2015款 2.8T 手动 短轴创业版后驱 柴油,beiqims","2015款 2.8T 手动 短轴财富版后驱 柴油,beiqimt",
							"2015款 2.8T 手动 长轴财富版后驱 柴油,beiqimu","2015款 2.8T 手动 长轴创业版后驱 柴油,beiqimv"
							],
							
					"锐铃":["2013款 2.2 手动 经济型两驱,beiqina","2013款 2.2 手动 豪华型两驱,beiqinb","2013款 2.5 手动 豪华型四驱,beiqinc",
						   "2013款 2.4 手动 舒适型四驱,beiqind","2013款 2.5T 手动 豪华型四驱 柴油 ,beiqine","2014款 2.8T 手动 精英版后驱 柴油,beiqinf",
						   "2014款 2.8T 手动 尊贵版后驱 柴油,beiqing","2014款 2.8T 手动 尊贵版四驱 柴油,beiqini","2014款 2.8T 手动 精英版四驱 柴油,beiqinj"
						   ],
						   
					"北京BW007":["2015款 2.0 手动 舒适版后驱,beiqioa","2015款 2.0T 手动 舒适版后驱,beiqiob","2015款 2.0T 手动 豪华版四驱,beiqioc",
						   "2015款 2.0T 手动 豪华版后驱,beiqiod"
						   ],	   

//本田            			 	
					"思域":["2000款 1.7 自动 (131HP),bentianaaj","2003款 1.7 自动 (116HP),bentianaak","2006款 1.3 自动 IMA油电混合,bentianaal","2007款 1.3 自动 IMA油电混合,bentianaam",
							"2006款 1.8 自动 豪华版Vti,bentianaac","2006款 1.8 自动 经典版Exi,bentianaad",
							"2006款 1.8 手动 尊贵版VTi,bentianaae","2006款 1.8 手动 尊贵版Vti-S,bentianaaf","2006款 1.8 自动 尊贵版Vti,bentianaag","2006款 1.8 手动 豪华版Vti,bentianaah",
							"2006款 1.8 手动 经典版Exi,bentianaai","2008款 1.8 手动 五周年纪念版Vti,bentianay","2008款 1.8 手动 五周年纪念版Exi,bentianaz",
							"2008款 1.8 自动 五周年纪念版Vti,bentianaaa","2008款 1.8 自动 五周年纪念版Exi,bentianaab","2009款 1.8 自动 尊贵型Vti-S,bentianar",
							"2009款 1.8 自动 豪华型Vti,bentianas","2009款 1.8 手动 豪华型Vti,bentianat","2009款 1.8 自动 舒适型Exi,bentianau","2009款 1.8 手动 舒适型Exi,bentianav",
							"2009款 1.8 自动 经典型Lxi,bentianaw","2009款 1.8 手动 经典型Lxi,bentianax","2011款 1.8 手动 精致升华型Exi,bentianap","2011款 1.8 自动 精致升华型Exi,bentianaq",
							"2012款 1.8 自动 豪华导航版Vti,bentianaj","2012款 1.8 自动 豪华版Vti,bentianak","2012款 2.0 自动 导航版TYPE-S,bentianal","2012款 2.0 自动 TYPE-S,bentianam","2012款 1.8 手动 舒适版Exi,bentianan",
							"2012款 1.8 自动 舒适版Exi,bentianao","2013款 1.8 自动 十周年纪念豪华版Vti,bentianag","2013款 1.8 手动 十周年纪念舒适版Exi,bentianah","2013款 1.8 自动 十周年纪念舒适版Exi,bentianai",
							"2014款 2.4 手动 Si,bentianaa","2014款 1.8 自动 豪华版Vti,bentianab","2014款 1.8 自动 舒适版Exi,bentianac","2014款 1.8 手动 舒适版Exi,bentianad",
							"2014款 1.8 自动 经典版Lxi,bentianae","2014款 1.8 手动 经典版Lxi,bentianaf"
							//下一个数据应该是bentianaan开始//
							],
       

					"思迪":["2006款 1.5 自动 豪华型,bentianba","2006款 1.5 自动 标准型,bentianbb","2006款 1.5 手动 标准型,bentianbc","2006款 1.5 手动 舒适型,bentianbd",
							"2006款 1.5 自动 舒适型,bentianbe","2006款 1.3 手动 标准型,bentianbf","2006款 1.3 手动 普通型,bentianbg","2007款 1.5 自动 舒适型,bentianbh",
							"2006款 1.5 手动 普通型,bentianbi","2007款 1.5 自动 豪华型,bentianbj","2007款 1.5 手动 舒适型,bentianbk","2007款 1.5 手动 标准型,bentianbl",
							"2007款 1.3 手动 标准型,bentianbm"
							],
							
					"CR-V":["2002款 2.0 自动,bentianc02a","2005款 2.0 自动 iVTEC四驱,bentianca","2005款 2.0 手动 iVTEC四驱,bentiancb","2005款 2.4 手动 iVTEC四驱,bentiancc",
							"2005款 2.4 自动 iVTEC四驱,bentiancd","2004款 2.0 自动 iVTEC四驱,bentiance","2006款 2.4 自动 iVTEC导航版四驱,bentiancf",
							"2006款 2.4 自动 iVTEC四驱,bentiancg","2006款 2.0 自动 iVTEC四驱,bentianch","2005款 2.4 自动 iVTEC导航版四驱,bentianci",
							"2007款 2.0 自动 Lxi都市版前驱,bentiancj","2007款 2.0 自动 iVTEC四驱,bentianck","2007款 2.4 手动 VTi豪华型四驱,bentiancl",
							"2007款 2.0 手动 Exi经典型四驱,bentiancm","2007款 2.4 自动 VTi尊贵型四驱,bentiancn","2007款 2.4 自动 VTi豪华型四驱,bentianco",
							"2007款 2.0 自动 Exi经典型四驱,bentiancp","2008款 2.0 手动 Lxi都市版前驱,bentiancq","2008款 2.0 自动 Lxi都市版前驱,bentiancr",
							"2010款 2.4 自动 VTi尊贵型四驱,bentiancs","2010款 2.4 自动 VTi尊贵导航型四驱,bentianct","2010款 2.4 自动 VTi豪华型四驱,bentiancu",
							"2010款 2.0 手动 Lxi都市型前驱,bentiancv","2010款 2.0 自动 Lxi都市型前驱,bentiancw","2010款 2.0 自动 Exi经典型四驱,bentiancx",
							"2012款 2.4 自动 VTi尊贵导航型四驱,bentiancy","2012款 2.4 自动 VTi尊贵型四驱,bentiancz","2012款 2.4 自动 VTi豪华型四驱,bentiancaa",
							"2012款 2.0 自动 Exi经典型四驱,bentiancab","2012款 2.0 自动 Lxi都市型前驱,bentiancac","2013款 2.4 自动 VTi豪华型前驱,bentiancad",
							"2013款 2.0 自动 Exi经典型前驱,bentiancae","2015款 2.0 自动 经典版前驱,bentiancaf","2015款 2.4 自动 尊贵版四驱,bentiancag",
							"2015款 2.4 自动 豪华版四驱,bentiancah","2015款 2.4 自动 豪华版前驱,bentiancai","2015款 2.0 自动 风尚版四驱,bentiancaj",
							"2015款 2.0 自动 风尚版前驱,bentiancak","2015款 2.0 自动 都市版前驱,bentiancal"
							],
					
					
					"奥德赛":["2002款 2.3 自动 舒适型,bentiandat","2002款 2.3 自动 豪华型,bentiandau","2005款 2.4 自动 豪华型,bentiandap",
							"2005款 2.4 自动 普通型,bentiandaq","2005款 2.4 自动 舒适型,bentiandar","2005款 2.4 自动 标准型,bentiandas",
							"2006款 2.4 自动 舒适天窗型,bentiandan","2006款 2.4 自动 豪华导航型,bentiandao","2007款 2.4 自动 舒适型,bentiandaj",
							"2007款 2.4 自动 普通型,bentiandak","2007款 2.4 自动 豪华型,bentiandal","2007款 2.4 自动 标准型,bentiandam",
							"2008款 2.4 自动 舒适型,bentiandaf","2008款 2.4 自动 豪华型,bentiandag","2008款 2.4 自动 普通型,bentiandah",
							"2008款 2.4 自动 标准型,bentiandai","2009款 2.4 自动 舒适型,bentiandac","2009款 2.4 自动 领秀型,bentiandad",
							"2009款 2.4 自动 豪华型,bentiandae","2011款 2.4 自动 劲秀领秀版,bentiandz","2011款 2.4 自动 劲秀豪华版,bentiandaa",
							"2011款 2.4 自动 劲秀舒适版,bentiandab","2013款 2.4 自动 舒适版,bentiandr",
							"2013款 2.4 自动 精英版,bentiands","2013款 2.4 自动 领秀版,bentiandt","2013款 2.4 自动 智能型,bentiandu",
							"2013款 2.4 自动 运动版,bentiandv","2013款 2.4 自动 豪华版,bentiandw","2013款 2.4 自动 明鉴版,bentiandx",
							"2013款 2.4 自动 明鉴领秀版,bentiandy","2014款 2.4 自动 运动版,bentiandk","2014款 2.4 自动 舒适版,bentiandl",
							"2014款 2.4 自动 明鉴领秀版,bentiandm","2014款 2.4 自动 明鉴版,bentiandn","2014款 2.4 自动 领秀版,bentiando",
							"2014款 2.4 自动 精英版,bentiandp","2014款 2.4 自动 豪华版,bentiandq","2015款 2.4 自动 至尊版改款,bentianda",
							"2015款 2.4 自动 智酷版改款,bentiandb","2015款 2.4 自动 智享版改款,bentiandc",
							"2015款 2.4 自动 豪华版改款,bentiandd","2015款 2.4 自动 舒适版改款,bentiande","2015款 2.4 自动 至尊版,bentiandf",
							"2015款 2.4 自动 尊享版,bentiandg","2015款 2.4 自动 智享版,bentiandh","2015款 2.4 自动 豪华版,bentiandi",
							"2015款 2.4 自动 舒适版,bentiandj"
							//下一个数据应该是bentiandav开始//
							],
							
					"飞度":["2003款 1.3 自动,bentianeal","2003款 1.3 手动,bentianeam","2004款 1.5 自动,bentianean","2004款 1.5 手动,bentianeao","2004款 1.5 手动,bentianea",
					        "2004款 1.3 手动,bentianeb","2004款 1.3 自动,bentianec","2004款 1.5 自动,bentianed","2006款 1.3 自动,bentianeap","2006款 1.5 自动,bentianeaq",
							"2006款 1.5 手动 标准型行动派,bentianee","2006款 1.5 自动 舒适型行动派,bentianef","2006款 1.3 手动 标准型行动派,bentianeg",
							"2006款 1.3 自动 舒适型行动派,bentianeh","2006款 1.5 手动 普通型行动派,bentianei","2007款 1.5 手动 标准型,bentianej",
							"2007款 1.3 手动 标准型,bentianek","2007款 1.5 自动 舒适型,bentianel","2007款 1.3 自动 舒适型,bentianem",
							"2008款 1.5 手动 标准型行动派,bentianen","2008款 1.5 自动 舒适型行动派,bentianeo","2008款 1.5 手动 豪华型,bentianep",
							"2008款 1.5 自动 豪华型,bentianeq","2008款 1.5 自动 炫酷运动型,bentianer","2008款 1.3 手动 舒适型,bentianes","2008款 1.3 手动 标准型行动派,bentianet",
							"2008款 1.3 自动 舒适型,bentianeu","2008款 1.3 自动 舒适型行动派,bentianev","2011款 1.5 手动 豪华型,bentianew","2011款 1.3 手动 舒适型,bentianex","2011款 1.3 自动 舒适型,bentianey",
							"2011款 1.5 自动 全景天窗版,bentianez","2011款 1.5 自动 豪华型,bentianeaa","2013款 1.3 自动 荷兰橙色舒适型,bentianeab",
							"2013款 1.5 自动 荷兰橙色全景天窗型,bentianeac","2013款 1.5 自动 荷兰橙色豪华型,bentianead","2013款 1.3 自动 油电混合,bentianear",
							"2013款 1.5 手动 荷兰橙色豪华型,bentianeae","2013款 1.3 手动 荷兰橙色舒适型,bentianeaf","2014款 1.5 自动 领先版,bentianeag",
							"2014款 1.5 自动 精英版,bentianeah","2014款 1.5 自动 时尚版,bentianeai",
							"2014款 1.5 自动 舒适版,bentianeaj","2014款 1.5 手动 舒适版,bentianeak"
							//下一个数据应该是bentianeas开始//
							],
	
					"雅阁":["2000款 2.3 自动,bentianfa","2001款 2.3 自动,bentianfb","2001款 2.0 自动,bentianfc","2001款 3.0 自动,bentianfd","2002款 2.3 自动,bentianfe",
							"2003款 3.0 自动,bentianff","2003款 2.4 自动,bentianfg",
							"2004款 2.4L,bentianfh","2004款 2.0L,bentianfi","2005款 2.0L 标准版,bentianfj","2005款 2.0L 舒适版,bentianfk","2005款 2.4L 标准版,bentianfl",
							"2005款 2.4L 加热版,bentianfm","2006款 2.4L 豪华版,bentianfn","2006款 3.0L V6豪华版,bentianfo","2006款 2.0L 普通版,bentianfp",
							"2006款 2.0L 标准版,bentianfq","2006款 2.0L 舒适版,bentianfr","2006款 2.4L 标准版,bentianfs","2006款 2.4L 加热版,bentianft",
							"2007款 2.4L 自动豪华版,bentianfu","2007款 2.0L 手动普通版,bentianfv","2007款 2.0L 自动标准版,bentianfw",
							"2007款 2.0L 自动舒适版,bentianfx","2007款 2.4L 自动舒适版,bentianfy","2007款 2.0L 手动普通精典版,bentianfz",
							"2007款 2.0L 自动舒适精典版,bentianfaa","2007款 2.4L 自动舒适精典版,bentianfab","2007款 2.4L 自动豪华精典版,bentianfac",
							"2008款 2.0L EX,bentianfad","2008款 2.4L EX,bentianfae","2008款 3.5L AT,bentianfaf","2008款 2.4L EXL Navi,bentianfag",
							"2008款 2.0L MT,bentianfah","2008款 2.0L EX Navi,bentianfai","2008款 2.4L EX Navi,bentianfaj","2009款 2.4L LX,bentianfak",
							"2010款 2.0L MT,bentianfal","2010款 2.0L EX,bentianfam","2010款 2.0L EX Navi,bentianfan","2010款 2.4L LX,bentianfao",
							"2010款 2.4L 百万纪念版,bentianfap","2010款 2.4L EX Navi,bentianfaq","2010款 2.4L EXL Navi,bentianfar",
							"2010款 3.5L V6,bentianfas","2011款 2.4L EXL Navi,bentianfat","2011款 2.4L EX Navi,bentianfau","2011款 2.4L EX,bentianfav",
							"2011款 2.4L LX,bentianfaw","2011款 2.0L EX Navi,bentianfax","2011款 2.0L EX,bentianfay","2011款 2.0L MT,bentianfaz",
							"2012款 2.0L SE,bentianfba","2012款 2.0L MT,bentianfbb","2012款 2.4L LX,bentianfbc","2012款 2.4L SE,bentianfbd",
							"2012款 2.4L EX Navi,bentianfbe","2013款 2.4L SE,bentianfbf","2013款 2.0L SE,bentianfbg","2013款 2.0L PE,bentianfbh",
							"2013款 2.4L LX,bentianfbi","2013款 2.4L PE,bentianfbj","2013款 2.0L LX,bentianfbk","2014款 2.4L EXN 豪华导航版,bentianfbl",
							"2014款 3.0L VTI 尊贵版,bentianfbm","2014款 2.4L EX 豪华版,bentianfbn","2014款 2.0L EX 豪华版,bentianfbo",
							"2014款 2.0L EXN 豪华导航版,bentianfbp","2014款 2.0L LX 舒适版,bentianfbq","2015款 2.0L LX 舒适版,bentianfbr",
							"2015款 2.0L LXS  精英版,bentianfbs","2015款 2.0L EX 豪华版,bentianfbt","2015款 2.4L EX 豪华版,bentianfbu"
							],
					
					"锋范":
							["2015款 1.5 自动 旗舰版,bentianga","2015款 1.5 手动 旗舰版,bentiangb","2015款 1.5 自动 豪华版,bentiangc","2015款 1.5 手动 豪华版,bentiangd",
							"2015款 1.5 自动 舒适版,bentiange","2015款 1.5 手动 舒适版,bentiangf","2015款 1.5 手动 进取版,bentiangg","2014款 1.5 自动 风尚精英版,bentiangh",
							"2014款 1.5 手动 风尚精英版,bentiangi","2012款 1.5 手动 舒适增配版,bentiangj","2012款 1.5 手动 精英型,bentiangk","2012款 1.8 自动 旗舰型,bentiangl",
							"2012款 1.8 自动 豪华型,bentiangm","2012款 1.8 自动 舒适型,bentiangn","2012款 1.5 自动 旗舰型,bentiango","2012款 1.5 手动 舒适型,bentiangp",
							"2012款 1.5 手动 精英增配版,bentiangq","2012款 1.5 自动 精英型,bentiangr","2011款 1.5 手动 精英品致款,bentiangs","2011款 1.5 自动 精英品致款,bentiangt",
							"2008款 1.8 自动 舒适型,bentiangu","2008款 1.8 自动 豪华型,bentiangv","2008款 1.5 手动 舒适型,bentiangw","2008款 1.5 手动 精英型,bentiangx","2008款 1.5 自动 精英型,bentiangy"
							],
							
					"Element":["2009款 2.4 自动 LX,bentianka","2009款 2.4 自动 EX,bentiankb"],
					
					"Insight":["2009款 1.3 自动 EX 油电混合,bentianma","2013款 1.3 自动,bentianmb"],

					"里程":["2002款 3.5 自动,bentianna","2002款 3.5 手动,bentiannb","2004款 3.5 自动合,bentiannc","2009款 3.7 自动,bentiannd"],
					
					"时韵":["2000款 2.0 自动 前驱,bentianoa","2000款 1.7 自动 前驱,bentianob","2004款 1.7 自动 前驱,bentianoc","2004款 2.0 自动 前驱,bentianod",
							"2006款 1.8 自动 RSZ前驱,bentianoe","2006款 1.8 自动 RSZ四驱,bentianof",
							"2006款 2.0 自动 RSZ四驱,bentianog","2006款 2.0 自动 RSZ前驱,bentianoh","2006款 2.0 自动 G四驱,bentianoi","2006款 2.0 自动 G前驱,bentianoj",
							"2006款 1.8 自动 X四驱,bentianok","2006款 1.8 自动 X前驱,bentianol","2007款 2.0 自动 G风尚版四驱,bentianom","2007款 2.0 自动 G风尚版前驱,bentianon",
							"2007款 1.8 自动 X风尚版四驱,bentianoo","2007款 1.8 自动 X风尚版前驱,bentianop","2009款 2.0 自动 ZS四驱,bentianoq",
							"2009款 2.0 自动 RST前驱,bentianor","2009款 1.8 自动 RST前驱,bentianos","2009款 1.8 自动 ZS前驱,bentianot",
							"2009款 2.0 自动 ZS前驱,bentianou","2009款 2.0 自动 Gi前驱,bentianov"
							],


					"思铂睿":["2015款 2.4 自动 尊耀版,bentianpa","2015款 2.4 自动 Si,bentianpb","2015款 2.4 自动 尊贵型,bentianpc","2015款 2.4 自动 豪华型,bentianpd",
							"2015款 2.0 自动 典藏型,bentianpe","2015款 2.0 自动 尊贵型,bentianpf","2015款 2.0 自动 豪华型,bentianpg","2013款 2.4 自动 TYPE-S,bentianph",
							"2013款 2.4 自动 尊贵型,bentianpi","2013款 2.4 自动 豪华型,bentianpj","2013款 2.0 自动 尊贵型,bentianpk","2013款 2.0 自动 豪华型,bentianpl",
							"2009款 2.4 自动 尊贵型,bentianpm","2009款 2.4 自动 尊贵导航型,bentianpn","2009款 2.4 自动 豪华型,bentianpo","2009款 2.4 自动 TYPE-S NAVI,bentianpp",
							"2009款 2.4 自动 TYPE-S,bentianpq"
							],

				
					"歌诗图":["2011款 3.5 自动 旗舰版,bentianra","2011款 3.5 自动 尊贵版,bentianrb","2012款 2.4 自动 豪华版,bentianrc",
							"2012款 2.4 自动 尊享版,bentianrd","2012款 3.5 自动 尊贵版,bentianre",
							"2012款 3.5 自动 旗舰版,bentianrf","2014款 2.4 自动 豪华版前驱,bentianrg","2014款 2.4 自动 豪华导航版前驱,bentianrh","2014款 3.0 自动 尊贵版四驱,bentianri",
							"2014款 3.0 自动 尊贵导航版四驱,bentianrj"
							],
							
							
					"CR-Z":["2010款 1.5 手动 VTEC IMA油电混合,bentiansa","2012款 1.5 自动 iVTEC 油电混合,bentiansb"
							],
							
					
					"理念":["2011款 1.3 自动 舒适版,bentianta","2011款 1.3 手动 舒适版,bentiantb","2011款 1.5 自动 豪华版,bentiantc",
							"2012款 1.3 自动 舒适版,bentiantd","2012款 1.3 手动 舒适版,bentiante",
							"2012款 1.5 自动 豪华版,bentiantf","2012款 1.5 手动 运动版,bentiantg","2012款 1.5 手动 舒适版,bentianth",
							"2013款 1.5 手动 舒适版,bentianti","2013款 1.5 手动 运动版,bentiantj","2013款 1.5 手动 豪华版,bentiantk","2013款 1.5 自动 舒适版,bentiantl",
							"2013款 1.5 自动 豪华版,bentiantm","2014款 1.3 自动 舒适版,bentiantn","2014款 1.3 手动 舒适版,bentianto"
							],
					
					
					"思铭":["2012款 1.8 手动,bentianwa","2012款 1.8 自动,bentianwb","2015款 1.8 手动 舒适版Exi,bentianwc",
							"2015款 1.8 自动 舒适版Exi,bentianwd","2015款 1.8 自动 豪华版Vti,bentianwe"
							],
					
					
					"艾力绅":["2012款 2.4 自动 舒适版,bentianxa","2012款 2.4 自动 豪华版,bentianxb","2012款 2.4 自动 尊贵版,bentianxc",
							"2012款 2.4 自动 豪华导航版,bentianxd","2012款 2.4 自动 尊贵导航版,bentianxe",
							"2015款 2.4 自动 舒适版Exi,bentianxf","2015款 2.4 自动 豪华版Vti,bentianxg","2015款 2.4 自动 尊贵版Vti-S,bentianxh"
							],
							
					"凌派":["2013款 1.8 手动 舒适版,bentianya","2013款 1.8 自动 舒适版,bentianyb","2013款 1.8 自动 豪华版,bentianyc",
							"2013款 1.8 自动 旗舰版,bentianyd","2015款 1.8 手动 舒适版,bentianye",
							"2015款 1.8 自动 舒适版,bentianyf","2015款 1.8 自动 豪华版,bentianyg","2015款 1.8 手动 豪华版,bentianyh",
							"2015款 1.8 自动 旗舰版,bentianyi"
							],
							
					"杰德":["2013款 1.8 自动 舒适型5座,bentianza","2013款 1.8 自动 舒适型6座,bentianzb","2013款 1.8 自动 豪华型5座,bentianzc",
							"2013款 1.8 自动 豪华型6座,bentianzd","2014款 1.8 自动 舒适精英版5座,bentianze",
							"2014款 1.8 自动 豪华版5座,bentianzf","2014款 1.8 自动 豪华尊享版5座,bentianzg"
							],
							
							
					"缤智":["2015款 1.8 手动 精英版前驱,bentianaaa","2015款 1.8 自动 精英版前驱,bentianaab","2015款 1.8 自动 豪华版前驱,bentianaac",
							"2015款 1.8 自动 旗舰版四驱,bentianaad","2015款 1.5 自动 舒适版前驱,bentianaae",
							"2015款 1.5 手动 舒适版前驱,bentianaaf"
							],
							
							
					"XR-V":["2015款 1.8 手动 EXi舒适版,bentianaba","2015款 1.8 自动 EXi舒适版,bentianabb","2015款 1.8 自动 VTi豪华版,bentianabc",
							"2015款 1.5 自动 LXi经典版,bentianabd","2015款 1.5 手动 LXi经典版,bentianabe"
							],
									
					"哥瑞":["2016款 1.5 手动 经典版,bentianaca","2016款 1.5 自动 经典版,bentianacb","2016款 1.5 自动 舒适版,bentianacc",
							"2016款 1.5 自动 风尚版,bentianacd","2016款 1.5 自动 豪华版,bentianace"
							],		

//北京汽车
							
							
					"E系列":["2012款 1.3 手动 乐活版,bjqcaa","2012款 1.3 手动 乐天版,bjqcab","2012款 1.3 手动 乐尚版,bjqcac","2012款 1.5 手动 乐尚版,bjqcad",
							"2012款 1.5 自动 乐尚版,bjqcae","2012款 1.5 自动 乐享版,bjqcaf","2013款 1.5 自动 乐天版,bjqcag","2013款 1.5 手动 乐天版,bjqcah",
							"2013款 1.3 手动 乐天版,bjqcai","2013款 1.5 手动 乐天版,bjqcaj","2013款 1.5 自动 乐天版,bjqcak","2013款 1.5 手动 乐尚版,bjqcal",
							"2013款 1.5 自动 乐尚版,bjqcam","2013款 1.5 自动 乐享版,bjqcan","2013款 1.3 手动 乐活版,bjqcao"
							],
					
					
					"威旺306":["2011款 1.3 手动 豪华型7座,bjqcba","2011款 1.3 手动 舒适型7座,bjqcbb","2011款 1.3 手动 基本型7座,bjqcbc",
							"2012款 1.3 手动 基本型8座,bjqcbd","2012款 1.3 手动 豪华型5座,bjqcbe","2012款 1.3 手动 基本型5座,bjqcbf",
							"2012款 1.3 手动 豪华型7座,bjqcbg","2012款 1.3 手动 基本型7座,bjqcbh","2012款 1.3 手动 舒适型7座,bjqcbi",
							"2013款 1.0 手动 基本型5座,bjqcbj","2013款 1.0 手动 创业型7座,bjqcbk","2013款 1.2 手动 基本型A12 CNG油气混合,bjqcbl",
							"2013款 1.2 手动 超值版舒适型A12,bjqcbm","2013款 1.2 手动 超值版基本型A12,bjqcbn","2013款 1.2 手动 超值版豪华型A12,bjqcbo",
							"2013款 1.3 手动 超值版豪华型8座,bjqcbp","2013款 1.3 手动 超值版舒适型7座,bjqcbq","2013款 1.3 手动 超值版基本型7座,bjqcbr",                  
							"2014款 1.3 手动 货箱标准型载重550,bjqcbs","2014款 1.3 手动 货箱基本型载重550,bjqcbt","2014款 1.3 手动 货箱舒适型载重550,bjqcbu",
							"2014款 1.3 手动 货箱舒适型载重350,bjqcbv","2014款 1.3 手动 货箱标准型载重350,bjqcbw","2014款 1.3 手动 货箱基本型载重350,bjqcbx",
							"2014款 1.2 手动 超值版厢货5座有助力舒适型,bjqcby","2014款 1.2 手动 超值版厢货5座基本型,bjqcbz","2014款 1.2 手动 超值版厢货5座有助力舒适型 油气混合,bjqcbaa",
							"2014款 1.2 手动 超值版厢货5座舒适型,bjqcbab","2014款 1.2 手动 超值版厢货2座有助力舒适型,bjqcbac","2014款 1.2 手动 超值版厢货2座舒适型,bjqcbad",
							"2014款 1.2 手动 超值版厢货5座有助力基本型,bjqcbae","2014款 1.2 手动 超值版8座基本型 油气混合,bjqcbaf","2014款 1.2 手动 超值版厢货2座有助力基本型,bjqcbag",
							"2014款 1.2 手动 超值版厢货2座基本型,bjqcbah"
							],
							
					"威旺205":["2013款 1.0 手动 旺业型,bjqcca","2013款 1.0 手动 兴业型,bjqccb","2013款 1.0 手动 乐业型,bjqccc"],		
							
					"威旺M20":["2013款 1.5 手动 尊尚型BJ415B发动机,bjqcda","2013款 1.5 手动 实用型BJ415A发动机,bjqcdb","2013款 1.5 手动 品尚型BJ415B发动机,bjqcdc",
							"2013款 1.5 手动 乐尚型BJ415B发动机,bjqcdd","2013款 1.5 手动 时尚型BJ415B发动机,bjqcde","2013款 1.5 手动 实力型BJ415A发动机,bjqcdf",
							"2014款 1.5 手动 舒适型DAM15DL,bjqcdg","2014款 1.5 手动 标准型DAM15DL,bjqcdh","2014款 1.5 手动 基本型DAM15DL,bjqcdi",
							"2014款 1.5 手动 基本型7座,bjqcdj","2014款 1.5 手动 实用型BJ415A发动机,bjqcdk","2014款 1.5 手动 舒适型8座,bjqcdl",
							"2014款 1.5 手动 超豪华型7座,bjqcdm","2014款 1.5 手动 实用型BJ415B发动机,bjqcdn","2014款 1.5 手动 实力型BJ415B发动机,bjqcdo",
							"2014款 1.5 手动 舒适型7座,bjqcdp","2014款 1.5 手动 豪华型7座,bjqcdq","2014款 1.5 手动 尊尚型BJ415A发动机,bjqcdr",
							"2014款 1.5 手动 实用型7座,bjqcds","2014款 1.5 手动 标准型7座,bjqcdt","2014款 1.5 手动 品尚型BJ415A发动机,bjqcdu",
							"2014款 1.5 手动 乐尚型BJ415A发动机,bjqcdv","2014款 1.5 手动 尊尚型,bjqcdw","2014款 1.5 手动 乐尚型,bjqcdx",
							"2014款 1.5 手动 品尚型,bjqcdy","2014款 1.5 手动 时尚型,bjqcdz","2014款 1.5 手动 实力型,bjqcdaa","2014款 1.5 手动 实用型,bjqcdab",
							"2015款 1.2 手动 经济型空调A12 7座,bjqcdac","2014款 1.5 手动 超豪华型DAM15DL,bjqcdad","2014款 1.5 手动 豪华型DAM15DL,bjqcdae"
							],
												
					
					"威旺206":["2013款 1.3 手动 加长旺业型,bjqcea","2013款 1.3 手动 加长兴业型,bjqceb","2013款 1.3 手动 加长乐业型,bjqcec",
							  "2013款 1.0 手动 加长旺业型,bjqced",
							  "2013款 1.0 手动 加长兴业型,bjqcee","2013款 1.0 手动 加长乐业型,bjqcef"
							],
							
							
					"威旺307":["2014款 1.2 手动 舒适型5座A12,bjqcfa","2014款 1.2 手动 豪华型5座A12,bjqcfb","2014款 1.2 手动 标准型5座A12,bjqcfc",
							"2014款 1.2 手动 豪华型7座A12,bjqcfd","2014款 1.2 手动 舒适型7座A12,bjqcfe","2014款 1.2 手动 标准型7座A12,bjqcff",
							"2014款 1.2 手动 旺业型7座,bjqcfg","2014款 1.2 手动 兴业型7座,bjqcfh","2014款 1.2 手动 乐业型7座,bjqcfi","2013款 1.2 手动 乐业型8座,bjqcfj"
							],
							
					"威旺T205-D":["2013款 1.3 手动 加长版,bjqcga","2013款 1.0 手动 加长版,bjqcgb","2013款 1.0 手动 标准版,bjqcgc"],		
							
					"威旺007":["2015款 2.0 手动 舒适型,bjqcha","2015款 2.0T 手动 豪华型,bjqchb"],
					
					"威旺M30":["2015款 1.5 手动 舒适型DAM15,bjqcia","2015款 1.5 手动 基本型DAM15,bjqcib","2015款 1.5 手动 舒适型BJ415B,bjqcic","2015款 1.5 手动 基本型BJ415B,bjqcid"],
					
					"北京40":["2014款 2.4 手动 穿越版四驱,bjqcja","2014款 2.4 手动 征途版四驱,bjqcjb","2014款 2.4 手动 酷野版四驱,bjqcjc",
							"2014款 2.4 手动 拓疆版四驱,bjqcjd","2015款 2.4 手动 远行版四驱,bjqcje"
							],
					
					"E150EV":["2012款 单速版 纯电动,bjqcka","2014款 时尚版 纯电动,bjqckb","2014款 科技版 纯电动,bjqckc"],
					
					"EV200":["2015款 轻秀版 纯电动,bjqcla","2015款 轻享版 纯电动,bjqclb","2015款 轻快版 纯电动,bjqclc"],
					
					"ES210":["2015款 畅睿版 纯电动,bjqcma"],
					
					"EV160":["2015款 轻秀版 纯电动,bjqcna","2015款 轻快版 纯电动,bjqcnb"],
					
					"绅宝D70":["2013款 2.3T 自动 精英版,bjqcoa","2013款 2.3T 自动 豪华版,bjqcob","2013款 2.0T 自动 豪华版,bjqcoc",
							"2013款 2.0T 自动 精英版,bjqcod","2013款 2.0T 自动 舒适版,bjqcoe","2013款 2.0T 自动 时尚版,bjqcof",
							"2013款 1.8T 自动 行政版,bjqcog","2013款 1.8T 自动 政务版,bjqcoh"
							], 
												
					"绅宝D50":["2014款 1.5 自动 豪华版,bjqcpa","2014款 1.5 自动 标准版,bjqcpb","2014款 1.5 手动 豪华版,bjqcpc",
							"2014款 1.5 手动 精英版,bjqcpd","2014款 1.5 手动 舒适版,bjqcpe","2014款 1.5 手动 标准版,bjqcpf",
							"2015款 1.5 自动 标准超值导航版,bjqcpg","2015款 1.5 手动 舒适超值导航版,bjqcph"
							],
							
					"绅宝D60":["2014款 1.8T 自动 豪华型,bjqcqa","2014款 1.8T 自动 精英型,bjqcqb","2014款 1.8T 自动 舒适型,bjqcqc",
							  "2014款 1.8T 手动 精英型,bjqcqd","2014款 1.8T 手动 舒适型,bjqcqe"
							],
					
					
					"绅宝D20":["2015款 1.5 自动 CROSS乐尚版,bjqcra","2015款 1.5 手动 CROSS乐尚版,bjqcrb","2015款 1.3 手动 CROSS天窗版,bjqcrc","2015款 1.5 自动 乐享版,bjqcrd",
							"2015款 1.5 自动 乐尚版,bjqcre","2015款 1.5 自动 乐天版,bjqcrf","2015款 1.5 手动 天窗版,bjqcrg","2015款 1.5 手动 乐尚版,bjqcrh",
							"2015款 1.5 手动 乐天版,bjqcri","2015款 1.3 手动 天窗版,bjqcrj","2015款 1.3 手动 乐天版,bjqcrk","2015款 1.3 手动 乐活版,bjqcrl",
							"2015款 1.5 手动 天窗版,bjqcrm","2015款 1.5 手动 乐天版,bjqcrn","2015款 1.5 手动 乐尚版,bjqcro","2015款 1.5 自动 乐享版,bjqcrp",
							"2015款 1.5 自动 乐天版,bjqcrq","2015款 1.5 自动 乐尚版,bjqcrr","2015款 1.3 手动 天窗版,bjqcrs","2015款 1.3 手动 乐天版,bjqcrt",
							"2015款 1.3 手动 乐活版,bjqcru"
							],
					
					"绅宝X65":["2015款 2.0T 手动 舒适版,bjqcsa","2015款 2.0T 手动 精英版,bjqcsb","2015款 2.0T 手动 豪华版,bjqcsc",
							  "2015款 2.0T 自动 舒适版,bjqcsd","2015款 2.0T 自动 精英版,bjqcse","2015款 2.0T 自动 豪华版,bjqcsf"
							],
					
					
					"绅宝CC":["2015款 2.0T 自动 豪华版,bjqcta","2015款 1.8T 自动 精英版,bjqctb","2015款 1.8T 手动 精英版,bjqctc",
							"2015款 1.8T 自动 舒适版,bjqctd","2015款 1.8T 手动 舒适版,bjqcte"
							],
					
					"绅宝D80":["2015款 2.8T 自动 豪华版,bjqcua","2015款 2.0T 自动 精英版,bjqcub","2015款 1.8T 自动 舒适版,bjqcuc"
							],
					
//奔腾
					"B70":["2006款 2.3 自动 豪华型,bentengaa","2006款 2.3 自动 导航型,bentengab","2006款 2.0 自动 舒适型,bentengac",
							"2006款 2.0 自动 时尚型,bentengad","2006款 2.0 自动 豪华型,bentengae","2006款 2.0 自动 导航型,bentengaf",
							"2006款 2.0 自动 尊贵型,bentengag","2006款 2.3 自动 尊贵型,bentengah","2007款 2.0 自动 豪华型,bentengai",
							"2007款 2.0 自动 舒适型,bentengaj","2007款 2.3 自动 豪华型,bentengak","2007款 2.0 手动 舒适型,bentengal",
							"2007款 2.0 手动 豪华型,bentengam","2007款 2.3 自动 导航型,bentengan","2007款 2.3 自动 豪华型,bentengao",
							"2008款 2.3 自动 导航型,bentengap","2008款 2.0 自动 舒适型,bentengaq","2008款 2.0 自动 豪华型,bentengar",
							"2009款 2.0 自动 特装版,bentengas","2009款 2.0 手动 特装版,bentengat","2009款 2.0 自动 贺岁版,bentengau",
							"2009款 2.3 自动 导航型,bentengav","2009款 2.3 自动 豪华型,bentengaw","2009款 2.0 自动 导航型,bentengax",
							"2009款 2.0 自动 豪华型,bentengay","2009款 2.0 手动 精英型,bentengaz","2010款 2.0 手动 时尚型,bentengaaa",
							"2010款 2.0 自动 时尚型,bentengaab","2010款 2.3 自动 豪华型,bentengaac","2010款 2.3 自动 导航型,bentengaad",
							"2010款 2.0 手动 舒适型,bentengaae","2010款 2.0 手动 精英型,bentengaaf","2010款 2.0 自动 导航型,bentengaag",
							"2010款 2.0 自动 豪华型,bentengaah","2010款 2.0 自动 舒适型,bentengaai","2011款 2.3 自动 豪华型,bentengaaj",
							"2011款 2.3 自动 导航型,bentengaak","2011款 2.0 手动 时尚型,bentengaal","2011款 2.0 手动 精英型,bentengaam",
							"2011款 2.0 自动 智领型,bentengaan","2011款 2.0 自动 时尚型,bentengaao","2011款 2.0 自动 豪华型,bentengaap",
							"2011款 1.8 自动 时尚型,bentengaaq","2011款 1.8 手动 舒适型,bentengaas","2011款 2.3 自动 智领型,bentengaat",
							"2012款 1.8 自动 豪华型,bentengaau","2012款 1.8 自动 时尚型,bentengaav","2012款 1.8 手动 豪华型,bentengaaw",
							"2012款 1.8 手动 时尚型,bentengaax","2012款 1.8 手动 舒适型,bentengaay","2014款 1.8T 自动 运动尊贵型,bentengaaz",
							"2014款 1.8T 自动 运动豪华型,bentengaba","2014款 2.0 自动 豪华型,bentengabb","2014款 2.0 自动 舒适型,bentengabc",
							"2014款 2.0 手动 豪华型,bentengabd","2014款 2.0 手动 舒适型,bentengabe","2014款 1.8T 自动 运动尊享型,bentengabf",
							"2015款 1.8T 自动 精英型,bentengabg"
							],
					
					
					"B50":["2009款 1.6 自动 3G型,bentengbac","2009款 1.6 自动 尊贵型,bentengbad",
							"2009款 1.6 自动 豪华型,bentengbae","2009款 1.6 手动 尊贵型,bentengbaf","2009款 1.6 手动 豪华型,bentengbag",
							"2011款 1.6 手动 尊贵型,bentengbv","2011款 1.6 手动 豪华型,bentengbw","2011款 1.6 手动 舒适型,bentengbx",
							"2011款 1.6 自动 舒适型,bentengby","2011款 1.6 自动 豪华型,bentengbz","2011款 1.6 自动 尊贵型,bentengbaa",
							"2011款 1.6 自动 3G型,bentengbab","2012款 1.6 手动 特装型 油气混合,bentengbh","2012款 1.6 手动 尊贵型 油气混合,bentengbi",
							"2012款 1.6 手动 豪华型 油气混合,bentengbj","2012款 1.6 手动 天窗型 油气混合,bentengbk","2012款 1.6 手动 时尚型 油气混合,bentengbl",
							"2012款 1.6 手动 尊贵型,bentengbm","2012款 1.6 手动 天窗型,bentengbn","2012款 1.6 手动 时尚型,bentengbo",
							"2012款 1.6 手动 豪华型,bentengbp","2012款 1.6 自动 尊贵型,bentengbq","2012款 1.6 自动 天窗型,bentengbr",
							"2012款 1.6 自动 时尚型,bentengbs","2012款 1.6 自动 豪华型,bentengbt","2012款 1.6 自动 3G型,bentengbu",
							"2013款 1.6 手动 伙伴版,bentengba","2013款 1.8 自动 尊贵型,bentengbb","2013款 1.8 自动 舒适型,bentengbc",
							"2013款 1.6 自动 豪华型,bentengbd","2013款 1.6 手动 尊贵型,bentengbe","2013款 1.6 手动 豪华型,bentengbf",
							"2013款 1.6 手动 舒适型,bentengbg"	
							//下一个数据应该是bentengbah开始//
							],
												
					
					"B90":[	"2012款 2.3 自动 旗舰型,bentengcm","2012款 2.3 自动 豪华型,bentengcn","2012款 2.0 自动 导航型,bentengco",
							"2012款 2.0 自动 豪华型,bentengcp","2012款 2.0 自动 舒适型,bentengcq","2012款 2.0 手动 豪华型,bentengcr",
							"2012款 2.0 手动 舒适型,bentengcs","2014款 1.8T 自动 尊贵型,bentengcg","2014款 1.8T 自动 旗舰型,bentengch",
							"2014款 2.0T 自动 豪华型,bentengci",
							"2014款 2.0T 自动 旗舰型,bentengcj","2014款 1.8T 自动 豪华型,bentengck","2014款 1.8T 自动 舒适型,bentengcl",
							"2015款 2.0T 自动 旗舰型,bentengca","2015款 2.0T 自动 豪华型,bentengcb","2015款 1.8T 自动 旗舰型,bentengcc",
							"2015款 1.8T 自动 尊贵型,bentengcd","2015款 1.8T 自动 豪华型,bentengce","2015款 1.8T 自动 舒适型,bentengcf"
							//下一个数据应该是bentengct开始//
							],
							
					"X80":["2013款 2.3 自动 旗舰型前驱,bentengdj","2013款 2.3 自动 豪华型前驱,bentengdk","2013款 2.3 自动 时尚型前驱,bentengdl",
							"2013款 2.0 自动 舒适型前驱,bentengdm","2013款 2.0 自动 豪华型前驱,bentengdn","2013款 2.0 手动 豪华型前驱,bentengdo",
							"2013款 2.0 手动 基本型前驱,bentengdp",
							"2015款 2.0 手动 两周年纪念型前驱,bentengda","2015款 1.8T 自动 旗舰型前驱,bentengdb","2015款 1.8T 自动 运动型前驱,bentengdc",
							"2015款 1.8T 自动 豪华型前驱,bentengdd","2015款 2.0 自动 豪华型前驱,bentengde","2015款 2.0 自动 舒适型前驱,bentengdf",
							"2015款 2.0 自动 舒适周年纪念型前驱,bentengdg","2015款 2.0 手动 豪华型前驱,bentengdh","2015款 2.0 手动 基本型前驱,bentengdi"	
							//下一个数据应该是bentengdr开始//
							],
							
					"奔腾B30":["2016款 1.6 自动 豪华型,bentengea","2016款 1.6 自动 尊享型,bentengeb","2016款 1.6 自动 尊贵型,bentengec",
							"2016款 1.6 手动 尊贵型,bentenged","2016款 1.6 手动 尊享型,bentengee","2016款 1.6 手动 豪华型,bentengef",
							"2016款 1.6 手动 舒适型,bentengeg"
							],	
												
//比亚迪
					"比亚迪S8":["2009款 2.0 手动 尊贵型,bydaa","2009款 2.0 手动 旗舰型,bydab","2009款 2.0 自动 尊贵型,bydac","2009款 2.0 自动 旗舰型,bydad"
						],
						
					"比亚迪F3R":["2007款 1.5 手动 休闲型,bydba","2007款 1.5 手动 活力型,bydbb","2007款 1.5 手动 时尚型,bydbc","2007款 1.5 手动 实用型,bydbd",
							"2008款 1.5 手动 休闲型,bydbe","2008款 1.5 手动 时尚型,bydbf","2008款 1.5 手动 活力型,bydbg","2008款 1.5 手动 舒适型,bydbh",
							"2008款 1.5 手动 实用型,bydbi","2008款 1.6 自动,bydbj","2009款 1.6 自动 金钻版精英型,bydbk","2009款 1.5 手动 金钻版时尚型GLXi,bydbl",
							"2009款 1.5 手动 金钻版舒适型GLi,bydbm","2009款 1.5 手动 金钻版实用型Gi,bydbn","2009款 1.6 自动 金钻版,bydbo",
							"2009款 1.5 手动 舒适型,bydbp","2009款 1.5 手动 实用型,bydbq","2009款 1.6 自动,bydbr","2011款 1.6 自动 时尚11版,bydbs",
							"2011款 1.5 手动 时尚11版舒适型,bydbt","2011款 1.5 手动 时尚11版时尚型,bydbu","2011款 1.5 手动 时尚11版精英型,bydbv"
							],
						
						
					"福莱尔":["2001款 0.8 手动 选装型进口机,bydca","2001款 0.8 手动 豪华型,bydcb","2001款 0.8 手动 选装型,bydcc",
							"2001款 0.8 手动 简装型,bydcd","2001款 0.8 手动 基本型,bydce","2001款 0.8 手动 标准型,bydcf","2001款 0.8 手动 基本型进口机,bydcg",
							"2003款 1.1 手动 福星选装型,bydch","2003款 1.1 手动 福星基本型,bydci","2003款 1.1 手动 福星标准型,bydcj",
							"2003款 1.1 手动 百灵选装型,bydck","2003款 1.1 手动 百灵基本型,bydcl","2003款 0.8 手动 百灵基本型,bydcm",
							"2003款 1.1 手动 百灵标准型,bydcn","2003款 0.9 手动 百灵选装型,bydco","2003款 0.9 手动 百灵标准型,bydcp",
							"2003款 0.8 手动 百灵简装型,bydcq","2003款 0.8 手动 百灵标准型,bydcr","2003款 0.8 手动 百灵选装型,bydcs",
							"2003款 0.9 手动 百灵基本型,bydct","2005款 1.1 手动 选装型,bydcu","2005款 0.9 手动 百灵选装型,bydcv",
							"2005款 1.1 手动 豪华型,bydcw","2005款 1.1 手动 标准型,bydcx","2005款 0.8 自动 标准型,bydcy","2005款 0.8 手动 标准型,bydcz",
							"2005款 0.8 自动 豪华型,bydcaa","2005款 0.8 手动 基本型,bydcab","2005款 0.9 手动 百灵标准型,bydcac"
							],
	
					"比亚迪F6":["2008款 2.4 自动 财富版旗舰型GSi Navi,bydda","2008款 2.4 自动 财富版尊贵型GSi,byddb","2008款 2.0 手动 财富版舒适型GLi,byddc",
						"2008 2.0 手动 财富版标准型Gi,byddd","2008款 2.4 自动 尊贵型GSi,bydde","2008款 2.4 自动 旗舰型GSi,byddf",
						"2008款 2.0 手动 豪华型GLXi,byddg","2008款 2.0 手动 舒适型GLi,byddh",
						"2009款 2.0 手动 CNG油气混合,byddi","2009款 2.4 自动 财富版旗舰型GSi Navi,byddj","2009款 2.4 自动 财富版尊贵型GSi,byddk",
						"2009款 2.0 手动 财富版尊贵型GSi,byddl","2009款 2.0 手动 财富版舒适型GLi,byddm","2009款 2.0 手动 财富版标准型Gi,byddn",                  
						"2010款 2.4 自动 黄金版尊贵型,byddo","2010款 2.4 自动 黄金版旗舰型,byddp",
						"2010款 1.8 手动 黄金版豪华型,byddq","2010款 2.0 自动 黄金版尊贵型,byddr","2010款 2.0 手动 黄金版尊贵型,bydds",
						"2011款 2.0 自动 双燃料,byddt","2011款 2.0 自动 黄金11版尊享型,byddu","2011款 1.8 手动 黄金11版尊享型,byddv",
						"2011款 1.8 手动 黄金11版豪华型,byddw","2011款 2.0 自动 黄金版,byddx","2013款 1.8 手动 尊享型,byddy",
						"2013款 2.0 自动 尊享型,byddz","2013款 1.8 手动 豪华型,byddaa"
						],  	
						
						
					"比亚迪F3":["2005款 1.6 手动 尊贵型GLXi,bydea","2005款 1.6 手动 旗舰型GLXi Navi CNG油气混合,bydeb",
							"2005款 1.6 自动 尊贵型GLX-I CNG油气混合,bydec","2005款 1.6 手动 尊贵型GLXi CNG油气混合,byded","2005款 1.6 自动 舒适型GL-I CNG油气混合,bydee",
							"2005款 1.6 手动 舒适型Gli CNG油气混合,bydef","2005款 1.6 手动 Gi CNG油气混合,bydeg","2005款 1.6 手动 经济型Gi CNG油气混合,bydeh",
							"2005款 1.6 手动 舒适型GLi,bydei","2005款 1.6 手动 经济型 Gi,bydej","2005款 1.6 手动 旗舰型GLXi Navi,bydek","2005款 1.6 自动 旗舰型GLXi-Navi,bydel",
							"2007款 1.5 手动 豪华型白金版,bydem","2007款 1.5 手动 驾驭型白金版,byden","2007款 1.5 手动 实用型白金版,bydeo","2007款 1.6 手动 旗舰型GLXi Navi,bydep",
							"2007款 1.6 手动 舒适型GLi,bydeq","2007款 1.6 手动 经济型Gi,byder","2007款 1.6 手动 尊贵型GLXi,bydes","2007款 1.8 手动 尊享型,bydet",
							"2007款 1.8 手动 旗舰型,bydeu", "2008款 1.6 自动 智能白金版 ,bydev","2008款 1.5 手动 白金版旗舰型GLXi Navi,bydew",
							"2008款 1.5 手动 白金版豪华型GLXi,bydex","2008款 1.5 手动 白金版标准型GLi,bydey","2008款 1.5 手动 白金版实用型 ,bydez",
							"2008款 1.6 自动 白金版 ,bydeaa","2008款 1.6 手动 CNG油气混合,bydeab","2008款 1.5 手动 豪华型白金版 CNG油气混合,bydeac",
							"2008款 1.5 手动 驾驭型白金版 CNG油气混合,bydead","2008款 1.5 手动 实用型白金版 CNG油气混合,bydeae",                 
							"2009款 1.6 自动 智能白金版 ,bydeaf","2009款 1.5 手动 智能白金版智能型GLXi Navi,bydeag","2009款 1.5 手动 智能白金版豪华型GLXi,bydeah",
							"2009款 1.5 手动 智能白金版豪锐型GLXi,bydeai","2009款 1.5 手动 智能白金版标准型GLi,bydeaj","2009款 1.6 手动 CNG油气混合,bydeak",
							"2009款 1.5 手动 实用型白金版 CNG油气混合,bydeal","2009款 1.5 手动 豪华型白金版 CNG油气混合,bydeam","2009款 1.5 手动 智能型白金版,bydean",
							"2009款 1.5 手动 智能型新白金版,bydeao","2009款 1.5 手动 智能型智能白金版,bydeap","2009款 1.5 手动 标准型白金版,bydeaq",
							"2009款 1.5 手动 豪华型白金版,bydear","2009款 1.5 手动 旗舰型白金版,bydeas","2009款 1.5 手动 实用型白金版,bydeat",
							"2009款 1.5 手动 旗舰型新白金版,bydeau","2009款 1.5 手动 豪华型新白金版,bydeav","2009款 1.5 手动 标准型新白金版,bydeaw",
							"2009款 1.5 手动 实用型新白金版,bydeax","2010款 1.6 自动 白金版 ,bydeay","2010款 1.5 手动 白金版豪锐型GLXi,bydeaz","2010款 1.5 手动 白金版实用型,bydeba",
							"2010款 1.5 手动 白金版标准型GLi,bydebb","2010款 1.5 手动 白金版豪华型GLXi,bydebc","2010款 1.5 手动 白金版智能型GLXi Navi,bydebd",                  
							"2011款 1.6 自动 智能白金版 ,bydebe","2011款 1.5 手动 豪华型新白金版,bydebf","2011款 1.5 手动 舒适型新白金版,bydebg",
							"2011款 1.5 手动 标准型新白金版,bydebh","2011款 1.5 自动 白金11版舒适型 CNG油气混合 ,bydebi","2011款 1.5 自动 白金11版豪华型 CNG油气混合 ,bydebj",
							"2011款 1.5 自动 白金11版标准型 CNG油气混合 ,bydebk","2011款 1.5 手动 白金11版舒适型,bydebl","2011款 1.5 手动 白金11版豪华型,bydebm",
							"2011款 1.5 手动 白金11版标准型,bydebn","2011款 1.5 手动 实用型白金版 CNG油气混合 ,bydebo","2011款 1.5 手动 豪华型白金版 CNG油气混合 ,bydebp",
							"2011款 1.6 自动 白金版 CNG油气混合 ,bydebq","2011款 1.5 手动 白金11版豪华型 CNG油气混合,bydebr","2011款 1.5 手动 白金11版舒适型 CNG油气混合,bydebs",
							"2011款 1.5 手动 白金11版标准型 CNG油气混合,bydebt","2011款 1.5 自动 白金11版豪华型,bydebu","2011款 1.5 自动 白金11版标准型,bydebv",
							"2011款 1.5 自动 白金11版舒适型,bydebw","2012款 1.5 手动 舒适型新白金版,bydebx","2012款 1.5 手动 豪华型新白金版,bydeby",
							"2012款 1.5 手动 标准型新白金版,bydebz","2012款 1.5 手动 舒适型GLi,bydeca","2012款 1.5 手动 豪华型GLXi,bydecb","2012款 1.5 手动 标准型GLi,bydecc",                                         
							"2013款 1.5 自动 豪华型,bydecd","2013款 1.5 自动 标准型,bydece","2013款 1.5 自动 舒适型,bydecf","2013款 1.5 手动 豪华型新白金版,bydecg",
							"2013款 1.5 手动 标准型新白金版,bydech","2013款 1.5 手动 舒适型新白金版,bydeci","2013款 1.5 手动 豪华型,bydecj",
							"2013款 1.5 手动 标准型,bydeck","2013款 1.5 手动 舒适型,bydecl","2014款 1.5 自动 尊贵型,bydecm",
							"2014款 1.5 自动 豪华型,bydecn","2014款 1.5T 手动 豪华型TI,bydeco","2014款 1.5 手动 尊贵型,bydecp",
							"2014款 1.5 手动 豪华型,bydecq","2014款 1.5 手动 舒适型,bydecr","2015款 1.5 自动 尊贵型节能版,bydecs",
							"2015款 1.5 自动 豪华型节能版,bydect","2015款 1.5 手动 尊贵型节能版,bydecu",
							"2015款 1.5 手动 舒适型节能版,bydecv","2015款 1.5 手动 豪华型节能版,bydecw"
							],
					
					"比亚迪F0":["2008款 1.0 手动 尊贵型,bydfa","2008款 1.0 手动 豪华型,bydfb","2008款 1.0 手动 舒适型,bydfc","2008款 1.0 手动 实用型,bydfd",
							"2009款 1.0 手动 爱国版旗舰型,bydfe","2009款 1.0 手动 爱国版尊贵型,bydff",
							"2009款 1.0 手动 爱国版舒适型,bydfg","2009款 1.0 手动 爱国版实用型,bydfh","2009款 1.0 手动 贺岁型,bydfi",                 
							"2010款 1.0 手动 爱国版尚酷型 ,bydfj","2010款 1.0 手动 爱国版悦酷型 ,bydfk",
							"2010款 1.0 手动 爱国版炫酷型 ,bydfl","2011款 1.0 手动 尚酷版尚酷型 ,bydfm","2011款 1.0 手动 尚酷版悦酷型 ,bydfn",
							"2011款 1.0 手动 尚酷版铉酷型 ,bydfo","2012款 1.0 手动 尚酷型 ,bydfp","2012款 1.0 手动 悦酷型 ,bydfq",
							"2012款 1.0 手动 铉酷型 ,bydfr","2013款 1.0 自动 悦酷型,bydfs","2013款 1.0 自动 铉酷型,bydft",                 
							"2015款 1.0 手动 悦酷型,bydfu","2015款 1.0 自动 铉酷型,bydfv","2015款 1.0 手动 尚酷型,bydfw","2015款 1.0 自动 悦酷型,bydfx",
							"2015款 1.0 手动 铉酷型,bydfy"
							],  
					
					"比亚迪F3DM":["2009款 1.0 自动 油电混合,bydga","2010款 1.0 自动 油电混合,bydgb"],
					
					"比亚迪G3":["2010款 1.8 自动 祺雅型,bydha","2010款 1.5 手动 豪雅型,bydhb",
							"2010款 1.5 手动 鑫雅型,bydhc","2010款 1.8 自动 豪雅型,bydhd","2010款 1.8 自动 祺雅型GLXi Navi,bydhe","2010款 1.5 手动 尊雅型GSi,bydhf",
							"2010款 1.5 手动 豪雅型GLXi,bydhg","2010款 1.5 手动 舒雅型GLi,bydhh",,"2011款 1.8 自动 数智版豪雅型,bydhi",
							"2011款 1.8 自动 数智版祺雅型,bydhj","2011款 1.5 自动 数智11版豪雅型,bydhk","2011款 1.5 自动 数智11版鑫雅型,bydhl",
							"2011款 1.5 手动 鑫雅型,bydhm","2011款 1.5 手动 豪雅型,bydhn","2011款 1.8 自动 祺雅型,bydho","2011款 1.8 自动 豪雅型,bydhp",
							"2011款 1.5 手动 数智版鑫雅型,bydhq","2011款 1.5 手动 数智版豪雅型,bydhr",
							"2012款 1.8 自动 豪华型,bydhs","2012款 1.5 手动 豪华型,bydht","2012款 1.5 手动 标准型,bydhu",
							"2013款 1.5 手动 豪华型,bydhv","2013款 1.5 手动 标准型,bydhw","2013款 1.5 自动 豪华型,bydhx","2013款 1.5 自动 标准型,bydhy"
							], 
					
					"比亚迪M6":["2010款 2.4 自动 旗舰型,bydia","2010款 2.4 自动 尊享型,bydib","2010款 2.0 手动 豪华型,bydic","2010款 2.0 手动 舒适型,bydid",
							"2011款 2.4 自动 尊稳版尊享型,bydie","2011款 2.4 自动 尊稳版尊贵型,bydif","2011款 2.4 自动 尊稳版旗舰型,bydig",
							"2011款 2.0 手动 尊稳版尊贵型,bydih","2011款 2.0 手动 尊稳版豪华型,bydii","2011款 2.4 自动 尊贵型,bydij","2011款 2.0 手动 尊贵型,bydik",                 
							"2013款 2.4 手动 尊贵型,bydil","2013款 2.4 手动 舒适型,bydim","2013款 2.4 手动 豪华型,bydin","2013款 2.4 自动 尊贵型,bydio",
							"2013款 2.4 自动 豪华型,bydip","2013款 2.0 手动 舒适型,bydiq",                  
							"2015款 2.4 自动 尊贵型,bydir","2015款 2.4 自动 豪华型,bydis"
							], 
   
					"比亚迪L3":["2009款 1.5 手动 尊贵型,bydja","2010款 1.8 自动 尊贵型,bydjb","2010款 1.8 自动 旗舰型,bydjc",
							"2011款 1.5 手动 尊贵型,bydjd","2011款 1.5 手动 舒适型,bydje","2011款 1.5 手动 新锋畅版舒适型,bydjf",
							"2011款 1.8 自动 新锋畅版尊贵型,bydjg","2011款 1.5 手动 新锋畅版尊贵型,bydjh","2011款 1.8 自动 锋畅版旗舰型,bydji","2011款 1.8 自动 锋畅版尊贵型,bydjj",
							"2011款 1.5 手动 锋畅版旗舰型,bydjk","2011款 1.5 手动 锋畅版尊贵型,bydjl","2011款 1.5 自动 尊贵型,bydjm",
							"2012款 1.5 手动 尊贵型,bydjn","2012款 1.5 手动 舒适型,bydjo","2012款 1.8 自动 尊贵型,bydjp","2013款 1.8 自动 尊贵型,bydjq",
							"2013款 1.5 自动 尊贵型,bydjr","2013款 1.5 自动 舒适型,bydjs","2015款 1.5 自动 节能版尊贵型,bydjt",
							"2015款 1.5 自动 节能版舒适型,bydju","2015款 1.5 手动 节能版尊贵型,bydjv","2015款 1.5 手动 节能版舒适型,bydjw"
							],
 
					"比亚迪G3R":["2011款 1.8 自动 尚雅型,bydka","2011款 1.5 手动 尚雅型,bydkb"],
					
					"比亚迪S6":["2011款 2.4 自动 尊贵型前驱,bydla","2011款 2.0 手动 尊贵型前驱,bydlb","2011款 2.0 手动 豪华型前驱,bydlc",
						"2012款 2.4 自动 尊荣型前驱,bydld","2012款 2.4 自动 尊享型前驱,bydle","2012款 2.4 自动 精英型前驱,bydlf",    
						"2013款 2.0 手动 劲悦版尊享型前驱,bydlg","2013款 2.0 手动 劲悦版尊贵型前驱,bydlh",
						"2013款 2.0 手动 劲悦版精英型前驱,bydli","2013款 2.0 手动 劲悦版豪华型前驱,bydlj","2013款 2.4 自动 白金版尊荣型前驱,bydlk",
						"2013款 2.4 自动 白金版尊亨型前驱,bydll","2013款 2.4 自动 白金版精英型前驱,bydlm","2013款 2.4 手动 白金版尊贵型前驱,bydln",
						"2013款 2.4 手动 白金版精英型前驱,bydlo","2013款 2.4 手动 白金版豪华型前驱,bydlp","2013款 2.0 手动 白金版尊享型前驱,bydlq",
						"2013款 2.0 手动 白金版尊贵型前驱,bydlr","2013款 2.0 手动 白金版精英型前驱,bydls","2013款 2.0 手动 白金版豪华型前驱,bydlt",
						"2013款 2.4 自动 劲悦版尊荣型前驱,bydlu","2013款 2.4 自动 劲悦版尊享型前驱,bydlv","2013款 2.4 自动 劲悦版精英型前驱,bydlw",
						"2013款 2.4 手动 劲悦版尊贵型前驱,bydlx","2013款 2.4 手动 劲悦版精英型前驱,bydly","2013款 2.4 手动 劲悦版豪华型前驱,bydlz",
						"2013款 2.4 手动 尊贵型前驱,bydlaa","2014款 2.4 自动 尊贵型前驱,bydlab","2014款 2.4 自动 旗舰型前驱,bydlac","2014款 2.4 自动 豪华型前驱,bydlad",
						"2014款 1.5T 手动 尊贵型TI 7座前驱,bydlae","2014款 2.4 自动 尊贵型7座前驱,bydlaf","2014款 2.0 手动 豪华型7座前驱,bydlag",
						"2014款 2.4 手动 尊贵型前驱,bydlah","2014款 2.4 手动 精英型前驱,bydlai","2014款 2.4 手动 豪华型前驱,bydlaj",
						"2014款 2.0 手动 尊享型前驱,bydlak","2014款 2.0 手动 尊贵型前驱,bydlal","2014款 2.0 手动 精英型前驱,bydlam",
						"2014款 2.0 手动 豪华型前驱,bydlan","2014款 1.5T 手动 旗舰型TI前驱,bydlao","2014款 1.5T 手动 尊贵型TI前驱,bydlap",
						"2014款 1.5T 手动 豪华型TI前驱,bydlaq"
						],  

					"比亚迪G6":["2011款 2.0 手动 尊贵型,bydma","2011款 2.0 手动 豪华型,bydmb","2011款 1.5T 手动 尊贵型TI,bydmc","2012款 2.0 手动 尊贵型,bydmd",
						"2012款 2.0 手动 豪华型,bydme","2012款 1.5T 手动 尊贵型TI,bydmf","2012款 1.5T 自动 尊荣型TID,bydmg","2012款 1.5T 自动 尊享型TID,bydmh",
						"2013款 2.0 手动 尊贵型,bydmi","2013款 2.0 手动 豪华型,bydmj","2013款 1.5T 手动 尊贵型TI,bydmk","2013款 1.5T 手动 豪华型TI,bydml",
						"2013款 1.5T 自动 尊荣型TID,bydmm","2013款 1.5T 自动 尊贵型TID,bydmn","2013款 1.5T 自动 周年纪念版尊荣型TID,bydmo",
						"2013款 1.5T 自动 周年纪念版尊贵型TID,bydmp","2013款 1.5T 手动 周年纪念版尊贵型TI,bydmq","2013款 1.5T 手动 周年纪念版豪华型TI,bydmr",
						"2013款 2.0 手动 周年纪念版尊贵型,bydms","2013款 2.0 手动 周年纪念版豪华型,bydmt"
						],
					
					"比亚迪E6":["2014款 先行者精英型北京版 纯电动,bydna","2014款 先行者豪华型北京版 纯电动,bydnb","2014款 先行者精英型 纯电动,bydnc",
						"2014款 先行者豪华型 纯电动,bydnd","2012款 先行者豪华型北京版 纯电动,bydne","2012款 先行者 纯电动,bydnf",
						"2016款 400尊贵版 纯电动,bydng","2016款 400精英版 纯电动,bydnh","2016款 先行者精英型北京版纯电动,bydni","2016款 先行者豪华型北京版纯电动,bydn"
						],
						
						
					"速锐":["2012款 1.5T 自动 舒适型TID,bydoa","2012款 1.5T 自动 旗舰型TID,bydob","2012款 1.5T 自动 豪华型TID,bydoc",
						"2012款 1.5T 手动 豪华型TI,bydod","2012款 1.5T 手动 舒适型TI,bydoe","2012款 1.5 手动 豪华型,bydof","2012款 1.5 手动 舒适型,bydog",
						"2013款 1.5T 自动 豪华型TID,bydoh","2013款 1.5T 手动 尊贵型TI,bydoi","2013款 1.5 手动 尊贵型,bydoj",
						"2014款 1.5T 自动 旗舰型TID,bydok","2014款 1.5T 自动 豪华型TID,bydol",
						"2014款 1.5T 自动 舒适型TID,bydom","2014款 1.5T 手动 豪华型TI,bydon","2014款 1.5T 手动 舒适型TI,bydoo",
						"2014款 1.5 自动 豪华型,bydop","2014款 1.5 自动 舒适型,bydoq","2014款 1.5 手动 豪华型,bydor","2014款 1.5 手动 舒适型,bydos",
						"2014款 1.5T 手动 尊贵型TI,bydot","2014款 1.5 手动 尊贵型,bydou",
						"2015款 1.5 自动 旗舰型,bydov","2015款 1.5 自动 豪华型,bydow","2015款 1.5 自动 舒适型,bydox","2015款 1.5 手动 旗舰型,bydoy",
						"2015款 1.5 手动 豪华型,bydoz","2015款 1.5 手动 舒适型,bydoaa"
						],	
					
					
					"思锐":["2013款 1.5T 自动 旗舰型,bydpa","2013款 1.5T 自动 尊享型,bydpb","2013款 1.5T 自动 尊贵型,bydpc","2013款 1.5T 自动 豪华型,bydpd",
						   "2013款 1.5T 手动 尊享型,bydpe","2013款 1.5T 手动 尊贵型,bydpf","2013款 1.5T 手动 豪华型,bydpg"
						   ],
					
					"秦":["2014款 1.5T 自动 酷黑骑士旗舰型 油电混合,bydqa","2014款 1.5T 自动 酷黑骑士尊贵型 油电混合,bydqb",
						"2014款 1.5T 自动 旗舰型 油电混合,bydqc","2014款 1.5T 自动 尊贵型 油电混合,bydqd",
						"2015款 1.5T 自动 双冠旗舰Plus版 油电混合,bydqe","2015款 1.5T 自动 双冠旗舰版 油电混合,bydqf"
						],
											
					"腾势":["2014款 尊贵版 纯电动,bydra","2014款 时尚版 纯电动,bydrb"],
					
					"比亚迪G5":["2014款 1.5T 自动 尊贵型TID,bydsa","2014款 1.5T 自动 旗舰型TID,bydsb","2014款 1.5T 自动 豪华型TID,bydsc",
						"2014款 1.5T 手动 旗舰型TI,bydsd","2014款 1.5T 手动 尊贵型TI,bydse","2014款 1.5T 手动 豪华型TI,bydsf"
						],
					
					"比亚迪S7":["2015款 2.0T 自动 升级版旗舰型7座前驱,bydta","2015款 2.0T 自动 升级版尊贵型7座前驱,bydtb","2015款 2.0T 自动 升级版豪华型7座前驱,bydtc",
						"2015款 1.5T 手动 升级版尊贵型7座前驱,bydtd","2015款 1.5T 手动 升级版豪华型7座前驱,bydte","2015款 1.5T 手动 尊贵型7座前驱,bydtf",
						"2015款 1.5T 手动 豪华型7座前驱,bydtg","2015款 2.0T 自动 旗舰型TID 7座前驱,bydth","2015款 2.0T 自动 尊贵型TID 7座前驱,bydti",
						"2015款 2.0T 自动 豪华型TID 7座前驱,bydtj"
						],
						
					"唐":["2015款 2.0T 自动 精英型四驱,bydua","2015款 2.0T 自动 旗舰型四驱,bydub","2015款 2.0T 自动 极速型四驱,byduc"],	
					
					"宋":["2016款 2.0T 自动 领潮型,bydva","2016款 1.5T 自动 双模版旗舰型四驱,bydvb","2016款 2.0T 自动 旗舰型,bydvc",
						"2016款 2.0T 自动 尊贵型,bydvd","2016款 2.0T 自动 豪华型,bydve","2016款 2.0T 自动 舒适型,bydvf",
						"2016款 1.5T 手动 尊贵型,bydvg","2016款 1.5T 手动 豪华型,bydvh","2016款 1.5T 手动 舒适型,bydvi"
						],
																	

//宝骏

					"宝骏630":["2011款 1.5 手动 标准型,baojunaf","2012款 1.5 自动 精英型,baojunaa","2012款 1.5 自动 舒适型,baojunab",
							"2012款 1.5 手动 舒适型,baojunac","2012款 1.5 手动 精英型,baojunad","2012款 1.5 手动 标准型,baojunae",
							"2013款 1.5 自动 舒适型,baojunag","2013款 1.5 自动 精英型,baojunah","2013款 1.5 手动 舒适炫酷版,baojunai",
							"2013款 1.5 手动 舒适超值版,baojunaj","2013款 1.5 手动 精英超值版,baojunak","2013款 1.5 手动 进取超值版,baojunal",
							"2013款 1.5 手动 标准超值版,baojunam","2013款 1.8 自动 精英型,baojunan","2013款 1.8 自动 舒适型,baojunao",
							"2013款 1.8 手动 精英型,baojunap","2013款 1.8 手动 舒适型,baojunaq","2014款 1.5 自动 精英型,baojunar",
							"2014款 1.5 自动 舒适型,baojunas","2014款 1.5 手动 精英型,baojunat","2014款 1.5 手动 舒适型,baojunau",
							"2014款 1.5 手动 标准型,baojunav"
							],
							
							
					"宝骏乐驰":["2012款 1.0 手动 P-TEC时尚型,baojunba","2012款 1.0 手动 P-TEC舒适型,baojunbb","2012款 1.0 手动 P-TEC标准型,baojunbc",
							"2012款 1.2 手动 P-TEC优越型改款,baojunbd","2012款 1.2 手动 P-TEC时尚型改款,baojunbe","2012款 1.2 手动 P-TEC活力型改款,baojunbf",
							"2012款 1.2 手动 优越型,baojunbg","2012款 1.2 手动 时尚型,baojunbh","2012款 1.2 手动 活力型,baojunbi","2012款 1.0 手动 时尚型,baojunbj",
							"2012款 1.0 手动 活力型,baojunbk","2012款 1.0 手动 优越型,baojunbl"
							],
							
					"宝骏610":["2014款 1.5 自动 Cross豪华型,baojunca","2014款 1.5 自动 Cross舒适型,baojuncb","2014款 1.5 手动 Cross舒适型,baojuncc",
							"2014款 1.5 手动 Cross标准型,baojuncd","2014款 1.5 自动 豪华型,baojunce","2014款 1.5 自动 舒适型,baojuncf","2014款 1.5 手动 舒适型,baojuncg",
							"2014款 1.5 手动 标准型,baojunch"
                         ],
                         
                         
                    "宝骏730":["2014款 1.5 手动 豪华导航ESP版7座,baojunda","2014款 1.5 手动 舒适型ESP版7座,baojundb","2014款 1.8 手动 舒适型7座,baojundc",
							"2014款 1.5 手动 豪华型7座,baojundd","2014款 1.5 手动 舒适型7座,baojunde","2014款 1.5 手动 标准型8座,baojundf",
							"2014款 1.5 手动 标准型7座,baojundg","2014款 1.5 手动 标准型5座,baojundh",
							"2015款 1.8 手动 舒适ESP版7座,baojundi","2015款 1.8 手动 豪华导航ESP版7座,baojundj",                 
							"2016款 1.8 手动 舒适型7座,baojundk","2016款 1.8 手动 豪华型7座,baojundl","2016款 1.5 手动 豪华型7座,baojundm",
							"2016款 1.5 手动 舒适型7座,baojundn"
							],     
                         
					"宝骏560":["2015款 1.8 手动 豪华型,baojunea","2015款 1.8 手动 精英型,baojuneb","2015款 1.8 手动 舒适型,baojunec"],

//"宾利"		
							
							
					"欧陆":["1991款 6.8 自动 R,binliaa","1996款 6.8T 自动 (426HP),binliab","1996款 6.8T 自动 (405HP),binliac",
						"2004款 6.0T 自动 GT,binliad","2005款 6.0T 自动 Flying Spur,binliae","2006款 6.0T 自动 GTC,binliaf",
						"2007款 6.0T 自动 GTC,binliag","2007款 4.0T 自动 GT speed,binliah","2008款 6.0T 自动 GTZ Zagato,binliai",
						"2008款 6.0T 自动 Flying Spur Speed,binliaj","2008款 6.0T 自动 Speed GT,binliak","2009款 6.0T 自动 Speed GTC,binlial",
						"2009款 6.0T 自动 Supersport,binliam","2010款 6.0T 自动 Supersports敞篷版,binlian","2011款 4.0T 自动 GTC,binliao",
						"2012款 4.0T 自动 GTC敞篷版,binliap","2012款 6.0T 自动 GTC,binliaq","2012款 4.0T 自动 GT,binliar","2012款 6.0T 自动 GT,binlias",
						"2012款 6.0T 自动 ISR敞篷版Supersport,binliat","2013款 6.0T 自动 GT Speed,binliau","2013款 6.0T 自动 GT Speed敞篷版,binliav",
						"2014款 6.0T 自动 GT极速敞篷版,binliaw","2014款 6.0T 自动 GT极速版,binliax","2014款 4.0T 自动 GTS敞篷尊贵版,binliay",
						"2014款 4.0T 自动 GTS敞篷标准版,binliaz","2014款 4.0T 自动 GTS尊贵版,binliaaa","2014款 4.0T 自动 GTS标准版,binliaab",
						"2015款 4.0T 自动 GT3 R限量版,binliaac"
						],		
							
					"雅致":["1998款 6.8T 自动,binliba","2000款 6.8T 自动 RL,binlibb","2002款 6.8T 自动 R,binlibc",
							"2004款 6.8T 自动 RL,binlibd","2005款 6.8T 自动 Blue Train Series,binlibe","2005款 6.8T 自动,binlibf",
							"2005款 6.8T 自动 R,binlibg","2006款 6.8T 自动,binlibh","2009款 6.8T 自动 Final Series,binlibi"
							],
							
					"慕尚":["2009款 6.8T 自动,binlica","2011款 6.8T 自动 尊贵版,binlicb","2011款 6.8T 自动 豪华版,binlicc",
							"2012款 6.8T 自动 EIC特别定制版,binlicd","2012款 6.8T 自动 女王登基钻禧纪念版,binlice",                  
							"2013款 6.8T 自动 Mulliner特别版,binlicf","2014款 6.8T 自动 四季臻藏版·春夏秋冬,binlicg",
							"2014款 6.8T 自动 四季臻华款,binlich","2015款 6.8T 自动 极致版,binlici","2015款 6.8T 自动 标准版,binlicj"
							 ],
							 
					"雅骏":["2006款 6.8T 自动,binlida","2009款 6.8T 自动,binlidb"],
					
					"飞驰":["2005款 6.0T 自动 基本型,binliea","2010款 6.0T 自动 Speed China,binlieb","2010款 6.0T 自动 5座,binliec",
							"2010款 6.0T 自动 4座,binlied","2012款 6.0T 自动 限量版,binliee","2012款 6.0T 自动 限量版Flying Spur,binlief",
							"2013款 6.0T 自动 豪华版,binlieg","2013款 6.0T 自动 尊贵版,binlieh","2013款 6.0T 自动 MULLINER,binliei",                                           
							"2014款 6.0T 自动 极速版Speed China,binliej","2014款 4.0T 自动 尊贵版,binliek","2014款 4.0T 自动 标准版,binliel"
							],
							
					"添越":["2016款 6.0T 自动 First-Edition限量版,binlifa","2016款 6.0T 自动 标准型,binlifb"],		
							
//布加迪

					"威航":["2005款 8.0T 自动,bjdaa","2008款 8.0T 自动 Grand Sports,bjdab","2008款 8.0T 自动 爱马仕特别版,bjdac"],
							
							
//标志							
					"标致301":["2014款 1.6 手动 舒适版,biaozhiaa","2014款 1.6 自动 舒适版,biaozhiab","2014款 1.6 手动 豪华版,biaozhiac",
							"2014款 1.6 自动 豪华版,biaozhiad","2014款 1.6 自动 尊贵版,biaozhiae","2015款 1.6 手动 舒适版 油气混合,biaozhiaf"
							],
		
		
		          "标致308":["2012款 2.0 手动 风尚型,biaozhiba","2012款 2.0 自动 尊尚型,biaozhibb","2012款 2.0 自动 风尚型,biaozhibc","2012款 1.6 手动 优尚型,biaozhibd",
							"2012款 1.6 手动 风尚型,biaozhibe","2012款 1.6 自动 优尚型,biaozhibf","2012款 1.6 自动 风尚型,biaozhibg","2013款 1.6 手动 优尚型 CNG油气混合,biaozhibh",
							"2013款 2.0 自动 风尚型,biaozhibi","2013款 1.6 手动 风尚型,biaozhibj","2013款 2.0 自动 尊尚型,biaozhibk","2013款 1.6 自动 风尚型,biaozhibl",
							"2013款 1.6 自动 优尚型,biaozhibm","2013款 1.6 手动 优尚型,biaozhibn","2014款 1.6 手动 乐享版优尚型,biaozhibo","2014款 1.6 手动 乐享版风尚型,biaozhibp",
							"2014款 2.0 手动 乐享版风尚型,biaozhibq","2014款 2.0 自动 乐享版风尚型,biaozhibr","2014款 1.6 手动 乐享版优尚型 CNG油气混合,biaozhibs",
							"2014款 1.6 自动 乐享版优尚型,biaozhibt","2014款 1.6 自动 乐享版风尚型,biaozhibu"
							],
							
					"标致308S":["2015款 1.6 手动 尚驰版,biaozhica","2015款 1.6 手动 劲驰版,biaozhicb","2015款 1.2T 自动 尚驰版,biaozhicc",
							   "2015款 1.2T 自动 劲驰版,biaozhicd","2015款 1.6T 自动 劲驰版,biaozhice","2015款 1.6T 自动 睿驰版,biaozhicf"
								],
							
					"标致408":["2010款 1.6 手动 舒适型,biaozhida","2010款 1.6 手动 豪华型,biaozhidb","2010款 1.6 自动 舒适型,biaozhidc",
							"2010款 1.6 自动 豪华型,biaozhidd","2010款 2.0 自动 舒适型200周年限量版,biaozhide","2010款 2.0 手动 舒适型200周年限量版,biaozhidf",
							"2010款 1.6 自动 舒适型200周年限量版,biaozhidg","2010款 1.6 手动 舒适型200周年限量版,biaozhidh","2010款 2.0 自动 尊贵型,biaozhidi",
							"2010款 2.0 自动 豪华型,biaozhidj","2010款 2.0 自动 舒适型,biaozhidk","2010款 2.0 手动 豪华型,biaozhidl","2010款 2.0 手动 舒适型,biaozhidm",
							"2011款 2.0 手动 舒适型,biaozhidn","2011款 2.0 手动 豪华型,biaozhido",
							"2011款 2.0 自动 尊贵型,biaozhidp","2011款 2.0 自动 舒适型,biaozhidq","2011款 2.0 自动 豪华型,biaozhidr",
							"2011款 1.6 手动 舒适型,biaozhids","2011款 1.6 手动 豪华型,biaozhidt","2011款 1.6 自动 舒适型,biaozhidu",
							"2011款 1.6 自动 豪华型,biaozhidv","2012款 2.0 自动 罗兰加洛斯版,biaozhidw","2013款 2.0 自动 尊贵型,biaozhidx","2013款 2.0 自动 舒适型,biaozhidy",
							"2013款 2.0 自动 豪华型,biaozhidz","2013款 1.6 手动 舒适型 油气混合,biaozhidaa","2013款 2.0 自动 车载互联尊贵版,biaozhidab",
							"2013款 2.0 自动 车载互联豪华版,biaozhidac","2013款 2.0 手动 豪华型改款,biaozhidad","2013款 2.0 手动 舒适型改款,biaozhidae",
							"2013款 2.0 手动 豪华型,biaozhidaf","2013款 2.0 手动 舒适型,biaozhidag","2013款 1.6 自动 舒适型,biaozhidah","2013款 1.6 手动 舒适型,biaozhidai",
							"2014款 1.6T 自动 至尊版,biaozhidaj","2014款 1.6T 自动 尊贵版,biaozhidak","2014款 1.8 自动 豪华版,biaozhidal","2014款 1.8 手动 领先版,biaozhidam",
							"2014款 1.8 自动 领先版,biaozhidan","2015款 1.2T 自动 荣耀版,biaozhidao","2015款 1.2T 自动 豪华版,biaozhidap"
							],		
							
					"标致508":["2011款 2.3 自动 旗舰版,biaozhiea","2011款 2.3 自动 经典版,biaozhieb","2011款 2.3 自动 豪华版,biaozhiec","2011款 2.0 手动 经典版,biaozhied",
							"2012款 2.0 自动 天窗经典版,biaozhiee","2012款 2.0 自动 智享版,biaozhief","2012款 2.3 自动 智享版,biaozhieg",
							"2012款 2.0 自动 经典版,biaozhieh","2012款 2.0 自动 豪华版,biaozhiei","2013款 2.0 自动 智享两周年纪念型,biaozhiej",
							"2013款 2.3 自动 两周年纪念旗舰版,biaozhiek","2013款 2.3 自动 两周年纪念智享版,biaozhiel",
							"2013款 2.0 自动 经典两周年纪念天窗版,biaozhiem","2013款 2.3 自动 罗兰加洛斯版,biaozhien","2014款 2.3 自动 罗兰加洛斯版,biaozhieo",
							"2015款 2.0 自动 致臻版,biaozhiep","2015款 2.0 自动 致逸版,biaozhieq","2015款 1.8T 自动 旗舰版,biaozhier",
							"2015款 1.8T 自动 致尊版,biaozhies","2015款 1.8T 自动 致臻版,biaozhiet","2015款 1.6T 自动 致臻版,biaozhieu",
							"2015款 1.6T 自动 致逸版,biaozhiev"
							],	
							
					"标致3008":["2013款 1.6T 自动 至尚版,biaozhifa","2013款 1.6T 自动 潮流版,biaozhifb","2013款 2.0 手动 经典版,biaozhifc",
								"2013款 2.0 手动 潮流版,biaozhifd","2013款 2.0 自动 经典版,biaozhife","2013款 2.0 自动 潮流版,biaozhiff","2013款 1.6T 自动 罗兰加洛斯版,biaozhifg",
								"2014款 1.6T 自动 逐乐版,biaozhifh","2014款 1.6T 自动 罗兰加洛斯版,biaozhifi","2015款 1.6T 自动 罗兰加洛斯版,biaozhifj",
								"2015款 2.0 手动 经典版,biaozhifk","2015款 2.0 手动 潮流版,biaozhifl","2015款 2.0 自动 经典版,biaozhifm",
								"2015款 2.0 自动 潮流版,biaozhifn","2015款 1.6T 自动 经典版,biaozhifo","2015款 1.6T 自动 至尚版,biaozhifp","2015款 1.6T 自动 潮流版,biaozhifq"
								],		
							
							
					"标致2008":["2015款 1.6T 自动 罗兰加洛斯版,biaozhga","2015款 1.6T 自动 领航版,biaozhgb","2015款 1.6T 自动 时尚版,biaozhgc",
								"2014款 1.6 手动 卓越版前驱,biaozhgd","2014款 1.6 自动 卓越版前驱,biaozhge","2014款 1.6 自动 领航版前驱,biaozhgf",
								"2014款 1.6 自动 时尚版前驱,biaozhgg","2014款 1.6 手动 时尚版前驱,biaozhgh","2014款 1.6 自动 潮流版前驱,biaozhgi",
								"2014款 1.6 手动 潮流版前驱,biaozhgj"
								],
													
					
					"标致206":["2005款 1.6 手动 XT,biaozhha","2005款 1.6 自动 XT,biaozhhb","2005款 1.6 自动 XR,biaozhhc","2005款 1.6 手动 XR,biaozhhd",
							"2006款 1.6 手动 XT,biaozhhe","2006款 1.4 手动 XR ETEC,biaozhhf","2007款 1.6 手动 XT真皮天窗版,biaozhhg",
							"2007款 1.6 自动 XT真皮天窗版,biaozhhh","2007款 1.6 手动 S运动版,biaozhhi",
							"2007款 1.6 自动 S运动版,biaozhhj","2007款 1.6 手动 跃动版,biaozhhk","2007款 1.6 手动 睿动版,biaozhhl",
							"2007款 1.6 自动 跃动版,biaozhhm","2007款 1.4 手动 S运动版,biaozhhn","2007款 1.4 手动 跃动版,biaozhho",
							"2007款 1.4 手动 XR风尚版,biaozhhp","2007款 1.6 自动 XT悦尚版,biaozhhq","2007款 1.6 手动 XT悦尚版,biaozhhr",
							"2007款 1.6 自动 XT智尚版,biaozhhs","2007款 1.6 手动 XT智尚版,biaozhht","2007款 1.6 自动 XR风尚版,biaozhhu",
							"2007款 1.6 手动 XR风尚版,biaozhhv","2007款 1.6 自动 睿动版,biaozhhw","2008款 1.4 手动 炫动版,biaozhhx",
							"2008款 1.6 自动 炫动版,biaozhhy","2008款 1.6 手动 炫动版,biaozhhz"
							],
							
					"标致207":["2009款 三厢 1.4L 手动驭乐版,biaozhiia","2009款 三厢 1.6L 手动驭乐版,biaozhiib","2009款 三厢 1.4L 手动品乐版,biaozhiic",
							"2009款 两厢 1.4L 手动驭乐版,biaozhiid","2009款 两厢 1.4L 手动品乐版,biaozhiie","2009款 两厢 1.6L 手动驭乐版,biaozhiif",
							"2009款 两厢 1.6L 手动品乐版,biaozhiig","2009款 两厢 1.6L 自动品乐版,biaozhiih","2010款 三厢 1.4L 手动驭乐版,biaozhiii",
							"2010款 两厢 1.4L 手动驭乐版,biaozhiij","2010款 三厢 1.4L 手动品乐版,biaozhiik","2010款 两厢 1.4L 手动品乐版,biaozhiil",
							"2010款 两厢 1.6L 手动驭乐版,biaozhiim","2010款 三厢 1.6L 手动品乐版,biaozhiin","2010款 三厢 1.6L 自动驭乐版,biaozhiio",
							"2010款 两厢 1.6L 自动驭乐版,biaozhiip","2010款 三厢 1.6L 自动品乐版,biaozhiiq","2010款 两厢 1.6L 自动品乐版,biaozhiir",
							"2011款 两厢 1.6L 自动驭乐版,biaozhiis","2011款 两厢 1.4L 手动驭乐版,biaozhiit","2011款 两厢 1.6L 手动驭乐版,biaozhiiu",
							"2011款 三厢 1.4L 手动驭乐版,biaozhiiv","2011款 两厢 1.6L 自动品乐版,biaozhiiw","2011款 两厢 1.4L 手动品乐版,biaozhiix",
							"2011款 三厢 1.4L 手动品乐版,biaozhiiy"
							],
							
					"标致307":["2004款 2.0L 自动Prestige,biaozhija","2004款 1.6L 手动XT,biaozhijb","2004款 1.6L 手动XS,biaozhijc","2004款 1.6L 自动XS,biaozhijd",
							"2004款 1.6L 自动XT,biaozhije","2004款 1.6L 自动Prestige,biaozhijf","2004款 2.0L 自动XT,biaozhijg","2004款 2.0L 手动Prestige,biaozhijh",
							"2005款 2.0L 驾御版,biaozhiji","2006款 1.6L 手动XS,biaozhijj","2006款 2.0L 手动驾御版,biaozhijk","2006款 1.6L 自动XS,biaozhijl",
							"2006款 1.6L 手动XT,biaozhijm","2006款 1.6L 自动XT,biaozhijn","2006款 2.0L 手动XT,biaozhijo","2006款 2.0L 手动Prestige,biaozhijp",
							"2006款 2.0L 自动Prestige,biaozhijq","2006款 2.0L 自动驾御版,biaozhijr","2007款 1.6L 手动舒适版,biaozhijs",
							"2007款 1.6L 自动舒适版,biaozhijt","2007款 2.0L 手动舒适版,biaozhiju","2007款 2.0L 自动舒适版,biaozhijv","2007款 改款 1.6L 手动舒适版,biaozhijw",
							"2007款 改款 1.6L 自动舒适版,biaozhijx","2007款 改款 1.6L 手动尊贵版,biaozhijy","2007款 改款 1.6L 自动尊贵版,biaozhijz","2007款 改款 2.0L 手动舒适版,biaozhijaa",
							"2007款 改款 2.0L 自动舒适版,biaozhijab","2007款 改款 2.0L 手动尊贵版,biaozhijac","2007款 改款 2.0L 自动尊贵版,biaozhijad","2008款 两厢 2.0L 自动逸致版,biaozhijae",
							"2008款 两厢 2.0L 手动逸致版,biaozhijaf","2008款 两厢 2.0L 自动雅致版,biaozhijag","2008款 两厢 2.0L 自动精致版,biaozhijah","2008款 两厢 2.0L 手动精致版,biaozhijai",
							"2008款 两厢 1.6L 自动精致版,biaozhijaj","2008款 两厢 1.6L 手动精致版,biaozhijak","2008款 两厢 1.6L 手动雅致版,biaozhijal","2008款 两厢 1.6L 自动雅致版,biaozhijam",
							"2009款 三厢 1.6L 手动爱乐版,biaozhijan","2009款 两厢 1.6L 自动爱乐版,biaozhijao","2010款 两厢 1.6L 手动舒适版,biaozhijap","2010款 两厢 1.6L 自动舒适版,biaozhijaq",
							"2010款 两厢 2.0L 手动舒适版,biaozhijar","2010款 两厢 2.0L 自动舒适版,biaozhijas","2010款 两厢 1.6L 手动豪华版,biaozhijat","2010款 两厢 1.6L 自动豪华版,biaozhijau",
							"2010款 三厢 1.6L 自动豪华版,biaozhijav","2010款 三厢 1.6L 手动豪华版,biaozhijaw","2010款 三厢 1.6L 手动舒适版,biaozhijax","2010款 三厢 1.6L 自动舒适版,biaozhijay",
							"2010款 三厢 2.0L 手动舒适版,biaozhijaz","2010款 三厢 1.6L 手动舒适纪念版,biaozhijba","2011款 1.6L 手动CROSS,biaozhijbb","2012款 两厢 1.6L 手动舒适版,biaozhijbc",
							"2012款 两厢 1.6L 手动豪华版,biaozhijbd","2012款 两厢 1.6L 自动舒适版,biaozhijbe","2012款 两厢 1.6L 自动豪华版,biaozhijbf","2012款 1.6L 自动CROSS,biaozhijbg",
							"2013款 两厢 1.6L 自动舒适版,biaozhijbh"
							],
							
					"标致RCZ":["2009款 1.6T 自动 (156HP),biaozhika","2011款 1.6T 自动 豪华优雅型,biaozhikb","2011款 1.6T 自动 时尚型,biaozhikc",
							"2011款 1.6T 自动 豪华运动型,biaozhikd","2014款 1.6T 自动 时尚型,biaozhike","2014款 1.6T 自动 豪华版浅色风格,biaozhikf",
							"2014款 1.6T 自动 豪华版深色风格,biaozhikg"
							],
							
					"标致308(进口)":["2008款 1.6T 自动 时尚型,biaozhijkla","2008款 1.6T 自动 豪华型,biaozhijklb","2008款 1.6T 自动 时尚型5座,biaozhijklc",
									"2012款 1.6T 自动 时尚型,biaozhijkld","2008款 1.6T 自动 豪华型5座,biaozhijkle","2008款 1.6T 自动 豪华型7座,biaozhijklf",
									"2008款 1.6T 自动 时尚型7座,biaozhijklg","2012款 1.6T 自动 豪华型,biaozhijklh",
									"2012款 1.6T 自动 豪华型,biaozhijkli","2012款 1.6T 自动 时尚型,biaozhijklj","2013款 1.6T 自动 罗兰加洛斯版,biaozhijklk"
									],
					
					"标致3008(进口)":["2011款 1.6T 自动 豪华版,biaozhijkma","2011款 1.6T 自动 时尚版,biaozhijkmb"],

					"标致206(进口)":["2004款 1.6 自动,biaozhijkna","2007款 1.6 自动,biaozhijknb"],
					
					"标致207(进口)":["2008款 1.6T 手动 精英版GT150,biaozhijkoa","2008款 1.6T 手动 时尚版GT150,biaozhijkob","2011款 1.6T 手动 风尚版,biaozhijkoc",
								   "2013款 1.6 自动 时尚型,biaozhijkod","2013款 1.6 自动 豪华型,biaozhijkoe","2013款 1.6 自动 罗兰加洛斯豪华版,biaozhijkof",
								   "2013款 1.6 自动 罗兰加洛斯时尚版,biaozhijkog"
								],
								
					"标致307(进口)":["2003款 2.0 自动,biaozhijkpa","2004款 2.0 自动,biaozhijkpb","2005款 2.0 自动,biaozhijkpc","2007款 2.0 自动,biaozhijkpd"
								],
								
					"307旅行车":[ "2005款 1.6 自动 16V 7座版,biaozhiqa","2005款 1.6 自动 16V 5座版,biaozhiqb","2005款 2.0 自动 16V 5座版,biaozhiqc",
								 "2005款 2.0 自动 16V 7座版,biaozhiqd","2008款 2.0 自动 精装版,biaozhiqe",
								 "2008款 2.0 自动 豪华版,biaozhiqf"
								   ],
					
		
					"标致407":[ "2004款 2.2 自动 精装版,biaozhira","2004款 3.0 自动 豪华版,biaozhirb","2004款 2.2 自动 豪华版,biaozhirc",
								 "2005款 3.0 自动,biaozhirc","2008款 2.2 自动 豪华版,biaozhird","2008款 2.2 自动 精装版,biaozhire",
								 "2008款 3.0 自动 豪华版,biaozhirf","2010款 2.2 自动 冠军限量版,biaozhirg",
								 "2010款 2.2 自动 运动导航版,biaozhirh"
								],
								
					"407旅行车":["2005款 2.2 自动 豪华版,biaozhisa","2005款 2.2 自动 豪华多媒体版,biaozhisb","2008款 2.2 自动 豪华版,biaozhisc",
								"2008款 2.2 自动 豪华多媒体版,biaozhisd"
							  ],
								   
					"标致607":["2000款 3.0 自动 ,biaozhita","2004款 3.0 自动 标准型,biaozhitb","2005款 2.2 自动,biaozhitc",
							  "2005款 3.0 自动 顶级型,biaozhitd"
							  ],
								 
					"307Cross":["2012款 1.6 手动,biaozhiua","2012款 1.6 自动,biaozhiub"],
					
					"207Cross":["2013款 1.4 手动 驭乐型 ,biaozhiva","2013款 1.4 手动 品乐型,biaozhivb","2013款 1.6 手动 驭乐型,biaozhivc",
							  "2013款 1.6 自动 驭乐型 顶级型,biaozhivcd","2013款 1.6 手动 品乐型,biaozhivce","2013款 1.6 自动 品乐型,biaozhivcf"
							  ],
					
//"北汽幻速"					
					
					"幻速S2":["2014款 1.5 手动 尊贵型,bqhsaa","2014款 1.5 手动 豪华型,bqhsab","2014款 1.5 手动 舒适型,bqhsac","2014款 1.5 手动 基本型,bqhsad","2015款 1.5 手动 舒适型,bqhsae",
							"2016款 1.5 手动 幸福版,bqhsaf","2016款 1.5 手动 尊贵型,bqhsag","2016款 1.5 手动 豪华型,bqhsah","2016款 1.5 手动 舒适型,bqhsai",
							"2016款 1.5 手动 基本型,bqhsaj","2016款 1.5 手动 创业型,bqhsak","2015款 1.5 手动 尊贵型,bqhsal","2015款 1.5 手动 豪华型,bqhsam"
							],
					
					"幻速S3":["2014款 1.5 手动 舒适型,bqhsba","2014款 1.5 手动 基本型,bqhsbb","2014款 1.5 手动 豪华型,bqhsbc",
							"2014款 1.8 手动 尊贵型,bqhsbd","2016款 1.8 手动 尊贵型,bqhsbe","2016款 1.5 手动 豪华型,bqhsbf",
							"2016款 1.5 手动 舒适型,bqhsbg","2016款 1.5 手动 基本型,bqhsbh","2016款 1.5 手动 创业型,bqhsbi"
							],
					
					"幻速H2":["2015款 1.5 自动 尊贵型,bqhsca","2015款 1.5 自动 舒适型,bqhscb","2015款 1.5 自动 豪华型,bqhscc",
							"2015款 1.5 手动 豪华型,bqhscd","2015款 1.5 手动 舒适型,bqhsce","2016款  1.5 手动 H2E精英型7座,bqhscf",
							"2016款  1.5 手动 H2E时尚型7座,bqhscg","2016款  1.5 手动 H2E经典型7座,bqhsch"
							],

					"幻速H3":["2015款 1.5 手动 标准型,bqhsda","2015款 1.5 手动 舒适型,bqhsdb","2015款 1.5 手动 豪华型,bqhsdc"
							],		
					
//"长丰"
					
					"猎豹CS6":["2008款 2.4 手动 舒适型后驱,changfengaa","2008款 2.5T 手动 豪华型四驱 柴油,changfengab","2008款 2.5T 手动 舒适型四驱 柴油,changfengac",
							"2008款 2.4 手动 豪华型四驱,changfengad","2008款 2.4 手动 舒适型四驱,changfengae","2010款 2.5T 手动 舒适型四驱 柴油,changfengaf",
							"2010款 2.5T 手动 豪华型四驱 柴油,changfengag","2010款 2.4 手动 舒适型四驱,changfengah","2010款 2.4 手动 豪华型四驱,changfengai",
							"2010款 2.4 手动 舒适型后驱,changfengaj"
							],
							
					"骐菱":["2008款 1.6 手动 标准型,changfengba","2008款 1.6 手动 尊贵型,changfengbb","2008款 1.6 手动 舒适型,changfengbc",
							"2009款 1.6 手动 尊贵型,changfengbd","2009款 1.6 手动 舒适型,changfengbe","2009款 1.6 手动 标准型,changfengbf"
							],
							
					"黑金刚":["2002款 3.0 手动 A豪华型四驱,changfengca","2002款 3.0 手动 A标准型四驱,changfengcb","2002款 2.4 手动 E环保型四驱,changfengcc",
							"2002款 3.0 自动 B-Standard旗舰型四驱,changfengcd","2002款 3.0 手动 A-Luxury旗舰型四驱,changfengce","2003款 3.0 手动 C豪华型四驱,changfengcf",
							"2003款 2.4 手动 F平顶四缸四驱,changfengcg","2003款 3.0 自动 D豪华型四驱,changfengch","2004款 2.4 手动 G入门级后驱,changfengci",
							"2004款 2.4 手动 H平顶四缸后驱,changfengcj","2005款 2.4 手动 舒适型后驱,changfengck","2005款 2.4 手动 L标准型四驱,changfengcl",
							"2006款 2.4 手动 L经典型四驱,changfengcm","2006款 2.4 手动 舒适型后驱,changfengcn","2007款 2.4 手动 豪华型后驱,changfengco",
							"2008款 2.4 手动 舒适型四驱,changfengcp","2008款 2.4 手动 舒适型后驱,changfengcq","2008款 2.4 手动 经典型四驱,changfengcr",
							"2008款 2.4 手动 经典型后驱,changfengcs","2009款 2.5T 手动 后驱 柴油,changfengct","2009款 2.4 手动 舒适型后驱,changfengcu",
							"2013款 2.2 手动 标准型后驱,changfengcv","2013款 2.2 手动 标准型四驱,changfengcw","2014款 2.5T 手动 后驱 柴油,changfengcx",
							"2014款 2.4 手动 四驱,changfengcy","2014款 2.4 手动 后驱,changfengcz"
							],		
							
					"飞腾":["2004款 2.0 自动 标准型四驱,changfengda","2004款 2.0 手动 豪华型四驱,changfengdb","2004款 2.0 自动 豪华型D四驱,changfengdc",
							"2004款 2.0 手动 标准型四驱,changfengdd","2004款 2.0 手动 标准型B四驱,changfengde","2005款 2.0 手动 四驱,changfengdf",
							"2005款 2.0 手动 豪华型C四驱,changfengdg","2006款 2.0 手动 标准型后驱,changfengdh","2009款 2.0 手动 豪华型四驱,changfengdi",
							"2009款 2.0 手动 标准型四驱,changfengdj","2009款 2.0 手动 标准型后驱,changfengdk","2010款 2.0 手动 经典版舒适型四驱,changfengdl",
							"2010款 2.0 手动 经典版豪华型四驱,changfengdm",
							"2010款 2.0 手动 经典版豪华型后驱dn,changfengdo","2010款 1.5 手动 时尚版舒适型后驱,changfengdp","2010款 1.5 手动 时尚版豪华型后驱,changfengdq",
							"2013款 2.0 手动 经典版标准型四驱,changfengdr","2013款 2.0 手动 经典版豪华型四驱,changfengds","2013款 2.0 手动 经典版标准型后驱,changfengdt"
							],		
							
					"长丰奇兵":["2005款 2.4 手动 四驱,changfengea","2005款 2.2 手动 后驱,changfengeb","2005款 2.4 手动 后驱,changfengec",
							"2007款 2.2 手动 四驱,changfenged","2008款 2.2 手动 后驱,changfengee","2008款 2.2 手动 四驱,changfengef"
							],
      		
							
					"DUV":["2009款 2.4 手动 四驱,changfengfa","2009款 2.4 手动 后驱,changfengfb"
							],		
							
					"飞铃":["2006款 2.2 手动 四驱,changfengga","2006款 2.2 手动 后驱,changfenggb","2007款 2.8T 手动 四驱 柴油,changfenggc","2007款 2.8T 手动 后驱 柴油,changfenggd",
							"2008款 2.8T 手动 四驱 柴油,changfengge","2008款 2.8T 手动 后驱 柴油,changfenggf","2008款 2.2 手动 四驱,changfenggg","2008款 2.2 手动 后驱,changfenggh",                  
							"2013款 2.2 手动 后驱,changfenggi","2013款 2.0T 手动 后驱 柴油,changfenggj"
							],		
								
					"猎豹CS7":["2009款 2.0 手动 运动版豪华型四驱,changfengha","2009款 2.0 手动 运动版舒适型四驱,changfenggb","2009款 2.0 手动 艺术版舒适型四驱,changfenghc",
							"2009款 2.0 手动 艺术版豪华型四驱,changfenghd","2009款 2.0 手动 运动版舒适型后驱,changfenghe","2009款 2.0 手动 艺术版舒适型后驱,changfenghf",
							"2009款 2.0 手动 运动版豪华型后驱,changfenghg","2009款 2.0 手动 艺术版豪华型后驱,changfenghh"
							],		
								
					"飞扬":["2006款 2.8T 手动 四驱 柴油,changfengia","2006款 2.8T 手动 后驱 柴油,changfengib","2006款 2.2 手动 四驱,changfengic","2006款 2.2 手动 后驱,changfengid",
							"2010款 2.8T 手动 四驱 柴油,changfengie","2010款 2.8T 手动 后驱 柴油,changfengif","2010款 2.2 手动 四驱,changfengig","2010款 2.2 手动 后驱,changfengih",
							"2013款 2.8T 手动 后驱 柴油,changfengii"
							],  		
								
					"猎豹CT5":["2011款 2.5T 手动 四驱 柴油,changfengka","2011款 2.5T 手动 后驱 柴油,changfengkb","2011款 2.4 手动 四驱,changfengkc",
						"2011款 2.4 手动 后驱,changfengkd","2014款 2.5T 手动 四驱 柴油,changfengke","2014款 2.5T 手动 后驱 柴油,changfengkf"
						],		
								
					"飞腾C5":["2014款 1.5T 自动 豪华型后驱,changfengla","2014款 1.5T 自动 舒适型后驱,changfenglb"
							],		
								
					"猎豹Q6":["2015款 2.4 手动 两驱,changfengma","2015款 2.4 手动 四驱,changfengmb","2015款 2.0T 手动 四驱,changfengmc",
							"2015款 2.0T 手动 两驱,changfengmd","2014款 2.4 手动 四驱,changfengme","2014款 2.4 手动 后驱,changfengmf"
							],
					
					"猎豹CS10":["2015款 2.0T 手动 至尊版,changfengna","2015款 2.0T 手动 卓越版,changfengnb",
								"2015款 2.0T 手动 风尚版,changfengnc","2015款 2.0T 手动 新锐版,changfengnd"
								],			
							
//"长安"	
					
					
					"CM8":["2005款 1.3 手动 GAK标准型,changanaa","2005款 1.3 手动 G1K豪华型,changanab","2006款 1.3 手动,changanac","2006款 1.0 手动,changanad",
							"2006款 1.3 手动 G3K简配型,changanae","2006款 1.6 手动 卓越型,changanaf","2005款 1.3 手动 GBA舒适型,changanag","2007款 1.3 手动 简配型,changanah",
							"2008款 1.3 手动 标准型,changanai","2008款 1.3 手动 豪华型,changanaj"
							],
					
					
					"CS35":["2012款 1.6 自动 舒适型前驱,changanba","2012款 1.6 自动 豪华导航型前驱,changanbb","2012款 1.6 手动 豪华导航型前驱,changanbc","2012款 1.6 自动 豪华型前驱,changanbd",
							"2012款 1.6 手动 豪华型前驱,changanbe","2012款 1.6 手动 舒适型前驱,changanbf","2014款 1.6 自动 豪华导航型前驱,changanbg","2014款 1.6 自动 豪华型前驱,changanbh",
							"2014款 1.6 自动 舒适型前驱,changanbi","2014款 1.6 手动 豪华导航型前驱,changanbj","2014款 1.6 手动 豪华型前驱,changanbk","2014款 1.6 手动 舒适型前驱,changanbl",
							"2015款 1.6 自动 劲悦版尊贵型前驱,changanbm","2015款 1.6 自动 尊贵型前驱,changanbn","2015款 1.6 自动 豪华型前驱,changanbo","2015款 1.6 手动 尊贵型前驱,changanbp",
							"2015款 1.6 手动 豪华型前驱,changanbq","2015款 1.6 手动 舒适型前驱,changanbr"
							],
					
					"CS75":["2014款 1.8T 自动 尊贵型,changanca","2014款 1.8T 自动 精英型,changancb","2014款 1.8T 自动 时尚型,changancc","2014款 2.0 手动 领先型,changancd","2014款 2.0 手动 豪华型,changance",
							"2014款 2.0 手动 舒适型,changancf","2015款 1.8T 自动 尊贵型四驱,changancg","2015款 1.8T 自动 精英型四驱,changanch","2015款 1.8T 自动 时尚型四驱,changanci"
							],
					
					
					
					"CX20":["2011款 1.3 手动 公务型,changanda","2011款 1.3 自动 公务型,changandb","2011款 1.3 自动 运动型,changandc",
							"2011款 1.3 自动 3G型,changandd","2011款 1.3 自动 舒适型,changande","2011款 1.3 手动 运动型,changandf","2011款 1.3 手动 舒适型,changandg",
							"2011款 1.3 手动 标准型,changandh","2013款 1.4 手动 天窗导航型,changandi","2013款 1.4 手动 运动型,changandj","2014款 1.4 自动 天窗导航型,changandk",
							"2014款 1.4 手动 天窗导航型,changandl","2014款 1.4 手动 运动型,changandm"
							],
							
							
					"CX30":["2010款 1.6 手动 舒适型,changanea","2010款 2.0 自动 豪华型,changaneb","2010款 2.0 自动 精英型,changanec",
							"2010款 2.0 手动 时尚型,changaned","2010款 1.6 手动 豪华低碳型,changanee","2010款 1.6 手动 时尚低碳型,changanef","2010款 1.6 手动 运动型,changaneg",
							"2010款 1.6 手动 豪华型,changaneh","2010款 1.6 手动 时尚型,changanei","2010款 1.6 手动 舒适性,changanej","2009款 1.6 手动,changanek",          
							"2011款 1.6 手动 时尚低碳型,changanel","2011款 1.6 手动 豪华低碳型,changanem","2011款 2.0 自动 舒适型,changanen","2011款 2.0 自动 豪华型,changaneo",
							"2011款 1.6 手动 智能豪华型,changanep","2011款 1.6 手动 豪华低碳型,changaneq","2011款 1.6 手动 舒适低碳型,changaner","2011款 1.6 手动 豪华型,changanes",
							"2011款 1.6 手动 精英型,changanet","2011款 1.6 手动 舒适型,changaneu","2011款 1.6 手动 标准型,changanev","2012款 1.6 手动 豪华低碳版,changanew",
							"2012款 1.6 手动 豪华型,changanex","2012款 1.6 手动 舒适型,changaney","2011款 2.0 自动 智能旗舰型,changanez"
							],
							
					"奔奔":["2007款 1.0 手动 标准型,changanfa","2007款 1.0 手动 基本型,changanfb","2007款 1.3 手动 超值型,changanfc","2007款 1.3 手动 豪华型,changanfd",
							"2007款 1.3 手动 经典型,changanfe","2007款 1.3 手动 舒适型,changanff","2008款 1.0 手动 情定E生版,changanfg","2008款 1.3 手动 ,changanfh",
							"2008款 1.0 手动 ,changanfi","2008款 1.3 自动 舒适型,changanfj","2008款 1.3 手动 运动版F3,changanfk","2008款 1.3 手动 运动版F2,changanfl",
							"2008款 1.3 手动 运动版F1,changanfm","2008款 1.3 自动 精英型,changanfn","2008款 1.3 自动 超值型,changanfo","2008款 1.3 手动 超值型,changanfp",
							"2008款 1.3 手动 豪华型,changanfq","2008款 1.3 手动 舒适型,changanfr","2008款 1.3 手动 经典型,changanfs","2009款 1.3 自动 LOVE,changanft",
							"2009款 1.3 手动 LOVE,changanfu","2009款 1.0 手动 MINI,changanfv","2009款 1.0 手动 情定E生版,changanfw","2009款 1.3 手动 两周年炫动纪念版,changanfx",
							"2010款 1.3 手动 运动型,changanfy","2010款 1.0 自动 MINI豪华型,changanfz","2010款 1.0 手动 MINI尊贵型,changanfaa","2010款 1.0 手动 MINI豪华型,changanfab",
							"2010款 1.0 手动 MINI舒适型,changanfac","2010款 1.0 手动 MINI标准型,changanfad","2010款 1.0 手动 MINI特别限量版,changanfae","2010款 1.0 手动 情定E生版,changanfaf",
							"2010款 1.3 自动 LOVE超值型,changanfag","2010款 1.3 手动 LOVE时尚型,changanfah","2010款 1.3 手动 LOVE超值型,changanfai","2010款 1.3 手动 LOVE基本型,changanfaj",
							"2010款 1.3 手动 LOVE标准型,changanfak","2011款 1.0 手动 MINI时尚型,changanfal","2011款 1.0 手动 MINI亲情型,changanfam","2011款 1.0 自动 MINI豪华型,changanfan",
							"2011款 1.0 自动 MINI舒适型,changanfao","2011款 1.0 手动 MINI尊贵型,changanfap",
							"2011款 1.0 手动 MINI舒适型,changanfaq","2011款 1.0 手动 MINI豪华型,changanfas","2011款 1.0 手动 MINI标准型,changanfat","2012款 1.0 自动 MINI导航型,changanfau",
							"2012款 1.0 自动 MINI时尚型,changanfav","2012款 1.0 手动 MINI导航型,changanfaw","2012款 1.0 手动 MINI时尚型,changanfax","2012款 1.0 手动 MINI亲情型,changanfay",
							"2014款 1.4 自动 尊贵天窗型,changanfaz","2014款 1.4 自动 尊贵型,changanfba","2014款 1.4 手动 天窗型,changanfbb","2014款 1.4 自动 豪华型,changanfbc",
							"2014款 1.4 手动 尊贵型,changanfbd","2014款 1.4 手动 豪华型,changanfbe","2015款 1.4 自动 天窗型,changanfbf","2015款 1.4 自动 尊贵型,changanfbg",
							"2015款 1.4 自动 豪华型,changanfbh","2015款 1.4 手动 天窗型,changanfbi","2015款 1.4 手动 尊贵型,changanfbj","2015款 1.4 手动 豪华型,changanfbk",
							"2015款 1.4 手动 舒适型,changanfbl","2015款 1.4 自动 尊贵天窗型,changanfbm"
							],		
														
					"尊行":["2014款 2.7 手动 尊享版,changanga","2014款 2.7 手动 尊贵版,changangb","2014款 2.7 手动 尊荣版,changangc",
							"2015款 2.4 手动 标准型,changangd","2015款 2.4 手动 舒适型,changange","2015款 2.7 手动 尊享型,changangf",
							"2015款 2.7 手动 尊贵型,changangg","2015款 2.7 手动 尊荣型,changangh"
							],		
							
							
					"志翔":["2007款 2.0 手动,changanha","2008款 1.6 手动 舒适型,changanhb","2008款 1.6 手动 标准型,changanhc",
							"2008款 2.0 手动 豪华型,changanhd","2008款 2.0 手动 精英型,changanhe","2008款 2.0 手动 舒适型,changanhf",
							"2008款 2.0 自动 豪华型,changanhg","2008款 2.0 自动 精英型,changanhh","2009款 1.6 手动 CNG油气混合,changanhi",
							"2011款 1.5 手动 油电混合,changanhj"
							],			
							
					"悦翔":["2009款 1.5 手动 尊贵型,changania","2009款 1.5 手动 舒适型,changanib","2009款 1.5 手动 基本型,changanic","2009款 1.5 手动 豪华型,changanid",
							"2010款 1.5 手动 两用燃料型,changanie","2010款 1.5 手动 运动型,changanif","2010款 1.5 自动 尊贵型,changanig",
							"2010款 1.5 自动 豪华型,changanih","2010款 1.5 手动 尊贵型,changanii","2010款 1.5 手动 舒适型,changanij","2010款 1.5 手动 豪华型,changanik",
							"2010款 1.5 自动 尊贵型,changanil","2010款 1.5 自动 舒适型,changanim","2010款 1.5 自动 豪华型,changanin",                  
							"2011款 1.5 自动 豪华型,changanio","2011款 1.5 手动 尊贵型,changanip","2011款 1.5 手动 豪华型,changaniq",
							"2012款 1.5 手动 舒适型油气混合,changanir","2012款 1.5 手动 V5运动型,changanis","2012款 1.5 手动 V5梦幻型,changanit",
							"2012款 1.5 自动 V5梦幻型,changaniu","2012款 1.5 手动 尊贵型,changaniv","2012款 1.5 手动 运动型,changaniw",
							"2012款 1.5 手动 舒适型,changanix","2012款 1.5 手动 豪华型,changaniy","2012款 1.5 自动 V5运动型,changaniz",
							"2012款 1.3 手动 V3豪华型,changaniaa","2012款 1.3 手动 V3舒适型,changaniab","2012款 1.3 手动 V3标准型,changaniac",
							"2012款 1.5 手动 3G型,changaniad","2015款 1.4 手动 V3幸福型,changaniae","2015款 1.4 手动 V3温馨型,changaniaf","2015款 1.4 手动 V3美满型,changaniag"
							],
							
					
					"星光":["2006款 1.3 手动,changanja","2006款 1.0 手动 加长型,changanjb","2006款 1.0 手动 豪华型,changanjc","2006款 1.0 手动 标准型,changanjd",
							"2007款 0.8 手动,changanje","2008款 1.0 手动 5-8座,changanjf","2009款 1.0 手动,changanjg","2009款 1.0 手动 5座,changanjh",
							"2009款 1.3 手动 基本型7座,changanji","2010款 0.8 手动 SC6390A3,changanjj","2011款 1.5 手动 标准型7座,changanjk",
							"2012款 1.3 手动 快运版5座,changanjl","2012款 1.5 手动 标准型7 座油气混合,changanjm","2012款 1.5 手动 标准型7-8座 油气混合,changanjn",
							"2012款 1.3 手动 基本型9座,changanjo","2012款 1.3 手动 标准型9座,changanjp","2012款 1.3 手动 基本型7座,changanjq",
							"2012款 1.3 手动 基本型7-8座,changanjr","2012款 1.3 手动 标准型7座,changanjs","2012款 1.3 手动 标准型7-8座,changanjt",          
							"2013款 1.0 手动 6座,changanju","2013款 1.0 手动 5座,changanjv","2013款 1.0 手动 5座,changanjw"
							],
					
						
					"星光小卡":["2013款 1.3 手动 SC1029SA4,changanka"],		

					
					"星韵":["2004款 1.0 手动,changanla","2004款 1.0 手动 ARH,changanlb","2004款 1.0 手动 A-RHK,changanlc",
							"2004款 1.0 手动 B-FAA,changanld","2004款 1.0 手动 B-FHA,changanle","2004款 1.0 手动 B-RHA,changanlf",
							"2004款 1.3 手动,changanlg","2004款 1.3 手动 FXA,changanlh","2004款 1.3 手动 RBA,changanli",
							"2006款 1.0 手动,changanlj","2006款 1.0 手动 N,changanlk"
							],			
							
					"杰勋":["2006款 2.0 自动,changanma","2006款 2.0 手动,changanmb","2007款 1.5 手动 油电混合,changanmc","2007款 2.0 手动 舒适型,changanmd",
							"2007款 2.0 自动 豪华型,changanme","2007款 2.0 自动 超豪华型,changanmf","2007款 2.0 自动 舒适型,changanmg","2007款 2.0 手动 标准型,changanmh",
							"2008款 1.5 手动 油电混合,changanmi","2008款 2.0 手动 舒适型,changanmj",
							"2008款 2.0 手动 标准型,changanmk","2008款 2.0 自动 舒适型,changanml","2008款 2.0 自动 豪华型,changanmm","2008款 2.0 自动 超豪华型,changanmn",
							"2009款 1.5 手动 舒适型 油电混合,changanmo","2009款 1.5 手动 豪华型 油电混合,changanmp"
							],
							     		
					"欧力威":	["2013款 1.4 手动 劲享型,changanna","2013款 1.4 手动 劲悦型,changannb","2013款 1.2 手动 精英型,changannc","2013款 1.2 手动 舒适型,changannd",
							"2014款 1.2 手动 畅享版,changanne","2014款 1.4 自动 X6旗舰型,changannf","2014款 1.4 手动 X6尊享型,changanng","2014款 1.4 自动 豪华型,changannh",
							"2014款 1.4 自动 优尚型,changanni","2014款 1.4 手动 劲悦型,changannj","2014款 1.4 手动 劲享型,changannk","2015款 1.4 手动 X6珠峰型,changannl",
							"2015款 1.2 手动 畅享型,changannm"
							],   	
					
					"欧诺":["2012款 1.3 手动 精英型5座,changanoa","2012款 1.3 手动 基本型5座,changanob",
							"2012款 1.3 手动 标准型5座,changanoc","2012款 1.5 手动 运动款豪华型,changanod","2012款 1.5 手动 运动款精英型,changanoe",
							"2012款 1.3 手动 商务款精英型,changanof","2012款 1.5 手动 商务款标准型,changanog","2012款 1.3 手动 商务款标准型,changanoh",
							"2012款 1.3 手动 商务款基本型,changanoi","2014款 1.5 手动 精英型8座,changanoj","2014款 1.5 手动 基本型8座,changanok",
							"2014款 1.3 手动 幸福型5座,changanol","2014款 1.3 手动 基本型5座,changanom","2014款 1.3 手动 幸福型7座,changanon","2014款 1.3 手动 精英型5座,changanoo",
							"2014款 1.5 手动 精英型5座,changanop","2014款 1.5 手动 基本型5座,changanoq","2014款 1.5 手动 豪华型7座,changanor",
							"2014款 1.5 手动 精英型7座,changanos","2014款 1.5 手动 基本型7座,changanot","2014款 1.3 手动 精英型7座,changanou",
							"2014款 1.3 手动 基本型7座,changanov","2015款 1.5 手动 基本型7座 双燃料,changanow","2015款 1.5 手动 基本型5座 双燃料,changanox",
							"2015款 1.5 手动 精英型8座,changanoy","2015款 1.5 手动 标准型8座,changanoz","2015款 1.3 手动 基本型7座,changanoaa","2015款 1.3 手动 基本型5座,changanoab",
							"2015款 1.5 手动 豪华型7座,changanoac","2015款 1.5 手动 精英型7座,changanoad","2015款 1.5 手动 标准型7座,changanoae"
							],
					
					"长安欧雅":["2001款 1.0 手动,changanpa","2001款 0.8 手动,changanpb"],	
					
					
					"睿行":["2014款 1.5 手动 舒适型6座,changanqa","2014款 1.5 手动 经典型6座,changanqb","2014款 1.5 手动 标准型5座 油气混合,changanqc",
							"2014款 1.5 手动 舒适型5座 油气混合,changanqd","2014款 1.5 手动 舒适型5座,changanqe","2014款 1.5 手动 标准型5座,changanqf",
							"2014款 1.5 手动 经典型7座,changanqg","2014款 1.5 手动 舒适型7座,changanqh","2014款 1.5 手动 标准型7座,changanqi",
							"2014款 1.5 手动 舒适型7座 油气混合,changanqj","2014款 1.5 手动 经典型9座 油气混合,changanqk","2014款 1.5 手动 经典型9座,changanql",
							"2014款 1.5 手动 基本型4座,changanqm","2014款 1.5 手动 基本型6座 油气混合,changanqn","2014款 1.5 手动 标准型6座,changanqo",
							"2014款 1.5 手动 基本型2座,changanqp"
							],
							
					"睿骋":["2013款 2.0 手动 舒适型,changanra","2013款 2.0 自动 豪华型,changanrb","2013款 1.8T 自动 领航版,changanrc",
							"2013款 1.8T 自动 尊崇型,changanrd","2013款 1.8T 自动 旗舰型,changanre","2013款 1.8T 自动 尊贵型,changanrf","2013款 1.8T 自动 精英型,changanrg",
							"2014款 1.8T 自动 周年版旗舰型,changanrh","2014款 1.8T 自动 周年版尊贵型,changanri","2014款 1.8T 自动 周年版精英型,changanrj",
							"2014款 2.0 自动 周年版豪华型,changanrk","2014款 2.0 手动 周年版舒适型,changanrl"
							], 		
							
					"绿色新星":["2003款 1.0 手动 RN,changansa","2003款 1.0 手动 FHK,changansb","2003款 1.0 手动 RP,changansc",
							"2003款 1.0 手动 RNK,changansd","2003款 1.0 手动 RPK,changanse","2003款 1.0 手动 FH,changansf",
							"2005款 1.0 手动 HP,changansg","2005款 1.0 手动 HPK,changansh","2006款 1.0 手动,changansi"
							],
							
					"致尚XT":["2013款 1.6 手动 致酷型,changanta","2013款 1.6 手动 尚酷型,changantb","2013款 1.6 手动 俊酷型,changantc","2013款 1.6 自动 致酷型,changantd",
							"2013款 1.6 自动 俊酷型,changante","2013款 1.5T 自动 劲酷型,changantf","2013款 1.6 手动 致酷型,changantg","2013款 1.6 手动 尚酷型,changanth",
							"2013款 1.6 手动 俊酷型,changanti","2013款 1.6 自动 致酷型,changantj","2013款 1.6 自动 俊酷型,changantk","2013款 1.5T 自动 炫酷型,changantl",              
							"2015款 1.5T 自动 劲酷型,changantm","2015款 1.5T 自动 炫酷型,changantn","2015款 1.6 自动 锐酷型,changanto","2015款 1.6 自动 致酷型,changantp",
							"2015款 1.6 自动 俊酷型,changantq","2015款 1.6 手动 锐酷型,changantr","2015款 1.6 手动 致酷型,changants","2015款 1.6 手动 俊酷型,changantt",
							"2015款 1.6 手动 尚酷型,changantu"
							],
  		
					"运动星":["2001款 1.0 手动,changanua","2002款 1.0 手动 A-RHA,changanub","2002款 1.0 手动 FBA,changanuc","2002款 1.0 手动 A-FWA,changanud",              
							"2003款 1.3 手动 FCA,changanue","2003款 1.3 手动 FXA,changanuf","2003款 1.3 手动 RCA,changanug",
							"2003款 1.3 手动 RBK,changanuh","2003款 1.3 手动 RGK运动风暴,changanui","2003款 1.0 手动 A-RGK,changanuj",
							"2003款 1.0 手动 A-RG,changanuk","2003款 1.0 手动 A-FHA,changanul",
							"2005款 1.0 手动 R2A运动升级版,changanum","2005款 1.3 手动 R1,changanun","2005款 1.3 手动 R1K运动风暴升级版,changanuo",
							"2005款 1.0 手动 R2A运动都市版,changanup","2005款 1.0 手动 R1K运动升级版,changanuq","2005款 1.0 手动 R1运动升级版,changanur"
							],		
							
					"运通":["2001款 0.8 手动 G-J2,changanva","2004款 1.0 手动 G-L4-L4K,changanvb","2004款 1.0 手动 G-L3-L3K,changanvc",
							"2005款 0.8 手动 G-L3,changanvd","2005款 0.8 手动 G-L4,changanve","2005款 0.8 手动 G-JX,changanvf"
							],		
					
					"逸动":["2011款 1.6 手动 尊贵型,changanwa","2011款 1.6 手动 豪华型,changanwb","2011款 1.6 手动 舒适型,changanwc",
							"2012款 1.6 手动 尊贵型,changanwd","2012款 1.6 自动 尊贵型,changanwe","2012款 1.6 自动 豪华型,changanwf",
							"2012款 1.6 手动 豪华型,changanwg","2012款 1.6 自动 舒雅型,changanwh","2012款 1.6 手动 舒雅型,changanwi",
							"2012款 1.6 手动 舒适型,changanwj","2013款 1.6 手动 豪华型,changanwk","2013款 1.6 自动 精英型,changanwl",
							"2013款 1.6 手动 精英型,changanwm","2013款 1.6 手动 舒适型,changanwn","2014款 1.6 手动 尊贵型,changanwo",
							"2014款 1.6 自动 尊贵型,changanwp","2014款 1.6 自动 豪华型,changanwq","2014款 1.6 手动 豪华型,changanwr",
							"2014款 1.6 自动 精英型,changanws","2014款 1.6 手动 精英型,changanwt","2014款 1.5T 自动 运动尊贵型,changanwu",
							"2014款 1.5T 自动 运动豪华型,changanwv","2015款 1.5T 自动 运动尊贵型,changanww","2015款 1.5T 自动 运动豪华型,changanwx",
							"2015款 1.6 自动 旗舰型,changanwy","2015款 1.6 手动 旗舰型,changanwz","2015款 尊贵型纯电动,changanwaa","2015款 精英型纯电动,changanwab",
							"2015款 1.6 自动 尊贵型,changanwac","2015款 1.6 手动 尊贵型,changanwad","2015款 1.6 自动 豪华型,changanwae",
							"2015款 1.6 手动 豪华型,changanwaf","2015款 1.6 自动 精英型,changanwag","2015款 1.6 手动 精英型,changanwah"
							],

					"都市彩虹":["2005款 1.0 手动 FA,changanxa","2005款 1.0 手动 FAA带空调,changanxb","2006款 1.3 手动 加长型,changanxc",
							"2006款 1.0 手动 FA,changanxd","2006款 1.0 手动 FAA带空调,changanxe","2006款 1.0 手动 FAA,changanxf",
							"2008款 1.0 手动,changanxg","2008款 1.3 手动 加长型,changanxh"
							],	
					
					"金牛星":["2011款 1.3 手动 标准型,changanya","2011款 1.3 手动 舒适型,changanyb","2011款 1.3 手动 精英型,changanyc",
							"2011款 1.3 手动 至尊型,changanyd","2011款 1.2 手动 标准型,changanye","2011款 1.2 手动 舒适型,changanyf",
							"2011款 1.2 手动 精英型,changanyg","2011款 1.2 手动 至尊型,changanyh","2013款 1.2 手动 精英型,changanyi",
							"2013款 1.2 手动 标准型,changanyj","2013款 1.2 手动 舒适型,changanyk"
							],	
					
					"长安之星":["2005款 1.3 手动 R1K,changanza","2006款 1.3 手动 MK9,changanzb","2006款 1.0 手动 R1,changanzc","2006款 1.0 手动 R1K,changanzd",
							"2006款 1.0 手动 R6A,changanze","2006款 1.0 手动 R6,changanzf","2007款 1.0 手动 M6,changanzg","2007款 1.0 手动,changanzh",
							"2007款 1.3 手动 运动风暴升级型,changanzi","2007款 1.3 手动 带空调,changanzj","2007款 1.0 手动 简配型,changanzk",
							"2008款 1.0 手动 35舒适型,changanzl","2008款 1.0 手动 35基本型,changanzm","2008款 0.8 手动 35基本型,changanzn",
							"2008款 1.0 手动,changanzo","2008款 1.0 手动 带空调,changanzp","2008款 1.3 手动 商务型,changanzq","2008款 1.3 手动 基本型,changanzr",
							"2008款 1.3 手动 标准型,changanzs","2008款 1.3 手动 MK9,changanzt","2008款 1.3 手动 运动风暴升级型,changanzu","2008款 1.3 手动 加长型,changanzv",
							"2008款 1.3 手动 带空调,changanzw","2008款 1.0 手动 升级型,changanzx","2008款 1.0 手动 加长型,changanzy","2008款 1.0 手动 豪华型,changanzz",
							"2008款 1.0 手动 选装型,changanzaa","2008款 1.0 手动 增配型,changanzab","2008款 1.0 手动 标准型,changanzac","2008款 1.0 手动 标配型,changanzad",
							"2008款 1.0 手动 带空调,changanzae","2008款 1.0 手动,changanzaf","2009款 1.0 手动 带空调,changanzag","2009款 1.0 手动 SC6399基本型7座,changanzah",
							"2009款 1.0 手动 SC6399B3S,changanzai","2009款 1.0 手动 5-8座,changanzaj","2009款 1.0 手动 SC6363B4Y 5-8座,changanzak",
							"2009款 1.0 手动 SC6363BV4 5-8座,changanzal","2009款 1.0 手动 SC6363BV3S 5-8座,changanzam","2009款 1.0 手动 SC6363B4 5-8座,changanzan",
							"2009款 1.0 手动 SC6363B 5-8座,changanzao","2009款 1.0 手动 舒适型7座,changanzap","2009款 1.0 手动 标准型7座,changanzaq","2007款 1.0 手动 5座,changanzar",
							"2009款 1.3 手动 SC6378D4 5-8座,changanzas","2009款 1.3 手动 SC6378D3 5-8座,changanzat","2009款 1.0 手动 SC6378A4S 5-8座,changanzau",
							"2009款 1.0 手动 SC6378A3S 5-8座,changanzav","2010款 1.0 手动 带空调,changanzaw","2011款 1.0 手动 标准型7座,changanzax","2011款 1.0 手动 基本型7座,changanzay",
							"2011款 1.3 手动 基本型7座,changanzaz","2011款 1.3 手动 标准型7座,changanzba","2011款 1.0 手动 基本型5座油气混合,changanzbb","2011款 1.2 手动 基本型8座油气混合,changanzbc",
							"2011款 1.2 手动 基本型7座油气混合,changanzbd","2011款 1.0 手动 基本型7座油气混合,changanzbe","2012款 1.3 手动 5座油气混合,changanzbf",
							"2012款 1.3 手动 7座油气混合,changanzbg","2012款 1.0 手动 基本型7座,changanzbh","2012款 1.0 手动 基本型5座,changanzbi","2012款 1.0 手动 超值版8座,changanzbj",
							"2012款 1.0 手动 基本型8座,changanzbk","2012款 1.0 手动 SC6363AV4S 5座,changanzbl","2013款 1.2 手动 基本型5座,changanzbm","2013款 1.2 手动 标准型5座,changanzbn",
							"2013款 1.0 手动 SC6368优化版7座,changanzbo","2013款 1.2 手动 基本型7-8座,changanzbp","2013款 1.2 手动 标准型7-8座,changanzbq","2013款 1.2 手动 标准型,changanzbr",
							"2013款 1.2 手动 基本型,changanzbs","2013款 1.3 手动 SC6378D4 7座,changanzbt","2013款 1.0 手动 SC6378A4S 7座,changanzbu",                                 
							"2014款 1.4 手动 舒适型7座,changanzbv","2014款 1.4 手动 精英型7座,changanzbw","2014款 1.4 手动 基本型7座,changanzbx","2014款 1.4 手动 精英型5座,changanzby",
							"2014款 1.4 手动 舒适型5座,changanzbz","2014款 1.4 手动 基本型5座,changanzca","2014款 1.5 手动 基本型8座,changanzcb","2014款 1.5 手动 基本型8座双燃料,changanzcc",
							"2014款 1.5 手动 标准型8座,changanzcd","2015款 1.5 手动 基本型5座,changanzce","2015款 1.5 手动 标准型7座,changanzcf","2015款 1.5 手动 标准型5座,changanzcg",
							"2015款 1.5 手动 基本型5座双燃料,changanzch","2015款 1.5 手动 基本型9座,changanzci","2015款 1.5 手动 基本型7座,changanzcj","2015款 1.0 手动 基本型空调,changanzk",
							"2015款 1.0 手动 基本型非空调,changanzcl"
							],
					
					"跨越新豹":["2013款 1.8T 手动 双排 柴油,changanaaa","2013款 1.8T 手动 双排栏板式 柴油,changanaab","2013款 1.8T 手动 单排自卸栏板式 柴油,changanaac",
							"2013款 1.0 手动 单排栏板式后驱SC1021CD31,changanaad","2013款 1.0 手动 单排栏板式后驱,changanaae","2013款 1.8T 手动 双排栏板式后驱 柴油,changanaaf",
							"2014款 1.2 手动 双排栏板式后驱,changanaag","2015款 1.0 手动 MINI双排465,changanaah","2015款 1.0 手动 MINI双排466,changanaai",
							"2015款 1.0 手动 MINI双排长轴465,changanaaj","2015款 1.0 手动 MINI双排长轴466,changanaak"
							],	
					
					"星卡":["2007款 1.0 手动 SC1022S7,changanaba","2012款 1.3L 手动 1029基本型,changanabb","2012款 1.3L 手动 1029标准型,changanabc","2012款 1.0 手动 SC1022S4N4双排后驱,changanabd",
							"2012款 1.0 手动 SC1026DAN4单排后驱,changanabe","2012款 1.0 手动 SC1022DBN单排后驱,changanabf","2012款 1.0 手动 SC1022DB4N单排后驱,changanabg",
							"2012款 1.0 手动 SC1022SAN双排后驱,changanabh","2012款 1.0 手动 SC1022S4N双排后驱,changanabi","2013款 1.0 手动 S201标准型,changanabj",
							"2013款 1.0 手动 S201基本型,changanabk","2013款 1.2 手动 基本型D201后驱,changanabl","2013款 1.0 手动 SC5026XXYDA4单排后驱,changanabm",                 
							"2014款 1.2 手动 基本型双排,changanabn","2014款 1.3 手动 SC1031DS42后驱,changanabo","2014款 1.2 手动 标准型双排后驱,changanabp",
							"2014款 1.2 手动 标准型加长后驱,changanabq","2014款 1.2 手动 基本型加长后驱,changanabr"
							],
					
					"镭蒙":["2004款 1.0 手动 NFK,changanaca","2004款 1.0 手动 NA,changanacb","2004款 1.0 手动 NBA,changanacc",
							"2004款 1.0 手动 NF,changanacd","2005款 1.3 手动 NCA,changanace","2007款 1.3 手动 NCA,changanacf",
							"2007款 1.0 手动,changanacg"
							],
					
					"雪虎":["2001款 1.3 手动,changanada","2004款 1.3 手动 豪华型,changanadb"
							],
							
					"神琪":["2015款 1.3 手动 标准型双排载货车,changanaea"
							],		


//"长城":
					"长城M2":["2010款 1.5L 手动豪华型,changchengaa","2010款 1.5L 手动精英型,changchengab","2010款 1.5L 手动天窗型,changchengac",
							"2010款 1.5L CVT豪华型,changchengad","2010款 1.5L CVT天窗型,changchengae","2012款 1.5L CVT都市版,changchengaf",
							"2012款 1.5L 手动都市版,changchengag","2013款 1.5L 手动导航版,changchengah"
							],
					
					"长城C30":["2010款 1.5L 手动精英型,changchengba","2010款 1.5L CVT豪华型,changchengbb","2010款 1.5L CVT舒适型,changchengbc",
							"2010款 1.5L 手动豪华型,changchengbd","2010款 1.5L 手动舒适型,changchengbe","2012款 1.5L 手动精英型,changchengbf",
							"2012款 1.5L 手动舒适型,changchengbg","2012款 1.5L 手动豪华型,changchengbh","2013款 1.5L 手动舒适型,changchengbi",
							"2013款 1.5L 手动豪华型,changchengbj"
							],
					
					"长城C50":["2012款 1.5T 手动尊贵型,changchengca","2012款 1.5T 手动豪华型,changchengcb","2012款 1.5T 手动精英型,changchengcc",
							"2012款 1.5T 手动时尚型,changchengcd","2013款 1.5T 手动精英型,changchengce"
							],
					
					"长城M4":["2012款 1.5L 手动精英型,changchengda","2012款 1.5L 手动豪华型,changchengdb","2012款 1.5L 手动舒适型,changchengdc",
							"2014款 1.5L 手动舒适型,changchengdd","2014款 1.5L 手动豪华型,changchengde"
							],
					
					"风骏5":["2011款 2.4L公务版 豪华型 两驱小双排,changchengea","2011款 2.4L公务版 豪华型 两驱大双排,changchengeb",
							"2011款 2.4L公务版 豪华型 四驱小双排,changchengec"
							],
					
					"风骏3":["2011款 2.4L公务版 豪华型 四驱小双排,changchengfa","2011款 2.4L公务版 尊贵型 两驱小双排,changchengfb"
							],
					
					"长城精灵":["2008款 1.3 手动 实用型,changchengga","2008款 1.3 手动 豪华型,changchenggb","2008款 1.3 手动 精英型,changchenggc",
							"2008款 1.3 手动 舒适型,changchenggd","2009款 1.3 手动 豪华型,changchengge","2009款 1.3 手动 进取型,changchenggf",
							"2009款 1.3 手动 精英型,changchenggg","2009款 1.3 手动 舒适型,changchenggh","2010款 1.3 手动 CORSS豪华型,changchenggi",
							"2010款 1.3 手动 CORSS舒适型,changchenggj"
							],
					
					"酷熊":["2009款 1.5L 手动精英型,changchengha","2009款 1.5L 手动豪华型,changchenghb","2009款 1.5L 手动天窗版,changchenghc"
							],
							
					"炫丽":["2008款 1.5VVT 精英型,changchengia","2008款 1.3L 豪华型,changchengib","2008款 1.5VVT 豪华型,changchengic","2009款 1.3VVT 豪华型,changchengid",
							"2009款 1.3VVT 精英型,changchengie","2009款 CROSS 1.3VVT,changchengif","2009款 CROSS 1.5VVT,changchengig","2010款 CROSS 1.5L 冠军版,changchengih",
							"2010款 CROSS 1.3L 冠军版,changchengii","2010款 CROSS 1.3L AMT,changchengij","2011款 CROSS 1.5L MT,changchengik","2011款 CROSS 1.5L AMT,changchengil"
							],		
							
					"长城M1":["2009款 1.3L 两驱豪华型,changchengja","2009款 1.3L 两驱精英型,changchengjb","2009款 1.3L 两驱舒适型,changchengjc",
							 "2010款 1.3L 四驱豪华型,changchengjd"
							],
									
					"赛弗":["2004款 2.2L 两驱豪华型,changchengka"
							],
					
					"长城V80":["2011款 2.0L 自动实用型,changchengla","2012款 1.5T 手动实尚型,changchenglb","2012款 1.5T 手动雅尚型,changchenglc",
							 "2012款 1.5T 手动尊尚型,changchengld","2013款 1.5T 手动实尚型,changchengle","2013款 1.5T 手动雅尚型,changchenglf",
							 "2013款 1.5T 手动尊尚型,changchenglg"
							],
							
					"嘉誉":["2009款 2.0L 手动舒适型,changchengma"
							],		

//"昌河"				
					"海豚":["2003款 0.9 手动,changheaa","2003款 1.0 手动,changheab","2003款 1.1 手动 16V,changheac",
							 "2003款 1.1 手动 8V,changhead"
							],
					
					"海象":["2002款 1.1 手动,changheba","2002款 0.9 手动,changhebb","2002款 0.8 手动,changhebc",
							 "2002款 1.0 手动,changhebd"
							],
					
					
					"福瑞达":["2006款 1.1 手动 钢板弹簧,changheca","2006款 1.1 手动 标准型钢板弹簧,changhecb","2006款 1.1 手动 豪华型钢板弹簧,changhecc",
							"2006款 1.3 手动 豪华型,changhecd","2006款 1.3 手动 标准型,changhece","2007款 1.1 手动 豪华型钢板弹簧,changhecf",
							"2007款 1.1 手动 标准型钢板弹簧,changhecg","2007款 1.1 手动 标准型平板弹簧,changhech","2008款 1.1 手动 豪华型钢板弹簧,changheci",
							"2008款 1.1 手动 标准型钢板弹簧,changhecj","2008款 1.1 手动 标准型平板弹簧,changheck","2009款 1.2 手动 加长版舒适型,changhecl",
							"2009款 1.2 手动 加长版实用型,changhecm","2009款 1.2 手动 舒适型7座,changhecn","2009款 1.2 手动 实用型7座,changheco",
							"2009款 1.0 手动 鸿运版STD型,changhecp","2009款 1.0 手动 鸿运版EC型,changhecq","2009款 1.0 手动 加长版舒适型,changhecr",
							"2009款 1.0 手动 加长版实用型,changhecs","2009款 1.0 手动 鸿运版标准型,changhect","2010款 1.0 手动 鸿运版标准型,changhecu",
							"2010款 1.1 手动 鸿运版豪华型,changhecv","2010款 1.1 手动 鸿运版标准型,changhecw","2011款 1.0 手动 鸿运版EC型,changhecx",
							"2013款 1.4 手动 双排DLX加长加高型K14B-A,changhecy","2013款 1.4 手动 双排STD加长加高型K14B-A,changhecz",
							"2013款 1.4 手动 双排DLX加长型K14B-A,changhecaa","2013款 1.4 手动 双排STD加长型K14B-A,changhecab",
							"2013款 1.4 手动 双排DLX型K14B-A,changhecac","2013款 1.4 手动 双排STD型K14B-A,changhecad",
							"2013款 1.4 手动 单排DLX型K14B-A,changhecae","2013款 1.4 手动 单排STD型K14B-A,changhecaf",
							"2014款 1.2 手动 鸿运版标准型7座,changhecag","2014款 1.0 手动 鸿运版标准型7座,changhecah","2014款 1.0 手动 鸿运版经济型7座,changhecai",
							"2014款 1.0 手动 双排标准后驱,changhecaj","2014款 1.0 手动 单排豪华型后驱,changhecak","2014款 1.0 手动 单排标准型后驱,changhecal"
							],
					
					"爱迪尔":["2004款 1.1 手动 标准型,changheda","2005款 1.1 手动 阳光版,changhedb","2005款 1.0 手动 简配型,changhedc",
							"2005款 1.0 手动 标准型,changhedd","2006款 1.1 手动 Ⅱ,changhede","2006款 1.1 手动 阳光版,changhedf",
							"2006款 1.0 手动 Ⅱ标准型,changhedg","2006款 1.1 手动 Ⅱ豪华型,changhedh","2006款 1.3 手动 Ⅱ豪华型,changhedi",
							"2007款 1.1 手动 Ⅱ,changhedj","2007款 1.1 手动 阳光版,changhedk","2007款 1.1 手动 Ⅱ豪华型,changhedl",
							"2007款 1.0 手动 Ⅱ标准型,changhedm","2007款 1.4 手动 Ⅱ标准型,changhedn","2007款 1.4 手动 Ⅱ经济型,changhedo",
							"2008款 1.1 手动 Ⅱ标准型,changhedp","2008款 1.4 手动 Ⅱ标准型,changhedq","2008款 1.0 手动 Ⅱ标准型,changhedr",
							"2009款 1.0 手动 Ⅱ标准型,changheds","2011款 1.0 手动 A+标准型,changhedt","2011款 1.0 手动 A+基本型,changhedu"
							],
												
					"骏马":["2005款 1.0 手动,changheea","2005款 0.8 手动,changheeb","2005款 1.1 手动,changheec"
							],
					
					
					"福运":["2009款 1.2 手动 基本型8座,changhefa","2009款 1.2 手动 标准型8座,changhefb","2012款 1.2 手动 标准型7座,changhefc",
							 "2012款 1.2 手动 基本型5座,changhefd"
							],
					
					"福瑞达M50":["2014款 1.5 手动 商务舱7座,changhega","2014款 1.5 手动 公务舱7座,changhegb","2015款 1.5 手动 S豪华舱,changhegc",
								"2015款 1.5 手动 S头等舱,changhegd","2015款 1.5 手动 S公务舱,changhege","2015款 1.5 手动 S商务舱,changhegf",
								"2015款 1.5 手动 实用舱7座,changhegg","2015款 1.5 手动 公务舱5座,changhegh","2015款 1.5 手动 商务舱5座,changhegi",
								"2015款 1.5 手动 实用舱5座,changhegj","2015款 1.5 手动 商务舱7座,changhegk","2015款 1.5 手动 公务舱7座,changhegl"
								],

//"道奇":
					
					
					"凯领":["2007款 3.0 自动 至尊豪华座椅版7座,daoqiaa","2007款 3.0 自动 豪华版7座,daoqiab","2007款 3.0 自动 经典版7座,daoqiac",
							 "2007款 3.0 自动 至尊版7座,daoqiad", "2007款 3.3 自动,daoqiae", "2007款 3.8 自动,daoqiaf"
							],
					
					
					"锋哲":["2007款 3.5 自动 R/T,daoqiba","2007款 3.5 自动 R/T四驱,daoqibb","2007款 2.4 自动,daoqibc",
							 "2007款 2.7 自动,daoqibd", "2007款 3.5 自动,daoqibe", "2007款 2.4 自动 SXT,daoqibf","2008款 2.4 自动 SE,daoqibg"
							],
					
					"酷搏":["2006款 1.8 自动 SE,daoqica","2006款 2.4 手动,daoqicb","2006款 2.0 手动,daoqicc","2006款 1.8 手动,daoqicd","2006款 2.4 自动 R/T,daoqice",
							"2006款 2.0 自动 SXT,daoqicf","2007款 2.4T 手动 SRT4,daoqicg","2009款 2.0 自动 豪华型,daoqich","2009款 2.0 自动 运动型SXT,daoqici",
							"2009款 1.8 自动 SXT,daoqicj","2009款 2.4 自动 R/T,daoqick","2011款 2.0 自动 舒适版SXT,daoqicl","2011款 2.0 自动 经典版SXT,daoqicm",
							"2011款 2.0 自动 豪华导航版SXT,daoqicn"
							],
					
					"蝰蛇":["2007款 8.4 手动 SRT10-Roadster,daoqida"],
					
					"酷威":["2008款 3.5 自动 R/T前驱,daoqiea","2008款 3.5 自动 SXT四驱,daoqieb","2008款 3.5 自动 SXT前驱,daoqiec","2008款 2.4 自动 SE前驱,daoqied",
							"2009款 2.7 自动 前驱,daoqiee","2010款 2.7 自动 前驱,daoqief","2011款 2.7 自动 豪华导航版前驱,daoqieg","2013款 3.6 自动 旗舰版四驱,daoqieh",
							"2015款 2.0T 自动 尊尚版四驱 柴油,daoqiei","2014款 2.4 自动 旅行版前驱,daoqiej","2013款 2.4 自动 尊尚版前驱,daoqiek","2013款 2.4 自动 智尚版SXT前驱,daoqiel"
							],
					
					"Challenger":["2015款 3.6 自动 基本型 美规版,daoqifa"],
					
					"翼龙":["2006款 3.7 自动 ,daoqiga"],
				
					"公羊":["2002款 5.2 自动 Conversion Van ,daoqiha"],
					
					"Ram":["2009款 3.7 自动 1500,daoqiia","2009款 4.7 自动 1500 Flex Fuel,daoqiib","2009款 5.7 自动 1500 HEMI,daoqiic",
						   "2010款 5.7 自动 1500 Sport,daoqiid", "2010款 5.7 自动 1500 Laramie,daoqiie",
						   "2010款 5.7 自动 1500 Laramie Longhorn,daoqiif"
							],

//"大宇"

					
					"典雅":["2003款 2.0 自动 ,dayuaa"],
					
					"旅行家":["2004款 1.6 自动 ,dayuba"],
					
					"马蒂兹":["2005款 1.0 手动 ,dayuca","2005款 0.8 手动,dayucb"],

					
//"大迪"		
					
					"都市威菱":["2005款 2.2 手动 超豪华型后驱,dadiaa","2005款 2.2 手动 豪华型后驱,dadiab","2005款 2.4 手动 超豪华型四驱,dadiac","2005款 2.2T 手动 超豪华型后驱 柴油,dadiad",
							"2005款 2.2T 手动 豪华型后驱 柴油,dadiae","2006款 2.4 手动 超豪华型四驱,dadiaf","2006款 2.2 手动 豪华型后驱,dadiag",
							 "2007款 2.2 手动 豪华型后驱,dadiah","2007款 2.8T 手动 豪华型后驱 柴油,dadiai","2007款 2.8T 手动 超豪华型后驱 柴油,dadiaj",
							"2007款 2.2 手动 超豪华型后驱,dadiak","2007款 2.4 手动 豪华型四驱,dadial","2007款 2.4 手动 超豪华型四驱,dadiam"
							],                 
							
							
							
					"都市骏马":["2005款 2.4 手动 超豪华型四驱,dadiba","2005款 2.2 手动 超豪华型后驱,dadibb","2005款 2.8T 手动 豪华型后驱 柴油,dadibc",
							"2005款 2.2 手动 豪华型后驱,dadibd","2005款 2.8T 手动 超豪华型后驱 柴油,dadibe",                                          
							"2008款 2.4 手动 超豪华型四驱,dadibf","2008款 2.2 手动 豪华型后驱,dadibg","2008款 2.2 手动 超豪华型后驱,dadibh"
							],
							
							                                                          
					"大迪霸道":["2005款 2.8T 手动 超豪华型后驱 柴油,dadica","2005款 2.2 手动 超豪华型后驱,dadicb","2007款 2.2 手动 豪华型四驱,dadicc",
							"2007款 2.2 手动 豪华型后驱,dadicd","2007款 2.2 手动 超豪华型四驱,dadice","2007款 2.8T 手动 豪华型后驱 柴油,dadicf",
							"2008款 2.2 手动 豪华型后驱,dadicg","2008款 2.2 手动 超豪华型四驱,dadich"
							],                  
							
							                                           
					"顺驰":["2006款 2.2 手动 豪华型后驱,dadida","2006款 2.2T 手动 豪华型后驱 柴油,dadidb","2006款 2.2 手动 超豪华型后驱,dadidc",
							"2006款 2.2T 手动 超豪华型后驱 柴油,dadidd"
							],

					
//"东南"			
					
					
					"菱利":["2006款 1.6 手动 舒适型,dongnanaa","2006款 1.6 手动 豪华型,dongnanab","2006款 1.3 手动 舒适型,dongnanac",
						   "2006款 1.3 手动 基本型,dongnanad"],
					
				
					"富利卡":["2005款 2.0 手动 超豪华型四驱,dongnanba","2005款 2.0 手动 豪华型四驱,dongnanbb","2005款 2.0 手动 真皮超值型后驱,dongnanbc"
							],
					
					"菱帅":["2005款 1.6 自动 舒适型,dongnanca","2005款 1.6 自动 豪华型,dongnancb","2005款 1.6 自动 标准型,dongnancc",
							"2005款 1.6 手动 舒适型,dongnancd","2005款 1.6 手动 基本型,dongnance","2005款 1.6 手动 超值型,dongnancf",
							"2005款 1.6 手动 舒适型,dongnancg","2005款 1.6 手动 基本型,dongnanch","2005款 1.6 手动 超值型,dongnanci",                  
							"2007款 1.6 手动 CNG油气混合,dongnancj","2007款 1.6 手动 LPG油气混合,dongnanck","2007款 1.6 手动 超值型,dongnancl",
							"2007款 1.6 手动 舒适型,dongnancm","2007款 1.6 手动 基本型,dongnancn"
							],
					
					"得利卡":["2005款 2.0 手动 精明者型9座,dongnanda","2005款 2.0 手动 舒适型5-9座,dongnandb","2005款 2.0 手动 时尚型7-9座,dongnandc",
							"2005款 2.0 手动 经济型5-9座,dongnandd","2005款 2.0 手动 经典型7-9座,dongnande","2005款 2.4 手动 征服者11座四驱,dongnandf",
							"2005款 2.4 手动 尊贵型11座,dongnandg","2005款 2.0 手动 精明者型8座,dongnandh","2006款 2.4 手动 豪华型11座,dongnandi",
							"2006款 2.0 手动 经典型7-9座,dongnandj","2006款 2.0 手动 舒适型5-9座,dongnandk",
							"2006款 2.0 手动 经济型5-9座,dongnandl","2006款 2.0 手动 舒适型11座,dongnandm","2006款 2.0 手动 时尚型11座,dongnandn",
							"2006款 2.0 手动 经济型11座,dongnando","2006款 2.0 手动 经典型11座,dongnandp","2006款 2.0 手动 时尚型7-9座,dongnandq",
							"2008款 2.0 手动 时尚型7-9座,dongnandr","2008款 2.0 手动 经济型7-9座,dongnands","2008款 2.0 手动 经典型7-9座,dongnandt",
							"2008款 2.0 手动 豪华型7-9座,dongnandu","2008款 2.0 手动 舒适型5-9座,dongnandv","2008款 2.0 手动 时尚型5-9座,dongnandw",
							"2008款 2.0 手动 经济型5-9座,dongnandx","2008款 2.0 手动 经典型5-9座,dongnandy","2008款 2.0 手动 豪华型5-9座,dongnandz",
							"2008款 2.0 手动 时尚型11座,dongnandaa","2008款 2.0 手动 经典型11座,dongnandab","2008款 2.0 手动 豪华型11座,dongnandac",
							"2008款 2.0 手动 经济型11座,dongnandad","2008款 2.0 手动 舒适型11座,dongnandae","2008款 2.4 手动 豪华型7座,dongnandaf",
							"2008款 2.4 手动 豪华型11座,dongnandag", "2010款 2.0 手动 创业先锋舒适型7座,dongnandah","2010款 2.0 手动 创业先锋舒适型11座,dongnandai",
							"2010款 2.0 手动 创业先锋豪华型7座,dongnandaj","2010款 2.0 手动 创业先锋豪华型11座,dongnandak","2010款 2.0 手动 创业先锋标准型7座,dongnandal",
							"2010款 2.0 手动 创业先锋标准型11座,dongnandam","2011款 2.0 手动 经济型11座,dongnandan","2011款 2.0 手动 经济型5-9座,dongnandao",
							"2014款 2.0 手动 新创业先锋实用型9座,dongnandap","2014款 2.0 手动 时尚型9座,dongnandaq","2014款 2.0 手动 经济型9座,dongnandar",
							"2014款 2.0 手动 经典型9座,dongnandas","2014款 2.0 手动 豪华型9座,dongnandat","2014款 2.0 手动 豪华型7-9座,dongnandau",
							"2014款 2.0 手动 时尚型7-9座,dongnandav","2014款 2.0 手动 经典型7-9座,dongnandaw","2014款 2.0 手动 新创业先锋实用型7-9座,dongnandax",
							"2014款 2.0 手动 经济型7-9座,dongnanday"
							],
					
					
					"菱动":["2006款 2.0 手动 尊贵型5座前驱,dongnanea", "2006款 2.0 手动 舒适型5座前驱,dongnaneb", "2006款 2.0 手动 基本型5座前驱,dongnanec", 
							"2006款 2.0 手动 尊贵型8座前驱,dongnaned", "2006款 2.0 手动 基本型8座前驱,dongnanee", "2006款 2.0 手动 舒适型8座前驱,dongnanef", 
							"2008款 2.0 手动 舒适型5座前驱,dongnaneg", "2008款 2.0 手动 舒适型8座前驱,dongnaneh"
							], 
					
					"菱悦":["2008款 1.5 手动 旗舰版,dongnanfa","2008款 1.5 手动 豪华版,dongnanfb","2008款 1.5 手动 舒适版,dongnanfc","2008款 1.5 手动 启航版,dongnanfd",
							"2009款 1.5 手动 旗舰升级版,dongnanfe","2009款 1.5 手动 风采版,dongnanff","2010款 1.5 自动  旗舰导航版改款,dongnanfg","2010款 1.5 自动  旗舰导航版,dongnanfh",
							"2010款 1.5 自动  旗舰版改款,dongnanfi","2010款 1.5 自动  豪华版改款,dongnanfj","2010款 1.5 手动  旗舰导航版改款,dongnanfk","2010款 1.5 手动风采版改款,dongnanfl",
							"2010款 1.5 手动 旗舰版改款,dongnanfm","2010款 1.5 手动 豪华版改款,dongnanfn","2010款 1.5 手动 舒适版改款,dongnanfo","2010款 1.5 自动 风采版,dongnanfp","2010款 1.5 自动 旗舰版,dongnanfq",
							"2010款 1.5 自动 豪华版,dongnanfr","2011款 1.5 手动 幸福版,dongnanfs","2011款 1.5 手动 舒适版,dongnanft","2011款 1.5 手动 启航升级版,dongnanfu","2011款 1.5 手动 启航版,dongnanfv",
							"2011款 1.5 手动 旗舰导航版,dongnanfw","2011款 1.5 手动 旗舰版,dongnanfx","2011款 1.5 手动 豪华版,dongnanfy","2011款 1.5 手动 风采版,dongnanfz",
							"2011款 1.5 自动 舒适版,dongnanfaa","2011款 1.5 自动 旗舰导航版,dongnanfab","2011款 1.5 自动 旗舰版,dongnanfac","2011款 1.5 自动 风采版,dongnanfad",
							"2011款 1.5 自动 豪华版,dongnanfae","2012款 1.5 手动 幸福版,dongnanfaf","2012款 1.5 手动 舒适版,dongnanfag","2012款 1.5 手动 旗舰风采版,dongnanfah","2012款 1.5 手动 旗舰版,dongnanfai",
							"2012款 1.5 手动 豪华风采版,dongnanfaj","2012款 1.5 手动 风采版改款,dongnanfak","2012款 1.5 手动 豪华版,dongnanfal","2012款 1.5 自动 旗舰风采版,dongnanfam",
							"2012款 1.5 自动 豪华版,dongnanfan","2012款 1.5 手动 旗舰版改款,dongnanfao","2012款 1.5 自动 旗舰版,dongnanfap","2012款 1.5 手动 豪华版改款,dongnanfaq",
							"2012款 1.5 手动 舒适版改款,dongnanfar","2012款 1.5 手动 新幸福版,dongnanfas","2013款 1.5 自动 亲民豪华版,dongnanfat","2013款 1.5 手动 亲民风采版,dongnanfau",
							"2013款 1.5 手动 亲民旗舰版,dongnanfav","2013款 1.5 手动 亲民豪华版,dongnanfaw","2013款 1.5 手动 亲民舒适版,dongnanfax","2013款 1.5 手动 亲民幸福版,dongnanfay",
							"2014款 1.5 自动 豪华版,dongnanfaz","2014款 1.5 手动 精明版,dongnanfba","2014款 1.5 手动 风采超值版,dongnanfbb","2014款 1.5 手动 风采版,dongnanfbc",
							"2014款 1.5 手动 旗舰版,dongnanfbd","2014款 1.5 手动 豪华版,dongnanfbe","2014款 1.5 手动 幸福版,dongnanfbf","2014款 1.5 手动 亲民版,dongnanfbg",
							"2015款 1.5 手动 幸福版CNG,dongnanfbh","2015款 1.5 手动 风采版CNG,dongnanfbi","2015款 1.5 手动 精明版CNG,dongnanfbj",
							"2015款 1.5 自动 幸福版,dongnanfbk","2015款 1.5 手动 幸福版,dongnanfbl","2015款 1.5 手动 风采版,dongnanfbm","2015款 1.5 手动 精明版,dongnanfbn"
							], 
							
					"希旺":["2010款 1.3 手动 东安引擎系列,dongnanga","2010款 1.3 手动 柳机引擎系列,dongnangb","2011款 1.3 手动 舒适型柳机引擎系列,dongnangc",
							"2011款 1.3 手动 舒适型东安引擎系列,dongnangd","2011款 1.3 手动 基本型柳机引擎系列,dongnange","2011款 1.3 手动 基本型东安引擎系列,dongnangf",
							"2011款 1.3 手动 豪华型东安引擎系列,dongnangg","2011款 1.3 手动 标准型柳机引擎系列,dongnangh","2011款 1.3 手动 标准型东安引擎系列,dongnangi"
							], 
							
					"菱致":["2012款 1.5 手动 豪华型,dongnanha","2012款 1.5 手动 舒适型,dongnanhb","2012款 1.5 手动 标准型,dongnanhc","2012款 1.5 自动 豪华型,dongnanhd",
							"2012款 1.5 自动 旗舰型,dongnanhe","2012款 1.5 自动 舒适型,dongnanhf","2012款 1.5 手动 旗舰型,dongnanhg","2013款 1.5 手动 旗舰型CNG双燃料,dongnanhh","2013款 1.5 自动 豪华导航版,dongnanhi",
							"2013款 1.5 自动 智尚型,dongnanhj","2013款 1.5 手动 智尚型,dongnanhk","2013款 1.5 手动 豪华导航版,dongnanhl","2013款 1.5 手动 豪华型CNG双燃料,dongnanhm",
							"2013款 1.5 手动 舒适型CNG双燃料,dongnanhn","2013款 1.5 手动 标准型CNG双燃料,dongnanho", "2014款 1.5T 自动 趣控型 双燃料,dongnanhp",
							"2014款 1.5 手动 标准型,dongnanhq","2014款 1.5 手动 舒雅型,dongnanhr","2014款 1.5T 自动 趣控型,dongnanhs","2014款 1.5 自动 豪华型,dongnanht",
							"2014款 1.5 自动 舒适型,dongnanhu","2014款 1.5 手动 豪华型,dongnanhv","2014款 1.5 手动 舒适型,dongnanhw","2014款 1.5T 手动 趣控型,dongnanhx","2014款 1.5T 手动 锐控型,dongnanhy",                         
							"2015款 1.5 手动 经典版,dongnanhz","2015款 1.5T 自动 Plus智控型,dongnanhaa","2015款 1.5T 手动 Plus智控型,dongnanhab",
							"2015款 1.5 手动 Plus爵士版,dongnanhac","2015款 1.5 自动 Plus智尊型,dongnanhad","2015款 1.5 手动 Plus智尊型,dongnanhae","2015款 1.5 手动 Plus智趣型,dongnanhaf",
							"2015款 1.5T 自动 智控型,dongnanhag","2015款 1.5T 自动 智控型 双燃料,dongnanhah","2015款 1.5 自动 舒适型,dongnanhai","2015款 1.5 自动 豪华型,dongnanhaj","2015款 1.5T 手动 智控型,dongnanhak",
							"2015款 1.5T 手动 趣控型,dongnanhal","2015款 1.5 手动 旗舰型,dongnanham","2015款 1.5 手动 豪华型,dongnanhan","2015款 1.5 手动 舒适型,dongnanhao","2015款 1.5 手动 爵士版,dongnanhap"
							 ],
					
					
					"菱仕":["2013款 1.5 手动 豪华女性版,dongnania","2013款 1.5 自动 女性高配版,dongnanib","2013款 1.5 自动 旗舰版,dongnanic",
							"2013款 1.5 手动 标准版,dongnanid","2013款 1.5 手动 豪华版,dongnanie",
							"2013款 1.5 手动 精英版,dongnanif","2013款 1.5 手动 女性版,dongnanig","2013款 1.5 手动 旗舰版,dongnanih",
							"2013款 1.5 手动 舒适版,dongnanii","2013款 1.5 自动 女性版,dongnanij",
							"2013款 1.5 自动 精英版,dongnanik","2013款 1.5 自动 豪华版,dongnanil","2014款 1.5T 手动 智控版,dongnanim",
							"2014款 1.5T 自动 智控版,dongnanin","2014款 1.5 自动 标准女性版,dongnanio",
							"2014款 1.5 手动 豪华女性版,dongnanip","2014款 1.5T 自动 趣控版,dongnaniq","2014款 1.5T 手动 趣控版,dongnanir",
							"2014款 1.5T 手动 锐控版,dongnanis","2015款 1.5T 手动 Cross智控型,dongnanit","2015款 1.5 自动 Cross智尊型,dongnaniu",
							"2015款 1.5 手动 Cross智尊型,dongnaniv","2015款 1.5 自动 Cross智趣型,dongnaniw","2015款 1.5 手动 Cross智趣型,dongnanix"
							], 
					
					"东南DX7":["2015款 2.0T 手动 运动旗舰型,dongnanja","2015款 2.0T 手动 运动豪华型,dongnanjb","2015款 1.5T 自动 尊贵型,dongnanjc","2015款 1.5T 自动 豪华型,dongnanjd",
							"2015款 1.5T 自动 精英型,dongnanje","2015款 1.5T 手动 尊贵型,dongnanjf","2015款 1.5T 手动 豪华型,dongnanjg","2015款 1.5T 手动 精英型,dongnanjh",
							"2015款 1.5T 手动 舒适型,dongnanji","2015款 1.5T 手动 启航型,dongnanjj"
							],
					
					
//大众					

					"POLO":["2005款 两厢  1.4 手动 舒适型,dazhongaa","2005款 两厢  1.4 手动 豪华型,dazhongab","2005款 两厢  1.6 自动 豪华型,dazhongac","2005款 三厢 1.6 自动 豪华型,dazhongad", 
							"2005款 劲情  1.6 自动 风尚版,dazhongae","2005款 劲情  1.6 手动 风尚版,dazhongaf","2005款 劲情  1.4 自动 风尚版,dazhongag","2005款 劲情  1.4 手动 风尚版,dazhongah",
							"2005款 劲取  1.6 自动,dazhongai","2005款 劲取  1.6 手动,dazhongaj","2005款 劲取  1.4 自动,dazhongak","2005款 劲取  1.4 手动,dazhongal",                 
							"2006款 劲情  1.6 自动 风尚版,dazhongam","2006款 劲情  1.6 手动 风尚版,dazhongan","2006款 劲情  1.4 自动 时尚版,dazhongao","2006款 劲情  1.4 自动 风尚版,dazhongap",
							"2006款 劲情  1.4 手动 时尚版,dazhongaq","2006款 劲情  1.4 手动 风尚版,dazhongar","2006款 劲取  1.6 自动 雅尊版,dazhongas","2006款 劲取  1.6 手动 雅致版,dazhongat",
							"2006款 劲取  1.6 手动 雅尊版,dazhongau","2006款 劲取  1.4 自动 雅致版,dazhongav","2006款 劲取  1.4 手动 雅致版,dazhongaw","2006款 劲取  1.4 手动 雅适版,dazhongax",
							"2007款 劲情  1.4 手动 时尚版,dazhongay","2007款 劲情  1.4 手动 风尚版,dazhongaz","2007款 劲情  1.6 手动 风尚版,dazhongaaa","2007款 劲情  1.6 自动 风尚版,dazhongaab",
							"2007款 劲情  1.4 自动 时尚版,dazhongaac","2007款 劲情  1.4 自动 风尚版,dazhongaad","2007款 劲情  1.4 手动 风尚运动版,dazhongaae","2007款 劲情  1.4 手动 时尚运动版,dazhongaaf",
							"2007款 三厢 1.6 手动,dazhongaag","2007款 Cross  1.6 自动 奥运限量版,dazhongaah","2007款 Cross  1.6 自动,dazhongaai","2007款 Cross  1.6 手动,dazhongaaj", 
							"2008款 劲取  1.6 自动 雅尊版,dazhongaak","2008款 劲取  1.6 手动 雅致版,dazhongaal","2008款 劲取  1.4 自动 雅致版,dazhongaam","2008款 劲取  1.4 手动 雅致版,dazhongaan",
							"2008款 劲取  1.4 手动 雅适版,dazhongaao","2009款 劲情  1.6 手动 风尚版,dazhongaap","2009款 劲情  1.6 手动 Sporty,dazhongaaq","2009款 劲情  1.6 自动 风尚版,dazhongaar",
							"2009款 劲情  1.6 自动 Sporty,dazhongaas","2009款 劲情  1.4 手动 舒尚版,dazhongaat","2009款 劲情  1.4 手动 时尚版,dazhongaau","2009款 劲情  1.4 自动 舒尚版,dazhongaav",
							"2009款 劲取  1.6 自动 雅致版,dazhongaaw","2009款 劲取  1.6 手动 雅致版,dazhongaax","2009款 劲取  1.4 自动 雅致版,dazhongaay","2009款 劲取  1.4 手动 雅致版,dazhongaaz",
							"2009款 劲取  1.4 手动 雅适版,dazhongaba","2009款 Cross  1.6 自动,dazhongabb","2009款 Cross  1.6 手动,dazhongabc",
							"2011款 劲取  1.6 手动 实酷版,dazhongabd","2011款 劲取  1.6 自动 实酷版,dazhongabe","2011款 劲取  1.4 自动 实尚版,dazhongagf","2011款 劲取  1.4 手动 实尚版,dazhongabg",
							"2011款 劲取  1.4 手动 实乐版,dazhongabh","2011款 两厢  1.4 自动 致尚版,dazhongabi","2011款 两厢  1.4 自动 致酷版,dazhongabj","2011款 两厢  1.4 手动 致尚版,dazhongabk",
							"2011款 两厢  1.4 手动 致乐版,dazhongabl","2011款 两厢  1.6 手动 致尚版,dazhongabm","2011款 两厢  1.6 自动 致尚版,dazhongabn","2011款 两厢  1.6 自动 致酷版,dazhongabo",
							"2012款 Cross  1.6 手动,dazhongabp","2012款 Cross  1.6 自动,dazhongabq","2012款 CTI 1.4T 自动 ,dazhongabr","2013款 两厢  1.4 手动 舒适版,dazhongabs",
							"2013款 两厢  1.4 手动 风尚版,dazhongabt","2013款 两厢  1.4 自动 豪华版,dazhongabu","2013款 两厢  1.4 自动 舒适版,dazhongabv","2013款 两厢  1.6 手动 舒适版,dazhongabw",
							"2013款 两厢  1.6 自动 豪华版,dazhongabx","2013款 两厢  1.6 自动 舒适版,dazhongaby","2014款 Cross  1.6 手动,dazhongabz","2014款 Cross  1.6 自动,dazhongaca",
							"2014款 两厢  1.4 自动 豪华版,dazhongacb","2014款 两厢  1.4 自动 舒适版,dazhongacc","2014款 两厢  1.6 手动 舒适版,dazhongacd","2014款 两厢  1.4 手动 舒适版,dazhongace",
							"2014款 两厢  1.4 手动 风尚版,dazhongacf","2015款 CTI 1.4T 自动,dazhongacg","2015款 两厢  1.6 自动 30周年纪念版,dazhongach","2014款 两厢  1.6 自动 30周年纪念版,dazhongaci",
							"2014款 两厢  1.6 自动 豪华版,dazhongacj","2014款 两厢  1.6 自动 舒适版,dazhongack"
							],
				
				
				
				
				"桑塔纳·尚纳":["2013款 1.6L 自动豪华版,dazhongba","2013款 1.4L 手动舒适版,dazhongbb","2013款 1.6L 手动豪华版,dazhongbc","2013款 1.4L 手动风尚版,dazhongbd",
								"2013款 1.6L 自动风尚版,dazhongbe","2013款 1.6L 手动舒适版,dazhongbf","2013款 1.6L 自动舒适版,dazhongbg","2014款 1.6L 自动30周年纪念版,dazhongbh",
								"2015款 1.4L 手动舒适版,dazhongbi","2015款 1.6L 手动舒适版,dazhongbj","2015款 1.6L 自动舒适版,dazhongbk"
								],
				
				"朗行":["2013款 1.4T 手动 舒适版,dazhongca","2013款 1.4T 手动 豪华版,dazhongcb","2013款 1.4T 自动 豪华版,dazhongcc","2013款 1.4T 自动 舒适版,dazhongcd",
						"2013款 1.6 自动 豪华版,dazhongce","2013款 1.6 自动 舒适版,dazhongcf","2013款 1.6 手动 舒适版,dazhongcg","2013款 1.6 自动 风尚版,dazhongch",
						"2013款 1.6 手动 风尚版,dazhongci","2014款 1.4T 自动 30周年纪念版,dazhongcj","2014款 1.4T 自动 运动版,dazhongck","2014款 1.6 自动 运动版,dazhongcl",
						"2015款 1.4T 自动 230TSI豪华型,dazhongcm","2015款 1.4T 手动 230TSI豪华型,dazhongcn","2015款 1.4T 自动 230TSI舒适型,dazhongco",
						"2015款 1.4T 手动 230TSI舒适型,dazhongcp","2015款 1.6 自动 豪华型,dazhongcq","2015款 1.6 自动 舒适型,dazhongcr","2015款 1.6 手动 舒适型,dazhongcs",
						"2015款 1.6 自动 风尚型,dazhongct","2015款 1.6 手动 风尚型,dazhongcu"
						],
				
				
				
				"朗逸":["2008款 1.6 自动 品雅版,dazhongda","2008款 1.6 自动 品雅进享版,dazhongdb","2008款 2.0 手动 品雅版,dazhongdc","2008款 1.6 手动 品雅版,dazhongdd",
						"2008款 1.6 自动 品轩版,dazhongde","2008款 1.6 手动 品轩版,dazhongdf","2008款 1.6 自动 品悠版,dazhongdg","2008款 2.0 自动 品轩版,dazhongdh",
						"2008款 1.6 手动 品悠版,dazhongdi","2008款 2.0 手动 品轩版,dazhongdj","2009款 2.0 手动 品轩版,dazhongdk","2009款 2.0 自动 品轩版,dazhongdl",
						"2009款 2.0 自动 品雅版,dazhongdm","2009款 2.0 手动 品悠版,dazhongdn","2009款 2.0 自动 品悠版,dazhongdo",
						"2010款 1.6 自动 品雅进享版,dazhongdp","2010款 1.6 自动 品轩进享版,dazhongdq","2010款 1.6 手动 品雅进享版,dazhongdr",
						"2010款 1.6 手动 品轩进享版,dazhongds","2010款 1.4T 自动 运动版,dazhongdt","2010款 1.6 手动 品悠进享版,dazhongdu",   
						"2011款 1.6 自动 品悠版,dazhongdv","2011款 1.6 手动 品悠版,dazhongdw","2011款 1.6 自动 品雅版,dazhongdx","2011款 1.6 自动 品轩版,dazhongdy",
						"2011款 1.6 手动 品雅版,dazhongdz","2011款 2.0 手动 品雅版,dazhongdaa","2011款 2.0 手动 品轩版,dazhongdab","2011款 1.4T 自动 品雅版,dazhongdac",
						"2011款 1.4T 自动 品轩版,dazhongdad","2011款 1.4T 手动 品雅版,dazhongdae","2011款 1.4T 手动 品轩版,dazhongdaf","2011款 2.0 自动 品悠版,dazhongdag",
						"2011款 2.0 手动 品悠版,dazhongdah","2013款 1.6 手动 经典风尚型,dazhongdai","2013款 1.4T 自动 舒适版改款,dazhongdaj",
						"2013款 1.4T 自动 豪华版改款,dazhongdak","2013款 1.6 自动 经典风尚型,dazhongdal","2013款 1.6 手动 舒适版改款,dazhongdam",
						"2013款 1.6 手动 风尚版改款,dazhongdan","2013款 1.6 自动 舒适版改款,dazhongdao","2013款 1.6 自动 豪华版改款,dazhongdap",
						"2013款 1.6 自动 风尚版改款,dazhongdaq","2013款 1.4T 手动 舒适版改款,dazhongdar","2013款 1.4T 手动 豪华版改款,dazhongdas",
						"2013款 1.4T 手动 豪华版,dazhongdat","2013款 1.4T 自动 豪华导航版,dazhongdau","2013款 1.4T 自动 豪华版,dazhongdav","2013款 1.4T 手动 舒适版,dazhongdaw",
						"2013款 1.6 自动 豪华版,dazhongdax","2013款 1.6 自动 舒适版,dazhongday",
						"2013款 1.6 自动 风尚版,dazhongdaz","2013款 1.6 手动 舒适版,dazhongdca","2013款 1.6 手动 风尚版,dazhongdcb","2013款 1.4T 自动 舒适版,dazhongdcc",
						"2014款 1.4T 自动 30周年纪念版,dazhongdcd","2014款 1.4T 自动 蓝驱版,dazhongdce","2014款 1.6 自动 运动版,dazhongdcf","2014款 1.4T 自动 运动版,dazhongdcg",
						"2015款 1.2T 自动 蓝驱技术版,dazhongdch","2015款 1.4T 自动 230TSI豪华版,dazhongdci","2015款 1.4T 手动 230TSI豪华版,dazhongdcj",
						"2015款 1.4T 自动 230TSI舒适版,dazhongdck","2015款 1.4T 手动 230TSI舒适版,dazhongdcl","2015款 1.6 自动 豪华版,dazhongdcm",
						"2015款 1.6 自动 舒适版,dazhongdcn","2015款 1.6 手动 舒适版,dazhongdco","2015款 1.6 自动 风尚版,dazhongdcp","2015款 1.6 手动 风尚版,dazhongdcq"
						],

				
				
				"朗境":["2014款 1.6 自动,dazhongea","2014款 1.4T 自动,dazhongeb","2016款 1.4T 自动 230TSI豪华版,dazhongec","2016款 1.4T 自动 230TSI舒适版,dazhonged","2016款 1.6 自动 豪华型,dazhongee","2016款 1.6 自动 舒适型,dazhongef"
						],
				
				
				
				"凌渡":["2015款 1.8T 自动 330TSI豪华版,dazhongfa","2015款 1.8T 自动 330TSI舒适版,dazhongfb","2015款 1.4T 自动 280TSI豪华版,dazhongfc",
						"2015款 1.4T 自动 280TSI舒适版,dazhongfd","2015款 1.4T 自动 230TSI风尚版,dazhongfe","2015款 1.4T 手动 280TSI舒适版,dazhongff",
						"2015款 1.4T 手动 230TSI风尚版,dazhongfg"
						],
				
				"帕萨特":["2005款 1.8T 自动 豪华型,dazhongga","2005款 1.8T 自动 天窗型,dazhonggb","2005款 1.8T 手动 天窗型,dazhonggc","2005款 2.0 手动 超值型,dazhonggd",
						"2005款 2.0 自动,dazhongge","2005款 2.8 自动 升级版,dazhonggf","2005款 2.8 自动 豪华商务版,dazhonggg","2005款 1.8T 手动 经典型,dazhonggh","2005款 2.8 自动 豪华行政版,dazhonggi",
						"2006款 1.8T 手动 经典型,dazhonggj","2006款 2.0 自动 超值型,dazhonggk","2006款 2.0 手动 超值款,dazhonggl","2006款 2.0 手动 经典型,dazhonggm",
						"2006款 2.0 自动 经典型,dazhonggn","2007款 1.8T 自动,dazhonggo","2007款 1.8T 手动 经典型,dazhonggp","2007款 2.0 自动 经典型,dazhonggq","2007款 2.0 手动 经典型,dazhonggr",
						"2007款 1.8T 手动 天窗增值型,dazhonggs","2008款 1.9T 手动 柴油版,dazhonggt","2008款 1.8T 手动 经典型,dazhonggu","2008款 2.0 自动 经典型,dazhonggv","2008款 2.0 手动 经典型,dazhonggw",
						"2011款 1.4T 自动 尊荣版,dazhonggx","2011款 1.4T 手动 尊雅版,dazhonggy","2011款 3.0 自动 旗舰尊享版,dazhonggz","2011款 3.0 自动 旗舰版,dazhonggaa",
						"2011款 2.0T 自动 至尊版,dazhonggab","2011款 2.0T 自动 御尊版,dazhonggac","2011款 1.8T 自动 至尊版,dazhonggad","2011款 1.8T 自动 御尊版,dazhonggae",
						"2011款 旅行车 1.4T 自动 豪华型,dazhongglx01","2011款 旅行车 1.4T 自动 舒适型,dazhongglx02","2011款 1.8T 自动 尊荣版,dazhonggaf","2013款 1.4T 手动 尊雅版,dazhonggag","2013款 1.4T 手动 尊荣版,dazhonggah","2013款 1.4T 自动 尊荣版,dazhonggai","2013款 1.4T 自动 蓝驱技术版,dazhonggaj",
						"2013款 1.4T 手动 尊荣导航版,dazhonggak","2013款 3.0 自动 旗舰尊享版,dazhonggal","2013款 3.0 自动 旗舰版,dazhonggam","2013款 2.0T 自动 至尊版,dazhonggan",
						"2013款 2.0T 自动 御尊导航版,dazhonggao","2013款 2.0T 自动 御尊版,dazhonggap","2013款 1.8T 自动 至尊版,dazhonggaq","2013款 1.8T 自动 御尊导航版,dazhonggar",
						"2013款 1.8T 自动 御尊版,dazhonggas","2013款 1.8T 自动 尊荣导航版,dazhonggat","2013款 1.8T 自动 尊荣版,dazhonggau","2013款 1.4T 自动 蓝驱导航版,dazhonggav",
						"2013款 1.4T 自动 蓝驱版,dazhonggaw","2013款 1.4T 自动 尊荣导航版,dazhonggax","2011款 1.4T 手动 尊荣版,dazhonggay","2011款 1.4T 自动 尊荣导航版,dazhonggaz",
						"2014款 2.0T 自动 御尊导航版,dazhonggba","2014款 1.8T 自动 30周年纪念版,dazhonggbb","2014款 3.0 自动 旗舰尊享版,dazhonggbc","2014款 3.0 自动 旗舰版,dazhonggbd",
						"2014款 2.0T 自动 至尊版,dazhonggbe","2014款 2.0T 自动 御尊版,dazhonggbf","2014款 1.8T 自动 至尊版,dazhonggbg","2014款 1.8T 自动 御尊版,dazhonggbh",
						"2014款 1.4T 自动 蓝驱技术版,dazhonggbi","2014款 1.4T 自动 尊荣版,dazhonggbj","2014款 1.4T 手动 尊荣版,dazhonggbk","2014款 1.4T 手动 尊雅版,dazhonggbl",
						"2014款 旅行车 1.4T 自动,dazhongglx03","2014款 1.8T 自动 御尊导航版,dazhonggbm","2014款 1.8T 自动 尊荣导航版,dazhonggbn","2014款 1.8T 自动 尊荣版,dazhonggbo","2014款 1.8T 自动 尊雅版,dazhonggbp",
						"2015款 3.0 自动 旗舰尊享版,dazhonggbq","2015款 3.0 自动 旗舰版,dazhonggbr","2015款 2.0T 自动 至尊版,dazhonggbs","2015款 2.0T 自动 御尊版,dazhonggbt",
						"2015款 1.8T 自动 至尊版,dazhonggbu","2015款 1.8T 自动 御尊版,dazhonggbv","2015款 1.8T 自动 尊荣6AT版,dazhonggbw","2015款 1.8T 自动 尊雅版,dazhonggbx",
						"2015款 1.8T 自动 尊荣版,dazhonggby","2015款 1.4T 自动 蓝驱技术版,dazhonggbz","2015款 1.4T 自动 尊荣版,dazhonggca","2015款 1.4T 手动 尊荣版,dazhonggcb",
						"2015款 1.4T 手动 尊雅版,dazhonggcc","2015款 旅行车 1.4T 自动 舒适型,dazhonggcd"
						
						       
						],
				
				"途观":["2010款 2.0T 自动 特别版四驱,dazhongha","2010款 2.0T 自动 豪华版四驱,dazhonghb","2010款 2.0T 自动 舒适版四驱,dazhonghc","2011款 2.0T 自动 R-Line四驱,dazhonghd",
						"2012款 2.0T 自动 舒适版四驱 柴油,dazhonghe","2012款 2.0T 自动 豪华版四驱 柴油,dazhonghf","2012款 2.0T 自动 豪华版四驱,dazhonghg","2012款 2.0T 自动 舒适版四驱,dazhonghh",     
						"2014款 2.0T 自动 专享版四驱,dazhonghh","2015款 2.0T 自动 舒适型,dazhonghi"
						],
				
				"途安":["2005款 2.0 自动 舒适型7座,dazhongia","2005款 2.0 自动 舒适型5座,dazhongib","2005款 2.0 手动 舒适型7座,dazhongic","2005款 2.0 手动 舒适型5座,dazhongid",
						"2005款 1.8T 自动 豪华型7座,dazhongie","2005款 1.8T 手动 豪华型7座,dazhongif","2005款 1.8T 手动 豪华型5座,dazhongig","2005款 1.8T 手动 舒适型7座,dazhongih",
						"2005款 1.8T 手动 舒适型5座,dazhongii","2005款 1.8T 自动 豪华型5座,dazhongij","2005款 1.8T 自动 舒适型7座,dazhongik","2005款 1.8T 自动 舒适型5座,dazhongil",
						"2006款 1.8T 自动 豪华型7座,dazhongim","2006款 1.8T 自动 豪华型5座,dazhongin","2006款 1.8T 手动 豪华型5座,dazhongio","2006款 2.0 自动 舒适型5座,dazhongip",
						"2006款 2.0 手动 舒适型5座,dazhongiq","2007款 2.0 自动 智享版5座,dazhongir","2007款 1.8T 手动 智尊版5座,dazhongis","2007款 1.8T 手动 舒适版5座,dazhongit","2007款 1.8T 自动 智尊版7座,dazhongiu",
						"2007款 1.8T 自动 舒适版5座,dazhongiv","2007款 1.8T 自动 智尊版5座,dazhongiw","2007款 2.0 手动 智享版5座,dazhongix",
						"2008款 2.0 自动 智雅版5座,dazhongiy","2008款 1.8T 自动 舒适版5座,dazhongiz","2008款 1.8T 手动 舒适版5座,dazhongiaa","2008款 2.0 自动 智享版5座,dazhongiab",
						"2008款 2.0 手动 智享版5座,dazhongiac","2008款 1.8T 自动 智尊版7座,dazhongiad","2008款 1.8T 自动 智尊版5座,dazhongiae","2008款 1.8T 手动 智尊版5座,dazhongiaf",
						"2009款 2.0 自动 智享版5座,dazhongiag","2009款 2.0 手动 智享版5座,dazhongiah","2009款 1.8T 自动 智尊版7座,dazhongiai",
						"2010款 2.0 自动 智享版5座,dazhongiaj","2010款 2.0 手动 智享版5座,dazhongiak","2010款 1.8T 自动 智尊版7座,dazhongial",
						"2011款 2.0 手动 智享版5座,dazhongiam","2011款 1.4T 手动 智臻版5座,dazhongian","2011款 1.4T 手动 智雅版5座,dazhongiao","2011款 1.4T 手动 智尚版5座,dazhongiap",
						"2011款 1.4T 自动 智臻版7座,dazhongiaq","2011款 1.4T 自动 智臻版5座,dazhongiar","2011款 1.4T 自动 智雅版5座,dazhongias",
						"2012款 1.4T 自动 豪华睿智版7座,dazhongiat","2012款 1.4T 自动 豪华璀璨版7座,dazhongiau","2012款 1.4T 自动 智臻版5座,dazhongiav",
						"2012款 1.4T 自动 智雅版5座,dazhongiaw","2012款 1.4T 手动 智尚版5座,dazhongiax","2012款 1.4T 手动 智雅版5座,dazhongiay","2012款 1.4T 手动 智臻版5座,dazhongiaz",
						"2012款 1.4T 自动 智臻版7座,dazhongiba","2013款 1.4T 自动 舒适版5座,dazhongibb","2013款 1.4T 自动 豪华版5座,dazhongibc","2013款 1.4T 手动 舒适版5座,dazhongibd",
						"2013款 1.4T 手动 豪华版5座,dazhongibe","2013款 1.4T 手动 风尚版5座,dazhongibf","2013款 1.4T 自动 豪华睿智版7座,dazhongibg",
						"2013款 1.4T 自动 豪华睿智版5座,dazhongibh","2013款 1.4T 自动 豪华璀璨版7座,dazhongibi","2013款 1.4T 自动 豪华璀璨版5座,dazhongibj",
						"2013款 1.4T 手动 豪华睿智版5座,dazhongibk","2013款 1.4T 手动 豪华璀璨版5座,dazhongibl","2014款 1.4T 自动 旗舰版5座,dazhongibm",
						"2014款 1.4T 自动 30周年纪念版5座,dazhongibn","2015款 1.4T 自动 旗舰版5座,dazhongibo","2015款 1.4T 自动 豪华版5座,dazhongibp","2015款 1.4T 手动 豪华版5座,dazhongibq",
						"2015款 1.4T 自动 舒适版5座,dazhongibr","2015款 1.4T 手动 舒适版5座,dazhongibs","2015款 1.4T 手动 风尚版5座,dazhongibt"
						],
				
				//"高尔":
				
				"Passat领驭":["2005款 2.8 自动 旗舰型,dazhongja","2005款 1.8T 手动 豪华型,dazhongjb","2005款 2.0 自动 标准型,dazhongjc","2005款 2.0 手动 ,dazhongjd",
							"2005款 2.8 自动 豪华型,dazhongje","2005款 1.8T 自动 豪华型,dazhongjf","2005款 1.8T 自动 舒适型,dazhongjg","2005款 1.8T 手动 舒适型,dazhongjh",
							"2006款 1.8T 自动 VIP,dazhongji","2006款 1.8T 自动 VIP导航型,dazhongjj","2006款 1.8T 自动 豪华型,dazhongjk","2006款 2.8 自动 旗舰型,dazhongjl",
							"2006款 2.0 自动 标准型,dazhongjm","2006款 2.0 自动 增配型,dazhongjn","2006款 2.0 手动 增配型,dazhongjo","2006款 1.8T 手动 标准型,dazhongjp",
							"2006款 1.8T 自动 舒适型VIP,dazhongjq","2006款 1.8T 自动 舒适型,dazhongjr","2006款 2.0 手动 标准型,dazhongjs","2006款 1.8T 手动 豪华型,dazhongjt",
							"2007款 2.8 自动 旗舰型,dazhongju","2007款 2.0 自动 标准型,dazhongjv","2007款 2.0 手动 标准型,dazhongjw","2007款 1.8T 手动 豪华型,dazhongjx",
							"2007款 1.8T 自动 豪华型,dazhongjy","2007款 1.8T 自动 VIP,dazhongjz","2007款 1.8T 自动 导航型VIP,dazhongjaa","2007款 1.8T 手动 标准型,dazhongjab",
							"2008款 2.0 手动 尊享型,dazhongjac","2008款 2.8 自动 至尊型,dazhongjad","2009款 1.8T 自动 尊仕型,dazhongjae","2009款 2.0 自动 尊享型,dazhongjaf",
							"2009款 2.0 手动 尊享型,dazhongjag","2009款 2.8 自动 至尊型,dazhongjah","2009款 1.8T 自动 尊杰型,dazhongjai",
							"2009款 1.8T 自动 尊品型,dazhongjaj","2009款 1.8T 手动 尊品型,dazhongjak","2009款 1.8T 手动 尊享型,dazhongjal",
							"2011款 2.0 手动 尊享型,dazhongjam","2011款 1.8T 手动 尊享型,dazhongjan","2011款 1.8T 自动 尊享型,dazhongjao","2011款 2.0 手动 尊品型,dazhongjap",
							"2011款 1.8T 自动 尊品型,dazhongjaq","2011款 2.8 自动 至尊型,dazhongjar","2011款 2.0 自动 尊享型,dazhongjas","2011款 1.8T 手动 尊品型,dazhongjat"
							],
				
				"桑塔纳经典":["2005款 1.8 手动,dazhongka","2005款 1.8 手动 LPG油气混合,dazhongkb","2005款 1.8 手动 BKU LPG油气混合,dazhongkc",
							"2006款 1.8 手动 基本型,dazhongkd","2006款 1.8 手动 豪华型,dazhongke","2006款 1.8 手动 旅行车GLi,dazhongkf","2006款 1.8 手动 AFE CNG油气混合,dazhongkg",                  
							"2007款 1.8 手动 锦畅豪华型,dazhongkh","2007款 1.8 手动 LPG油气混合,dazhongki","2007款 1.8 手动 BKT CNG油气混合,dazhongkj",
							"2007款 1.8 手动 旅行车GLi,dazhongkk","2007款 1.8 手动 AFE CNG油气混合,dazhongkl",
							"2013款 1.6 自动 油气混合,dazhongkm","2013款 1.6 手动 豪华版,dazhongkn","2013款 1.6 自动 豪华版,dazhongko","2013款 1.6 自动 舒适版,dazhongkp",
							"2013款 1.6 手动 舒适版,dazhongkq","2013款 1.6 自动 风尚版,dazhongkr","2013款 1.6 手动 风尚版,dazhongks","2013款 1.4 手动 舒适版,dazhongkt",
							"2013款 1.4 手动 风尚版,dazhongku","2015款 1.6 自动 30周年纪念版,dazhongkv","2015款 1.4T 自动 尊享版,dazhongkw","2015款 1.6 手动 舒适版,dazhongkx","2015款 1.6 手动 豪华版,dazhongky",
							"2015款 1.6 手动 风尚版,dazhongkz","2015款 1.6 自动 舒适版,dazhongkaa","2015款 1.6 自动 豪华版,dazhongkab","2015款 1.6 自动 风尚版,dazhongkac",
							"2015款 1.4 手动 舒适版,dazhongkad","2015款 1.4 手动 风尚版,dazhongkae"
							],
						
				"桑塔纳志俊":["2007款 1.8 手动 休闲型,dazhongma","2007款 2.0 自动 豪华型,dazhongmb",
							"2008款 1.8 手动 实尚型,dazhongmc","2008款 2.0 手动 豪华型,dazhongmd","2008款 2.0 自动 豪华型,dazhongme","2008款 1.8 手动 休闲型,dazhongmf",
							"2008款 1.8 手动 舒适型,dazhongmg","2008款 1.8 手动 休闲型升级版,dazhongmh","2008款 1.8 手动 LPG油气混合,dazhongmi","2008款 1.8 手动 CNG油气混合,dazhongmj",                  
							"2009款 1.8 手动 油气混合,dazhongmk","2009款 1.8 手动 CNG油气混合,dazhongml","2009款 1.8 手动 休闲型CKZ,dazhongmm","2009款 1.8 手动 舒适型CKZ,dazhongmn",
							"2010款 1.6 手动 休闲型升级版,dazhongmo","2010款 1.6 手动 实尚型,dazhongmp","2010款 1.6 手动 舒适型,dazhongmq","2010款 1.6 手动 休闲型,dazhongmr"
							],		

						
				"捷达":["2005款 1.6 手动 CIX 油气混合,dazhongna","2005款 1.6 手动 CIX伙伴 LPG油气混合,dazhongnb","2005款 1.6 手动 CIX伙伴 CNG油气混合,dazhongnc",
						"2005款 1.6 手动 CIF都市春天基本型 CNG油气混合,dazhongnd","2005款 1.6 手动 CIX伙伴,dazhongne","2006款 1.6 手动 GIF豪华型,dazhongnf","2006款 1.6 手动 CIX伙伴,dazhongng","2006款 1.6 手动 CIF都市春天舒适型,dazhongnh",
						"2006款 1.6 手动 CIF都市春天基本型,dazhongni","2006款 1.6 手动 CIF都市春天舒适型 CNG油气混合,dazhongnj","2006款 1.6 自动 ATF都市春天舒适型,dazhongnk",
						"2006款 1.6 自动 ATF前卫百万纪念版,dazhongnl", "2007款 1.6 手动 CIX伙伴,dazhongnm","2007款 1.6 手动 CIF都市春天舒适型,dazhongnn","2007款 1.6 手动 CIF都市春天基本型,dazhongno",
						"2008款 1.9 手动 GDF先锋 柴油,dazhongnp","2008款 1.6 自动 ATF前卫,dazhongnq","2008款 1.6 手动 GIF前卫,dazhongnr","2008款 1.6 手动 CIF伙伴,dazhongns",
						"2008款 1.6 手动 CIF都市春天基本型 LPG油气混合,dazhongnt","2009款 1.6 手动 CIF,dazhongnu","2009款 1.6 手动 CIF都市春天基本型 LPG油气混合,dazhongnv","2009款 1.6 自动 CIF自由伙伴限量版,dazhongnw",
						"2009款 1.6 手动 CIF伙伴精英版,dazhongnx","2008款 1.6 自动 GIF新前卫,dazhongny","2010款 1.6 手动 GIF前卫,dazhongnz","2010款 1.6 手动 CIF伙伴,dazhongnaa","2010款 1.9 手动 GDF柴油先锋,dazhongnab","2010款 1.6 手动 CNG油气混合,dazhongnac",
						"2011款 1.6 手动 CNG油气混合,dazhongnad","2012款 1.6 手动 GIF纪念版,dazhongnae","2012款 1.6 手动 GIF前卫,dazhongnaf","2012款 1.6 手动 GIF典藏版,dazhongnag",
						"2013款 1.6 自动 CNG油气混合,dazhongnah","2013款 1.6 自动 时尚型,dazhongnai","2013款 1.6 自动 豪华型,dazhongnaj","2013款 1.6 手动 豪华型,dazhongnak",
						"2013款 1.6 自动 舒适型,dazhongnal","2013款 1.6 手动 舒适型,dazhongnam","2013款 1.6 手动 时尚型,dazhongnan","2013款 1.4 手动 时尚型,dazhongnao",
						"2013款 1.4 手动 舒适型,dazhongnap","2015款 1.4T 自动 质惠版运动型,dazhongnaq","2015款 1.6 自动 质惠版舒适型,dazhongnar","2015款 1.6 手动 质惠版舒适型,dazhongnas","2015款 1.6 自动 质惠版时尚型,dazhongnat",
						"2015款 1.6 手动 质惠版时尚型,dazhongnau","2015款 1.4 手动 质惠版舒适型,dazhongnav","2015款 1.4 手动 质惠版时尚型,dazhongnaw","2015款 1.4T 自动 Sportline ,dazhongnax",
						"2015款 1.6 自动 豪华型,dazhongnay","2015款 1.6 手动 豪华型,dazhongnaz","2015款 1.6 自动 舒适型,dazhongnba","2015款 1.6 手动 舒适型,dazhongnbb","2015款 1.6 自动 时尚型,dazhongnbc",
						"2015款 1.6 手动 时尚型,dazhongnbd","2015款 1.4 手动 舒适型,dazhongnbe","2015款 1.4 手动 时尚型,dazhongnbf"
						],		
				
				"宝来":["2005款 三厢 1.8 手动 奥运限量版,dazhongoa","2005款 三厢 1.8 自动 奥运限量版,dazhongob","2005款 三厢 1.8T 手动 豪华型,dazhongoc","2005款 三厢 1.8T 自动 豪华型,dazhongod",
						"2005款 三厢 1.8T 手动 尊贵型,dazhongoe","2005款 三厢 1.8T 手动 舒适型,dazhongof","2005款 三厢 1.8 手动 豪华型,dazhongog","2005款 三厢 1.8 自动 豪华型,dazhongoh",
						"2005款 三厢 1.8 手动 尊贵型,dazhongoi","2005款 三厢 1.8 自动 尊贵型,dazhongoj","2005款 三厢 1.8 手动 舒适型,dazhongok","2005款 三厢 1.8 自动 舒适型,dazhongol",
						"2005款 三厢 1.6 手动 2V基本型,dazhongom","2005款 三厢 1.6 自动 2V基本型,dazhongon","2005款 三厢 1.8T 手动 R,dazhongoo","2006款 三厢 1.6 手动 2V时尚型TL,dazhongop",
						"2006款 三厢 1.6 自动 2V时尚型TL,dazhongoq","2006款 三厢 1.8 手动 5V豪华型HL,dazhongor","2006款 三厢 1.6 手动 2V豪华型HL ,dazhongos",
						"2006款 三厢 1.8 自动 5V豪华型HL,dazhongot","2006款 三厢 1.6 自动 2V豪华型HL,dazhongou","2006款 三厢 1.8 自动 舒适型,dazhongov","2006款 三厢 1.8 手动 豪华型,dazhongow",                 
						"2007款 三厢 1.6 自动 2V,dazhongox","2008款 三厢 1.6 手动 2V时尚型TL,dazhongoy","2008款 三厢 1.6 自动 2V时尚型TL,dazhongoz","2008款 三厢 1.6 手动 2V豪华型HL,dazhongoaa",
						"2008款 三厢 1.6 自动 2V豪华型HL,dazhongoab",
						"2008款 三厢 1.6 自动 时尚型,dazhongoac","2008款 三厢 1.6 手动 时尚型,dazhongoad","2008款 三厢 2.0 自动 豪华型,dazhongoae","2008款 三厢 1.6 自动 豪华型,dazhongoaf",
						"2008款 三厢 2.0 自动 舒适型,dazhongoag",
						"2008款 三厢 2.0 手动 舒适型,dazhongoah","2008款 三厢 1.6 自动 舒适型,dazhongoai","2008款 三厢 1.6 手动 舒适型,dazhongoaj",
						"2011款 三厢 1.4T 手动 Sportline,dazhongoak","2011款 三厢 1.4T 自动 Sportline,dazhongoal","2011款 三厢 1.6 自动 舒适型,dazhongoam","2011款 三厢 1.6 手动 舒适型,dazhongoan",
						"2011款 三厢 1.6 手动 时尚型,dazhongoao","2011款 三厢 1.6 自动 时尚型,dazhongoap","2011款 三厢 1.4T 手动 舒适型,dazhongoaq","2011款 三厢 1.4T 自动 舒适型,dazhongoar",
						"2011款 三厢 1.4T 手动 豪华型,dazhongoas","2011款 三厢 1.4T 自动 豪华型,dazhongoat",
						"2012款 三厢 1.6 手动 舒适超值版,dazhongoau","2012款 三厢 1.6 手动 时尚超值版,dazhongoav","2012款 三厢 1.6 手动 舒适型,dazhongoaw","2012款 三厢 1.6 手动 时尚型,dazhongoax",
						"2012款 三厢 1.6 自动 豪华型,dazhongoay","2012款 三厢 1.6 自动 舒适超值版,dazhongoaz","2012款 三厢 1.6 自动 舒适型,dazhongoba","2012款 三厢 1.6 自动 时尚超值版,dazhongobb",
						"2012款 三厢 1.6 自动 时尚型,dazhongobc","2012款 三厢 1.6 手动 时尚特别版,dazhongobd","2012款 三厢 1.6 自动 舒适特别版,dazhongobe","2012款 三厢 1.6 自动 时尚特别版,dazhongobf",
						"2012款 三厢 1.6 手动 舒适特别版,dazhongobg","2012款 三厢 1.4T 手动 舒适型,dazhongobh","2012款 三厢 1.4T 自动 舒适型,dazhongobi","2012款 三厢 1.4T 自动 豪华型,dazhongobj",
						"2013款 三厢 1.4T 手动 舒适型,dazhongobk","2013款 三厢 1.6 自动 时尚型,dazhongobl","2013款 三厢 1.4T 自动 豪华型,dazhongobm","2013款 三厢 1.4T 自动 舒适型,dazhongobn",
						"2013款 三厢 1.6 自动 豪华型,dazhongobo","2013款 三厢 1.6 自动 舒适型,dazhongobp","2013款 三厢 1.6 手动 舒适型,dazhongobq","2013款 三厢 1.6 手动 时尚型,dazhongobr",
						"2014款 三厢 1.4T 自动 Sportline,dazhongobs","2014款 三厢 1.4T 手动 Sportline,dazhongobt","2014款 三厢 1.6 自动 豪华型,dazhongobu","2014款 三厢 1.6 自动 舒适型,dazhongobv",
						"2014款 三厢 1.6 自动 时尚型,dazhongobw","2014款 三厢 1.6 手动 舒适型,dazhongobx","2014款 三厢 1.6 手动 时尚型,dazhongoby","2014款 三厢 1.4T 自动 豪华型,dazhongobz",
						"2014款 三厢 1.4T 自动 舒适型,dazhongoca","2014款 三厢 1.4T 手动 舒适型,dazhongocb",
						"2015款 三厢 1.4T 自动 质惠版Sportline,dazhongocc","2015款 三厢 1.6 自动 质惠版舒适型,dazhongocd","2015款 三厢 1.6 手动 质惠版舒适型,dazhongoce",
						"2015款 三厢 1.6 自动 质惠版时尚型,dazhongocf","2015款 三厢 1.6 手动 质惠版时尚型,dazhongocg"
						],	
				
				"高尔夫":["2005款 1.6 自动 舒适型2V,dazhongpa","2005款 1.6 自动 时尚型2V,dazhongpb","2005款 1.6 手动 舒适型2V,dazhongpc","2005款 1.6 手动 时尚型2V,dazhongpd",
						"2006款 1.6 手动 舒适型2V,dazhongpe","2006款 1.6 手动 时尚型2V,dazhongpf","2006款 1.6 自动 舒适型2V,dazhongpg","2006款 1.6 自动 时尚型2V,dazhongph",
						"2008款 1.6 手动 舒适型2V,dazhongpi","2008款 1.6 手动 时尚型2V,dazhongpj","2008款 1.6 自动 五彩奥运版,dazhongpk","2008款 1.6 自动 舒适型2V,dazhongpl",
						"2008款 1.6 自动 时尚型2V,dazhongpm","2008款 1.6 手动 五彩奥运版,dazhongpn",
						"2010款 1.6 自动 豪华型,dazhongpo","2010款 1.6 手动 舒适型,dazhongpp","2010款 1.4T 自动 舒适型,dazhongpq","2010款 1.4T 手动 舒适型,dazhongpr",
						"2010款 1.4T 自动 豪华型,dazhongps","2010款 1.6 自动 舒适型,dazhongpt","2010款 1.6 自动 时尚型,dazhongpu","2010款 1.6 手动 时尚型,dazhongpv",
						"2011款 1.6 手动 舒适型,dazhongpw","2011款 1.6 手动 时尚型,dazhongpx","2011款 1.6 自动 舒适型,dazhongpy","2011款 1.6 自动 时尚型,dazhongpz",
						"2011款 1.6 自动 豪华型,dazhongpaa","2011款 1.4T 手动 舒适型,dazhongpab","2011款 1.4T 自动 舒适型,dazhongpac","2011款 1.4T 自动 豪华型,dazhongpad",
						"2012款 1.4T 自动 舒适型,dazhongpae","2012款 1.4T 自动 豪华型,dazhongpaf","2012款 1.4T 自动 蓝驱版,dazhongpag","2012款 1.4T 手动 舒适型,dazhongpah",
						"2012款 1.6 手动 舒适型,dazhongpai","2012款 1.6 手动 时尚型,dazhongpaj","2012款 1.6 自动 舒适型,dazhongpak","2012款 1.6 自动 时尚型,dazhongpal",
						"2012款 1.6 自动 豪华型,dazhongpam","2013款 1.6 手动 舒适型,dazhongpan","2013款 1.6 手动 时尚型,dazhongpao","2013款 1.6 自动 舒适型,dazhongpap","2013款 1.6 自动 时尚型,dazhongpaq",
						"2013款 1.6 自动 豪华型,dazhongpar","2013款 1.4T 手动 舒适型,dazhongpas","2013款 1.4T 自动 舒适型,dazhongpat","2013款 1.4T 自动 豪华型,dazhongpau",
						"2014款 1.6 手动 时尚型,dazhongpav","2014款 1.6 自动 舒适型,dazhongpaw","2014款 1.6 自动 时尚型,dazhongpax","2014款 1.4T 手动 舒适型,dazhongpay",
						"2014款 1.4T 自动 舒适型,dazhongpaz","2014款 1.4T 自动 旗舰型,dazhongpba","2014款 1.4T 自动 豪华型,dazhongpbb",
						"2015款 1.4T 自动 R-Line,dazhongpbc","2015款 1.4T 手动 R-Line,dazhongpbd","2015款 1.2T 自动 舒适型,dazhongpbe","2015款 1.4T 手动 舒适型,dazhongpbf",
						"2015款 1.4T 自动 舒适型,dazhongpbg","2015款 1.4T 自动 旗舰型,dazhongpbh","2015款 1.4T 自动 豪华型,dazhongpbi","2015款 1.6 自动 舒适型,dazhongpbj",
						"2015款 1.6 自动 时尚型,dazhongpbk","2015款 1.6 手动 时尚型,dazhongpbl","2016款 1.4T 自动 旗舰型,dazhongpbm","2016款 1.4T 自动 R-Line,dazhongpbn","2016款 1.4T 手动 R-Line,dazhongpbo","2016款 1.4T 自动 豪华型,dazhongpbp",
						"2016款 1.4T 自动 舒适型,dazhongpbq","2016款 1.4T 手动 舒适型,dazhongpbr","2016款 1.2T 自动 舒适型,dazhongpbs","2016款 1.6 自动 舒适型,dazhongpbt",
						"2016款 1.6 自动 时尚型,dazhongpbu","2016款 1.6 手动 时尚型,dazhongpbv"
						],
				
				"速腾":["2006款 2.0 手动 舒适型,dazhongqa","2006款 2.0 手动 时尚型,dazhongqb","2006款 2.0 自动 舒适型,dazhongqc","2006款 2.0 自动 时尚型,dazhongqd",
						"2006款 1.8T 手动 舒适型丝绒版,dazhongqe","2006款 1.8T 手动 时尚型,dazhongqf","2006款 1.8T 手动 豪华型,dazhongqg","2006款 1.8T 自动 舒适型丝绒版,dazhongqh",
						"2006款 1.8T 自动 时尚型,dazhongqi","2006款 1.8T 自动 豪华型,dazhongqj","2006款 1.6 手动 时尚型,dazhongqk","2006款 1.6 自动 时尚型,dazhongql",
						"2007款 2.0 手动 舒适型,dazhongqm","2007款 2.0 手动 时尚型,dazhongqn","2007款 2.0 自动 舒适型,dazhongqo","2007款 2.0 自动 时尚型,dazhongqp",
						"2007款 1.8T 手动 豪华型,dazhongqq","2007款 1.8T 手动 时尚型,dazhongqr","2007款 1.8T 手动 豪华型,dazhongqs","2007款 1.8T 自动 时尚型,dazhongqt",
						"2007款 1.8T 自动 豪华型,dazhongqu","2007款 1.6 手动 时尚型,dazhongqv","2007款 1.6 自动 时尚型,dazhongqw","2007款 1.8T 手动 舒适型丝绒版,dazhongqx",
						"2007款 1.8T 手动 舒适型真皮版,dazhongqy","2007款 1.8T 自动 舒适型丝绒版,dazhongqz","2007款 1.8T 自动 舒适型真皮版,dazhongqaa",
						"2008款 1.8T 手动 舒适型丝绒版,dazhongqab","2008款 1.8T 手动 舒适型真皮版,dazhongqac","2008款 1.8T 自动 冠军版,dazhongqad","2008款 1.8T 手动 冠军版,dazhongqae",
						"2009款 1.6 手动 舒适型,dazhongqaf","2009款 1.6 自动 舒适型,dazhongqag","2009款 1.6 手动 时尚型,dazhongqah","2009款 1.6 自动 时尚型,dazhongqai","2009款 2.0 自动 舒适型真皮版,dazhongqaj",
						"2009款 2.0 手动 舒适型真皮版,dazhongqak","2009款 1.6 手动 时尚型真皮版,dazhongqal","2009款 1.6 自动 时尚型真皮版,dazhongqam","2009款 1.6 手动 时尚型天窗版,dazhongqan","2009款 1.6 自动 时尚型天窗版,dazhongqao",
						"2010款 1.8T 手动 冠军版,dazhongqap","2010款 1.8T 自动 冠军版,dazhongqaq","2010款 1.4T 自动 技术型,dazhongqar","2010款 1.4T 自动 豪华型,dazhongqas",
						"2010款 1.4T 手动 豪华型,dazhongqat","2010款 1.4T 手动 技术型,dazhongqau","2011款 1.6 自动 特别版,dazhongqav","2011款 1.6 手动 特别版,dazhongqaw","2011款 1.6 自动 特别版,dazhongqax","2011款 1.8T 手动 冠军版,dazhongqay",
						"2011款 1.8T 自动 冠军版,dazhongqaz","2011款 1.6 手动 舒适型,dazhongqba","2011款 1.6 手动 时尚型,dazhongqbb","2011款 1.6 自动 舒适型,dazhongqbc","2011款 1.6 自动 时尚型,dazhongqbd",
						"2011款 1.4T 手动 技术型,dazhongqbe","2011款 1.4T 手动 豪华型,dazhongqbf","2011款 1.4T 自动 技术型,dazhongqbg","2011款 1.4T 自动 豪华型,dazhongqbh",
						"2012款 1.4T 手动 豪华型,dazhongqbi","2012款 1.4T 自动 旗舰型,dazhongqbj","2012款 1.4T 自动 豪华型,dazhongqbk",
						"2012款 1.4T 自动 时尚型,dazhongqbl","2012款 1.4T 手动 时尚型,dazhongqbm",
						"2012款 1.8T 自动 旗舰型,dazhongqbn","2012款 1.6 自动 舒适型,dazhongqbo","2012款 1.6 手动 舒适型,dazhongqbp","2012款 1.6 自动 时尚型,dazhongqbq",
						"2012款 1.6 手动 时尚型,dazhongqbr","2013款 2.0T 自动 GLI,dazhongqbs","2013款 1.4T 自动 蓝驱版,dazhongqbt",
						"2014款 1.4T 自动 230TSI精英型改款,dazhongqbu","2014款 1.4T 手动 230TSI精英型改款,dazhongqbv","2014款 1.6 自动 领先型改款,dazhongqbw",
						"2014款 1.6 手动 领先型改款,dazhongqbx","2014款 1.8T 自动 旗舰型,dazhongqby","2014款 1.4T 自动 旗舰型,dazhongqbz","2014款 1.4T 自动 豪华型,dazhongqca",
						"2014款 1.4T 自动 时尚型,dazhongqcb","2014款 1.4T 自动 蓝驱版,dazhongqcc","2014款 1.6 自动 舒适型,dazhongqcd","2014款 1.4T 手动 豪华型,dazhongqce",
						"2014款 1.4T 手动 时尚型,dazhongqcf","2014款 1.6 手动 时尚型,dazhongqcg","2014款 1.6 自动 时尚型,dazhongqch","2014款 1.6 手动 舒适型,dazhongqci",
						"2015款 1.4T 自动 280TSI旗舰型,dazhongqcj","2015款 1.4T 自动 230TSI豪华型,dazhongqck","2015款 1.4T 自动 230TSI舒适型,dazhongqcl",
						"2015款 1.4T 手动 230TSI豪华型,dazhongqcm","2015款 1.4T 手动 230TSI舒适型,dazhongqcn","2015款 1.6 手动 舒适型,dazhongqco",
						"2015款 1.6 手动 时尚型,dazhongqcp","2015款 1.6 自动 舒适型,dazhongqcq","2015款 1.6 自动 时尚型,dazhongqcr"
						],
				
				"迈腾":["2007款 2.0T 自动,dazhongra","2007款 2.0 手动 标准型,dazhongrb","2007款 1.8T 手动 舒适型,dazhongrc","2007款 1.8T 手动 技术型,dazhongrd",
						"2007款 1.8T 手动 标准型,dazhongre","2007款 1.8T 自动 舒适型,dazhongrf","2007款 1.8T 自动 技术型,dazhongrg","2007款 1.8T 自动 豪华型,dazhongrh",
						"2008款 2.0T 自动 豪华型,dazhongr08a","2008款 2.0T 自动 舒适型,dazhongri",
						"2009款 1.4T 自动,dazhongrj","2009款 1.8T 自动 技术型,dazhongrk","2009款 1.8T 自动 舒适型,dazhongrl","2009款 1.8T 自动 豪华型,dazhongrm",
						"2009款 2.0T 自动 豪华型,dazhongrn","2009款 2.0T 自动 舒适型,dazhongro","2009款 1.8T 手动 舒适型,dazhongrp","2009款 1.8T 手动 技术型,dazhongrq",
						"2010款 1.4T 自动 技术升级型,dazhongrr","2010款 1.4T 自动 限量型,dazhongrs","2010款 1.8T 手动 舒适型,dazhongrt","2010款 1.8T 自动 舒适型,dazhongru",
						"2010款 1.8T 自动 豪华型,dazhongrv","2010款 1.4T 自动 精英型,dazhongrw","2010款 1.4T 自动 标准型,dazhongrx",
						"2011款 1.8T 手动 舒适型,dazhongry","2011款 1.4T 自动 精英型,dazhongrz","2011款 1.4T 自动 标准型,dazhongraa","2011款 1.8T 自动 尊贵型,dazhongrab",
						"2011款 1.8T 自动 豪华型,dazhongrac","2011款 1.8T 自动 舒适型,dazhongrad","2011款 2.0T 自动 舒适型,dazhongrae","2011款 2.0T 自动 豪华型,dazhongraf",
						"2012款 2.0T 自动 豪华型改款,dazhongrag","2012款 2.0T 自动 尊贵型改款,dazhongrah","2012款 2.0T 自动 至尊型改款,dazhongrai",
						"2012款 3.0 自动 旗舰型,dazhongraj","2012款 2.0T 自动 尊贵型,dazhongrak","2012款 2.0T 自动 至尊型,dazhongral","2012款 1.4T 自动 豪华型,dazhongram",
						"2012款 1.4T 自动 舒适型,dazhongran","2012款 1.8T 自动 尊贵型,dazhongrao","2012款 1.8T 自动 豪华型,dazhongrap","2012款 1.8T 自动 领先型,dazhongraq",
						"2012款 1.8T 自动 舒适型,dazhongrar","2013款 3.0 自动 旗舰型,dazhongras","2013款 1.8T 自动 尊贵型,dazhongrat","2013款 1.8T 自动 舒适型,dazhongrau","2013款 1.8T 自动 领先型,dazhongrav",
						"2013款 1.8T 自动 豪华型,dazhongraw","2013款 1.4T 自动 舒适型,dazhongrax","2013款 1.4T 自动 豪华型,dazhongray","2013款 2.0T 自动 至尊型,dazhongraz",
						"2013款 2.0T 自动 尊贵型,dazhongrca","2013款 2.0T 自动 豪华型,dazhongrcb","2013款 1.4T 自动 蓝驱版,dazhongrcc",
						"2015款 1.8T 自动 领先型,dazhongrcd","2015款 1.8T 自动 舒适型,dazhongrce","2015款 1.4T 自动 舒适型,dazhongrcf","2015款 1.8T 自动 尊贵型,dazhongrcg",
						"2015款 1.8T 自动 豪华型,dazhongrch","2015款 2.0T 自动 尊贵型,dazhongrci","2015款 1.4T 自动 豪华型,dazhongrcj","2015款 2.0T 自动 旗舰型,dazhongrck",
						"2015款 2.0T 自动 豪华型,dazhongrcl","2015款 2.0T 自动 尊贵型改款,dazhongrcm","2015款 2.0T 自动 旗舰型改款,dazhongrcn",
						"2015款 2.0T 自动 豪华型改款,dazhongrco","2015款 1.8T 自动 尊贵型改款,dazhongrcp","2015款 1.8T 自动 豪华型改款,dazhongrcq","2015款 1.8T 自动 领先型改款,dazhongrcr",
						"2015款 1.8T 自动 舒适型改款,dazhongrcs","2015款 1.4T 自动 豪华型改款,dazhongrct","2015款 1.4T 自动 舒适型改款,dazhongrcu"
						],
				
				
				
				
				
				"一汽-大众CC":["2010款 2.0T 自动 至尊型,dazhongsa","2010款 2.0T 自动 豪华型,dazhongsb","2010款 2.0T 自动 尊贵型,dazhongsc",
						"2011款 1.8T 自动 尊贵型,dazhongsd","2011款 1.8T 自动 豪华型,dazhongse","2011款 2.0T 自动 尊贵型,dazhongsf","2011款 2.0T 自动 至尊型,dazhongsg",
						"2011款 2.0T 自动 豪华型,dazhongsh","2012款 3.0 自动 V6,dazhongsi","2012款 1.8T 自动 豪华型,dazhongsj","2012款 1.8T 自动 精英型,dazhongsk","2012款 1.8T 自动 尊贵型,dazhongsl",
						"2012款 2.0T 自动 至尊型,dazhongsm","2012款 2.0T 自动 尊贵型,dazhongsn","2012款 2.0T 自动 豪华型,dazhongso",
						"2013款 1.8T 自动 尊贵型,dazhongsp","2013款 1.8T 自动 豪华型,dazhongsq",
						"2013款 3.0 自动 V6,dazhongsr","2013款 2.0T 自动 尊贵型,dazhongss","2013款 2.0T 自动 至尊型,dazhongst","2013款 2.0T 自动 豪华型,dazhongsu",
						"2015款 3.0 自动 V6,dazhongsv","2015款 2.0T 自动 至尊型,dazhongsw","2015款 2.0T 自动 豪华型,dazhongsx","2015款 2.0T 自动 尊贵型,dazhongsy",
						"2015款 1.8T 自动 豪华型,dazhongsz","2015款 1.8T 自动 尊贵型,dazhongsaa",                          
						"2016款 3.0 自动 V6,dazhongsab","2016款 2.0T 自动 至尊型,dazhongsac","2016款 2.0T 自动 豪华型,dazhongsad","2016款 2.0T 自动 尊贵型,dazhongsae",
						"2016款 1.8T 自动 豪华型,dazhongsaf","2016款 1.8T 自动 尊贵型,dazhongsag"
						],


				
				"开迪":["2005款 1.6 自动 前驱,dazhongta","2005款 1.4 自动 前驱,dazhongtb","2005款 1.4 手动 前驱,dazhongtc",
						"2005款 1.6 手动 舒适型7座,dazhongtd","2005款 1.6 手动 功能型7座,dazhongte","2005款 1.6 手动 功能型5座,dazhongtf","2005款 1.6 手动 舒适型5座,dazhongtg"
						],
				
				
				"甲壳虫":["2005款 敞篷版 2.5 手动,dazhongua","2005款 敞篷版 1.6 手动,dazhongub","2005款 敞篷版 1.4 手动,dazhonguc","2005款 敞篷版 2.0 自动,dazhongud",
						"2005款 掀背 2.5 手动 ,dazhongue","2005款 掀背 2.5 自动 ,dazhonguf","2005款 掀背 1.8T 手动 ,dazhongug","2005款 掀背 1.8T 自动 ,dazhonguh",
						"2005款 掀背 2.0 手动 ,dazhongui","2005款 掀背 1.6 自动 ,dazhonguj","2005款 掀背 1.6 手动 ,dazhonguk","2005款 掀背 1.4 手动 ,dazhongul","2006款 敞篷版 1.8T 自动,dazhongum",
						"2006款 敞篷版 2.0 自动 豪华版,dazhongun","2006款 掀背 2.0 自动 豪华版,dazhonguo","2006款 掀背 1.8T 自动 运动版,dazhongup","2006款 掀背 2.0 自动 顶配版,dazhonguq",
						"2006款 掀背 2.0 自动 标配版,dazhongur","2008款 掀背 2.0 自动 顶配版,dazhongus","2008款 掀背 2.0 自动 标配版,dazhongut",
						"2008款 掀背 1.8T 自动 豪华版,dazhonguu","2009款 敞篷版 2.0 自动 顶配,dazhonguv","2009款 敞篷版 2.0 自动 标配,dazhonguw","2010款 敞篷版 2.0 自动 墨橘豪华版,dazhongux",
						"2010款 掀背 2.0 自动 墨橘舒适版,dazhonguy","2010款 掀背 2.0 自动 墨橘豪华版,dazhonguz","2010款 掀背 1.8T 自动 墨橘豪华版,dazhonguaa",
						"2010款 掀背 1.6 自动 ,dazhonguab","2011款 掀背 2.0T 自动 ,dazhonguac","2011款 掀背 1.4T 自动 ,dazhonguad","2011款 掀背 1.2T 自动 ,dazhonguae",
						"2011款 掀背 2.5 自动 ,dazhonguaf","2013款 掀背 1.4T 自动 R-Line,dazhonguag","2013款 掀背 1.4T 自动 摇滚版Fender,dazhonguah","2013款 掀背 2.0T 自动 顶配型,dazhonguai",
						"2013款 掀背 1.4T 自动 豪华型,dazhonguaj","2013款 掀背 1.4T 自动 时尚型,dazhonguak","2013款 掀背 1.4T 自动 舒适型,dazhongual",
						"2013款 掀背 1.2T 自动 舒适型,dazhonguam","2013款 掀背 1.2T 自动 时尚型,dazhonguan",
						"2014款 掀背 1.4T 自动 R-Line,dazhonguao","2014款 掀背 2.0T 自动 性能版,dazhonguap",
						"2015款 掀背 2.0T 自动,dazhonguaq","2015款 掀背 1.4T 自动,dazhonguar","2015款 掀背 1.2T 自动,dazhonguas",
						"2015款 敞篷版 1.2T 自动 ,dazhonguat"
						],   
				
				
				"高尔夫(进口)":["2006款 三门版 2.0T 自动 GTI,dazhongva","2006款 五门版 2.0T 自动 GTI 五门版,dazhongvb",
								"2009款 R 2.0T 自动 六代五门版,dazhongvc","2010款 GTI 2.0T 自动,dazhongvd",
								"2011款 R 2.0T 自动 五门版,dazhongve","2011款 Cross 1.4T 自动,dazhongvf", 
								"2011款 敞篷 1.4T 自动,dazhongvg","2012款 敞篷 1.4T 自动 舒适型,dazhongvh","2011款 GTI 2.0T 自动,dazhongvi",
								"2011款 旅行版  1.4T 自动 豪华型,dazhongvj","2011款 旅行版  1.4T 自动 舒适型,dazhongvk","2012款 GTI 2.0T 自动,dazhongvl", 
								"2012款 敞篷 1.4T 自动 豪华型,dazhongvm","2013款 敞篷 2.0T 自动 GTI,dazhongvn","2014款 旅行版  1.4T 自动,dazhongvo",
								"2015款 旅行版  1.4T 自动 舒适型,dazhongvp","2014款 R 2.0T 自动 敞篷版,dazhongvq","2015款 Sportsvan 1.4T 自动,dazhongvr",                 
								"2015款 GTE 1.4T 自动 GTE,dazhongvs",                
								"2015款 R旅行版 2.0T 自动 R 四驱,dazhongvt","2015款 R 2.0T 自动 五门版,dazhongvu","2016款 GTI 2.0T 自动,dazhongvv"
								],

				
						
				"辉腾":["2006款 6.0 自动 豪华型4座,dazhongwa","2007款 3.2 自动 基本版5座,dazhongwb","2007款 3.6 自动 行政版加长5座,dazhongwc",
						"2007款 3.6 自动 行政版加长4座,dazhongwd","2007款 3.6 自动 舒适版加长5座,dazhongwe",
						"2007款 3.6 自动 豪华版加长5座,dazhongwf","2007款 3.6 自动 顶级版加长5座,dazhongwg","2007款 6.0 自动 尊崇版5座,dazhongwh",
						"2007款 6.0 自动 尊崇版4座,dazhongwi",
						"2007款 4.2 自动 行政版5座,dazhongwj","2007款 4.2 自动 行政版加长4座,dazhongwk","2008款 4.2 自动 豪华型4座,dazhongwl",
						"2009款 6.0 自动 尊崇版加长5座,dazhongwm","2009款 6.0 自动 尊崇版加长4座,dazhongwn","2009款 3.6 自动 豪华版加长5座,dazhongwo",
						"2009款 3.6 自动 顶级版加长5座,dazhongwp",
						"2009款 3.6 自动 行政版加长5座,dazhongwq","2009款 3.6 自动 行政版加长4座,dazhongwr","2009款 3.6 自动 舒适版加长5座,dazhongws",
						"2009款 4.2 自动 行政版加长5座,dazhongwt",
						"2009款 4.2 自动 行政版加长4座,dazhongwu","2010款 6.0 自动,dazhongwv","2010款 4.2 自动,dazhongww",
						"2011款 6.0 自动 Individual版加长4座,dazhongwx","2011款 4.2 自动 Individual版加长4座,dazhongwy","2011款 3.6 自动 舒适版加长5座,dazhongwz",
						"2011款 3.6 自动 商务版加长5座,dazhongwaa","2011款 3.6 自动 Individual版加长5座,dazhongwab","2011款 3.6 自动 Individual版加长4座,dazhongwac",
						"2012款 6.0 自动 奢想定制型,dazhongwad","2012款 4.2 自动 奢想定制型,dazhongwae","2012款 3.6 自动 尊享定制型,dazhongwaf",
						"2012款 3.0 自动 精英定制型,dazhongwag",
						"2012款 3.0T 自动 精英定制型 柴油,dazhongwah","2012款 3.0T 自动 行政型 柴油,dazhongwai","2012款 3.0 自动 行政型,dazhongwaj",
						"2012款 3.0 自动 商务型,dazhongwak",
						"2013款 4.2 自动 柏秋纳弗洛限量版,dazhongwal","2014款 3.0 自动 行政型,dazhongwam","2014款 3.0 自动 商务型,dazhongwan",
						"2015款 4.2 自动 奢享定制型,dazhongwao","2015款 3.6 自动 尊享定制型,dazhongwap","2015款 3.0 自动 精英定制型,dazhongwaq",
						"2015款 3.0 自动 商务型,dazhongwar"
						],
				
				"迈腾(进口)":["2005款 三厢 3.2 自动 豪华型四驱,dazhongxa","2005款 三厢 3.2 自动 尊贵型四驱,dazhongxb","2012款 2.0T 自动 豪华版四驱,dazhongxc",
						"2012款 2.0T 自动 舒适版四驱,dazhongxd","2012款 2.0T 自动 豪华版,dazhongxe","2012款 2.0T 自动 舒适版,dazhongxf"
						],
 
						
						
				
				"途锐":["2005款 3.2 自动 标准型四驱,dazhongya","2005款 6.0 自动 顶配型四驱,dazhongyb",
						"2006款 4.2自动 顶级型四驱,dazhongyc","2006款 3.2 自动 顶级型四驱,dazhongyd","2006款 3.2 自动 豪华型四驱,dazhongye","2006款 2.5T 自动 四驱 柴油,dazhongyf",
						"2006款 3.6 自动 标准型四驱,dazhongyg","2006款 3.6 自动 顶级型四驱,dazhongyh","2006款 3.6 自动 舒适型四驱,dazhongyu","2006款 3.6 自动 豪华型四驱,dazhongyj",
						"2007款 6.0 自动 顶配型四驱,dazhongyk","2007款 4.2 自动 基本型四驱,dazhongyl","2007款 3.6 自动 基本型四驱,dazhongym","2007款 4.2 自动 顶配型四驱,dazhongyn",
						"2007款 3.0T 自动 舒适型四驱 柴油,dazhongyo","2007款 3.0T 自动 豪华型四驱 柴油,dazhongyp",
						"2009款 3.0T 自动 舒适型四驱 柴油,dazhongyq","2009款 3.0T 自动 豪华型四驱 柴油,dazhongyr","2009款 3.0T 自动 高配型四驱 柴油,dazhongys",
						"2010款 4.2T 自动 四驱 柴油,dazhongyt","2010款 3.6 自动 四驱,dazhongyu","2010款 4.2 自动 限量顶配型四驱,dazhongyv","2010款 3.6 自动 限量舒适型四驱,dazhongyw",
						"2010款 3.6 自动 限量豪华型四驱,dazhongyx","2010款 3.6 自动 限量顶配型四驱,dazhongyy","2010款 3.0T 自动 舒适型四驱 柴油,dazhongyz",
						"2010款 3.0T 自动 豪华型四驱 柴油,dazhongyaa","2010款 3.0T 自动 顶配型四驱 柴油,dazhongyab","2011款 3.0T 自动 四驱 油电混合,dazhongyac",
						"2011款 3.0T 自动 舒适型四驱,dazhongyad","2011款 3.0T 自动 豪华型四驱,dazhongyae","2011款 3.0T 自动 高配型四驱,dazhongyaf",
						"2011款 3.0T 自动 标配型四驱,dazhongyag","2011款 3.0T 自动 舒适型四驱 柴油,dazhongyah","2011款 3.0T 自动 豪华型四驱 柴油,dazhongyai",
						"2011款 3.0T 自动 高配型四驱 柴油,dazhongyaj",
						"2011款 3.0T 自动 标配型四驱 柴油,dazhongyak","2012款 3.0T 自动 豪华型R-Line四驱,dazhongyal","2012款 3.0T 自动 高配型R-Line四驱,dazhongyam",
						"2013款 3.0T 自动 X十周年限量版四驱,dazhongyan","2013款 3.6 自动 越野增强豪华型四驱,dazhongyao","2013款 3.6 自动 越野增强高配型四驱,dazhongyap",
						"2013款 3.0T 自动 限量奢华版四驱,dazhongyaq","2014款 4.2 自动,dazhongyar","2014款 3.0T 自动 黑色探险者四驱,dazhongyas",
						"2014款 3.0T 自动 新锐版四驱,dazhongyat","2015款 3.0T 自动 R-Line豪华型四驱,dazhongyau","2015款 3.0T 自动 舒适型四驱,dazhongyav",
						"2015款 3.0T 自动 豪华型四驱,dazhongyaw","2015款 3.0T 自动 高配型四驱,dazhongyax","2015款 3.0T 自动 标配型四驱,dazhongyay",
						"2015款 3.0T 自动 舒适型四驱 柴油,dazhongyaz","2015款 3.0T 自动 标配型四驱 柴油,dazhongyba","2015款 3.0T 自动 耀锐限量版四驱,dazhongybb",
						"2016款 3.0T 自动 R-Line四驱,dazhongybc","2016款 3.0T 自动 高配型四驱,dazhongybd","2016款 3.0T 自动 舒适型四驱,dazhongybe",
						"2016款 3.0T 自动 标配型四驱,dazhongybf"
						],
				
				"夏朗":["2005款 1.8T 自动 ,dazhongza","2010款 2.0T 自动 ,dazhongzb","2012款 2.0T 自动 舒适版,dazhongzc",
						"2012款 2.0T 自动 豪华版,dazhongzd","2012款 2.0T 自动 标配版,dazhongze",
						"2013款 2.0T 自动 豪华版,dazhongzf","2013款 2.0T 自动 舒适版,dazhongzg","2013款 2.0T 自动 标配版,dazhongzh",
						"2013款 1.8T 自动 舒适版,dazhongzi","2013款 1.8T 自动 标配版,dazhongzj",
						"2014款 2.0T 自动 豪华版,dazhongzk","2014款 2.0T 自动 舒适版,dazhongzl","2014款 2.0T 自动 标配版,dazhongzm"
						],

				
				"迈特威":["2008款 3.2 自动 豪华版前驱,dazhongaa1a","2012款 2.0T 自动 尊享版四驱,dazhongaa1b","2012款 2.0T 自动 豪华版前驱,dazhongaa1c",
						"2012款 2.0T 自动 商务版前驱,dazhongaa1d","2016款 2.0T 自动 65周年限量版,dazhongaa1e","2014款 2.0T 自动 尊享版四驱,dazhongaa1f",
						"2014款 2.0T 自动 行政版四驱,dazhongaa1g"
						],

				
				"凯路威":["2008款 3.2 自动 舒适版前驱,dazhongab1a","2014款 2.0T 自动 舒适版四驱,dazhongab1b","2014款 2.0T 自动 舒适版前驱,dazhongab1c",
						"2015款 2.0T 自动 舒适版四驱,dazhongab1d","2015款 2.0T 自动 舒适版前驱,dazhongab1e"
						], 
				
				"尚酷":["2009款 两厢 1.4T 自动 运动版,dazhongac1a","2009款 两厢 2.0T 自动 运动版,dazhongac1b","2009款 两厢 1.4T 手动 运动版,dazhongac1c",
						"2009款 R 2.0T 自动 ,dazhongac1d","2009款 两厢 1.4T 自动 豪华全景天窗版,dazhongac1e","2009款 两厢 1.4T 自动 豪华版,dazhongac1f","2009款 两厢 2.0T 自动 豪华版,dazhongac1g",
						"2009款 两厢 1.4T 自动 风尚版,dazhongac1h","2010款 R 2.0T 自动 ,dazhongac1i","2011款 两厢 2.0T 自动 R-Line,dazhongac1j","2011款 两厢 1.4T 自动 舒适版,dazhongac1k",
						"2011款 两厢 2.0T 自动 豪华版,dazhongac1l","2011款 两厢 1.4T 自动 风尚版,dazhongac1m","2011款 R 2.0T 自动 四驱,dazhongac1n","2013款 两厢 2.0T 自动 百万里程版,dazhongac1o",
						"2013款 两厢 2.0T 自动 GTS,dazhongac1p","2013款 两厢 1.4T 自动 GTS,dazhongac1q","2014款 两厢 1.4T 自动 竞驰版,dazhongac1r",                                 
						"2015款 R 2.0T 自动 ,dazhongac1s","2015款 两厢 2.0T 自动 豪华版,dazhongac1t","2015款 两厢 1.4T 自动 舒适版,dazhongac1u",
						"2015款 两厢 1.4T 自动 风尚版,dazhongac1v"
						],
				
				"大众CC":["2009款 3.6 自动 舒适版四驱,dazhongad1a","2009款 3.6 自动 顶配版四驱,dazhongad1b",
							"2010款 2.0T 自动 豪华版,dazhongad1c","2010款 2.0T 自动 风尚版,dazhongad1d","2010款 2.0T 自动 豪华版四驱,dazhongad1e",
							"2010款 2.0T 自动 风尚版四驱,dazhongad1f","2012款 3.6 自动 R-Line四驱,dazhongad1g","2012款 1.8T 自动,dazhongad1h"
							],
											
				"大众Eos":["2006款 2.0 自动,dazhongae1a","2008款 2.0T 手动,dazhongae1b","2008款 2.0T 自动,dazhongae1c",
							"2010款 2.0T 自动 豪华版,dazhongae1d","2010款 2.0T 自动 舒适版,dazhongae1e","2010款 2.0T 自动 月夜版,dazhongae1f",
							"2011款 2.0T 自动 黑顶版,dazhongae1g","2011款 2.0T 自动,dazhongae1h"
							],
				
				"桑塔纳3000":["2005款 1.8 手动 舒适型,dazhongaf1a","2005款 1.8 自动 舒适型,dazhongaf1b","2006款 1.8 手动 双燃料,dazhongaf1c",
							"2006款 2.0 自动 豪华型,dazhongaf1d","2006款 2.0 手动 豪华型,dazhongaf1e","2006款 1.8 手动 舒适型,dazhongaf1f","2006款 1.8 手动 导入型,dazhongaf1g",
							"2007款 1.8 自动 舒适型,dazhongaf1h","2008款 1.8 手动 政府采购版,dazhongaf1i"
							],
				
				"UP!":["2015款 1.0 自动 high up!,dazhongag1a",
						"2015款 1.0 自动 move up!,dazhongag1b"
						],


//"大通"				
					
					"V80":["2011款 2.5T 手动 尊杰版长轴中顶 柴油,datongaa","2011款 2.5T 手动 商杰版长轴高顶 柴油,datongab","2011款 2.5T 手动 商杰版长轴中顶 柴油,datongac",
							"2011款 2.5T 手动 尊杰版短轴中顶 柴油,datongad","2011款 2.5T 手动 尊杰版短轴低顶 柴油,datongae","2011款 2.5T 手动 商杰版短轴中顶 柴油,datongaf",
							"2012款 2.5T 手动 商杰版短轴低顶 柴油,datongag","2012款 2.5T 手动 舒适商务车2+2+3座 柴油,datongah","2012款 2.5T 手动 豪华商务车2+2+3座 柴油,datongai",
							"2012款 2.5T 手动 高级商务车2+3座 柴油,datongaj","2012款 2.5T 手动 商务车VIP2+3座 柴油,datongak","2012款 2.5T 手动 舒适商务车2+2+2座 柴油,datongal",
							"2012款 2.5T 手动 豪华商务车2+2+2座 柴油,datongam","2012款 2.5T 手动 高级商务车2+2座 柴油,datongan","2012款 2.5T 手动 商务车VIP2+2座 柴油,datongao",
							"2012款 2.5T 手动 商杰版长轴高顶 柴油,datongap","2012款 2.5T 手动 商杰版长轴中顶 柴油,datongaq","2012款 2.5T 手动 商杰版短轴中顶 柴油,datongar",
							"2012款 2.5T 手动 尊杰版短轴低顶 柴油,datongas","2012款 2.5T 手动 尊杰版短轴中顶 柴油,datongat","2012款 2.5T 手动 尊杰版长轴中顶 柴油,datongau",
							"2012款 2.5T 手动 傲运通F短轴中顶 柴油,datongav","2012款 2.5T 手动 傲运通E短轴中顶 柴油,datongaw","2012款 2.5T 手动 傲运通F短轴低顶 柴油,datongax",
							"2012款 2.5T 手动 傲运通E短轴低顶 柴油,datongay","2012款 2.5T 手动 傲运通D短轴中顶 柴油,datongaz","2012款 2.5T 手动 傲运通D短轴低顶 柴油,datongaaa",
							"2012款 2.5T 手动 傲运通C短轴中顶 柴油,datongaab","2012款 2.5T 手动 傲运通C短轴低顶 柴油,datongaac","2012款 2.5T 手动 傲运通B长轴高顶 柴油,datongaad",
							"2012款 2.5T 手动 傲运通B长轴中顶 柴油,datongaae","2012款 2.5T 手动 傲运通A短轴低顶 柴油,datongaaf","2012款 2.5T 手动 傲运通A短轴中顶 柴油,datongaag",
							"2013款 2.5T 手动 傲运通长轴高顶 柴油,datongaah","2013款 2.5T 手动 傲运通长轴中顶 柴油,datongaai","2013款 2.5T 手动 傲运通短轴中顶 柴油,datongaaj",
							"2013款 2.5T 手动 傲运通短轴低顶 柴油,datongaak","2014款 2.5T 手动 商旅版长轴高顶 柴油,datongaal","2014款 2.5T 手动 尊杰行政版长轴高顶 柴油,datongaam",
							"2014款 2.5T 手动 尊杰行政版短轴低顶 柴油,datongaan","2014款 2.5T 手动 尊杰版长轴高顶 柴油,datongaao","2014款 2.5T 手动 尊杰版短轴低顶 柴油,datongaap",
							"2014款 2.5T 手动 商杰版长轴高顶 柴油,datongaaq","2014款 2.5T 手动 商杰版长轴中顶 柴油,datongaar","2014款 2.5T 手动 商杰版短轴中顶 柴油,datongaas",
							"2014款 2.5T 手动 商杰版短轴低顶 柴油,datongaat","2015款 2.5T 自动 傲运通长轴高顶 柴油,datongaau","2015款 2.5T 手动 傲运通长轴高顶 柴油,datongaav",
							"2015款 2.5T 手动 傲运通短轴低顶 柴油,datongaaw","2015款 2.5T 手动 长轴高顶商旅版 柴油,datongaax","2015款 2.5T 手动 长轴中顶商旅版 柴油,datongaay",
							"2015款 2.5T 手动 长轴中顶傲运通版 柴油,datongaaz","2015款 2.5T 手动 长轴高顶商杰版 柴油,datongaba","2015款 2.5T 手动 长轴中顶商杰版 柴油,datongabb"
							],


					"G10":["2014款 2.0T 自动 旗舰版,datongba","2014款 2.0T 自动 豪华行政版,datongbb","2014款 2.0T 自动 行政版,datongbc",
							"2014款 2.0T 自动 豪华版,datongbd","2014款 2.0T 自动 时尚版,datongbe","2014款 2.0T 自动 精英版,datongbf",
							"2014款 2.0T 手动 豪华版,datongbg","2014款 2.0T 手动 精英版,datongbh","2014款 2.4 手动 豪华版,datongbi",
							"2014款 2.4 手动 精英版,datongbj","2014款 2.4 手动 时尚版,datongbk","2015款 2.4 手动 纪念版,datongbl",
							"2015款 2.0T 手动 纪念版,datongbm","2015款 2.0T 自动 纪念版,datongbn"
							],
					
//"帝豪"			
					
					"EC7":["2009款 1.8 手动 舒适型,dihaoa09a","2009款 1.8 手动 旗舰型,dihaoa09b","2009款 1.8 手动 豪华型,dihaoa09c",
							"2010款 1.5 手动 尊贵型,dihaoa10a","2010款 1.5 手动 舒适型,dihaoa10b","2010款 1.5 手动 豪华型,dihaoa10c","2010款 1.8 自动 尊贵型,dihaoa10d",
							"2010款 1.8 自动 精英型,dihaoaa","2010款 1.8 自动 天窗型,dihaoab","2010款 1.8 自动 豪华型,dihaoac","2010款 1.8 手动 尊贵型,dihaoad",
							"2010款 1.8 手动 豪华型,dihaoae","2010款 1.8 手动 舒适型,dihaoaf","2010款 1.8 手动 尊贵型,dihaoag","2010款 1.8 手动 标准型,dihaoah",
							"2010款 1.8 自动 尊贵型,dihaoai","2010款 1.8 自动 天窗型,dihaoaj","2010款 1.8 自动 精英型,dihaoak","2010款 1.8 自动 豪华型,dihaoal",                 
							"2012款 1.8 自动 油气混合,dihaoam","2012款 1.8 手动 尊贵型 油电混合,dihaoan","2012款 1.8 手动 舒适型 油电混合,dihaoao",
							"2012款 1.8 手动 豪华型 油电混合,dihaoap","2012款 1.8 手动 GSG版 油电混合,dihaoaq","2012款 1.8 手动 ESC型 油电混合,dihaoar",
							"2012款 1.8 自动 尊贵型 油气混合,dihaoas","2012款 1.8 自动 舒适型 油气混合,dihaoat","2012款 1.8 自动 豪华型 油气混合,dihaoau",
							"2012款 1.5 手动 超悦型,dihaoav","2012款 1.8 手动 尊贵型,dihaoaw","2012款 1.8 手动 舒适型,dihaoax","2012款 1.8 手动 豪华型,dihaoay",
							"2012款 1.8 手动 GSG版,dihaoaz","2012款 1.8 手动 ESC版,dihaoaaa","2012款 1.8 自动 尊贵型,dihaoaab","2012款 1.8 自动 舒适型,dihaoaac",
							"2012款 1.8 自动 豪华型,dihaoaad","2012款 1.5 手动 舒适型,dihaoaae","2012款 1.5 手动 标准型,dihaoaaf","2012款 1.8 手动 尊贵型,dihaoaag",
							"2012款 1.8 手动 舒适型,dihaoaah",
							"2012款 1.8 手动 豪华型,dihaoaai","2012款 1.5 手动 超悦型,dihaoaaj","2012款 1.8 手动 尊贵型 油电混合,dihaoaak","2012款 1.8 手动 舒适型 油电混合,dihaoaal",
							"2012款 1.8 手动 豪华型 油电混合,dihaoaam","2012款 1.8 自动 尊贵型,dihaoaan","2012款 1.8 自动 舒适型,dihaoaao","2012款 1.8 自动 豪华型,dihaoaap",
							"2012款 1.5 手动 舒适型,dihaoaaq","2012款 1.5 手动 豪华型,dihaoaar","2012款 1.5 手动 标准型,dihaoaas","2013款 1.8 自动 尊贵型,dihaoaat",
							"2013款 1.8 自动 精英型,dihaoaau","2013款 1.5 手动 尊贵型,dihaoaav","2013款 1.5 手动 精英型,dihaoaaw","2013款 1.5 手动 进取型,dihaoaax",
							"2013款 1.8 手动 精英型 油气混合,dihaoaay","2013款 1.8 手动 进取型 油气混合,dihaoaaz","2013款 1.8 手动 尊贵型,dihaoaba","2013款 1.8 手动 旗舰型,dihaoabb",
							"2013款 1.8 手动 精英型,dihaoabc","2013款 1.8 自动 旗舰型,dihaoabd","2013款 1.8 自动 尊贵型,dihaoabe","2013款 1.8 自动 精英型,dihaoabf",
							"2013款 1.5 手动 精英型,dihaoabg","2013款 1.5 手动 进取型,dihaoabh"
							],
					
					"EC8":["2011款 2.4 自动 尊贵型,dihaoba","2011款 2.4 自动 行政型,dihaobb","2011款 2.4 自动 豪华型,dihaobc","2011款 2.4 自动 BMBS版,dihaobd",
							"2011款 2.0 手动 舒适型,dihaobe","2011款 2.0 手动 标准型,dihaobf","2011款 2.0 自动 尊贵型,dihaobg","2011款 2.0 自动 行政型,dihaobh",
							"2011款 2.0 自动 舒适型,dihaobi","2011款 2.0 自动 豪华型,dihaobj","2011款 2.0 自动 标准型,dihaobk","2011款 2.0 自动 BMBS版,dihaobl",
							"2013款 2.4 自动 至尊型,dihaobm","2013款 2.4 自动 行政型,dihaobn","2013款 2.4 自动 旗舰型,dihaobo","2013款 2.4 自动 尊贵型,dihaobp",
							"2013款 2.0 自动 旗舰型,dihaobq","2013款 2.0 自动 尊贵型,dihaobr","2013款 2.0 手动 尊贵型,dihaobs","2013款 2.0 自动 精英型,dihaobt",
							"2013款 2.0 手动 精英型,dihaobu","2013款 2.0 自动 进取型,dihaobv","2013款 2.0 手动 进取型,dihaobw",
							"2015款 2.0 自动 尊贵型,dihaobx","2015款 2.0 自动 精英型,dihaoby","2015款 2.0 手动 精英型,dihaobz","2015款 2.0 手动 进取型,dihaobaa"
							],
					

//"东风"
					"小康K07":["2005款 1.0 手动 5-8座,dongfengaa","2006款 1.0 手动 5-8座,dongfengab","2007款 1.05 手动 5座,dongfengac",
							  "2008款 1.0 手动 5座福运08,dongfengad",
							  "2009款 1.0 手动 5-8座AF1007,dongfengae","2013款 1.0 手动 基本型5座,dongfengaf","2014款 1.2 手动 经典版,dongfengag" 
							],
					
					
					"小康K07-ii":["2007款 1.0 手动 基本型8座,dongfengba","2008款 1.05 手动 3U金钻5-8座标准型,dongfengbb",
								 "2008款 1.0 手动 3U金钻5-8座豪华型,dongfengbc","2009款 1.0 手动 3U金钻5-8座,dongfengbd",
							     "2013款 1.0 手动 基本型7座,dongfengbe","2014款 1.0 手动 标准型7座,dongfengbf","2015款 1.0 手动 S实用型7座,dongfengbg" 
							],
							
					"小康K17":["2008款 1.0 手动 创业先锋5座,dongfengca","2008款 1.0 手动 创业先锋5-8座,dongfengcb",
							  "2009款 1.0 手动 创业先锋5-8座,dongfengcc","2009款 1.0 手动 基本型AF1006,dongfengcd",
							  "2011款 1.0 手动 民生版5座,dongfengce","2011款 1.0 手动 民生版5-8座,dongfengcf"
							],		
							
							
					"小康K06":["2006款 1.05 手动 6-8座,dongfengda","2006款 1.3 手动 6-8座,dongfengdb",
							  "2008款 1.0 手动 5-8座,dongfengdc","2009款 1.0 手动 5-8座AF1007,dongfengdd",
							  "2009款 1.0 手动 5-8座,dongfengde","2010款 1.0 手动 5-7座,dongfengdf"
							],
					
					"小康V27":["2009款 1.0 手动 5-8座,dongfengea","2009款 1.3 手动 5-8座,dongfengeb",
							  "2010款 1.0 手动 标准型5-8座,dongfengec","2010款 1.0 手动 豪华型5-8座,dongfenged",
							  "2010款 1.0 手动 简单型5-8座,dongfengee","2010款 1.3 手动 标准型5-8座,dongfengef"
							],
					
					"小康V29":["2012款 1.2 手动 标准型,dongfengfa","2012款 1.3 手动 精典型DK1305,dongfengfb",
							  "2012款 1.3 手动 舒适型DK1306,dongfengfc"
							],
							
					"小康C37":["2013款 1.2 手动 创业I型,dongfengga","2013款 1.4 手动 精典型,dongfenggb",
							  "2013款 1.4 手动 创业II型,dongfenggc","2014款 1.3 手动 豪华型,dongfenggd",
							  "2014款 1.3 手动 精典型,dongfengge","2015款 1.5 手动 豪华型,dongfenggf",
							  "2015款 1.5 手动 精典型,dongfenggg"
							],
							
					"小康V26":["2011款 1.3 手动 BG1320,dongfengha","2011款 1.0 手动 BG1001,dongfenghb"
							],
							
					"小康V07S":["2011款 1.0 手动 基本型AF1006,dongfengia","2011款 1.0 手动 标准型BG1001,dongfengib"
							],
							
					"小康风光":["2014款 1.5 手动 360豪华型5座,dongfengja","2014款 1.5 手动 330舒适型5座,dongfengjb","2014款 1.5 手动 330标准型5座,dongfengjc",
								"2014款 1.5 手动 330实用型5座,dongfengjd","2014款 1.3T 手动 360节能型 柴油,dongfengje","2014款 1.3 自动 330豪华型7座,dongfengjf",
								"2014款 1.3 手动 330实用型7座,dongfengjg","2014款 1.5 手动 350尊贵型7座,dongfengjh","2014款 1.5 手动 330舒适型7座,dongfengji",
								"2014款 1.5 手动 350豪华型7座,dongfengjj","2014款 1.5 手动 豪华型7座,dongfengjk","2014款 1.5 手动 350舒适型7座,dongfengjl",
								"2014款 1.5 手动 330实用型7座,dongfengjm","2014款 1.2 手动 330实用型7座,dongfengjn","2014款 1.5 手动 350基本型7座,dongfengjo",
								"2014款 1.5 手动 330基本型7座,dongfengjp","2014款 1.2 手动 330基本型7座,dongfengjq","2014款 1.3 手动 舒适型DK1308发动机,dongfengjr",
								"2014款 1.3 手动 经典型DK1308发动机,dongfengjs","2014款 1.3 手动 基本型DK1308发动机,dongfengjt","2014款 1.5 手动 舒适型DK1501发动机,dongfengju",
								"2014款 1.5 手动 经典型DK1501发动机,dongfengjv","2014款 1.3 手动 基本型,dongfengjw","2014款 1.3 手动 经典型,dongfengjx","2014款 1.3 手动 舒适型,dongfengjy",
								"2014款 1.5 手动 经典型,dongfengjz","2014款 1.5 手动 舒适型,dongfengjaa","2013款 1.3 手动 舒适型DK1308,dongfengjab",
								"2015款 1.5 手动 360舒适型7座,dongfengjac","2015款 1.3T 手动 360豪华型 柴油,dongfengjad","2015款 1.5 手动 舒适型,dongfengjae",
								"2015款 1.3T 手动 节能型 柴油,dongfengjaf","2015款 1.5 手动 330舒适型,dongfengjag","2015款 1.5 手动 330标准型,dongfengjah",
								"2015款 1.5 手动 330实用型,dongfengjai","2015款 1.5 手动 360豪华型7座,dongfengjaj"
								],
							
					"小康C35":["2013款 1.4 手动 基本型,dongfengka","2014款 1.5 手动 基本型,dongfengkb"
							],
							
					"小康K02":["2011款 1.0 手动 平板货箱版,dongfengla","2011款 1.0 手动 瓦楞货箱版,dongfenglb"
							],
					
					"小康K05":["2005款 1.0 手动 标准型,dongfengma","2014款 1.2 手动,dongfengmb"
							],
					
					"小康K01":["2011款 1.0 手动 标准型,dongfengna","2011款 1.1 手动 标准型,dongfengnb",
							  "2011款 1.3 手动 标准型,dongfengnc"
							],
					
					"小康V21":["2011款 1.3 手动 标准型,dongfengoa"],
					
					"小康V22":["2011款 1.0 手动 带护栏型,dongfengpa"],
					
					"小康C36":["2014款 1.4 手动 基本型7座,dongfengqa","2014款 1.5 手动 标准型7座,dongfengqb"],
					
					"小康C32":["2015款 1.2 手动 标准型DK12-05,dongfengra","2015款 1.5 手动 标准型DK15-06,dongfengrb"],
					
					"小康K07S":["2015款 1.0 手动 实用型7座AF11-05,dongfengsa","2015款 1.0 手动 实用型7座AF10-12,dongfengsb",
							   "2015款 1.2 手动 精典型7座DK12-05,dongfengsc","2015款 1.2 手动 实用型7座DK12-05,dongfengsd"
							],
					
					"景逸":["2008款 1.8 手动 舒适型,dongfengta","2008款 1.8 手动 时尚型,dongfengtb",
							"2009款 1.8T 手动 TT劲悦型,dongfengtc","2009款 1.8T 自动 TT逸动型,dongfengtd","2009款 1.8T 自动 TT劲悦型,dongfengte",
							"2009款 1.8 手动 欢动型,dongfengtf","2009款 1.8 手动 时尚型,dongfengtg","2009款 1.8 手动 舒适型,dongfength",
							"2010款 1.8 手动 舒适型,dongfengti","2010款 1.8 手动 豪华型,dongfengtj","2010款 1.8 手动 标准型,dongfengtk",
							"2011款 1.5 自动 尊享型,dongfengtl","2011款 1.5 自动 尊贵型,dongfengtm",
							"2011款 1.5 自动 舒适型,dongfengtn","2011款 1.5 自动 旗舰型,dongfengto","2011款 1.5 自动 豪华型,dongfengtp",
							"2011款 1.5 手动 尊享型,dongfengtq","2011款 1.5 手动 尊贵型,dongfengtr","2011款 1.5 手动 舒适型,dongfengts",
							"2011款 1.5 手动 旗舰型,dongfengtt","2011款 1.5 手动 豪华型,dongfengtu","2011款 1.5 手动 标准型,dongfengtv",
							"2012款 1.8 手动 豪华型,dongfengtw","2012款 1.5 自动 旗舰型,dongfengtx",
							"2012款 1.5 手动 旗舰型,dongfengty","2012款 1.5 自动 尊贵型,dongfengtz","2012款 1.5 手动 尊贵型,dongfengtaa",
							"2012款 1.5 自动 尊享型,dongfengtab","2012款 1.5 手动 尊享型,dongfengtac","2012款 1.5 自动 豪华型,dongfengtad",
							"2012款 1.5 手动 豪华型,dongfengtae","2012款 1.5 自动 舒适型,dongfengtaf","2012款 1.5 手动 舒适型,dongfengtag",
							"2012款 1.5 手动 标准型,dongfengtah","2014款 1.5 手动 XL豪华型,dongfengtai","2014款 1.5 手动 LV尊贵型,dongfengtaj",
							"2014款 1.5 手动 LV豪华型,dongfengtak",
							"2014款 1.5 手动 XL舒适型,dongfengtal","2014款 1.5 自动 XL豪华型,dongfengtam","2014款 1.5 自动 LV豪华型,dongfengtan",
							"2014款 1.8 手动 LV豪华型,dongfengtao","2015款 1.5 自动 LV六十周年庆特惠版,dongfengtap",
							"2015款 1.5 手动 LV六十周年庆特惠版,dongfengtaq","2015款 1.5 手动 XL六十周年庆特惠版,dongfengtar"
							],

					
					"风行":["2002款 2.4 自动 领先版7座	,dongfengua","2002款 2.4 手动 后驱7座,dongfengub"],
					
					"小王子":["2007款 1.0 自动 国产发动机,dongfengva"],
							
					"风神水星":["2002款 3.0 自动,dongfengwa"],

					"菱智":["2012款 2.4 手动 M5 QA长轴豪华型9座,dongfengxa","2012款 2.4 手动 M5 QA长轴豪华型7座,dongfengxb","2012款 2.4 手动 M5 QA长轴舒适型9座,dongfengxc",
							"2012款 2.4 手动 M5 QA长轴舒适型7座,dongfengxd","2012款 2.4 手动 M5 QA短轴豪华型7座,dongfengxe","2012款 2.4 手动 M5 QA短轴舒适型7座,dongfengxf",
							"2012款 2.4 手动 M5 QA短轴标准型7座,dongfengxg","2012款 2.0 手动 M5 Q3长轴舒适型9座,dongfengxh","2012款 2.0 手动 M5 Q3长轴舒适型7座,dongfengxi",
							"2012款 2.0 手动 M5 Q3长轴标准型9座,dongfengxj","2012款 2.0 手动 M5 Q3长轴标准型7座,dongfengxk","2012款 2.0 手动 M5 Q3短轴豪华型7座,dongfengxl",
							"2012款 2.0 手动 M5 Q3短轴舒适型7座,dongfengxm","2012款 2.0 手动 M5 Q3短轴标准型7座,dongfengxn","2012款 1.9T 手动 M5长轴舒适型9座 柴油,dongfengxo",
							"2012款 1.9T 手动 M5长轴舒适型7座 柴油,dongfengxp","2012款 1.9T 手动 M5长轴标准型9座 柴油,dongfengxq",
							"2012款 1.9T 手动 M5长轴标准型7座 柴油,dongfengxr","2012款 1.9T 手动 M5短轴舒适型7座 柴油,dongfengxs",
							"2012款 1.6 手动 乘用旗舰型7座,dongfengxt","2012款 1.6 手动 乘用豪华型7座,dongfengxu","2012款 1.6 手动 乘用舒适型7座,dongfengxv",
							"2012款 1.6 手动 商用标准型7座,dongfengxw","2012款 1.6 手动 商用实用型7座,dongfengxx","2012款 1.6 手动 商用基本型7座,dongfengxy",
							"2012款 1.6 手动 商用特惠型,dongfengxz","2013款 1.5 手动 V3舒适型5座,dongfengxaa","2013款 2.0 手动 M5 Q3长轴舒适型7座,dongfengxab",
							"2013款 1.9T 手动 M5长轴舒适型9座 柴油,dongfengxac",
							"2013款 1.9T 手动 M5长轴舒适型7座 柴油,dongfengxad","2013款 1.9T 手动 M5长轴豪华型9座 柴油,dongfengxae","2013款 1.9T 手动 M5长轴豪华型7座 柴油,dongfengxaf",
							"2013款 1.6 手动 M3豪华型7座,dongfengxag","2013款 1.6 手动 M3舒适型7座,dongfengxah","2013款 1.6 手动 M3标准型7座,dongfengxai",
							"2013款 1.5 手动 V3豪华型7座,dongfengxaj","2013款 1.5 手动 V3舒适型7座,dongfengxak","2013款 1.5 手动 V3标准型7座,dongfengxal",
							"2013款 1.5 手动 V3标准型5座,dongfengxam",
							"2013款 2.4 手动 M5 QA长轴豪华型9座,dongfengxan","2013款 2.4 手动 M5 QA长轴豪华型7座,dongfengxao","2013款 2.4 手动 M5 QA短轴豪华型7座,dongfengxap",
							"2013款 2.0 手动 M5 Q7长轴舒适版9座,dongfengxaq","2013款 2.0 手动 M5 Q7长轴舒适版7座,dongfengxar","2013款 2.0 手动 M5 Q7短轴舒适版7座,dongfengxas",
							"2013款 2.0 手动 M5 Q3长轴舒适型9座,dongfengxat","2013款 2.0 手动 M5 Q3长轴豪华型9座,dongfengxau","2013款 2.0 手动 M5 Q3长轴豪华型7座,dongfengxav",
							"2013款 2.0 手动 M5 Q3短轴豪华型7座,dongfengxaw","2014款 1.6 手动 M3舒适型9座,dongfengxax",
							"2014款 1.6 手动 M3豪华型9座,dongfengxay","2014款 1.6 手动 M3标准型9座,dongfengxaz","2014款 1.5 手动 V3豪华型5座,dongfengxba",
							"2014款 2.4 手动 M5 QA长轴豪华型9座,dongfengxbb","2014款 2.4 手动 M5 QA长轴豪华型7座,dongfengxbc",
							"2014款 2.4 手动 M5 QA短轴豪华型7座,dongfengxbd","2014款 2.0 手动 M5 Q3长轴豪华型9座,dongfengxbe","2014款 2.0 手动 M5 Q3长轴豪华型7座,dongfengxbf",
							"2014款 2.0 手动 M5 Q3短轴豪华型7座,dongfengxbg","2014款 2.0 手动 M5 Q3长轴舒适型9座,dongfengxbh","2014款 2.0 手动 M5 Q3长轴舒适型7座,dongfengxbi",
							"2014款 2.0 手动 M5 Q7长轴舒适型9座,dongfengxbj","2014款 2.0 手动 M5 Q7长轴舒适型7座,dongfengxbk","2014款 2.0 手动 M5 Q7短轴舒适型7座,dongfengxbl",
							"2014款 1.6 手动 M3豪华型7座,dongfengxbm","2014款 1.6 手动 M3舒适型7座,dongfengxbn","2014款 1.6 手动 M3标准型7座,dongfengxbo",
							"2014款 1.5 手动 V3豪华型7座,dongfengxbp",
							"2014款 1.5 手动 V3舒适型7座,dongfengxbq","2014款 1.5 手动 V3标准Ⅱ型7座,dongfengxbr","2014款 1.5 手动 V3标准Ⅰ型5座,dongfengxbs",
							"2015款 1.6 手动 V3舒适型7座,dongfengxbt","2015款 1.6 手动 V3舒适型9座,dongfengxbu","2015款 1.6 手动 V3标准型9座,dongfengxbv",
							"2015款 1.6 手动 V3标准型7座,dongfengxbw","2015款 1.5 手动 V3豪华型5座,dongfengxbx","2015款 2.0 手动 M5 Q3短轴豪华型7座,dongfengxby",
							"2015款 2.0 手动 M5 Q3长轴豪华型9座,dongfengxbz","2015款 2.0 手动 M5 Q3长轴豪华型7座,dongfengxca","2015款 2.0 手动 M5 Q3长轴舒适型9座,dongfengxcb",
							"2015款 2.0 手动 M5 Q3长轴舒适版7座,dongfengxcc","2015款 2.0 手动 M5 Q7长轴豪华型9座,dongfengxcd",
							"2015款 2.0 手动 M5 Q7长轴豪华型7座,dongfengxce","2015款 2.0 手动 M5 Q7长轴舒适型9座,dongfengxcf",
							"2015款 2.0 手动 M5 Q7长轴舒适型7座,dongfengxcg","2015款 1.6 手动 M3豪华型7座,dongfengxch",
							"2015款 1.6 手动 M3舒适型7座,dongfengxci","2015款 1.6 手动 M3标准型7座,dongfengxcj",
							"2015款 1.5 手动 V3豪华型7座,dongfengxck","2015款 1.5 手动 V3舒适型7座,dongfengxcl","2015款 1.5 手动 V3标准型7座,dongfengxcm",
							"2015款 1.5 手动 V3特惠型5座,dongfengxcn","2015款 1.5 手动 V3特供型5座,dongfengxco"
							],

					"菱越":["2007款 2.0 手动 实业版9座	,dongfengya","2007款 2.0 手动 标准版7座,dongfengyb",
							"2007款 2.4 自动 影院版7座,dongfengyc","2007款 2.4 自动 领先版7座,dongfengyd","2007款 2.4 手动 领先版7座,dongfengye"
							],
					
					"菱通":["2007款 2.0 手动 时尚版7座	,dongfengza","2007款 2.0 手动 进取版9座,dongfengzb",
							"2007款 2.0 手动 标准版7座,dongfengzc"
							],
					
					"风神S30":["2009款 1.6 手动 尊雅型 油电混合,dongfengaaa","2009款 1.6 自动 导航型,dongfengaab","2009款 1.6 自动 尊贵型,dongfengaac",
							"2009款 1.6 手动 尊贵型,dongfengaad","2009款 1.6 自动 尊雅型,dongfengaae","2009款 1.6 手动 尊雅型,dongfengaaf",
							"2010款 1.6 自动 双庆限量型,dongfengaag","2010款 1.6 手动 双庆限量型,dongfengaah","2011款 1.6 手动 尊雅型总冠军版,dongfengaai",
							"2011款 1.6 自动 尊雅型总冠军版,dongfengaaj","2011款 1.6 自动 尊贵型,dongfengaak","2011款 1.6 手动 尊雅型,dongfengaal",
							"2011款 1.6 手动 尊贵型,dongfengaam","2011款 1.6 自动 尊雅型,dongfengaan","2011款 1.6 自动 电子导航型,dongfengaao",
							"2012款 1.6 手动 尊贵型 油电混合,dongfengaap","2012款 1.6 手动 尊雅型 油电混合,dongfengaaq","2012款 1.6 手动 尊雅型 油气混合,dongfengaar",
							"2012款 1.6 自动 尊贵型,dongfengaas","2012款 1.6 手动 尊贵型,dongfengaat","2012款 1.6 自动 尊雅型,dongfengaau",
							"2012款 1.6 自动 电子导航型,dongfengaav","2012款 1.6 手动 尊雅型,dongfengaaw",
							"2013款 1.6 手动 尊雅型,dongfengaax","2013款 1.6 手动 尊贵型,dongfengaay","2013款 1.6 手动 进取型,dongfengaaz",
							"2013款 1.5 手动 尊贵型,dongfengaaaa","2013款 1.5 手动 尊雅型,dongfengaaab","2013款 1.5 手动 进取型,dongfengaaac",
							"2013款 1.5 自动 尊雅型,dongfengaaad","2013款 1.5 自动 尊贵型,dongfengaaae","2013款 1.6 手动 油气混合,dongfengaaaf",
							"2014款 1.5 手动 智驱感恩版,dongfengaaag","2014款 1.5 自动 超值版,dongfengaaah","2014款 1.6 手动 感恩版 油气混合,dongfengaaai",
							"2014款 1.6 手动 尊雅型 油气混合,dongfengaaaj","2014款 1.5 自动 尊贵型,dongfengaaal","2014款 1.5 自动 尊雅型,dongfengaaam",
							"2014款 1.5 手动 尊贵型,dongfengaaan","2014款 1.5 手动 尊雅型,dongfengaaao","2014款 1.5 手动 感恩版,dongfengaaap"
							],
							
					"风神H30":["2010款 1.6 自动 电子导航型,dongfengaba","2010款 1.6 自动 尊雅型,dongfengabb","2010款 1.6 自动 尊贵型,dongfengabc","2010款 1.6 手动 尊雅型,dongfengabd",
								"2010款 1.6 手动 尊贵型,dongfengabe","2011款 1.6 手动 尊雅总冠军版,dongfengabf","2011款 1.6 自动 尊雅总冠军版,dongfengabg",
								"2011款 1.6 自动 尊雅型,dongfengabh","2011款 1.6 手动 尊雅型,dongfengabi","2011款 1.6 手动 尊贵型,dongfengabj","2011款 1.6 自动 尊贵型,dongfengabk",
								"2011款 1.6 自动 电子导航型,dongfengabl","2012款 1.6 手动 尊雅型,dongfengabm","2012款 1.6 手动 尊贵型,dongfengabn","2012款 1.6 自动 电子导航型,dongfengabo",
								"2012款 1.6 自动 尊贵型,dongfengabp","2012款 1.6 自动 尊雅型,dongfengabq"
								],
								     				
					"EQ7240BP":["2009款 2.4 手动,dongfengaca"],		
							
					"猛士":["2007款 4.0T 自动 EQ2050MR3四驱 柴油,dongfengada","2007款 4.0T 自动 EQ2050ER四驱 柴油,dongfengadb"
							],		
							
					"风神H30 Cross":["2011款 1.6 手动 尊尚型,dongfengaea","2011款 1.6 自动 尊逸型,dongfengaeb","2011款 1.6 手动 尊逸型,dongfengaec",
							"2012款 1.6 手动 尊尚型,dongfengaed","2012款 1.6 自动 尊逸型,dongfengaee","2012款 1.6 手动 尊逸型,dongfengaef",
							"2013款 1.6 手动 尊逸型,dongfengaeg","2013款 1.6 手动 尊尚型,dongfengaeh","2013款 1.6 手动 进取型,dongfengaei",
							"2013款 1.5 自动 尊逸型,dongfengaej","2013款 1.5 自动 尊尚型,dongfengaek","2013款 1.5 手动 尊逸型,dongfengael",
							"2013款 1.5 手动 尊尚型,dongfengaem","2014款 1.5 自动 尊逸型,dongfengaen","2014款 1.5 自动 尊尚型,dongfengaeo",
							"2014款 1.5 手动 智驱尊逸型,dongfengaep","2014款 1.5 手动 智驱尊尚型,dongfengaeq","2014款 1.5 手动 智驱感恩版,dongfengaer",
							"2014款 1.6 手动 尊逸型,dongfengaes","2014款 1.6 手动 尊尚型,dongfengaet","2014款 1.5 手动 感恩版,dongfengaeu","2015款 1.5 自动 超值版,dongfengaev"
							],
							
					"景逸Cross":["2012款 1.5 自动 LV尊享型,dongfengafa","2012款 1.5 手动 LV尊贵型,dongfengafb"
							],
							
					"途逸":["2006款 2.2 手动 前驱	,dongfengaga","2006款 2.0 手动 前驱,dongfengagb",
							"2006款 2.8T 手动 前驱 柴油,dongfengagc"
							],
							
					"风神A60":["2012款 2.0 自动 豪华型,dongfengaha","2014款 1.6 手动 舒适型,dongfengahb",
							"2016款 1.4T 手动 尊贵型,dongfengahc"
							],
							
					"景逸SUV":["2012款 1.6 手动 舒适型前驱,dongfengaia","2012款 1.6 手动 豪华型前驱,dongfengaib",
							 "2012款 1.6 手动 尊享型前驱,dongfengaic","2012款 1.6 手动 尊贵型前驱,dongfengaid"
							],
							
							
					"俊风CV03":["2012款 1.3 手动 标准型8座,dongfengaja","2012款 1.3 手动 舒适型8座,dongfengajb",
							 "2012款 1.3 手动 精英型8座,dongfengajc","2012款 1.3 手动 豪华型8座,dongfengajd"
							],
							
					"御风":["2012款 3.0T 手动 领运版短轴中顶,dongfengaka","2012款 3.0T 手动 领尊版短轴中顶,dongfengakb",
							 "2014款 2.5T 手动 御利宝物流车标配短轴中顶ZD25,dongfengakc","2014款 3.0T 手动 领尊版短轴中顶ZD30,dongfengakd"
							],
							
					"景逸X5":["2015款 1.6 手动 豪华型,dongfengala","2015款 1.6 手动 尊享型,dongfengalb",
							 "2015款 1.6 手动 尊贵型,dongfengalc","2015款 1.8T 手动 尊享型,dongfengald","2015款 1.8T 手动 旗舰型,dongfengale"
							],	
							
					"虎视":["2012款 2.8T 手动 标准型短轴两驱 柴油,dongfengama","2012款 2.8T 手动 标准型短轴四驱 柴油,dongfengamb",
							 "2012款 2.8T 手动 标准型长轴两驱 柴油,dongfengamc","2012款 2.8T 手动 标准型长轴四驱 柴油,dongfengamd"
							],
					
					"信天游":["2013款 2.8T 手动 皮卡双排 柴油,dongfengana"],
					
					
					"景逸X3":["2014款 1.5 手动 舒适型前驱,dongfengapa","2014款 1.5 手动 豪华型前驱,dongfengapb",
							 "2014款 1.5 手动 尊享型前驱,dongfengapc","2015款 1.5 手动 舒适型前驱,dongfengapd",
							 "2015款 1.5 手动 豪华型前驱,dongfengape","2015款 1.5 手动 尊享型前驱,dongfengapf"
							],
					
					"风行CM7":["2014款 2.4 手动 行政型7座,dongfengaqa","2014款 2.4 手动 公务型7座,dongfengaqb",
							 "2014款 2.4 手动 商务型7座,dongfengaqc","2014款 2.0T 手动 豪华版7座,dongfengaqd",
							 "2014款 2.0T 手动 尊享版7座,dongfengaqe","2014款 2.0T 手动 旗舰版7座,dongfengaqf"
							],
					
					"景逸S50":["2014款 1.5 手动 豪华型,dongfengara","2014款 1.5 手动 尊享型,dongfengarb",
							 "2014款 1.5 手动 尊贵型,dongfengarc","2014款 1.5 手动 旗舰型,dongfengard",
							 "2014款 1.6 自动 豪华型,dongfengare","2014款 1.6 自动 尊享型,dongfengarf",
							 "2014款 1.6 自动 旗舰型,dongfengarg"
							],
					
					"风神A30":["2014款 1.5 手动 实尚型,dongfengasa","2014款 1.5 手动 智驱实尚型,dongfengasb",
							 "2014款 1.5 手动 智驱智尚型,dongfengasc","2014款 1.5 手动 智驱尊尚型,dongfengasd",
							 "2014款 1.5 自动 智尚型,dongfengase","2014款 1.5 自动 尊尚型,dongfengasf"
							],
					
					"风神AX7":["2015款 2.0 手动 智悦型,dongfengata","2015款 2.0 手动 智逸型,dongfengatb",
							 "2015款 2.0 自动 智悦型,dongfengatc","2015款 2.0 自动 智逸型,dongfengatd",
							 "2015款 2.3 自动 智尊型,dongfengate","2015款 2.0 手动 阅兵型,dongfengatf"
							],
					
					"风神L60":["2015款 1.6 手动 新动型,dongfengaua","2015款 1.6 手动 新享型,dongfengaub",
							 "2015款 1.8 手动 新享型,dongfengauc","2015款 1.8 自动 新享型,dongfengaud",
							 "2015款 1.8 自动 新睿型,dongfengaue"
							],
					
					"景逸XV":["2015款 1.6 自动 舒适型,dongfengava","2015款 1.6 自动 豪华型,dongfengavb"
							],
					
					"风度MX6":["2016款 1.6 手动 豪华型7座,dongfengawa","2016款 1.5 手动 尊享型7座,dongfengawb"
							],
							
					"风光360":["2015款 1.5 手动 舒适型Ⅱ 7座,dongfengaxa"
							],
					
					"风光330":["2014款 1.5 手动 标准型7座,dongfengaxa","2014款 1.5 手动 舒适型7座,dongfengaxb"
							],

//"菲斯克"

					"Karma":["2014款 2.0T 自动 标准版 油电混合,fsikeaa"
							],

					
//"福田"
				
					"风景G7":["2015款 2.0L商运版短轴平顶4Q20M,futianaa"
							],
					
					"福田风景":["2010款 2.0L快运经典型短轴版低顶491EQ4A,futianba","2010款 2.0L快运经典型短轴版低顶486EQV4,futianbb",
							"2012款 2.0L快运经典型短轴版491EQ4A,futianbc","2012款 2.0L快运经典型长轴版491EQ4A,futianbd",
							"2012款 2.0L快运经典型长轴版486EQV4,futianbe","2014款 2.0L快运标准型短轴版低顶4Y20M,futianbf",
							"2014款 2.0L快运标准型短轴版低顶4Q20M,futianbg","2014款 2.0L快运标准型短轴版低顶4Q20M1,futianbh",
							"2014款 2.0L快捷舒适型短轴版低顶486EQV4B,futianbi"
							],
			
					"蒙派克E":["2010款 2.4L财富快车标准版M240L特别版4G64,futianca","2011款 2.0L-M240L财富快车经典版486EQV4,futiancb",
							"2011款 2.4L-X240G商务舱豪华版4G64,futiancc","2011款 2.0L-M200L财富快车经典版4G63S4M,futiancd",
							"2012款 2.4L商务版舒适型 长轴4G64,futiance","2014款 2.0L财富快车经典版简配4Gi-VVT,futiancf",
							"2014款 2.0L财富快车舒适版简配4G63,futiancg","2014款 2.4L财富快车舒适版简配4G69,futianch",
							"2014款 2.0L商务舱标准版4G64,futianci"
							],
					
					"蒙派克S":["2014款 2.4L新干线标准型4G69,futianda"
							],
					
					"迷迪":["2009款 宜家 1.6L 标准型,futianea","2009款 宜商 1.6L 长车身标准型,futianeb","2011款 宜家 1.5L 升级版,futianec"
							],
							
//"菲亚特"
		
					"菲翔":["2012款 1.4T 手动畅享版,fyateaa","2012款 1.4T 自动畅享版,fyateab","2012款 1.4T 自动劲享版,fyateac",
						"2012款 1.4T 自动尊享版,fyatead","2013款 1.4T 周年纪念版,fyateae","2015款 1.4T 自动跃享版,fyateaf",
						"2015款 1.4T 自动智享版,fyateag","2015款 1.4T 自动劲享版,fyateah"
						],
						
					"致悦":["2014款 1.4T 自动舒适版,fyateba"
							],
					
					"派朗":["2006款 1.7L 加速版,fyateca"
							],
					
					
					"派力奥":["2004款 1.5L HL,fyateda","2004款 1.5L HL SUNROOF,fyatedb","2004款 1.3L Speedgear EL,fyatedc","2004款 1.5L MT,fyatedd",
							"2004款 1.3L EX,fyatede","2005款 1.5L FX,fyatedf","2005款 1.5L FX(CD),fyatedg","2005款 1.5L FSX,fyatedh",
							"2006款 1.5L GX,fyatedi","2006款 1.5L GX(CD),fyatedj","2006款 1.5L GSX,fyatedk","2006款 1.5L GL,fyatedl",
							"2007款 1.5L 厚道版,fyatedm","2007款 1.5L 厚恩版,fyatedn"
							],
					
					"西耶那":["2004款 1.5L HL,fyateea","2005款 1.5L FLX,fyateeb","2006款 1.5L GL,fyateec",
							  "2006款 1.5L GLX,fyateed","2006款 1.5L GSX,fyateee","2015款 1.4T 自动跃享版,fyateef"
						    ],
					
					
					"周末风":["2004款 1.3L Speedgear HLX,fyatefa","2004款 1.5L HL,fyatefb","2005款 1.5L FSX,fyatefc"
						    ],
					
					"菲跃":["2012款 2.4L 豪华版,fyatega","2012款 2.4L 舒适版,fyategb","2013款 2.4L 豪华导航版,fyategc","2013款 3.6L 智能全驱版,fyategd"
						    ],
					
					"博悦":["2011款 1.4T 自动运动版,fyateha"
						    ],
					
//"丰田"
					"致炫":["2014款 1.5 自动 GS锐动版,fengtianaa","2014款 1.5 自动 G炫动版,fengtianab","2014款 1.5 手动 G炫动版,fengtianac","2014款 1.3 自动 E魅动版,fengtianad",
							"2014款 1.3 手动 E魅动版,fengtianae","2014款 1.3 手动 灵动版,fengtianaf","2015款 1.5 手动 橙色限量版,fengtianag","2015款 1.5 自动 橙色限量版,fengtianah",
							"2015款 1.5 自动 GS锐动特别版,fengtianai","2015款 1.5 自动 G炫动天窗特别版,fengtianaj","2015款 1.5 手动 G炫动天窗特别版,fengtianak",
							"2015款 1.5 自动 E魅动版,fengtianal"
							],	    

					"雷凌":["2014款 1.8 自动 V豪华版,fengtianba","2014款 1.8 自动 GSL领先版,fengtianbb","2014款 1.8 自动 GS精英版,fengtianbc",
							"2014款 1.6 自动 GL领先版,fengtianbd","2014款 1.6 自动 G精英版,fengtianbe","2014款 1.6 手动 GL领先版,fengtianbf",
							"2014款 1.6 手动 G精英版,fengtianbg","2014款 1.6 手动 E新锐版,fengtianbh","2015款 1.6 自动 E新锐版,fengtianbi",
							"2015款 1.8 自动 GS橙色限量版,fengtianbj","2015款 1.6 自动 G橙色限量版,fengtianbk",               
							"2016款 1.8 自动 H V豪华版双擎 混合动力,fengtianbl","2016款 1.8 自动 H GSL领先版双擎 混合动力,fengtianbm",
							"2016款 1.8 自动 H GS精英天窗版双擎 混合动力,fengtianbn","2016款 1.8 自动 H GS精英版双擎 混合动力,fengtianbo"
							],
												
					"凯美瑞":["2006款 2.4 自动 240V至尊型,fengtianca","2006款 2.0 自动 200G豪华导航型,fengtiancb","2006款 2.4 自动 240G豪华导航型,fengtiancc",
							"2006款 2.4 自动 240G豪华型,fengtiancd","2006款 2.4 自动 240V至尊导航型,fengtiance","2006款 2.0 自动 200G豪华型,fengtiancf",
							"2006款 2.0 自动 200E精英型,fengtiancg","2008款 2.0 自动 200E精英型,fengtianch","2008款 2.0 自动 200G豪华型,fengtianci","2008款 2.0 自动 200G豪华导航型,fengtiancj",
							"2008款 2.4 自动 240V至尊型,fengtianck","2008款 2.4 自动 240G豪华型,fengtiancl","2008款 2.4 自动 240V至尊导航型,fengtiancm",
							"2008款 2.4 自动 240G豪华导航型,fengtiancn","2008款 2.0 自动 200E精英真皮型,fengtianco",                  
							"2009款 2.0 自动 200E运动型,fengtiancp","2009款 2.4 自动 240V导航型,fengtiancq","2009款 2.4 自动 240G豪华导航型,fengtiancr",
							"2009款 2.4 自动 240G豪华型,fengtiancs","2009款 2.0 自动 200G豪华导航型,fengtianct","2009款 2.0 自动 200G豪华型,fengtiancu",
							"2009款 2.0 自动 200E精英真皮型,fengtiancv","2009款 2.4 自动 240V至尊导航型,fengtiancw","2009款 2.4 自动 240V至尊型,fengtiancx",
							"2009款 2.0 自动 200E精英型,fengtiancy","2009款 2.4 自动 240G豪华升级导航型,fengtiancz","2009款 2.0 自动 200G豪华升级导航型,fengtiancaa",
							"2009款 2.4 自动 240G豪华升级型,fengtiancab","2009款 2.0 自动 200G豪华升级型,fengtiancac","2009款 2.4 自动 240E精英型,fengtiancad",
							"2010款 2.4 自动 240HG豪华型 油电混合,fengtiancae","2010款 2.4 自动 240HV至尊导航型 油电混合,fengtiancaf",
							"2010款 2.4 自动 240HV智能领航型G-Book 油电混合,fengtiancag","2010款 2.4 自动 240V智能领航型G-Book,fengtiancah",
							"2010款 2.4 自动 240V至尊导航型,fengtiancai","2010款 2.4 自动 240G智能领航型G-Book,fengtiancaj","2010款 2.4 自动 240V至尊型,fengtiancak",
							"2010款 2.4 自动 240G豪华导航型,fengtiancal","2010款 2.4 自动 240G豪华型,fengtiancam","2010款 2.4 自动 240G经典型,fengtiancan",
							"2010款 2.0 自动 200G智能领航型G-Book,fengtiancao","2010款 2.0 自动 200G豪华导航型,fengtiancap","2010款 2.0 自动 200G经典型,fengtiancaq",
							"2010款 2.0 自动 200E精英型,fengtiancar","2010款 2.0 自动 200E运动真皮型,fengtiancas","2010款 2.0 自动 200G豪华型,fengtiancat",
							"2010款 2.4 自动 240G纪念版豪华导航型,fengtiancau","2010款 2.4 自动 240G纪念豪华型,fengtiancav",
							"2011款 2.0 自动 200E精英天窗型,fengtiancaw","2011款 2.4 自动 240G经典型周年纪念版,fengtiancax",
							"2011款 2.4 自动 240G豪华型周年纪念版,fengtiancay","2011款 2.0 自动 200E精英型天窗版,fengtiancaz",
							"2011款 2.0 自动 200G经典型周年纪念版,fengtiancba","2011款 2.0 自动 200G豪华型周年纪念版,fengtiancbb",
							"2012款 2.0 自动 200G经典豪华版,fengtiancbc","2012款 2.5 自动 旗舰版HQ 油电混合,fengtiancbd","2012款 2.5 自动 至尊版HV 油电混合,fengtiancbe",
							"2012款 2.5 自动 豪华版HG 油电混合,fengtiancbf","2012款 2.0 自动 200E精英型,fengtiancbg","2012款 2.0 自动 200G经典型,fengtiancbh",
							"2012款 2.5 自动 骏瑞凌动版,fengtiancbi","2012款 2.5 自动 骏瑞凌动导航版,fengtiancbj","2012款 2.5 自动 豪华版,fengtiancbk",
							"2012款 2.5 自动 至尊导航版,fengtiancbl","2012款 2.5 自动 豪华导航版,fengtiancbm","2012款 2.5 自动 G-Book旗舰版,fengtiancbn",
							"2012款 2.0 自动 骏瑞耀动版,fengtiancbo","2012款 2.0 自动 豪华导航版,fengtiancbp","2012款 2.0 自动 豪华版,fengtiancbq",
							"2013款 2.0 自动 200E经典精英版,fengtiancbr","2013款 2.0 自动 200G经典豪华版,fengtiancbs","2013款 2.5 自动 增值版舒适版,fengtiancbt",
							"2013款 2.0 自动 增值版舒适版,fengtiancbu","2013款 2.0 自动 增值版精英版,fengtiancbv",
							"2013款 2.0 自动 G舒适版,fengtiancbw","2013款 2.0 自动 E精英版,fengtiancbx","2013款 2.5 自动 G舒适版,fengtiancby",
							"2015款 2.5 自动 HQ旗舰版 油电混合,fengtiancbz","2015款 2.5 自动 HG豪华导航版 油电混合,fengtiancca","2015款 2.5 自动 Q旗舰版,fengtianccb",
							"2015款 2.5 自动 G豪华导航版,fengtianccc","2015款 2.5 自动 骏瑞 S凌动导航版,fengtianccd","2015款 2.0 自动 G豪华版,fengtiancce",
							"2015款 2.0 自动 骏瑞 S凌动版,fengtianccf","2015款 2.0 自动 G领先版,fengtianccg","2015款 2.0 自动 E精英版,fengtiancch"
							],
       
					"汉兰达":["2009款 3.5 自动 至尊版7座四驱,fengtianda","2009款 3.5 自动 精英版7座四驱,fengtiandb","2009款 3.5 自动 豪华导航版7座四驱,fengtiandc",
							"2009款 3.5 自动 豪华版7座四驱,fengtiandd","2009款 2.7 自动 至尊版7座前驱,fengtiande","2009款 2.7 自动 运动版5座前驱,fengtiandf",
							"2009款 2.7 自动 精英版7座前驱,fengtiandg","2009款 2.7 自动 精英版5座前驱,fengtiandh","2009款 2.7 自动 豪华导航版7座前驱,fengtiandi",
							"2009款 2.7 自动 豪华版7座前驱,fengtiandj","2011款 2.7 自动 运动版5座前驱,fengtiandk","2011款 2.7 自动 豪华导航版7座前驱,fengtiandl",
							"2011款 2.7 自动 豪华版7座前驱,fengtiandm","2011款 2.7 自动 至尊版7座前驱,fengtiandn","2011款 2.7 自动 精英版7座前驱,fengtiando",
							"2011款 2.7 自动 精英版5座前驱,fengtiandp","2012款 2.7 自动 精英版7座前驱,fengtiandq","2012款 2.7 自动 运动版5座前驱,fengtiandr",
							"2012款 2.7 自动 豪华版7座前驱,fengtiands","2012款 2.7 自动 至尊版7座前驱,fengtiandt","2012款 2.7 自动 精英版5座前驱,fengtiandu",
							"2012款 3.5 自动 至尊版7座四驱,fengtiandv","2012款 3.5 自动 豪华导航版7座四驱,fengtiandw","2012款 3.5 自动 豪华版7座四驱,fengtiandx",
							"2012款 3.5 自动 精英版7座四驱,fengtiandy","2012款 2.7 自动 运动版7座前驱,fengtiandz","2012款 2.7 自动 豪华导航版7座前驱,fengtiandaa",
							"2013款 2.7 自动 探索版7座前驱,fengtiandab","2013款 2.7 自动 紫金版7座前驱,fengtiandac","2013款 2.7 自动 紫金版5座前驱,fengtiandad",
							"2015款 3.5 自动 至尊版7座四驱,fengtiandae","2015款 3.5 自动 豪华导航版7座四驱,fengtiandaf","2015款 3.5 自动 豪华版7座四驱,fengtiandag",
							"2015款 3.5 自动 精英版7座四驱,fengtiandah","2015款 2.0T 自动 至尊版7座四驱,fengtiandai","2015款 2.0T 自动 豪华导航版7座四驱,fengtiandaj",
							"2015款 2.0T 自动 豪华导航版7座前驱,fengtiandak","2015款 2.0T 自动 豪华版7座四驱,fengtiandal","2015款 2.0T 自动 豪华版7座前驱,fengtiandam",
							"2015款 2.0T 自动 精英版7座四驱,fengtiandan","2015款 2.0T 自动 精英版7座前驱,fengtiandao","2015款 2.0T 自动 精英版5座前驱,fengtiandap"
							],
					
					
					"逸致":["2011款 1.8 自动 G舒适版5座,fengtianea","2011款 1.8 自动 G豪华多功能版7座,fengtianeb","2011款 1.8 自动 G豪华版5座,fengtianec",
							"2011款 1.8 自动 E精英版5座,fengtianed","2011款 2.0 自动 V至尊导航版7座,fengtianee","2011款 1.6 手动 E精英版5座,fengtianef",
							"2012款 1.8 自动 V精英多功能版7座,fengtianeg","2012款 1.8 自动 V至尊多功能版7座,fengtianeh","2012款 1.8 自动 V至尊版5座,fengtianei",
							"2012款 1.8 自动 G舒适多功能版7座,fengtianej","2012款 1.8 手动 E精英版5座,fengtianek",                  
							"2014款 1.8 自动 G星耀豪华多功能版7座,fengtianel","2014款 1.8 自动 G星耀豪华版,fengtianem","2014款 1.8 自动 G星耀舒适多功能版7座,fengtianen",
							"2014款 1.8 自动 G星耀舒适版,fengtianeo","2014款 1.8 自动 E星耀精英多功能版,fengtianep","2014款 1.8 自动 E星耀精英版,fengtianeq",
							"2014款 1.8 手动 E星耀精英版,fengtianer","2014款 1.6 手动 E星耀精英版,fengtianes","2014款 1.8 自动 E星耀精英多功能版7座,fengtianet",
							"2015款 1.8 自动 E跨界版,fengtianeu"
							], 
							
					"雅力士":["2008款 1.6 手动 G精致型,fengtianfa","2008款 1.6 手动 RS至尊锐动型,fengtianfb","2008款 1.6 手动 G精致型,fengtianfc",
							"2008款 1.6 自动 RS至尊锐动型,fengtianfd","2008款 1.6 自动 G精致型,fengtianfe","2008款 1.6 自动 GS精致智能型,fengtianff",
							"2008款 1.6 手动 RS至尊型,fengtianfg","2008款 1.6 自动 RS至尊型,fengtianfh","2008款 1.3 自动 E舒适型,fengtianfi",
							"2008款 1.3 手动 E舒适型,fengtianfj","2009款 1.6 自动 E舒适型,fengtianfk","2009款 1.6 手动 E舒适型,fengtianfl",                                
							"2011款 1.6 自动 GS锐动版,fengtianfm","2011款 1.6 手动 G炫动版,fengtianfn","2011款 1.6 自动 G炫动版,fengtianfo","2011款 1.6 手动 E魅动版,fengtianfp",
							"2011款 1.6 自动 E魅动版,fengtianfq","2011款 1.3 手动 E魅动版,fengtianfr","2011款 1.3 自动 E魅动版,fengtianfs"
							],
					
					
					
					"威驰":["2005款 1.5 手动 2周年特装版GL,fengtianga","2005款 1.3 手动 3周年纪念版GL,fengtiangb","2005款 1.3 自动 3周年纪念版GL,fengtiangc",
							"2005款 1.5 自动 2周年特装版GL,fengtiangd","2006款 1.3 自动 GL,fengtiange","2006款 1.5 自动 GLX-i电子导航版,fengtiangf",
							"2006款 1.5 自动 GL-s ,fengtiangg","2006款 1.3 手动 GL特别版,fengtiangh",
							"2006款 1.3 自动 GL特别版,fengtiangi","2006款 1.5 自动 GLX-i ,fengtiangj","2006款 1.5 自动 GL-s运动版,fengtiangk","2006款 1.5 手动 GL-i,fengtiangl",
							"2007款 1.3 自动 GL-i,fengtiangm","2007款 1.3 手动 GL-i,fengtiangn","2007款 1.6 自动 GL-i,fengtiango","2007款 1.6 自动 GL-s炫酷运动版,fengtiangp",
							"2007款 1.6 自动 GLX-i,fengtiangq","2007款 1.6 手动 GL-i,fengtiangr","2006款 1.5 自动 GL-i,fengtiangs","2006款 1.3 手动 GL,fengtiangt",
							"2008款 1.6 自动 GLX-i炫酷天窗版,fengtiangu","2008款 1.3 手动 GL-i特别纪念版,fengtiangv","2008款 1.3 自动 GL-i特别纪念版,fengtiangw",
							"2008款 1.6 手动 GL-i特别纪念版,fengtiangx","2008款 1.6 自动 GL-i特别纪念版,fengtiangy","2008款 1.6 自动 GL-s炫酷运动版,fengtiangz",
							"2008款 1.3 自动 GL-i标准版,fengtiangaa","2008款 1.6 自动 GL-i标准版,fengtiangab","2008款 1.3 手动 GL-i标准版,fengtiangac",
							"2008款 1.6 手动 GL-i标准版,fengtiangad","2008款 1.6 自动 GLX-i天窗版,fengtiangae",
							"2009款 1.6 自动 GLX-i,fengtiangaf","2009款 1.6 手动 GL-i特别限量版,fengtiangag","2009款 1.6 自动 GL-i特别限量版,fengtiangah",
							"2010款 1.6 自动 GLX-i天窗型,fengtiangai","2010款 1.6 自动 GLX-i天窗型,fengtiangaj","2010款 1.6 自动 GL-s运动型,fengtiangak",
							"2010款 1.3 手动 GL-i标准版,fengtiangal","2010款 1.3 自动 GL-i标准版,fengtiangam",
							"2013款 1.6 手动 GL-i型尚天窗版,fengtiangan","2013款 1.6 自动 GLX-i型尚智领版,fengtiangao","2013款 1.6 自动 GL-i型尚天窗版,fengtiangap",
							"2013款 1.3 手动 GL-i型尚版,fengtiangaq","2013款 1.3 手动 GL-i标准版,fengtiangar","2013款 1.3 自动 GL-i型尚版,fengtiangas",
							"2013款 1.3 自动 GL-i标准版,fengtiangat","2011款 1.6 手动 GL-i天窗版,fengtiangau","2011款 1.6 自动 GL-i天窗版,fengtiangav",
							"2014款 1.5 手动 智臻版,fengtiangaw","2014款 1.3 手动 型尚版,fengtiangax","2014款 1.3 手动 超值版,fengtiangay",
							"2014款 1.3 自动 型尚版,fengtiangaz","2014款 1.5 自动 智尊版,fengtiangba","2014款 1.5 自动 智享版,fengtiangbb",
							"2014款 1.5 自动 智尚版,fengtiangbc","2014款 1.5 自动 智臻版,fengtiangbd","2014款 1.5 手动 智享版,fengtiangbe",
							"2014款 1.5 手动 智尚版,fengtiangbf","2014款 1.3 自动 智尚版,fengtiangbg",
							"2015款 1.5 自动 智享星光版,fengtiangbh","2015款 1.5 手动 智享星光版,fengtiangbi",
							"2016款 1.5 自动 智臻星耀版,fengtiangbj","2016款 1.5 手动 智臻星耀版,fengtiangbk","2015款 1.5 自动 智尊星光版,fengtiangbl"
							],
					
					"花冠":["2005款 1.8 手动 GL-I,fengtianha","2005款 1.8 自动 GLX-I周年纪念版,fengtianhb","2005款 1.8 手动 GLX-I周年纪念版,fengtianhc",
							"2006款 1.8 自动 DLX,fengtianhd","2006款 1.8 自动 GLX-S运动版,fengtianhe","2006款 1.8 自动 GLX-I电子导航版,fengtianhf",
							"2006款 1.8 手动 GLX-I,fengtianhg","2006款 1.8 自动 GLX-I,fengtianhh","2006款 1.8 手动 GL-I,fengtianhi","2006款 1.8 自动 GL-I,fengtianhj",
							"2007款 1.6 手动 L,fengtianhk","2007款 1.6 自动 G,fengtianhl","2007款 1.6 手动 G,fengtianhm",
							"2009款 1.6 手动 豪华型,fengtianhn","2009款 1.6 自动 豪华型,fengtianho","2009款 1.6 手动 特惠版G,fengtianhp","2009款 1.6 自动 特别版G,fengtianhq",
							"2009款 1.6 手动 特别版G,fengtianhr","2010款 1.6 手动 特惠型,fengtianhs","2010款 1.6 手动 经典型,fengtianht","2010款 1.6 自动 经典型,fengtianhu",
							"2011款 1.6 自动 经典型,fengtianhv","2011款 1.6 手动 特惠型,fengtianhw","2011款 1.6 手动 经典型,fengtianhx","2011款 1.6 手动 豪华型,fengtianhy",
							"2011款 1.6 自动 豪华型,fengtianhz","2013款 1.6 自动 豪华型,fengtianhaa","2013款 1.6 手动 豪华型,fengtianhab","2013款 1.6 自动 卓越型,fengtianhac",
							"2013款 1.6 手动 卓越型,fengtianhad","2013款 1.6 手动 超值版,fengtianhae"
							],
					
					"卡罗拉":["2007款 1.8 手动 GLi,fengtiania","2007款 1.8 自动 Premium Navi,fengtianib","2007款 1.8 自动 Premium,fengtianic",
							"2007款 1.8 手动 GLXi,fengtianid","2007款 1.8 自动 GLXi导航版,fengtianie","2007款 1.8 自动 GLXi,fengtianif",
							"2007款 1.8 自动 GLi,fengtianig","2007款 1.6 手动 GL,fengtianih","2007款 1.6 自动 GL,fengtianii",
							"2008款 1.8 手动 GLXi,fengtianij","2008款 1.8 手动 GLi天窗特别版,fengtianik","2008款 1.8 自动 GLi天窗特别版,fengtianil",
							"2008款 1.8 手动 GLXS,fengtianim","2008款 1.8 自动 GLXS,fengtianin","2008款 1.8 手动 GLS,fengtianio","2008款 1.8 自动 GLS,fengtianip",
							"2009款 1.8 自动 GLXS,fengtianiq","2009款 1.8 自动 GLXi特别纪念版,fengtianir","2009款 1.8 手动 GLXi特别纪念版,fengtianis",
							"2010款 1.6 手动 GL天窗特别版,fengtianit","2010款 1.6 自动 GL天窗特别版,fengtianiu",
							"2011款 1.8 自动 GLXi,fengtianiv","2011款 1.8 手动 GLi纪念版,fengtianiw","2011款 1.8 自动 GLi纪念版,fengtianix","2011款 1.6 手动 GL纪念版,fengtianiy",
							"2011款 1.6 自动 GL纪念版,fengtianiz","2011款 2.0 手动 GLX,fengtianiaa","2011款 2.0 自动 GLX导航版,fengtianiab","2011款 2.0 自动 GLX,fengtianiac",
							"2011款 1.8 手动 GLXS,fengtianiad","2011款 1.8 手动 GLXi,fengtianiae","2011款 1.8 手动 GLS,fengtianiaf","2011款 1.8 手动 GLi,fengtianiag",
							"2011款 1.8 自动 GLXS导航版,fengtianiah","2011款 1.8 自动 GLXS,fengtianiai","2011款 1.8 自动 GLXi导航版,fengtianiaj","2011款 1.8 自动 GLS,fengtianiak",
							"2011款 1.8 自动 GLi,fengtianial","2011款 1.6 手动 GL天窗版,fengtianiam","2011款 1.6 手动 GL,fengtianian","2011款 1.6 自动 GL天窗版,fengtianiao",
							"2011款 1.6 自动 GL,fengtianiap","2012款 1.8 手动 GLi炫装版,fengtianiaq","2012款 1.8 自动 GLi炫装版,fengtianiar","2012款 1.6 手动 GL炫装版,fengtianias","2012款 1.6 自动 GL炫装版,fengtianiat",
							"2013款 1.8 手动 GLXi,fengtianiau","2013款 1.8 自动 GLXi导航版,fengtianiav","2013款 1.8 自动 GLXi,fengtianiaw","2013款 1.8 手动 GLi至酷特装版,fengtianiax",
							"2013款 1.8 自动 GLi至酷特装版,fengtianiay","2013款 1.6 手动 GL至酷特装版,fengtianiaz","2013款 1.6 手动 GL炫酷特装版,fengtianiba",
							"2013款 1.6 自动 GL至酷特装版,fengtianibb","2013款 1.6 自动 GL炫酷特装版,fengtianibc","2013款 1.8 手动 GLi至酷版,fengtianibd",
							"2013款 1.6 自动 GL至酷版,fengtianibe","2013款 1.6 自动 GL炫酷版,fengtianibf","2013款 1.8 自动 GLi至酷版,fengtianibg",
							"2013款 1.6 手动 GL至酷版,fengtianibh","2013款 1.6 手动 GL炫酷版,fengtianibi",
							"2014款 1.8 自动 至高版,fengtianibj","2014款 1.8 自动 GLXi,fengtianibk","2014款 1.8 手动 GLXi,fengtianibl","2014款 1.6 自动 GLXi导航版,fengtianibm",
							"2014款 1.6 自动 GLXi,fengtianibn","2014款 1.6 手动 GLXi,fengtianibo","2014款 1.6 自动 GLi真皮版,fengtianibp","2014款 1.6 手动 GLi真皮版,fengtianibq",
							"2014款 1.6 自动 GLi,fengtianibr","2014款 1.6 手动 GLi,fengtianibs","2014款 1.6 自动 GL,fengtianibt","2014款 1.6 手动 GL,fengtianibu",
							"2016款 1.8 自动 旗舰版双擎 油电混合,fengtianibv","2016款 1.8 自动 豪华版双擎 油电混合,fengtianibw","2016款 1.8 自动 精英版双擎 油电混合,fengtianibx",
							"2016款 1.8 自动 先锋版双擎 油电混合,fengtianiby","2016款 1.6 自动 GLi炫酷版,fengtianibz","2016款 1.6 手动 GLi炫酷版,fengtianica"
							],
					
					"普锐斯":["2005款 1.5 自动 真皮导航版 油电混合,fengtianja","2006款 1.5 自动 真皮导航版 油电混合,fengtianjb","2006款 1.5 自动 真皮座椅版 油电混合,fengtianjc",
							"2006款 1.5 自动 织物座椅版 油电混合,fengtianjd","2011款 1.8 自动 豪华先进型 油电混合,fengtianje",
							"2012款 1.8 自动 豪华型 油电混合,fengtianjf","2012款 1.8 自动 标准型 油电混合,fengtianjg","2012款 1.8 自动 豪华先进型 油电混合,fengtianjh"
							],

					
					
					"锐志":["2006款 3.0 自动 V Premium导航版,fengtianka","2006款 3.0 自动 V Premium,fengtiankb","2006款 3.0 自动 V Premium天窗导航版,fengtiankc",
							"2006款 2.5 自动 V电子导航版,fengtiankd","2006款 2.5 自动 V天窗版,fengtianke","2006款 2.5 自动 V,fengtiankf",
							"2006款 2.5 自动 S特别版,fengtiankg","2006款 2.5 自动 S,fengtiankh",
							"2007款 2.5 自动 V,fengtianki","2007款 2.5 自动 S,fengtiankj","2007款 2.5 自动 V天窗导航版,fengtiankk","2007款 2.5 自动 V天窗版,fengtiankl",
							"2007款 3.0 自动 V超级运动版,fengtiankm","2007款 2.5 自动 V超级运动版,fengtiankn","2007款 2.5 自动 S真皮天窗版,fengtianko",     
							"2008款 2.5 自动 S特别导航版,fengtiankp","2008款 2.5 自动 S特别纪念版,fengtiankq",
							"2009款 2.5 自动 V,fengtiankr","2009款 2.5 自动 S AVX版,fengtianks","2009款 2.5 自动 S舒适版,fengtiankt",
							"2010款 3.0 自动 风尚旗舰导航版,fengtianku","2010款 3.0 自动 风尚旗舰版,fengtiankv","2010款 2.5 自动 V风尚菁英版,fengtiankw",
							"2010款 2.5 自动 V风尚豪华导航版,fengtiankx","2010款 2.5 自动 V风度菁英版,fengtianky","2010款 2.5 自动 S风尚菁华版,fengtiankz",
							"2010款 2.5 自动 S风度菁华版,fengtiankaa","2012款 2.5 自动 V风度菁英炫装版,fengtiankab",
							"2013款 3.0 自动 V尊锐导航版,fengtiankac","2013款 2.5 自动 V尊锐导航版,fengtiankad","2013款 2.5 自动 V尊锐版,fengtiankae",
							"2013款 2.5 自动 S菁锐版,fengtiankaf","2013款 2.5 自动 V尚锐导航版,fengtiankag","2013款 2.5 自动 V尚锐版,fengtiankah",
							"2013款 2.5 自动 V菁锐版,fengtiankai"
							],
					
					"皇冠":["2005款 3.0 自动 Royal Saloon导航版,fengtianla","2005款 3.0 自动 Royal Saloon G VIP,fengtianlb","2005款 3.0 自动 Royal真皮版,fengtianlc",
							"2005款 3.0 自动 Royal,fengtianld","2005款 3.0 自动 Royal E真皮版,fengtianle","2005款 3.0 自动 Royal E,fengtianlf",
							"2005款 3.0 自动 Royal Saloon,fengtianlg","2005款 3.0 自动 Royal Saloon G,fengtianlh",
							"2006款 2.5 自动 Royal真皮版,fengtianli","2006款 3.0 自动 Royal Saloon导航版,fengtianlj","2006款 3.0 自动 Royal真皮版,fengtianlk",
							"2006款 3.0 自动 Royal Saloon特别版,fengtianll","2006款 2.5 自动 行政版,fengtianlm","2006款 2.5 自动 Royal,fengtianln",
							"2007款 3.0 自动 Royal Saloon时尚导航版,fengtianlo","2007款 3.0 自动 Royal Saloon G VIP,fengtianlp","2007款 3.0 自动 Royal Saloon标准版,fengtianlq",
							"2007款 2.5 自动 Royal真皮版,fengtianlr","2007款 2.5 自动 Royal真皮特别版,fengtianls","2008款 2.5 自动 Royal,fengtianlt",
							"2009款 2.5 自动 行政版,fengtianlu","2009款 2.5 自动 Royal,fengtianlv","2009款 2.5 自动 Royal真皮版,fengtianlw","2009款 3.0 自动 Royal Saloon G,fengtianlx",
							"2009款 3.0 自动 Royal Saloon导航强化版,fengtianly","2009款 2.5 自动 Royal特别导航版,fengtianlz","2009款 2.5 自动 Royal特别强化版,fengtianlaa","2009款 2.5 自动 Royal特别纪念版,fengtianlab",
							"2010款 2.5 自动 Royal真皮导航版,fengtianlac","2010款 4.3 自动 Royal Saloon VIP,fengtianlad","2010款 4.3 自动 Royal Saloon,fengtianlae",
							"2010款 3.0 自动 Royal真皮版,fengtianlaf","2010款 3.0 自动 Royal Saloon尊享版,fengtianlag","2010款 3.0 自动 Royal Saloon VIP,fengtianlah",
							"2010款 3.0 自动 Royal Saloon,fengtianlai","2010款 2.5 自动 Royal真皮天窗版,fengtianlaj","2010款 2.5 自动 Royal天窗导航版,fengtianlak",
							"2010款 2.5 自动 Royal,fengtianlal","2011款 2.5 自动 Royal真皮天窗特别版,fengtianlam",
							"2012款 2.5 自动 Royal真皮版,fengtianlan","2012款 2.5 自动 Royal,fengtianlao","2012款 2.5 自动 Royal导航版,fengtianlap",
							"2012款 2.5 自动 Royal,fengtianlaq","2012款 2.5 自动 Royal Saloon,fengtianlar","2012款 3.0 自动 Royal Saloon VIP,fengtianlas",
							"2012款 3.0 自动 Royal Saloon,fengtianlat","2012款 2.5 自动 Royal Saloon尊贵版,fengtianlau","2012款 2.5 自动 Royal Saloon,fengtianlav",
							"2012款 2.5 自动 Royal舒适版,fengtianlaw",
							"2015款 2.0T 自动 尊享版,fengtianlax","2015款 2.0T 自动 豪华版,fengtianlay","2015款 2.0T 自动 精英版,fengtianlaz","2015款 2.0T 自动 时尚版,fengtianlba",
							"2015款 2.0T 自动 先锋版,fengtianlbb","2015款 2.5 自动 尊享版,fengtianlbc","2015款 2.5 自动 豪华版,fengtianlbd","2015款 2.5 自动 精英版,fengtianlbe",
							"2015款 2.5 自动 时尚版,fengtianlbf","2015款 2.5 自动 标准版,fengtianlbg"
							],
					
					"一汽丰田RAV4":["2009款 2.4 手动 豪华导航型四驱,fengtianma","2009款 2.4 自动 豪华导航型四驱,fengtianmb","2009款 2.0 自动 豪华型前驱,fengtianmc",
							"2009款 2.0 自动 豪华型政前,fengtianmd","2009款 2.4 自动 豪华型四驱,fengtianme","2009款 2.4 手动 豪华型四驱,fengtianmf","2009款 2.0 自动 豪华导航型前驱,fengtianmg",
							"2009款 2.0 手动 豪华导航型前驱,fengtianmh","2009款 2.0 手动 豪华型前驱,fengtianmi","2009款 2.0 自动 经典型前驱,fengtianmj","2009款 2.0 手动 经典型前驱,fengtianmk",
							"2010款 2.4 手动 豪华升级型四驱,fengtianml","2010款 2.4 自动 豪华升级型四驱,fengtianmm","2010款 2.0 手动 豪华升级型前驱,fengtianmn","2010款 2.0 自动 豪华升级型前驱,fengtianmo",
							"2011款 2.4 手动 至臻导航版四驱,fengtianmp","2011款 2.4 手动 至臻版四驱,fengtianmq","2011款 2.4 手动 豪华型四驱,fengtianmr",
							"2011款 2.4 自动 至臻导航版四驱,fengtianms","2011款 2.0 手动 豪华型前驱,fengtianmt","2011款 2.0 自动 豪华型前驱,fengtianmu","2011款 2.0 手动 经典型前驱,fengtianmv",
							"2011款 2.0 自动 经典型前驱,fengtianmw","2011款 2.4 自动 至臻版四驱,fengtianmx","2011款 2.4 自动 豪华型四驱,fengtianmy","2011款 2.4 自动 豪华升级型四驱,fengtianmz",
							"2012款 2.4 手动 至臻导航版四驱,fengtianmaa","2012款 2.4 手动 至臻版四驱,fengtianmab","2012款 2.4 手动 豪华型四驱,fengtianmac",
							"2012款 2.4 自动 至臻导航版四驱,fengtianmad","2012款 2.0 手动 经典型前驱,fengtianmae","2012款 2.0 手动 豪华型前驱,fengtianmaf","2012款 2.0 自动 经典型前驱,fengtianmag",
							"2012款 2.0 自动 豪华型前驱,fengtianmah","2012款 2.0 手动 炫装版四驱,fengtianmai","2012款 2.0 自动 炫装版四驱,fengtianmaj","2012款 2.4 手动 豪华型炫装版四驱,fengtianmak",
							"2012款 2.4 自动 豪华型炫装版四驱,fengtianmal","2012款 2.0 自动 豪华型,fengtianmam","2013款 2.0 自动 风尚版前驱,fengtianman","2013款 2.5 自动 尊贵型四驱,fengtianmao","2013款 2.5 自动 豪华型四驱,fengtianmap",
							"2013款 2.0 自动 风尚型四驱,fengtianmaq","2013款 2.5 自动 精英型四驱,fengtianmar","2013款 2.0 自动 新锐型四驱,fengtianmas","2013款 2.0 手动 都市型前驱,fengtianmat",
							"2013款 2.0 自动 都市版前驱,fengtianmau","2013款 2.0 自动 都市型前驱,fengtianmav","2013款 2.4 手动 豪华型炫装版四驱,fengtianmaw","2013款 2.4 自动 特享尊崇版四驱,fengtianmax",
							"2013款 2.4 手动 特享尊崇版四驱,fengtianmay","2013款 2.0 手动 特享精英版前驱,fengtianmaz","2013款 2.0 自动 特享精英版前驱,fengtianmba","2013款 2.0 自动 特享经典版前驱,fengtianmbb",
							"2013款 2.0 手动 特享经典版前驱,fengtianmbc","2015款 2.0 自动 风尚版前驱,fengtianmbd","2015款 2.0 自动 风尚版四驱,fengtianmbe","2015款 2.0 自动 都市版前驱,fengtianmbf",
							"2015款 2.0 手动 都市版前驱,fengtianmbg","2015款 2.5 自动 豪华版四驱,fengtianmbh","2015款 2.5 自动 精英版四驱,fengtianmbi","2015款 2.5 自动 尊贵版四驱,fengtianmbj",
							"2015款 2.0 自动 新锐版四驱,fengtianmbk"
							],

					"兰德酷路泽":["2007款 4.0 手动 GX-R四驱,fengtianna","2007款 4.7 自动 VX-R四驱,fengtiannb","2007款 4.7 自动 GX-R四驱,fengtiannc","2007款 4.0 自动 GX-R四驱,fengtiannd",
							"2009款 4.7 自动 VX四驱,fengtianne","2009款 4.7 自动 GX-R四驱,fengtiannf","2009款 4.0 自动 GX-R四驱,fengtianng","2009款 4.0 手动 GX-R四驱,fengtiannh",    
							"2010款 4.7 自动 VX四驱,fengtianni","2010款 4.0 自动 GX-R四驱,fengtiannj","2010款 4.0 自动 VX四驱,fengtiannk","2010款 4.7 自动 VX-R四驱,fengtiannl",
							"2011款 4.7 自动 VX四驱,fengtiannm","2011款 4.0 自动 GX-R四驱,fengtiannn",
							"2012款 4.7 自动 VX四驱,fengtianno","2012款 4.0 自动 GX-R四驱,fengtiannp","2012款 4.6 自动 VX-R四驱,fengtiannq","2012款 4.6 自动 VX四驱,fengtiannr",
							"2012款 4.0 自动 VX四驱,fengtianns"
							],
					
					
					"普拉多":["2005款 4.0 自动 GX运动DVD版四驱,fengtianoa","2005款 4.0 自动 VX四驱,fengtianob","2005款 4.0 自动 GX四驱,fengtianoc",
							"2005款 4.0 自动 GX运动DVD版四驱,fengtianod","2005款 4.0 自动 GX豪华DVD版四驱,fengtianoe","2005款 4.0 自动 GX豪华版四驱,fengtianof","2006款 4.0 自动 VX导航版四驱,fengtianog",
							"2006款 4.0 自动 VX四驱,fengtianoh","2007款 4.0 自动 VX四驱,fengtianoi","2007款 4.0 自动 GX运动DVD版四驱,fengtianoj","2007款 4.0 自动 GX豪华版四驱,fengtianok",
							"2007款 4.0 自动 GX豪华DVD版四驱,fengtianol","2007款 4.0 自动 GX四驱,fengtianom","2010款 4.0 自动 VX NAVI四驱,fengtianon","2010款 4.0 自动 VX四驱,fengtianoo","2010款 4.0 自动 TXL NAVI四驱,fengtianop",
							"2010款 4.0 自动 TXL四驱,fengtianoq","2010款 4.0 自动 TX四驱,fengtianor","2014款 4.0 自动 VX NAVI四驱,fengtianos","2014款 4.0 自动 VX四驱,fengtianot","2014款 4.0 自动 TXL NAVI四驱,fengtianou",
							"2014款 4.0 自动 TXL四驱,fengtianov","2014款 4.0 自动 TX四驱,fengtianow","2015款 2.7 自动 豪华版四驱,fengtianox","2015款 2.7 自动 标准版四驱,fengtianoy","2015款 2.7 手动 四驱,fengtianoz",
							"2016款 3.5 自动 TX-L NAVI四驱,fengtianoaa","2016款 2.7 手动 四驱,fengtianoab","2016款 2.7 自动 标准型四驱,fengtianoac",
							"2016款 3.5 自动 VX NAVI四驱,fengtianoad","2016款 3.5 自动 VX四驱,fengtianoae","2016款 3.5 自动 TX-L四驱,fengtianoaf","2016款 3.5 自动 TX四驱,fengtianoag","2016款 2.7 自动 豪华型四驱,fengtianoah"
							],
					
					"柯斯达":["2006款 2.7 手动 豪华型23座,fengtianpa","2006款 2.7 手动 标准型23座,fengtianpb","2006款 4.1T 手动 28座 柴油,fengtianpc",
							"2006款 4.1T 手动 标准型30座 柴油,fengtianpd","2006款 4.1T 手动 豪华车23座 柴油,fengtianpe","2006款 4.1T 手动 豪华车20座 柴油,fengtianpf",
							"2006款 4.1T 手动 高级车23座 柴油,fengtianpg","2006款 4.1T 手动 高级车20座 柴油,fengtianph","2006款 2.7 手动 豪华车23座,fengtianpi",
							"2006款 2.7 手动 豪华车20座,fengtianpj","2006款 2.7 手动 高级车23座,fengtianpk","2006款 2.7 手动 高级车20座,fengtianpl",
							"2007款 4.1T 手动 豪华车23座 柴油,fengtianpm","2007款 4.1T 手动 豪华车20座 柴油,fengtianpn",
							"2007款 4.1T 手动 高级车23座升级版 柴油,fengtianpo","2007款 4.1T 手动 高级车23座 柴油,fengtianpp",
							"2007款 4.1T 手动 高级车20座 柴油,fengtianpq","2007款 2.7 手动 豪华车23座,fengtianpr",
							"2007款 2.7 手动 豪华车20座,fengtianps","2007款 2.7 手动 高级车23座升级版,fengtianpt",
							"2007款 2.7 手动 高级车23座,fengtianpu","2007款 2.7 手动 高级车20座,fengtianpv",
							"2010款 2.7 手动 20座,fengtianpw","2010款 2.7 手动 豪华车23座,fengtianpx","2010款 2.7 手动 豪华车20座,fengtianpy",
							"2011款 4.1T 手动 高顶豪华版20座 柴油,fengtianpz","2011款 4.1T 手动 高顶豪华版23座 柴油,fengtianpaa",
							"2011款 4.1T 手动 高级升级版23座 柴油,fengtianpab","2011款 4.1T 手动 高级版23座 柴油,fengtianpac",
							"2011款 4.1T 手动 高级版20座 柴油,fengtianpad","2011款 4.1T 手动 高级升级车23座 柴油,fengtianpae",
							"2011款 4.1T 手动 豪华车23座 柴油,fengtianpaf","2011款 4.1T 手动 豪华车20座 柴油,fengtianpag",
							"2011款 4.1T 手动 高级车23座升级版 柴油,fengtianpah","2011款 4.1T 手动 高级车23座 柴油,fengtianpai",
							"2011款 4.1T 手动 高级车20座 柴油,fengtianpaj","2011款 2.7 手动 豪华车23座,fengtianpak",
							"2011款 2.7 手动 豪华车23座导航版,fengtianpal","2011款 2.7 手动 豪华车20座,fengtianpam",
							"2011款 2.7 手动 豪华车20座导航版,fengtianpan","2011款 2.7 手动 高级车23座升级版,fengtianpao",
							"2011款 2.7 手动 高级车23座,fengtianpap","2011款 2.7 手动 高级车20座,fengtianpaq",
							"2013款 4.0 手动 豪华版11-13座,fengtianpar","2013款 2.7 手动 高级升级版23座,fengtianpas","2013款 2.7 手动 高级版20座,fengtianpat",
							"2013款 2.7 手动 高级版23座,fengtianpau","2013款 4.0 手动 豪华版20座,fengtianpav","2013款 4.0 手动 豪华版23座,fengtianpaw"
							],
					
					"FJ 酷路泽":["2006款 4.0 手动 后驱,fengtianra","2006款 4.0 手动 四驱,fengtianrb","2006款 4.0 自动 四驱,fengtianrc",
								"2007款 4.0 自动 四驱,fengtianrd","2010款 4.0 手动 四驱,fengtianre","2010款 4.0 自动 四驱,fengtianrf",
								"2011款 4.0 自动 四驱,fengtianrg","2013款 4.0 自动 四驱,fengtianrh"
								],
					
					
					"威飒":["2009款 3.5 自动,fengtianta","2009款 2.7 自动,fengtiantb","2013款 2.7 自动 四驱至尊版,fengtiantc",
							"2013款 2.7 自动 前驱至尊版,fengtiantd","2013款 2.7 自动 四驱豪华版,fengtiante","2013款 2.7 自动 前驱豪华版,fengtiantf" 
							],
					
					
					"红杉":["2008款 5.7 自动 四驱,fengtianva","2008款 5.7 自动 后驱,fengtianvb","2008款 4.7 自动 后驱,fengtianvc",
							"2010款 5.7 自动 白金版四驱,fengtianvd"
							],
					
					"埃尔法":["2011款 3.5 自动 豪华型,fengtianwa","2011款 2.4 自动 豪华型,fengtianwb","2012款 3.5 自动 豪华型,fengtianwc",
							"2012款 2.4 自动 豪华型,fengtianwd","2012款 3.5 自动 尊贵版,fengtianwe","2015款 3.5 自动 尊贵版,fengtianwf","2015款 3.5 自动 豪华型,fengtianwg"
							],
					
					"普瑞维亚":["2006款 2.4 自动 前驱7座豪华型,fengtianxa","2006款 2.4 自动 前驱7座标准型,fengtianxb","2007款 3.5 自动 前驱7座豪华型,fengtianxc",
								"2012款 3.5 自动 前驱7座豪华版,fengtianxd","2012款 2.4 自动 前驱7座标准版,fengtianxe","2012款 2.4 自动 前驱7座豪华版,fengtianxf"
			                  ],
					
					"丰田86":["2012款 2.0 自动 GT,fengtianza","2013款 2.0 自动 豪华版,fengtianzb","2013款 2.0 手动 豪华版,fengtianzc",
							"2014款 2.0 自动 豪华版,fengtianzd","2014款 2.0 手动 豪华版,fengtianze"
							],
					
					"杰路驰":["2011款 2.5 自动 豪华版,fengtianaaa","2011款 2.5 自动 标准版,fengtianaab","2014款 2.5 自动 豪华版,fengtianaac"],
					
					"坦途":["2006款 5.7 自动 Crewmax,fengtianaca","2006款 4.7 自动,fengtianacb","2006款 4.7 自动 Crewmax,fengtianacc",
							"2006款 4.7 自动 Regular Cab,fengtianacd","2006款 4.0 自动 Regular Cab,fengtianace","2006款 4.0 自动,fengtianacf","2006款 5.7 自动,fengtianacg",
							"2010款 4.0 自动,fengtianach","2010款 5.7 自动,fengtianaci","2010款 4.6 自动,fengtianacj","2011款 5.7 自动 标准型,fengtianack",
							"2014款 5.7 自动 美规SR5版,fengtianacl","2014款 5.7 自动 美规1794限量版,fengtianacm","2015款 5.7 自动 美规TRDPRO版,fengtianacn"
							],
					
					"普拉多(进口)":["2007款 2.7 自动 中东版,fengtianada","2009款 3.0T 手动 中东版五门 柴油,fengtianadb","2009款 4.0 自动 ,fengtianadc",
								"2010款 2.7 手动 标准版,fengtianadd","2010款 2.7 自动 豪华版,fengtianade","2010款 2.7 自动 标准版,fengtianadf","2010款 4.0 自动 中东版,fengtianadg",
								"2010款 3.0 自动 中东版 柴油,fengtianadh","2012款 2.7 自动 中东版,fengtianadi","2013款 2.7 自动 中东版,fengtianadj",
								"2014款 2.7 自动 中东版三门,fengtianadk","2014款 2.7 自动 中东版,fengtianadl","2014款 2.7 自动 豪华版,fengtianadm",
								"2014款 2.7 自动 标准版,fengtianadn","2014款 2.7 手动 ,fengtianado","2015款 4.0 自动 中东版,fengtianadp"
								],
					
					"丰田RAV4(进口)":["2006款 2.4 自动 标准版,fengtianaea","2006款 2.4 自动 豪华版,fengtianaeb","2006款 2.5 自动,fengtianaec","2006款 2.0 自动,fengtianaed",
									"2006款 2.0 手动 五门,fengtianaee","2008款 3.5 自动,fengtianaef","2009款 2.5 自动,fengtianaeg","2010款 2.0 手动 五门,fengtianaeh","2010款 2.0 自动,fengtianaei"
									],
					
					"汉兰达(进口)":["2007款 3.5 自动 至尊版,fengtianafa","2007款 3.5 自动 运动豪华版,fengtianafb","2007款 3.5 自动 运动版,fengtianafc","2007款 3.5 自动 精英版,fengtianafd",
							"2007款 3.3 自动 7座,fengtianafe","2007款 3.3 自动 5座,fengtianaff","2008款 3.5 自动,fengtianafg","2008款 2.7 自动,fengtianafh"
							    ],
					
					"丰田佳美":["2011款 2.5 自动,fengtianaga"],
					
					"Wish":["2011款 2.0 自动 低配版,fengtiansa","2011款 2.0 自动 豪华版,fengtiansb","2011款 2.0 自动 科技版,fengtiansc",
							"2011款 2.0 自动 运动版,fengtiansd"
							],
					
					"奔跑者":["2011款 2.7 自动 中东版,fengtianqa","2014款 2.7 自动 2700中东版,fengtianqb","2015款 2.7 自动 标准版,fengtianqc"
							],
					

//"福特"

					"S-MAX":["2006款 2.3 自动,fordaa","2007款 2.3 自动 旗舰型导航版7座,fordab","2007款 2.3 自动 时尚型7座,fordac","2007款 2.3 自动 豪华型7座,fordad",
							"2007款 2.3 自动 旗舰型7座,fordae","2008款 2.3 自动 时尚型5座,fordaf","2008款 2.3 自动 旗舰型天窗版7座,fordag","2008款 2.3 自动 旗舰型导航天窗版7座,fordah",
							"2008款 2.3 自动 豪华型天窗版7座,fordai","2008款 2.3 自动 时尚型7座,fordaj","2008款 2.3 自动 豪华型7座,fordak"
							],
					
					"嘉年华":["2005款 三厢 1.6 手动 运动型,fordba","2005款 三厢 1.6 自动 豪华型,fordbb","2005款 三厢 1.6 手动 舒适型,fordbc",
							"2006款 三厢 1.6 自动 运动版,fordbd","2006款 三厢 1.6 自动 豪华型,fordbe","2006款 三厢 1.6 手动 运动型,fordbf","2006款 三厢 1.6 手动 舒适型,fordbg",
							"2006款 三厢 1.6 自动 运动型,fordbh","2008款 三厢 1.5 手动,fordbi","2008款 三厢 1.5 自动,fordbj","2008款 两厢 1.5 手动,fordbk",
							"2008款 两厢 1.5 自动,fordbl","2008款 两厢 1.3 手动,fordbm","2008款 两厢 1.3 自动 时尚型,fordbn","2009款 三厢 1.5 自动 豪华型,fordbo",
							"2009款 三厢 1.5 手动 运动型,fordbp","2009款 三厢 1.5 自动 时尚型,fordbq","2009款 三厢 1.5 手动 时尚型,fordbr",
							"2009款 两厢 1.3 手动 时尚经典型,fordbs","2009款 两厢 1.5 自动 运动型,fordbt","2009款 两厢 1.5 手动 运动型,fordbu","2009款 两厢 1.5 自动 时尚型,fordbv",
							"2009款 两厢 1.5 手动 时尚型,fordbw","2009款 两厢 1.3 自动 时尚型,fordbx","2009款 两厢 1.3 手动 时尚型,fordby",   
							"2010款 三厢 1.5 手动 光芒限定版,fordbz","2010款 三厢 1.5 自动 光芒限定版,fordbaa","2010款 三厢 1.3 手动 风尚型,fordbab",
							"2010款 两厢 1.5 手动 光芒限定版,fordbac","2010款 两厢 1.5 自动 光芒限定版,fordbad","2010款 两厢 1.3 手动 风尚型,fordbae","2010款 两厢 1.5 手动 限量型,fordbaf",
							"2010款 两厢 1.5 自动 限量型,fordbag",    
							"2011款 两厢 1.5 手动 运动型,fordbah","2011款 两厢 1.5 手动 时尚型,fordbai","2011款 两厢 1.5 自动 运动型,fordbaj","2011款 两厢 1.5 自动 时尚型,fordbak",
							"2011款 两厢 1.5 自动 劲动型,fordbal","2011款 两厢 1.3 手动 风尚型,fordbam",
							"2011款 三厢 1.5 手动 时尚型锋潮版,fordban","2011款 三厢 1.5 自动 时尚型锋潮版,fordbao","2011款 三厢 1.5 自动 豪华型锋潮版,fordbap",
							"2011款 三厢 1.3 手动 风尚型锋潮版,fordbaq","2012款 两厢 1.5 手动 动感限量版,fordbar","2012款 两厢 1.5 自动 动感限量版,fordbas",
							"2013款 两厢 1.5 自动 劲动型,fordbat","2013款 两厢 1.5 自动 运动型,fordbau","2013款 两厢 1.5 自动 品尚型,fordbav","2013款 两厢 1.5 自动 时尚型,fordbaw",
							"2013款 两厢 1.5 手动 时尚型,fordbax","2013款 两厢 1.5 手动 风尚型,fordbay",
							"2013款 三厢 1.5 自动 旗舰型,fordbaz","2013款 三厢 1.5 自动 品尚型,fordbba","2013款 三厢 1.5 自动 时尚型,fordbbb","2013款 三厢 1.5 手动 时尚型,fordbbc",
							"2013款 三厢 1.5 手动 风尚型,fordbbd","2014款 两厢 1.0T 自动 劲动型,fordbbe"
							],
					
					"福克斯":["2005款 三厢 1.8 手动 经典型,fordca","2005款 三厢 2.0 手动 运动型,fordcb","2005款 三厢 2.0 自动 豪华型,fordcc","2005款 三厢 1.8 自动 时尚型,fordcd",
							"2006款 两厢 1.8 手动 基本型,fordce","2006款 两厢 1.8 手动 经典型,fordcf","2006款 两厢 1.8 自动 时尚型,fordcg","2006款 两厢 2.0 手动 运动型,fordch",
							"2007款 三厢 2.0 自动 豪华型,fordci","2007款 三厢 1.8 手动 舒适型,fordcj","2007款 三厢 1.8 自动 时尚型,fordck",
							"2007款 两厢 2.0 手动 运动型,fordcl","2007款 两厢 2.0 自动 运动型,fordcm","2007款 两厢 1.8 手动 舒适型,fordcn","2007款 两厢 1.8 自动 时尚型,fordco",
							"2007款 两厢 1.8 手动 经典型,fordcp","2009款 三厢 1.8 自动 豪华型,fordcq","2009款 三厢 1.8 手动 舒适型,fordcr","2009款 三厢 1.8 自动 时尚型,fordcs",
							"2009款 三厢 2.0 自动 豪华型,fordct","2009款 两厢 2.0 手动 运动型,fordcu","2009款 两厢 2.0 自动 运动型,fordcv","2009款 两厢 1.8 自动 时尚型,fordcw",
							"2009款 两厢 1.8 手动 舒适型,fordcx","2010款 两厢 2.0 手动 SVP限量版,fordcy",
							"2011款 三厢 1.8 自动 时尚型,fordcz","2011款 三厢 1.8 手动 经典型,fordcaa","2011款 三厢 1.8 手动 舒适型,fordcab","2011款 三厢 2.0 自动 豪华型,fordcac",
							"2011款 三厢 1.8 自动 豪华型,fordcad","2011款 两厢 1.8 自动 时尚型,fordcae","2011款 两厢 2.0 自动 运动型,fordcaf","2011款 两厢 2.0 手动 运动型,fordcag",
							"2011款 两厢 1.8 手动 经典型,fordcah","2011款 两厢 1.8 手动 舒适型,fordcai","2012款 两厢 2.0 自动 豪华运动型,fordcaj","2012款 两厢 1.6 手动 舒适型,fordcak",
							"2012款 两厢 1.6 手动 风尚型,fordcal","2012款 两厢 1.6 自动 舒适型,fordcam","2012款 两厢 1.6 自动 风尚型,fordcan","2012款 两厢 1.8 自动 经典时尚型,fordcao",
							"2012款 两厢 1.8 手动 经典时尚型,fordcap","2012款 两厢 1.8 自动 经典基本型,fordcaq","2012款 两厢 1.8 手动 经典基本型,fordcar",
							"2012款 两厢 2.0 手动 豪华运动型,fordcas","2012款 三厢 2.0 自动 旗舰型,fordcat","2012款 三厢 1.8 自动 经典时尚型,fordcau",
							"2012款 三厢 1.8 手动 经典时尚型,fordcav","2012款 三厢 1.8 自动 经典基本型,fordcaw",
							"2012款 三厢 1.8 手动 经典基本型,fordcax","2012款 三厢 1.6 自动 尊贵型,fordcay","2012款 三厢 1.6 自动 风尚型,fordcaz","2012款 三厢 1.6 手动 风尚型,fordcba",
							"2012款 三厢 1.6 自动 舒适型,fordcbb","2012款 三厢 1.6 手动 舒适型,fordcbc","2013款 两厢 1.8 自动 百万纪念版,fordcbd","2013款 两厢 1.8 手动 百万纪念版,fordcbe",
							"2013款 两厢 1.8 手动 经典时尚型,fordcbf","2013款 两厢 1.8 自动 经典时尚型,fordcbg","2013款 两厢 1.8 自动 经典基本型,fordcbh","2013款 两厢 1.8 手动 经典基本型,fordcbi",
							"2013款 三厢 1.8 手动 百万纪念版,fordcbj","2013款 三厢 1.8 自动 百万纪念版,fordcbk","2013款 三厢 1.8 手动 经典时尚型,fordcbl","2013款 三厢 1.8 自动 经典时尚型,fordcbm",
							"2013款 三厢 1.8 自动 经典基本型,fordcbn","2013款 三厢 1.8 手动 经典基本型,fordcbo","2013款 两厢 2.0T 手动 ST标准版,fordcbp","2013款 两厢 2.0T 手动 ST橙色版,fordcbq",
							"2014款 两厢 1.8 自动 经典酷白典藏版,fordcbr","2014款 两厢 1.8 手动 经典酷白典藏版,fordcbs",
							"2015款 两厢 1.6 自动 舒适型,fordcbt","2015款 两厢 1.6 手动 舒适型,fordcbu",
							"2015款 三厢 1.5T 自动 GTDi旗舰型,fordcbv","2015款 三厢 1.5T 自动 GTDi精英型,fordcbw","2015款 三厢 1.0T 自动 GTDi超能风尚型,fordcbx",
							"2015款 三厢 1.0T 手动 GTDi超能风尚型,fordcby","2015款 三厢 1.6 自动 风尚型,fordcbz","2015款 三厢 1.6 手动 风尚型,fordcca","2015款 三厢 1.6 自动 舒适型,fordccb",
							"2015款 三厢 1.6 手动 舒适型,fordccc","2015款 两厢 1.5T 自动 GTDi运动型,fordccd","2015款 两厢 1.5T 自动 GTDi精英型,fordcce",
							"2015款 两厢 1.0T 自动 GTDi超能风尚型,fordccf","2015款 两厢 1.0T 手动 GTDi超能风尚型,fordccg","2015款 两厢 1.6 自动 风尚型,fordcch","2015款 两厢 1.6 手动 风尚型,fordcci"
							],
					
					"福睿斯":["2015款 1.5 自动 时尚型,fordda","2015款 1.5 自动 舒适型,forddb","2015款 1.5 手动 时尚型,forddc","2015款 1.5 手动 舒适型,forddd"
							],
					
					"翼搏":["2013款 1.5 自动 尊贵型前驱,fordea","2013款 1.0T 手动 尊贵型前驱,fordeb","2013款 1.5 手动 尊贵型前驱,fordec",
							"2013款 1.5 自动 风尚型前驱,forded","2013款 1.5 手动 风尚型前驱,fordee","2013款 1.5 手动 舒适型前驱,fordef"
							],
					
					"翼虎":["2013款 2.0T 自动 尊贵型四驱,fordfa","2013款 2.0T 自动 运动型四驱,fordfb","2013款 2.0T 自动 精英型四驱,fordfc","2013款 1.6T 自动 精英型四驱,fordfd",
							"2013款 1.6T 自动 风尚型前驱,fordfe","2013款 1.6T 自动 舒适型前驱,fordff",
							"2015款 2.0T 自动 尊贵型四驱,fordfg","2015款 2.0T 自动 运动型四驱,fordfh","2015款 2.0T 自动 精英型四驱,fordfi","2015款 1.5T 自动 精英型四驱,fordfj",
							"2015款 1.5T 自动 风尚型前驱,fordfk","2015款 1.5T 自动 舒适型前驱,fordfl"
							],   

					
					"蒙迪欧":["2005款 2.0 自动 尊贵型新装版,fordga","2005款 2.0 手动 经典型,fordgb","2005款 2.0 自动 精英型,fordgc","2005款 2.5 自动 旗舰型,fordgd",
							"2005款 2.0 自动 尊享型,fordge","2006款 2.0 自动 经典型,fordgf","2006款 2.5 自动 经典型,fordgg","2006款 2.5 自动 旗舰型,fordgh",
							"2006款 2.5 自动 精英型,fordgi","2006款 2.0 自动 尊贵型新装版,fordgj","2006款 2.0 自动 精英型,fordgk","2006款 2.0 手动 经典型,fordgl",
							"2007款 2.5 自动 旗舰型,fordgm","2007款 2.0 自动 尊贵型,fordgn","2007款 2.0 自动 精英型,fordgo","2007款 2.0 手动 经典型,fordgp",
							"2008款 2.3 自动 豪华运动型导航版,fordgq","2008款 2.3 自动 至尊型,fordgr","2008款 2.3 自动 时尚型,fordgs","2008款 2.3 自动 豪华运动型,fordgt",
							"2008款 2.0 手动 舒适型,fordgu","2008款 2.3 自动 豪华型,fordgv",
							"2010款 2.3 自动 至尊型,fordgw","2010款 2.3 自动 豪华运动型导航版,fordgx","2010款 2.3 自动 豪华运动型,fordgy","2010款 2.3 自动 豪华型,fordgz",
							"2010款 2.3 自动 时尚型,fordgaa","2010款 2.0 手动 舒适型,fordgab",
							"2011款 2.0T 自动 GTDi200豪华型,fordgac","2011款 2.0T 自动 GTDi200时尚型,fordgad","2011款 2.0T 自动 GTDi240豪华运动型,fordgae",
							"2011款 2.0T 自动 GTDi240豪华运动型导航版,fordgaf","2011款 2.0T 自动 GTDi240至尊型,fordgag","2011款 2.3 自动 时尚型,fordgah",
							"2011款 2.3 自动 豪华型,fordgai","2011款 2.0 手动 舒适型,fordgaj","2012款 2.0T 自动 GTDi240旗舰运动型,fordgak",
							"2013款 2.0T 自动 至尊型GTDi240,fordgal","2013款 2.0T 自动 时尚型GTDi200,fordgam","2013款 2.0T 自动 旗舰型GTDi240,fordgan",
							"2013款 2.0T 自动 豪华运动型GTDi240,fordgao","2013款 2.0T 自动 豪华型GTDi200,fordgap","2013款 2.3 自动 豪华型,fordgaq",
							"2013款 2.3 自动 时尚型,fordgar","2013款 1.5T 自动 时尚型GTDi180,fordgas","2013款 1.5T 自动 舒适型GTDi180,fordgat"
							],
					
					"锐界":["2011款 3.7 自动 运动版,fordha","2011款 3.5 自动 尊锐型,fordhb","2011款 3.5 自动 精锐型天窗版,fordhc","2011款 3.5 自动 精锐型,fordhd",              
							"2012款 2.0T 自动 尊锐型,fordhe","2012款 2.0T 自动 精锐天窗版,fordhf","2012款 2.0T 自动 精锐型,fordhg","2012款 3.5 自动 尊锐型,fordhh",
							"2012款 3.5 自动 精锐型天窗版,fordhi","2012款 3.5 自动 精锐型,fordhj"
							],
					
					"全顺":["2015款 2.4T 手动 新世代长轴中顶多功能型6座 柴油,fordia"],
					
					
					"嘉年华(进口)":["2012款 两厢 1.6T 手动 ST三门版,fordka","2013款 两厢 1.6T 手动 ST,fordkb"],
					
					"房车E系列":["2007款 5.4 自动 250,fordla","2008款 6.8 自动 450,fordlb","2010款 5.4 自动 350七座皇家游艇版,fordlc",
								"2011款 5.4 自动 350七座皇家游艇版,fordld","2011款 5.4 自动 350铂金限量版,fordle","2011款 5.4 自动 350铂金版,fordlf",
								"2011款 5.4 自动 350白金版,fordlg"
								],
													
					"探险者":["2005款 4.6 自动 后驱,fordma","2005款 4.0 自动 四驱,fordmb","2005款 4.0 自动 后驱,fordmc",
							"2006款 4.6 自动 后驱,fordmd","2010款 3.5 自动 前驱,fordme","2013款 3.5 自动 尊享型,fordmf",
							"2013款 3.5 自动 尊尚型,fordmg","2016款 3.5T 自动 铂金版,fordmh","2016款 3.5T 自动 运动版,fordmi",
							"2016款 3.5T 自动 精英版,fordmj","2016款 2.3T 自动 精英版,fordmk","2016款 2.3T 自动 风尚版,fordml"
							],
					
					"爱虎":["2007款 3.5 自动,fordna","2007款 3.5 自动 四驱,fordnb","2009款 3.5 自动 Sport四驱,fordnc"],
					
					"猛禽F系":["2009款 6.2 自动,fordoa","2009款 5.4 自动 F-150-Super-Crew(320HP),fordob","2009款 5.4 自动 F-150-Super-Crew(310HP),fordoc",
							"2009款 4.6 自动 F-150-Super-Crew(292HP),fordod","2009款 4.6 自动 F-150-Super-Crew(282HP),fordoe",
							"2009款 4.6 自动 F-150-Super-Crew(248HP),fordof","2009款 4.6 自动 F-150-Super-Crew(238HP),fordog",
							"2009款 5.4 自动 (330HP),fordoh","2009款 5.4 自动 F-150-Super-Cab(320HP),fordoi","2009款 5.4 自动 F-150-Super-Cab(310HP),fordoj",
							"2009款 5.4 自动 (320HP),fordok","2009款 5.4 自动 (310HP),fordol","2009款 4.6 自动 (292HP),fordom",
							"2009款 4.6 自动 (282HP),fordon","2009款 4.6 自动 (248HP),fordoo","2009款 4.6 自动 (238HP),fordop",
							"2009款 5.4 自动,fordoq",
							"2011款 6.7T 自动 柴油,fordor","2011款 6.2 自动,fordos","2011款 6.2 自动 Harley Davidson,fordot",
							"2011款 6.2 自动 SVT Raptor SuperCrew,fordou","2011款 6.2 自动 SVT Raptor SuperCab,fordov",
							"2013款 6.2 自动 F-350,fordow","2014款 6.2 自动 F-150美规版,fordox","2015款 3.5T 自动 F-150美规版,fordoy"
							],
					
					"福克斯(进口)":["2013款 两厢 2.0T 手动 ST橙色版,fordpa","2013款 两厢 2.0T 手动 ST标准版,fordpb"],
					
					
					"翼虎(进口)":["2007款  Maverick 3.0 自动 都市探险型,fordra","2007款 Escape 3.0 自动 四驱,fordrb","2008款 Escape 3.0 自动 前驱,fordrc",
								"2010款 Escape 2.3 自动 导航版四驱,fordrd","2010款 Escape 2.3 自动 标准版四驱,fordre","2010款 Escape 2.3 自动 导航版前驱,fordrf",
								"2010款 Escape 2.3 自动 标准版前驱,fordrg","2010款 Escape 2.3 自动 标准版四驱,fordrh"
								],
					
					"蒙迪欧(进口)":["2005款 2.0 自动,fordsa","2005款 2.5 自动,fordsb"],
					
					"野马":["2009款 敞篷版 5.8 手动 GT500,fordta","2009款 敞篷版 5.4 手动 GT500,fordtb","2009款 敞篷版 4.6 手动 V8,fordtc","2009款 敞篷版 4.6 自动 V8,fordtd",
							"2009款 敞篷版 4.0 手动 V6,fordte","2009款 敞篷版 4.0 自动 V6,fordtf","2009款 5.8 手动 GT500,fordtg","2009款 4.6 自动,fordt","2009款 4.0 自动,fordth",
							"2009款 5.4 手动 GT500,fordti","2009款 4.6 手动,fordtj","2009款 4.0 手动,fordtk",
							"2012款 5.0 手动 Boss302赛道版,fordtl","2012款 5.0 手动 Boss302标准型,fordtm","2012款 5.0 自动 豪华型GT,fordtn","2012款 5.0 手动 豪华型GT,fordto",
							"2012款 5.0 手动 标准型GT,fordtp","2012款 5.0 自动 标准型GT,fordtq","2012款 5.4T 手动 GT500豪华型,fordtr","2012款 5.4T 手动 GT500标准型,fordts",
							"2012款 3.7 自动 豪华型,fordtt","2012款 3.7 手动 豪华型,fordtu","2012款 3.7 自动 标准型,fordtv","2012款 3.7 手动 标准型,fordtw",
							"2013款 3.7 自动 标准型,fordtx","2013款 3.7 手动 标准型,fordty","2013款 5.0 手动 GT标准型,fordtz","2013款 5.0 自动 GT标准型,fordtaa",
							"2013款 5.8T 手动 GT500,fordtab","2013款 5.0 手动 Boss302,fordtac",
							"2015款 5.0 自动 运动版GT,fordtad","2015款 5.0 自动 性能版GT,fordtae","2015款 2.3T 自动 50周年纪念版,fordtaf","2015款 2.3T 自动 运动版,fordtag",
							"2015款 2.3T 自动 性能版,fordtah"
							],
					
					"锐界":["2015款 2.7T 自动 尊锐型四驱,fordua","2015款 2.7T 自动 运动型四驱,fordub","2015款 2.0T 自动 尊锐型四驱,forduc","2015款 2.0T 自动 豪锐型四驱,fordud",
							"2015款 2.0T 自动 豪锐型前驱,fordue","2015款 2.0T 自动 铂锐型前驱,forduf","2015款 2.0T 自动 精锐型前驱,fordug"
							],
							
					"金牛座":["2016款 2.7T 自动 旗舰型GTDi,fordva","2016款 2.7T 自动 时尚型GTDi,fordvb","2016款 2.0T 自动 至尊型GTDi,fordvc",
							"2016款 2.0T 自动 豪华型GTDi,fordvd","2016款 2.0T 自动 时尚型GTDi,fordve"
							],		

					
//"福迪"			
					
					
					"小超人":["2006款 2.2 手动,fudiaa","2012款 2.2 手动 后驱,fudiab","2012款 2.0T 手动 后驱 柴油,fudiac",
							  "2012款 2.2 手动 加长版后驱,fudiad","2012款 2.0T 手动 加长版后驱 柴油,fudiae"
						    ],
					
					"探索者":["2006款 2.2 手动 后驱,fudiba","2006款 2.8T 手动 豪华型升级版后驱 柴油,fudibb","2006款 2.8T 手动 标准型升级版后驱 柴油,fudibc",
							  "2007款 2.2 手动 标准型后驱,fudibd","2007款 2.2 手动 豪华型后驱,fudibe","2008款 2.2 手动 标准型后驱,fudibf",
							  "2008款 2.2 手动 豪华型后驱,fudibg","2008款 2.8T 手动 标准型后驱 柴油,fudibh",
							  "2008款 2.8T 手动 豪华型后驱 柴油,fudibi","2009款 2.8T 手动 标准型后驱 柴油,fudibj",
							  "2009款 2.8T 手动 豪华型后驱 柴油,fudibk","2010款 2.2 手动 标准型后驱,fudibl","2011款 2.0T 手动 后驱 柴油,fudiblm",
							  "2011款 2.2 手动 后驱,fudibln"
						    ],
					
					
					"揽福":["2015款 2.4 手动 标准版后驱,fudica","2015款 2.4 手动 豪华版后驱,fudicb","2015款 2.4 手动 豪华版四驱,fudicc",
							"2015款 1.9T 手动 标准版后驱 柴油,fudicd","2015款 1.9T 手动 豪华版后驱 柴油,fudice","2015款 1.9T 手动 豪华版四驱 柴油,fudicf",
						    ],
					
					
					"雄狮":["2011款 2.2 手动 NHQ1027A4,fudida","2011款 2.2 手动 NHQ1027A4加长版,fudidb","2011款 2.0 手动 NHQ1028D1,fudidc",
							  "2011款 2.0 手动 NHQ1028LD1加长版,fudidd","2012款 2.5T 手动 NH1028D3 柴油,fudide","2012款 2.4 手动 豪华型后驱,fudidf",
							  "2012款 2.4 手动 豪华型四驱,fudidg","2012款 2.8T 手动 豪华型四驱 柴油,fudidh",
							  "2012款 2.8T 手动 豪华型后驱 柴油,fudidi","2012款 2.0T 手动 加长版柴油后驱,fudidj",
							  "2013款 2.2 手动 标准型后驱,fudidk"
						    ],
					
					"飞越":["2005款 2.2 手动 实用型后驱,fudiea","2005款 2.4 手动 精英型后驱,fudieb","2005款 3.2T 手动 精英型后驱 柴油,fudiec",
							"2005款 2.4 手动 精英型四驱,fudied","2006款 2.2 手动 实用型后驱,fudiee"
						    ],
					
//"法拉利"
					
					"488":["2015款 3.9T 自动 GTB,ferrayaa"
						    ],
					
					"575":["2002款 5.7 手动 M Maranello,ferrayba"
						    ],
					
					"599":["2006款 6.0 自动 GTB Fiorano,ferrayca","2006款 6.0 手动 GTB Fiorano,ferraycb","2011款 6.0 自动 Sa Aperta限量版,ferraycc"
						    ],
					
					"612":["2004款 5.7 自动 Sessanta,ferrayda","2004款 5.7 自动 Scaglietti,ferraydb","2007款 5.7 自动 Sessanta,ferraydc"
						    ],
					
					"360":["2004款 3.6 手动 GTC,ferrayea","2004款 3.6 自动,ferrayeb"
						    ],
					
					"F430":["2004款 4.3 手动,ferrayfa","2004款 4.3 自动,ferrayfb","2006款 4.3 手动 标准硬顶版赛车Challenge,ferrayfc",
							"2006款 4.0 手动 赛道硬顶版赛车GTC,ferrayfd","2007款 4.3 手动 Scuderia Coupe,ferrayfe",
							"2009款 4.3 自动 16M纪念版限量版,ferrayff","2009款 4.3 自动 Scuderia Coupe,ferrayfg"
						    ],
					
					"F149":["2008款 4.3 自动 ,ferrayga","2012款 4.3 自动 ,ferraygb","2015款 3.9T 自动 ,ferraygc"
						    ],
						    
					"458":["2009款 4.5 自动 ,ferrayha","2011款 4.5 自动,ferrayhb","2012款 4.5 自动 Italia中国限量版,ferrayhc",
							"2014款 4.5 自动 Speciale,ferrayhd"
						    ],
					
					"FF":["2011款 6.3 自动,ferrayia"
						    ],
					
					"F12":["2013款 6.3 自动 标准型,ferrayia"
						    ],
					
					"LaFerrari":["2014款 6.3 自动 限量版 油电混合,ferrayja"
						    ],
					
					
//"光冈"
				
					"大蛇":["2010款 3.3 自动 普通款 ,mitsuokaaa","2010款 3.5T 自动 机械增压版,mitsuokaab"
						    ],
					
					"女王":["2010款 2.0 自动,mitsuokaba"
						    ],
					
					"嘉路":["2005款 4.5 自动,mitsuokaca","2010款 3.7 自动,mitsuokacb"
						    ],
					
					
//	"GMC"
					
					
					"阿卡迪亚":["2007款 3.6 自动,gmcaa"
						    ],
					
					
					"使节":["2008款 4.2 自动,gmcba","2008款 5.3 自动,gmcbb"
						    ],
					
					"Savana":["2005款 6.0 自动,gmcca","2005款 5.3 自动,gmccb","2007款 6.0 自动 总统级7座,gmccc","2007款 6.0 自动 皇家级别EXP 12座,gmccd",
							"2007款 6.0 自动 领袖级7座,gmcce","2007款 6.0 自动 皇家级7座,gmccf","2007款 6.0 自动 皇家级10座,gmccg","2007款 6.0 自动 领袖级10座,gmcch",
							"2007款 5.3 自动 皇家级7座,gmcci","2007款 5.3 自动 领袖级7座,gmccj","2010款 6.0 自动 总统级7座,gmcck","2010款 5.3 自动 总统级7座,gmccl",
							"2011款 6.0 自动 商务之星10座,gmccm","2011款 6.0 自动 商务之星7座,gmccn","2011款 5.3 自动 商务之星7座,gmcco",
							"2011款 6.0 自动 加长领袖级7座,gmccp","2012款 6.0 自动 皇家级,gmccq","2012款 6.0 自动 商务之星公爵版,gmccr","2012款 6.0 自动 总裁级隐私屏版,gmccs",
							"2012款 6.0 自动 总裁级无隐私屏版,gmcct","2012款 6.0 自动 领袖级至尊版,gmccu","2012款 6.0 自动 领袖级经典版,gmccv",                  
							"2013款 6.0 自动 舒适版3500,gmccw","2013款 6.0 自动 标准版3500,gmccx","2013款 6.0 自动 运动版2500S 10座,gmccy",
							"2013款 6.0 自动 运动版2500S 7座,gmccz","2013款 6.0 自动 领袖级长轴,gmccaa","2013款 5.3 自动 领袖级四驱,gmccab",
							"2013款 5.3 自动 领袖级,gmccac","2013款 5.3 自动 运动版1500,gmccad","2013款 5.3 自动 总裁级,gmccae",
							"2013款 6.0 自动 豪华隐私屏版7座,gmccaf","2013款 6.0 自动 领袖级商务车,gmccag","2013款 5.3 自动 领袖至尊版,gmccah",                  
							"2014款 5.3 自动 雅致版G600S四驱,gmccai","2014款 5.3 自动 领袖级G2500S四驱,gmccaj","2014款 5.3 自动 尊享版1500S,gmccak",
							"2014款 6.0 自动 领袖级长轴,gmccal","2014款 6.0 自动 2500S,gmccam","2014款 5.3 自动 1500S,gmccan",
							"2014款 5.3 自动 领袖级四驱,gmccao","2014款 5.3 自动 领袖级,gmccap"
							],                  

					"育空河":["2008款 4.8 自动,gmcda","2008款 5.3 自动,gmcdb","2008款 5.3 自动 XL,gmcdc",
							"2008款 6.0 自动 油电混合,gmcdd","2008款 6.0 自动 XL,gmcde",
							"2008款 6.2 自动,gmcdf","2008款 6.2 自动 XL,gmcdg","2011款 6.2 自动 领袖版长轴,gmcdh"
						    ],
					
					"西拉":["2012款 6.2 自动 Denali,gmcea"
						    ],
					
					"Terrain":["2009款 3.0 自动 ,gmcfa","2012款 3.0 自动 基本型,gmcfb"
						    ],
					
					
				
//"广汽"

				"传祺GA5":["2011款 2.0 自动 精英型,guangqiaa","2011款 2.0 手动 舒适型,guangqiab","2011款 2.0 手动 精英型,guangqiac","2011款 2.0 自动 尊贵型,guangqiad",
						"2011款 2.0 自动 豪华型,guangqiae","2012款 2.0 手动 舒适型,guangqiaf","2012款 2.0 手动 精英型,guangqiag","2012款 2.0 自动 精英型,guangqiah",
						"2012款 2.0 自动 尊贵型,guangqiai","2012款 1.8 手动 舒适型,guangqiaj","2012款 1.8 手动 精英型,guangqiak","2012款 1.8 自动 尊贵型,guangqial",
						"2012款 1.8 自动 豪华型,guangqiam","2012款 2.0 自动 豪华型,guangqian", "2013款 1.8 手动 舒适版,guangqiao",                 
						"2013款 1.8T 自动 尊贵版,guangqiap","2013款 1.8T 自动 至尊版,guangqiaq","2013款 1.8T 自动 舒适版,guangqiar","2013款 1.8T 自动 精英版,guangqias",
						"2013款 1.8T 自动 豪华版,guangqiat","2013款 1.8T 自动 五周年行政版,guangqiau","2013款 1.8T 自动 五周年纪念版,guangqiav",
						"2013款 2.0 自动 豪华版,guangqiaw","2013款 2.0 自动 精英版,guangqiax","2013款 1.8 自动 豪华型,guangqiay","2013款 1.8 手动 精英版,guangqiaz",
						"2014款 1.6T 自动 豪华版,guangqiaaa","2014款 1.6T 自动 精英版,guangqiaab","2014款 1.6T 手动 舒适版,guangqiaac","2014款 1.6T 自动 舒适版,guangqiaad",
						"2015款 增程式尊享版 纯电动,guangqiaae","2015款 增程式精英版 纯电动,guangqiaaf"
						],
				
				
				
				
				"传祺GS5":["2012款 2.0 手动 周年限量版前驱,guangqiba","2012款 2.0 自动 周年纪念版前驱,guangqibb","2012款 2.0 手动 精英ESP版前驱,guangqibc",
						"2012款 1.8T 自动 至尊版四驱,guangqibd","2012款 2.0 自动 尊贵版前驱,guangqibe","2012款 2.0 自动 豪华版前驱,guangqibf",
						"2012款 2.0 自动 精英版前驱,guangqibg","2012款 2.0 手动 豪华版前驱,guangqibh","2012款 2.0 手动 精英版前驱,guangqibi","2012款 2.0 手动 舒适版前驱,guangqibj",
						"2013款 1.8T 自动 至尊版四驱,guangqibk","2013款 1.8T 自动 精英版前驱,guangqibl","2013款 1.8T 自动 尊贵版四驱,guangqibm",
						"2013款 1.8T 自动 五周年纪念版前驱,guangqibn",
						"2013款 1.8T 自动 五周年纪念版四驱,guangqibo","2013款 1.8T 自动 豪华版前驱,guangqibp","2013款 1.8T 自动 豪华版四驱,guangqibq",
						"2013款 1.8T 手动 精英版前驱,guangqibr","2013款 2.0 自动 周年纪念版前驱,guangqibs","2013款 2.0 自动 尊贵版前驱,guangqibt",
						"2013款 2.0 自动 豪华版前驱,guangqibu","2013款 2.0 自动 精英版前驱,guangqibv","2013款 2.0 手动 豪华版前驱,guangqibw",
						"2013款 2.0 手动 精英ESP版前驱,guangqibx","2013款 2.0 手动 精英版前驱,guangqiby","2013款 2.0 手动 舒适版前驱,guangqibz",                  
						"2014款 2.0 自动 周年增值版前驱,guangqibaa","2014款 2.0 手动 周年增值版前驱,guangqibab","2014款 2.0 自动 超享版前驱,guangqibac",
						"2014款 1.8T 自动 超享版前驱,guangqibad","2014款 2.0 自动 浅内特别版前驱,guangqibae","2014款 2.0 手动 浅内特别版前驱,guangqibaf",
						"2015款 2.0 手动 速博舒适版前驱,guangqibag","2015款 2.0 手动 速博精英版前驱,guangqibah","2015款 1.8T 自动 豪华导航版前驱,guangqibai",
						"2015款 1.8T 自动 舒适版前驱,guangqibaj","2015款 2.0 自动 豪华导航版前驱,guangqibak","2015款 2.0 自动 精英版前驱,guangqibal",
						"2015款 2.0 自动 舒适版前驱,guangqibam","2015款 2.0 手动 精英版前驱,guangqiban","2015款 2.0 手动 舒适版前驱,guangqibao",
						"2015款 1.8T 自动 尊贵版四驱,guangqibap","2015款 1.8T 自动 精英版四驱,guangqibaq","2015款 1.8T 自动 豪华版四驱,guangqibar",
						"2015款 1.8T 自动 至尊版前驱,guangqibas","2015款 1.8T 自动 尊贵版前驱,guangqibat","2015款 1.8T 自动 豪华版前驱,guangqibau"
						],
				    
				    
				    
				    
				"传祺GA3":["2013款 1.6 自动 至尊ESP型,guangqica","2013款 1.6 自动 尊贵ESP型,guangqicb","2013款 1.6 自动 豪华ESP型,guangqicc",
						"2013款 1.6 自动 精英ESP型,guangqicd","2013款 1.6 手动 尊贵ESP型,guangqice","2013款 1.6 手动 豪华ESP型,guangqicf",
						"2013款 1.6 手动 豪华型,guangqicg","2013款 1.6 手动 精英型,guangqich","2014款 1.6 自动 精英智慧型,guangqici",
						"2014款 1.6 自动 智慧版,guangqicj"
						],
				
				                  
				                  
				"传祺GA3S视界":["2014款 1.6 自动 至尊ESP版,guangqida","2014款 1.6 自动 智慧ESP版,guangqidb","2014款 1.6 自动 豪华ESP版,guangqidc",
						"2014款 1.6 手动 豪华ESP版,guangqidd","2014款 1.6 自动 精英ESP版,guangqide","2014款 1.6 手动 精英ESP版,guangqidf",
						"2014款 1.6 手动 豪华版,guangqidg","2014款 1.6 手动 精英版,guangqidh"
						 ],
				                 
				                  
				                                           
				"传祺GA6":["2015款 1.6T 手动 风尚型,guangqiea","2015款 1.8T 自动 至尊型,guangqieb","2015款 1.8T 自动 尊贵型,guangqiec",
						"2015款 1.8T 自动 豪华智慧型,guangqied","2015款 1.8T 自动 豪华导航型,guangqiee","2015款 1.8T 自动 精英型,guangqief",
						"2015款 1.8T 自动 舒适型,guangqieg","2015款 1.6T 手动 精英型,guangqieh","2015款 1.6T 手动 舒适型,guangqiei"
						],
				                  
				                  
				                                           
				"传祺GS4":["2015款 1.3T 自动 200T尊贵版,guangqifa","2015款 1.3T 自动 200T豪华版,guangqifb","2015款 1.3T 自动 200T精英版,guangqifc",
						"2015款 1.3T 自动 200T舒适版,guangqifd","2015款 1.3T 手动 200T尊贵版,guangqife","2015款 1.3T 手动 200T豪华版,guangqiff",
						"2015款 1.3T 手动 200T精英版,guangqifg","2015款 1.3T 手动 200T舒适版,guangqifh"
						],
    
					
//"观致"		
					
					
				"观致3":["2015款 1.6 自动 致享型增强版,guanzhiaa","2015款 1.6 手动 致享型增强版,guanzhiab","2015款 1.6 手动 致享型,guanzhiac",
					"2015款 1.6T 自动 致酷型,guanzhiad","2015款 1.6 自动 致能型,guanzhiae","2015款 1.6 自动 致享型,guanzhiaf",
					"2015款 1.6 手动 致尚型,guanzhiag","2015款 1.6T 自动 至臻增强型,guanzhiah",                                                       
					"2015款 1.6 自动 致享型增强版,guanzhiai","2015款 1.6T 自动 致酷型,guanzhiaj","2015款 1.6 自动 致能型,guanzhiak",
					"2015款 1.6 自动 致享型,guanzhial","2015款 1.6 手动 致尚型,guanzhiam",
					"2014款 1.6 手动 致享增强型,guanzhian","2014款 1.6T 自动 致臻型,guanzhiao","2014款 1.6T 自动 致悦增强型,guanzhiap",
					"2014款 1.6 自动 致悦增强型,guanzhiaq","2014款 1.6 自动 致悦型,guanzhiar","2014款 1.6 手动 致享型,guanzhias",
					"2014款 1.6T 自动 致臻增强型,guanzhiat","2014款 1.6 自动 致臻增强型,guanzhiau","2014款 1.6 手动 致享增强型,guanzhiav",
					"2014款 1.6T 自动 致臻型,guanzhiaw","2014款 1.6T 自动 致悦型,guanzhiax","2014款 1.6 自动 致悦增强型,guanzhiay",
					"2014款 1.6 自动 致悦型,guanzhiaz","2014款 1.6 手动 致悦型,guanzhiaaa","2014款 1.6 手动 致享型,guanzhiaab"
					],                  
					                 
					                                           
				"观致3都市SUV":["2015款 1.6 手动 致享型增强版,guanzhiba","2015款 1.6T 自动 致臻型,guanzhibb","2015款 1.6T 自动 致酷型,guanzhibc",
					"2015款 1.6T 自动 致悦型,guanzhibd","2015款 1.6T 手动 致悦型,guanzhibe","2015款 1.6T 手动 致享型,guanzhibf",
					"2015款 1.6T 自动 型动派增强型,guanzhibg","2015款 1.6T 自动 炫动派,guanzhibh","2015款 1.6T 自动 型动派,guanzhibi",
					"2015款 1.6T 手动 型动派,guanzhibj","2015款 1.6T 手动 悦动派,guanzhibk"
					],
					
					
//"GUMPERT"		
				"阿波罗":["2007款 4.2T 手动,gumpertaa"
					],	

					
//"悍马"	
					
				"悍马H2":["2004款 6.0 自动 SUT,hanmaaa","2007款 6.2 自动 加长版,hanmaab","2008款 6.2 自动,hanmaac",
					  "2011款 6.2 自动 六轮加长版,hanmaad"
					],	
					
					
				"悍马H3":["2005款 5.3 自动,hanmaba","2005款 3.5 自动,hanmabb","2007款 5.3 自动 阿尔法,hanmabc",
					  "2008款 3.7 自动,hanmabd","2009款 3.7T 自动,hanmabe","2009款 3.7T 手动,hanmabf"
					],	
					
//"华泰"		
					
					
				"圣达菲":["2009款 1.8T 手动 C9豪华版前驱,huataiaa","2009款 1.8T 手动 C9舒适版前驱,huataiab","2009款 2.0T 手动 豪华型四驱 柴油,huataiac",
						"2009款 2.0T 手动 C9舒适型前驱 柴油,huataiad","2009款 2.0T 手动 C9豪华型前驱 柴油,huataiae","2009款 2.0T 手动 舒适型前驱 柴油,huataiaf",
						"2009款 2.0T 手动 豪华型前驱 柴油,huataiag","2009款 2.7 自动 豪华型四驱,huataiah","2009款 2.7 自动 标准型四驱,huataiai",
						"2009款 2.0T 手动 标准型前驱 柴油,huataiaj",
						"2010款 2.7 自动 C9商务型四驱,huataiak","2010款 1.8T 手动 C9豪华型前驱,huataial","2010款 1.8T 手动 C9舒适型前驱,huataiam",
						"2012款 2.0T 手动 飓风型四驱 柴油,huataian","2012款 2.0T 手动 飓风型前驱 柴油,huataiao","2012款 2.0 手动 导航版前驱,huataiap",
						"2012款 2.7 自动 旗舰型四驱,huataiaq","2012款 2.0T 自动 尊贵型四驱 柴油,huataiar","2012款 2.0T 手动 豪华型四驱 柴油,huataias",
						"2012款 2.0T 手动 豪华型前驱 柴油,huataiat","2012款 1.8T 自动 尊贵型前驱,huataiau","2012款 1.8T 手动 舒适型前驱,huataiav",
						"2012款 1.8T 手动 豪华型前驱,huataiaw","2012款 2.0 手动 豪华型前驱,huataiax","2012款 2.0 手动 舒适型前驱,huataiay",
						"2013款 2.0 手动 天窗版前驱,huataiaz","2013款 1.8T 手动 导航版前驱,huataiaaa","2013款 1.8T 自动 天窗版前驱,huataiaab",
						"2013款 2.0T 手动 天窗型四驱 柴油,huataiaac","2013款 2.0T 手动 天窗型前驱 柴油,huataiaad","2015款 2.0 手动 经典舒适型前驱,huataiaae",
						"2015款 2.0 手动 经典标准型前驱,huataiaaf","2015款 2.0 手动 经典实用型前驱,huataiaag","2015款 2.0T 手动 豪华型前驱 柴油,huataiaah",
						"2015款 2.0T 手动 舒适型前驱 柴油,huataiaai","2015款 1.5T 手动 豪华型前驱,huataiaaj","2015款 1.5T 手动 舒适型前驱,huataiaak"
						],
				                 
				                  
				                                           
				
				
				"特拉卡":["2007款 2.4 手动 豪华型四驱,huataiba","2007款 2.4 手动 豪华型后驱,huataibb","2007款 2.4 手动 超值版后驱,huataibc",
						"2008款 2.4 手动 豪华型四驱,huataibd","2012款 2.4 手动 豪华型四驱,huataibe","2012款 2.4 手动 豪华型后驱,huataibf",
						"2010款 2.4 手动 T9豪华型四驱,huataibg","2010款 2.4 手动 T9豪华型后驱,huataibh"
						],                  
						   
				   
				"吉田":["2003款 3.0 自动 四驱,huataica"], 
				                  
				                  
				                                           
				"B11":["2011款 2.0T 手动 尊贵版 柴油,huataida","2011款 2.0T 手动 舒适版 柴油,huataidb","2011款 2.0T 手动 豪华版 柴油,huataidc","2011款 2.0T 自动 尊贵版 柴油,huataidd",
						"2011款 2.0T 自动 舒适版 柴油,huataide","2011款 2.0T 自动 豪华版 柴油,huataidf","2011款 1.8T 手动 尊贵版,huataidg","2011款 1.8T 手动 舒适版,huataidh",
						"2011款 1.8T 手动 豪华版,huataidi","2011款 1.8T 自动 尊贵版,huataidj","2011款 1.8T 自动 舒适版,huataidk","2011款 1.8T 自动 豪华版,huataidl"
						],
				                  
				                  
				                                           
				
				
				"宝利格":["2012款 2.0T 自动 智汇版前驱 柴油,huataiea","2012款 2.0T 手动 爱国版前驱 柴油,huataieb","2012款 1.8T 手动 爱国版前驱 柴油,huataiec",
						"2012款 2.0T 手动 智汇版前驱 柴油,huataied","2012款 1.8T 手动 智汇版前驱,huataiee","2012款 2.0T 自动 智尊版四驱 柴油,huataief",
						"2012款 2.0T 自动 行政版前驱 柴油,huataieg","2012款 2.0T 手动 商务版前驱 柴油,huataieh","2012款 1.8T 手动 商务版前驱,huataiei",
						"2012款 1.8T 自动 智尊版四驱,huataiej","2012款 1.8T 自动 旗舰版四驱,huataiek","2012款 1.8T 自动 行政版前驱,huataiel","2012款 1.8T 自动 商务版前驱,huataiem",
						"2013款 1.8T 自动 旗舰版四驱,huataien","2013款 1.8T 手动 智尊版前驱,huataieo","2013款 1.8T 手动 智汇版前驱,huataiep",
						"2013款 2.0T 自动 智尊版前驱 柴油,huataieq","2013款 1.8T 自动 智尊版前驱,huataier","2013款 1.8T 自动 智汇版前驱,huataies",
						"2013款 2.0T 手动 智尊版前驱 柴油,huataiet","2014款 2.0T 手动 智静版前驱 柴油,huataieu","2014款 1.8T 手动 智享版前驱,huataiev"
						],                  
				                  
				    
				    
				"路盛E70":["2013款 1.5T 自动 尊贵型 柴油,huataifa","2013款 1.5T 手动 尊贵型 柴油,huataifb","2013款 1.5T 手动 舒适型 柴油,huataifc","2013款 2.0 自动 尊贵型,huataifd",
						"2013款 2.0 自动 舒适型,huataife","2013款 2.0 手动 尊贵型,huataiff","2013款 2.0 手动 舒适型,huataifg"
						],			
											
//"黑豹"	
					
					
				"朗杰":["2005款 2.2 手动 尊贵型后驱,heibaoaa"],
					
				"多功能车":["2005款 1.6 手动 后驱,heibaoba","2005款 2.0 手动 后驱2005款 2.2 手动 后驱,heibaobb"],	
					
//"黄海"

					
					
				"傲羚":["2006款 3.2T 手动 皮卡 柴油,huanghaiaa"],		
					
				"傲骏":["2013款 2.4 手动 豪华加长型前驱,huanghaiba"],		
					
				"傲龙CUV":["2005款 3.2T 手动 超豪华型后驱 柴油,huanghaica"],		
					
				"大柴神":["2013款 2.4T 手动 DD1022K豪华型两驱 柴油,huanghaida","2014款 2.8T 手动 豪华型四驱 柴油,huanghaidb",
						"2014款 2.8T 手动 豪华型后驱 柴油,huanghaidc","2014款 2.4T 手动 豪华型两驱 柴油,huanghaidd",
						"2015款 2.2 手动 豪华型两驱,huanghaide"
						],
					
					
				"小柴神":["2009款 2.7T 手动 皮卡 柴油,huanghai2da"
						],	
					
				"挑战者":["2006款 2.0 手动 后驱,huanghaiea","2008款 2.0 手动 标准型后驱,huanghaieb",
						"2008款 2.0 手动 绵阳发动机后驱,huanghaiec","2009款 2.5T 手动 标准型后驱 柴油,huanghaied",
						"2009款 2.4T 手动 后驱 柴油,huanghaiee","2013款 2.2T 手动 后驱 柴油,huanghaief"
						],		
					
				"旗胜F1":["2008款 2.0 手动 后驱,huanghaifa","2008款 2.5T 手动 后驱 柴油,huanghaifb","2009款 2.5T 手动 标准型四驱 柴油,huanghaifc",
						"2009款 2.5T 手动 标准型后驱 柴油,huanghaifd","2009款 2.4 手动 标准型四驱,huanghaife",
						"2009款 2.4 手动 标准型后驱,huanghaiff","2009款 2.4 自动 标准型后驱,huanghaifg","2009款 2.0 手动 标准型后驱,huanghaifh",
						"2012款 2.0T 手动 超豪华型四驱DD6461AC 柴油,huanghaifi","2012款 2.0T 手动 超豪华型后驱DD6461A 柴油,huanghaifj",
						"2012款 2.0T 手动 豪华型后驱DD6461A 柴油,huanghaifk","2012款 2.4 自动 超豪华型后驱DD6470E,huanghaifl",
						"2012款 2.4 手动 超豪华型四驱DD6470E,huanghaifm","2012款 2.4 手动 豪华型四驱DD6470E,huanghaifn",
						"2012款 2.4 手动 超豪华型后驱DD6470E,huanghaifo","2012款 2.0 手动 超豪华型至尊版后驱DD6460D,huanghaifp",
						"2012款 2.0 手动 豪华型精英版后驱DD6460D,huanghaifq"
						],	
					
				"旗胜V3":["2011款 2.0 手动 精英版前驱,huanghaiga","2011款 2.4 手动 豪华版前驱,huanghaigb",
						"2011款 2.0 手动 超豪华型前驱,huanghaigc","2011款 2.4 手动 超豪华型前驱,huanghaigd",
						"2011款 2.0 手动 豪华型前驱,huanghaige","2012款 2.0 手动 升级版前驱,huanghaigf",
						"2014款 1.8T 手动 超豪华前驱 柴油,huanghaigg"
						],	
					
				
				"翱龙CUV":["2006款 2.8T 手动 舒适型后驱 柴油,huanghaiha","2006款 2.4 手动 豪华型后驱,huanghaihb",
						  "2006款 2.2 手动 后驱,huanghaihc"
						],
						
						
				"领航者CUV":["2005款 2.2T 手动 后驱 柴油,huanghaiia","2005款 2.4 手动 豪华型后驱,huanghaiib",
						"2005款 2.4 手动 标准型后驱,huanghaiic","2005款 2.4 手动 豪华型四驱,huanghaiid"
						],			
						
				"黄海N1":["2015款 2.8T 手动 N1S运动版四驱 柴油,huanghaija","2015款 2.8T 手动 N1S至尊版四驱 柴油,huanghaijb",
						"2015款 2.8T 手动 N1S至尊版后驱 柴油,huanghaijc","2015款 2.8T 手动 N1S运动版后驱 柴油,huanghaijd",
						"2015款 2.4 手动 N1S运动版四驱,huanghaije","2015款 2.4 手动 N1S至尊版四驱,huanghaijf",
						"2015款 2.4 手动 N1S至尊版后驱,huanghaijg","2015款 2.4 手动 N1S运动版后驱,huanghaijh",
						"2014款 2.8T 手动 至尊型后驱 柴油,huanghaiji","2014款 2.2 手动 经济型CNG,huanghaijj",
						"2014款 2.2 手动 超值版后驱,huanghaijk","2014款 2.8T 手动 超值型后驱 柴油,huanghaijl",
						"2014款 2.8T 手动 运动型后驱 柴油,huanghaijm","2014款 2.8T 手动 精英型后驱 柴油,huanghaijn",
						"2014款 2.8T 手动 豪华型后驱 柴油,huanghaijo","2014款 2.4 手动 豪华型后驱,huanghaijp",
						"2014款 2.4 手动 精英型后驱,huanghaijq"
						],
				
				"黄海N2":["2015款 2.4 手动 后驱 油气混合,huanghaika","2015款 2.8T 手动 运动版四驱 柴油,huanghaikb",
						"2015款 2.8T 手动 运动版后驱 柴油,huanghaikc","2015款 2.8T 手动 至尊版四驱 柴油,huanghaikd",
						"2015款 2.8T 手动 至尊版后驱 柴油,huanghaike","2015款 2.4 手动 运动版四驱,huanghaikf",
						"2015款 2.4 手动 至尊版四驱,huanghaikg","2015款 2.4 手动 至尊版后驱,huanghaikh",
						"2015款 2.4 手动 运动版后驱,huanghaiki"
						],
									
					
// "哈飞"
				"中意":["2008款 1.05 手动 5-8座,hafeiaa"
						],
					
				"松花江":["2008款 1.0 手动 5座,hafeiba","2008款 1.05 手动 5座,hafeibb",
						"2008款 1.0 手动 1012 5座,hafeibc","2008款 1.05 手动 1012 5座,hafeibd"
						],
				
				"民意":["2012款 1.0 手动 科技基本型8座DA465QA发动机,hafeica","2012款 1.0 手动 科技空调型8座DA465QA发动机,hafeicb"
						],
				
				"赛豹":["2006款 1.6 手动,hafeida","2006款 1.8 手动,hafeidb",
						"2007款 1.8 手动 基本型,hafeidc","2007款 1.8 手动 豪华型,hafeidd"
						],
				
				"赛马":["2005款 1.3 手动 标准型,hafeifa","2005款 1.3 手动 超值实用型,hafeifb","2005款 1.6 手动 标准型,hafeifc",
						"2006款 1.3 手动 超值实用型,hafeifd","2006款 1.6 手动 标准型,hafeife","2006款 1.6 自动 豪华型,hafeiff","2006款 1.3 手动 超值实用版,hafeifg",                 
						"2007款 1.3 手动 超值实用版,hafeifh","2007款 1.3 手动 舒适型,hafeifi","2007款 1.3 手动 标准型,hafeifj","2007款 1.6 手动 豪华型,hafeifk",                  
						"2008款 1.6 自动 舒适型,hafeifl","2008款 1.6 手动 舒适型,hafeifm","2009款 1.5 手动 舒适型,hafeifn","2009款 1.5 手动 豪华型,hafeifo",
						"2009款 1.5 手动 标准型,hafeifp"
						],
				
				"路宝":["2006款 1.0 手动 GZ301超值实用型,hafeiga","2006款 1.0 手动 GZ058标准型,hafeigb","2006款 1.0 手动 GZ033超值实用型,hafeigc",
						"2006款 1.3 手动 GZ008标准型,hafeigd","2006款 1.3 手动 GZ201豪华型,hafeige","2006款 1.1 手动 GZ002标准型,hafeigf",
						"2006款 1.1 手动 GZ00豪华型,hafeigg","2007款 节油π 1.1 手动,hafeigh","2007款 1.0 手动 GZ058标准型,hafeigi",
						"2007款 1.0 手动 GZ033超值实用型,hafeigj","2007款 1.3 手动 GZ008标准型,hafeigk","2007款 1.3 手动 GZ201豪华型,hafeigl",
						"2007款 1.1 手动 GZ201标准型,hafeigm","2008款 1.0 手动 GZ033超值实用型,hafeign","2008款 1.0 手动 GZ058标准型,hafeigo",
						"2008款 1.0 手动 GZ301超值实用型,hafeigp","2008款 1.1 手动 GZ202舒适型,hafeigq","2008款 节油π 1.1 手动 标准型,hafeigr",
						"2008款 节油π 1.1 手动 舒适型,hafeigs","2008款 节油π 1.3 手动 舒适型,hafeigt","2008款 节油π 1.1 自动 舒适型,hafeigu",
						"2008款 节油π 1.1 自动 标准型,hafeigv","2009款 节油π 1.3 手动 舒适型,hafeigw","2009款 节油π 1.1 手动,hafeigx",
						"2010款 灵动 1.0 手动 豪华型,hafeigy","2010款 灵动 1.0 手动 舒适型,hafeigz","2010款 灵动 1.0 手动,hafeigaa","2010款 灵动 1.3 手动 舒适型,hafeigab",
						"2010款 灵动 1.1 手动 舒适型,hafeigac","2010款 灵动 1.1 手动 豪华型,hafeigad","2010款 灵动 1.1 自动 舒适型,hafeigae",
						"2010款 灵动 1.1 自动 豪华型,hafeigaf","2010款 灵动 1.0 手动 豪华型,hafeigag","2010款 节油π 1.0 手动 标准型,hafeigah",
						"2010款 节油π 1.1 手动,hafeigai","2011款 1.0 手动 超值版,hafeigaj","2011款 1.0 手动 舒适型,hafeigak"
						],
				
				"路尊大霸王":["2009款 1.5 手动 尊贵型,hafeiha","2009款 1.5 手动 尊荣型,hafeihb",
						   "2010款 1.5 手动 标准型,hafeihc","2010款 1.5 手动 舒适型,hafeihd",
						   "2010款 1.5 手动 尊贵型,hafeihe","2010款 1.5 手动 尊享型,hafeihf"
						],
				
				
				"路尊小霸王":["2009款 1.1 手动 标准型,hafeiia","2009款 1.1 手动 豪华型,hafeiib",
						   "2009款 1.1 手动 基本型,hafeiic","2010款 1.0 手动 心动版经济型8座,hafeiid",
						   "2010款 1.0 手动 心动版舒适型,hafeiie","2010款 1.1 手动 心动版豪华型,hafeiif"
						],
				
				
				"骏意":["2011款 1.3 手动 基本型5座,hafeija","2011款 1.3 手动 空调型5座,hafeijb"
						],
				
				
// "红旗"
				
				"H7":["2015款 1.8T 自动 豪华型,flagaa","2015款 1.8T 自动 技术型,flagab","2015款 3.0 自动 尊贵型,flagac","2015款 3.0 自动 豪华型,flagad",
					"2015款 2.0T 自动 尊贵型,flagae","2015款 2.0T 自动 豪华型,flagaf","2015款 2.0T 自动 技术型,flagag",
					"2013款 2.5 自动 尊贵型,flagah","2013款 3.0 自动 尊贵型,flagai","2013款 3.0 自动 豪华型,flagaj","2013款 2.0T 自动 尊贵型,flagak",
					"2013款 2.0T 自动 豪华型,flagal","2013款 2.0T 自动 技术型,flagam"
					],
				
				"HQ3":["2006款 3.0 自动 豪华型,flagba","2006款 3.0 自动 商务型,flagbb","2006款 4.3 自动 精英型,flagbc","2007款 3.0 自动 豪华型,flagbd",
					"2007款 3.0 自动 商务型,flagbe","2007款 4.3 自动 精英型,flagbf","2007款 3.0 自动 公务型,flagbg",
					"2008款 3.0 自动 周年纪念版,flagbh"
					],
				
				"L5":["2014款 6.0 自动 帜尊版,flagca"
					],
				
				"世纪星":["2005款 2.0 手动 加长型,flagda","2006款 2.0 手动,flagdb","2006款 1.8 手动,flagdc","2007款 2.0 手动 舒适型,flagdd",
					    "2007款 1.8 手动 标准型,flagde"
					],
				
				"旗舰":["2002款 4.6 自动 基本型,flagea","2002款 4.6 自动 豪华加长型,flageb"
					],
				
				"明仕":["2005款 1.8 手动 商务型,flagfa","2005款 1.8 手动 CNG油气混合,flagfb","2005款 1.8 手动 LPG油气混合,flagfc",
					   "2005款 1.8 手动,flagfd","2006款 1.8 手动 商务型,flagfe","2006款 1.8 手动,flagff","2007款 1.8 手动 商务型,flagfg",
					   "2007款 1.8 手动 LPG油气混合,flagfh","2008款 1.8 手动 奥运之星,flagfh"
					],
				
				"盛世":["2009款 3.0 自动 豪华型,flagga","2009款 3.0 自动 尊贵型,flaggb","2009款 4.3 自动 精英型,flaggc"
					],
				
				"一汽红旗":["2000款 1.8 手动 AE吉星,flagha","2001款 2.0 手动 AE,flaghb"
					],
				
//"华翔"			
				
				"驭虎":["2006款 2.7 手动 后驱,huaxiangaa","2006款 3.0 手动 四驱,huaxiangab","2006款 3.0 自动 后驱,huaxiangac",
					   "2006款 3.0 自动 四驱,huaxiangad","2006款 2.8T 手动 后驱 柴油,huaxiangae","2006款 2.8T 手动 四驱 柴油,huaxiangaf",
					   "2007款 2.7 手动 四驱,huaxiangag",
					   "2007款 2.7 手动 后驱,huaxiangah","2008款 2.8T 手动 四驱 柴油,huaxiangai","2008款 2.8T 手动 后驱 柴油,huaxiangaj"
					],
				
				
				"富奇":["2005款 2.4 手动 6500四驱,huaxiangba","2005款 3.0 手动 6500四驱,huaxiangbb"
					],
				
				
//   "华北"
				
				
				"腾狮RV":["2004款 2.2 手动 标准型后驱,huabeiaa","2004款 2.2 自动 标准型后驱,huabeiab","2004款 2.2 手动 豪华型后驱,huabeiac",
					   "2004款 2.2 自动 豪华型后驱,huabeiad","2004款 2.4 自动 超豪华型 后驱,huabeiae","2004款 2.4 手动 超豪华型 后驱,huabeiaf",
					   "2004款 2.4 手动 超豪华型 四驱,huabeiag","2004款 2.4 自动 超豪华型 四驱,huabeiah"
					],
				
				
				"骏霸":["2004款 2.8T 手动 后驱 柴油,huabeiba"
					],
				
				
				"超赛":["2004款 2.2 手动 Ⅰ型后驱,huabeica","2004款 2.2 手动 Ⅱ型后驱,huabeicb"
					],
				
				
				"醒狮":["2004款 2.2 手动 Ⅰ型后驱,huabeida","2004款 2.2 手动 Ⅱ型后驱,huabeidb"
					],
				
// "海马"
			
				
				"323":["2002款 1.8 手动 豪华型,haimaaa","2002款 1.8 自动 豪华型,haimaab"
					],
				
				
				"海马M3":["2013款 1.5 自动 旗舰型,haimaba","2013款 1.5 手动 旗舰型,haimabb","2013款 1.5 自动 豪华型,haimabc","2013款 1.5 手动 豪华型,haimabd",
					"2013款 1.5 自动 舒适型,haimabe","2013款 1.5 手动 舒适型,haimabf","2013款 1.5 自动 精英型,haimabg","2013款 1.5 手动 精英型,haimabh",
					"2013款 1.5 手动 标准型,haimabi","2013款 1.5 手动 基本型,haimabj","2014款 1.5 手动 七彩领航版,haimabk","2014款 1.5 自动 旗舰型,haimabl",
					"2014款 1.5 自动 舒适型,haimabm","2014款 1.5 自动 精英型,haimabn","2014款 1.5 手动 旗舰型,haimabo",
					"2014款 1.5 手动 豪华型,haimabp","2014款 1.5 手动 舒适型,haimabq","2014款 1.5 手动 精英型,haimabr",
					"2014款 1.5 手动 标准型,haimabs","2014款 1.5 自动 炫彩旗舰型,haimabt","2014款 1.5 手动 炫彩旗舰型,haimabu",
					"2014款 1.5 手动 炫彩豪华型,haimabv","2014款 1.5 手动 炫彩舒适型,haimabw","2014款 1.5 手动 炫彩精英型,haimabx",
					"2014款 1.5 手动 炫彩标准型,haimaby","2014款 1.5 手动 炫彩基本型,haimabz","2015款 1.5 自动 精英天窗版,haimabaa",
					"2015款 1.5 手动 精英天窗版,haimabab","2015款 1.5 手动 标准天窗版,haimabac","2015款 1.5 自动 七彩领航版,haimabad",
					"2015款 1.5 自动 旗舰型,haimabae","2015款 1.5 自动 舒适型,haimabaf","2015款 1.5 自动 精英型,haimabag",
					"2015款 1.5 手动 旗舰型,haimabah","2015款 1.5 手动 豪华型,haimabai","2015款 1.5 手动 舒适型,haimabaj",
					"2015款 1.5 手动 精英型,haimabak","2015款 1.5 手动 标准型,haimabal","2015款 1.5 手动 七彩领航版,haimabam"
					],
				
				
				"海马M6":["2015款 1.5T 手动 乐FUN型,haimaca","2015款 1.5T 手动 驭FUN型,haimacb","2015款 1.5T 手动 智FUN型,haimacc",
					   "2015款 1.5T 手动 睿FUN型,haimacd","2015款 1.5T 自动 驭FUN型,haimace","2015款 1.5T 自动 智FUN型,haimacf",
					   "2015款 1.5T 自动 睿FUN型,haimacg"
					],
				
				
				"海马M8":["2014款 2.0 手动 舒适型,haimada","2014款 2.0 自动 舒适型,haimadb","2014款 2.0 自动 豪华型,haimadc",
					   "2015款 2.0 手动 时尚型,haimadd","2015款 2.0 自动 时尚型,haimade","2015款 1.8T 自动 舒适型,haimadf",
					   "2015款 1.8T 自动 豪华型,haimadg"
					],
				
				
				"海马S5":["2014款 1.6 手动 智臻型前驱,haimaea","2014款 1.6 手动 智尊型前驱,haimaeb","2014款 1.6 手动 智骋型前驱,haimaec",
					"2014款 1.6 手动 智炫型前驱,haimaed","2014款 1.6 手动 智乐型前驱,haimaee","2014款 1.6 手动 智尚型前驱,haimaef",
					"2015款 1.5T 手动 运动智尊型,haimaeg","2015款 1.5T 手动 运动智骋型,haimaeh","2015款 1.5T 手动 运动智炫型,haimaei",
					"2015款 1.5T 手动 运动智尚型,haimaej","2015款 1.6 手动 智臻型前驱,haimaek","2015款 1.6 手动 智尊型前驱,haimael",
					"2015款 1.6 手动 智炫型前驱,haimaem","2015款 1.6 手动 智乐型前驱,haimaen","2015款 1.6 手动 智尚型前驱,haimaeo",
					"2015款 1.5T 自动 智尊型前驱,haimaep","2015款 1.5T 自动 智骋型前驱,haimaeq","2015款 1.5T 自动 智炫型前驱,haimaer"
					],
					
				
				"海马S7":["2013款 2.0 手动 智享型前驱,haimafa","2013款 2.0 自动 智臻型前驱,haimafb","2013款 2.0 手动 智臻型前驱,haimafc",
					"2013款 2.0 自动 智享版前驱,haimafd","2013款 2.0 自动 智尚型前驱,haimafe","2013款 2.0 手动 智尚型前驱,haimaff",
					"2015款 1.8T 自动 纵领型前驱,haimafg","2015款 1.8T 自动 纵聘型前驱,haimafh","2015款 1.8T 自动 纵驰型前驱,haimafi",
					"2015款 2.0 自动 纵驰型前驱,haimafj","2015款 2.0 手动 纵聘型前驱,haimafk","2015款 2.0 手动 纵驰型前驱,haimafl",
					"2015款 2.0 手动 纵享型前驱,haimafm","2015款 2.0 手动 挚爱版前驱,haimafn","2015款 2.0 手动 智臻型前驱,haimafo",
					"2015款 2.0 手动 智享型前驱,haimafp","2015款 2.0 手动 智尚型前驱,haimafq","2015款 2.0 自动 智臻型前驱,haimafr",
					"2015款 2.0 自动 智尚型前驱,haimafs","2015款 2.0 自动 智享版前驱,haimaft"
					],
					
					
				"丘比特":["2010款 1.5 自动 sports,haimaga","2010款 1.5 自动 实用型,haimagb","2010款 1.5 自动 舒适型,haimagc","2010款 1.5 手动 sports,haimagd",
						"2010款 1.5 手动 豪华型,haimage","2010款 1.3 手动 豪华型,haimagf","2010款 1.3 手动 舒适型,haimagg",
						"2010款 1.3 手动 实用型,haimagh","2011款 1.5 自动 DX舒适型,haimagi","2011款 1.3 手动 GL基本型,haimagj","2011款 1.3 手动 GLX舒适型,haimagk",
						"2011款 1.3 手动 C-sport,haimagl","2012款 1.5 自动 灵动型,haimagm","2012款 1.3 手动 灵悦型,haimagn","2012款 1.3 手动 C-sport劲酷型,haimago",
						"2012款 1.3 手动 C-sport炫酷型,haimagp","2013款 1.3 手动 炫酷型,haimagq","2013款 1.3 手动 灵悦型,haimagr","2013款 1.3 手动 劲酷型,haimags",
						"2013款 1.5 自动 灵动型,haimagt","2015款 1.5 自动 清悦版,haimagu","2015款 1.3 手动 清新版,haimagv","2015款 1.3 手动 青葱版,haimagw"
						],	
				
				"普力马":["2001款 1.8 自动 SDX豪华型7座,haimaha","2001款 1.8 手动 GL标准型5座,haimahb","2001款 1.8 手动 GL标准型7座,haimahc",
						"2003款 1.8 自动 DX舒适型5座,haimahd","2003款 1.8 自动 DX舒适型7座,haimahe","2003款 1.8 自动 旗舰型7座,haimahf","2003款 1.8 自动 时尚型7座,haimahg",
						"2003款 1.8 手动 运动型5座,haimahh","2002款 1.8 自动 SDX豪华型5座,haimahi","2002款 1.8 自动 SDX豪华型7座,haimahj",
						"2004款 1.8 手动 GLS舒适型7座,haimahk","2004款 1.8 手动 GL标准型7座,haimahl","2004款 1.8 自动 SDX豪华型5座,haimahm","2004款 1.8 自动 DX舒适型7座,haimahn",
						"2004款 1.8 手动 GLX舒适型5座,haimaho","2004款 1.8 手动 GL标准型5座,haimahp","2004款 1.8 自动 SDX豪华型7座,haimahq","2004款 1.8 自动 DX舒适型5座,haimahr",
						"2005款 1.8 自动 SDX豪华型5座,haimahs","2005款 1.8 自动 DX舒适型5座,haimaht","2006款 1.8 自动 DX舒适型7座,haimahu",
						"2006款 1.8 自动 STD标准型5座,haimahv","2006款 1.8 自动 SDX豪华型5座,haimahw","2006款 1.8 手动 GL标准型5座,haimahx",
						"2006款 1.8 手动 GLS豪华型7座,haimahy","2006款 1.8 手动 GLX舒适型5座,haimahz","2007款 1.8 自动 STD标准型5座,haimahaa",
						"2007款 1.8 手动 GLS豪华型7座,haimahab","2007款 1.8 自动 SDX豪华型5座,haimahac","2007款 1.8 手动 GLX舒适型5座,haimahad","2007款 1.8 手动 GL标准型5座,haimahae",
						"2010款 1.6 自动 舒适型5座,haimahaf","2010款 5座 纯电动,haimahag","2010款 1.6 手动 豪华型7座,haimahah","2010款 1.6 手动 舒适型5座,haimahai",
						"2010款 1.8 自动 豪华型7座,haimahaj","2010款 1.8 自动 舒适型5座,haimahak","2010款 1.6 手动 豪华型5座,haimahal","2010款 1.6 手动 基本型5座,haimaham",
						"2011款 1.8 手动 创想型7座,haimahan","2011款 1.8 自动 尊享型7座,haimahao","2011款 1.6 手动 炫动型5座,haimahap","2011款 1.6 手动 舒适型5座,haimahaq",
						"2011款 1.6 手动 开拓型7座,haimahar","2011款 1.6 手动 创想型7座,haimahas","2011款 1.6 自动 炫酷型5座,haimahat","2011款 1.6 自动 舒适型5座,haimahau",
						"2013款 1.6 手动 开拓版7座,haimahav","2013款 1.6 手动 进取版7座,haimahaw","2013款 1.6 手动 创想版7座,haimahax","2013款 1.8 自动 尊享版7座,haimahay",
						"2013款 1.6 自动 乐享版7座,haimahaz","2014款 1.8 自动 尊享版7座,haimahba","2014款 1.6 自动 乐享版7座,haimahbb","2014款 1.6 手动 开拓版7座,haimahbc",
						"2014款 1.6 手动 创想版7座,haimahbd"
						],

				
				"欢动":["2009款 1.6 自动 豪华型,haimaia","2009款 1.6 手动 豪华型,haimaib","2009款 1.6 手动 舒适型,haimaic",
						"2009款 1.6 手动 标准型,haimaid","2009款 1.8 自动 GLS豪华型,haimaie","2009款 1.8 手动 GLS豪华型,haimaif",
						"2009款 1.8 自动 GLX舒适型,haimaig","2009款 1.8 手动 GLX舒适型,haimaih","2010款 1.6 手动 运动型,haimaii",
						"2010款 1.6 手动 舒适型,haimaij","2010款 1.6 自动 运动型,haimaik","2010款 1.6 自动 舒适型,haimail"
						],
				
				"海福星":["2006款 1.6 手动,haimaja","2007款 1.6 手动 GLX舒适型,haimajb","2007款 1.6 手动 GLS豪华型,haimajc","2007款 1.6 手动 GL标准型,haimajd",
						"2008款 1.6 手动,haimaje","2008款 1.6 手动 CNG油气混合,haimajf","2009款 1.6 手动 GL标准型,haimajg","2009款 1.6 手动 进口发动机,haimajh",
						"2009款 1.6 手动 油气混合,haimaji","2009款 1.6 手动 CNG油气混合,haimajj","2009款 1.6 手动 GLX舒适型,haimajk","2009款 1.6 手动 GX幸福版,haimajl",
						"2010款 1.6 手动 GLX舒适天窗型,haimajm","2010款 1.6 手动 CNG油气混合,haimajn","2010款 1.6 手动,haimajo",
						"2010款 1.5 手动 精英型,haimajp","2010款 1.3 手动 舒适型,haimajq","2010款 1.6 手动 GL标准型,haimajr",
						"2011款 1.3 手动 舒适型,haimajs","2011款 1.3 手动 豪华型,haimajt","2012款 1.5 手动 尊贵型,haimaju","2012款 1.5 手动 精英型,haimajv"
						],

				
				
				"海马3":["2007款 1.8 手动 GL标准型,haimaka","2007款 1.8 手动 GLS豪华型,haimakb","2008款 1.8 手动 运动版,haimakc",
					     "2010款 1.6 手动 GLX天窗型,haimakd","2010款 1.6 自动 SDX豪华型,haimake"
						],
				
				
				"福美来":["2002款 1.8 自动 尊贵型,haimal1","2002款 1.8 自动 精英型,haimal2","2003款 1.8 自动 精英型,haimal3","2003款 1.6 手动 标准型,haimal4",                  
						"2004款 1.6 手动 标准型,haimal5","2004款 1.8 自动 尊荣型,haimal6","2004款 1.6 手动 新贵型,haimal7",
						"2004款 1.8 自动 尊贵型,haimal8","2004款 1.8 自动 精英型,haimal9","2004款 1.8 自动 精英型天窗版,haimal10",
						"2004款 1.6 手动 新悦型,haimal11","2005款 1.6 自动 STD标准型,haimal12","2005款 1.6 自动 SDX豪华型,haimal13","2005款 1.6 手动 GX普通型,haimal14",
						"2005款 1.6 手动 GLS豪华型,haimal15","2005款 1.6 手动 GL标准型,haimal16","2005款 1.6 自动 豪华型,haimal17",
						"2005款 1.6 手动 豪华型,haimal18","2005款 1.6 手动 舒适型,haimal19","2005款 1.6 自动 舒适型,haimal20",
						"2005款 1.6 手动 新锐型,haimal21","2006款 1.6 自动 STD标准型,haimal22","2006款 1.6 自动 SDX豪华型,haimal23","2006款 1.6 手动 GX普通型,haimal24",
						"2006款 1.6 手动 GLS豪华型,haimal25","2006款 1.6 手动 GL标准型,haimal26","2006款 1.6 手动 运动型,haimal27",
						"2007款 1.6 手动 GX普通型,haimal28","2007款 1.6 手动 GLS豪华型,haimal29","2007款 1.6 手动 GL标准型,haimal30",
						"2007款 1.6 手动 DX舒适型,haimal31","2007款 1.6 手动 DX豪华型,haimal32","2007款 1.6 自动 DX豪华型,haimal33",
						"2007款 1.6 自动 STD标准型,haimal34","2007款 1.6 自动 SDX豪华型,haimal35","2007款 1.6 手动 GLX舒适型,haimal36",
						"2007款 1.6 自动 DX舒适型,haimal37","2008款 1.6 自动 SDX豪华型,haimal38","2008款 1.6 自动 DX舒适型,haimal39","2008款 1.6 手动 GLS豪华型,haimal40",
						"2008款 1.6 手动 GLX舒适型,haimal41","2009款 1.6 手动 超值型,haimal42","2009款 1.6 自动 超值型,haimal43",
						"2010款 1.6 自动 舒适型,haimal44","2010款 1.6 手动 豪华型,haimal45","2010款 1.6 手动 舒适型,haimal46",
						"2011款 1.6 手动 宜居版三代,haimal47","2011款 1.6 手动 舒适型三代,haimal48","2011款 1.6 手动 豪华型三代,haimal49",
						"2011款 1.6 自动 宜居版三代,haimal50","2011款 1.6 自动 舒适型三代,haimal51","2011款 1.6 自动 豪华型三代,haimal52",
						"2011款 1.6 自动 8周年纪念型,haimal53","2011款 1.6 手动 8周年纪念型,haimal54",
						"2012款 1.6 自动 CNG油气混合,haimal55","2012款 1.6 自动 旗舰版,haimal56","2012款 1.6 手动 旗舰版,haimal57",
						"2012款 1.6 自动 精英版,haimal58","2012款 1.6 手动 精英版,haimal59","2012款 1.6 自动 风尚版,haimal60","2012款 1.6 手动 风尚版,haimal61",
						"2012款 1.6 自动 旗舰版,haimal62","2012款 1.6 手动 旗舰版,haimal63","2012款 1.6 自动 精英版,haimal64","2012款 1.6 手动 精英版,haimal65",
						"2012款 1.6 自动 风尚版,haimal66","2012款 1.6 手动 风尚版,haimal67","2013款 1.6 自动 旗舰版,haimal68","2013款 1.6 自动 精英版,haimal69",
						"2013款 1.6 手动 旗舰版,haimal70","2013款 1.6 手动 精英版,haimal71","2013款 1.6 手动 风尚版,haimal72","2013款 1.6 手动 冠军版,haimal73",
						"2014款 1.6 手动 时尚版,haimal74","2014款 1.6 手动 豪华版,haimal75","2014款 1.6 手动 冠军版,haimal76",
						"2015款 1.5T 自动 行政尊尚型,haimal77","2015款 1.5T 自动 悦尚型,haimal78","2015款 1.5T 自动 品尚型,haimal79",
						"2015款 1.6 自动 品尚型,haimal80","2015款 1.6 自动 风尚型,haimal81","2015款 1.6 手动 智尚型,haimal82",
						"2015款 1.6 手动 悦尚型,haimal83","2015款 1.6 手动 品尚型,haimal84","2015款 1.6 手动 风尚型,haimal85",
						"2015款 1.6 手动 天窗版,haimal86","2015款 1.6 手动 精英版,haimal87","2015款 1.6 手动 冠军版,haimal88",
						"2015款 1.6 手动 豪华版,haimal89","2015款 1.6 手动 时尚版,haimal90"
						],

				
				"福美来M5":["2014款 1.6 手动 标准型,haimama","2014款 1.6 手动 精英型,haimamb","2014款 1.6 手动 舒适型,haimamc",
						"2014款 1.6 手动 时尚型,haimamd","2014款 1.6 手动 豪华型,haimame","2014款 1.6 手动 尊贵型,haimamf",
						"2014款 1.6 自动 精英型,haimamg","2014款 1.6 自动 舒适型,haimamh","2014款 1.6 自动 尊贵型,haimami",
						"2015款 1.6 手动 挚爱版,haimamj"
						],
				


				"海马骑士":["2010款 2.0 手动 都市精英型,haimaoa","2010款 2.0 手动 智能领航型,haimaob","2011款 2.0 手动 丝路限量纪念版,haimaoc",
						"2012款 2.0 手动 智能领航型,haimaod","2012款 2.0 手动 都市精英型,haimaoe","2012款 2.0 手动 典雅风尚型,haimaof",
						"2012款 2.0 自动 智能领航型,haimaog","2012款 2.0 自动 典雅风尚型,haimaoh","2013款 2.0 手动 经典型,haimaoi",
						"2013款 2.0 自动 智能领航型,haimaoj","2013款 2.0 自动 典雅风尚型,haimaok","2013款 2.0 手动 智能领航型,haimaol",
						"2013款 2.0 手动 都市精英型,haimaom","2013款 2.0 手动 典雅风尚型,haimaon","2013款 2.0 自动 都市精英版,haimaoo"
						],
				
				"爱尚":["2012款 1.0 手动 基本型,haimapa","2012款 1.0 手动 标准型,haimapb","2012款 1.0 手动 舒适型,haimapc",
						"2012款 1.0 手动 豪华型,haimapd","2012款 1.2 手动 豪华型,haimape","2012款 1.2 自动 舒适型,haimapf"
						],
				
				
				"王子":["2009款 1.0 手动,haimaqa","2010款 1.0 手动 经济型,haimaqb","2010款 1.0 手动 标准型,haimaqc",
						"2010款 1.0 手动 实力型,haimaqd","2010款 1.0 手动 进取型,haimaqe","2010款 1.0 手动 精英型,haimaqf",
						"2011款 1.0 手动 豪华型,haimaqg","2011款 1.0 手动 基本型,haimaqh","2011款 1.0 手动 经济型,haimaqi",
						"2011款 1.0 手动 舒适型,haimaqj"
						],
				
				
				"福仕达":["2009款 1.0 手动 超值型改款,haimara","2009款 1.0 手动 实用型,haimarb","2009款 1.0 手动 超值版,haimarc","2009款 1.0 手动 豪华型7座,haimard",
						"2009款 1.0 手动 舒适型7座,haimare","2009款 1.0 手动 标准型7座,haimarf","2009款 1.0 手动 实用型7座,haimarg","2009款 1.0 手动 超值型7座,haimarh",
						"2009款 1.0 手动 舒适型,haimari","2009款 1.0 手动 豪华型,haimarj","2009款 1.0 手动 标准型,haimark","2011款 1.0 手动 豪华型7座,haimarl",
						"2011款 1.0 手动 标准型7座,haimarm","2011款 1.0 手动 超值型,haimarn","2010款 1.0 手动 实用型,haimaro","2012款 1.2 手动 实用型7座,haimarp",
						"2012款 1.0 手动 实用型7座,haimarq","2012款 1.2 手动 豪华型7座,haimarr","2012款 1.2 手动 标准型7座,haimars","2013款 1.2 手动 旗舰型7座,haimart",
						"2013款 1.2 手动 豪华型7座,haimaru","2013款 1.2 手动 舒适型7座,haimarv","2013款 1.2 手动 标准型7座,haimarw","2014款 1.2 手动 标准型5座,haimarx",
						"2014款 1.2 手动 标准型8座,haimary","2014款 1.2 手动 标准型7座,haimarz","2014款 1.2 手动 豪华型5座,haimaraa","2014款 1.2 手动 豪华型8座,haimarab",
						"2014款 1.2 手动 豪华型7座,haimarac"
						],

//"汇众"
				
				
				"伊斯坦纳":["2006款 2.3 手动 超豪华型15座,huizhongaa","2006款 2.3 手动 超豪华型12座,huizhongab","2006款 2.3 手动 豪华型15座,huizhongac",
						"2006款 2.3 手动 超豪华型10座,huizhongad","2006款 2.3 手动 豪华型10座,huizhongae","2006款 2.3 手动 公务版15座,huizhongaf",
						"2006款 2.3 手动 旅游版15座,huizhongag","2006款 2.3 手动 商务版15座,huizhongah","2006款 2.3 手动 豪华型12座,huizhongai","2006款 2.3 手动 舒适型12座,huizhongaj",
						"2009款 2.3 手动 舒适型9座,huizhongak","2009款 2.3 手动 舒适型15座,huizhongal","2009款 2.3 手动 舒适型12座,huizhongam",
						"2009款 2.3 手动 豪华型9座,huizhongan","2009款 2.3 手动 豪华型15座,huizhongao","2009款 2.3 手动 豪华型12座,huizhongap",
						"2009款 2.3 手动 豪华型10座,huizhongaq","2009款 2.3 手动 超豪华型9座,huizhongar","2009款 2.3 手动 超豪华型15座,huizhongas",
						"2009款 2.3 手动 超豪华型12座,huizhongat","2009款 2.3 手动 超豪华型10座,huizhongau","2009款 2.3 手动 公务版15座,huizhongav",
						"2009款 2.3 手动 旅游版15座,huizhongaw","2009款 2.3 手动 商务版15座,huizhongax",
						"2013款 2.3 手动 君享版长轴,huizhongay","2013款 2.3 手动 尊享版长轴,huizhongaz","2013款 2.3 手动 君享版短轴,huizhongaaa",
						"2013款 2.3 手动 尊享版短轴,huizhongaab","2013款 1.8T 手动 精英旅游版长轴,huizhongaac","2013款 1.8T 手动 精英版长轴,huizhongaad",
						"2013款 1.8T 手动 超值版长轴,huizhongaae","2013款 1.8T 手动 精英版短轴,huizhongaaf","2013款 1.8T 手动 精英版短轴9座,huizhongaag",
						"2013款 1.8T 手动 超值版短轴,huizhongaah","2013款 1.8T 手动 超值版短轴9座,huizhongaai"
						],
										
				
				"德驰":["2007款 2.3 手动 LX精英型7座,huizhongba","2007款 2.3 手动 LX精英型9座,huizhongbb","2007款 2.3 手动 SLX精英型6座,huizhongbc",
						"2007款 2.3 手动 SLX精英型7座,huizhongbd","2007款 2.3 手动 SLX精英型9座,huizhongbe"
						],
				
				

//"华普"
							
				
				"M203":["2004款 1.5 手动 标准型,huapuaa","2004款 1.5 手动 舒适型,huapuab","2004款 1.5 手动 基本型,huapuac",
						"2004款 1.5 手动 豪华型,huapuad"
						],
				
				"朗风":["2004款 1.5 手动,huapuba","2004款 1.6 手动,huapubb","2008款 1.6 手动,huapubc"
						],
				
				
				"杰士达美鹿":["2002款 1.3 手动 普通型两厢,huapuca","2002款 1.3 手动 两厢,huapucb"
						],
				
				
				"海域":["2005款 1.5 手动 周年型,huapud05a","2005款 1.8 手动 豪华型,huapud05b","2005款 1.5 手动 标准型,huapud05c","2005款 1.5 手动 舒适型,huapud05d",
						"2006款 1.8 手动 305,huapuda","2006款 1.3 手动 经济型两厢,huapudb","2007款 1.3 手动 经济型两厢,huapudc",
						"2007款 1.3 手动 舒适型两厢,huapudd","2007款 1.3 手动 舒适型三厢,huapude","2008款 1.3 手动 经济型两厢,huapudf",
						"2008款 1.3 手动 舒适型两厢,huapudg"
						],
				
				
				"海尚":["2005款 1.8 手动 豪华型,huapuea","2006款 1.5 手动 时尚型,huapueb","2006款 1.8 手动 尊尚型,huapuec","2006款 1.8 手动 高尚型,huapued",
						"2006款 1.8 手动 时尚型,huapuee","2006款 1.5 手动 舒适型,huapuef","2006款 1.8 手动 LPG油气混合,huapueg","2007款 1.8 手动 LPG油气混合,huapueh",
						"2007款 1.8 手动 舒适型,huapuei","2007款 1.8 手动 豪华型,huapuej","2007款 1.5 手动 舒适型,huapuek",                  
						"2008款 1.5 手动 舒适型,huapuel","2008款 1.8 手动 舒适型,huapuem","2008款 1.8 手动 豪华型,huapuen","2008款 1.5 手动 舒适型 油气混合,huapueo"
						],
				
				"海悦":["2009款 1.5 手动 舒适型,huapufa"
						],
				
				
				"海景":["2009款 1.8 手动 基本型,huapuga","2009款 1.8 手动 标准型,huapugb","2009款 1.8 手动 舒适型,huapugc",
						"2009款 1.8 手动 豪华型,huapugd",
						"2010款 1.5 手动 基本型,huapuge","2010款 1.5 手动 标准型,huapugf","2010款 1.5 手动 舒适型,huapugg"
						],
				
				"海炫":["2006款 1.5 手动 优雅型,huapuha"
						],
						
				"海迅":["2005款 1.5 手动,huapui05a","2006款 1.8 手动,huapuia","2006款 1.5 手动 舒适型两厢,huapuib","2006款 1.8 手动 舒适型两厢,huapuic",
						"2007款 1.8 手动 舒适型两厢,huapuid","2007款 1.5 手动 舒适型两厢,huapuie","2007款 1.8 手动 讯威型,huapuif",
						"2007款 1.8 手动 舒适型三厢,huapuig","2008款 1.5 手动 舒适型两厢,huapuih"
						],		
				
				"海锋":["2006款 1.5 手动 舒适型,huapuja","2006款 1.8 手动 舒适型,huapujb","2008款 1.8 手动 舒适型,huapujc",
						"2008款 1.5 手动 舒适型,huapujd",
						"2009款 1.5 手动 舒适型 CNG油气混合,huapuje","2009款 1.8 手动 舒适型,huapujf"
						],	

				"飓风":["2005款 1.3 手动 标准型,huapuka"
						],
						
				"飙风":["2004款 1.3 手动,huapula","2004款 1.3 手动 二代基本型,huapulb","2004款 1.3 手动 天窗版,huapulc",
						"2004款 1.3 手动 二代标准型,huapuld","2005款 1.3 手动 浅内饰版,huapule"
						],		


// "华阳"	

				"五菱":["2007款 1.1 手动 5座,huayangaa","2008款 1.1 手动 5座,huayangab","2009款 1.1 手动 7座,huayangaac"
						],	
				
								
//"恒天"	
				
				
				"途腾T1":["2012款 2.2 手动 商务版,hengtianaa","2012款 2.8T 手动 商务版 柴油,hengtianab"
						],
				
				
				"途腾T2":["2012款 2.2 手动 商务版皮卡,hengtianba","2012款 2.8T 手动 商务版皮卡后驱 柴油,hengtianbb",
						  "2012款 2.8T 手动 精英版皮卡后驱 柴油,hengtianbc","2012款 2.8T 手动 商务版皮卡四驱 柴油,hengtianbd",
						"2012款 2.4 手动 商务版皮卡后驱,hengtianbe","2012款 2.4 手动 商务版皮卡四驱,hengtianbf"
						],	
				
				"途腾T3":["2013款 2.2 手动 商务版后驱,hengtianca","2013款 2.2 手动 至尊版后驱,hengtiancb",
						  "2013款 2.4 手动 商务版后驱,hengtiancc","2013款 2.4 手动 至尊版后驱,hengtiancd",
						"2013款 2.8T 手动 至尊版后驱 柴油,hengtiance","2013款 2.8T 手动 商务版后驱 柴油,hengtiancf"
						],	
				
//"航天"	
				
				"成功一号":["2012款 1.2 手动 基本型5-7座,hangtianaa","2013款 1.0 手动 基本型5-7座,hangtianab",
						  "2013款 1.0 手动 标准型5-7座,hangtianac","2014款 1.2 手动 V2基本型7座,hangtianad",
						  "2014款 1.2 手动 V1舒适型,hangtianae","2014款 1.2 手动 V1基本型,hangtianaf","2014款 1.2 手动 K2基本型,hangtianag"
						],	
				
	   		
//"海格"
				
				"H5C":["2011款 2.4 手动 基本型6-15座4RB2,haigeaa","2011款 2.4 手动 基本型6-15座4G69S4N,haigeab",
						  "2011款 2.5T 手动 基本型6-15座DK4B1 柴油,haigeac","2013款 2.4 手动 基本型6-15座4G69S4N,haigead",
						  "2013款 2.4 手动 基本型6-15座4RB2,haigeae","2013款 2.5T 手动 基本型6-15座DK4B1 柴油,haigeaf",
						  "2013款 2.7 手动 基本型6-15座G4BA,haigeag"
						],	
				
				"H6C":["2014款 2.7 手动 10座,haigeba","2015款 2.7 手动 10座,haigebb",
						  "2015款 2.8T 手动 10座 柴油,haigebc"
						],	
				
				"H6V":["2015款 10座 纯电动,haigeca","2015款 16座 纯电动,haigecb"
						],
				
				"御骏":["2012款 2.8T 手动 单排后驱 柴油,haigeda","2011款 2.8T 手动 大双排后驱4JB1TC 柴油,haigedb",
						"2013款 2.8T 手动 长轴四驱 柴油,haigedc","2013款 2.8T 手动 长轴两驱 柴油,haigedd","2013款 2.8T 手动 短轴两驱 柴油,haigede",
						"2013款 2.8T 手动 短轴四驱 柴油,haigedf","2013款 2.5T 手动 短轴四驱 柴油,haigedg","2013款 2.4 手动 长轴四驱,haigedh",
						"2013款 2.2 手动 短轴四驱,haigedi","2013款 2.2 手动 长轴四驱,haigedj","2013款 2.2 手动 长轴两驱,haigedk","2013款 2.2 手动 短轴两驱,haigedl",
						"2014款 2.8T 手动 标准版大双4JB1T-4B 柴油,haigedm","2014款 2.8T 手动 时尚版大双4JB1T-4B 柴油,haigedn"
						],

				"龙威":["2013款 2.5T 手动 加长型后驱 柴油,haigeea","2013款 2.5T 手动 标准四驱 柴油,haigeeb",
						"2013款 2.5T 手动 标准后驱 柴油,haigeec","2013款 2.5T 手动 加长型四驱 柴油,haigeed",
						"2013款 2.4 手动 加长型后驱,haigeee","2013款 2.4 手动 标准型后驱,haigeef",
						"2013款 2.8T 手动 长轴型四驱 柴油,haigeeg","2013款 2.4 手动 长轴四驱,haigeeh","2013款 2.4 手动 短轴四驱,haigeei",
						"2014款 2.8T 手动 经典型四驱 柴油,haigeej","2014款 2.8T 手动 经典型后驱 柴油,haigeek",
						"2014款 2.8T 手动 经典型加长四驱 柴油,haigeel","2014款 2.8T 手动 经典型加长后驱 柴油,haigeem",
						"2015款 2.8T 手动 大双标准版4JB1T-4B后驱 柴油,haigeen","2015款 2.8T 自动 大双旗舰版后驱 柴油,haigeeo",
						"2015款 2.8T 自动 大双至尊版后驱 柴油,haigeep","2015款 2.4 自动 大双旗舰版后驱,haigeeq",
						"2015款 2.4 自动 大双至尊版后驱,haigeer","2015款 2.8T 手动 大双领先版后驱 柴油,haigees"
						],
				
   		
//"华颂"
				
				"华颂7":["2015款 2.0T 自动 舒适型7座,huasongaa","2015款 2.0T 自动 豪华型7座,huasongab",
						  "2015款 2.0T 自动 旗舰型7座,huasongac"
						],	
				
				
//"金龙"
				
				"金威":["2015款 2.5T 手动 玉柴创享型,jinlongaa","2015款 2.5T 手动 玉柴长轴创意型,jinlongab","2015款 2.5T 手动 玉柴创意型,jinlongac",
						"2015款 2.5T 手动 厢货长轴型 柴油,jinlongad","2015款 2.5T 手动 厢货柴油,jinlongae","2015款 2.0 手动 厢货长轴型V20,jinlongaf",
						"2015款 2.0 手动 厢货V20,jinlongag","2015款 2.0 手动 厢货长轴型4G21B,jinlongah","2015款 2.0 手动 厢货4G21B,jinlongai",
						"2015款 2.0 手动 小型专用客车,jinlongaj","2015款 2.0 手动 创业升级型,jinlongak","2015款 2.0 手动 创业型,jinlongal",
						"2015款 2.0 手动 实惠升级型,jinlongam","2015款 2.0 手动 实惠型,jinlongan","2015款 2.0 手动 实用超值型,jinlongao"
						],
    
				"金龙海狮":["2005款 2.7T 手动 7座柴油,jinlongba","2005款 2.7T 手动 经典型10座 柴油,jinlongbb","2005款 2.2 手动 JM491Q经典型9座,jinlongbc",
							"2005款 2.4 手动 低顶经典型9座,jinlongbd","2005款 2.2 手动 低顶经典型9座,jinlongbe","2005款 2.4 手动 低顶经典型14座,jinlongbf",
							"2005款 2.2 手动 低顶经典型14座,jinlongbg","2005款 2.4 手动 长轴经典型9座,jinlongbh","2005款 2.4 手动 长轴经典型14座,jinlongbi",
							"2005款 2.2 手动 长轴经典型14座,jinlongbj","2005款 2.2 手动 长轴经典型9座,jinlongbk","2005款 2.4 手动 经典型14座,jinlongbl",
							"2005款 2.2 手动 经典型14座,jinlongbm","2005款 2.4 手动 经典型9座,jinlongbn","2005款 2.2 手动 经典型9座,jinlongbo",
							"2006款 2.2 手动 经典型9座,jinlongbp","2006款 2.2 手动 经典型11座,jinlongbq","2006款 2.4 手动 经典型11座,jinlongbr","2006款 2.4 手动 经典型7座,jinlongbs",
							"2010款 2.0 手动 标准轴高级版V19,jinlongbt","2010款 2.2 手动 标准轴高级版,jinlongbu","2010款 2.8 手动 标准轴高级版,jinlongbv",
							"2013款 2.2 手动 豪华型V22,jinlongbw","2013款 2.0 手动 创享型9座,jinlongbx","2014款 2.2 手动 豪华型V22,jinlongby"
							],
											
				 
//"金程"
				
				"先锋":["2006款 2.4 手动 后驱,jinchengaa"
						],
				
				"横行":["2006款 2.2 手动 后驱,jinchengba"
						],
				
				"赛风":["2002款 2.4 手动 标准型,jinchengca","2002款 2.4 手动 豪华型,jinchengcb"
						],
				
				"金程之星":["2002款 2.0 手动 标准型,jinchengda","2002款 2.0 手动 豪华型,jinchengdb","2002款 2.0 手动 精品型,jinchengdc"
						],
				
				"金程海狮":["2003款 2.2 手动 经济型I,jinchengea","2003款 2.2 手动 经济型II,jinlongeb","2003款 2.2 手动 普及型I,jinlongec",
						"2003款 2.2 手动 标准型II,jinlonged","2003款 2.2 手动 标准型III,jinlongee","2003款 2.2 手动 豪华型I,jinlongef"
						],
				
				"领跑":["2006款 2.2 手动 后驱,jinchengfa"
						],
				
				
				
//"吉利"


		   		"中国龙":["2009款 1.5 手动 标准型,geelyaa","2009款 1.8 手动 标准型,geelyab"
		   				],
		   				
		   				
		   		"优利欧":["2005款 1.3 手动 舒适型,geelyba","2005款 1.3 手动 基本型,geelybb","2005款 1.05 手动 温馨版标准型,geelybc","2005款 1.05 手动 温馨版舒适型,geelybd",
						"2005款 1.05 手动 温馨版基本型,geelybe","2005款 1.05 手动 幸福版舒适型,geelybf","2005款 1.05 手动 幸福版基本型,geelybg",
						"2005款 1.0 手动 温馨版舒适型,geelybh","2005款 1.0 手动 温馨版标准型,geelybi","2005款 1.0 手动 温馨版基本型,geelybj",
						"2005款 1.0 手动 基本型,geelybk","2006款 1.3 手动 舒适型,geelybl","2006款 1.3 手动 基本型,geelybm","2006款 1.0 手动 温馨版基本型,geelybn","2006款 1.0 手动 温馨版标准型,geelybo",
						"2006款 1.3 手动 幸福版舒适型,geelybp","2006款 1.3 手动 幸福版基本型,geelybq",
						"2007款 1.3 手动 舒适型,geelybr","2007款 1.3 手动 基本型,geelybs","2007款 1.05 手动 幸福版舒适型,geelybt",
						"2007款 1.05 手动 幸福版基本型,geelybu","2007款 1.0 手动 温馨版基本型,geelybv","2007款 1.0 手动 温馨版标准型,geelybw"
						],
						
				"博瑞":["2015款 3.5 自动 旗舰型,geelyca","2015款 2.4 自动 舒适型,geelycb","2015款 2.4 自动 标准型,geelycc","2015款 1.8T 自动 旗舰型,geelycd",
						"2015款 1.8T 自动 尊贵型,geelyce","2015款 1.8T 自动 舒适型,geelycf","2015款 1.8T 自动 标准型,geelycg","2015款 1.8T 自动 尊享型礼宾限量版,geelych",
						"2015款 1.8T 自动 典藏型礼宾限量版,geelyci","2016款 2.4 自动 豪华型,geelycj","2016款 1.8T 自动 豪华型,geelyck"
						],
				
				"吉利SC3":["2012款 1.3L 手动 豪华型,geelyda","2012款 1.3L 手动 舒适型,geelydb","2012款 1.3L 手动 标准型,geelydc","2012款 1.3L 手动 基本型,geelydd",
							"2014款 1.3 手动 SC3尊贵型,geelyde","2014款 1.3 手动 SC3进取型,geelydf"
							],
				
				
				"吉利帝豪":["2014款 三厢 1.3T 自动 尊贵型,geelyea","2014款 三厢 1.3T 自动 精英型,geelyeb","2014款 三厢 1.3T 手动 尊贵型,geelyec","2014款 三厢 1.3T 手动 精英型,geelyed",
						"2014款 三厢 1.3T 手动 时尚型,geelyee","2014款 三厢 1.5 自动 精英型,geelyef","2014款 三厢 1.5 手动 精英型,geelyeg","2014款 三厢 1.5 手动 时尚型,geelyeh",
						"2014款 两厢 1.3T 自动 尊贵型,geelyei","2014款 两厢 1.3T 自动 精英型,geelyej","2014款 两厢 1.3T 手动 精英型,geelyek","2014款 两厢 1.3T 手动 时尚型,geelyel",
						"2014款 两厢 1.5 自动 精英型,geelyem","2014款 两厢 1.5 手动 精英型,geelyen","2014款 两厢 1.5 手动 时尚型,geelyeo",
						"2015款 两厢 1.3 自动 向上版,geelyep","2015款 两厢 1.3 手动 向上版,geelyeq","2015款 两厢 1.5 手动 向上版,geelyer","2015款 两厢 1.5 自动 向上版,geelyes",
						"2015款 三厢 1.5 手动 向上版,geelyet","2015款 三厢 1.5 自动 向上版,geelyeu","2015款 三厢 1.3T 手动 向上版,geelyev","2015款 三厢 1.3T 自动 向上版,geelyew",
						"2016款 三厢 EV 尊贵型 纯电动,geelyex","2016款 三厢 EV 精英型 纯电动,geelyey","2016款 三厢 EV 进取型 纯电动,geelyez"
						],
				
				"吉利海景":["2014款 1.5 手动 超越型,geelyfa","2014款 1.5 手动 尊贵型,geelyfb","2014款 1.5 手动 精英型,geelyfc",
						"2015款 1.5 手动 精英型,geelyfd","2015款 1.5 手动 进取型,geelyfe"
						],
				
				"美人豹":["2005款 1.3 手动 自助版,geelyga","2005款 1.5 手动 自助版,geelygb",
						"2006款 1.8 手动,geelygc","2006款 1.6 手动 基本型,geelygd","2006款 1.5 自动 爱她版豪华型,geelyge","2006款 1.5 自动 爱她版标准型,geelygf",
						"2006款 1.5 手动 自助版,geelygg","2007款 1.5 手动 自助版,geelygh","2007款 1.5 手动,geelygi","2007款 1.5 自动,geelygj"
						],
				
				"美日":["2006款 1.0 手动 美日之星舒适型,geelyha","2006款 1.0 手动 美日之星基本型,geelyhb","2006款 1.3 手动 美日之星舒适型,geelyhc",
						"2006款 1.05 手动 美日之星舒适型,geelyhd","2006款 1.3 手动 美日之星基本型,geelyhe","2006款 1.05 手动 美日之星基本型,geelyhf",
						"2015款 1.5 自动 精英型,geelyhg","2015款 1.5 手动 尊贵型,geelyhh","2015款 1.5 手动 精英型,geelyhi","2015款 1.5 手动 进取型,geelyhj"
						],
										
				
				"自由舰":["2005款 1.3 手动 基本型,geelyia","2005款 1.6 手动 基本型,geelyib","2005款 1.6 手动 舒适型,geelyic","2005款 1.3 手动 舒适型,geelyid",                
						"2006款 1.5 手动 基本型,geelyie","2006款 1.6 手动 舒适型,geelyif","2006款 1.6 手动 标准型,geelyig","2006款 1.5 手动 标准型,geelyih",
						"2006款 1.3 手动 基本型精智舰,geelyii","2006款 1.3 手动 舒适型,geelyij","2006款 1.3 手动 标准型,geelyik","2006款 1.5 手动 舒适型,geelyil",
						"2006款 1.5 自动 舒适型,geelyim","2006款 1.6 手动 舒适型精英舰,geelyin","2006款 1.6 手动 标准型精锐舰,geelyio","2006款 1.3 手动 标准型精慧舰,geelyip",
						"2006款 1.3 手动 舒适型精诚舰,geelyiq","2007款 1.5 自动 舒适型,geelyir","2007款 1.3 手动 基本型,geelyis","2007款 1.3 手动 标准型,geelyit",
						"2008款 1.5 手动 舒适型,geelyiu","2008款 1.5 手动 标准型,geelyiv","2008款 1.5 自动 舒适型,geelyiw","2008款 1.5 自动 标准型,geelyix",
						"2008款 1.3 手动 舒适型,geelyiy","2008款 1.3 手动 舒适型 油气混合,geelyiz","2008款 1.3 手动 基本型 油气混合,geelyiaa",
						"2008款 1.3 手动 标准型 油气混合,geelyiab","2008款 1.3 手动 限量版 CNG油气混合,geelyiac",
						"2009款 1.3 手动 舒适型,geelyiad","2009款 1.3 手动 豪华型,geelyiae","2009款 1.3 手动 精致版标准型,geelyiaf","2009款 1.3 手动 基本型,geelyiag",
						"2009款 1.3 手动 标准型,geelyiah","2009款 1.5 自动 精致版,geelyiai","2009款 1.5 手动 基本型,geelyiaj","2009款 1.3 手动 限量版 CNG油气混合,geelyiak",
						"2010款 1.3 手动 财富版,geelyial","2010款 1.5 自动 精致版,geelyiam","2010款 1.5 手动 金钻版,geelyian","2010款 1.5 手动 冠军版,geelyiao",
						"2010款 1.5 自动 金钻版,geelyiap","2010款 1.5 自动 冠军版,geelyiaq","2010款 1.3 手动 精致版,geelyiar","2010款 1.3 手动 经典版,geelyias",
						"2010款 1.3 手动 金钻版,geelyiat","2010款 1.3 手动 冠军版,geelyiau","2010款 1.3 手动 限量版 CNG油气混合,geelyiav",
						"2011款 1.0 手动 精英型,geelyiaw","2011款 1.0 手动 进取型,geelyiax","2011款 1.3 手动,geelyiay","2011款 1.5 手动 运动型,geelyiaz",
						"2012款 1.5 手动 运动型Ⅱ,geelyiba","2012款 1.3 手动 时尚型Ⅲ,geelyibb","2012款 1.3 手动 时尚型Ⅱ,geelyibc","2012款 1.0 手动 精英型Ⅱ,geelyibd",
						"2012款 1.0 手动 进取型Ⅱ,geelyibe","2015款 1.3 手动 幸福型,geelyibf","2015款 1.3 手动 财富型,geelyibg"
						],
				
				"豪情SUV":["2014款 2.4 手动 尊贵型四驱,geelyja","2014款 2.4 自动 尊贵型前驱,geelyjb","2014款 2.4 自动 尊享型前驱,geelyjc",
							"2014款 2.4 自动 豪华型前驱,geelyjd","2014款 2.4 手动 豪华型前驱,geelyje"
							],
				
				"豹风GT":["2006款 1.3 手动 舒适型,geelyka"],
				
				"远景":["2006款 1.8 手动 舒适型,geelyla","2006款 1.8 手动 商务型,geelylb","2006款 1.8 手动 基本型,geelylc","2006款 1.8 手动 标准型,geelyld",
						"2006款 1.8 手动 天窗版,geelyle","2008款 1.8 手动,geelylf","2008款 1.5 手动,geelylg","2008款 1.8 手动 天然气 CNG油气混合,geelylh",	
						"2009款 1.8 手动 油气混合,geelyli","2009款 1.5 手动 天窗型,geelylj","2009款 1.5 手动 尊贵型,geelylk","2009款 1.5 手动 铂金型,geelyll",
						"2010款 1.8 手动 油气混合,geelylm","2010款 1.5 手动 智能导航型,geelyln","2011款 1.5 手动 天窗导航版,geelylo",
						"2012款 1.8 手动 舒适型,geelylp","2012款 1.5 手动 豪华型,geelylq","2012款 1.5 手动 舒适型,geelylr","2012款 1.5 手动 标准型,geelyls",
						"2012款 1.8 手动 标准型,geelylt","2012款 1.5 手动 天窗版,geelylu","2013款 1.5 手动 舒适型,geelylv",
						"2014款 1.8 手动 舒适型 CNG油气混合,geelylw","2014款 1.5 手动 限量超值版,geelylx",
						"2015款 1.5 手动 幸福型,geelyly","2015款 1.3T 手动 幸福型,geelylz","2015款 1.3T 手动 尊贵型,geelylaa","2015款 1.5 手动 精英型,geelylab",
						"2015款 1.5 手动 进取型,geelylac"
						],
				
				
				"金鹰":["2007款 1.5 自动,geelyma",
						"2008款 1.5 手动 舒适型,geelymb","2008款 1.5 手动 豪华型,geelymc","2008款 1.5 手动 标准型,geelymd","2008款 1.5 自动 舒适型,geelyme",
						"2008款 1.5 自动 豪华型,geelymf","2008款 1.5 自动 标准型,geelymg",
						"2009款 1.5 自动 舒适型,geelymh","2009款 1.5 自动 豪华型,geelymi","2009款 1.5 自动 标准型,geelymj",
						"2010款 1.5 手动 CROSS舒适型,geelymk","2010款 1.5 手动 CROSS豪华型,geelyml","2010款 1.5 手动 CROSS标准型,geelymm",
						"2011款 1.5 手动 CROSS标准型,geelymn","2011款 1.5 手动,geelymo"
						],
				
				
				"金刚":["2006款 三厢 1.6 手动 GLX豪华型,geelyna","2006款 三厢 1.6 手动 CX舒适型,geelynb","2006款 三厢 1.6 手动 STDX标准型,geelync",
						"2006款 三厢 1.6 手动 GLX豪华型,geelynd","2006款 三厢 1.6 手动 CX舒适型,geelyne","2006款 三厢 1.5 自动 GLX豪华型,geelynf",
						"2007款 三厢 1.5 手动 STDX标准型,geelyng","2007款 三厢 1.8 手动 GLX豪华型,geelynh","2007款 三厢 1.8 手动 CX舒适型,geelyni",
						"2007款 三厢 1.5 手动 CX舒适型,geelynj","2007款 三厢 1.5 手动 GLX豪华型,geelynk","2008款 三厢 1.5 手动 基本型,geelynl",
						"2008款 三厢 1.5 手动 天窗版,geelynm","2008款 三厢 1.5 手动 油气混合,geelynn","2008款 三厢 1.5 自动 GLX豪华型,geelyno",
						"2009款 三厢 1.5 自动 基本型,geelynp","2009款 三厢 1.5 手动 豪华型二代,geelynq","2009款 三厢 1.5 手动 导航型二代,geelynr",
						"2009款 三厢 1.5 手动 标准型二代,geelyns","2009款 三厢 1.5 手动 油气混合型,geelynt","2009款 三厢 1.5 手动 基本型,geelynu",
						"2009款 三厢 1.5 手动 STDX标准型,geelynv","2010款 三厢 1.5 手动 经典版,geelynw","2010款 三厢 1.5 自动 基本型,geelynx",
						"2010款 三厢 1.5 手动 无敌增配版,geelyny","2010款 三厢 1.5 手动 无敌版,geelynz","2011款 三厢 1.5 手动,geelynaa",
						"2013款 三厢 1.5 手动 尊贵型,geelynab","2013款 三厢 1.5 手动 精英型,geelynac","2013款 三厢 1.5 手动 进取型,geelynad",
						"2014款 三厢 1.5 手动 财富精英型,geelynae","2014款 三厢 1.5 手动 进财富取型,geelynaf","2014款 三厢 1.5 手动 财富尊贵型,geelynag",
						"2014款 三厢 1.5 手动 精英型 CNG油气混合,geelynah","2014款 三厢 1.5 自动 精英型,geelynai","2014款 三厢 1.5 手动 尊贵型,geelynaj",
						"2014款 三厢 1.5 手动 精英型,geelynak","2014款 三厢 1.5 手动 进取型,geelynal","2014款 Cross 1.5 自动 精英型,geelynam",
						"2014款 Cross 1.5 手动 精英型,geelynan","2014款 Cross 1.5 手动 进取型,geelynao", "2014款 两厢  1.5 自动 精英型,geelynap",
						"2014款 两厢 1.5 手动 尊贵型,geelynaq","2014款 两厢 1.5 手动 精英型,geelynar","2014款 两厢 1.5 手动 进取型,geelynas",
						"2015款 三厢 1.5 手动 超悦型,geelynat","2015款 三厢 1.5 手动 财富尊贵型,geelynau","2015款 三厢 1.5 手动 财富精英型,geelynav",
						"2015款 三厢 1.5 手动 财富进取型,geelynaw"
						],
				
				"豪情":["2005款 1.3 手动 303舒适型,geelyoa","2005款 1.05 手动 300A舒适型,geelyob","2005款 1.05 手动 300A技术领先型,geelyoc",
						"2005款 1.05 手动 300A基本型,geelyod","2005款 1.5 自动 SRV舒适型,geelyoe","2005款 1.3 手动 SRV舒适型,geelyof",
						"2005款 1.0 手动 SRV舒适型,geelyog","2005款 1.05 手动 SRV舒适型,geelyoh","2005款 1.3 手动 303基本型,geelyoi",
						"2005款 1.0 手动 亮星基本型,geelyoj","2005款 1.0 手动 203A舒适型,geelyok","2005款 1.0 手动 203A基本型,geelyol",
						"2005款 1.0 手动 203A技术领先型,geelyom","2005款 1.05 手动 203A技术领先型,geelyon","2005款 1.0 手动 亮星舒适型,geelyoo",
						"2006款 1.3 手动,geelyop","2006款 1.0 手动 舒适型,geelyoq","2006款 1.0 手动 基本型,geelyor","2006款 1.05 手动 舒适型,geelyos",
						"2006款 1.05 手动 基本型,geelyot","2006款 1.0 手动 SRV舒适型,geelyou","2006款 1.3 手动 SRV舒适型,geelyov",           
						"2007款 1.3 手动 亮星基本型,geelyow","2007款 1.0 手动 亮星,geelyox","2007款 1.0 手动 亮星2代基本型,geelyoy","2007款 1.05 手动 SRV舒适型,geelyoz",
						"2007款 1.3 手动 SRV舒适型,geelyoaa","2007款 1.0 手动 SRV舒适型,geelyoab"
						],
				
				"雳靓":["2007款 1.8 手动 2门,geelypa"],
				
				
				"GC7":["2012款 1.5 手动 豪华型,geelyqa","2012款 1.5 手动 精英型,geelyqb","2012款 1.5 手动 标准型,geelyqc","2012款 1.5 手动 舒适型,geelyqd",
						"2012款 1.8 自动 豪华型,geelyqe","2012款 1.8 自动 舒适型,geelyqf","2012款 1.8 自动 精英型,geelyqg","2012款 1.8 手动 豪华型,geelyqh",
						"2012款 1.8 手动 精英型,geelyqi","2012款 1.8 手动 舒适型,geelyqj","2013款 1.5 手动 尊贵型,geelyqk","2013款 1.5 手动 舒适型,geelyql",
						"2013款 1.8 自动 尊贵型,geelyqm"
						],
				
				"GX2":["2011款 1.5 手动 尊贵型,geelyra","2011款 1.5 手动 无敌型,geelyrb","2011款 1.5 自动 舒适型,geelyrc","2011款 1.3 手动 舒适型,geelyrd",
						"2011款 1.3 手动 豪华型,geelyre","2012款 1.5 自动 标准型Ⅱ,geelyrf","2012款 1.3 手动 舒适型,geelyrg"
						],
				
				"GX7":["2012款 2.4 自动 旗舰型前驱,geelysa","2012款 2.0 自动 精英型前驱,geelysb","2012款 2.0 自动 舒适型前驱,geelysc","2012款 2.0 手动 精英型前驱,geelysd",
						"2012款 2.0 手动 舒适型前驱,geelyse","2012款 1.8 手动 行政版前驱,geelysf","2012款 1.8 手动 精英型前驱,geelysg","2012款 1.8 手动 舒适型前驱,geelysh",
						"2013款 2.0 自动 进取型前驱,geelysi","2013款 2.0 手动 进取型前驱,geelysj","2013款 2.4 自动 尊贵型前驱,geelysk","2013款 2.0 自动 尊贵型前驱,geelysl",
						"2013款 2.0 自动 精英型前驱,geelysm","2013款 2.0 手动 尊贵型前驱,geelysn","2013款 2.0 手动 精英型前驱,geelyso","2013款 1.8 手动 尊贵型前驱,geelysp",
						"2013款 1.8 手动 精英型前驱,geelysq","2013款 1.8 手动 进取型前驱,geelysr","2013款 2.4 自动 行政版前驱,geelyss","2013款 2.4 自动 舒适型前驱,geelyst",
						"2014款 2.0 手动 运动版精英型,geelysu","2014款 2.0 自动 运动版尊享型,geelysv","2014款 2.0 自动 运动版尊贵型,geelysw","2014款 2.0 自动 运动版豪华型,geelysx",
						"2014款 1.8 手动 电商专供型前驱,geelysy","2014款 1.8 手动 超值天窗版前驱,geelysz","2014款 2.0 自动 超值版前驱,geelysaa","2014款 1.8 手动 超值版前驱,geelysab",
						"2014款 2.4 自动 尊贵型两驱,geelysac","2014款 2.0 自动 尊贵型两驱,geelysad","2014款 2.0 手动 尊贵型两驱,geelysae","2014款 2.0 自动 豪华型两驱,geelysaf",
						"2014款 2.0 手动 进取型两驱,geelysag","2014款 1.8 手动 尊贵型两驱,geelysah","2014款 1.8 手动 精英型两驱,geelysai","2014款 1.8 手动 进取型两驱,geelysaj",
						"2015款 1.8 手动 经典版电商专供型前驱,geelysak","2015款 2.0 自动 运动版豪华升级型前驱,geelysal","2015款 1.8 手动 运动版新精英升级型前驱,geelysam",
						"2015款 1.8 手动 经典版新都市升级型前驱,geelysan","2015款 1.8 手动 经典版新都市型前驱,geelysao","2015款 1.8 手动 精英型前驱,geelysap",
						"2015款 1.8 手动 都市型前驱,geelysaq","2015款 1.8 手动 超值天窗版前驱,geelysar","2015款 1.8 手动 超值版前驱,geelysas","2015款 1.8 手动 电商专供型前驱,geelysat",
						"2015款 2.0 自动 运动版尊贵型,geelysau","2015款 2.0 自动 运动版尊享型,geelysav","2015款 2.0 自动 运动版豪华型前驱,geelysaw","2015款 2.0 手动 运动版精英型,geelysax"
						],
				
				"熊猫":["2008款 1.0 手动,geelyta","2008款 1.3 自动,geelytb",         
						"2009款 1.3 手动 功夫升级版,geelytc","2009款 1.3 自动 无敌版,geelytd","2009款 1.3 自动 乐动版,geelyte","2009款 1.3 自动 灵动版,geelytf",
						"2009款 1.3 手动 无敌版,geelytg","2009款 1.3 手动 灵动版,geelyth","2009款 1.3 手动 乐动版,geelyti","2009款 1.3 手动 功夫版,geelytj",
						"2010款 1.0 手动 标准型,geelytk","2010款 1.5 自动 爱她版尊贵型,geelytl","2010款 1.3 手动 领航版尊贵型,geelytm","2010款 1.3 自动 爱她版尊贵型,geelytn",
						"2010款 1.3 自动 爱她版无敌型,geelyto","2010款 1.0 手动 舒适版,geelytp","2010款 1.0 手动 豪华版,geelytq","2010款 1.5 自动 标准版,geelytr",
						"2010款 1.5 自动 豪华版,geelyts","2010款 1.0 手动 无敌版,geelytt","2010款 1.0 手动 灵动版,geelytu","2010款 1.0 手动 功夫版,geelytv",
						"2010款 1.0 手动 乐动版,geelytw","2011款 1.3 手动 舒适型Ⅱ,geelytx","2011款 1.3 自动 尊贵型,geelyty","2011款 1.5 自动 爱她版尊贵型,geelytz",
						"2011款 1.5 自动 爱她版无敌型,geelytaa","2011款 1.3 手动 无敌版,geelytab","2013款 1.0 手动 精英型,geelytac","2013款 1.0 手动 进取型,geelytad",
						"2015款 1.5 自动 酷趣版,geelytae","2015款 1.0 手动 帅真版,geelytaf","2015款 1.0 手动 萌动版,geelytag"
						],

//金杯				
				"大力神":["2009款 2.2 手动 豪华型长轴后驱,jinbeiaa","2009款 2.2 手动 简配型短轴后驱,jinbeiab",
						"2013款 2.5T 手动 D25短轴两驱 柴油,jinbeiac","2013款 2.2T 手动 短轴四驱 柴油,jinbeiad",
						"2013款 2.2T 手动 长轴四驱 柴油,jinbeiae","2013款 2.2T 手动 长轴两驱 柴油,jinbeiaf",
						"2013款 2.2T 手动 短轴两驱 柴油,jinbeiag","2013款 2.2 手动 长轴后驱,jinbeiah",
						"2013款 2.2 手动 短轴两驱,jinbeiai","2013款 2.5T 手动 D25长轴两驱 柴油,jinbeiaj",
						"2015款 2.2T 手动 精英型两驱 柴油,jinbeiak","2015款 2.2T 手动 豪华型两驱 柴油,jinbeial"
						],
				
				"智尚S30":["2012款 1.5 手动 精英型,jinbeiba","2012款 1.5 手动 时尚型,jinbeibb","2012款 1.5 手动 舒适型,jinbeibc",
							"2013款 1.5 手动 豪华型,jinbeibd","2013款 1.5 手动 精英型,jinbeibe","2013款 1.5 手动 时尚型,jinbeibf",
							"2013款 1.5 手动 舒适型,jinbeibg","2014款 1.5 自动 精英型,jinbeibh","2014款 1.5 自动 豪华型,jinbeibi",
							"2014款 1.5 手动 精英型,jinbeibj","2014款 1.5 手动 豪华型,jinbeibk","2014款 1.5 手动 舒适型,jinbeibl",
							"2015款 1.5 自动 精英型,jinbeibm","2015款 1.5 自动 豪华型,jinbeibn","2015款 1.5 手动 精英型,jinbeibo",
							"2015款 1.5 手动 豪华型,jinbeibp","2015款 1.5 自动 舒适型,jinbeibq","2015款 1.5 手动 舒适型,jinbeibr",
							"2015款 1.5 手动 电商版,jinbeibs"
							],
				
				
				"小海狮X30":["2013款 1.3 手动 标准型5-8座,jinbeica","2013款 1.3 手动 舒适型5-8座,jinbeicb","2014款 1.3 手动 标准型5-8座,jinbeicc",
							"2014款 1.3 手动 舒适型5-8座,jinbeicd","2014款 1.3 手动 货箱版5座,jinbeice","2014款 1.3 手动 CNG,jinbeicf",
							"2015款 1.3 手动 旗舰型7座,jinbeicg"
							],
				
				
				"海星":["2010款 1.0 手动 标准型,jinbeida","2010款 1.0 手动 豪华型,jinbeidb","2010款 1.0 手动 基本型,jinbeidc",
						"2011款 1.0 手动 A9标准型7座,jinbeidd","2011款 1.0 手动 A9豪华版7座,jinbeide","2011款 1.0 手动 特惠版,jinbeidf",
						"2012款 1.0 手动 A9豪华版7座,jinbeidg","2012款 1.0 手动 A9标准型7座,jinbeidh","2014款 1.3 手动 升级标准型后驱,jinbeidi"
						],
				
				"金典":["2009款 2.4T 手动 豪华型短轴 柴油,jinbeiea","2009款 2.4T 手动 豪华型长轴 柴油,jinbeieb",
						"2009款 2.4T 手动 舒适型长轴 柴油,jinbeiec","2009款 2.4T 手动 舒适型短轴 柴油,jinbeied",
						"2009款 2.2 手动 豪华型短轴,jinbeiee","2009款 2.2 手动 豪华型长轴,jinbeief",
						"2009款 2.2 手动 舒适型长轴,jinbeieg","2009款 2.2 手动 舒适型短轴,jinbeieh",
						"2009款 2.1T 手动 豪华型长轴 柴油,jinbeiei","2009款 2.1T 手动 豪华型短轴 柴油,jinbeiej",
						"2009款 2.1T 手动 舒适型长轴 柴油,jinbeiek","2009款 2.1T 手动 舒适型短轴 柴油,jinbeiel",
						"2014款 2.4T 手动 009舒适型短轴 柴油,jinbeiem","2014款 2.4T 手动 009舒适型长轴 柴油,jinbeien",
						"2014款 2.2T 手动 009豪华型短轴后驱 柴油,jinbeieo","2014款 2.4T 手动 舒适型长轴长货箱 柴油,jinbeiep"
						],
				
				"金杯750":["2015款 1.5 手动 舒适型5-7座,jinbeifa","2015款 1.5 手动 豪华型5-7座,jinbeifb","2015款 1.5 手动 尊贵版5-7座,jinbeifc",
						"2015款 1.5 手动 旗舰版5-7座,jinbeifd"
						],
						
				"金杯S50":["2011款 2.0 手动 标准型后驱,jinbeiga","2011款 2.0 手动 舒适型后驱,jinbeigb","2011款 2.4 自动 舒适型后驱,jinbeigc",
						"2011款 2.4 自动 舒适型四驱,jinbeigd","2011款 2.4 手动 标准型后驱,jinbeige","2011款 2.4 手动 舒适型后驱,jinbeigf",
						"2011款 2.4 手动 舒适型四驱,jinbeigg"
						],
						
				"金杯T30":["2015款 1.3 手动 标准型后驱,jinbeiha","2015款 1.3 手动 舒适型后驱,jinbeihb"
						],
				
				"金杯T32":["2015款 1.3 手动 标准型后驱,jinbeiia","2015款 1.3 手动 舒适型后驱,jinbeiib"
						],
				
				"金杯海狮":["2007款 2.0 手动 新动力经典型高顶加长8座,jinbeija","2007款 2.0 手动 新动力经典型高顶加长11-14座,jinbeijb",
							"2007款 2.0 手动 新动力豪华型高顶加长8座,jinbeijc","2007款 2.0 手动 新动力豪华型高顶加长11-14座,jinbeijd",
							"2007款 2.0 手动 新动力标准型高顶加长8座,jinbeije","2007款 2.0 手动 新动力标准型高顶加长11-14座,jinbeijf",
							"2007款 2.0 手动 绿色动力经济型9座,jinbeijg","2007款 2.0 手动 绿色动力经济型11座,jinbeijh",
							"2007款 2.0 手动 动力王舒适型9座,jinbeiji","2007款 2.0 手动 动力王舒适型11座,jinbeijj",
							"2007款 2.0 手动 动力王商务型9座,jinbeijk","2007款 2.0 手动 动力王商务型11座,jinbeijl",
							"2007款 2.0 手动 动力王经典型9座,jinbeijm","2007款 2.0 手动 动力王经典型11座,jinbeijn",
							"2007款 2.0 手动 动力王豪华型9座,jinbeijo","2007款 2.0 手动 动力王豪华型11座,jinbeijp",
							"2007款 2.0 手动 动力王超豪华型9座,jinbeijq","2007款 2.0 手动 动力王超豪华型11座,jinbeijr",
							"2007款 2.0 手动 动力王标准型9座,jinbeijs","2007款 2.0 手动 动力王标准型11座,jinbeijt",
							"2007款 2.0 手动 快运豪华型,jinbeiju","2007款 2.0 手动 快运标准型,jinbeijv",
							"2007款 2.5T 手动 快运超豪华型高顶加长10-12座 柴油,jinbeijw","2007款 2.2 手动 新动力商务型高顶加长11-14座,jinbeijx",
							"2007款 2.2 手动 新动力经典型高顶加长11-14座,jinbeijy","2007款 2.2 手动 新动力豪华型高顶加长11-14座,jinbeijz",
							"2007款 2.2 手动 新动力标准型高顶加长11-14座,jinbeijaa","2007款 2.2 手动 新动力经典型高顶加长8座,jinbeijab",
							"2007款 2.2 手动 新动力标准型高顶加长8座,jinbeijac","2007款 2.2 手动 新动力豪华型高顶加长8座,jinbeijad",
							"2007款 2.5T 手动 快运豪华型高顶加长10-12座 柴油,jinbeijae","2007款 2.5T 手动 快运经典型高顶加长10-12座 柴油,jinbeijaf",
							"2007款 2.5T 手动 快运标准型高顶加长10-12座 柴油,jinbeijag","2007款 2.2 手动 城市快运经典型高顶加长10-14座,jinbeijah",
							"2007款 2.2 手动 城市快运豪华型高顶加长10-14座,jinbeijai","2007款 2.2 手动 城市快运标准型高顶加长10-14座,jinbeijaj",
							"2007款 2.4 手动 动力王旗舰型10座,jinbeijak","2007款 2.4 手动 动力王豪华型10座,jinbeijal",
							"2007款 2.4 手动 动力王旗舰型9座,jinbeijam","2007款 2.4 手动 动力王豪华型9座,jinbeijan",
							"2008款 2.0 手动 新动力快运U豪华型10-11座,jinbeijao","2008款 2.0 手动 新动力快运经典型6-9座,jinbeijap",
							"2008款 2.0 手动 新动力快运经典型10-11座,jinbeijaq","2008款 2.0 手动 新动力快运豪华型6-9座,jinbeijar",
							"2008款 2.0 手动 新动力快运豪华型10-11座,jinbeijas","2008款 2.0 手动 新动力快运标准型6-9座,jinbeijat",
							"2008款 2.5T 手动 快运经典型9座 柴油,jinbeijau","2008款 2.5T 手动 快运经典型10座 柴油,jinbeijav",
							"2008款 2.5T 手动 快运豪华型9座 柴油,jinbeijaw","2008款 2.5T 手动 快运豪华型10座 柴油,jinbeijax",
							"2008款 2.5T 手动 快运超豪华型9座 柴油,jinbeijay","2008款 2.5T 手动 快运超豪华型10座 柴油,jinbeijaz",
							"2008款 2.5T 手动 快运标准型9座 柴油,jinbeijba","2008款 2.5T 手动 快运标准型10座 柴油,jinbeijbb",
							"2008款 2.0 手动 动力王舒适型高顶加长8座,jinbeijbc","2008款 2.0 手动 动力王经典型高顶加长8座,jinbeijbd",
							"2008款 2.0 手动 动力王标准型高顶加长8座,jinbeijbe","2008款 2.0 手动 新动力快运标准型10-11座,jinbeijbf",
							"2009款 2.0 手动 新动力快运标准型10-11座,jinbeijbg","2009款 2.0 手动 第六代畅意标准型9座,jinbeijbh",
							"2009款 2.0 手动 第六代畅意标准型10-11座,jinbeijbi","2009款 2.0 手动 第六代畅行经济型9座,jinbeijbj",
							"2009款 2.0 手动 第六代畅行经济型11座,jinbeijbk","2009款 2.0 手动 第六代畅行经典型9座,jinbeijbl",
							"2009款 2.0 手动 第六代畅行经典型11座,jinbeijbm","2009款 2.2 手动 第六代畅享豪华型9座,jinbeijbn",
							"2009款 2.2 手动 第六代畅享豪华型11座,jinbeijbo","2009款 2.2 手动 第六代畅享标准型9座,jinbeijbp",
							"2009款 2.2 手动 第六代畅享标准型11座,jinbeijbq","2009款 2.0 手动 第六代畅享经典型9座,jinbeijbr",
							"2009款 2.0 手动 第六代畅享经典型11座,jinbeijbs","2009款 2.0 手动 第六代畅享豪华型9座,jinbeijbt",
							"2009款 2.0 手动 第六代畅享豪华型11座,jinbeijbu","2009款 2.0 手动 第六代畅享标准型9座,jinbeijbv",
							"2009款 2.0 手动 第六代畅享标准型11座,jinbeijbw","2009款 2.4 手动 第六代畅领尊贵型11座,jinbeijbx",
							"2009款 2.4 手动 第六代畅领豪华型11座,jinbeijby","2009款 2.4 手动 第六代畅领标准型11座,jinbeijbz",
							"2009款 2.2 手动 新动力高顶加长11-14座,jinbeijca","2009款 2.0 手动 新动力快运豪华型10-11座,jinbeijcb",
							"2009款 2.4 手动 第六代畅领尊贵型9座,jinbeijcc","2009款 2.4 手动 第六代畅领豪华型9座,jinbeijcd",
							"2009款 2.4 手动 第六代畅领标准型9座,jinbeijce","2010款 2.2 手动 新动力高顶加长11-14座,jinbeijcf",
							"2011款 2.0 手动 第五代快运王商务型4G21B 11座,jinbeijcg","2011款 2.0 手动 第五代快运王豪华型4G21B 11座,jinbeijch",
							"2011款 2.0 手动 第五代快运王舒适K型4G21B 11座,jinbeijci","2011款 2.0 手动 第五代快运王舒适Z型4G21B 11座,jinbeijcj",
							"2011款 2.0 手动 第五代快运王标准型4G21B 11座,jinbeijck","2011款 2.0 手动 第五代翔运豪华型4G20D4B 14座,jinbeijcl",
							"2011款 2.0 手动 第五代翔运舒适型4G20D4B 14座,jinbeijcm","2011款 2.0 手动 第五代翔运经典型4G20D4B 14座,jinbeijcn",
							"2011款 2.0 手动 第五代快运王舒适Z型4G21C,jinbeijco","2011款 2.0 手动 第五代快运王舒适K型4G21C,jinbeijcp",
							"2011款 2.0 手动 第五代快运王豪华型4G21C,jinbeijcq","2011款 2.0 手动 第五代快运王标准型4G21C,jinbeijcr",
							"2011款 2.0 手动 第五代快运王标准型4G20B,jinbeijcs","2011款 2.0 手动 第五代快运王豪华型4G20B,jinbeijct",
							"2011款 2.5T 手动 财富标准型 柴油,jinbeijcu","2011款 2.5T 手动 财富经典型 柴油,jinbeijcv",
							"2011款 2.5T 手动 创业标准型 柴油,jinbeijcw","2011款 2.0 手动 第五代快运王标准型4G19,jinbeijcx",
							"2011款 2.0 手动 第五代快运王舒适Z型4G19,jinbeijcy","2011款 2.0 手动 第五代快运王舒适K型4G19,jinbeijcz",
							"2011款 2.0 手动 第五代快运王豪华型4G19,jinbeijda","2011款 2.0 手动 豪华型4G20D4B,jinbeijdb","2011款 2.0 手动 舒适型4G20D4B,jinbeijdc",
							"2011款 2.0 手动 经典型4G20D4B,jinbeijdd","2011款 2.0 手动 动力王超豪华型9座,jinbeijde",
							"2011款 2.0 手动 第六代翔龙经典型V19,jinbeijdf","2011款 2.0 手动 动力王经典型高顶加长10座,jinbeijdg",
							"2011款 2.5T 手动 大海狮旗舰型 柴油,jinbeijdh","2011款 2.5T 手动 大海狮豪华型 柴油,jinbeijdi",
							"2011款 2.4 手动 大海狮旗舰型,jinbeijdj","2011款 2.4 手动 大海狮豪华型,jinbeijdk","2011款 2.4 手动 大海狮标准型,jinbeijdl",
							"2013款 2.0 手动 动力王舒适版9座,jinbeijdm","2013款 2.0 手动 标准型 油气混合,jinbeijdn",
							"2014款 2.0 手动 第五代快运王商务型4G21C,jinbeijdo","2014款 2.0 手动 第五代快运王商务型4G20B,jinbeijdp",
							"2014款 2.0 手动 第五代快运王商务型4G19,jinbeijdq","2014款 2.0 手动 第五代动力王豪华型V19,jinbeijdr",
							"2014款 2.0 手动 第五代动力王乘用型V19,jinbeijds","2014款 2.0 手动 第五代动力王舒适型V19,jinbeijdt",
							"2014款 2.0 手动 第五代动力王经典型V19,jinbeijdu","2014款 2.0 手动 第五代翔运舒适1型4G20C,jinbeijdv",
							"2014款 2.0 手动 舒适型6-10座,jinbeijdw","2014款 2.0 手动 豪华型9座,jinbeijdx",
							"2014款 2.2 手动 豪华型10座,jinbeijdy","2014款 2.2 手动 V22豪华型6座,jinbeijdz",
							"2014款 2.0 手动 V19豪华型6座,jinbeijea","2014款 2.0 手动 V19舒适型6座,jinbeijeb",
							"2015款 2.7 自动 大海狮L丰田动力版旗舰型14座,jinbeijec","2015款 2.7 手动 大海狮L丰田动力版旗舰型14座,jinbeijed"
							],


					"金杯霸道":["2007款 2.0 手动 豪华型后驱,jinbeika","2007款 2.0 手动 超豪华型后驱,jinbeikb","2007款 2.2 手动 豪华型后驱,jinbeikc",
							"2007款 2.4 手动 豪华型后驱,jinbeikd","2007款 2.8T 手动 豪华型后驱 柴油,jinbeike","2007款 2.8T 手动 超豪华型后驱 柴油,jinbeikf",
							"2008款 2.0 手动 豪华型后驱,jinbeikg","2008款 2.8T 手动 豪华型后驱 柴油,jinbeikh"
							],
					
					
					"锐驰":["2005款 2.7T 手动 商务豪华型10-14座 柴油,jinbeil1","2005款 2.7T 手动 商务标准型10-14座 柴油,jinbeil11",
							"2005款 2.7T 手动 商务豪华型6-7座 柴油,jinbeil111","2005款 2.7T 手动 商务标准型6-7座 柴油,jinbeil2","2005款 2.7T 手动 商务豪华型8-9座 柴油,jinbeil22",
							"2005款 2.4 手动 舒适版旗舰型10-14座,jinbeil222","2005款 2.4 手动 舒适版豪华型10-14座,jinbeil3","2005款 2.4 手动 舒适版超豪华型10-14座,jinbeil33",
							"2005款 2.4 手动 舒适版旗舰型6-7座,jinbeil333","2005款 2.4 手动 舒适版超豪华型6-7座,jinbeil4","2005款 2.4 手动 舒适版豪华型6-7座,jinbeil444",
							"2005款 2.4 手动 舒适超版豪华型8-9座,jinbeil5","2005款 2.4 手动 舒适版豪华型8-9座,jinbeil55","2005款 2.4 手动 舒适版旗舰型8-9座,jinbeil555",
							"2005款 2.4 手动 三菱动力旗舰型10-14座,jinbeil6","2005款 2.4 手动 三菱动力豪华型10-14座,jinbeil666","2005款 2.4 手动 三菱动力超豪华型10-14座,jinbeil7",
							"2005款 2.4 手动 三菱动力旗舰型8-9座,jinbeil77","2005款 2.4 手动 三菱动力豪华型8-9座,jinbeil8","2005款 2.4 手动 三菱动力旗舰型6-7座,jinbeil88",
							"2005款 2.4 手动 三菱动力豪华型6-7座,jinbeil9","2005款 2.4 手动 三菱动力超豪华型8-9座,jinbeil99","2005款 2.4 手动 三菱动力超豪华型6-7座,jinbeila11",
							"2005款 2.2 手动 商务型10-14座,jinbeila12","2005款 2.2 手动 旗舰型10-14座,jinbeila13","2005款 2.2 手动 超豪华型10-14座,jinbeila14",
							"2005款 2.2 手动 豪华型10-14座,jinbeila15","2005款 2.2 手动 标准型10-14座,jinbeila16","2005款 2.2 手动 旗舰型8-9座,jinbeila17",
							"2005款 2.2 手动 商务型8-9座,jinbeila18","2005款 2.2 手动 超豪华型8-9座,jinbeila19","2005款 2.2 手动 豪华型8-9座,jinbeila20",
							"2005款 2.2 手动 标准型8-9座,jinbeila21","2005款 2.2 手动 旗舰型6-7座,jinbeila22","2005款 2.2 手动 超豪华型6-7座,jinbeila23",
							"2005款 2.2 手动 商务型6-7座,jinbeila","2005款 2.2 手动 豪华型6-7座,jinbeilb","2005款 2.2 手动 标准型6-7座,jinbeilc",
							"2005款 2.7T 手动 商务标准型8-9座 柴油,jinbeild","2005款 2.4 手动 旗舰型10-14座,jinbeile","2005款 2.4 手动 豪华型10-14座,jinbeilf",
							"2005款 2.4 手动 超豪华型10-14座,jinbeilg","2005款 2.4 手动 旗舰型8-9座,jinbeilh","2005款 2.4 手动 豪华型8-9座,jinbeili",
							"2005款 2.4 手动 超豪华型8-9座,jinbeilj","2005款 2.4 手动 丰田动力旗舰型10-14座,jinbeilk","2005款 2.4 手动 丰田动力豪华型10-14座,jinbeill",
							"2005款 2.4 手动 丰田动力超豪华型10-14座,jinbeilm","2005款 2.4 手动 丰田动力标准型10-14座,jinbeiln","2005款 2.4 手动 丰田动力旗舰型6-7座,jinbeilo",
							"2005款 2.4 手动 丰田动力豪华型6-7座,jinbeilp","2005款 2.4 手动 丰田动力标准型6-7座,jinbeilq","2005款 2.4 手动 丰田动力超豪华型6-7座,jinbeilr",
							"2005款 2.4 手动 丰田动力旗舰型8-9座,jinbeils","2005款 2.4 手动 丰田动力豪华型8-9座,jinbeilt","2005款 2.4 手动 丰田动力超豪华型8-9座,jinbeilu",
							"2005款 2.4 手动 丰田动力标准型8-9座,jinbeilv",
							"2006款 2.4 手动 三菱动力商务型8-9座,jinbeilw","2006款 2.4 手动 三菱动力商务型10-14座,jinbeilx","2006款 2.4 手动 三菱动力精品型8-9座,jinbeily",
							"2006款 2.4 手动 三菱动力精品型10-14座,jinbeilz","2006款 2.4 手动 三菱动力豪华型8-9座,jinbeilaa","2006款 2.4 手动 三菱动力豪华型10-14座,jinbeilab",
							"2006款 2.2 手动 新动力商务型6-9座,jinbeilac","2006款 2.2 手动 新动力商务型10-11座,jinbeilad","2006款 2.2 手动 新动力豪华型6-9座,jinbeilae",
							"2006款 2.2 手动 新动力豪华型10-11座,jinbeilaf","2006款 2.0 手动 动力王商务型8-9座,jinbeilag","2006款 2.0 手动 动力王经典型8-9座,jinbeilah",
							"2006款 2.0 手动 动力王豪华型8-9座,jinbeilai","2006款 2.0 手动 动力王标准型8-9座,jinbeilaj","2006款 2.5T 手动 经典型10-11座 柴油,jinbeilak",
							"2006款 2.5T 手动 豪华型10-11座 柴油,jinbeilal","2006款 2.5T 手动 超豪华型10-11座 柴油,jinbeilam","2006款 2.5T 手动 标准型10-11座 柴油,jinbeilan",
							"2006款 2.5T 手动 经典型6-9座 柴油,jinbeilao","2006款 2.5T 手动 标准型6-9座 柴油,jinbeilap","2006款 2.5T 手动 超豪华型6-9座 柴油,jinbeilaq",
							"2006款 2.5T 手动 豪华型6-9座柴油,jinbeilar","2006款 2.4 手动 三菱动力商务型6-7座,jinbeilas","2006款 2.4 手动 三菱动力精品型6-7座,jinbeilat",
							"2006款 2.4 手动 三菱动力豪华型6-7座,jinbeilau","2006款 2.0 手动 动力王商务型10-11座,jinbeilav","2006款 2.0 手动 动力王经典型10-11座,jinbeilaw",
							"2006款 2.0 手动 动力王豪华型10-11座,jinbeilax","2006款 2.0 手动 动力王标准型10-11座,jinbeilay","2006款 2.0 手动 动力王商务型6-7座,jinbeilaz",
							"2006款 2.0 手动 动力王经典型6-7座,jinbeilba","2006款 2.0 手动 动力王豪华型6-7座,jinbeilbb","2006款 2.2 手动 动力王商务型6-7座,jinbeilbc",
							"2006款 2.2 手动 动力王豪华型6-7座,jinbeilbd","2006款 2.2 手动 动力王超豪华型6-7座,jinbeilbe","2006款 2.2 手动 动力王超豪华型8-9座,jinbeilbf",
							"2006款 2.2 手动 动力王豪华型8-9座,jinbeilbg","2006款 2.2 手动 动力王商务型8-9座,jinbeilbh","2006款 2.2 手动 动力王商务型10-12座,jinbeilbi",
							"2006款 2.2 手动 动力王超豪华型10-12座,jinbeilbj","2006款 2.2 手动 动力王豪华型10-12座,jinbeilbk","2006款 2.0 手动 动力王标准型6-7座,jinbeilbl",
							"2006款 2.4 手动 旗舰型6-7座,jinbeilbm","2006款 2.4 手动 豪华型6-7座,jinbeilbn","2006款 2.4 手动 超豪华型6-7座,jinbeilbo",
							"2006款 2.4 手动 三菱动力旗舰型6-7座 CNG油气混合,jinbeilbp","2006款 2.4 手动 三菱动力豪华型6-7座 CNG油气混合,jinbeilbq",
							"2006款 2.4 手动 三菱动力超豪华型6-7座 CNG油气混合,jinbeilbr","2006款 2.4 手动 三菱动力豪华型8-9座 CNG油气混合,jinbeilbs",
							"2006款 2.4 手动 三菱动力超豪华型8-9座 CNG油气混合,jinbeilbt","2006款 2.4 手动 三菱动力旗舰型8-9座 CNG油气混合,jinbeilbu",
							"2007款 2.2 手动 经济型10-14座,jinbeilbv","2007款 2.2 手动 豪华型10-14座,jinbeilbw","2007款 2.2 手动 标准型10-14座,jinbeilbx",
							"2007款 2.2 手动 经济型6-7座,jinbeilby","2007款 2.2 手动 豪华型6-7座,jinbeilbz","2007款 2.2 手动 标准型6-7座,jinbeilca",
							"2007款 2.2 手动 经济型8-9座,jinbeilcb","2007款 2.2 手动 豪华型8-9座,jinbeilcc","2007款 2.2 手动 标准型8-9座,jinbeilcd","2009款 2.0 手动 动力王6-7座,jinbeilce",
							"2008款 2.4 手动 动力王旗舰型6-7座,jinbeilcf","2008款 2.4 手动 动力王豪华型6-7座,jinbeilcg","2008款 2.4 手动 动力王豪华型10-11座,jinbeilch",
							"2008款 2.4 手动 动力王豪华型8-9座,jinbeilci","2008款 2.4 手动 动力王旗舰型10-11座,jinbeilcj","2008款 2.4 手动 动力王旗舰型8-9座,jinbeilck"
							],

					
					
					
					"阁瑞斯":["2005款 2.4 手动 金色之旅经典型长轴10-11座,jinbeima","2005款 2.4 手动 金色之旅超值版长轴10-11座,jinbeimb",
							"2005款 2.4 手动 金色之旅标准型长轴10-11座,jinbeimc","2005款 2.4 手动 金色之旅经典型长轴8-9座,jinbeimd",
							"2005款 2.4 手动 金色之旅超值版长轴8-9座,jinbeime","2005款 2.7 手动 头等舱豪华型长轴8-9座,jinbeimf",
							"2005款 2.7 自动 商务舱超豪华型短轴6-8座,jinbeimg","2005款 2.7 手动 商务舱超豪华型短轴6-8座,jinbeimh",
							"2005款 2.4 手动 金色之旅超值版本版短轴6-8座,jinbeimi","2005款 2.7 自动 商务舱豪华型短轴6-8座,jinbeimj",
							"2005款 2.7 自动 商务舱经典型短轴6-8座,jinbeimk","2005款 2.7 手动 商务舱标准型短轴6-8座,jinbeiml",
							"2005款 2.4 手动 金色之旅标准型天窗版短轴6-8座,jinbeimm","2005款 2.4 手动 金色之旅豪华型短轴6-8座,jinbeimn",
							"2005款 2.4 手动 金色之旅标准型短轴6-8座,jinbeimo","2005款 2.7 手动 头等舱豪华型短轴6-8座,jinbeimp",
							"2005款 2.7 自动 头等舱超豪华型短轴6-8座,jinbeimq","2005款 2.7 自动 头等舱旗舰型短轴6-8座,jinbeimr",
							"2005款 2.7 手动 商务舱豪华型长轴10-11座,jinbeims","2005款 2.7 手动 商务舱豪华型长轴8-9座,jinbeimt",
							"2005款 2.4 手动 金色之旅超值版短轴6-8座,jinbeimu","2005款 2.4 手动 金色之旅标准型长轴8-9座,jinbeimv",
							"2005款 2.4 手动 金色之旅超值版长轴9座,jinbeimw",
							"2006款 2.4 手动 尊领豪华型短轴6-8座,jinbeimx","2006款 2.4 手动 尊领豪华型长轴6-9座,jinbeimy",
							"2006款 2.4 手动 尊领标准型短轴6-8座,jinbeimz","2006款 2.4 手动 尊领标准型长轴6-9座,jinbeimaa",
							"2006款 2.7 手动 御领豪华型7座,jinbeimab","2006款 2.7 手动 御领豪华精品型7座,jinbeimac",
							"2006款 2.7 手动 御领丰田动力豪华型6-9座,jinbeimad","2006款 2.7 手动 御领丰田动力豪华精品型6-9座,jinbeimae",
							"2006款 2.4 手动 尊领豪华型长轴8-9座,jinbeimaf","2006款 2.4 手动 尊领标准型长轴8-9座,jinbeimag",
							"2006款 2.7 手动 商务舱经典型长轴8-9座,jinbeimah","2006款 2.4 手动 金色之旅标准型长轴6-7座,jinbeimai",
							"2006款 2.7 手动 御领豪华型长轴6-7座,jinbeimaj","2006款 2.4 自动 尊领豪华型短轴6-8座,jinbeimak",
							"2006款 2.7 手动 商务舱经典型短轴6-8座,jinbeimal","2006款 2.7 手动 商务舱豪华型短轴6-8座,jinbeimam",
							"2007款 2.4 手动 尊领豪华型长轴6-9座,jinbeiman","2007款 2.4 手动 尊领豪华型长轴10-11座,jinbeimao",
							"2007款 2.4 手动 尊领标准型长轴6-9座,jinbeimap","2007款 2.4 手动 尊领标准型长轴10-11座,jinbeimaq",
							"2007款 2.0 手动 智领豪华型短轴7座,jinbeimar","2007款 2.0 手动 智领豪华型长轴6-9座,jinbeimas",
							"2007款 2.0 手动 智领豪华型长轴10-11座,jinbeimat","2007款 2.0 手动 智领标准型短轴7座,jinbeimau",
							"2007款 2.0 手动 智领标准型长轴6-9座,jinbeimav","2007款 2.0 手动 智领标准型长轴10-11座,jinbeimaw",
							"2007款 2.7 手动 御领丰田动力豪华型10-11座,jinbeimax","2007款 2.7 手动 御领丰田动力豪华精品型10-11座,jinbeimay",
							"2007款 2.7 自动 御领旗舰型7座短轴,jinbeimaz","2007款 2.4 手动 尊领标准型长轴6-7座,jinbeimba",
							"2007款 2.4 手动 尊领豪华型长轴6-7座,jinbeimbb","2007款 2.4 手动 金色之旅标准型长轴10-11座,jinbeimbc",
							"2007款 2.7 手动 御领豪华型长轴10-11座,jinbeimbd","2007款 2.0 手动 智领基本型短轴7座,jinbeimbe",
							"2008款 2.5T 手动 尊领豪华型6-9座 柴油,jinbeimbf","2008款 2.5T 手动 尊领标准型6-9座 柴油,jinbeimbg",
							"2008款 2.5T 手动 尊领标准型10-11座 柴油,jinbeimbh","2008款 2.5T 手动 尊领豪华型10-11座 柴油,jinbeimbi",
							"2008款 2.7 自动 头等舱豪华型长轴8-9座,jinbeimbj","2008款 2.4 手动 尊领豪华型短轴6-8座,jinbeimbk",
							"2008款 2.4 手动 尊领标准型短轴6-8座,jinbeimbl","2009款 2.7 自动 御领旗舰型短轴7座,jinbeimbm",
							"2009款 2.4 手动 尊领豪华型短轴6-8座,jinbeimbn","2009款 2.4 手动 尊领标准型短轴6-8座,jinbeimbo","2009款 2.7 手动 御领豪华型6-9座,jinbeimbp",
							"2009款 2.7 手动 御领豪华型10-11座,jinbeimbq","2009款 2.7 自动 御领旗舰型6-9座,jinbeimbr",
							"2009款 2.7 自动 御领旗舰型10-11座,jinbeimbs","2009款 2.0 手动 睿翔豪华型,jinbeimbt","2010款 2.4 手动 尊领标准型9座,jinbeimbu",
							"2012款 2.0 手动 智领豪华型全运纪念版11座,jinbeimv","2012款 2.0 手动 智领豪华型全运纪念版9座,jinbeimbw",
							"2012款 2.0 手动 智领豪华型全运纪念版7座,jinbeimbx","2012款 2.0 手动 智领标准型全运纪念版11座,jinbeimby",
							"2012款 2.0 手动 智领标准型全运纪念版9座,jinbeimbz","2012款 2.0 手动 智领标准型全运纪念版7座,jinbeimca",
							"2014款 2.0 手动 标准型短轴7座,jinbeimcb","2014款 2.0 手动 标准型长轴11座,jinbeimcc","2014款 2.0 手动 标准型长轴9座,jinbeimcd",
							"2014款 2.7 自动 旗舰型长轴四门11座,jinbeimce","2014款 2.7 自动 旗舰型长轴五门9座,jinbeimcf",
							"2014款 2.7 自动 旗舰型短轴五门7座,jinbeimcg","2014款 2.7 手动 行政型长轴四门11座,jinbeimch",
							"2014款 2.7 手动 行政型长轴五门9座,jinbeimci","2014款 2.7 手动 行政型短轴五门7座,jinbeimcj",
							"2014款 2.7 手动 豪华型长轴四门11座,jinbeimck","2014款 2.7 手动 豪华型长轴五门9座,jinbeimcl",
							"2014款 2.7 手动 豪华型短轴五门7座,jinbeimcm","2014款 2.7 手动 标准型长轴四门11座,jinbeimcn",
							"2014款 2.7 手动 标准型长轴五门9座,jinbeimco","2014款 2.0 手动 豪华型长轴四门11座,jinbeimcp",
							"2014款 2.0 手动 豪华型长轴五门9座,jinbeimcq","2014款 2.0 手动 豪华型短轴五门7座,jinbeimcr",
							"2014款 2.4 手动 旗舰型长轴四门11座,jinbeimcs","2014款 2.4 手动 旗舰型长轴五门9座,jinbeimct",
							"2014款 2.4 手动 旗舰型短轴五门7座,jinbeimcu","2014款 2.4 手动 商务型长轴四门11座,jinbeimcv",
							"2014款 2.4 手动 商务型长轴五门9座,jinbeimcw"
							],
					
					
					
					"雷龙":["2009款 2.2 手动 经济型短轴,jinbeina","2009款 2.2 手动 经济型长轴,jinbeinb","2009款 2.2 手动 标准型长轴,jinbeinc",
							"2009款 2.2 手动 标准型短轴,jinbeind","2009款 2.2 手动 豪华型短轴,jinbeine","2009款 2.2 手动 豪华型长轴,jinbeinf",
							"2014款 2.2 手动 标厢短轴四驱,jinbeing"
							],
					
					
//"江淮"
					"iEV5":["2015款 iEV5 纯电动,jianghuaiaa","2015款 iEV4豪华型 纯电动,jianghuaiab"
							],
				
				
					"凌铃":["2011款 1.0 手动 单排后驱,jianghuaiba","2011款 1.8T 手动 单排HFC1021KFA后驱 柴油,jianghuaibb",
							"2011款 1.8T 手动 单排HFC1030KFA后驱 柴油,jianghuaibc"
							],
				
					"同悦":["2008款 1.5 手动 尊贵型,jianghuaica","2008款 1.3 手动 标准型,jianghuaicb","2008款 1.5 手动 豪华型 CNG油气混合,jianghuaicc",
							"2008款 1.5 手动 舒适型 CNG油气混合,jianghuaicd","2008款 1.5 手动 豪华型,jianghuaice","2008款 1.5 手动 舒适型,jianghuaicf",
							"2009款 1.3 自动 舒适型RS,jianghuaicg","2009款 1.3 自动 豪华型RS,jianghuaich","2009款 1.5 手动 舒适型RS,jianghuaici",
							"2009款 1.5 手动 豪华型RS,jianghuaicj","2009款 1.3 手动 豪华型RS,jianghuaick","2009款 1.3 手动 舒适型RS,jianghuaicl",
							"2009款 1.3 手动 标准型RS,jianghuaicm","2010款 1.3 手动 舒适型RS,jianghuaicn","2010款 1.3 手动 豪华型RS,jianghuaico",
							"2009款 1.3 手动 舒适型,jianghuaicp","2009款 1.3 手动 豪华型,jianghuaicq","2010款 1.3 自动 豪华型,jianghuaicr", 
							"2010款 1.3 手动 舒适型,jianghuaics","2010款 1.3 手动 豪华型,jianghuaict","2010款 1.3 自动 舒适型,jianghuaicu",
							"2012款 1.3 手动 舒适型,jianghuaicv","2012款 1.3 手动 尚动尊贵型,jianghuaicw","2012款 1.3 手动 尚动豪华型,jianghuaicx",
							"2012款 1.3 手动 豪华型,jianghuaicy","2012款 1.3 手动 标准型,jianghuaicz","2012款 1.3 自动 豪华型,jianghuaicaa",
							"2012款 1.3 手动 豪华型RS,jianghuaicab","2012款 1.3 自动 豪华型RS,jianghuaicac","2012款 1.3 手动 尚动尊贵型RS,jianghuaicad",
							"2012款 1.3 手动 尚动豪华型RS,jianghuaicae","2012款 1.3 手动 豪华型RS,jianghuaicaf","2012款 1.3 手动 舒适型RS,jianghuaicag",
							"2012款 1.3 手动 标准型RS,jianghuaicah","2013款 爱意为三代 纯电动,jianghuaicai"
							],
											
					"和悦":["2010款 1.5 手动 豪华型,jianghuaida","2010款 1.5 手动 舒适型,jianghuaidb","2010款 1.5 手动 标准型,jianghuaidc",
							"2010款 1.8 手动 舒适型5座RS,jianghuaidd","2010款 1.8 手动 标准型5座RS,jianghuaide","2010款 1.8 手动 尊逸型5座RS,jianghuaidf",
							"2010款 1.8 手动 优雅型5座RS,jianghuaidg","2010款 1.8 手动 尊贵型5座RS,jianghuaidh","2010款 1.8 手动 豪华型5座RS,jianghuaidi",
							"2011款 1.8 手动 商务豪华型,jianghuaidj","2011款 1.5 手动 尊贵型,jianghuaidk","2011款 1.5 手动 舒适型,jianghuaidl",
							"2011款 1.5 手动 尊逸型,jianghuaidm","2011款 1.5 手动 智能尊雅型,jianghuaidn","2011款 1.5 手动 运动型,jianghuaido",
							"2011款 1.5 手动 运动天窗型,jianghuaidp","2011款 1.5 手动 优雅型,jianghuaidq","2011款 1.5 手动 优雅型钢轮,jianghuaidr",
							"2011款 1.5 手动 豪华型,jianghuaids","2010款 1.5 手动 优雅型,jianghuaidt","2010款 1.5 手动 尊贵型,jianghuaidu",
							"2011款 1.8 手动 优雅型5座RS,jianghuaidv","2011款 1.8 手动 运动型7座RS,jianghuaidw","2011款 1.8 手动 尊逸型7座RS,jianghuaidx",
							"2011款 1.8 手动 尊逸型5座RS,jianghuaidy","2011款 1.8 手动 运动型5座RS,jianghuaidz","2011款 1.8 手动 运动天窗型5座RS,jianghuaidaa",
							"2011款 1.8 手动 舒适型7座RS,jianghuaidab","2011款 1.8 手动 舒适型5座RS,jianghuaidac","2011款 1.8 手动 豪华型7座RS,jianghuaidad",
							"2011款 1.8 手动 豪华型5座RS,jianghuaidae","2011款 1.5 手动 宜家舒适版RS,jianghuaidaf","2011款 1.5 手动 宜家豪华版RS,jianghuaidag",
							"2011款 1.5 手动 宜家标准版RS,jianghuaidah","2011款 1.5 手动 RS,jianghuaidai","2011款 1.5 手动 标准型,jianghuaidaj",
							"2012款 1.5 手动 商务尊逸型,jianghuaidak","2012款 1.5 手动 商务豪华型,jianghuaidal","2012款 1.5 手动 50周年限量版,jianghuaidam",
							"2012款 爱意为二代 纯电动,jianghuaidan","2012款 1.5 手动 尊贵运动型 油气混合,jianghuaidao","2012款 1.5 手动 尊逸运动型 油气混合,jianghuaidap",
							"2012款 1.5 手动 豪华运动型 油气混合,jianghuaidaq","2012款 1.8 自动 商务尊贵型,jianghuaidar","2012款 1.8 自动 尊逸运动型,jianghuaidas",
							"2012款 1.8 手动 尊逸运动型,jianghuaidat","2012款 1.5 手动 尊贵运动型,jianghuaidau","2012款 1.5 手动 尊逸运动型,jianghuaidav",
							"2012款 1.5 手动 豪华运动型,jianghuaidaw","2012款 1.5 手动 舒适运动型,jianghuaidax","2012款 1.5 手动 尊逸型,jianghuaiday",
							"2012款 1.5 手动 尊贵型,jianghuaidaz","2012款 1.5 手动 舒适型,jianghuaidba","2012款 1.5 手动 豪华型,jianghuaidbb",
							"2012款 1.5 手动 标准型,jianghuaidbc","2012款 1.8 手动 商务豪华型,jianghuaidbd","2012款 1.8 自动 公务豪华型,jianghuaidbe",
							"2012款 1.8 手动 尊逸运动型RS,jianghuaidbf","2012款 1.8 手动 宜商精英版5座RS,jianghuaidbg","2012款 1.5 手动 豪华运动型RS,jianghuaidbh",
							"2012款 1.5 手动 舒适运动型RS,jianghuaidbi","2012款 1.8 自动 公务豪华型RS,jianghuaidbj","2012款 1.8 手动 宜商尊逸版7座RS,jianghuaidbk",
							"2012款 1.8 手动 宜商尊逸版5座RS,jianghuaidbl","2012款 1.8 手动 宜商豪华版5座RS,jianghuaidbm","2012款 1.8 手动 宜商豪华版7座RS,jianghuaidbn",
							"2012款 1.8 手动 宜商舒适版7座RS,jianghuaidbo","2012款 1.8 手动 宜商舒适版5座RS,jianghuaidbp","2012款 1.5 手动 宜家豪华版RS,jianghuaidbq",
							"2012款 1.5 手动 宜家舒适版RS,jianghuaidbr","2012款 1.5 手动 宜家标准版RS,jianghuaidbs","2012款 1.8 自动 5座RS,jianghuaidbt",
							"2013款 1.5 手动 舒适型RS,jianghuaidbu","2013款 1.5 手动 豪华型RS,jianghuaidbv","2013款 1.8 自动 公务遵逸运动型7座RS,jianghuaidbw",
							"2013款 1.8 自动 尊贵型RS,jianghuaidbx","2013款 1.8 自动 豪华型RS,jianghuaidby","2013款 1.8 手动 尊贵型RS,jianghuaidbz",
							"2013款 1.8 手动 豪华型RS,jianghuaidca","2013款 1.8 自动 公务遵逸运动型5座RS,jianghuaidcb","2013款 1.8 手动 豪华运动型5座RS,jianghuaidcc",
							"2013款 1.8 手动 精英运动型5座RS,jianghuaidcd","2013款 爱意为三代 纯电动,jianghuaidce",
							"2014款 爱意为四代 纯电动,jianghuaidcf","2014款 1.8 自动 尊逸型,jianghuaidcg","2014款 1.8 自动 尊贵型,jianghuaidch",
							"2014款 1.5 手动 尊贵型,jianghuaidci","2014款 1.5 手动 尊逸型,jianghuaidcj","2014款 1.5 手动 豪华型,jianghuaidck"
							],
					
					
					"和悦A13Cross":["2012款 1.3 手动 尊贵型,jianghuaiea","2012款 1.3 手动 舒适型,jianghuaieb","2012款 1.3 手动 豪华型,jianghuaiec"				
							],
							
					"和悦A13":["2012款 两厢 1.3 手动 舒适型,jianghuaie2a","2012款 三厢 1.3 手动 尊贵型,jianghuaie3a","2012款 三厢 1.3 手动 舒适型,jianghuaie3b",
								"2012款 三厢 1.3 手动 豪华型,jianghuaie3c"
							],
						
					
					"和悦A30":["2013款 1.5 手动 舒适型,jianghuaifa","2013款 1.5 手动 豪华型,jianghuaifb","2013款 1.5 手动 尊贵型,jianghuaifc",
							"2013款 1.5 自动 豪华型,jinbeinfd","2013款 1.5 自动 尊贵型,jinbeinfe","2013款 1.5 自动 舒适型,jinbeinff",
							"2014款 1.5 手动 尊享型,jinbeinfg"
							],
					
					"宾悦":["2008款 2.0 手动 豪华型,jianghuaiga","2008款 2.0 手动 顶级型,jianghuaigb","2008款 2.4 手动 顶级型,jianghuaigc","2008款 2.4 手动 豪华型,jianghuaigd",
							"2008款 2.4 手动 舒适型,jianghuaige","2008款 2.0 手动 舒适型,jianghuaigf","2009款 2.4 手动 舒适型,jianghuaigg","2009款 2.4 手动 豪华型,jianghuaigh",
							"2009款 2.4 手动 顶级型,jianghuaigi","2009款 2.0 手动 CNG油气混合,jianghuaigj","2009款 2.0 手动 精英型,jianghuaigk","2009款 2.0 自动 尊崇型,jianghuaigl",
							"2009款 2.0 自动 豪华型,jianghuaigm","2009款 2.0 自动 经济型,jianghuaign","2011款 2.0 手动 尊贵型,jianghuaigo","2011款 2.0 手动 豪华型,jianghuaigp",
							"2012款 2.0 自动 尊崇型,jianghuaigq","2012款 2.0 自动 豪华型,jianghuaigr","2012款 2.0 手动 尊贵型,jianghuaigs","2012款 2.0 手动 豪华型,jianghuaigt",
							"2012款 1.8 手动 公务型,jianghuaigu","2012款 1.8 手动 商务型,jianghuaigv","2012款 1.8 手动 豪华型,jianghuaigw","2012款 1.8 手动 精英型,jianghuaigx"
							],
					
					"帅铃T6":["2015款 2.8T 手动 旗舰型 柴油,jianghuaiha","2015款 2.8T 手动 精英型 柴油,jianghuaihb","2015款 2.8T 手动 新锐型 柴油,jianghuaihc",
							"2015款 2.8T 手动 进取型 柴油,jianghuaihd","2015款 2.8T 手动 至尊版 柴油,jianghuaihe","2015款 2.8T 手动 商务版 柴油,jianghuaihf",
							"2015款 2.8T 手动 舒适版 柴油,jianghuaihg","2015款 2.8T 手动 豪华版 柴油,jianghuaihh","2015款 2.0 手动 至尊版,jianghuaihi",
							"2015款 2.0 手动 豪华版,jianghuaihj","2015款 2.0 手动 商务版,jianghuaihk","2015款 2.0 手动 舒适版,jianghuaihl"
							],
					
					
					"悦悦":["2011款 1.3 手动 舒适型,jianghuaiia","2011款 1.3 手动 豪华型,jianghuaiib","2011款 1.3 自动 舒适型,jianghuaiic",
							"2011款 1.3 自动 豪华型,jianghuaiid","2011款 1.0 手动 舒适型,jianghuaiie","2011款 1.0 手动 豪华型,jianghuaiif",
							"2012款 1.0 手动 舒适型,jianghuaiig","2012款 1.0 手动 豪华型,jianghuaiih",
							"2013款 1.0 手动 导航版,jianghuaiii","2013款 1.0 手动 舒适型,jianghuaiij","2013款 1.0 手动 豪华型,jianghuaiik",
							"2014款 1.0 手动 J2豪华型,jianghuaiil","2014款 1.0 手动 J2舒适型,jianghuaiim","2014款 1.0 手动 导航型国际版,jianghuaiin",
							"2014款 1.0 手动 豪华型国际版,jianghuaiio","2014款 1.0 手动 舒适型国际版,jianghuaiip"
							],
					
					
					"星锐":["2011款 2.8T 手动 商务客车舒适版 柴油,jianghuaija","2011款 2.8T 手动 商务客车商务版 柴油,jianghuaijb",
							"2011款 2.8T 手动 商务客车旅行版 柴油,jianghuaijc","2011款 2.8T 手动 商务客车快运版 柴油,jianghuaijd",
							"2013款 1.9T 手动 舒适版T4 柴油,jianghuaije","2013款 1.9T 手动 星快运T4 柴油,jianghuaijf","2013款 2.8T 手动 星快运T5 柴油,jianghuaijg",
							"2013款 2.8T 手动 舒适版T5 柴油,jianghuaijh","2013款 2.8T 手动 旅行版T5 柴油,jianghuaiji","2013款 2.8T 手动 星快运T6,jianghuaijj",
							"2013款 2.8T 手动 旅行版T6,jianghuaijk","2013款 2.8T 手动 舒适版T6,jianghuaijl","2013款 2.8T 手动 4快运版 柴油,jianghuaijm",
							"2013款 2.8T 手动 4舒适版 柴油,jianghuaijn"
							],                 
   					
   					"瑞铃":["2011款 2.5T 手动 舒适型标双后驱 柴油,jianghuaika","2012款 2.8T 手动 越野版HFC4DA12B1 柴油,jianghuaikb",
							"2012款 2.8T 手动 豪华型标双排HFC4DA12B1 柴油,jianghuaikc",
							"2012款 2.8T 手动 舒适型标双排HFC4DA12B1 柴油,jianghuaikd","2012款 2.5T 手动 标准型标双排4D25TC 柴油,jianghuaike",
							"2012款 2.4T 手动 标准型标双排4D25 柴油,jianghuaikf","2012款 2.2 手动 标准型标双排491QE,jianghuaikg",
							"2012款 2.2 手动 越野版491QE,jianghuaikh","2013款 2.4T 手动 舒适型后驱 柴油,jianghuaiki","2013款 2.4T 手动 标准型标双排4D25U 柴油,jianghuaikj",
							"2014款 2.2 手动 V1标准型大双排491QE四驱,jianghuaikk","2014款 2.2 手动 V1标准型大双排491QE后驱,jianghuaikl",
							"2014款 2.8T 手动 V5ABS版标双排4DA12C后驱 柴油,jianghuaikm","2014款 2.5T 手动 标准型大双排后驱 柴油,jianghuaikn",
							"2014款 2.5T 手动 标准型标双排后驱 柴油,jianghuaiko","2014款 2.4T 手动 V1标准型标双排4D25U后驱 柴油,jianghuaikp",
							"2014款 2.4T 手动 V1标准型大双排4D25U后驱 柴油,jianghuaikq","2014款 2.8T 手动 V1标准型大双排4DA12C后驱 柴油,jianghuaikr",
							"2014款 2.8T 手动 V1标准型大双排4DA12C四驱 柴油,jianghuaiks","2014款 1.9T 手动 V5 ABS版标双排4DB12C后驱 柴油,jianghuaikt",
							"2014款 2.4T 手动 标准型大双排 柴油,jianghuaiku","2014款 2.0T 手动 大双排四驱 柴油,jianghuaikv",
							"2014款 2.0T 手动 ABS版大双排后驱 柴油,jianghuaikw"
							],
   					
   					"瑞风":["2005款 2.5T 手动 祥和天窗型7座 柴油,jianghuail1","2005款 2.4 自动 祥和豪华型7座,jianghuail2","2005款 2.5T 手动 加长版7座 柴油,jianghuail3",
							"2005款 2.5T 手动 彩色之旅标准型7-9座 柴油,jianghuail4","2005款 2.5T 手动 彩色之旅标准型11座 柴油,jianghuail5",
							"2005款 2.4 手动 彩色之旅标准型9-11座,jianghuail6",
							"2005款 2.5T 手动 专业型6-8座 柴油,jianghuail7","2005款 2.5T 手动 标准型6-8座 柴油,jianghuail8","2005款 2.5T 手动 国际版10-12座 柴油,jianghuail9",
							"2006款 2.0 手动 穿梭7-12座,jianghuail10","2006款 2.0 手动 穿梭简配型8座,jianghuail11","2006款 2.0 手动 长轴距7座,jianghuail12",
							"2006款 2.0 手动 穿梭5座,jianghuail13","2006款 2.4 手动 10-12座,jianghuail14","2006款 2.4 手动 彩色之旅10-12座,jianghuail15",
							"2006款 2.4 手动 I标准型5-8座,jianghuail16","2006款 2.4 手动 5-8座标准型I,jianghuail17","2006款 2.4 自动 Ⅱ高配版7座,jianghuail18",
							"2006款 2.4 自动 I豪华公务型7座,jianghuail19","2006款 2.4 手动 I豪华公务版7座,jianghuail20","2006款 2.4 手动 I标准公务版7座,jianghuail21",
							"2006款 2.4 手动 Ⅱ手动高配版7座,jianghuaila","2006款 2.0 手动 穿梭标准型7座,jianghuailb","2006款 2.4 手动 加长型7座,jianghuailc",
							"2006款 2.4 手动 彩色之旅标准型7座,jianghuaild","2006款 2.4 手动 彩色之旅标准型11座,jianghuaile","2006款 2.5T 手动 彩色之旅标准型11座 柴油,jianghuailf",
							"2006款 2.4 手动 Ⅱ豪华型7座,jianghuailg","2006款 2.5T 手动 Ⅱ6-8座 柴油,jianghuailh","2006款 2.4 手动 Ⅱ专业型7座,jianghuaili",
							"2006款 2.4 自动 Ⅱ豪华型7座,jianghuailj",
							"2006款 2.0 手动 穿梭加长版7座,jianghuailk","2006款 2.4 手动 标准型8座,jianghuaill","2006款 2.4 手动 标准型7座,jianghuailm",
							"2006款 2.4 自动 豪华型7座,jianghuailn","2006款 2.4 手动 加长版7座,jianghuailo","2006款 2.4 手动 彩色之旅标准型8座,jianghuailp",
							"2007款 2.4 手动 祥和豪华型6-8座,jianghuailq","2007款 2.4 手动 祥和标准型6-8座,jianghuailr","2007款 2.0 手动 穿梭简配8座,jianghuails",
							"2007款 2.0 手动 穿梭5-9座,jianghuailt","2007款 2.4 手动 彩色之旅基本型8座HFC2B,jianghuailu","2007款 2.4 手动 彩色之旅基本型5-8座,jianghuailv",
							"2007款 2.4 手动 彩色之旅基本型9座,jianghuailw","2007款 2.4 手动 彩色之旅标准型9座,jianghuailx","2007款 2.4 手动 I豪华型5-8座,jianghuaily",
							"2007款 2.4 手动 I标准型5-8座,jianghuailz","2007款 2.4 手动 彩色之旅基本型9座HFC2B,jianghuaila1","2007款 2.4 手动 彩色之旅基本型8座HFC4GA1B,jianghuaila2",
							"2007款 2.4 手动 彩色之旅基本型HFC4GA1B,jianghuaila3","2007款 2.4 手动 彩色之旅基本型,jianghuaila4","2007款 2.4 手动 彩色之旅标准型,jianghuaila5",
							"2007款 2.8T 手动 穿梭简配型7-9座 柴油,jianghuaila6","2007款 2.0 手动 5-8座,jianghuaila7","2007款 2.4 手动 彩色之旅标准加长型5-8座,jianghuaila8",
							"2007款 2.4 手动 祥和标准型5-9座,jianghuaila9","2007款 2.4 手动 彩色之旅豪华型11座,jianghuailb1","2007款 2.5T 手动 祥和天窗型7座 柴油,jianghuailb2",
							"2007款 2.4 手动 祥和豪华型7座,jianghuailb3","2007款 2.4 手动 祥和标准型7座,jianghuailb4","2007款 2.4 自动 祥和豪华型7座,jianghuailb5",
							"2007款 2.4 自动 I豪华型7座,jianghuailb6","2007款 2.4 自动 I标准型7座,jianghuailb7","2007款 2.4 手动 I豪华版6-8座,jianghuailb8",
							"2007款 2.4 手动 I标准版6-8座,jianghuailb9","2007款 2.0 手动 I豪华型6-8座,jianghuailc1","2007款 2.0 手动 I标准型6-8座,jianghuailc2",
							"2008款 2.0 手动 I标准型,jianghuailc3","2008款 2.4 手动 彩色之旅舒适版10-11座,jianghuailc4","2008款 2.4 手动 彩色之旅标准版10-11座,jianghuailc5",
							"2008款 2.8T 手动 穿梭长轴舒适型10-11座 柴油,jianghuailc6","2008款 2.8T 手动 穿梭长轴标准型10-11座 柴油,jianghuailc7",
							"2008款 2.0 手动 祥和豪华型7座,jianghuailc8","2008款 2.0 手动 祥和标准型7座,jianghuailc9","2008款 2.8T 手动 穿梭简配型8座 柴油,jianghuailaa",
							"2008款 2.8T 手动 穿梭7-9座 柴油,jianghuailab","2008款 2.0 手动 5-9座,jianghuailac","2008款 2.5T 手动 国际版10-12座 柴油,jianghuailad",
							"2008款 2.0 手动 穿梭5座,jianghuailae","2008款 2.4 手动 彩色之旅10-12座,jianghuailaf","2008款 2.4 手动 I标准型5-8座,jianghuailag",
							"2008款 2.4 手动 彩色之旅简配型5-8座,jianghuailah","2008款 2.4 手动 彩色之旅标配型5-8座,jianghuailai","2008款 2.4 手动 彩色之旅5-9座,jianghuailaj",
							"2008款 2.0 手动 祥和7座,jianghuailak","2009款 2.0 手动 穿梭长轴舒适型5-8座,jianghuailal","2009款 2.0 手动 穿梭长轴标准型5-8座,jianghuailam",
							"2009款 2.0 手动 祥和7座,jianghuailan","2009款 2.0 手动 I 6-8座,jianghuailao","2009款 2.4 手动 彩色之旅标配型5-8座,jianghuailap",
							"2010款 2.4 手动 彩色之旅标配型5-8座,jianghuailaq","2010款 2.0 手动 祥和7座,jianghuailar","2010款 2.0 手动 I 6-8座,jianghuailas",                  
							"2011款 2.4 手动 豪华型7-11座,jianghuailat","2011款 2.4 手动 祥和豪华版6-8座HFC4GA1C,jianghuailau",
							"2011款 2.4 手动 祥和标准版6-8座HFC4GA1C,jianghuailav","2011款 2.0T 手动 祥和短轴豪华版6-8座,jianghuailaw",
							"2011款 2.0T 手动 祥和短轴标准版6-8座,jianghuailax","2011款 2.8T 手动 穿梭长轴舒适型7-9座 柴油,jianghuailay",
							"2011款 2.8T 手动 穿梭长轴标准型7-9座 柴油,jianghuailaz","2011款 2.4 手动 祥和长轴7-9座,jianghuailba",
							"2011款 2.0 手动 穿梭舒适型5-8座HFC4GA3,jianghuailbb","2011款 2.0 手动 穿梭标准型5-8座HFC4GA3,jianghuailbc",
							"2011款 2.4 手动 一家亲豪华版,jianghuailbd","2011款 2.4 手动 一家亲标准版,jianghuailbe","2011款 2.0 手动 一家亲豪华版,jianghuailbf",
							"2011款 2.0 手动 一家亲标准版,jianghuailbg","2011款 2.0T 手动 和畅商务型7座,jianghuailbh","2011款 2.0T 手动 和畅公务型7座,jianghuailbi",
							 "2012款 2.0 手动 穿梭长轴标准型7座,jianghuailbj","2012款 2.0 手动 穿梭长轴舒适版7座,jianghuailbk",
							"2012款 1.9T 手动 穿梭标准型7-8座 柴油,jianghuailbl","2012款 1.9T 手动 穿梭短轴标准型7座 柴油,jianghuailbm",
							"2012款 1.9T 手动 穿梭长轴舒适型11座 柴油,jianghuailbn","2012款 2.0 手动 穿梭短轴时光版7座,jianghuailbo",
							"2012款 2.0 手动 穿梭长轴舒适型11座,jianghuailbp","2012款 2.0 手动 穿梭长轴标准型11座,jianghuailbq",
							"2012款 1.8 手动 穿梭短轴豪华版7座,jianghuailbr","2012款 1.8 手动 穿梭短轴标准版7座,jianghuailbs",
							"2012款 1.9T 手动 祥和长轴11座 柴油,jianghuailbt","2012款 1.9T 手动 穿梭舒适型11座 柴油,jianghuailbu",
							"2012款 1.9T 手动 穿梭标准型11座 柴油,jianghuailbv","2012款 2.0 手动 祥和长轴7-9座,jianghuailbw",
							"2012款 1.9T 手动 祥和长轴7-9座 柴油,jianghuailbx","2012款 1.9T 手动 穿梭舒适型7-9座 柴油,jianghuailby",
							"2012款 1.9T 手动 穿梭标准型7-9座 柴油,jianghuailbz", "2013款 2.4 手动 祥和短轴豪华版6-8座,jianghuailca","2013款 2.4 手动 祥和短轴标准版6-8座,jianghuailcb",
							"2015款 2.0 手动 长轴舒适版7-8座HFC4GA3-3D,jianghuailcc","2015款 2.0 手动 长轴标准型7-8座HFC4GA3-3D,jianghuaicd"
							],
							   					
					"瑞风M3":["2015款 1.6 手动 豪华型,jianghuaima","2015款 1.6 手动 豪华智能型,jianghuaimb","2015款 2.0 手动 豪华型,jianghuaimc",
							"2015款 2.0 手动 豪华智能型,jianghuaimd"
							],
					
					"瑞风M5":["2012款 1.9T 手动 商务版7-8座 柴油,jianghuaina","2012款 1.9T 手动 公务版7-8座 柴油,jianghuainb","2013款 2.0T 自动 商务版,jianghuainc",
							"2013款 2.0T 自动 公务版,jianghuaind","2013款 2.0T 手动 公务版,jianghuaine","2013款 2.0T 手动 商务版,jianghuainf",
							"2014款 1.9T 手动 公务版7-8座 柴油,jianghuaing","2014款 1.9T 手动 商务版7-8座 柴油,jianghuainh",
							],
					
					"瑞风S3":["2014款 1.5 手动 舒适型,jianghuaima","2014款 1.5 手动 豪华型,jianghuaimb","2014款 1.5 手动 豪华智能型,jianghuaimc",
							"2014款 1.5 手动 豪华智能尊享版,jianghuaimd","2014款 1.5 自动 豪华型,jianghuaime","2014款 1.5 自动 豪华智能型,jianghuaimf"
							],
					
					
					"瑞风S5":["2013款 2.0T 手动 领航版前驱,jianghuaipa","2013款 2.0T 手动 尊享版前驱,jianghuaipb","2013款 2.0T 手动 新锐版前驱,jianghuaipc",
							"2013款 1.8T 手动 领航版前驱,jianghuaipd","2013款 1.8T 手动 尊享版前驱,jianghuaipe","2013款 1.8T 手动 新锐版前驱,jianghuaipf",
							"2013款 2.0 手动 尊贵版前驱,jianghuaipg","2013款 2.0 手动 精英版前驱,jianghuaiph","2014款 2.0T 自动 尊享版,jianghuaipi",
							"2014款 1.5T 手动 尊享版,jianghuaipj","2014款 2.0T 手动 领航版前驱,jianghuaipk",
							"2014款 2.0T 手动 尊享版前驱,jianghuaipl","2014款 2.0T 手动 新锐版前驱,jianghuaipm","2014款 2.0 手动 尊贵版前驱,jianghuaipn",
							"2015款 2.0T 自动 豪华型前驱,jianghuaipo","2015款 1.5T 手动 豪华运动版前驱,jianghuaipp","2015款 1.5T 自动 豪华智能型前驱,jianghuaipq",
							"2015款 1.5T 自动 豪华型前驱,jianghuaipr","2015款 1.5T 手动 豪华智能型前驱,jianghuaips","2015款 1.5T 自动 舒适型前驱,jianghuaipt",
							"2015款 1.5T 手动 豪华型前驱,jianghuaipu","2015款 1.5T 手动 舒适型前驱,jianghuaipv","2015款 2.0 手动 豪华型前驱,jianghuaipw",
							"2015款 2.0 手动 舒适型前驱,jianghuaipx"
							],
					
					"瑞鹰":["2006款 2.4 手动 四驱,jianghuaiqa","2007款 2.4 手动 四驱,jianghuaiqb","2007款 2.4 手动 前驱,jianghuaiqc","2007款 2.0 手动 前驱,jianghuaiqd",
							"2008款 2.4 手动 四驱,jianghuaiqe","2008款 2.4 手动 前驱,jianghuaiqf","2008款 2.0 手动 前驱,jianghuaiqg","2008款 1.9T 手动 豪华型前驱 柴油,jianghuaiqh",
							"2008款 1.9T 手动 前驱 柴油,jianghuaiqi","2009款 2.0T 手动 都市版前驱,jianghuaiqj","2009款 2.4 手动 豪华运动型四驱,jianghuaiqk",
							"2009款 2.4 手动 豪华运动型前驱,jianghuaiql","2009款 2.0 手动 豪华舒适型前驱,jianghuaiqm","2009款 2.4 手动 四驱,jianghuaiqn",
							"2010款 1.9T 手动 豪华型前驱 柴油,jianghuaiqo","2010款 2.4 手动 豪华运动型前驱,jianghuaiqp","2010款 2.0 手动 精英版前驱,jianghuaiqq",
							"2010款 2.0 手动 都市型前驱,jianghuaiqr","2010款 1.9T 手动 前驱 柴油,jianghuaiqs",
							"2011款 2.0T 手动 进取型前驱,jianghuaiqt","2011款 2.4 手动 豪华版四驱,jianghuaiqu","2011款 2.0T 手动 豪华版前驱,jianghuaiqv",
							"2011款 2.0T 手动 经典版前驱,jianghuaiqw","2011款 2.0 手动 豪华版前驱,jianghuaiqx","2011款 2.0 手动 经典版前驱,jianghuaiqy",
							"2011款 2.0 手动 进取型前驱,jianghuaiqz","2011款 2.0T 自动 豪华版前驱,jianghuaiqaa","2011款 2.4 手动 公务版四驱,jianghuaiqab",
							"2011款 2.0T 手动 越野商务版四驱,jianghuaiqac","2011款 2.0T 手动 越野版四驱,jianghuaiqad",
							"2012款 1.9T 手动 柴油版前驱,jianghuaiqae","2012款 1.9T 手动 豪华型前驱 柴油,jianghuaiqaf","2012款 1.9T 手动 经典型前驱 柴油,jianghuaiqag",
							"2012款 2.4 手动 经典型前驱,jianghuaiqah","2012款 2.0 手动 进取型前驱,jianghuaiqai","2012款 2.0 手动 经典型前驱,jianghuaiqaj",
							"2013款 2.0T 手动 进取型前驱,jianghuaiqak","2013款 1.8 手动 经典型前驱,jianghuaiqal","2013款 2.4 手动 豪华版四驱,jianghuaiqam",
							"2013款 2.4 手动 公务版四驱,jianghuaiqan","2013款 2.4 手动 经典型前驱,jianghuaiqao","2013款 2.0T 手动 公务版四驱,jianghuaiqap",
							"2013款 2.0T 手动 豪华版四驱,jianghuaiqaq","2013款 2.0T 自动 豪华版前驱,jianghuaiqar","2013款 2.0T 手动 豪华型前驱,jianghuaiqas",
							"2013款 2.0 手动 经典型前驱,jianghuaiqat","2013款 2.0 手动 进取型前驱,jianghuaiqau","2013款 2.0T 手动 经典型前驱,jianghuaiqav",
							"2013款 2.0 手动 豪华型前驱,jianghuaiqaw"
							],


				
//"吉奥"
				
				"E美":["2014款 1.6 手动 舒适版,jiaoaa","2014款 1.6 手动 豪华版,jiaoab","2014款 1.6 自动 尊贵版,jiaoac"
					  ],
					
				
				"GP150":["2015款 2.5T 手动 尊享型四驱 柴油,jiaoba","2015款 2.5T 手动 尊享型后驱 柴油,jiaobb","2015款 2.5T 手动 尊贵型四驱 柴油,jiaobc",
						"2015款 2.5T 手动 尊贵型后驱 柴油,jiaobd","2015款 2.5T 手动 至尊型四驱 柴油,jiaobe","2015款 2.5T 手动 至尊型后驱 柴油,jiaobf",
						"2015款 2.7 手动 尊享型四驱,jiaobg","2015款 2.7 手动 尊贵型四驱,jiaobh","2015款 2.7 手动 至尊型四驱,jiaobi",
						"2015款 2.7 手动 至尊型后驱,jiaobj","2015款 2.7 手动 尊贵型后驱,jiaobk","2015款 2.7 手动 尊享型后驱,jiaobl"
						],
				
				"GS50":["2005款 2.2 手动 舒适型后驱,jiaoca","2005款 2.7T 手动 舒适型后驱 柴油,jiaocb","2005款 2.8T 手动 舒适型后驱 柴油,jiaocc"
					  ],
				
				"GX6":["2005款 2.4 手动 后驱,jiaoda","2005款 2.2 手动 后驱,jiaodb","2006款 2.8T 手动 后驱 柴油,jiaodc",
						"2014款 2.4 手动 精英版后驱,jiaodd","2015款 2.4 手动 精英版后驱,jiaode","2015款 2.4 手动 精英版四驱,jiaodf",
						"2015款 2.4 手动 旗舰版后驱,jiaodg","2015款 2.4 手动 旗舰版四驱,jiaodh"
						],
				
				"伊美":["2006款 1.1 手动 经济型,jiaoea","2006款 1.1 手动 舒适型,jiaoeb","2006款 1.1 手动 标准型,jiaoec",
						"2007款 1.1 手动 标准型,jiaoed","2007款 1.1 手动 经济型,jiaoee","2007款 1.1 手动 舒适型,jiaoef",
						"2008款 1.1 手动,jiaoeg"
						],
				
				"吉奥凯旋":["2005款 2.2 手动 后驱,jiaofa"
						],
				
				"凯睿":["2007款 2.0 手动 豪华后驱,jiaoga","2007款 2.4 手动 豪华四驱,jiaogb","2007款 2.0 手动 豪华四驱,jiaogc",
						"2009款 2.0 手动 标准版后驱,jiaogd",
					  ],
				
				"奥腾":["2007款 2.8T 手动 豪华型皮卡 柴油,jiaoha"
						],
						
				"奥轩G3":["2011款 2.0 手动 前驱,jiaoia"
						],		
						
				"奥轩G5":["2011款 2.4 自动 至尊版后驱,jiaoja","2011款 2.4 自动 精英版后驱,jiaojb","2011款 2.4 手动 至尊版后驱,jiaojc",
						"2011款 2.4 手动 精英版后驱,jiaojd","2011款 2.4 手动 豪华版后驱,jiaoje",
						"2012款 2.4 手动 豪华版四驱,jiaojf","2012款 2.4 自动 精英版后驱,jiaojg","2012款 2.4 自动 至尊版后驱,jiaojh",
						"2012款 2.0 手动 智选版后驱,jiaoji","2012款 2.4 自动 至尊版四驱,jiaojj","2012款 2.4 自动 精英版四驱,jiaojk",
						"2012款 2.5T 手动 至尊版后驱 柴油,jiaojl","2012款 2.5T 手动 豪华版后驱 柴油,jiaojm","2012款 2.5T 手动 精英版后驱 柴油,jiaojn",
						"2012款 2.4 手动 至尊版四驱,jiaojo","2012款 2.4 手动 精英版四驱,jiaojp","2012款 2.0 手动 至尊版后驱,jiaojq",
						"2012款 2.0 手动 精英版后驱,jiaojr","2012款 2.0 手动 豪华版后驱,jiaojs"
						],
						
				"奥轩GX5":["2012款 2.4 手动 天窗版四驱,jiaoka","2012款 2.4 手动 时尚版四驱,jiaokb","2012款 2.4 自动 时尚版后驱,jiaokc",
						"2012款 2.4 自动 天窗版后驱,jiaokd","2012款 2.4 手动 时尚版后驱,jiaoke","2012款 2.4 手动 天窗版后驱,jiaokf",
						"2012款 2.0 手动 天窗版后驱,jiaokg","2012款 2.0 手动 时尚版后驱,jiaokh","2012款 2.4 自动 天窗版四驱,jiaoki",
						"2012款 2.4 自动 时尚版四驱,jiaokj","2012款 2.4 自动 精英版四驱,jiaokk"
						],		
						
				"帅凌":["2004款 2.4 手动 高顶四驱,jiaola","2004款 2.4 手动 高顶后驱,jiaolb","2004款 2.4 手动 后驱,jiaolc",
						"2004款 2.4 手动 四驱,jiaold"
					  ],		
						
				"帅威":["2005款 2.2 手动 标准型前驱,jiaoma","2005款 2.2 手动 豪华型前驱,jiaomb"
					  ],
					  
					  
				"帅舰":["2007款 2.2 手动 后驱,jiaona","2007款 2.0 手动 后驱,jiaonb","2008款 2.0 手动 后驱,jiaonc",
						"2008款 2.0T 手动 后驱 柴油,jiaond"
					  ],	  
					  
				"帅豹":["2010款 2.4 自动 豪华型前驱,jiaooa","2010款 2.4 自动 舒适型前驱,jiaoob","2010款 2.4 手动 豪华型前驱,jiaooc",
						"2010款 2.4 手动 舒适型前驱,jiaood"
					  ],	  
					  
				"帅驰":["2005款 2.8T 手动 豪华型后驱 柴油,jiaopa","2005款 2.8T 手动 标准型后驱 柴油,jiaopb","2005款 2.8T 手动 舒适型后驱 柴油,jiaopc",
						"2005款 2.2 手动 舒适型后驱,jiaopd","2005款 2.2 手动 豪华型后驱,jiaope","2005款 2.2 手动 标准型后驱,jiaopf",
						"2005款 2.2 手动 后驱,jiaopg","2005款 2.0 手动 后驱,jiaoph"
						],
				
				"星旺":["2008款 1.0 手动 标准型,jiaoqa","2009款 1.0 手动 豪华型,jiaoqb","2009款 1.0 手动 精英型,jiaoqc","2009款 1.0 手动 标准型,jiaoqd",
						"2010款 1.0 手动 舒适型,jiaoqe","2010款 1.0 手动 新动力基本型,jiaoqf","2010款 1.0 手动 新动力豪华型,jiaoqg",
						"2010款 1.0 手动 新动力标准型,jiaoqh","2010款 1.1 手动 快运舒适型,jiaoqi","2010款 1.1 手动 快运基本型,jiaoqj",
						"2010款 1.1 手动 快运豪华型,jiaoqk","2010款 1.1 手动 快运标准型,jiaoql","2012款 1.0 手动 豪华型7座,jiaoqm",
						"2012款 1.0 手动 精英型7座,jiaoqn","2012款 1.0 手动 豪华型,jiaoqo","2012款 1.0 手动 精英型,jiaoqp","2012款 1.0 手动 超值型,jiaoqq"
						],
				
				"星朗":["2014款 1.5 手动 精英型5座,jiaora","2014款 1.5 手动 舒适型5座,jiaorb","2014款 1.3 手动 舒适型5座,jiaorc",
						"2014款 1.5 手动 舒适型7座,jiaord","2014款 1.3 手动 豪华型5座,jiaore","2014款 1.3 手动 舒适型7座,jiaorf",
						"2014款 1.3 手动 标准型7座,jiaorg","2014款 1.5 手动 精英型7座,jiaorh","2014款 1.5 手动 豪华型7座,jiaori",
						"2014款 1.5 手动 至尊型7座,jiaorj","2014款 1.3 手动 基本型5座,jiaork",
						"2015款 1.5 手动 至尊型7座,jiaorl","2015款 1.5 手动 豪华型7座,jiaorm","2015款 1.5 手动 精英型7座,jiaorn"
						],

				"猛将旅":["2006款 2.2 手动 手动经济型C,jiaosa","2007款 2.2 手动 手动经济型,jiaosb","2007款 2.2 手动 手动经济型C,jiaosc"
					  ],
				
				
				"财运":["2006款 2.7T 手动 舒适型 柴油,jiaota","2010款 2.2T 手动 300绵阳标准型D22A 柴油,jiaotb",
						"2010款 2.2T 手动 100标准型绵阳后驱 柴油,jiaotc",
						"2010款 2.2T 手动 300标准型短货箱后驱 柴油,jiaotd","2010款 2.8T 手动 500豪华型后驱 柴油,jiaote",
						"2010款 2.8T 手动 500标准型后驱 柴油,jiaotf","2010款 2.2 手动 500标准型长货箱后驱,jiaotg",
						"2010款 2.2 手动 500标准型短货箱后驱,jiaoth","2010款 2.2 手动 100舒适型后驱,jiaoti",
						"2010款 2.2T 手动 100标准型后驱 柴油,jiaotj","2010款 2.2T 手动 100舒适型莱动后驱 柴油,jiaotk",
						"2010款 2.2T 手动 100标准型莱动后驱 柴油,jiaotl","2010款 2.8T 手动 100豪华型VE泵后驱 柴油,jiaotm",
						"2010款 2.8T 手动 100舒适型VE泵后驱 柴油,jiaotn","2011款 2.2T 手动 300标准型后驱 柴油,jiaoto",
						"2011款 2.2 手动 300豪华型加长版后驱,jiaotp","2012款 2.5T 手动 500豪华型后驱 柴油,jiaotq",
						"2013款 2.2T 手动 300标准型后驱 柴油,jiaotr","2013款 2.8T 手动 300豪华型后驱 柴油,jiaots",
						"2014款 2.2T 手动 100标准型后驱 柴油,jiaott"
						],
										
				
//"江铃"

				"域虎":["2012款 2.4T 手动 普通版JX4D24两驱,jianglingaa","2012款 2.4T 手动 超豪华版JX4D24两驱,jianglingab","2012款 2.4T 手动 豪华版JX4D24两驱,jianglingac",
						"2012款 2.4T 手动 SLX两驱 柴油,jianglingad","2012款 2.4T 手动 SLX四驱 柴油,jianglingae","2012款 2.4T 手动 LX四驱 柴油,jianglingaf",
						"2012款 2.4T 手动 LX两驱 柴油,jianglingag","2012款 2.4T 手动 GL四驱 柴油,jianglingah","2012款 2.4T 手动 GL两驱 柴油,jianglingai",
						"2012款 2.4 手动 SLX四驱,jianglingaj","2012款 2.4 手动 SLX两驱,jianglingak","2012款 2.4 手动 LX四驱,jianglingal","2012款 2.4 手动 LX两驱,jianglingam",
						"2012款 2.4 手动 GL四驱,jianglingan","2012款 2.4 手动 GL两驱,jianglingao"
						],
						                  
						
						
				"宝典":["2009款 2.8T 手动 超值版四驱 柴油,jianglingba","2009款 2.8T 手动 LX后驱 柴油,jianglingbb",
						"2013款 2.8T 手动 舒适时尚版两驱 柴油,jianglingbc","2013款 2.8T 手动 新超值版加长款舒适型后驱 柴油,jianglingbd","2013款 2.4 手动 超值版舒适型四驱,jianglingbe",
						"2013款 2.8T 手动 豪华时尚版四驱 柴油,jianglingbf","2013款 2.8T 手动 超值版加长型后驱 柴油,jianglingbg","2013款 2.8T 手动 舒适时尚版四驱 柴油,jianglingbh",
						"2013款 2.8T 手动 加长型后驱 柴油,jianglingbi","2013款 2.0 手动 超值版舒适型四驱,jianglingbj","2013款 2.0 手动 超值版舒适型后驱,jianglingbk",
						"2013款 2.8T 手动 超值版舒适型四驱 柴油,jianglingbl","2013款 2.8T 手动 新超值版舒适型四驱 柴油,jianglingbm","2013款 2.8T 手动 新超值版后驱舒适 柴油,jianglingbn",
						"2013款 2.8T 手动 超值版舒适型后驱 柴油,jianglingbo","2015款 2.4 手动 新时尚版加长版两驱,jianglingbp","2015款 2.9T 手动 豪华型PLUS后驱 柴油,jianglingbq",
						"2015款 2.9T 手动 豪华型PLUS四驱 柴油,jianglingbr","2015款 2.9T 手动 舒适型PLUS四驱 柴油,jianglingbs","2015款 2.4 手动 新时尚版舒适版四驱,jianglingbt",
						"2015款 2.8T 手动 新时尚版加长款舒适型后驱 柴油,jianglingbu","2015款 2.9T 手动 舒适型PLUS后驱 柴油,jianglingbv"
						],
						
						
						
				"宝威":["2005款 2.8T 手动 后驱 柴油,jianglingca","2005款 2.8T 手动 四驱 柴油,jianglingcb","2005款 2.4 手动 后驱,jianglingcc",
						"2009款 2.8T 手动 LX 7座后驱 柴油,jianglingcd","2009款 2.8T 手动 LX 5座后驱 柴油,jianglingce","2009款 2.8T 手动 四驱 柴油,jianglingcf"
						],
						
				
				"江铃轻卡":["2013款 2.8T 手动 凯锐单排后驱 柴油,jianglingda"
						],
						                  
				"运霸":["2004款 2.8T 手动 柴油,jianglingea","2006款 2.8T 手动 柴油,jianglingeb"
						],
						                  
						
						
				"驭胜":["2011款 2.4T 手动 超豪华型四驱5座 柴油,jianglingfa","2011款 2.4T 手动 超豪华型后驱 柴油,jianglingfb","2011款 2.4T 手动 豪华型后驱 柴油,jianglingfc",
						"2011款 2.4T 手动 超豪华型5后驱 柴油,jianglingfd","2011款 2.4T 手动 豪华型5座后驱 柴油,jianglingfe",
						"2012款 2.4 手动 豪华型增沉版7座后驱,jianglingff","2012款 2.4 手动 豪华型增沉版5座后驱,jianglingfg","2012款 2.4 手动 超豪华型增沉版7座后驱,jianglingfh",
						"2012款 2.4 手动 超豪华型增沉版5座后驱,jianglingfi","2012款 2.4 手动 超豪华型7座后驱,jianglingfj","2012款 2.4 手动 豪华型7座后驱,jianglingfk",
						"2012款 2.4 手动 超豪华型5座后驱,jianglingfl","2012款 2.4 手动 豪华型5座后驱,jianglingfm","2012款 2.4T 手动 超豪华型四驱7座 柴油,jianglingfn",
						"2012款 2.4T 手动 豪华型7座后驱 柴油,jianglingfo","2012款 2.4T 手动 超豪华型7座后驱 柴油,jianglingfp",
						"2013款 2.4T 手动 S350特装版四驱 柴油,jianglingfq","2013款 2.4 手动 S350特装版四驱,jianglingfr","2013款 2.4T 手动 S350特装版后驱 柴油,jianglingfs",
						"2013款 2.4 手动 S350特装版后驱,jianglingft","2014款 2.4T 自动 S350豪华天窗版7座四驱 柴油,jianglingfu","2014款 2.4T 自动 S350豪华天窗版7座后驱 柴油,jianglingfv",
						"2014款 2.4T 手动 S350特装版四驱 柴油,jianglingfw","2014款 2.4T 手动 S350特装版后驱 柴油,jianglingfx","2014款 2.4 手动 S350特装版四驱,jianglingfy",
						"2014款 2.4 手动 S350特装版后驱,jianglingfz","2014款 2.0T 手动 S350超豪华版5座四驱,jianglingfaa","2014款 2.0T 手动 S350超豪华版5座后驱,jianglingfab",
						"2014款 2.0T 手动 S350豪华版5座四驱,jianglingfac","2014款 2.0T 手动 S350豪华版5座后驱,jianglingfad","2014款 2.4T 手动 S350超豪华版5座四驱 柴油,jianglingfae",
						"2014款 2.4T 手动 S350超豪华版5座后驱 柴油,jianglingfaf","2014款 2.4T 手动 S350豪华版5座四驱 柴油,jianglingfag","2014款 2.4T 手动 S350豪华版5座后驱 柴油,jianglingfag",
						"2014款 2.4T 自动 S350超豪华版5座四驱 柴油,jianglingfah","2014款 2.4T 自动 S350超豪华版5座后驱 柴油,jianglingfai","2014款 2.4T 自动 S350豪华版5座后驱 柴油,jianglingfaj",
						"2014款 2.0T 手动 S350超豪华版7座四驱,jianglingfak","2014款 2.0T 手动 S350超豪华版7座后驱,jianglingfal","2014款 2.0T 手动 S350豪华版7座四驱,jianglingfam",
						"2014款 2.0T 手动 S350豪华版7座后驱,jianglingfan","2014款 2.4T 手动 S350超豪华版7座四驱 柴油,jianglingfao","2014款 2.4T 手动 S350超豪华版7座后驱 柴油,jianglingfap",
						"2014款 2.4T 手动 S350豪华版7座四驱 柴油,jianglingfaq","2014款 2.4T 手动 S350豪华版7座后驱 柴油,jianglingfar","2014款 2.4T 自动 S350超豪华版7座四驱 柴油,jianglingfas",
						"2014款 2.4T 自动 S350超豪华版7座后驱 柴油,jianglingfat","2014款 2.4T 自动 S350豪华版7座后驱 柴油,jianglingfau"
						],
						
						
						
						
				"骐铃":["2013款 2.2 手动 豪华型加长轴4G22B四驱,jianglingga","2013款 2.8T 手动 加长轴JE493ZLQ4E四驱 柴油,jianglinggb",
						"2013款 2.8T 手动 豪华型加长轴JE493ZLQ4CB四驱 柴油,jianglinggc","2013款 2.2 手动 加长轴4G22B后驱,jianglinggd",
						"2013款 2.2 手动 标准轴4G22B后驱,jianglingge","2013款 2.4T 手动 加长轴4D25后驱 柴油,jianglinggf",
						"2013款 2.4T 手动 标准轴4D25后驱 柴油,jianglinggg","2013款 2.8T 手动 加长轴JE493ZLQ4CB后驱 柴油,jianglinggh",
						"2013款 2.8T 手动 标准轴JE493ZLQ4CB后驱 柴油,jianglinggi","2013款 2.8T 手动 加长轴JE493ZLQ4E后驱 柴油,jianglinggj",
						"2013款 2.8T 手动 标准轴JE493ZLQ4E后驱 柴油,jianglinggk","2014款 2.8T 手动 豪华型加长轴距JE493ZLQ4E两驱 柴油,jianglinggl",
						"2014款 2.8T 手动 升级型加长轴距JE493ZLQ4E两驱 柴油,jianglinggm","2014款 2.8T 手动 超值版加长轴距JE493ZLQ4E两驱 柴油,jianglinggn",
						"2014款 2.8T 手动 豪华型标准轴距JE493ZLQ4E两驱 柴油,jianglinggo","2014款 2.8T 手动 升级型标准轴距JE493ZLQ4E两驱 柴油,jianglinggp",
						"2014款 2.8T 手动 超值版标准轴距JE493ZLQ4E两驱 柴油,jianglinggq","2014款 2.8T 手动 豪华型长轴四驱 柴油,jianglinggr",
						"2014款 2.8T 手动 升级型长轴四驱 柴油,jianglinggs","2014款 2.8T 手动 精英型加长轴距JE493ZLQ4CB四驱 柴油,jianglinggt"
						],
						
						                  
				"骐铃T7":["2015款 2.8T 手动 尊贵版加长轴距JE493ZLQ4CB四驱 柴油,jianglingha","2015款 2.8T 手动 舒适版加长轴距JE493ZLQ4CB四驱 柴油,jianglinghb",
						"2015款 2.8T 手动 尊贵版标准轴距JE493ZLQ4CB四驱 柴油,jianglinghc","2015款 2.8T 手动 舒适版标准轴距JE493ZLQ4CB四驱 柴油,jianglinghd",
						"2015款 2.8T 手动 尊贵版加长轴距JE493ZLQ4CB两驱 柴油,jianglinghe","2015款 2.8T 手动 舒适版加长轴距JE493ZLQ4CB两驱 柴油,jianglinghf",
						"2015款 2.8T 手动 尊贵版标准轴距JE493ZLQ4CB两驱 柴油,jianglinghg","2015款 2.8T 手动 舒适版标准轴距JE493ZLQ4CB两驱 柴油,jianglinghh",
						"2015款 2.8T 手动 尊贵版加长轴距JE493ZLQ4E两驱 柴油,jianglinghi","2015款 2.8T 手动 舒适版加长轴距JE493ZLQ4E两驱 柴油,jianglinghj",
						"2015款 2.8T 手动 尊贵版标准轴距JE493ZLQ4E两驱 柴油,jianglinghk","2015款 2.8T 手动 舒适版标准轴距JE493ZLQ4E两驱 柴油,jianglinghl",
						"2015款 2.2 手动 尊贵版加长轴距4G22B四驱,jianglinghm","2015款 2.2 手动 舒适版加长轴距4G22B四驱,jianglinghn",
						"2015款 2.2 手动 尊贵版标准轴距4G22B四驱,jianglingho","2015款 2.2 手动 舒适版标准轴距4G22B四驱,jianglinghp",
						"2015款 2.2 手动 尊贵版加长轴距4G22B两驱,jianglinghq","2015款 2.2 手动 舒适版加长轴距4G22B两驱,jianglinghr",
						"2015款 2.2 手动 尊贵版标准轴距4G22B两驱,jianglinghs","2015款 2.2 手动 舒适版标准轴距4G22B两驱,jianglinght"
						],
								
				
//"吉普"
				"JEEP2500":["2005款 2.4 手动 后驱,jeepaa"],
				
				"JEEP2700":["2006款 2.7 手动 四驱,jeepba"],
				
				"大切诺基(进口)":["2005款 5.7 自动 四驱,jeepjkca","2005款 3.7 自动 四驱,jeepjkcb","2005款 3.0T 自动 CRD四驱 柴油,jeepjkcc","2005款 4.7 自动 四驱,jeepjkcd",
							"2006款 6.1 自动 SRT8四驱,jeepjkce","2007款 4.7 自动 四驱,jeepjkcf","2007款 5.7 自动 四驱,jeepjkcg","2007款 5.7 自动 Overland四驱,jeepjkch",
							"2007款 3.7 自动 Limited四驱,jeepjkci","2007款 3.7 自动 Laredo X Package四驱,jeepjkcj","2007款 3.7 自动 Rockey Mountain四驱,jeepjkck",
							"2007款 3.7 自动 Laredo四驱,jeepjkcl","2008款 3.7 自动 四驱,jeepjkcm","2010款 5.7 自动 四驱,jeepjkcn","2010款 3.7 自动 四驱,jeepjkco",
							"2011款 3.6 自动 周年纪念导航舒适型,jeepjkcp","2011款 3.6 自动 周年纪念导航豪华型,jeepjkcq",
							"2011款 6.4 自动 Hemi SRT8四驱,jeepjkcr","2011款 5.7 自动 旗舰导航版四驱,jeepjkcs","2011款 3.6 自动 舒适版四驱,jeepjkct",
							"2011款 3.6 自动 旗舰导航版四驱,jeepjkcu","2011款 3.6 自动 旗舰版四驱,jeepjkcv","2011款 3.6 自动 经典版四驱,jeepjkcw",
							"2011款 3.6 自动 豪华导航版四驱,jeepjkcx","2011款 3.6 自动 豪华版四驱,jeepjkcy","2011款 3.6 自动 70周年限量版四驱,jeepjkcz",
							"2012款 3.6 自动 舒适导航版,jeepjkcaa","2012款 3.6 自动 周年导航版,jeepjkcab","2012款 3.6 自动 梦十珍藏版,jeepjkcac",
							"2013款 3.6 自动 旗舰尊悦版,jeepjkcad","2013款 3.6 自动 精英导航版,jeepjkcae","2013款 3.6 自动 舒适导航版,jeepjkcaf",
							"2013款 6.4 自动 SRT8,jeepjkcag","2013款 5.7 自动 旗舰尊崇版,jeepjkcah","2013款 3.6 自动 旗舰尊崇版,jeepjkcai",
							"2013款 3.6 自动 豪华导航版,jeepjkcaj","2013款 3.6 自动 舒享导航版,jeepjkcak","2013款 3.6 自动 舒适版,jeepjkcal",
							"2013款 6.4 自动 炫黑版SRT8,jeepjkcam","2014款 3.0 自动 旗舰尊悦版,jeepjkcan","2014款 3.0T 自动 舒享导航版 柴油,jeepjkcao",
							"2014款 5.7 自动 旗舰尊悦版,jeepjkcap","2014款 3.6 自动 旗舰尊悦版,jeepjkcaq","2014款 3.6 自动 精英导航版,jeepjkcar",
							"2014款 3.0 自动 舒享导航版,jeepjkcas","2015款 3.6 自动 旗舰尊耀版,jeepjkcat","2015款 3.0 自动 旗舰尊耀版,jeepjkcau",
							"2015款 3.0 自动 精英导航版,jeepjkcav","2015款 3.0T 自动 精英导航版 柴油,jeepjkcaw"
							],
				
				"城市猎人":["2001款 2.5 手动 硬衬顶篷四驱,jeepda"],
				
				"大切诺基":["2006款 4.7 自动 四驱,jeepea","2006款 4.0 自动 征程四驱,jeepeb","2006款 4.7 自动 征途四驱,jeepec"],
				
				"切诺基(进口)":["2005款 2.5 手动 工程车四驱,jeepjkfa"],
				
				"指南者":["2006款 2.4 手动 前驱,jeepga","2006款 2.4 自动 豪华型前驱,jeepgb","2006款 2.4 自动 标准型前驱,jeepgc",
						"2007款 2.4 自动 限量版四驱,jeepgd","2007款 2.4 自动 运动版四驱,jeepge",
						"2009款 2.4 自动 运动版四驱,jeepgf","2009款 2.4 自动 限量版四驱,jeepgg","2010款 2.4 自动 世界杯特别版四驱,jeepgh",
						"2011款 2.4 自动 70周年限量版四驱,jeepgi","2011款 2.4 自动 舒适升级版四驱,jeepgj","2011款 2.4 自动 经典升级版四驱,jeepgk",
						"2011款 2.4 自动 舒适版四驱,jeepgl","2011款 2.4 自动 运动版四驱,jeepgm","2011款 2.4 自动 经典版四驱,jeepgn",
						"2011款 2.4 自动 豪华导航版四驱,jeepgo","2011款 2.4 自动 豪华版四驱,jeepgp",
						"2012款 2.4 自动 都市版四驱,jeepgq","2012款 2.0 自动 豪华版前驱,jeepgr","2012款 2.0 自动 运动版前驱,jeepgs",
						"2013款 2.4 自动 豪华导航版四驱,jeepgt","2013款 2.4 自动 蛇行珍藏版四驱,jeepgu","2013款 2.0 自动 都市版前驱,jeepgv",
						"2013款 2.4 自动 炫黑导航版四驱,jeepgw","2013款 2.4 自动 都市版四驱,jeepgx","2013款 2.0 自动 炫黑豪华版前驱,jeepgy",
						"2013款 2.0 自动 豪华版前驱,jeepgz","2013款 2.4 自动 豪华版四驱,jeepgaa","2013款 2.4 自动 运动版四驱,jeepgab",
						"2013款 2.0 自动 运动版前驱,jeepgac","2014款 2.4 自动 豪华导航版四驱改款,jeepgad","2014款 2.4 自动 豪华版四驱改款,jeepgae",
						"2014款 2.4 自动 舒适版四驱改款,jeepgaf","2014款 2.0 自动 精英版前驱改款,jeepgag","2014款 2.0 自动 进取版前驱改款,jeepgah",
						"2015款 2.0 自动 豪华版前驱,jeepgai","2015款 2.0 自动 运动版前驱,jeepgaj"
						],
				
				"指挥官":["2005款 5.7 自动 四驱,jeepha","2005款 4.7 自动 四驱,jeephb",
						"2006款 3.7 手动 Sport7座四驱,jeephc","2006款 3.7 手动 Sport U Package7座四驱,jeephd",
						"2006款 3.7 自动 Sport7座四驱,jeephe","2006款 3.7 自动 Sport U Package7座四驱,jeephf",
						"2006款 3.7 自动 Sport5座四驱,jeephg","2007款 5.7 自动 限量版四驱,jeephi","2007款 5.7 自动 四驱,jeephj",
						"2007款 4.7 自动 四驱,jeephk","2007款 4.7 自动 限量版四驱,jeephl","2007款 4.7 自动 Sport5座四驱,jeephm",
						"2008款 3.0T 自动 四驱 柴油,jeephn","2008款 5.7 自动 四驱,jeepho","2008款 4.7 自动 四驱,jeephp"
						],      
				
				
				
				"牧马人":["2015款 3.6 自动 Rubicon舒享版四门,jeepia","2015款 3.6 自动 Rubicon舒享版两门,jeepib","2015款 3.0 自动 Sahara舒享版四门,jeepic",
						"2015款 2.8T 自动 Sahara舒享版四门 柴油,jeepid","2015款 3.6 自动 Sahara四门,jeepie","2015款 3.6 自动 Rubicon四门,jeepif",
						"2015款 3.6 自动 Sahara两门,jeepig","2015款 3.6 自动 Rubicon两门,jeepih","2015款 3.0 自动 Sahara四门,jeepii",
						"2015款 2.8T 自动 Sahara四门 柴油,jeepij","2014款 2.8T 自动 Sahara四门 柴油,jeepik","2014款 3.0 自动 Sahara四门,jeepil","2014款 3.6 自动 龙腾典藏版,jeepim",
						"2013款 3.6 自动 罗宾汉十周年限量纪念版四门,jeepin","2013款 3.6 自动 罗宾汉十周年限量纪念版两门,jeepio",
						"2013款 3.6 自动 摩崖四驱特别版四门,jeepip","2013款 3.6 自动 Sahara四门,jeepiq","2013款 3.6 自动 Rubicon四门,jeepir",
						"2013款 3.6 自动 Sahara两门,jeepis","2013款 3.6 自动 Rubicon两门,jeepit",
						"2012款 3.6 自动 梦十珍藏版四门,jeepiu","2012款 3.6 自动 极地限量版,jeepiv","2012款 3.6 自动 Sahara四门,jeepiw",
						"2012款 3.6 自动 Sahara两门,jeepix","2012款 3.6 自动 Rubicon四门,jeepiy","2012款 3.6 自动 Rubicon两门,jeepiz",
						"2011款 3.8 自动 70周年限量版Sahara四门,jeepiaa","2011款 3.8 自动 Rubicon两门,jeepiab","2011款 3.8 自动 Rubicon四门,jeepiac",
						"2011款 3.8 自动 Sahara四门,jeepiad","2011款 3.8 自动 Sahara两门,jeepiae",
						"2010款 3.8 自动 Rubicon四门,jeepiaf","2010款 3.8 自动 Wrangler冰川纪念版,jeepiag",
						"2008款 3.8 自动 Unlimited Sahara四门,jeepiah","2008款 3.8 手动 Unlimited Sahara,jeepiai","2008款 3.8 自动 Unlimited X RHD,jeepiaj",
						"2008款 3.8 自动 Unlimited Sahara两门,jeepiak","2008款 3.8 自动 Unlimited Sahara,jeepial","2008款 3.8 手动 Unlimited X,jeepiam",
						"2008款 3.8 手动 Unlimited X S-Package,jeepian","2008款 3.8 自动 Unlimited X S,jeepiao","2008款 3.8 自动 Unlimited X S-Package,jeepiap",
						"2007款 3.8 自动 Rubicon两门,jeepiaq","2007款 3.8 自动 Sahara两门,jeepiar","2007款 3.8 自动 Sahara四门,jeepias","2007款 3.8 自动 Rubicon四门,jeepiat",
						"2007款 3.8 手动 X S-Package,jeepiau","2007款 3.8 手动 X,jeepiav",
						"2006款 4.0 自动 Rubicon,jeepiaw","2006款 2.4 自动 ,jeepiax","2006款 3.8 自动 Rubicon两门,jeepiay","2006款 3.8 手动 Unlimited,jeepiaz",
						"2006款 3.6 自动 Unlimited,jeepiba","2006款 3.6 手动 Unlimited,jeepibb","2006款 3.6 自动 ,jeepibc","2006款 3.6 手动 ,jeepibd",
						"2006款 3.8 自动 Unlimited Rubicon,jeepibe","2006款 3.8 手动 两门,jeepibf","2006款 3.8 自动 两门,jeepibg","2005款 4.0 自动 Rubicon,jeepibh"
						],
				
				
				"自由人":["2008款 3.7 自动 Limited四驱,jeepja","2008款 3.7 自动 Rocky Mountain四驱,jeepjb","2008款 3.7 自动 Limited前驱,jeepjc",
						"2008款 3.7 自动 Sport四驱,jeepjd","2008款 3.7 自动 Rocky Mountain前驱,jeepje","2008款 3.7 自动 Sport前驱,jeepjf",
						"2007款 3.7 手动 四驱,jeepjg","2005款 2.4 手动 四驱,jeepjh"
						],
				
				
				"自由光":["2015款 2.4 自动 精英版,jeepka","2014款 2.4 自动 高性能版,jeepkb","2014款 3.2 自动 高性能版,jeepkc","2014款 2.4 自动 精锐版,jeepkd",
							"2014款 2.4 自动 豪华版,jeepke","2014款 2.4 自动 都市版,jeepkf"
							],
     
				
				"自由客":["2015款 2.0 自动 运动版,jeepla","2014款 2.0 自动 运动增强版,jeeplb","2014款 2.4 自动 运动版,jeeplc",
						"2014款 2.4 自动 豪华导航版,jeepld","2014款 2.4 自动 蛇行珍藏版,jeeple","2014款 2.0 自动 运动版,jeeplf",
						"2013款 2.4 自动 豪华导航版,jeeplg","2013款 2.4 自动 运动版,jeeplh","2013款 2.4 自动 炫黑运动版,jeepli",
						"2012款 2.4 自动 豪华版四驱,jeeplj","2012款 2.4 自动 运动版四驱,jeeplk","2011款 2.4 自动 运动升级版,jeepll",
						"2011款 2.4 自动 运动版,jeeplm","2011款 2.4 自动 经典升级版,jeepln","2011款 2.4 自动 经典版,jeeplo",
						"2011款 2.4 自动 豪华版,jeeplp","2011款 2.4 自动 70周年限量版,jeeplq",
						"2007款 2.4 手动 前驱,jeeplr","2007款 2.0 手动 前驱,jeepls","2007款 2.0 自动 四驱,jeeplt","2007款 2.0 自动 前驱,jeeplu",
						"2007款 2.4 自动 前驱,jeeplv","2007款 2.4 自动 四驱,jeeplw","2007款 2.4 手动 四驱,jeeplx","2007款 2.0 手动 四驱,jeeply",
						"2007款 2.0T 手动 四驱 柴油,jeeplz"
						],
				
// "江南"
	
				"江南TT":["2010款 0.8 手动 实用型,jiangnanaa","2010款 0.8 手动 舒适型,jiangnanab","2010款 0.8 手动 尊贵型,jiangnanac"
					],
     				
				
				"传奇":["2005款 1.5 手动 经济型,jiangnanba","2006款 1.5 手动 豪华型,jiangnanbb","2006款 1.5 手动 舒适型,jiangnanbc",
						"2008款 1.5 手动,jiangnanbd"
					  ],
				
				"奥拓":["2006款 1.1 手动 标准型,jiangnanca","2007款 1.1 手动 舒适型,jiangnancb","2007款 1.1 手动 豪华型,jiangnancc",
						"2007款 0.8 手动 天窗型,jiangnancd","2007款 0.8 手动 豪华型,jiangnance",
						"2007款 0.8 手动 舒适型,jiangnancf","2007款 0.8 手动 普通型,jiangnancg","2007款 0.8 手动 基本型,jiangnanch",
						"2008款 0.8 自动,jiangnanci","2008款 1.1 手动,jiangnancj","2009款 0.8 手动 经典型,jiangnanck",
						"2011款 0.8 手动 基本型,jiangnancl"
						], 
				
				"精灵":["2005款 0.8 手动,jiangnanda","2006款 1.1 手动 舒适型,jiangnandb","2007款 0.8 手动,jiangnandc",
						"2008款 0.8 手动,jiangnandd","2008款 1.1 手动 舒适型,jiangnande"
					  ],
				
				"风光":["2005款 1.8 手动,jiangnanea"
						],
				
				
//"九龙"	
				
				"天马商务车":["2010款 2.8 手动 基本型JE4D28A,jiulongaa","2010款 2.5T 手动 柴油版,jiulongab","2010款 2.4 手动 精英型4RB2,jiulongac",
							"2010款 2.4 手动 精英型4G69S4N,jiulongad","2010款 2.4 自动 豪华型4RB2,jiulongae","2010款 2.4 手动 豪华型4RB2,jiulongaf",
							"2010款 2.4 自动 豪华型4G69S4N,jiulongag","2011款 2.7 手动 精英型,jiulongah","2014款 2.2 手动 经济版JM491Q-ME,jiulongai"
							],  
				
				"艾菲":["2015款 2.4 手动 标准型7座,jiulongba"
						],
				
				
// "捷豹"
				
				
				"F-Type":["2013款 5.0T 自动 S中国限量版,jiebaoaa","2013款 5.0T 自动 S,jiebaoab","2013款 3.0T 自动 S,jiebaoac","2013款 3.0T 自动,jiebaoad",
						"2014款 5.0T 自动 R,jiebaoae","2014款 3.0T 自动 S,jiebaoaf","2014款 3.0T 自动,jiebaoag","2015款 3.0T 自动 S硬顶版四驱,jiebaoah",
						"2015款 5.0T 自动 R敞篷版四驱,jiebaoai","2015款 3.0T 自动 S敞篷版四驱,jiebaoaj","2015款 5.0T 自动 R硬顶版四驱,jiebaoak"
						],
				
				"S-Type":["2006款 3.0 自动 动感典藏版,jiebaoba","2007款 3.0 自动 基本型,jiebaobb"
						],
				
				"X-Type":["2004款 2.5 自动 X,jiebaoca"
						],
				
				"XE":["2015款 2.0T 自动 200PS R-SPORT,jiebaoda","2015款 2.0T 自动 240PS R-SPORT,jiebaodb","2015款 3.0T 自动 340PS S,jiebaodc"
						],
				
				"XF":["2007款 5.0T 自动 机械增压,jiebaoea","2007款 4.2 自动,jiebaoeb","2007款 5.0 自动,jiebaoec","2007款 4.2T 自动 机械增压,jiebaoed",
						"2007款 3.0 自动 豪华版,jiebaoee","2008款 5.0 自动 奢华版,jiebaoef","2008款 3.0 自动 优质豪华版,jiebaoeg","2009款 5.0T 自动 R,jiebaoeh",
						"2011款 3.0 自动 75周年纪念版,jiebaoei","2012款 3.0 自动 伦敦限量版,jiebaoej","2012款 3.0 自动 豪华版,jiebaoek",
						"2012款 3.0 自动 风华版,jiebaoel","2013款 2.0T 自动 剑桥限量版,jiebaoem","2013款 3.0T 自动 SC奢华版,jiebaoen",
						"2013款 3.0T 自动 SC豪华版,jiebaoeo","2013款 3.0T 自动 SC风华版,jiebaoep","2013款 2.0T 自动 奢华版,jiebaoeq",
						"2013款 2.0T 自动 豪华版,jiebaoer","2013款 2.0T 自动 风华版,jiebaoes","2014款 3.0T 自动 SC 奢华版,jiebaoet",
						"2014款 3.0T 自动 SC 豪华版,jiebaoeu","2014款 3.0T 自动 SC 风华版,jiebaoev",
						"2014款 2.0T 自动 奢华版,jiebaoew","2014款 2.0T 自动 豪华版,jiebaoex","2014款 2.0T 自动 风华版,jiebaoey",
						"2014款 3.0T 自动 SC Sport Club限量版,jiebaoez","2014款 2.0T 自动 Style Club限量版,jiebaoeaa",
						"2015款 3.0T 自动 80周年典藏R-Sport版,jiebaoeab","2015款 2.0T 自动 80周年典藏豪华版,jiebaoeac",
						"2015款 2.0T 自动 80周年典藏风华版,jiebaoead","2015款 3.0T 自动 SC奢华版R-Sport,jiebaoeae",
						"2015款 3.0T 自动 SC豪华版R-Sport,jiebaoeaf","2015款 3.0T 自动 SC风华版R-Sport,jiebaoeag",
						"2015款 2.0T 自动 豪华版Sportbrake,jiebaoeah","2015款 2.0T 自动 风华版Sportbrake,jiebaoeai",
						"2015款 2.0T 自动 奢华版,jiebaoeaj","2015款 2.0T 自动 豪华版,jiebaoeak","2015款 2.0T 自动 风华版,jiebaoeal"
						],
				
				"XJ":["2005款 4.2 自动 XJ8L皇家加长版,jiebaofa","2005款 3.0 自动 XJ6L皇家加长版,jiebaofb","2007款 4.2 自动 XJ8,jiebaof07a","2007款 3.5 自动 XJ8,jiebaofc",
					"2007款 3.0 自动 XJ6,jiebaofd","2007款 4.2 自动 XJ8L皇家加长版,jiebaofe","2007款 3.0 自动 XJ6L皇家加长版,jiebaoff","2007款 3.0 自动 XJL精英领袖版,jiebaofg",
					"2009款 5.0T 自动 SWB,jiebaofh","2009款 5.0T 自动 LWB,jiebaofi","2009款 5.0 自动 SWB,jiebaofj","2009款 5.0 自动 LWB,jiebaofk",
					"2009款 3.0T 自动 SWB 柴油,jiebaofl","2009款 3.0T 自动 LWB 柴油,jiebaofm","2011款 5.0 自动 全景奢华版,jiebaofn",
					"2011款 5.0 自动 皇家婚礼限量版,jiebaofo","2011款 3.0 自动 全景商务版,jiebaofp","2011款 5.0T 自动 旗舰尊崇版,jiebaofq",
					"2010款 5.0 自动 全景奢华版,jiebaofr","2012款 5.0 自动 伦敦限量版,jiebaofs","2012款 3.0 自动 伦敦限量版,jiebaoft",
					"2012款 5.0 自动 全景奢华版,jiebaofu","2012款 3.0 自动 旗舰商务版,jiebaofv","2012款 3.0 自动 全景商务版,jiebaofw",
					"2012款 5.0T 自动 旗舰尊崇版,jiebaofx","2013款 3.0T 自动 剑桥限量版,jiebaofy","2013款 5.0T 自动 SC巅峰创世版4座,jiebaofz",
					"2013款 5.0T 自动 SC巅峰创世版5座,jiebaofaa","2013款 3.0T 自动 SC旗舰商务版四驱,jiebaofab","2013款 3.0T 自动 SC旗舰商务版,jiebaofac",
					"2013款 3.0T 自动 SC全景商务版,jiebaofad","2013款 2.0T 自动 全景商务版,jiebaofae","2013款 2.0T 自动 典雅商务版,jiebaofaf",
					"2014款 3.0T 自动 SC旗舰商务版四驱,jiebaofag","2014款 3.0T 自动 SC旗舰商务版,jiebaofah","2014款 3.0T 自动 SC尊享商务版四驱,jiebaofai",
					"2014款 3.0T 自动 SC尊享商务版,jiebaofaj","2014款 3.0T 自动 SC全景商务版四驱,jiebaofak","2014款 3.0T 自动 SC全景商务版,jiebaofal",
					"2014款 2.0T 自动 尊享商务版,jiebaofam","2014款 2.0T 自动 全景商务版,jiebaofan","2014款 2.0T 自动 典雅商务版,jiebaofao",
					"2015款 3.0T 自动  SC尊享商务80周年典藏版四驱,jiebaofap","2015款 3.0T 自动  SC全景商务80周年典藏版四驱,jiebaofaq",
					"2015款 3.0T 自动  SC典雅商务80周年典藏版,jiebaofar","2015款 2.0T 自动 典雅商务80周年典藏版,jiebaofas",
					"2015款 3.0T 自动 SC尊享商务版四驱,jiebaofat","2015款 3.0T 自动 SC全景商务版四驱,jiebaofau",
					"2015款 3.0T 自动 SC旗舰商务版四驱,jiebaofav","2015款 3.0T 自动 SC尊享商务版,jiebaofaw",
					"2015款 3.0T 自动 SC全景商务版,jiebaofax","2015款 3.0T 自动 SC旗舰商务版,jiebaofay",
					"2015款 2.0T 自动 尊享商务版,jiebaofaz","2015款 2.0T 自动 全景商务版,jiebaofba",
					"2015款 2.0T 自动 典雅商务版,jiebaofbb","2015款 3.0T 自动 SC剑桥限量版,jiebaofbc"
					],
				
				
				"XK":["2006款 3.5 自动,jiebaoga","2006款 4.2 自动,jiebaogb","2006款 5.0 自动,jiebaogc"
					],
				
				
   			   
//"开瑞"		
				
				"K50":["2015款 1.5 自动 标准型5-7座,kairuiaa","2015款 1.5 自动 舒适型5-7座,kairuiab","2015款 1.5 自动 豪华型5-7座,kairuiac",
						"2015款 1.5 手动 豪华型5-7座,kairuiad","2015款 1.5 手动 舒适型5-7座,kairuiae","2015款 1.5 手动 标准型5-7座,kairuiaf",
						"2015款 1.5 手动 基本型5-7座,kairuiag","2015款 1.5 手动 创业型5-7座,kairuiah",
						"2016款 1.5 自动 K50S豪华型5-7座,kairuiai","2016款 1.5 自动 K50S舒适型5-7座,kairuiaj","2016款 1.5 自动 K50S标准型5-7座,kairuiak",
						"2016款 1.5 手动 K50S豪华型5-7座,kairuial","2016款 1.5 手动 K50S舒适型5-7座,kairuiam","2016款 1.5 手动 K50S标准型5-7座,kairuian"
						],

				"优优":["2010款 1.0T 手动 舒适型5-8座 柴油,kairuiba","2010款 1.0T 手动 豪华型5-8座 柴油,kairuibb","2010款 1.0T 手动 标准型5-8座 柴油,kairuibc",
						"2010款 1.0T 手动 基本型5-8座 柴油,kairuibd","2010款 1.1 手动 舒适型5-8座,kairuibe","2010款 1.1 手动 基本型5-8座,kairuibf",
						"2010款 1.1 手动 基本Ⅰ型5-8座,kairuibg","2010款 1.1 手动 豪华型5-8座,kairuibh","2010款 1.1 手动 标准型5-8座,kairuibi",
						"2010款 1.1 手动 标准Ⅰ型5-8座,kairuibj","2010款 1.0 手动 舒适型5-8座,kairuibk","2010款 1.0 手动 基本型5-8座,kairuibl",
						"2010款 1.0 手动 基本Ⅰ型5-8座,kairuibm","2010款 1.0 手动 豪华型5-8座,kairuibn","2010款 1.0 手动 标准型5-8座,kairuibo",
						"2010款 1.0 手动 标准Ⅰ型5-8座,kairuibp","2012款 1.2 手动 加长标准型5座,kairuibq","2012款 1.2 手动 加长豪华型9座,kairuibr",
						"2012款 1.2 手动 加长舒适型9座,kairuibs",
						"2012款 1.2 手动 加长标准Ⅰ型9座,kairuibt","2012款 1.1 手动 标准Ⅰ型5-8座,kairuibu","2012款 1.1 手动 基本型5-8座,kairuibv",
						"2012款 1.1 手动 基本Ⅰ型5-8座,kairuibw","2012款 1.0 手动 标准Ⅰ型5-8座,kairuibx","2012款 1.0 手动 基本Ⅰ型5-8座,kairuiby",
						"2012款 1.0 手动 基本型5-8座,kairuibz","2012款 1.2 手动 加长舒适型5-8座,kairuibaa","2012款 1.2 手动 豪华型5-8座,kairuibab", 
						"2012款 1.2 手动 舒适型5-8座,kairuibac","2012款 1.2 手动 标准型5-8座,kairuibad","2012款 1.2 手动 加长标准Ⅰ型5-8座,kairuibae",
						"2012款 1.1 手动 舒适型5-8座,kairuibaf","2012款 1.1 手动 豪华型5-8座,kairuibag","2012款 1.1 手动 标准型5-8座,kairuibah",
						"2012款 1.0 手动 豪华型5-8座,kairuibai","2012款 1.0 手动 舒适型5-8座,kairuibaj","2012款 1.0 手动 标准型5-8座,kairuibak",
						"2012款 1.2 手动 加长豪华型5-8座,kairuibal","2013款 1.2 手动 Ⅱ基本型,kairuibam","2013款 1.2 手动 Ⅱ基本型(碟刹)5-8座,kairuiban",
						"2013款 1.2 手动 Ⅱ基本型5-8座,kairuibao",
						"2013款 1.0 手动 Ⅱ基本型(碟刹)5-8座,kairuibap","2013款 1.0 手动 Ⅱ基本型5-8座,kairuibaq","2013款 1.0 手动 Ⅱ标准型(碟刹)5-8座,kairuibar",
						"2013款 1.2 手动 Ⅱ标准碟刹型5-8座,kairuibas","2013款 1.2 手动 Ⅱ舒适型5-8座,kairuibat","2013款 1.2 手动 Ⅱ豪华型5-8座,kairuibau",
						"2013款 1.0 手动 Ⅱ舒适型5-8座,kairuibav","2013款 1.0 手动 Ⅱ标准型5-8座,kairuibaw","2013款 1.0 手动 Ⅱ特惠型5-8座,kairuibax",
						"2014款 1.3 手动 豪华型5-8座加长,kairuibay","2014款 1.3 手动 舒适型5-8座加长,kairuibaz","2014款 1.3 手动 标准型5-8座加长,kairuibba"
						],
				
				"优劲":["2010款 1.0 手动 单排基本型后驱,kairuica","2012款 1.1 手动 基本型,kairuicb","2012款 1.1 手动 标准型,kairuicc",
						"2012款 1.1 手动 单排货箱式后驱,kairuicd","2012款 1.1 手动 单排舒适型后驱,kairuice",
						"2012款 1.1 手动 单排标准型后驱,kairuicf","2012款 1.1 手动 单排基本型后驱,kairuicg",
						"2013款 1.2 手动 单排基本型后驱,kairuich","2013款 1.2 手动 加长版舒适型,kairuici",
						"2013款 1.2 手动 单排货箱式加长基本型后驱,kairuicj","2013款 1.2 手动 双排标准I型,kairuick",
						"2013款 1.0T 手动 改装房车款BDD5021XXC 柴油,kairuicl","2013款 1.0T 手动 改装房车款BDD5020XSH 柴油,kairuicm",
						"2013款 1.2 手动 单排货箱式加长标准型后驱,kairuicn","2013款 1.0T 手动 单排标准型后驱 柴油,kairuico",
						"2013款 1.2 手动 双排舒适型,kairuicp","2013款 1.2 手动 单排标准型后驱,kairuicq","2013款 1.2 手动 双排标准型,kairuicr",
						"2013款 1.2 手动 双排基本型,kairuics","2013款 1.1 手动 双排舒适型,kairuict",
						"2013款 1.1 手动 双排标准I型,kairuicu","2013款 1.1 手动 双排标准型,kairuicv","2013款 1.1 手动 双排基本型,kairuicw",
						"2016款 1.2 手动 加长舒适型5座,kairuicx","2016款 1.2 手动 加长标准Ⅰ型5座,kairuicy"
						],
				
				
				"优派":["2009款 1.0 手动 标准型,kairuida","2009款 1.0 手动 豪华型,kairuidb","2009款 1.0 手动 舒适型,kairuidc",
						"2009款 1.1 手动 标准型,kairuidd","2009款 1.1 手动 豪华型,kairuide","2009款 1.1 手动 舒适型,kairuidf"
						],

				
				"优翼":["2007款 1.6 手动 标准型5-7座,kairuiea","2007款 1.6 手动 基本型5-7座,kairuieb","2007款 1.6 手动 舒适型5-7座,kairuiec",
						"2009款 1.5 手动 标准型5-7座,kairuied","2009款 1.5 手动 基本型5-7座,kairuiee","2009款 1.5 手动 舒适型5-7座,kairuief"
						],
				
				
				"优胜":["2010款 1.1 手动 豪华型,kairuifa","2010款 1.0 手动 豪华型,kairuifb","2010款 1.1 手动 舒适型,kairuifc","2010款 1.0 手动 舒适型,kairuifd",
						"2010款 1.1 手动 标准型,kairuife","2010款 1.0 手动 标准型,kairuiff","2010款 1.1 手动 基本型,kairuifg","2010款 1.0 手动 基本型,kairuifh",
						"2011款 1.0 手动 Ⅱ实力Ⅱ型,kairuifi","2011款 1.0 手动 Ⅱ实力Ⅰ型,kairuifj","2011款 1.1 手动 Ⅱ舒适型,kairuifk","2011款 1.1 手动 Ⅱ基本型,kairuifl",
						"2011款 1.1 手动 Ⅱ豪华型,kairuifm","2011款 1.1 手动 Ⅱ标准型,kairuifn","2011款 1.0 手动 Ⅱ舒适型,kairuifo","2011款 1.0 手动 Ⅱ基本型,kairuifp",
						"2011款 1.0 手动 Ⅱ豪华型,kairuifq","2011款 1.0 手动 Ⅱ标准型,kairuifr","2013款 1.0 手动 Ⅱ实力Ⅱ型,kairuifs","2013款 1.0 手动 Ⅱ实力Ⅰ型,kairuift",
						"2014款 1.0 手动 实力型,kairuifu"
						],
						
						
				"优雅":["2007款 1.3 手动 舒适型5-8座,kairuiga","2007款 1.3 手动 豪华型5-8座,kairuigb","2007款 1.3 手动 标准型5-8座,kairuigc",
						"2008款 1.2 手动 5-8座,kairuigd","2009款 1.2 手动 舒适型5-8座,kairuige","2009款 1.2 手动 实力型5-8座,kairuigf",
						"2009款 1.2 手动 标准型5-8座,kairuigg","2012款 1.2 手动 新标准型8座,kairuigh","2012款 1.3 手动 新标准型5座,kairuigi",
						"2012款 1.3 手动 豪华型5座,kairuigj","2012款 1.3 手动 舒适型5座,kairuigk","2012款 1.3 手动 新标准型8座,kairuigl",
						"2012款 1.2 手动 舒适型5座,kairuigm","2012款 1.2 手动 新标准型5座,kairuign","2012款 1.2 手动 新实力型5座,kairuigo",
						"2013款 1.5 手动 优雅2尊贵型5-8座,kairuigp","2013款 1.5 手动 优雅2豪华型5-8座,kairuigq","2013款 1.5 手动 优雅2舒适型5-8座,kairuigr",
						"2013款 1.5 手动 优雅2标准型5-8座,kairuigs","2014款 1.5 手动 优雅2实力型5-8座,kairuigt"
						],
				
				"杰虎":["2015款 2.2 手动 财富版JM491Q-ME,kairuiha","2015款 2.2 手动 创富版JM491Q-ME,kairuihb","2015款 2.2 手动 领航版JM491Q-ME,kairuihc",
						"2015款 2.2 手动 精英版JM491Q-ME,kairuihd"
						],
				
				"绿卡":["2013款 3.7T 手动 T一排半标准型II型YZ4DA2 柴油,kairuiia","2013款 3.7T 手动 T一排半标准型I型YZ4DA2 柴油,kairuiib",
						"2013款 3.7T 手动 T一排半标准型YZ4DA2 柴油,kairuiic","2013款 3.7T 手动 T一排半豪华型YZ4DA2 柴油,kairuiid",
						"2013款 3.7T 手动 T单排豪华型YZ4DA2 柴油,kairuiie","2013款 3.7T 手动 T单排标准型II型YZ4DA2 柴油,kairuiif",
						"2013款 3.7T 手动 T单排标准型I型YZ4DA2 柴油,kairuiig","2013款 3.7T 手动 T单排标准型YZ4DA2 柴油,kairuiih",
						"2013款 2.8T 手动 单排H16豪华型二类WLY5T 柴油,kairuiii","2013款 2.8T 手动 T排半标准型箱货WLY6T100 柴油,kairuiij",
						"2013款 2.8T 手动 T排半标准Ⅰ型二类WLY6T100 柴油,kairuiik","2013款 2.8T 手动 T排半H17标准Ⅰ型二类WLY5T92 柴油,kairuiil",
						"2013款 2.8T 手动 T排半H17标准型二类WLY5T92 柴油,kairuiim","2013款 2.8T 手动 T排半H17豪华型二类WLY5T92 柴油,kairuiin",
						"2013款 2.8T 手动 T单排H16豪华型二类WLY5T92 柴油,kairuiio","2013款 2.8T 手动 T单排H16标准型二类WLY5T92 柴油,kairuiip",
						"2013款 2.8T 手动 T单排H16标准Ⅰ型二类WLY5T92 柴油,kairuiiq","2013款 2.8T 手动 T排半豪华型二类WLY6T100 柴油,kairuiir",
						"2013款 2.8T 手动 T排半标准型二类WLY6T100 柴油,kairuiis","2013款 2.8T 手动 T排半WLY6T100 柴油,kairuiit",
						"2013款 2.8T 手动 T单排豪华型二类WLY6T100 柴油,kairuiiu","2013款 2.8T 手动 T单排标准Ⅰ型二类WLY6T100 柴油,kairuiiv",
						"2013款 2.8T 手动 T单排标准型二类WLY6T100 柴油,kairuiiw","2013款 2.8T 手动 S单排H02豪华型厢货LC5T35 柴油,kairuiix",
						"2013款 2.8T 手动 S排半基本型厢货ZF5S400 柴油,kairuiiy","2013款 2.8T 手动 S排半舒适型厢货ZF5S400 柴油,kairuiiz",
						"2013款 2.8T 手动 S排半豪华型厢货ZF5S400 柴油,kairuiiaa","2013款 2.8T 手动 S单排豪华型厢货ZF5S400 柴油,kairuiiab",
						"2013款 2.8T 手动 S单排舒适型厢货ZF5S400 柴油,kairuiiac","2013款 2.8T 手动 S单排基本型厢货ZF5S400 柴油,kairuiiad",
						"2013款 2.8T 手动 S排半H01舒适型厢货LC5T35 柴油,kairuiiae","2013款 2.8T 手动 S排半H01基本型厢货LC5T35 柴油,kairuiiaf",
						"2013款 2.8T 手动 S排半H01豪华型厢货LC5T35 柴油,kairuiiag","2013款 2.8T 手动 S单排H02舒适型厢货LC5T35 柴油,kairuiiah",
						"2013款 2.8T 手动 S单排H02基本型厢货LC5T35 柴油,kairuiiai","2013款 2.8T 手动 S排半H01基本型厢货ZF5S400 柴油,kairuiiaj",
						"2013款 2.8T 手动 S排半H01舒适型厢货ZF5S400 柴油,kairuiiak","2013款 2.8T 手动 S排半H01豪华型厢货ZF5S400 柴油,kairuiial",
						"2013款 2.8T 手动 S单排H02豪华型厢货ZF5S400 柴油,kairuiiam","2013款 2.8T 手动 S单排H02舒适型厢货ZF5S400 柴油,kairuiian",
						"2013款 2.8T 手动 S单排H02基本型厢货ZF5S400 柴油,kairuiiao"
						],
				
						
//克莱斯勒
							
				"300C":["2005款 6.1 自动 SRT8,chrysleraa","2012款 3.6 自动 豪华版,chryslerab","2013款 3.6 自动 尊享版,chryslerac",
						"2013款 3.6 自动 尊崇版,chryslerad","2013款 3.6 自动 尊适版,chryslerae","2014款 3.0 自动 卓越版,chrysleraf"
						],
				
				"铂锐":["2007款 2.0 自动 舒适型,chryslerba","2007款 2.0 自动 豪华型,chryslerbb","2007款 2.4 自动 豪华型,chryslerbc",				
						"2007款 2.7 自动 豪华型,chryslerbd","2008款 2.0 自动 豪华型,chryslerbe","2008款 2.4 自动 豪华型,chryslerbf",
						"2008款 2.7 自动 豪华型,chryslerbg","2008款 2.0 自动 舒适型,chryslerbh"
						],
				
				"大捷龙":["2008款 3.3 自动 带STOW-N-GO功能商务版,chryslerca","2008款 3.3 自动 带STOW-N-GO功能限量,chryslercb",
						"2008款 3.3 自动 带Captain豪华座椅商务版,chryslercc",
						"2008款 3.3 自动 带Captain豪华座椅限量版,chryslercd","2011款 3.0 自动 经典版,chryslerce","2011款 3.0 自动 豪华版,chryslercf",
						"2011款 3.0 自动 至尊版,chryslercg"
						],
				
				"300C(进口)":["2006款 2.7 自动 豪华型,chryslerjkda","2006款 3.5 自动 豪华型,chryslerjkdb","2006款 5.7 自动 豪华型,chryslerjkdc",
						"2006款 5.7 自动 豪华领航版,chryslerjkdd","2007款 2.7 自动 商务型,chryslerjkde","2008款 2.7 自动 豪华型,chryslerjkdf",
						"2008款 2.7 自动 豪华领航型,chryslerjkdg","2008款 3.5 自动 豪华领航版,chryslerjkdh"
						],
				
				"300S":["2013款 3.6 自动 锋尚版,chryslerea"
						],
				
				"PT漫步者":["2006款 2.4 自动,chryslerfa","2007款 2.4 自动,chryslerfb"
						],
				
				"交叉火力":["2004款 3.2 自动 SRT6,chryslerga","2007款 3.2 自动,chryslergb"
						],
				
				"大捷龙(进口)":["2005款 3.3 自动 航海家,chryslerjkha","2007款 3.8 自动 航海家,chryslerjkhb","2007款 2.4 自动 航海家,chryslerjkhc",
						"2007款 3.3 自动 航海家,chryslerjkhd","2007款 3.3 自动 航海家LX,chryslerjkhe","2008款 3.8 自动,chryslerjkhf",
						"2013款 3.6 自动 豪华版,chryslerjkhg","2014款 3.6 自动 舒适版,chryslerjkhh"
						],
				
				"彩虹":["2011款 2.0 自动 ,chrysleria"
						],
						
				"赛百灵":["2006款 2.4 自动,chryslerja","2006款 2.7 自动,chryslerjb","2006款 3.5 自动,chryslerjc"
						],		
										
				
				
// "柯尼赛格"		
				
				"Agera":["2011款 5.0T 自动 R,koenigseggaa"
						],
				
				
				"CCR":["2004款 4.7T 手动 (806HP),koenigseggba"
						],
				
				"CCX":["2006款 4.7T 手动 基本型,koenigseggca"
						],
				
				"CCXR":["2007款 4.7T 手动 Edition,koenigseggda","2010款 4.8T 手动 Trevita,koenigseggdb"
						],
				
//"卡尔森"							
				
				"GL级":["2012款 4.7 自动 CGL45豪华版,kersaa","2012款 4.7 自动 CGL45皇家版,kersab"
						],
				
				
				"S级":["2012款 5.5T 自动 CS60豪华版,kersba","2012款 5.5T 自动 CS60皇家版,kersbb"
						],
				
				
 //"凯迪拉克":				
				"ATS-L":["2014款 2.0T 自动 28T领先型,cadillacaa","2014款 2.0T 自动 28T豪华型,cadillacab","2014款 2.0T 自动 28T精英型,cadillacac",
						"2014款 2.0T 自动 25T时尚型,cadillacad","2014款 2.0T 自动 25T舒适型,cadillacae","2016款 2.0T 自动 28T领先型,cadillacaf",
						"2016款 2.0T 自动 28T豪华型,cadillacag","2016款 2.0T 自动 28T精英型,cadillacah","2016款 2.0T 自动 25T时尚型,cadillacai",
						"2016款 2.0T 自动 25T舒适型,cadillacaj"
						],
				   
				
				"XTS":["2013款 2.0T 自动 舒适型,cadillacba","2013款 2.0T 自动 领先型,cadillacbb","2013款 2.0T 自动 科技型,cadillacbc",
						"2013款 2.0T 自动 精英型,cadillacbd","2013款 2.0T 自动 豪华型,cadillacbe","2013款 2.0T 自动 典雅型,cadillacbf",
						"2013款 3.6 自动 铂金版,cadillacbg","2014款 2.0T 自动 铂金版,cadillacbh","2014款 2.0T 自动 豪华型,cadillacbi",
						"2014款 2.0T 自动 科技型,cadillacbj","2014款 3.6 自动 铂金版,cadillacbk","2014款 2.0T 自动 舒适型,cadillacbl",
						"2014款 2.0T 自动 精英型,cadillacbm","2014款 2.0T 自动 典雅型,cadillacbn","2015款 2.0T 自动 舒适型,cadillacbo",
						"2015款 2.0T 自动 精英型,cadillacbp","2015款 2.0T 自动 豪华型,cadillacbq","2015款 2.0T 自动 铂金版,cadillacbr"                 
						],
				   
				"CTS":["2005款 3.6 自动,cadillacca","2005款 2.8 自动,cadillaccb"
						],
				                     
				
				"赛威SLS":["2007款 3.6 自动 精英型,cadillacda","2007款 3.6 自动 豪华型,cadillacdb","2007款 2.8 自动 豪华型,cadillacdc","2007款 4.6 自动 旗舰型,cadillacdd",
						"2007款 2.8 自动 精英型,cadillacde","2008款 2.8 自动 精英型,cadillacdf","2008款 2.8 自动 豪华型,cadillacdg","2008款 3.6 自动 豪华型,cadillacdh",
						"2008款 4.6 自动 旗舰型,cadillacdi","2008款 3.6 自动 精英型,cadillacdj","2010款 3.6 自动 运动高性能型,cadillacdk",
						"2010款 3.6 自动 旗舰高性能型,cadillacdl","2010款 3.0 自动 精英运动型,cadillacdm","2010款 3.0 自动 豪华运动型,cadillacdn",
						"2011款 2.0T 自动 行政型,cadillacdo","2011款 2.0T 自动 精英型,cadillacdp","2011款 2.0T 自动 豪华型,cadillacdq","2012款 2.0T 自动 舒适型,cadillacdr"
						],
				   
				
				                  
				"ATS":["2012款 3.6 自动 V6,cadillacea","2012款 3.6 手动 V6,cadillaceb","2012款 2.5 自动,cadillacec","2012款 2.5 手动,cadillaced",
						"2012款 2.0T 自动,cadillacee","2012款 2.0T 手动,cadillacef","2014款 2.0T 自动 领先型,cadillaceg","2014款 2.0T 自动 豪华型,cadillaceh",
						"2014款 2.0T 自动 精英型,cadillacei","2014款 2.0T 自动 舒适型,cadillacej"
						],
				   
				
				
				"CTS(进口)":["2008款 2.8 自动 精英运动版,cadillacfa","2008款 2.8 自动 豪华运动版,cadillacfb","2008款 3.6 自动 旗舰高性能型,cadillacfc",
						"2008款 3.6 自动 精英高性能型,cadillacfd","2008款 6.2T 自动,cadillacfe","2009款 2.8 自动 精英运动版,cadillacff",
						"2009款 2.8 自动 豪华运动版,cadillacfg","2009款 2.8 自动 精英运动天窗版,cadillacfh","2010款 3.6 自动 旗舰高性能版,cadillacfi",
						"2010款 3.6 自动 精英高性能版,cadillacfj","2010款 3.0 自动 精英运动版,cadillacfk","2010款 3.0 自动 豪华运动版,cadillacfl",
						"2010款 3.0 自动 风尚运动版,cadillacfm","2011款 3.6 自动 RWD,cadillacfn","2011款 3.6 手动 RWD,cadillacfo","2011款 3.6 自动 AWD,cadillacfp",
						"2011款 3.6 自动,cadillacfq",
						"2012款 3.0 自动 豪华纪念版Vday,cadillacfr","2012款 3.0 自动 精英纪念版Vday,cadillacfs","2012款 3.0 自动 精英运动版,cadillacft",
						"2012款 3.0 自动 豪华运动版,cadillacfu","2012款 3.0 自动 风尚运动版,cadillacfv","2012款 6.2T 自动 V8,cadillacfw","2012款 6.2T 自动 SC V8,cadillacfx",
						"2014款 2.0T 自动 28T领先版,cadillacfy","2014款 2.0T 自动 28T豪华版,cadillacfz","2014款 2.0T 自动 28T精英版,cadillacfaa"
						],
				   
				
				 
				 
				"SRX":["2006款 3.6 自动 豪华版四驱,cadillacga","2008款 3.6 自动 精英型四驱,cadillacgb","2008款 3.6 自动 豪华型四驱,cadillacgc",
						"2008款 4.6 自动 豪华版四驱,cadillacgd","2009款 4.6 自动 豪华型四驱,cadillacge","2009款 3.6 自动 豪华型四驱,cadillacgf",
						"2009款 3.6 自动 精英版四驱,cadillacgg","2010款 3.0 自动 旗舰版四驱,cadillacgh","2010款 3.0 自动 豪华版四驱,cadillacgi",
						"2011款 3.0 自动 旗舰版四驱,cadillacgj","2011款 3.0 自动 豪华版四驱,cadillacgk",
						"2012款 3.0 自动 舒适版前驱,cadillacgl","2012款 3.0 自动 旗舰版四驱,cadillacgm","2012款 3.0 自动 领先版四驱,cadillacgn",
						"2012款 3.0 自动 精英版前驱,cadillacgo","2012款 3.0 自动 豪华版四驱,cadillacgp",
						"2013款 3.6 自动 旗舰型,cadillacgq","2013款 3.0 自动 旗舰型,cadillacgr","2013款 3.0 自动 领先型,cadillacgs",
						"2013款 3.0 自动 豪华型,cadillacgt","2013款 3.0 自动 精英型,cadillacgu","2013款 3.0 自动 舒适型,cadillacgv",
						"2014款 3.6 自动 旗舰版,cadillacgw","2014款 3.0 自动 旗舰版,cadillacgx","2014款 3.0 自动 领先版,cadillacgy",
						"2014款 3.0 自动 豪华版,cadillacgz","2014款 3.0 自动 精英版,cadillacgaa","2014款 3.0 自动 舒适版,cadillacgab",
						"2015款 3.6 自动 旗舰型四驱,cadillacgac","2015款 3.0 自动 旗舰型四驱,cadillacgad","2015款 3.0 自动 领先型四驱,cadillacgae",
						"2015款 3.0 自动 豪华型四驱,cadillacgaf","2015款 3.0 自动 精英型前驱,cadillacgag","2015款 3.0 自动 舒适型前驱,cadillacgah"
						],
				   
				                         
				"XLR":["2005款 4.4 自动 V,cadillacha","2008款 4.6 自动,cadillachb"
					],
								
								
				"凯雷德":["2006款 6.2 自动 四驱,cadillacia","2006款 6.2 自动,cadillacib","2006款 6.2 自动 四驱,cadillacic","2007款 6.0 自动 白金版四驱,cadillacid","2007款 6.0 自动 四驱,cadillacie",
						"2008款 6.2 自动 限量白金版四驱,cadillacif","2008款 6.0 自动 油电混合,cadillacig","2010款 6.0 自动 油电混合,cadillacih","2011款 6.2 自动 总统一号四驱,cadillacii",
						"2013款 6.2 自动 铂金版加长型,cadillacij"
						],
     				
				
//"凯佰赫"
				
				"战盾":["2011款 8.1 自动 作战版,kaibaiheaa"
					],
				
// "KTM"			
				
				"X-BOW":["2014款 2.0T 手动 GT,ktmaa"
					],
				
				
// "卡威"			

				"K1":["2014款 3.2T 手动 豪华型四驱 柴油,kaweiaa","2014款 3.2T 手动 豪华型前驱 柴油,kaweiab","2014款 2.8T 手动 增值型四驱 柴油,kaweiac",
					"2014款 2.8T 手动 增值型后驱 柴油,kaweiad","2014款 3.2T 手动 舒适型四驱 柴油,kaweiae","2014款 3.2T 手动 舒适型前驱 柴油,kaweiaf",
					"2014款 2.4 手动 舒适型四驱,kaweiag","2014款 2.4 手动 舒适型后驱,kaweiah"
					],
				                  
				
				"W1":["2014款 2.4 手动 时尚型,kaweiba","2014款 2.0 手动 时尚型,kaweibb","2014款 2.4 手动 至尊型,kaweibc",
					"2014款 2.4 手动 豪华型,kaweibd","2014款 2.0 手动 至尊型,kaweibe","2014款 2.0 手动 豪华型,kaweibf"
					],			
				
//"凯马汽车"				
				"凯马皮卡":["2013款 2.8T 手动 加长版后驱 柴油,kaimaaa","2014款 2.2 手动 JM491Q-ME,kaimaab"
					],
				
// "凯翼"
			    
			    "C3":["2015款 1.5 手动 蓝钻版,kaiyiaa","2015款 1.5 手动 黄钻版,kaiyiab","2015款 1.5 手动 金钻版,kaiyiac",
					"2015款 1.5 自动 蓝钻版,kaiyiad","2015款 1.5 自动 黄钻版,kaiyiae","2015款 1.5 自动 金钻版,kaiyiaf"
					],			
				
				"C3R":["2015款 1.5 手动 蓝钻版,kaiyiba","2015款 1.5 手动 黄钻版,kaiyibb","2015款 1.5 手动 金钻版,kaiyibc",
					],
			    
			    
			    
//"康迪"			
				
				"康迪小电跑":["2015款 K10纯电动,kangdiaa"
					],
				
				
				"康迪熊猫":["2015款 K11纯电动,kangdiba"
					],
				
// "雷诺"
		
			"卡缤":["2015款 1.2T 自动 豪华抢鲜版,renaultaa","2015款 1.2T 自动 舒适抢鲜版,renaultab","2015款 1.2T 自动 豪华版,renaultac",
					"2015款 1.2T 自动 舒适版,renaultad","2015款 1.2T 自动 标准版,renaultae"
					],
				
			"塔利斯曼":["2012款 3.5 自动 Nappa行政版,renaultba","2012款 2.5 自动 Nappa旗舰版,renaultbb","2012款 3.5 自动 行政版,renaultbc",
						"2012款 2.5 自动 尊贵版,renaultbd","2012款 2.5 自动 豪华版,renaultbe","2013款 3.5 自动 Nappa行政版,renaultbf",
						"2013款 2.5 自动 Nappa旗舰版,renaultbg","2013款 2.5 自动 豪华版,renaultbh"
						],
				
			"威赛帝":["2005款 3.5 自动,renaultca","2005款 3.0T 自动 柴油,renaultcb"],	
				
				
			"拉古那":["2012款 2.0T 自动 豪华导航版,renaultda","2011款 2.0T 自动 豪华导航版,renaultdab",
					"2011款 2.0T 自动 舒适版,renaultdc","2011款 2.0T 自动 技术版,renaultdd","2009款 2.0T 自动,renaultde",
					"2007款 2.0T 自动,renaultdf","2005款 3.0 自动,renaultdg","2005款 2.0T 自动 ,renaultdh",
					"2005款 2.0 手动,renaultdi","2005款 2.0 自动,renaultdj"
					],	
				
			"拉古那Couple":["2008款 3.5 自动,renaultea","2012款 2.0T 自动 豪华导航版,renaulteb"],
				
			"梅甘娜":["2006款 1.6 自动,renaultfa"],	
				
			"梅甘娜":["2006款 两厢 2.0T 手动,renaultha","2006款 三厢 1.6 自动,renaultgd","2006款 三厢 2.0 自动,renaultge",
			          "2007款 三厢 2.0 自动 豪华型,renaultga","2007款 三厢 2.0 自动 舒适型,renaultgb","2007款 三厢 2.0 自动 标准型,renaultgc",
					  "2008款 两厢 2.0T 手动,renaulthb"
					],
				
			"梅甘娜CC":["2006款 1.6 自动,renaultia","2006款 2.0 自动,renaultib","2006款 2.0T 手动,renaultic",
						 "2010款 2.0T 自动,renaultid","2012款 2.0 自动,renaultie"
						],	
				
			"梅甘娜Couple":["2006款 2.0T 手动,renaultja","2006款 2.0 自动,renaultjb","2006款 1.6 自动,renaultjc","2006款 2.0T 手动 RS,renaultjd",
							"2006款 2.0T 手动 GT,renaultje","2008款 2.0T 手动,renaultjf","2009款 2.0T 手动 RS,renaultjg","2011款 2.0T 手动 RS,renaultjh",
							"2015款 2.0T 手动 赛版RS,renaultji","2015款 2.0T 手动 街版RS,renaultjj"
						  ],
				
				
			"科雷傲":["2008款 2.0T 自动 前驱 柴油,renaultka","2008款 2.5 自动 前驱,renaultkb","2008款 2.5 手动 前驱,renaultkc","2008款 2.0T 手动 前驱 柴油,renaultkd",
					"2009款 2.5 自动 豪华型四驱,renaultke","2009款 2.5 自动 舒适型四驱,renaultkf","2009款 2.5 自动 豪华型前驱,renaultkg","2009款 2.5 自动 舒适型前驱,renaultkh",                  
					"2010款 2.5 自动 舒适型四驱,renaultki","2010款 2.5 自动 豪华型四驱,renaultkj","2010款 2.5 自动 舒适型前驱,renaultkk",
					"2010款 2.5 自动 时尚型前驱,renaultkl","2010款 2.5 自动 豪华型前驱,renaultkm","2011款 2.5 自动 限量版Bose四驱,renaultkn",
					"2011款 2.0T 手动 前驱 柴油,renaultko","2011款 2.0T 自动 前驱 柴油,renaultkp",
					"2012款 2.5 自动 舒适导航型四驱,renaultkq","2012款 2.5 自动 豪华导航型四驱,renaultkr","2012款 2.5 自动 都市型四驱,renaultks",
					"2012款 2.5 自动 舒适导航型前驱,renaultkt","2012款 2.5 自动 豪华导航型前驱,renaultku","2012款 2.5 自动 都市型前驱,renaultkv",
					"2013款 2.5 自动 豪华型四驱,renaultkw","2013款 2.5 自动 豪华型前驱,renaultkx","2013款 2.5 自动 舒适型四驱,renaultky",
					"2013款 2.5 自动 舒适型前驱,renaultkz","2013款 2.5 自动 舒适导航型四驱,renaultkaa","2013款 2.5 自动 豪华导航型四驱,renaultkab",
					"2013款 2.5 自动 都市型四驱,renaultkac","2013款 2.5 自动 舒适导航型前驱,renaultkad","2013款 2.5 自动 豪华导航型前驱,renaultkae",
					"2013款 2.5 自动 都市型前驱,renaultkaf","2014款 2.5 自动 Touring限量版四驱,renaultkag","2014款 2.5 自动 Touring限量版前驱,renaultkah",
					"2014款 2.5 自动 Sportway限量版四驱,renaultkai",
					"2014款 2.5 自动 Sportway限量版前驱,renaultkaj","2014款 2.5 自动 豪华版四驱,renaultkak","2014款 2.5 自动 舒适版四驱,renaultkal",
					"2014款 2.5 自动 舒适版前驱,renaultkam","2014款 2.5 自动 都市版前驱,renaultkan","2014款 2.0 自动 时尚版前驱,renaultkao",
					"2014款 2.0 自动 标准版前驱,renaultkap","2014款 2.0 自动 智享版前驱,renaultkaq",
					"2015款 2.5 自动 豪华版四驱,renaultkar","2015款 2.5 自动 舒适版四驱,renaultkas","2015款 2.5 自动 舒适版前驱,renaultkat",
					"2015款 2.5 自动 都市版前驱,renaultkau","2015款 2.0 自动 舒适版前驱,renaultkav","2015款 2.0 自动 时尚版前驱,renaultkaw",
					"2015款 2.0 自动 标准版前驱,renaultkax","2016款 2.5 自动 舒适运动版四驱,renaultkay","2016款 2.5 自动 舒适运动版前驱,renaultkaz",
					"2016款 2.0 自动 舒适BOSE版前驱,renaultkba","2016款 2.0 自动 舒适版前驱,renaultkbb","2016款 2.0 自动 时尚版前驱,renaultkbc"
					],
			
			
			"纬度":["2011款 2.5 自动 旗舰导航版,renaultla","2011款 2.5 自动 豪华导航版,renaultlb","2011款 2.0 自动 时尚版,renaultlc",
					"2011款 2.0 自动 豪华导航版,renaultld",
					"2013款 2.5 自动 旗舰版,renaultle","2013款 2.5 自动 豪华版,renaultlf","2013款 2.0 自动 豪华版,renaultlg",
					"2013款 2.0 自动 时尚版,renaultlh","2013款 2.0 自动 舒适版,renaultli","2013款 2.0 自动 悦享版,renaultlj",
					"2015款 2.5 自动 旗舰版,renaultlk","2015款 2.5 自动 豪华版,renaultll","2015款 2.0 自动 豪华版,renaultlm",
					"2015款 2.0 自动 时尚版,renaultln"
					],
					
			"风景":["2006款 2.0T 手动 16V,renaultma","2006款 2.0 自动 16V,renaultmb","2006款 2.0 手动 16V,renaultmc","2006款 1.6 手动 16V,renaultmd", 
					"2009款 2.0 自动,renaultme","2011款 2.0 自动 舒适版7座,renaultmf","2011款 2.0 自动 舒适版5座,renaultmg","2011款 2.0 自动 时尚版5座,renaultmh",
					"2012款 2.0 自动 舒适版,renaultmi","2012款 2.0 自动 时尚版,renaultmj","2012款 2.0 自动 豪华导航版,renaultmk","2012款 2.0 自动 舒适版7座,renaultml",
					"2012款 2.0 自动 豪华导航版7座,renaultmm"
					],
			
			"风朗":["2011款 2.0 自动 时尚版,renaultna","2011款 2.0 自动 豪华导航版,renaultnb","2011款 2.0 自动 标准版,renaultnc",
					"2013款 2.0 自动 舒适版,renaultnd","2013款 2.0 自动 悦享版,renaultne","2013款 2.0 自动 豪华版,renaultnf",
					"2013款 2.0 自动 时尚版,renaultng","2013款 2.0 自动 标准版,renaultnh","2015款 2.0 自动 豪华导航版,renaultni",
					"2015款 2.0 自动 时尚版,renaultnj","2015款 2.0 自动 标准版,renaultnk"
					],
			

//"莲花"		
			"L3两厢":["2009款 1.6 手动 时尚型,lianhuaaa","2009款 1.6 手动 豪华型,lianhuaab","2009款 1.6 自动 时尚型,lianhuaac",
					"2009款 1.6 自动 豪华型,lianhuaad","2009款 1.6 自动 运动豪华型,lianhuaae","2009款 1.6 手动 运动豪华型,lianhuaaf",
					"2009款 1.6 自动 运动时尚型,lianhuaag","2009款 1.6 手动 运动时尚型,lianhuaah","2009款 1.6 手动 标准型,lianhuaai",
					"2013款 1.5 自动 精英型,lianhuaaj","2013款 1.5 手动 精致型,lianhuaak","2013款 1.5 手动 精英型,lianhuaal",
					"2013款 1.6 手动 精致型,lianhuaam","2013款 1.6 手动 精英型,lianhuaan","2013款 1.6 自动 精致型,lianhuaao","2013款 1.6 自动 精英型,lianhuaap"
					],                  
			
			                                           
			
			"L3三厢":["2010款 1.6 手动 精英版,lianhuaba","2010款 1.6 手动 豪华版,lianhuabb","2010款 1.6 自动 精英版,lianhuabc","2010款 1.6 自动 豪华版,lianhuabd",
					"2013款 1.5 自动 精英型,lianhuabe","2013款 1.5 手动 精英型,lianhuabf","2013款 1.6 自动 精致型,lianhuabg","2013款 1.6 自动 精英型,lianhuabh",
					"2013款 1.6 手动 精致型,lianhuabi","2013款 1.6 手动 精英型,lianhuabj"
					],                  
			
			                                           
			                                           
			"L5两厢":["2011款 1.6 手动 精智版,lianhuaca","2011款 1.6 手动 风尚版,lianhuacb","2011款 1.6 自动 精智导航版,lianhuacc","2011款 1.6 自动 精智版,lianhuacd",
					"2011款 1.6 自动 风尚版,lianhuace","2012款 1.8 手动 尊贵型,lianhuacf","2012款 1.8 自动 尊贵型,lianhuacg","2012款 1.8 自动 精智导航版,lianhuach",
					"2012款 1.8 自动 精智版,lianhuaci","2012款 1.8 手动 精智版,lianhuacj","2012款 1.8 自动 风尚版,lianhuack","2012款 1.8 手动 风尚版,lianhuacl",
					"2013款 1.6 手动 豪华版,lianhuacm","2013款 1.6 自动 豪华版,lianhuacn","2013款 1.6 自动 劲取版,lianhuaco","2013款 1.6 手动 劲取版,lianhuacp"
					],
			
			   
			   
			
			"L5三厢":["2011款 1.6 手动 精智版,lianhuada","2011款 1.6 手动 风尚导航版,lianhuadb","2011款 1.6 手动 风尚版,lianhuadc","2011款 1.6 自动 精智导航版,lianhuadd",
					"2011款 1.6 自动 精智版,lianhuade","2011款 1.6 自动 风尚导航版,lianhuadf","2011款 1.6 自动 风尚版,lianhuadg","2012款 1.8 手动 尊贵型,lianhuadh",
					"2012款 1.8 自动 尊贵型,lianhuadi","2012款 1.8 自动 精智导航版,lianhuadj","2012款 1.8 手动 精智版,lianhuadk","2012款 1.8 自动 精智版,lianhuadl",
					"2012款 1.8 手动 风尚版,lianhuadm","2012款 1.8 自动 风尚版,lianhuadn","2013款 1.6 自动 精英版,lianhuado"
					],	
				
//"雷克萨斯"		
				


			"雷克萨斯CT":["2010款 1.8 自动 200h,lexusaa","2012款 1.8 自动 200h F-Sport,lexusab","2012款 1.8 自动 200h领先版,lexusac",
				"2012款 1.8 自动 200h精英版,lexusad","2012款 1.8 自动 200h豪华版,lexusae","2013款 1.8 自动 200h花语花忆限量版,lexusaf",
				"2013款 1.8 自动 200h舒适版,lexusag","2013款 1.8 自动 200h都市版,lexusah","2013款 1.8 自动 200h暗夜骑士限量版,lexusai",
				"2014款 1.8 自动 200h F-Sport双色,lexusaj","2014款 1.8 自动 200h精英版双色,lexusak","2014款 1.8 自动 200h舒适版双色,lexusal",
				"2014款 1.8 自动 200h精英版单色,lexusam","2014款 1.8 自动 200h舒适版单色,lexusan","2014款 1.8 自动 200h领先版双色,lexusao",
				"2014款 1.8 自动 200h领先版单色,lexusap","2014款 1.8 自动 200h F-Sport单色,lexusaq","2015款 1.8 自动 200h巧克力特别限量版,lexusar",
				"2015款 1.8 自动 200h暗夜骑士特别限量版,lexusas"
				],
			
			                  
			
			"雷克萨斯ES":["2006款 3.5 自动 350豪华型,lexusba","2007款 3.5 自动 350,lexusbb","2010款 3.5 自动 350尊贵版,lexusbc","2010款 3.5 自动 350豪华型,lexusbd",
				"2010款 3.5 自动 350典雅版,lexusbe","2010款 2.4 自动 240豪华版,lexusbf","2010款 2.4 自动 240典雅版,lexusbg","2012款 2.4 自动 240特别限量版,lexusbh",
				"2013款 2.5 自动 250限量版Mark Levinson,lexusbi","2013款 2.5 自动 300h豪华版 油电混合,lexusbj","2013款 2.5 自动 300h精英版 油电混合,lexusbk",
				"2013款 3.5 自动 350尊贵版,lexusbl","2013款 3.5 自动 350典雅版,lexusbm","2013款 2.5 自动 250豪华版,lexusbn","2013款 2.5 自动 250典雅版,lexusbo",
				"2013款 2.5 自动 250精英版,lexusbp","2014款 2.5 自动 300h舒适版 油电混合,lexusbq","2014款 2.5 自动 250舒适版,lexusbr","2014款 3.5 自动 350尊贵版,lexusbs",
				"2014款 3.5 自动 350典雅版,lexusbt","2014款 2.5 自动 300h豪华版 油电混合,lexusbu","2014款 2.5 自动 300h精英版 油电混合,lexusbv",
				"2014款 2.5 自动 250豪华版,lexusbw","2014款 2.5 自动 250典雅版,lexusbx","2014款 2.5 自动 250精英版,lexusby",
				"2015款 2.5 自动 300h尊贵版 油电混合,lexusbz","2015款 2.5 自动 300h豪华版 油电混合,lexusbaa","2015款 2.5 自动 300h舒适版 油电混合,lexusbab",
				"2015款 2.5 自动 250豪华版,lexusbac","2015款 2.5 自动 250典雅版,lexusbad","2015款 2.0 自动 200舒适版,lexusbae","2015款 2.0 自动 200精英版,lexusbaf"
				],
			                  
			
			
			
			"雷克萨斯GS":["2005款 3.5 自动 350,lexusca","2005款 3.0 自动 300,lexuscb","2005款 3.5 自动 350h 油电混合,lexuscc","2005款 3.0 自动 300豪华型,lexuscd",
				"2005款 4.3 自动 430,lexusce","2005款 3.0 自动 300基本型,lexuscf","2008款 4.6 自动 460,lexuscg","2008款 3.0 自动 300,lexusch","2009款 3.0 自动 300,lexusci",
				"2009款 3.5 自动 350四驱,lexuscj","2009款 3.5 自动 350,lexusck","2009款 4.6 自动 460,lexuscl","2009款 3.5 自动 450h 油电混合,lexuscm",                  
				"2010款 3.5 自动 450h 油电混合,lexuscn","2011款 3.0 自动 300辉煌限量版,lexusco","2012款 3.5 自动 450h 油电混合,lexuscp",
				"2012款 3.5 自动 350豪华版 四驱,lexuscq","2012款 3.5 自动 350 F-SPORT,lexuscr","2012款 2.5 自动 250豪华版,lexuscs",
				"2012款 2.5 自动 250领先版,lexusct","2012款 2.5 自动 250 F-SPORT,lexuscu","2014款 2.5 自动 300h豪华版 油电混合,lexuscv",
				"2014款 2.5 自动 300h领先版 油电混合,lexuscw","2014款 3.5 自动 450h 油电混合,lexuscx","2014款 3.5 自动 350豪华版四驱,lexuscy",
				"2014款 3.5 自动 350 F-SPORT,lexuscz","2014款 2.5 自动 250豪华版,lexuscaa","2014款 2.5 自动 250 F-SPORT,lexuscab",
				"2014款 2.5 自动 250领先版,lexuscac"
				],
			
			
			
			
			"雷克萨斯GX":["2010款 4.6 自动 460,lexusda","2012款 4.0 自动 400,lexusdb","2014款 4.0 自动 400豪华版 ,lexusdc","2014款 4.0 自动 400尊享版 ,lexusdd",
				"2014款 4.0 自动 400尊贵版 ,lexusde"
				],
			                  
			
			
			"雷克萨斯IS":["2005款 2.2 手动 220 柴油,lexusea","2005款 3.5 自动 350,lexuseb","2005款 2.5 手动 250,lexusec","2005款 2.0 自动 200,lexused",
				"2006款 3.0 自动 300豪华版,lexusee","2006款 3.0 自动 300炫动版,lexusef","2007款 3.0 自动 300炫动特别版,lexuseg","2007款 3.0 自动 300,lexuseh",                 
				"2008款 5.0 自动 500F,lexusei","2009款 2.5 自动 250四驱,lexusej","2009款 2.5 手动 250,lexusek","2009款 2.5 自动 250,lexusel",
				"2009款 3.5 自动 350,lexusem","2009款 3.0 自动 300,lexusen","2009款 3.0 自动 300炫动版,lexuseo","2009款 3.0 自动 300豪华版,lexusep",
				"2011款 2.5 自动 250风驰版,lexuseq","2011款 2.5 自动 250豪华版,lexuser","2011款 2.5 自动 250精英版,lexuses",
				"2011款 2.5 自动 250炫动版,lexuset","2011款 2.5 自动 250运动版,lexuseu","2011款 2.5 自动 250特别限量版F-SPORT,lexusev",
				"2011款 2.5 自动 250精英版,lexusew","2013款 2.5 自动 250 F-SPORT,lexusex","2013款 2.5 自动 250尊贵版,lexusey",
				"2013款 2.5 自动 250豪华版,lexusez","2013款 2.5 自动 250锋尚版,lexuseaa","2013款 2.5 自动 250领先版,lexuseab",
				"2015款 2.0T 自动 200t豪华版,lexuseav","2015款 2.0T 自动 200t锋尚版,lexusead","2015款 2.0T 自动 200t领先版,lexuseae",
				"2015款 2.0T 自动 200t F-SPORT,lexuseaf","2015款 2.5 自动 250逐风之翼特别限量版,lexuseag"
				],
			
			
			
			
			"雷克萨斯LFA":["2010款 4.8 自动,lexusfa"
				],
				                  
				
			
			"雷克萨斯LS":["2005款 4.3 自动 430,lexusga","2006款 4.6 自动 460,lexusgb","2006款 4.6 自动 460尊贵加长版,lexusgc","2006款 4.6 自动 460加长版,lexusgd",
				"2007款 5.0 自动 600HL加长版 油电混合,lexusge","2007款 5.0 自动 600HL尊贵加长版四驱 油电混合,lexusgf",                  
				"2010款 5.0 自动 600HL加长版四驱 油电混合,lexusgg","2010款 4.6 自动 460尊贵加长版,lexusgh","2010款 4.6 自动 460加长版,lexusgi",
				"2010款 4.6 自动 460豪华加长版,lexusgj","2010款 4.6 自动 460,lexusgk","2013款 5.0 自动 600HL加长版四驱 油电混合,lexusgl",
				"2013款 4.6 自动 460尊贵加长版四驱,lexusgm","2013款 4.6 自动 460加长版,lexusgn","2013款 4.6 自动 460豪华加长版,lexusgo",
				"2014款 5.0 自动 600HL加长版四驱 油电混合,lexusgp","2014款 4.6 自动 460尊贵加长版四驱,lexusgq","2014款 4.6 自动 460豪华加长版,lexusgr",
				"2014款 4.6 自动 460加长版,lexusgs","2016款 5.0 自动 600HL加长版四驱 油电混合,lexusgt","2016款 4.6 自动 460尊贵加长版四驱,lexusgu",
				"2016款 4.6 自动 460豪华加长版,lexusgv","2016款 4.6 自动 460加长版,lexusgw"
				],
			
			
			
			"雷克萨斯LX":["2005款 4.7 自动 470,lexusha","2007款 5.7 自动 570,lexushb","2008款 5.7 自动 570,lexushc","2012款 5.7 自动 570,lexushd",
				"2013款 5.7 自动 570,lexushe","2016款 5.7 自动 570动感豪华版,lexushf","2016款 5.7 自动 570尊贵豪华版,lexushg"
				],
			
			                  
			
			
			"雷克萨斯NX":["2015款 2.5 自动 300h锋芒版四驱 油电混合,lexusia","2015款 2.5 自动 300h锋致版四驱 油电混合,lexusib","2015款 2.5 自动 300h锋尚版前驱 油电混合,lexusic",
				"2015款 2.0T 自动 200t F-Sport四驱,lexusid","2015款 2.0T 自动 200t锋尚版四驱,lexusie","2015款 2.0 自动 200锋尚版四驱,lexusif","2015款 2.0 自动 200锋行版前驱,lexusig"
				],
				                  
			 
			"雷克萨斯RC":["2015款 5.0 自动 碳纤维版F,lexusja","2015款 5.0 自动 巅峰版F,lexusjb","2015款 5.0 自动 驭峰版F,lexusjc"
			],
			                  
			
			
			"雷克萨斯RX":["2006款 3.5 自动 350四驱,lexuska","2007款 3.3 自动 400h四驱 油电混合,lexuskb","2009款 3.5 自动 350豪华版四驱,lexuskc",
				"2009款 3.5 自动 350典雅版四驱,lexuskd","2009款 3.5 自动 450h四驱 油电混合,lexuske","2009款 3.5 自动 350尊贵版四驱,lexuskf",
				"2011款 2.7 自动 270精英版前驱,lexuskg","2011款 2.7 自动 270豪华版前驱,lexuskh","2011款 2.7 自动 270典雅版前驱,lexuski",
				"2012款 3.5 自动 450h四驱 油电混合,lexuskj","2012款 3.5 自动 350尊贵版四驱,lexuskk","2012款 3.5 自动 350豪华版四驱,lexuskl",
				"2012款 3.5 自动 350典雅版四驱,lexuskm","2012款 3.5 自动 350 F-SPROT四驱,lexuskn","2012款 2.7 自动 270豪华版前驱,lexusko",
				"2012款 2.7 自动 270典雅版前驱,lexuskp","2013款 3.5 自动 350 F-SPROT四驱,lexuskq","2013款 3.5 自动 350尊贵版四驱,lexuskr",
				"2013款 3.5 自动 350豪华版四驱,lexusks","2013款 3.5 自动 350典雅版四驱,lexuskt","2013款 3.5 自动 450h尊享版四驱 油电混合,lexusku",
				"2013款 3.5 自动 450h尊贵版四驱 油电混合,lexuskv","2013款 2.7 自动 270豪华版前驱,lexuskw","2013款 2.7 自动 270典雅版前驱,lexuskx",
				"2013款 2.7 自动 270精英版前驱,lexusky","2013款 2.7 自动 270限量版Mark Levinson前驱,lexuskz","2014款 2.7 自动 270暗夜爵士特别限量版,lexuskaa",
				"2016款 2.0T 自动 200t豪华版四驱,lexuskab","2016款 2.0T 自动 200t F-SPORT四驱,lexuskac","2016款 2.0T 自动 200t典雅版四驱,lexuskad",
				"2016款 2.0T 自动 200t舒适版前驱,lexuskae","2016款 2.0T 自动 200t精英版前驱,lexuskaf","2016款 3.5 自动 450h尊贵版四驱 油电混合,lexuskag",
				"2016款 3.5 自动 450h豪华版四驱 油电混合,lexuskah","2016款 3.5 自动 450h典雅版四驱 油电混合,lexuskai"
				],
			                 
			
			
			
			"雷克萨斯SC":["2006款 4.3 自动 430,lexusla","2008款 4.3 自动 430圆石滩珍藏版,lexuslb"
			],
   
				
//"路特斯"			
				
				
			"Elise":["2008款 1.8 手动 SC(217HP),lutesiaa","2008款 1.8 手动 S(134HP),lutesiab","2008款 1.8 手动 R(189HP),lutesiac","2011款 1.8 手动 R(189HP),lutesiad",
				"2011款 1.8 手动 SC(217HP),lutesiae","2011款 1.8T 手动 R标准版,lutesiaf","2011款 1.8T 手动 SC标准版,lutesiag",
				"2011款 1.8T 手动 SC运动版,lutesiah","2015款 1.8T 手动 S,lutesiai"
				],	
				
			"Evora":["2011款 3.5T 自动 GTE,lutesiba","2011款 3.5 自动 IPS版4座,lutesibb","2011款 3.5 手动 标准版2座,lutesibc",
			    "2011款 3.5 手动 标准版4座,lutesibd","2011款 3.5 手动 运动版4座,lutesibe","2012款 3.5T 自动 GTE中国限量版,lutesibf"
				],		
				
			"Exige":["2008款 1.8T 手动 S(220HP),lutesica","2008款 1.8T 手动 S(240HP),lutesicb","2008款 1.8T 手动 S CUP(257HP),lutesicc",
			    "2011款 1.8T 手动 S240标准版,lutesicd","2011款 1.8T 手动 S260标准版,lutesice","2015款 3.5T 自动 S硬顶版,lutesicf"
				],		
				
			"竞悦":["2009款 1.6 自动 时尚型,lutesida","2009款 1.6 手动 时尚型,lutesidb"
			],	
				
				
			"竞速":["2008款 1.6 手动 RCR豪华型,lutesiea","2008款 1.6 自动 RCR豪华型,lutesieb","2008款 1.6 手动 RCR运动型,lutesiec",
			    "2008款 1.6 自动 RCR运动型,lutesied"
				],		


//"林肯"			
				
			"Blackwood":["2011款 5.4 自动 ,lincolnaa"
						],		
				
				
			"LS":["2000款 3.0 手动,lincolnba","2000款 3.0 自动,lincolnbb","2000款 4.0 手动,lincolnbc",
			    "2000款 4.0 自动,lincolnbd","2003款 3.0 自动,lincolnbe","2003款 3.0 手动,lincolnbf",
			    "2003款 4.0 自动,lincolnbg","2003款 4.0 手动,lincolnbh"
				],		
				
				
			"MKC":["2014款 2.0T 自动 尊享版两驱,lincolnca","2014款 2.0T 自动 尊雅版两驱,lincolncb","2014款 2.0T 自动 尊雅版四驱,lincolncc",
			    "2014款 2.0T 自动 尊耀版四驱,lincolncd","2014款 2.0T 自动 总统 四驱,lincolnce"
				],		
				
				
			"MKS":["2008款 3.7 自动 前驱,lincolnda","2008款 3.7 自动 四驱,lincolndb","2009款 3.5T 自动 四驱,lincolndc",
			    "2009款 3.7T 自动 前驱,lincolndd"
				],	
				
				
			"MKT":["2009款 3.7 自动 前驱,lincolnea","2009款 3.7 自动 四驱,lincolneb","2009款 3.5T 自动 四驱,lincolnec"
				],		
				
				
			"MKX":["2006款 3.5 自动 四驱,lincolnfa","2006款 3.5 自动 前驱,lincolnfb","2010款 3.5 自动 四驱,lincolnfc",
			    "2012款 3.7 自动 美规版 四驱,lincolnfd","2015款 2.0T 自动 尊享版前驱,lincolnfe","2015款 2.0T 自动 尊雅版四驱,lincolnff",
			    "2015款 2.7T 自动 尊耀版四驱,lincolnfg","2015款 2.7T 自动 总统版四驱,lincolnfh"
				],		
				
				
			"MKZ":["2006款 3.5 自动 前驱,lincolnga","2006款 3.5 自动 四驱,lincolngb","2008款 3.5 自动 前驱,lincolngc",
			    "2008款 3.5 自动 四驱,lincolngd","2010款 3.5 自动 四驱,lincolnge","2014款 2.0T 自动 尊享版,lincolngf",
			    "2014款 2.0T 自动 尊雅版,lincolngg","2014款 2.0T 自动 尊耀版,lincolngh","2014款 2.0T 自动 总统,lincolngi"
				],		
			
			
			"城市":["2007款 4.6 自动 V8,lincolnha"
				],	
				
			"领航员":["2005款 5.4 自动 加长版,lincolnia","2006款 5.4 自动,lincolnib","2010款 5.4 自动 加长版,lincolnic",
					  "2010款 5.4 自动,lincolnid"
					],		
				
				
//"铃木"
			
				
			"启悦":["2015款 1.6 手动 舒享型,suzuki1aa","2015款 1.6 手动 乐享型,suzuki1ab","2015款 1.6 自动 舒享型,suzuki1ac",
					"2015款 1.6 自动 乐享型,suzuki1ad","2015款 1.6 自动 尊享型,suzuki1ae"
					],	
				
			"天语SX4两厢":["2006款 1.6 自动,suzukiaa","2006款 1.6 手动,suzukiab","2007款 1.6 手动,suzukiac","2007款 1.6 手动 精英型,suzukiad","2007款 1.6 自动 精英型,suzukiae",
						"2007款 1.6 自动 豪华型,suzukiaf","2007款 1.6 手动 豪华型,suzukiag","2007款 1.6 手动 标准型,suzukiah","2008款 1.6 自动 运动休旅型,suzukiai",
						"2008款 1.6 手动 运动休旅型,suzukiaj","2008款 1.6 手动 都市时尚型,suzukiak","2008款 1.6 自动 都市时尚型,suzukial","2008款 1.6 自动 时尚天骄型,suzukiam",
						"2008款 1.6 手动 时尚天骄型,suzukian","2009款 1.6 自动 豪华型,suzukiao","2009款 1.6 手动 豪华型,suzukiap","2009款 1.8 自动 运动型,suzukiaq",
						"2009款 1.8 手动 运动型,suzukiar","2009款 1.6 自动 运动型,suzukias","2009款 1.6 手动 运动型,suzukiat","2009款 1.6 手动 时尚型,suzukiau",
						"2010款 1.6 手动 冠军限量版,suzukiav","2010款 1.6 自动 冠军限量版,suzukiaw","2011款 1.6 手动 运动型,suzukiax","2011款 1.6 手动 时尚型,suzukiay",
						"2011款 1.6 手动 灵动型,suzukiaz","2011款 1.6 手动 舒适型,suzukiaaa","2011款 1.8 自动 运动型,suzukiaab","2011款 1.8 手动 运动型,suzukiaac",
						"2011款 1.6 自动 运动型,suzukiaad","2011款 1.6 自动 舒适型,suzukiaae","2011款 1.6 自动 灵动型,suzukiaaf","2011款 1.6 手动 运动型增配版,suzukiaag",
						"2012款 1.6 手动 锐骑型,suzukiaah","2012款 1.6 自动 锐骑型,suzukiaai","2012款 1.6 手动 运动型,suzukiaaj",
						"2012款 1.6 手动 舒适型,suzukiaak","2012款 1.6 手动 灵动型,suzukiaal","2012款 1.6 自动 灵动型,suzukiaam",
						"2012款 1.6 自动 舒适型,suzukiaan","2012款 1.6 自动 运动型,suzukiaao","2012款 1.6 手动 锐骑运动型,suzukiaap",
						"2012款 1.8 自动 锐骑运动型,suzukiaaq","2012款 1.8 手动 锐骑运动型,suzukiaar","2012款 1.6 自动 锐骑运动型,suzukiaas",
						"2013款 1.6 自动 酷锐灵动型,suzukiaat","2013款 1.6 手动 酷锐灵动型改款,suzukiaau","2013款 1.6 手动 酷锐运动型改款,suzukiaav",
						"2013款 1.6 自动 酷锐运动型20周年3G智能版,suzukiaaw","2013款 1.6 手动 酷锐运动型20周年3G智能版,suzukiaax",
						"2013款 1.6 自动 酷锐运动型,suzukiaay","2013款 1.6 手动 酷锐运动型,suzukiaaz","2013款 1.6 手动 酷锐灵动型,suzukiaba",
						"2013款 1.6 自动 酷锐舒适型,suzukiabb","2013款 1.6 手动 酷锐舒适型,suzukiabc"
						],	
				
			"天语SX4三厢":["2006款 1.6 手动 精英型,suzukiba","2006款 1.6 手动 豪华型,suzukibb","2006款 1.6 手动 标准型,suzukibc","2006款 1.6 自动 精英型,suzukibd",
						"2006款 1.6 自动 豪华型,suzukibe","2008款 1.6 手动 CNG油气混合,suzukibf","2008款 1.6 自动 悦享版,suzukibg","2008款 1.6 手动 悦享版,suzukibh",
						"2008款 1.6 手动 豪华天窗型,suzukibi","2008款 1.6 手动 豪华经典型,suzukibj","2008款 1.6 自动 豪华天窗型,suzukibk",
						"2008款 1.6 自动 豪华经典型,suzukibl","2008款 1.6 手动 精英型,suzukibm",
						"2008款 1.6 自动 精英型,suzukibn","2009款 1.8 手动 豪华型,suzukibo","2009款 1.8 自动 豪华型,suzukibp","2009款 1.6 自动 豪华型,suzukibq",
						"2009款 1.6 自动 精英型,suzukibr","2009款 1.6 手动 精英型,suzukibs","2010款 1.6 手动 超值版,suzukibt","2010款 1.6 自动 超值版,suzukibu"
						],	
				
			"铃木奥拓":["2007款 0.8 手动 SC标准型,suzukica","2009款 1.0 自动 豪华型,suzukicb","2009款 1.0 手动 豪华型,suzukicc","2009款 1.0 手动 舒适型,suzukicd",
					"2010款 1.0 手动 限量版,suzukice","2010款 1.0 手动 炫酷型,suzukicf","2010款 1.0 自动 炫酷型,suzukicg","2012款 1.0 手动 实用型,suzukich",
					"2012款 1.0 自动 炫酷型,suzukici","2012款 1.0 手动 炫酷型,suzukicj","2013款 1.0 自动 豪华影音型,suzukick","2013款 1.0 手动 实用型,suzukicl",
					"2013款 1.0 手动 舒适型,suzukicm","2013款 1.0 手动 豪华影音型型,suzukicn","2013款 1.0 自动 20周年限量版,suzukico",
					"2013款 1.0 手动 20周年限量版,suzukicp","2013款 1.0 手动 炫酷型,suzukicq","2013款 1.0 自动 炫酷型,suzukicr",
					"2013款 1.0 自动 豪华型,suzukics","2013款 1.0 手动 豪华型,suzukict","2015款 1.0 自动 豪华限定版,suzukicu","2015款 1.0 手动 豪华限定版,suzukicv"
					],	
				
			"尚悦":["2011款 1.8 自动 豪华型,suzukida","2011款 1.8 手动 豪华型,suzukidb","2011款 1.6 手动 舒适型,suzukidc","2011款 1.6 手动 豪华型,suzukidd",
					"2011款 1.6 自动 豪华型,suzukide","2011款 1.6 自动 舒适型,suzukidf","2012款 1.6 手动 实用型升级版,suzukidg","2012款 1.6 自动 实用版,suzukidh",
					"2012款 1.6 自动 舒适型,suzukidi","2012款 1.6 自动 实用性升级版,suzukidj","2012款 1.6 手动 舒适型,suzukidk","2014款 1.6 手动 舒适型 油气混合,suzukidl"
					],	
				
			"羚羊":["2006款 1.3 手动,suzukiea","2005款 1.3 手动,suzukieb","2005款 1.3 手动 CNG油气混合,suzukiec","2007款 1.3 手动,suzukied",
					"2007款 1.3 手动 舒适型,suzukiee","2007款 1.3 手动 标准型,suzukief","2009款 1.3 手动 白金型,suzukieg",
					"2011款 1.3 手动 标准型,suzukieh","2011款 1.3 手动 基本型,suzukiei","2011款 1.3 手动 舒适型,suzukiej",
					"2012款 1.3 手动 舒适型,suzukiek","2012款 1.3 手动 基本型,suzukiel","2012款 1.3 手动 标准型,suzukiem"
					],
				
			"锋驭":["2014款 1.6 自动 尊贵型四驱,suzukifa","2014款 1.6 自动 精英型前驱,suzukifb","2014款 1.6 手动 精英型前驱,suzukifc",
					"2014款 1.6 自动 进取型前驱,suzukifd","2014款 1.6 手动 进取型前驱,suzukife","2015款 1.4T 自动 尊贵型四驱,suzukiff",
					"2015款 1.4T 自动 尊贵型两驱,suzukifg","2015款 1.4T 手动 尊贵型两驱,suzukifh"
					],	
				
				
			"雨燕":["2005款 1.3 手动 舒适型,suzukiha","2005款 1.3 手动 豪华型,suzukihb","2005款 1.3 手动 超豪华型,suzukihc","2005款 1.3 自动 超豪华型,suzukihd",
					"2005款 1.3 动 豪华型,suzukihe","2005款 1.3 手动 超豪华型,suzukihf","2005款 1.3 手动 豪华型,suzukihg","2005款 1.3 手动 舒适型,suzukihh",
					"2006款 1.3 手动 超豪华型周年纪念版,suzukihi","2006款 1.3 自动 超豪华型周年纪念版,suzukihj","2007款 1.3 手动 炫彩版,suzukihk",
					"2007款 1.3 自动 炫乐型,suzukihl","2007款 1.3 手动 炫乐版,suzukihm","2007款 1.5 手动 超炫版炫锐款,suzukihn",
					"2007款 1.5 自动 超炫版炫酷款,suzukiho","2007款 1.5 自动 超炫版炫锐款,suzukihp","2008款 1.5 自动 炫锐型,suzukihq",
					"2008款 1.5 自动 劲乐炫锐版,suzukihr","2008款 1.5 手动 劲乐炫锐版,suzukihs","2008款 1.5 自动 炫酷冬恋限量版,suzukiht",
					"2008款 1.5 自动 炫锐冬恋限量版,suzukihu","2008款 1.5 手动 炫锐冬恋限量版,suzukihv","2008款 1.3 自动 炫乐冬恋限量型,suzukihw",
					"2008款 1.3 手动 炫乐冬恋限量版,suzukihx","2008款 1.3 手动 炫彩冬恋限量版,suzukihy","2008款 1.5 自动 劲乐炫酷版,suzukihz",
					"2008款 1.5 自动 炫锐版,suzukihaa","2009款 1.3 手动 超值版,suzukihab","2009款 1.3 自动 超值版,suzukihac","2009款 1.5 手动 墨客超炫版,suzukihad",
					"2009款 1.5 自动 墨客超炫版,suzukihae","2009款 1.5 自动 爱丽丝超炫版,suzukihaf","2009款 1.5 自动 炫酷双色版,suzukihag",
					"2009款 1.5 自动 炫锐双色版,suzukihah","2009款 1.5 手动 炫锐双色版,suzukihai","2010款 1.5 自动 冠军限量版,suzukihaj",
					"2010款 1.5 手动 冠军限量版,suzukihak","2011款 1.5 自动 标准型,suzukihal","2011款 1.5 手动 标准型,suzukiham",
					"2011款 1.3 自动 时尚版,suzukihan","2011款 1.3 手动 时尚版,suzukihao","2011款 1.5 手动 运动版,suzukihap",
					"2011款 1.5 自动 运动版,suzukihaq","2011款 1.3 手动 超值版,suzukihar","2012款 1.5 自动 标准型,suzukihas",
					"2012款 1.5 手动 标准型,suzukihat","2012款 1.3 手动 超值版,suzukihau","2013款 1.3 手动 超值版,suzukihav",
					"2013款 1.5 自动 20周年限量型,suzukihaw","2013款 1.5 手动 20周年限量型,suzukihax",
					"2013款 1.5 自动 运动型,suzukihay","2013款 1.5 手动 运动型,suzukihaz","2013款 1.5 自动 标准型,suzukihba",
					"2013款 1.5 手动 标准型,suzukihbb","2014款 1.5 自动 时尚型,suzukihbc","2014款 1.5 手动 时尚型,suzukihbd",
					"2014款 1.3 手动 标准型,suzukihbe","2015款 1.5 手动 时尚型限定版,suzukihbf","2015款 1.5 自动 时尚型限定版,suzukihbg"
					],
			
			
			"利亚纳两厢":["2006款 1.6 自动 STD标准型,suzukiia","2006款 1.6 手动 DLX豪华型,suzukiib","2006款 1.6 自动 DLX豪华型,suzukiic","2006款 1.6 手动 STD标准型,suzukiid",
						"2006款 1.6 手动 EC实用型,suzukiie","2007款 1.6 手动 STD标准型,suzukiif","2007款 1.6 自动 STD标准型,suzukiig","2007款 1.6 自动 DLX豪华型,suzukiih",
						"2008款 1.6 自动 豪华型,suzukiii","2008款 1.6 自动 标准型,suzukiij","2008款 1.6 手动 豪华型,suzukiik","2008款 1.6 手动 标准型,suzukiil",
						"2008款 1.6 手动 实用型,suzukiim","2008款 1.6 自动 EC实用型,suzukiin","2008款 1.6 手动 STD舒适型,suzukiio","2008款 1.6 自动 STD舒适型,suzukiip",
						"2008款 1.6 手动 DLX豪华型,suzukiiq","2008款 1.6 自动 DLX豪华型,suzukiis","2011款 1.6 自动 豪华型,suzukiit","2011款 1.6 自动 标准型,suzukiiu",
						"2011款 1.6 自动 特别版,suzukiiv","2011款 1.4 手动 豪华Ⅰ型A,suzukiiw","2011款 1.4 手动 豪华Ⅱ型A,suzukiix","2011款 1.4 手动 标准型A,suzukiiy",                 
						"2012款 1.6 手动 豪华型,suzukiiz","2012款 1.6 手动 标准型,suzukiiaa","2012款 1.4 手动 经济型A,suzukiiab","2012款 1.4 手动 超豪华型A,suzukiiac",
						"2012款 1.4 手动 豪华型A,suzukiiad","2012款 1.4 手动 舒适型A,suzukiiae","2013款 1.4 手动 豪华型A,suzukiaf","2013款 1.4 手动 标准型A,suzukiiag",
						"2013款 1.4 手动 标准型2,suzukiiah"
						],
			
			
			"利亚纳三厢":["2005款 1.6 手动 DLX豪华型,suzukija","2005款 1.6 手动 STD标准型,suzukijb","2005款 1.6 自动 STD标准型,suzukijc",
						"2005款 1.6 手动 DLX特别型,suzukijd","2005款 1.6 自动 DLX特别型,suzukije","2006款 1.6 手动 EC实用型,suzukijf",
						"2007款 1.6 自动 DLX特别型,suzukijg","2008款 1.6 手动 STD标准型,suzukih","2008款 1.6 手动 EC实用型,suzukiji",
						"2008款 1.6 自动 EC实用型,suzukijj","2008款 1.6 手动 STD舒适型,suzukijk","2008款 1.6 自动 STD舒适型,suzukijl",
						"2008款 1.6 手动 DLX豪华型,suzukijm","2008款 1.6 自动 DLX豪华型,suzukijn","2011款 1.4 手动 豪华Ⅰ型A,suzukijo",
						"2011款 1.4 手动 豪华Ⅱ型A,suzukijp","2011款 1.4 手动 标准型A,suzukijq","2012款 1.4 手动 超豪华型A,suzukijr",
						"2012款 1.4 手动 豪华型A,suzukijs","2012款 1.4 手动 经济型A,suzukijt","2012款 1.4 手动 舒适型A,suzukiju"
						],
									
			
			"利亚纳A6两厢":["2014款 1.4 手动 畅想型,suzukika","2014款 1.5 自动 理想型,suzukikb","2014款 1.5 自动 畅想型,suzukikc","2014款 1.4 手动 理想型,suzukikd",
						"2015款 1.5 自动 理想型,suzukike","2015款 1.5 自动 畅想型,suzukikf","2015款 1.4 手动 理想型,suzukikg","2015款 1.4 手动 畅想型,suzukikh",
						"2015款 1.5 自动 梦想型,suzukiki","2015款 1.4 手动 梦想型,suzukikj","2015款 1.4 手动 幸福型,suzukikk"
						],
			
			
			"利亚纳A6三厢":["2014款 1.4 手动 幸福型,suzukila","2014款 1.5 自动 理想型,suzukilb","2014款 1.5 自动 畅想型,suzukilc","2014款 1.4 手动 梦想型,suzukild",
							"2014款 1.4 手动 畅想型,suzukile","2015款 1.5 自动 理想型,suzukilf","2015款 1.5 自动 畅想型,suzukilg","2015款 1.4 手动 幸福型,suzukilh",
							"2015款 1.4 手动 梦想型,suzukili","2015款 1.4 手动 畅想型,suzukilj","2015款 1.5 自动 梦想型,suzukilk","2015款 1.4 手动 理想型,suzukill"
							],
			
			"昌铃王":["2005款 1.0 手动 ED,suzukima"
					],	

					
					
			"浪迪":["2007款 1.4 手动 标准Ⅱ型后驱,suzukina","2007款 1.4 手动 经济型后驱,suzukinb","2007款 1.4 手动 标准Ⅱ型四驱,suzukinc",
					"2007款 1.4 手动 标准Ⅰ型后驱,suzukind","2007款 1.4 手动 标准Ⅰ型四驱,suzukine","2007款 1.4 手动 超豪华型后驱,suzukinf",
					"2007款 1.4 自动 超豪华型后驱,suzuking","2007款 1.4 手动 豪华型后驱,suzukinh","2007款 1.4 自动 豪华型后驱,suzukini",
					"2009款 1.4 手动 阳光版标准型后驱,suzukinj","2009款 1.4 手动 阳光版舒适型后驱,suzukink","2009款 1.4 手动 四驱,suzukinl",                  
					"2010款 1.2 手动 舒适型阳光版,suzukinm","2010款 1.2 手动 标准型阳光版,suzukinn"
					],
					
					
			"派喜":["2012款 1.4 手动 豪华型,suzukioa","2012款 1.4 手动 精英型,suzukiob","2012款 1.4 手动 尊贵型,suzukioc"
					],
			
			
			"凯泽西":["2010款 2.4 自动 豪华导航版四驱,suzukipa","2010款 2.4 自动 豪华导航版,suzukipb","2010款 2.4 自动 运动导航版四驱,suzukipc",
					"2010款 2.4 自动 豪华版四驱,suzukipd","2010款 2.4 自动 豪华版,suzukipe","2010款 2.4 手动 标准版,suzukipf",
					"2011款 2.4 自动 运动导航版四驱,suzukipg","2011款 2.4 手动 标准版,suzukiph","2011款 2.4 自动 豪华导航版四驱,suzukipi",
					"2011款 2.4 自动 运动导航版,suzukipj","2011款 2.4 自动 豪华导航版,suzukipk","2011款 2.4 自动 标准版,suzukipl"
					],
			
			"吉姆尼":["2005款 1.5T 手动 柴油,suzukiqa","2005款 1.3 手动 敞篷版,suzukiqb","2005款 1.3 自动 敞篷版,suzukiqc","2005款 1.5T 手动 敞篷版 柴油,suzukiqd",
					"2005款 1.3 自动,suzukiqe","2005款 1.3 手动,suzukiqf","2007款 1.3 自动,suzukiqg","2007款 1.3 手动,suzukiqh","2009款 1.3 自动 时尚型,suzukiqi",
					"2009款 1.3 手动 时尚型,suzukiqj","2011款 1.3 自动 双色版,suzukiqk","2011款 1.3 手动,suzukiql","2011款 1.3 自动,suzukiqm","2011款 1.3 手动 双色版,suzukiqn",
					"2011款 1.3 自动 双色导航版,suzukiqo","2011款 1.3 手动 双色导航版,suzukiqp","2011款 1.3 自动 导航版,suzukiqq","2011款 1.3 手动 导航版,suzukiqr","2012款 1.3 自动 Mode3导航版,suzukiqs",
					"2012款 1.3 自动 JLX导航版,suzukiqt","2012款 1.3 手动 Mode3导航版,suzukiqu","2012款 1.3 手动 JLX导航版,suzukiqv",
					"2012款 1.3 自动 JLX,suzukiqw","2012款 1.3 手动 Mode3,suzukiqx","2012款 1.3 自动 Mode3,suzukiqy","2012款 1.3 手动 JLX,suzukiqz",
					"2015款 1.3 自动 Mode3导航版,suzukiqaa","2015款 1.3 自动 JLX导航版,suzukiqab","2015款 1.3 自动 JLX,suzukiqac","2015款 1.3 手动 Mode3导航版,suzukiqad",
					"2015款 1.3 手动 JLX导航版,suzukiqae","2015款 1.3 手动 JLX,suzukiqaf"
					],
			
			"超级维特拉":["2005款 2.7 自动 五门版,suzukira","2005款 2.0 自动 五门版,suzukirb","2005款 2.0 手动 五门版,suzukirc",
						"2007款 2.0 自动 豪华型五门,suzukird","2007款 2.0 手动 豪华型五门,suzukire","2007款 2.0 自动 舒适型五门,suzukirf",
						"2007款 2.0 手动 舒适型五门,suzukirg","2007款 1.6 手动 基本型三门,suzukirh","2008款 3.2 自动 JLX EL五门版,suzukiri",
						"2008款 2.4 自动 JLX EL五门版,suzukirj","2008款 2.4 手动 JLX EL五门版,suzukirk","2008款 2.4 自动 JLX五门版,suzukirl",
						"2008款 2.4 自动 JX EH三门版,suzukirm","2008款 2.4 手动 JX三门版,suzukirn","2011款 2.4 手动 JLX EL五门版,suzukiro",
						"2011款 2.4 自动 JLX EL五门限量版,suzukirp","2011款 2.4 自动 JX EH三门版,suzukirq","2012款 2.4 自动 豪华导航五门版,suzukirr",
						"2012款 2.4 手动 豪华导航五门版,suzukirs","2011款 2.4 自动 JLX EL五门版,suzukirt"
						],
			
			
			"速翼特":["2012款 1.6 自动 五门版,suzukisa","2014款 1.6 手动 豪华版,suzukisb","2014款 1.6 自动 豪华版,suzukisc"
					],
			
			
			"北斗星":["2005款 1.0 手动 4S实用型,suzukita","2005款 1.0 手动 5S豪华型,suzukitb","2005款 1.0 手动 3S简约型,suzukitc","2005款 1.0 手动 2S经济型,suzukitd",
					"2006款 1.4 手动 STD标准型,suzukite","2006款 1.4 手动 ES实用型,suzukitf","2006款 1.4 手动 EC经济型,suzukitg","2006款 1.4 手动 DLX豪华型,suzukith",
					"2008款 1.4 自动 竞驭,suzukiti","2008款 1.0 手动 领航版,suzukitj","2008款 1.0 手动 5S豪华型,suzukitk","2008款 1.0 手动 4S实用型,suzukitl",
					"2008款 1.0 手动 3S简约型,suzukitm","2008款 1.0 手动 2S经济型,suzukitn","2008款 1.4 手动 限量型,suzukito","2008款 1.4 手动 ES实用型,suzukitp",
					"2008款 1.4 手动 STD标准型,suzukitq","2008款 1.4 手动 EC经济型,suzukitr","2008款 1.4 手动 DLX豪华型,suzukits","2009款 1.4 手动 ES实用型,suzukitt",
					"2009款 1.4 手动 EC经济型,suzukitu","2009款 1.4 手动 E,suzukitv","2009款 1.0 手动 领航版,suzukitw","2009款 1.4 手动 DLX豪华型,suzukitx",
					"2009款 1.4 手动 STD标准型,suzukity","2010款 1.4 手动 E,suzukitz","2010款 1.0 手动 领航版,suzukitaa","2010款 1.4 自动 E,suzukitab","2010款 1.4 自动 竞驭,suzukitac",
					"2011款 1.4 手动 标准2型,suzukitad","2011款 1.4 手动 标准1型,suzukitae","2011款 1.4 自动 全能版,suzukitaf","2012款 1.4 手动 冠军版实用型,suzukitag",
					"2012款 1.4 手动 X5巡航型,suzukitah","2012款 1.4 手动 X5豪华型,suzukitai","2012款 1.4 手动 全能版标准型,suzukitaj","2012款 1.4 手动 E标准型,suzukitak",
					"2012款 1.4 手动 X5尊贵型,suzukital","2013款 1.0 手动 E精英型,suzukitam","2013款 1.4 手动 全能升级版标准型,suzukitan","2013款 1.4 手动 全能版实用型,suzukitao",
					"2013款 1.0 手动 创业版舒适型,suzukitap","2013款 1.0 手动 创业版实用3型,suzukitaq","2013款 1.0 手动 E创业版经济型,suzukitar","2015款 1.0 手动 X5精英版,suzukitas",
					"2015款 1.0 手动 舒适型,suzukitat","2015款 1.0 手动 实用型,suzukitau","2015款 1.0 手动 经济型,suzukitav","2015款 1.4 手动 全能升级型,suzukitaw",
					"2015款 1.4 手动 全能型,suzukitax"
					],
			
// "力帆"

		"320":["2009款 1.3 手动 灵逸版,lifanaa","2009款 1.3 手动 灵炫版,lifanab","2009款 1.3 手动 灵动版,lifanac","2009款 1.3 手动 典藏版,lifanad",
				"2009款 1.3 手动 标准型,lifanae","2009款 1.3 手动 舒适型,lifanaf","2009款 1.3 手动 豪华型,lifanag","2011款 1.3 自动 尊贵型,lifanah",
				"2011款 1.3 自动 豪华型,lifanai","2011款 1.3 自动 标准型,lifanaj","2011款 1.3 手动 雪地版舒适型,lifanak","2011款 1.3 手动 雪地版豪华型,lifanal",
				"2011款 1.3 手动 雪地版标准型,lifanam","2012款 1.3 自动 冠军版标准型,lifanan","2012款 1.3 手动 冠军版舒适型,lifanao",
				"2012款 1.3 手动 冠军版旗舰型,lifanap","2012款 1.3 手动 冠军版豪华型,lifanaq","2012款 1.3 手动 冠军版标准型,lifanar",
				"2012款 1.3 自动 冠军版尊贵型,lifanas","2012款 1.3 自动 冠军版舒适型,lifanat","2012款 1.3 自动 冠军版旗舰型,lifanau"
				],
		    
		    
		
		"330":["2014款 1.3 自动 旗舰型,lifanba","2014款 1.3 自动 豪华型,lifanbb","2014款 1.3 自动 标准型,lifanbc","2014款 1.3 手动 豪华型,lifanbd",
			"2014款 1.3 手动 舒适型,lifanbe","2014款 1.3 手动 标准型,lifanbf","2015款 EV豪华型 纯电动,lifanbg","2015款 EV标准型 纯电动,lifanbh"
			],
			                  
		
		
		"520两厢":["2007款 1.3 手动 ,lifanca","2007款 1.6 手动 LX舒适型,lifancb","2007款 1.6 手动 EX豪华型,lifancc","2007款 1.6 手动 DX经济型,lifancd",
				"2008款 1.6 手动 VIP顶级型,lifance","2008款 1.3 手动 DX经济型,lifancf","2008款 1.3 手动 EX豪华型,lifancg","2008款 1.3 手动 LX舒适型,lifanch",                  
				"2009款 1.3 手动 LX舒适型,lifanci","2009款 1.3 手动 EX豪华型,lifancj","2009款 1.3 手动 DX经济型,lifanck",
				"2010款 1.3 手动 LX舒适型,lifancl","2010款 1.3 手动 EX豪华型,lifancm","2010款 1.3 手动 DX经济型,lifancn","2010款 1.3 手动 DX标准型,lifanco",
				"2011款 1.3 手动 DX舒适型,lifancp","2011款 1.3 手动 DX实用型,lifancq","2012款 1.3 手动 DX悦行版舒适型,lifancr","2012款 1.3 手动 DX悦行版实用型,lifancs"
				],
		
		
		
		"520三厢":["2006款 1.6 手动 EX豪华型,lifanda","2006款 1.6 手动 LX舒适型,lifandb","2006款 1.6 手动 DX标准型,lifandc",
				"2007款 1.6 手动 CNG油气混合,lifandd","2007款 1.3 手动 DX标准型,lifande","2007款 1.3 手动 CNG,lifandf","2007款 1.6 手动 VIP顶级型,lifandg",
				"2007款 1.6 手动 LX舒适型,lifandh","2007款 1.6 手动 EX豪华型,lifandi","2007款 1.6 手动 DX经济型,lifandj","2007款 1.3 手动 LX舒适型,lifandk",
				"2007款 1.3 手动 EX豪华型,lifandl","2007款 1.3 手动 DX经济型,lifandm","2008款 1.3 手动 DX经济型,lifandn",
				"2008款 1.6 手动 CNG油气混合,lifando","2008款 1.3 手动 CNG,lifandp","2008款 1.3 手动 LX舒适型,lifandq","2008款 1.3 手动 EX豪华型,lifandr",
				"2009款 1.6 手动 CNG油气混合,lifands","2009款 1.3 手动 CNG,lifandt","2010款 1.3 手动 LX舒适型,lifandu","2010款 1.3 手动 EX豪华型,lifandv",
				"2010款 1.3 手动 DX经济型,lifandw","2011款 1.3 手动 领航版基本型,lifandx","2011款 1.3 手动 领航版实用型,lifandy",
				"2011款 1.3 手动 领航版舒适型,lifandz","2011款 1.5 手动 领航版豪华型,lifandaa","2011款 1.5 手动 领航版舒适型,lifandab"
				],
		
		
		
		
		"530":["2014款 1.5 手动 旗舰型,lifanea","2014款 1.5 手动 豪华型,lifaneb","2014款 1.5 手动 标准型,lifanec","2014款 1.5 自动 旗舰型,lifaned",
				"2014款 1.5 自动 豪华型,lifanee","2014款 1.5 自动 标准型,lifanef","2014款 1.3 手动 旗舰型,lifaneg","2014款 1.3 手动 豪华型,lifaneh",
				"2014款 1.3 手动 标准型,lifanei"
				],
		 
		
		
		
		"620":["2008款 1.6 手动 经济型,lifanfa","2008款 1.6 手动 豪华型,lifanfb","2009款 1.6 手动 CNG油气混合,lifanfc","2009款 1.6 手动 精锐型,lifanfd",
				"2009款 1.6 手动 舒适型,lifanfe","2010款 1.5 手动 精典型,lifanff","2010款 1.6 手动 精钻型,lifanfg","2010款 1.6 手动 铂尊版,lifanfh",
				"2010款 1.6 自动 精钻型,lifanfi","2010款 1.6 自动 精锐型,lifanfj","2010款 1.5 手动 精锐型 CNG油气混合,lifanfk",
				"2010款 1.5 手动 精英型 CNG油气混合,lifanfl","2010款 1.5 手动 贺岁版,lifanfm","2011款 1.5 手动 雪地版精英型,lifanfn",
				"2011款 1.5 手动 雪地版精锐型,lifanfo",
				"2012款 1.6 自动 旗舰型,lifanfp","2012款 1.5 手动 旗舰型 CNG油气混合,lifanfq","2012款 1.5 手动 豪华型 CNG油气混合,lifanfr",
				"2012款 1.5 手动 舒适B型 CNG油气混合,lifanfs","2012款 1.5 手动 舒适A型 CNG油气混合,lifanft","2012款 1.5 手动 旗舰型,lifanfu",
				"2012款 1.5 手动 豪华型,lifanfv","2012款 1.5 手动 舒适C型,lifanfw","2012款 1.5 手动 舒适B型,lifanfx","2012款 1.5 手动 舒适A型,lifanfy",
				"2015款 EV换电版 纯电动,lifanfz","2015款 EV充电版 纯电动,lifanfaa"
				],
		   
		   
		   
		"630":["2014款 1.5 自动 豪华型,lifanga","2014款 1.5 自动 舒适型,lifangb","2014款 1.5 手动 旗舰型,lifangc","2014款 1.5 手动 标准型,lifangd",
			"2014款 1.5 手动 舒适B型,lifange","2014款 1.5 手动 舒适A型,lifangf"
			],
			 
		 
		 
		"720":["2013款 1.5 手动 豪华型,lifanha","2013款 1.5 手动 标准型,lifanhb","2013款 1.8 手动 标准型,lifanhc","2013款 1.8 手动 豪华型,lifanhd",
				"2014款 1.5 手动 豪华型,lifanhe","2014款 1.5 手动 标准型,lifanhf","2014款 1.8 手动 舒雅豪华型,lifanhg","2014款 1.8 手动 舒雅标准型,lifanhh",
				"2015款 1.8 自动 尊贵型,lifanhi","2015款 1.8 自动 豪华型,lifanhj","2015款 1.5 手动 尊贵型,lifanhk","2015款 1.5 手动 旗舰型,lifanhl",
				"2015款 1.5 手动 豪华型,lifanhm","2015款 1.5 手动 舒适型,lifanhn"
				], 
			
		"T21":["2013款 1.3 手动 标准型,lifania","2013款 1.3 手动 舒适型,lifanib"],	
		
		"X50":["2014款 1.5 自动 尊贵型,lifanja","2014款 1.5 自动 豪华型,lifanjb","2014款 1.5 自动 精英型,lifanjc","2014款 1.5 手动 尊贵型,lifanjd",
				"2014款 1.5 手动 豪华型,lifanje","2014款 1.5 手动 精英型,lifanjf"
				],
		                  
		
		"X60":["2011款 1.8 手动 豪华型,lifanka","2011款 1.8 手动 标准型,lifankb","2012款 1.8 手动 标准型,lifankc","2012款 1.8 手动 基本型,lifankd",
				"2012款 1.8 手动 发现版豪华型,lifanke","2012款 1.8 手动 发现版舒适型,lifankf","2013款 1.8 手动 冠军版,lifankg",
				"2015款 1.8 手动 舒适型,lifankh","2015款 1.8 手动 豪华型,lifanki","2015款 1.8 自动 豪华型,lifankj","2015款 1.8 自动 舒适型,lifankk"
				],
				                  
		
		"丰顺":["2009款 1.3 手动,lifanla","2010款 1.3 手动 CNG,lifanlb","2010款 1.3 手动,lifanlc","2011款 1.0 手动 标准型LF465Q5,lifanld",
				"2011款 1.0 手动 油气混合,lifanle","2011款 1.0 手动 舒适型LF466Q,lifanlf","2011款 1.0 手动 舒适型LF465Q5,lifanlg",
				"2014款 1.3 手动 箱货舒适型,lifanlh","2014款 1.3 手动 箱货标准型,lifanli"
				],                 
		
		
		"乐途":["2015款 1.5 手动 S标准型,lifanma","2015款 1.5 手动 S基本型,lifanmb","2015款 1.2 手动 纪念版舒适型,lifanmc","2015款 1.2 手动 纪念版标准型,lifanmd",
				"2015款 1.5 手动 S-VIP,lifanme","2015款 1.5 手动 S豪华型,lifanmf","2015款 1.5 手动 S舒适型,lifanmg","2015款 1.2 手动 舒适型,lifanmh",
				"2015款 1.2 手动 标准型,lifanmi","2015款 1.2 手动 基本型,lifanmj"
				],
		                  
		
		"兴顺":["2011款 1.3 手动 舒适型 油气混合,lifanna","2011款 1.3 手动 基本型 油气混合,lifannb","2011款 1.3 手动 豪华型 油气混合,lifannc",
				"2011款 1.3 手动 标准型 油气混合,lifannd","2011款 1.0 手动 舒适型,lifanne","2011款 1.0 手动 基本型,lifannf",
				"2011款 1.0 手动 豪华型,lifanng","2011款 1.0 手动 标准型,lifannh","2011款 1.3 手动 豪华型,lifanni",
				"2011款 1.3 手动 舒适型,lifannj","2011款 1.3 手动 标准型,lifannk","2011款 1.3 手动 基本型,lifannl"
				],
		                  
		
		"力帆820":["2015款 2.4 自动 旗舰型,lifanoa","2015款 2.4 自动 豪华型,lifanob","2015款 1.8 手动 旗舰型,lifanoc","2015款 1.8 手动 豪华型,lifanod",
			"2015款 1.8 手动 舒适型,lifanoe"
			],
		                  
		
		"福顺":["2012款 1.0 手动 LF465Q5A发动机,lifanpa","2012款 1.0 手动,lifanpb"
			],
    	
	
/// "路虎"
		
		"卫士":["2007款 2.4T 手动 90 柴油,landroveaa","2007款 2.4T 手动 110 柴油,landroveab","2010款 2.4T 手动 冰火限量冰版 柴油,landroveac",
				"2010款 2.4T 手动 冰火限量火版 柴油,landrovead"
				],
		
		
		
		"发现":["2009款 2.7T 自动 S 柴油,landroveba","2005款 4.4 自动 HSE,landrovebb","2005款 4.0 自动 HSE,landrovebc","2010款 5.0 自动 HSE,landrovebd",
				"2010款 4.0 自动 HSE,landrovebe","2010款 3.0T 自动 HSE 柴油,landrovebf","2010款 2.7T 自动 S 柴油,landrovebg","2011款 5.0 自动 黑白限量版,landrovebh",
				"2011款 5.0 自动 HSE,landrovebi","2011款 4.0 自动 HSE,landrovebj","2011款 3.0T 自动 SD HSE 柴油,landrovebk","2011款 2.7T 自动 HSE 柴油,landrovebl",
				"2011款 2.7T 自动 S 柴油,landrovebm","2012款 5.0 自动 HSE奢朗限量版,landrovebn","2012款 3.0T 自动 HSE奢朗限量版SD 柴油,landrovebo",
				"2012款 3.0T 自动 HSE 柴油,landrovebp","2012款 5.0 自动 SE,landrovebq","2012款 5.0 自动 HSE,landrovebr","2013款 3.0T 自动 HSE 7座 柴油,landrovebs",
				"2013款 5.0 自动 尊黑限量版,landrovebt","2013款 3.0T 自动 尊黑限量版 柴油,landrovebu","2013款 5.0 自动 SE,landrovebv","2013款 5.0 自动 HSE,landrovebw",
				"2013款 3.0T 自动 HSE 柴油,landrovebx","2014款 3.0T 自动 25周年纪念版XXV-Edition,landroveby","2014款 3.0T 自动 SC HSE Luxury,landrovebz",
				"2014款 3.0T 自动 SC HSE,landrovebaa","2014款 3.0T 自动 SC SE,landrovebab","2014款 3.0T 自动 SD HSE 柴油,landrovebac",
				"2015款 3.0T 自动 HSE-Lusury,landrovebad","2015款 3.0T 自动 HSE,landrovebae","2015款 3.0T 自动 SE,landrovebaf","2015款 3.0T 自动 SD 柴油,landrovebag"
				],
		
		                  
		"发现神行":["2015款 2.0T 自动 HSE Luxury,landroveca","2015款 2.0T 自动 SE,landrovecb","2016款 2.0T 自动 HSE LUXURY,landrovecc","2016款 2.0T 自动 HSE,landrovecd",
				"2016款 2.0T 自动 SE,landrovece","2016款 2.0T 自动 S,landrovecf"
				],
				
		
		"揽胜":["2005款 4.4 自动,landroveda","2005款 4.2T 自动 SC,landrovedb","2005款 2.9T 自动 柴油,landrovedc","2005款 3.6T 自动 柴油,landrovedd",
				"2007款 4.2T 自动 ,landrovede","2007款 4.4 自动 ,landrovedf","2009款 4.4 自动 HSE,landrovedg","2009款 4.2T 自动 SC,landrovedh","2009款 3.6T 自动 柴油,landrovedi",
				"2010款 3.6T 自动 HSE 柴油,landrovedj","2010款 5.0T 自动 SC创世尊崇版,landrovedk","2010款 5.0T 自动 HSE SC,landrovedl","2010款 5.0 自动 HSE NA,landrovedm",
				"2011款 5.0T 自动 SC尊崇创世版,landrovedn","2011款 5.0T 自动 SC,landrovedo","2011款 5.0 自动 HSE NA,landrovedp","2011款 4.4T 自动 柴油,landrovedq",
				"2011款 3.6T 自动 柴油,landrovedr","2012款 5.0T 自动 SC巅峰创世典藏版,landroveds","2012款 3.6T 自动 柴油,landrovedt","2012款 5.0T 自动 SC尊崇创世版,landrovedu",
				"2012款 5.0T 自动 SC,landrovedv","2012款 5.0 自动 NA,landrovedw","2012款 4.4T 自动 柴油,landrovedx","2013款 3.0T 自动 Vogue SC SE创世加长版小贸车,landrovedy",
				"2013款 3.0T 自动 Vogue SE SC,landrovedz","2013款 3.0T 自动 Vogue SC,landrovedaa","2013款 3.0T 自动 Vogue SE 柴油,landrovedab",
				"2013款 3.0T 自动 Vogue 柴油,landrovedac","2013款 5.0T 自动 AB尊崇创世版,landrovedad","2013款 5.0T 自动 Vogue SE SC,landrovedae",
				"2013款 5.0 自动 Vogue SE ,landrovedaf","2013款 5.0 自动 Vogue ,landrovedag","2014款 5.0T 自动 SC AB巅峰创世加长版,landrovedah",
				"2014款 5.0T 自动 SC AB尊崇创世加长版,landrovedai","2014款 3.0T 自动 Vogue SC SE创世加长版,landrovedaj","2014款 3.0 V6 SC HSE Dynamic,landroved14a",
				"2014款 3.0T 自动 Vogue SE创世加长版 柴油,landrovedak","2014款揽胜 3.0 V6 SC Vogue SE,landroved14a",
				"2015款 3.0T 自动 美规行政版,landrovedal","2015款 3.0T 自动 油电混合,landrovedam",
				"2015款 3.0 TDV6 Vogue",
				"2015款 3.0T 自动 黑曜限量版,landrovedan","2016款 5.0T 自动 巅峰创世加长版,landrovedao","2016款 5.0T 自动 尊崇创世加长版,landrovedap",
				"2016款 3.0T 自动 尊崇创世加长版,landrovedaq","2016款 3.0T 自动 创世加长版 混合动力,landrovedar","2016款 3.0T 自动 创世加长版,landrovedas",
				"2016款 3.0T 自动 Vogue加长版,landrovedat","2016款 3.0T 自动 Vogue 柴油,landrovedau"
				],
		
		
					
		"极光":["2012款 2.0T 自动 耀致版五门,landroveea","2012款 2.0T 自动 耀致版三门,landroveeb","2012款 2.0T 自动 耀动版五门,landroveec",
				"2012款 2.0T 自动 耀动版三门,landroveed","2013款 2.0T 自动 维多利亚限量版三门,landroveee","2013款 2.2T 自动 柴油版五门,landroveef",
				"2013款 2.0T 自动 熠动限量版五门,landroveeg","2013款 2.0T 自动 眩蓝限量版三门,landroveeh","2013款 2.0T 自动 耀真版五门,landroveei",
				"2013款 2.0T 自动 耀致版三门,landroveej","2013款 2.0T 自动 耀致版五门,landroveek","2013款 2.0T 自动 耀动版三门,landroveel",
				"2013款 2.0T 自动 耀动版五门,landroveem","2014款 2.0T 自动 限量版五门,landroveen","2014款 2.0T 自动 耀动版五门,landroveeo",
				"2014款 2.0T 自动 耀享版五门,landroveep","2014款 2.0T 自动 耀真版五门,landroveeq","2014款 2.0T 自动 圣诞限量版五门,landroveer", 
				"2015款 2.0T 自动 NW8 Brit限量版五门,landrovees","2015款 2.0T 自动 耀慕创世限量版,landroveet","2015款 2.0T 自动 倾橙限量版五门,landroveeu"
				],
		                  
		
		"神行者":["2006款 3.2 自动 SE,landrovefa","2006款 2.2T 手动 柴油,landrovefb","2006款 2.2T 自动 柴油,landrovefc","2007款 3.2 自动,landrovefd",
				"2009款 3.2 自动 HSE,landrovefe","2009款 3.2 自动 SE,landroveff","2010款 3.2 自动 SE,landrovefg","2010款 2.2T 自动 SD4 HSE 柴油,landrovefh",
				"2010款 3.2 自动 HSE,landrovefi","2010款 2.2T 自动 TD4 HSE 柴油,landrovefj","2010款 2.2T 手动 SE 柴油,landrovefk","2010款 2.2T 自动 TD4 SE 柴油,landrovefl",
				"2011款 2.2T 自动 TD4 HSE 柴油,landrovefm","2011款 2.2T 手动 TD4 SE 柴油,landrovefn","2011款 3.2 自动 HSE,landrovefo","2011款 3.2 自动 SE,landrovefp",
				"2011款 2.2T 自动 SD4 HSE 柴油,landrovefq","2011款 2.2T 自动 SE 柴油,landrovefr","2011款 3.2 自动 中国红限量版,landrovefs",
				"2012款 2.0T 自动 Si4 HSE,landroveft","2012款 2.0T 自动 Si4 SE,landrovefu","2012款 2.2T 自动 SD4 HSE 柴油,landrovefv",
				"2012款 2.2T 自动 SD4 SE 柴油,landrovefw","2012款 2.2T 自动 SD4 S 柴油,landrovefx","2012款 3.2 自动 SE圣诞新年特别版,landrovefz",
				"2012款 3.2 自动 HSE圣诞新年特别版,landrovefaa",
				"2013款 2.0T 自动 Si4 越动限量版,landrovefab","2013款 2.2T 自动 SD4 越动限量 柴油,landrovefac","2013款 2.0T 自动 Si4 HSE,landrovefad",
				"2013款 2.0T 自动 Si4 SE,landrovefae","2013款 2.2T 自动 SD4 S 柴油,landrovefaf","2013款 2.2T 自动 SD4 SE 柴油,landrovefag",
				"2014款 2.2T 自动 SD4 SE 柴油,landrovefah","2014款 2.2T 自动 SD4 S 柴油,landrovefai","2014款 2.0T 自动 Si4 HSE,landrovefaj",
				"2014款 2.0T 自动 Si4 SE,landrovefaj","2014款 2.0T 自动 Si4 S,landrovefak",
				"2015款 2.0T 自动 Si4 HSE LUXURY,landrovefal","2015款 2.2T 自动 SD4 XS 柴油,landrovefam","2015款 2.0T 自动 Si4 XS,landrovefan"
				],
		 
		 
		
		"揽胜极光":["2015款 2.0T 自动 五门致享版,landrovega","2015款 2.0T 自动 五门锐动版,landrovegb","2015款 2.0T 自动 五门智耀版,landrovegc",
				"2015款 2.0T 自动 五门风尚版,landrovegd"
				],
				     
		    
		
		"揽胜运动版":["2005款 2.7T 自动 柴油,landroveha","2005款 4.4 自动,landrovehb","2005款 4.2T 自动 SC,landrovehc","2005款 3.6T 自动 柴油,landrovehd",                
					"2009款 3.0T 自动 柴油,landrovehe","2009款 3.6T 自动 柴油,landrovehf","2009款 5.0T 自动 SC,landrovehg","2009款 5.0 自动,landrovehh",
					"2010款 5.0T 自动 SC HSE,landrovehi","2010款 5.0 自动 HSE ,landrovehj","2010款 3.6T 自动 HSE 柴油,landrovehk","2010款 3.0T 自动 HSE 柴油,landrovehl",
					"2011款 5.0T 自动 SC HSE,landrovehm","2011款 5.0T 自动 SC Autobiography,landrovehn","2011款 3.0T 自动 Sporty 柴油,landroveho",
					"2011款 3.0T 自动 柴油,landrovehp",
					"2011款 5.0 自动 HSE,landrovehq","2012款 5.0 自动 HSE,landrovehr","2012款 5.0T 自动 SC LR,landrovehs","2012款 3.0T 自动 LR HSE 柴油,landroveht",
					"2012款 5.0T 自动 SC锋尚创世版,landrovehu","2013款 5.0 自动 HSE黑标限量版,landrovehv","2013款 3.0T 自动 黑标限量版 柴油,landrovehw",
					"2013款 5.0T 自动 SC锋尚创世版,landrovehx","2013款 5.0T 自动 SC HSE,landrovehy","2013款 5.0 自动 HSE ,landrovehz","2013款 3.0T 自动 极致版 柴油,landrovehaa",
					"2013款 3.0T 自动 柴油,landrovehab","2013款 5.0 自动 HSE驭红限量版,landrovehac","2014款 3.0T 自动 SC HSE7座,landrovehad",
					"2014款 3.0T 自动 SC智利红限量版,landrovehae",
					"2014款 5.0T 自动 SC锋尚创世版Dynamic,landrovehaf","2014款 3.0T 自动 SC锋尚创世版,landrovehag","2014款 3.0T 自动 SC HSE Dynamic ,landrovehah",
					"2014款 3.0T 自动 SC HSE ,landrovehai","2015款 3.0T 自动 HSE 柴油,landrovehaj","2015款 3.0T 自动 SDV6 Hybrid HSE Dynamic 柴油混动,landrovehak",
					"2016款 5.0T 自动 SVR,landrovehal","2016款 3.0T 自动 SDV6 HSE Dynamic 柴油混动,landroveham","2016款 3.0T 自动 HST,landrovehan",
					"2016款 3.0T 自动 SC HSE Dynamic,landrovehao",
					"2016款 3.0T 自动 SC HSE ,landrovehap","2016款 3.0T 自动 SDV6 HSE Dynamic 柴油,landrovehaq","2016款 3.0T 自动 SDV6 HSE 柴油,landrovehar",
					"2016款 2.0T 自动 GTDi HSE,landrovehas",
					"2016款 3.0T 自动 SC SE,landrovehat","2015款 3.0T 自动 HSE Dynamic 柴油,landrovehau"
					],	
			
			
//  "兰博基尼"			
			
			
			"Aventador":["2011款 6.5 自动 LP700-4,lamborghiniaa","2012款 6.5 自动 LP700-4,lamborghiniab","2013款 6.5 自动 LP700-4,lamborghiniac",
						"2015款 6.5 自动 LP750-4 Superveloce,lamborghiniad"
						],
			
			
			"Huracan":["2014款 5.2 自动 LP610-4,lamborghiniba","2016款 5.2 自动 LP600-4限量版,lamborghinibb"
						],
			
			
			"Reventon":["2008款 6.5 手动,Reventona"
						],
			
			
			"盖拉多":["2005款 5.0 手动 SE,lamborghinida","2006款 5.0 手动,lamborghinidb","2006款 5.0 自动,lamborghinidc","2007款 5.0 自动,lamborghinidd",
					"2007款 5.0 自动 Superleggera,lamborghinide","2008款 5.2 手动,lamborghinidf","2008款 5.2 自动 LP560-4,lamborghinidg",
					"2009款 5.2 自动 LP560-4,lamborghinidh","2009款 5.2 自动 LP550-2 Valentino Balboni,lamborghinidi",
					"2010款 5.2 自动 LP570-4 Superleggera,lamborghinidj","2010款 5.2 自动 LP550-2 标准版,lamborghinidk",
					"2011款 5.2 自动 LP560-4 Bicolore,lamborghinidl","2011款 5.2 自动 LP550-4 Bicolore,lamborghinidm",
					"2011款 5.2 自动 LP550-2 Tricolore,lamborghinidn","2011款 5.2 自动 LP570-4 Performance,lamborghinido",
					"2012款 5.2 自动 LP560-4 Gold Edition金色限量版,lamborghinidp","2012款 5.2 自动 LP570-4 Super Trofeo Stradale特别版,lamborghinidq"
					],

			
			"蝙蝠":["2006款 6.5 自动 LP640,lamborghiniea","2006款 6.5 手动 LP640,lamborghinieb","2007款 6.5 手动 LP640双座,lamborghiniec",
					"2009款 6.5 自动 LP670-4 SuperVeloce,lamborghinied","2010款 6.5 自动 LP670 VS顶级限量版,lamborghiniee",
					"2010款 6.5 自动 LP650-4限量,lamborghinief"
					],
			
			
//"劳斯莱斯"	
			
			"古思特":["2010款 6.6T 自动 标准版,rollsroyceaa","2011款 6.6T 自动 加长版,rollsroyceab","2013款 6.6T 自动 装饰派艺术典藏版,rollsroyceac",
					"2014款 6.6T 自动 加长特别版,rollsroycead","2014款 6.6T 自动 标准版,rollsroyceae",
					"2014款 6.6T 自动 加长版,rollsroyceaf","2014款 6.6T 自动 阿尔卑斯挑战之旅世纪典藏版,rollsroyceag"
					],
			
			"幻影":["2005款 6.7 自动 加长版,rollsroyceba","2006款 6.7 自动 软顶敞篷版,rollsroycebb","2007款 6.7 自动 双门轿跑,rollsroycebc",
					"2007款 6.7 自动 元首级,rollsroycebd","2011款 6.7 自动 标准版,rollsroycebe",
					"2013款 6.7 自动 加长版(EWB),rollsroycebf","2013款 6.7 自动 软顶敞篷版,rollsroycebg",
					"2013款 6.7 自动 双门轿跑,rollsroycebh","2013款 6.7 自动 标准版,rollsroycebi",
					"2014款 6.7 自动 古德伍德十周年纪念典藏版,rollsroycebj"
					],
			
			"银刺":["1996款 6.2 自动,rollsroyceca"],
			
			"银影":["1965款 6.7 自动 四门轿跑,rollsroyceda"],
			
			"银灵":["1980款 6.7 自动,rollsroyceea"],
			
			"魅影":["2013款 6.6T 自动,rollsroycefa"],
			
// "陆风"	

			"X6":["2005款 2.4 手动 旗舰版后驱,lufengaa","2005款 2.0 手动 舒适版后驱,lufengab","2005款 2.4 手动 旗舰版四驱,lufengac","2005款 2.0 手动 舒适版四驱,lufengad",
				"2005款 2.8T 手动 后驱 柴油,lufengae","2005款 2.4 手动 舒适版后驱,lufengaf","2005款 2.8T 手动 四驱 柴油,lufengag","2005款 2.4 手动 舒适版四驱,lufengah",
				"2007款 2.0 手动 舒适版后驱,lufengai","2007款 2.0 手动 超值版后驱,lufengaj","2007款 2.4 手动 舒适版后驱,lufengak","2007款 2.0 手动 舒适版四驱,lufengal",
				"2007款 2.8T 手动 新饰力豪华版后驱 柴油,lufengam","2007款 2.8T 手动 新饰力豪华版四驱 柴油,lufengan","2007款 2.8T 手动 新饰力标准版四驱 柴油,lufengao",
				"2007款 2.8T 手动 新饰力标准版后驱 柴油,lufengap","2007款 2.8T 手动 四驱 柴油,lufeng","2007款 2.8T 手动 后驱 柴油,lufengaq",
				"2008款 2.5T 手动 龙腾版舒适型后驱 柴油,lufengar","2008款 2.5T 手动 龙腾版豪华型后驱 柴油,lufengas","2008款 2.5T 手动 龙腾版豪华型四驱 柴油,lufengat",
				"2008款 2.5T 手动 龙腾版舒适型四驱 柴油,lufengau","2008款 2.5T 手动 龙腾版标准型四驱 柴油,lufengav","2008款 2.5T 手动 龙腾版标准型后驱 柴油,lufengaw",
				"2009款 2.0 手动 舒适版四驱,lufengax","2009款 2.5T 手动 龙腾版舒适型四驱 柴油,lufengay","2010款 2.8T 手动 超值版后驱 柴油,lufengaz",
				"2010款 2.8T 手动 超值版四驱 柴油,lufengaaa","2011款 2.0 手动 超值版后驱,lufengaab","2011款 2.0 手动 豪华版后驱,lufengaac",
				"2011款 2.8T 手动 豪华后驱 柴油,lufengaad","2011款 2.8T 手动 豪华四驱 柴油,lufengaae","2011款 2.8T 手动 超值四驱 柴油,lufengaaf",
				"2011款 2.8T 手动 超值后驱 柴油,lufengaag"
				],
			
			  
			  
			  
			"X8":["2009款 2.5T 手动 豪华型四驱 柴油,lufengba","2009款 2.5T 手动 豪华型后驱 柴油,lufengbb","2009款 2.5T 手动 超豪华型后驱 柴油,lufengbc",
				"2009款 2.4 手动 超豪华型四驱,lufengbd","2009款 2.4 手动 豪华型四驱,lufengbe","2009款 2.4 手动 豪华型后驱,lufengbf",
				"2009款 2.4 手动 超豪华型后驱,lufengbg","2009款 2.5T 手动 超豪华型四驱 柴油,lufengbh",
				"2009款 2.5T 手动 超豪华型后驱 柴油,lufengbi","2009款 2.5T 手动 豪华型四驱 柴油,lufengbj","2009款 2.5T 手动 豪华型后驱 柴油,lufengbk",
				"2011款 2.5T 手动 豪华型四驱 柴油,lufengbl","2011款 2.5T 手动 豪华型后驱 柴油,lufengbm","2011款 2.5T 手动 导航型四驱 柴油,lufengbn",
				"2011款 2.5T 手动 导航型后驱 柴油,lufengbobr","2011款 2.5T 手动 超豪华型四驱 柴油,lufengbpbs","2011款 2.5T 手动 超豪华型后驱 柴油,lufengbqbt",
				"2011款 2.4 手动 导航型后驱,lufengbu","2011款 2.4 手动 超豪华型后驱,lufengbv","2011款 2.4 手动 豪华型四驱,lufengbw","2011款 2.4 手动 导航型四驱,lufengbx",
				"2011款 2.4 手动 超豪华型四驱,lufengby","2011款 2.4 手动 豪华型后驱,lufengbz",
				"2012款 2.0T 手动 超豪华型后驱 柴油,lufengbaa","2012款 2.0T 手动 超豪华型四驱 柴油,lufengbab","2012款 2.0T 手动 豪华型四驱 柴油,lufengbac",
				"2012款 2.0T 手动 豪华型后驱 柴油,lufengbad","2012款 2.5T 手动 豪华探索版四驱 柴油,lufengbae","2012款 2.5T 手动 豪华探索版后驱 柴油,lufengbaf",
				"2012款 2.5T 手动 导航探索版四驱 柴油,lufengbag","2012款 2.5T 手动 导航探索版后驱 柴油,lufengbah","2012款 2.5T 手动 超豪华探索版四驱 柴油,lufengbai",
				"2012款 2.5T 手动 超豪华探索版后驱 柴油,lufengbaj","2012款 2.4 手动 豪华探索版四驱,lufengbak","2012款 2.4 手动 豪华探索版后驱,lufengbal",
				"2012款 2.4 手动 导航探索版四驱,lufengbam","2012款 2.4 手动 导航探索版后驱,lufengban","2012款 2.4 手动 超豪华探索版四驱,lufengbao",
				"2012款 2.4 手动 超豪华探索版后驱,lufengbap","2012款 2.0 手动 都市先锋探索版后驱,lufengbaq","2012款 2.0 手动 都市先锋版后驱,lufengbar",
				"2014款 2.0T 手动 豪华探索版后驱,lufengbas","2014款 2.0T 手动 超豪华探索版四驱,lufengbat","2014款 2.0T 手动 豪华探索版四驱 柴油,lufengbau",
				"2014款 2.0T 手动 豪华探索版后驱 柴油,lufengbav","2014款 2.0T 手动 超豪华探索版四驱 柴油,lufengbaw","2014款 2.0T 手动 超豪华探索版后驱 柴油,lufengbax",
				"2014款 2.0T 手动 超豪华探索版后驱,lufengbay","2014款 2.0T 手动 豪华探索版四驱,lufengbaz","2014款 2.5T 手动 豪华探索版四驱 柴油,lufengbba",
				"2014款 2.5T 手动 豪华探索版后驱 柴油,lufengbbb","2014款 2.5T 手动 超豪华探索版四驱 柴油,lufengbbc","2014款 2.5T 手动 超豪华探索版后驱 柴油,lufengbbd",
				"2014款 2.4 手动 豪华探索版四驱,lufengbbe","2014款 2.4 手动 豪华探索版后驱,lufengbbf","2014款 2.4 手动 超豪华探索版四驱,lufengbbg",
				"2014款 2.4 手动 超豪华探索版后驱,lufengbbh","2014款 2.0 手动 都市先锋版后驱,lufengbbi","2014款 2.5T 手动 导航探索版四驱 柴油,lufengbbj",
				"2015款 2.0T 手动 超豪华探索版后驱,lufengbbk","2015款 2.0T 手动 超豪华探索版四驱,lufengbbl","2015款 2.0T 手动 豪华探索版后驱,lufengbbm",
				"2015款 2.0T 手动 豪华探索版四驱,lufengbbn","2015款 2.0T 手动 超豪华探索版四驱 柴油,lufengbbo","2015款 2.0T 手动 超豪华探索版后驱 柴油,lufengbbp",
				"2015款 2.0T 手动 豪华探索版四驱 柴油,lufengbbq","2015款 2.0T 手动 豪华探索版后驱 柴油,lufengbbr"
				],
			
			                
			
			
			"X9":["2006款 2.8T 手动 领先版四驱 柴油,lufengca","2006款 2.8T 手动 精悍版四驱 柴油,lufengcb","2006款 2.8T 手动 精悍版后驱 柴油,lufengcc",
				"2006款 2.4 手动 领先版四驱,lufengcd","2006款 2.4 手动 领先版后驱,lufengce","2006款 2.4 手动 精悍版四驱,lufengcf",
				"2008款 2.4 手动 领先版后驱,lufengcg","2008款 2.0 手动 领先版后驱,lufengch","2008款 2.0 手动 精悍版后驱,lufengci","2008款 2.4 手动 精悍版后驱,lufengcj",
				"2008款 2.8T 手动 精悍版后驱 柴油,lufengck","2008款 2.8T 手动 精悍版四驱 柴油,lufengcl","2008款 2.4 手动 精悍版四驱,lufengcm",
				"2008款 2.8T 手动 领先版四驱 柴油,lufengcn","2008款 2.8T 手动 领先版后驱 柴油,lufengco","2008款 2.4 手动 领先版四驱,lufengcp",
				"2009款 2.5T 手动 VM冠军限量版四驱 柴油,lufengcq","2009款 2.5T 手动 VM冠军限量版后驱 柴油,lufengcr","2009款 2.4 手动 冠军限量版四驱,lufengcs",
				"2009款 2.0 手动 冠军限量版后驱,lufengct","2013款 2.0T 手动 前驱 柴油,lufengcu","2013款 2.0T 手动 四驱 柴油,lufengcv"
				],
			
			       
			
			"新饰界":["2005款 2.8T 手动 后驱 柴油,lufengda","2005款 2.4 手动 后驱,lufengdb","2005款 2.0 手动 后驱,lufengdc","2005款 2.8T 手动 四驱 柴油,lufengdd",
				"2005款 2.4 手动 四驱,lufengde"
				],                  
			
			"X7":["2015款 2.0T 自动 全景旗舰版,lufengea","2015款 2.0T 自动 全景尊贵版,lufengeb","2015款 2.0T 自动 全景尊享版,lufengec"
				],
			     
			
			"风华":["2008款 1.5 手动 舒适型,lufengfa","2008款 1.5 手动 豪华型,lufengfb","2008款 1.5 手动 运动型,lufengfc"
				],
            
			                  
			                  
			"风尚":["2006款 1.8 手动 5座,lufengga","2006款 1.6 手动 7座,lufenggb","2006款 2.0 手动 7座,lufenggc","2006款 1.6 手动 5座,lufenggd","2006款 2.0 手动 5座,lufengge",
				"2008款 2.0 手动 7座,lufenggf","2008款 2.0 手动 5座,lufenggg","2008款 2.0 自动 7座,lufenggh","2008款 2.0 自动 5座,lufenggi","2008款 1.8 手动 7座,lufenggj",
				"2008款 1.8 手动 5座,lufenggk","2008款 1.6 手动 7座,lufenggl","2008款 1.6 手动 5座,lufenggm","2009款 1.5 手动 吉星5座,lufenggn","2009款 1.6 手动 5座,lufenggo",                 
				"2011款 1.6 手动 精质版7座,lufenggp","2011款 1.6 手动 精质版5座,lufenggq"
				],
				
			
			"陆风X5":["2013款 2.0T 自动 创享版前驱,lufengha","2013款 2.0T 手动 创领版前驱,lufenghb","2013款 2.0T 手动 创享版前驱,lufenghc",
				"2013款 2.0T 手动 创行版前驱,lufenghd","2014款 2.0T 自动 创享版前驱,lufenghe","2014款 2.0T 自动 创领版前驱,lufenghf",
				"2015款 1.5T 手动 创领版前驱,lufenghg","2015款 1.5T 自动 创领版前驱,lufenghh","2015款 1.5T 自动 创享版前驱,lufenghi",
				"2015款 1.5T 手动 创享版前驱,lufenghj"
				],
			   
			
//"陆地方舟"
			
			"陆地方舟V5":["2013款 经济型 纯电动,ludishipaa","2013款 舒适型 纯电动,ludishipab","2013款 豪华型 纯电动,ludishipac","2013款 旗舰型 纯电动,ludishipad"
				],
			
			"艾威":["2012款 标准型 纯电动,ludishipba","2013款 标准型 纯电动,ludishipbb"
				],
			
			"陆地方舟陆风":["2013款 标准型 纯电动,ludishipca"
				],
			
			
//"罗孚"	
			
			"罗孚75":["2004款 2.5 自动 标准型,luofuaa","2004款 2.5 自动 豪华型,luofuab"
				],
			
//"摩根"
			
			"4/4":["2008款 1.6 手动 双座,mogenaa"
				],
			
			
			"Aero Coupe":["2012款 4.8 手动,mogenba","2012款 4.8 自动,mogenbb"
				],
			
			"Aero SuperSports":["2011款 4.8 手动,mogenca","2011款 4.8 自动,mogencb"
				],
			
			"Plus 4":["2006款 2.0 手动 四座,mogenda"
				],
			
			"Plus 8":["2013款 4.8 手动,mogenea","2013款 4.8 自动,mogeneb"
				],
			
			"摩根Roadster":["2006款 3.7 手动 四座,mogenfa"
				],
			
			"三轮车":["2011款 1.9 手动 V2,mogenha","2014款 2.0 手动 标准版,mogenhb",
					"2014款 2.0 手动 超级干燥版,mogenhc","2014款 2.0 手动 布鲁克兰限量版,mogenhd"
				],
			
			
//   "迷你"
			
			"Clubman":["2007款 1.6 自动,miniaa","2007款 1.6 自动 Excitement,miniab","2007款 1.6 自动 Fun,miniac","2007款 1.6 自动 Cheer,miniad",
						"2008款 1.6T 自动 S,miniae","2011款 1.6T 自动 Fun S,miniaf","2011款 1.6 自动 Fun,miniag","2011款 1.6T 自动 S Hampton,miniah",
						"2011款 1.6T 自动 S,miniai","2011款 1.6 自动 One,miniaj","2011款 1.6 自动 Hampton,miniak","2011款 1.6 自动 Excitement,minial",
						"2012款 1.6T 自动 TCJEDITION S,miniam","2012款 1.6T 自动 中国任务特制版S,minian",
						"2013款 1.6T 自动 JCW,miniao","2013款 1.6T 自动 Cooper Bond Street特别版S,miniap",
						"2013款 1.6 自动 Cooper Bond Street特别版,miniaq",
						"2016款 2.0T 自动 JCW套件版,miniar","2016款 2.0T 自动 S极客版,minias","2016款 2.0T 自动 S,miniat",
						"2016款 1.5T 自动 极客版,miniau","2016款 1.5T 自动 鉴赏家版,miniav","2016款 1.5T 自动 ,miniaw"
						],
			
			"Roadster":["2012款 1.6 自动,miniba","2012款 1.6T 自动 S,minibb"
						],
			
			
			"Countryman":["2011款 1.6T 自动 S All 4,minica","2011款 1.6 自动 ONE,minicb","2011款 1.6 自动 Fun,minicc","2011款 1.6 自动 Excitemen,minicd",
							"2012款 1.6T 自动 Cooper S,minice","2013款 1.6T 自动 Cooper S限量滑雪板,minicf",                  
							"2013款 1.6T 自动 Excitement四驱,minicg","2013款 1.6T 自动 Fun四驱,minich","2013款 1.6T 自动 JCW,minici",
							"2014款 1.6T 自动 JCW版四驱,minicj","2014款 1.6T 自动 Cooper Fun四驱,minick","2014款 1.6 自动 Cooper Fun,minicl",
							"2014款 1.6T 自动 Cooper Excitement四驱,minicm","2014款 1.6 自动 Cooper Excitement,minicn","2014款 1.6T 自动 Cooper S四驱,minico",
							"2014款 1.6 自动 one,minicp","2015款 1.6T 自动 Cooper S进藏限量版,minicq","2015款 1.6T 自动 Cooper S,minicr",
							"2015款 1.6T 自动 Cooper S极致暗夜限量版四驱,minics"
							],
										
			"Paceman":["2013款 1.6T 自动 JCW版四驱,minida","2013款 1.6T 自动 S Cooper四驱,minidb","2013款 1.6 自动 Cooper前驱,minidc",
						"2014款 1.6T 自动 JCW版四驱,minidd","2014款 1.6T 自动 Cooper S四驱,minide","2014款 1.6T 自动 Cooper四驱,minidf","2014款 1.6 自动 Cooper前驱,minidg"
						],
			
			"Cooper":["2006款 1.6 自动 个性版 Checkmate,miniea","2006款 1.6 自动 个性版 Park Lane,minieb","2006款 1.6 自动 个性版 Seven,miniec",
					"2007款 1.6 自动 Fun,minied","2007款 1.6 自动 Excitement,miniee","2007款 1.6 自动,minief","2007款 1.6T 自动 S,minieg",                  
					"2010款 1.6 自动 Cheer,minieh","2010款 1.6T 自动 S 50 Camden,miniei","2010款 1.6 自动 50 Mayfair,miniej",
					"2011款 1.6 自动 Excitement,miniek","2011款 1.6 自动 Fun,miniel","2011款 1.6T 自动 S,miniem",
					"2012款 1.6T 自动 inspircd by goodwood,minien","2012款 1.6 自动 中国奥林皮克纪念版,minieo","2012款 1.6T 自动 bayswater S,miniep",
					"2012款 1.6 自动 bayswater,minieq","2012款 1.6 自动 Baker Street,minier","2012款 1.6T 自动 S中国任务特制版,minies",
					"2012款 1.6T 自动 Fun S,miniet","2013款 1.6T 自动 JCW,minieu","2014款 2.0T 自动 S Excitement,miniev",
					"2014款 2.0T 自动 S Fun,miniew","2014款 1.5T 自动 Excitement,miniex","2014款 1.5T 自动 Fun,miniey",
					"2015款 2.0T 自动 JCW,miniez","2015款 2.0T 自动 S五门版,minieaa","2015款 1.5T 自动 Excitement五门版,minieab",
					"2015款 1.5T 自动 Fun五门版,minieav"
					],
			
			"Couple":["2012款 1.6 自动,minifa","2012款 1.6T 自动 S,minifb","2013款 1.6T 自动 JCW,minifc"
						],
			
			
			"One":["2006款 1.6T 自动 个性版 Park Lane,miniga","2007款 1.6T 自动,minigb","2008款 1.6T 自动,minigc","2011款 1.6 自动,minigd",
					"2012款 1.6 自动 Baker Street,minigf","2013款 1.6 自动 限量版第三款,minigg","2013款 1.6 自动 限量版第二款,minigh",
					"2013款 1.6 自动 限量版第一款,minigi","2014款 1.2T 自动,minigj","2014款 1.2T 手动,minigk","2014款 1.2T 自动 ONE+,minigl",
					"2015款 1.2T 自动 五门版,minigm","2015款 1.2T 手动 五门版,minign"
					],



			
			"敞篷版":["2005款 1.6T 自动 Cooper S,miniha","2005款 1.6 自动 Cooper,minihb","2007款 1.6T 自动 Cooper S Sidewalk,minihc",
					"2008款 1.6T 自动 Cooper S Sidewalk,minihd","2008款 1.6 自动 Cooper Sidewalk,minihe",
					"2009款 1.6T 自动 John Cooper Works,minihf","2009款 1.6T 自动 Cooper S,minihg","2009款 1.6 自动 Cooper,minihh",
					"2011款 1.6T 自动 Cooper S,minihi","2011款 1.6 自动 Cooper,minihj",
					"2012款 1.6T 自动 highgate S Cooper,minihk","2012款 1.6 自动 highgate Cooper,minihl"
					],
			
//"迈巴赫"	
			
			"57":["2005款 6.0T 自动 Spezial,maybachaa","2009款 6.0T 自动 Zeppelin Biturbo,maybachab"
				],
			
			"62":["2007款 6.0T 自动 Spezial,maybachba","2008款 6.0T 自动 S Landaulet Biturbo,maybachbb",
				  "2009款 6.0T 自动 Zeppelin Biturbo,maybachbc","2011款 6.0T 自动 Spezial,maybachbd"
				],
			
// "迈凯伦"
			
			"540C":["2015款 3.8T 自动 Coupe,maikailunaa"
				],
			
			"650S":["2014款 3.8T 自动 Coupe,maikailunba","2014款 3.8T 自动 Spider,maikailunbb"
				],
			
			"MP4-12C":["2010款 3.8T 自动,maikailunca","2013款 3.8T 自动 COUPE,maikailuncb",
						"2013款 3.8T 自动 SPIDER硬顶敞篷,maikailuncc","2013款 3.8T 自动 COUPE 50周年纪念版,maikailuncd",
						"2013款 3.8T 自动 SPIDER硬顶敞篷50周年纪念版,maikailunce"
						],
			
			"P1":["2013款 3.8T 自动 全球限量版,maikailunda","2014款 3.8T 自动 基本型,maikailundb"
				],
			
//"名爵"
		
			"MG 3SW":["2007款 1.4 手动 舒适型,mingjueaa","2008款 1.8 自动 豪华型,mingjueab","2008款 1.4 手动 豪华型,mingjueac","2008款 1.4 手动 舒适型,mingjuead",
					"2008款 1.8 自动 舒适型,mingjueae","2009款 1.4 手动 舒适型野酷版,mingjueaf","2009款 1.4 手动 豪华型野酷版,mingjueag",
					"2010款 1.4 自动 舒适型,mingjueah","2010款 1.4 自动 豪华型,mingjueai"
					],
			
			 
			 
			
			"MG 7":["2006款 1.8T 手动,mingjueba","2007款 2.5 手动 加长版,mingjuebb","2007款 2.5 自动 加长版,mingjuebc","2007款 1.8T 手动 标准版,mingjuebd","2007款 1.8T 自动 标准版,mingjuebe",
					"2008款 2.5 手动 加长版,mingjuebf","2008款 2.5 自动 加长版,mingjuebg","2008款 1.8T 自动 标准版,mingjuebh","2008款 1.8T 自动 加长版,mingjuebi",
					"2008款 1.8T 手动 舒适版,mingjuebj","2008款 1.8T 手动 豪华运动版,mingjuebk","2008款 1.8T 手动 豪华版,mingjuebl","2008款 1.8T 手动 精英版,mingjuebm",
					"2009款 2.5 手动 加长版,mingjuebn","2009款 1.8T 手动 标准版,mingjuebo","2009款 2.5 自动 旗舰加长版,mingjuebp","2009款 2.5 自动 豪华加长版,mingjuebq",
					"2009款 1.8T 自动 豪华加长版,mingjuebr","2009款 1.8T 自动 舒适加长版,mingjuebs","2009款 1.8T 自动 超豪华版,mingjuebt","2009款 1.8T 自动 豪华版,mingjuebu",
					"2009款 1.8T 自动 舒适版,mingjuebv","2010款 1.8T 手动 舒适版,mingjuebw","2010款 1.8T 自动 舒适版,mingjuebx","2010款 1.8T 自动 精英版,mingjueby",
					"2010款 1.8T 自动 豪华版,mingjuebz","2010款 2.5 自动 豪华加长版,mingjuebaa","2010款 2.5 自动 旗舰加长版,mingjuebab","2010款 1.8T 自动 舒适加长版,mingjuebac",
					"2010款 1.8T 自动 豪华加长版,mingjuebad"
					],
			
			
			  
			"MG TF":["2006款 1.8 手动 标准版,mingjueca","2006款 1.8 自动 标准版,mingjuecb",  
					"2007款 1.8 自动 标准版,mingjuecc","2007款 1.8 手动 标准版,mingjuecd", 
					"2008款 1.8 手动 标准版,mingjuece","2008款 1.8 自动 标准版,mingjuecf"
					],
			
			
			                  
			
			"TF":["2002款 1.8 手动 160HP,mingjueda","2002款 1.8 手动 135HP,mingjuedb"
			    ],
			                  
			
			
			"MG 3":["2011款 1.5 手动 精英版,mingjueea","2011款 1.5 手动 XROSS精英版,mingjueeb","2011款 1.5 自动 精英版,mingjueec",
					"2011款 1.5 自动 XROSS豪华版,mingjueed","2011款 1.3 手动 舒适版,mingjueee","2011款 1.3 自动 舒适版,mingjueef",
					"2012款 1.3 自动 舒适版,mingjueeg","2012款 1.3 手动 舒适版,mingjueeh","2013款 1.5 手动 精英版,mingjueei",
					"2013款 1.5 手动 超值版,mingjueej","2013款 1.5 自动 精英版,mingjueek","2013款 1.5 自动 豪华版,mingjueel",
					"2013款 1.3 手动 舒适版,mingjueem","2013款 1.3 自动 舒适版,mingjueen","2014款 1.5 手动 精英版,mingjueeo",
					"2014款 1.5 手动 超值版,mingjueep","2014款 1.5 自动 精英版,mingjueeq","2014款 1.5 自动 豪华版,mingjueer",
					"2014款 1.3 手动 舒适版,mingjuees","2014款 1.3 自动 舒适版,mingjueet"
					],
			                  
			
					   		
						
			
			"MG 5":["2012款 1.5 自动 领航版,mingjuefa","2012款 1.5 自动 豪华版,mingjuefb","2012款 1.5 手动 豪华版,mingjuefc","2012款 1.5 自动 精英版,mingjuefd",
					"2012款 1.5 手动 精英版,mingjuefe","2012款 1.5 手动 风尚版,mingjueff","2013款 1.5T 自动 旗舰型,mingjuefg","2013款 1.5T 自动 豪华型,mingjuefh",
					"2014款 1.5T 手动 豪华型,mingjuefi","2014款 1.5T 自动 旗舰型,mingjuefj","2014款 1.5T 自动 豪华型,mingjuefk",
					"2015款 1.5T 自动 inkaNet旗舰版,mingjuefl","2015款 1.5T 自动 inkaNet豪华版,mingjuefm","2015款 1.5T 自动 豪华型,mingjuefn",
					"2015款 1.5 自动 inkaNet豪华版,mingjuefo","2015款 1.5 自动 inkaNet精英版,mingjuefp","2015款 1.5 自动 精英版,mingjuefq",
					"2015款 1.5 手动 精英版,mingjuefr"
					],
			  
			  
			  
			
			"MG 6 两厢":["2010款 1.8 自动 世博型贺岁版,mingjuega","2010款 1.8 手动 世博型贺岁版,mingjuegb","2010款 1.8T 自动 豪华型,mingjuegc","2010款 1.8T 自动 精英型,mingjuegd",
					"2010款 1.8T 手动 舒适型,mingjuege","2010款 1.8 自动 精英型,mingjuegf","2010款 1.8 手动 舒适型,mingjuegg",
					"2012款 1.8T 手动 GT超值版,mingjuegh","2012款 1.8T 自动 GT超值版,mingjuegi","2012款 1.8T 自动 性能版,mingjuegj","2012款 1.8T 手动 性能型,mingjuegk",
					"2012款 1.8T 手动 舒适型,mingjuegl","2012款 1.8T 自动 精英型,mingjuegm","2012款 1.8T 自动 豪华型,mingjuegn","2012款 1.8 手动 舒适型,mingjuego",
					"2012款 1.8 手动 驾值型,mingjuegp","2012款 1.8 自动 舒适型,mingjuegq","2012款 1.8 自动 精英型,mingjuegr","2012款 1.8 自动 驾值型,mingjuegs",                  
					"2013款 1.8T 手动 性能型,mingjuegt","2013款 1.8T 自动 性能型,mingjuegu","2013款 1.8T 自动 精英型,mingjuegv","2013款 1.8T 自动 豪华型,mingjuegw",
					"2013款 1.8 手动 舒适型,mingjuegx","2013款 1.8 手动 驾值型,mingjuegy","2013款 1.8 自动 驾值型,mingjuegz",
					"2014款 1.8T 手动 性能型,mingjuegaa","2014款 1.8T 自动 性能型,mingjuegab","2014款 1.8T 自动 精英型,mingjuegac","2014款 1.8T 自动 豪华型,mingjuegad",
					"2014款 1.8 手动 舒适型,mingjuegae","2014款 1.8 手动 驾值型,mingjuegaf","2014款 1.8 自动 驾值型,mingjuegag",
					"2015款 1.8T 自动 inkaNet旗舰型,mingjuegah","2015款 1.8T 自动 inkaNet精英型,mingjuegai"
					],
			
			
			
			
			
			"MG 6 三厢":["2011款 1.8T 手动 精英型,mingjueha","2011款 1.8T 自动 精英型,mingjuehb","2011款 1.8T 自动 豪华型,mingjuehc","2011款 1.8T 手动 舒适型,mingjuehd",
					"2011款 1.8 自动 舒适型,mingjuehe","2011款 1.8 自动 精英型,mingjuehf","2012款 1.8T 手动 精英型限量版,mingjuehg",
					"2012款 1.8T 自动 精英型限量版,mingjuehh","2012款 1.8T 自动 豪华型限量版,mingjuehi",
					"2012款 1.8T 手动 舒适型限量版,mingjuehj","2012款 1.8 自动 舒适型限量版,mingjuehk","2012款 1.8 自动 精英型限量版,mingjuehl",
					"2013款 1.8 手动 驾值型,mingjuehm","2013款 1.8 手动 舒适型,mingjuehn","2013款 1.8 自动 驾值型,mingjueho","2013款 1.8 自动 精英型,mingjuehp",
					"2013款 1.8T 自动 豪华型,mingjuehq","2013款 1.8T 自动 精英型,mingjuehr"
					],
			                  
			
			
			"锐行GT":["2015款 1.4T 自动 旗舰版,mingjueia","2015款 1.4T 自动 豪华版,mingjueib","2015款 1.4T 自动 精英版,mingjueic","2015款 1.4T 手动 精英版,mingjueid",
					"2015款 1.5T 自动 豪华版,mingjueie","2015款 1.5T 自动 精英版,mingjueif","2015款 1.5T 手动 精英版,mingjueig","2015款 1.5T 自动 风尚版,mingjueih",
					"2015款 1.5T 手动 风尚版,mingjueii"
					],
			                  
			
			"锐腾":["2015款 2.0T 自动 旗舰版四驱,mingjueja","2015款 2.0T 自动 豪华版,mingjuejb","2015款 1.5T 自动 豪华版四驱,mingjuejc","2015款 1.5T 自动 豪华版,mingjuejd",
					"2015款 1.5T 自动 精英版,mingjueje","2015款 1.5T 手动 精英版,mingjuejf","2015款 1.5T 手动 都市版,mingjuejg"
					],
			                  
			
			"名爵3系SW":["2015款 1.5 自动 精英型,mingjueka"
					],
	
			
// "马自达"	
			
			"马自达2":["2007款 1.3L 手动标准型,mazdaaa","2007款 1.3L 自动时尚型,mazdaab","2007款 1.3L 自动标准型,mazdaac",
					"2008款 1.5L 自动豪华型,mazdaad","2008款 1.5L 自动尊贵型,mazdaae","2009款 1.5L 手动超值版,mazdaaf",
					"2009款 1.5L 自动超值版,mazdaag","2011款 炫动 1.3L 自动标准版,mazdaah","2011款 炫动 1.5L 自动超值版,mazdaai",
					"2011款 炫动 1.5L 自动豪华版,mazdaaj","2012款 两厢炫动 1.5L 自动豪华版,mazdaak"
					],
			
			"马自达3 Axela昂克赛拉":["2014款 三厢 2.0L 自动旗舰型,mazdaba","2014款 三厢 1.5L 自动豪华型,mazdabb",
					"2014款 三厢 1.5L 自动尊贵型,mazdabc","2014款 三厢 2.0L 自动运动型,mazdabd"
					],
			
			"马自达3星骋":["2011款 三厢 2.0L 自动豪华型,mazdaca","2011款 三厢 1.6L 自动舒适型,mazdacb",
					"2011款 三厢 1.6L 自动精英型,mazdacc","2011款 三厢 2.0L 手动运动型,mazdacd",
					"2012款 两厢 2.0L 自动豪华型,mazdace","2012款 两厢 2.0L 手动运动型,mazdacf",
					"2012款 两厢 1.6L 自动精英型,mazdacg","2013款 两厢 1.6L 自动精英型,mazdach",
					"2013款 三厢 1.6L 自动精英型,mazdaci"
					],
			
			"马自达CX-5":["2013款 2.5L 自动四驱旗舰型,mazdada","2013款 2.0L 自动四驱尊贵型,mazdadb","2013款 2.5L 自动四驱豪华型,mazdadc"
					],
			
			"马自达2劲翔":["2008款 1.3L 自动标准型,mazdaea","2008款 1.5L 手动时尚型,mazdaeb","2008款 1.5L 自动时尚型,mazdaec","2011款 炫动 1.5L 手动时尚版,mazdaed"
					],
			
			"马自达3":["2006款 2.0L 自动豪华型,mazdafa","2006款 2.0L 手动豪华型,mazdafb","2006款 2.0L 手动标准型,mazdafc","2006款 2.0L 自动标准型,mazdafd",
					"2007款 1.6L 手动标准型,mazdafe","2007款 1.6L 自动标准型,mazdaff","2007款 1.6L 自动豪华型,mazdafg","2009款 1.6L 自动天窗版,mazdafh",
					"2010款 2.0L 手动经典运动型,mazdafi","2010款 1.6L 自动经典精英型,mazdafj","2010款 1.6L 手动经典时尚型,mazdafk","2010款 1.6L 自动经典时尚型,mazdafl",
					"2010款 2.0L 自动经典豪华型,mazdafm","2012款 1.6L 手动经典特惠型,mazdafn","2012款 1.6L 自动经典特惠型,mazdafo","2012款 1.6L 手动经典标准型,mazdafp",
					"2012款 1.6L 自动经典标准型,mazdafq"
					],
			
			"阿特兹":["2014款 2.5L 蓝天至尊版,mazdaga","2014款 2.0L 蓝天豪华版,mazdagb"
					],
				
				
			"马自达6":["2006款 Wagon 2.3L 自动型,mazdaha","2006款 2.0L 自动超豪华型,mazdahb","2006款 2.3L 自动豪华型,mazdahc","2006款 2.3L 自动旗舰型,mazdahd",
					"2006款 2.0L 手动型,mazdahe","2006款 轿跑 2.3L 自动型,mazdahf","2007款 2.0L 手动型,mazdahg","2007款 2.0L 自动豪华型,mazdahh","2007款 2.0L 自动超豪华型,mazdahi",
					"2007款 2.3L 自动旗舰型,mazdahj","2007款 轿跑 2.3L 手动型,mazdahk","2007款 轿跑 2.3L 自动型,mazdahl","2007款 轿跑 2.0L 自动型,mazdahm",
					"2008款 2.0L 自动时尚型,mazdahn","2008款 轿跑 2.0L 自动运动型,mazdaho","2011款 2.0L 自动时尚型,mazdahp","2011款 2.0L 手动型,mazdahq",
					"2011款 2.0L 自动超豪华型,mazdahr","2011款 2.0L 自动豪华型,mazdahs","2012款 2.0L 自动时尚型,mazdaht","2012款 2.0L 手动型,mazdahu",
					"2012款 2.0L 自动超豪华型,mazdahv","2013款 2.0L 手动型,mazdahw","2013款 2.0L 自动时尚型,mazdahx","2013款 2.0L 自动超豪华型,mazdahy",
					"2014款 2.0L 自动经典型,mazdahz","2015款 2.0L 自动时尚型,mazdahaa","2015款 2.0L 自动经典型,mazdahab","03款 2.3L 自动,mazdahac"
					],
			
			"睿翼":["2009款 2.5L 自动尊贵版,mazdaia","2009款 2.5L 自动至尊版,mazdaib","2009款 2.5L 自动导航版,mazdaic","2010款 轿跑 2.5L 自动至尊版,mazdaid",
					"2010款 2.0L 手动版,mazdaie","2010款 2.0L 自动豪华版,mazdaif","2010款 轿跑 2.0L 自动豪华版,mazdaig","2011款 2.0L 自动精英版,mazdaih",
					"2011款 轿跑 2.0L 自动精英版,mazdaii",
					"2012款 2.0L 自动精英版,mazdaij","2012款 轿跑 2.0L 自动豪华版,mazdaik","2012款 2.0L 自动豪华版,mazdail","2012款 2.5L 自动尊贵版,mazdaim",
					"2012款 2.5L 自动至尊版,mazdain","2012款 轿跑 2.0L 自动精英版,mazdaio"
					],
			
			"马自达CX-7":["2014款 2.5L 2WD 时尚版,mazdaja"
					],
			
			"马自达8":["2011款 2.3L 至尊版,mazdaka","2011款 2.3L 尊贵版,mazdakb","2013款 2.5L 至尊版,mazdakc","2013款 2.5L 精英版,mazdakd",
					"2013款 2.5L 尊贵版,mazdake","2015款 2.5L 领航版,mazdakf"
					],
					
			"马自达CX-7(进口)":["2010款 2.5L 豪华型,mazdala"
					],
			
			"马自达CX-9":["2013款 3.7L 自动标准型,mazdama"
					],
			
			"马自达5":["2008款 2.0 手动舒适型,mazdana","2008款 2.0 自动舒适型,mazdanb","2008款 2.0 自动豪华型,mazdanc","2011款 2.0L 手动舒适型,mazdand",
					"2011款 2.0L 自动舒适型,mazdane","2011款 2.0L 自动豪华型,mazdanf","2013款 2.0L 自动舒适型,mazdang"
					],
			
			"马自达MX-5":["2009款 2.0,mazdaoa"
					],
			
				
			
			"马自达3(进口)":["2010款 两厢 1.6自动豪华型,mazdapa","2010款 两厢 2.0自动豪华型,mazdapb","2008款 两厢 1.6豪华型,mazdapc","2008款 两厢 2.0豪华型,mazdapd",
					"2008款 两厢 1.6标准型,mazdape"
					],
			
			"ATENZA":["2013款 2.0L 标准型,mazdaqa"
					],
			
			"马自达RX-8":["2004款 1.3 MT,mazdara"
					],
			
			
//   "玛莎拉蒂"		
			
			"3200GT":["1997款 3.2 手动,maseratiaa","1998款 3.2 手动,maseratiab"
					],
			
			
			"Coupe":["2002款 4.2 手动,maseratiba","2002款 4.2 自动 Cambiocorsa,maseratibb"
					],
			
			"Ghibli":["2014款 3.0T 自动,maseratica","2014款 3.0T 自动 S,maseraticb","2014款 3.0T 自动 S Q4,maseraticc"
					],
			
			
			"GranCabrio":["2010款 4.7 自动,maseratida","2012款 4.7 自动 运动版,maseratidb","2015款 4.7 自动 百年纪念版,maseratidc"
					],
			
			
			"GranSport":["2004款 4.2 自动,maseratiea"
					],
			
			
			"GranTurismo":["2007款 4.2 自动,maseratifa","2008款 4.7 自动 S,maseratifb","2009款 4.7 自动 S,maseratifc","2010款 4.7 自动 MC Stradale,maseratifd",
					"2013款 4.7 自动 S,maseratife","2013款 4.7 自动 S F1,maseratiff","2015款 4.7 自动 MC百年纪念版,maseratifg"
					],
			
			
			"总裁":["2006款 4.2 自动 行政版GT,maseratiga","2006款 4.2 自动 运动版GT,maseratigb","2006款 4.2 自动 标准版,maseratigc",
					"2008款 4.7 自动 行政版,maseratigd","2008款 4.2 自动 精英版,maseratige","2009款 4.7 自动 行政版,maseratigf",
					"2009款 4.2 自动 精英版,maseratigg","2009款 4.7 自动 运动版,maseratigh","2011款 4.7 自动 全球荣誉版,maseratigi",
					"2013款 3.0T 自动 标准型,maseratigj","2013款 3.8T 自动 标准型,maseratigk","2015款 3.0T 自动 四驱,maseratigl"
					],
			
//"美亚"
			
			
			"奇兵":["2006款 2.8T 手动 柴油,meiyaaa","2007款 2.0 手动 ,meiyaab","2007款 2.8T 手动 柴油,meiyaac"
					],
			
			"美亚奇骏":["2007款 2.0 手动 经济型,meiyaba","2007款 2.0 手动 标准型,meiyabb","2007款 2.0 手动 豪华型,meiyabc"
					],
			
			"海狮":["2005款 2.2 手动 标准型中顶9座,meiyaca","2005款 2.2 手动 豪华型中顶9座,meiyacb","2005款 2.2 手动 经济型中顶9座,meiyacc",
					"2005款 2.2 手动 普通型中顶9座,meiyacd","2005款 2.2 手动 标准型平顶9座,meiyace","2005款 2.2 手动 豪华型平顶9座,meiyacf",
					"2005款 2.2 手动 经济型平顶9座,meiyacg","2005款 2.2 手动 普通型平顶9座,meiyach","2005款 2.2 手动 实用型平顶9座,meiyaci"
					],
			
			"陆程":["2010款 2.2T 手动 TM1020A4 柴油,meiyada","2010款 2.2T 手动 TM1020AL2 柴油,meiyadb","2010款 2.2T 手动 柴油,meiyadc"
					],
			
//"纳智捷"
			
			
			"5 Sedan":["2013款 2.0T 自动 旗舰型,nazhijieaa","2013款 1.8T 自动 旗舰型,nazhijieab","2013款 1.8T 自动 尊贵型,nazhijieac","2013款 1.8T 自动 豪华型,nazhijiead",
					"2013款 1.8T 手动 经典型,nazhijieae","2013款 1.8T 手动 精致型,nazhijieaf","2014款 1.8T 手动 经典型舞动限量版,nazhijieag",
					"2014款 1.8T 自动 尊贵型舞动限量版,nazhijieah","2014款 1.8T 自动 豪华型舞动限量版,nazhijieai","2014款 2.0T 自动 旗舰型荣耀导航版,nazhijieaj",
					"2014款 1.8T 自动 旗舰型荣耀导航版,nazhijieak","2014款 1.8T 自动 尊贵型荣耀导航版,nazhijieal","2014款 1.8T 自动 豪华型荣耀导航版,nazhijieam",
					"2014款 1.8T 手动 经典型荣耀导航版,nazhijiean","2014款 1.8T 手动 精致型荣耀导航版,nazhijieao","2014款 1.8T 自动 精致型,nazhijieap",
					"2015款 1.8T 自动 旗舰型,nazhijieaq","2015款 1.8T 自动 智尊型,nazhijiear","2015款 1.8T 自动 智慧型,nazhijieas","2015款 1.8T 手动 智慧型,nazhijieat"
					],
			                  
			
			"CEO":["2013款 2.2T 自动 专属旗舰版,nazhijieba","2013款 2.2T 自动 行政版,nazhijiebb"
					],
			                  
			
			
			"优6":["2014款 2.0T 自动 旗舰型,nazhijieca","2014款 2.0T 自动 智尊型,nazhijiecb","2014款 2.0T 自动 时尚型,nazhijiecc","2014款 1.8T 自动 智尊型,nazhijiecd",
					"2014款 1.8T 自动 时尚型,nazhijiece","2014款 1.8T 自动 魅力型,nazhijiecf","2014款 1.8T 自动 新创型,nazhijiecg","2015款 1.8T 自动 范特西限量版,nazhijiech",
					"2015款 2.0T 自动 旗舰型,nazhijieci","2015款 2.0T 自动 智尊型,nazhijiecj","2015款 1.8T 自动 智尊型,nazhijieck",
					"2015款 1.8T 自动 新创型,nazhijiecl","2015款 1.8T 自动 时尚型,nazhijiecm","2015款 1.8T 自动 魅力型,nazhijiecn"
					],                  
			
			
			
			"大7 MPV":["2013款 2.0T 自动 旗舰型,nazhijieda","2013款 2.0T 自动 行政型,nazhijiedb","2013款 2.0T 自动 豪华型,nazhijiedc","2013款 2.0T 自动 商务型,nazhijiedd",
					"2013款 2.0T 自动 精英型,nazhijiede","2015款 2.0T 自动 商务型,nazhijiedf","2015款 2.0T 自动 旗舰型,nazhijiedg","2015款 2.0T 自动 精英型,nazhijiedh",
					"2015款 2.0T 自动 豪华型,nazhijiedi"
					],
			                  
			  
			
			"大7 SUV":["2011款 2.2T 自动 智尊型四驱,nazhijieea","2011款 2.2T 自动 智慧型前驱,nazhijieeb","2011款 2.2T 自动 新创型前驱,nazhijieec",
					"2011款 2.2T 自动 旗舰型四驱,nazhijieed",
					"2011款 2.0T 自动 新创型前驱,nazhijieee","2012款 2.2T 自动 锋芒限量智慧型前驱,nazhijieef","2012款 2.2T 自动 锋芒限量旗舰型四驱,nazhijieeg",
					"2012款 2.2T 自动 锋芒限量智尊型四驱,nazhijieeh","2012款 2.2T 自动 旗舰型前驱,nazhijieei","2013款 2.2T 自动 锋芒限量旗舰型四驱,nazhijieej",
					"2013款 2.2T 自动 锋芒限量智尊型四驱,nazhijieek","2013款 2.2T 自动 锋芒限量智慧型前驱,nazhijieel","2014款 2.2T 自动 荣耀典藏智慧型前驱,nazhijieem",
					"2014款 2.2T 自动 旗舰型四驱,nazhijieen","2014款 2.2T 自动 智尊型四驱,nazhijieeo","2014款 2.2T 自动 智慧型前驱,nazhijieep",
					"2014款 2.2T 自动 超级锋芒版四驱,nazhijieeq",
					"2015款 2.2T 自动 旗舰型四驱,nazhijieer","2015款 2.2T 自动 智尊型四驱,nazhijiees","2015款 2.2T 自动 智尊型前驱,nazhijieet",
					"2015款 2.2T 自动 智慧型前驱,nazhijieeu"
					],
			
//"南汽"		
			
			"优尼柯":["2005款 1.5 手动 标准型侧开门 LPG油气混合,nanqiaa","2005款 1.5 手动 增强型,nanqiab",
					"2005款 1.5 手动 增强型侧开门 LPG油气混合,nanqiac","2005款 1.5 手动 基本型,nanqiad",
					"2007款 1.3 手动 ,nanqiae","2007款 1.3 手动 F1,nanqiaf","2007款 1.5 手动 ,nanqiag","2007款 1.5 手动 F1,nanqiah"
					],
			
			"君达":["2004款 2.2 手动 标准型,nanqiba"
					],
			
			
			"新雅途":["2005款 1.5 手动 增强型 LPG油气混合,nanqica","2005款 1.5 手动 优越型 LPG油气混合,nanqicb",
					"2005款 1.5 手动 舒适型 LPG油气混合,nanqicc","2005款 1.5 手动 基本型 LPG油气混合,nanqicd","2005款 1.5 手动 豪华型 LPG油气混合,nanqice",
					"2005款 1.5 手动 豪华天窗版 LPG油气混合,nanqicf","2005款 1.3 手动 心动版,nanqicg","2005款 1.5 手动 标准型 LPG油气混合,nanqich",
					"2005款 1.5 手动 优越性,nanqici","2005款 1.5 手动 舒适型,nanqicj","2005款 1.5 手动 豪华天窗版,nanqick","2005款 1.5 手动 增强型,nanqicl",
					"2005款 1.5 手动 豪华型,nanqicm","2005款 1.5 手动 标准型,nanqicn","2005款 1.5 手动 基本型,nanqico","2005款 1.3 手动 超越版,nanqicp",
					"2005款 1.3 手动 精装版,nanqicq","2005款 1.3 手动 经典版,nanqicr","2005款 1.3 手动 超值版,nanqics","2007款 1.5 手动 T2,nanqict","2007款 1.5 手动 T1,nanqicu"
					],
			
//"讴歌"
			
			"ILX":["2013款 1.5 自动 油电混合,ougeaa","2013款 2.0 自动 精锐版,ougeab"
					],
			
			
			"MDX":["2006款 3.7 自动,ougeba","2007款 3.7 自动,ougebb","2009款 3.7 自动 豪华运动版,ougebc",
					"2009款 3.7 自动,ougebd","2010款 3.7 自动 舒适豪华运动版,ougebe","2010款 3.7 自动 标准豪华运动版,ougebf",
					"2010款 3.7 自动 舒适型,ougebg","2010款 3.7 自动 标准型,ougebh","2010款 3.7 自动 豪华娱乐型,ougebi",
					"2010款 3.7 自动 豪华型,ougebj","2010款 3.7 自动 技术娱乐型,ougebk",
					"2010款 3.7 自动 技术型,ougebl","2010款 3.7 自动 基本型,ougebm",
					"2011款 3.7 自动 舒适尊享运动版,ougebn","2011款 3.7 自动 标准尊享运动版,ougebo",
					"2014款 3.5 自动 豪华版,ougebp","2014款 3.5 自动 精英版,ougebq"
					],

			
			
			"RDX":["2007款 2.3T 自动 四驱,ougeca","2012款 3.5 自动 四驱,ougecb","2013款 3.0 自动 精英版前驱,ougecc","2013款 3.5 自动 豪华版四驱,ougecd"
					],
			
			
			"RL":["2005款 3.5 自动,ougeda","2006款 3.5 自动,ougedb","2008款 3.7 自动,ougedc","2010款 3.7 自动,ougedd"
					],
			
			
			"RLX":["2013款 3.5 自动 前驱,ougeea","2015款 3.5 自动 四驱油电混合 ,ougeeb"
					],
			
			
			"TL":["2006款 3.2 自动 炫速版,ougefa","2006款 3.2 自动 炫航版,ougefb",
					"2008款 3.5 自动,ougefc","2009款 3.5 自动 标准版,ougefd",
					"2009款 3.5 自动 豪华运动版,ougefe","2010款 3.5 自动 炫速版,ougeff","2010款 3.5 自动 尊贵版,ougefg",
					"2012款 3.5 自动 标准版,ougefh"
					],
			
			"TLX":["2015款 2.4 自动 精英版,ougega","2015款 2.4 自动 豪华版,ougegb"
					],
			
			
			"ZDX":["2010款 3.7 自动,ougeha","2012款 3.7 自动 标准版,ougehb"
					],
		
//"欧宝"
	
			"威达两厢":["2002款 3.2T 自动 GTS,opelaa"
					],
			
			"威达三厢":["2005款 2.2T 自动 ,opelba","2006款 2.2 自动 舒适型,opelbb","2006款 2.2 自动 豪华型,opelbc","2006款 2.2 自动 豪华导航型,opelbd"
					],
			
			"安德拉":["2008款 3.2 自动 四驱,opelca","2008款 2.4 自动 四驱,opelcb","2010款 3.2 自动 四驱,opelcc","2010款 2.4 自动 四驱,opelcd",
					"2011款 2.4 自动 舒适型四驱,opelce","2011款 2.4 自动 舒适型前驱,opelcf","2011款 2.4 自动 豪华型四驱,opelcg",
					"2011款 2.4 自动 豪华型前驱,opelch","2012款 2.4 自动 豪华型四驱,opelci","2012款 2.4 自动 豪华型前驱,opelcj",
					"2012款 2.4 自动 舒适型四驱,opelck","2012款 2.4 自动 舒适型前驱,opelcl","2013款 2.4 自动 舒适型四驱,opelcm",
					"2013款 2.4 自动 舒适型前驱,opelcn","2013款 2.4 自动 豪华型四驱,opelco","2013款 2.4 自动 豪华型前驱,opelcp"
					],  
			
			"欧捷利":["2008款 1.0 手动 前驱,opelda","2008款 1.2 手动 前驱,opeldb"
					],
			
			"欧美佳三厢":["2003款 2.2 自动 后驱,opelfa"
					],
			
			"英速亚两厢":["2008款 1.6 自动 前驱,opelga","2008款 1.6T 自动 前驱,opelgb","2008款 1.8 自动 前驱,opelgc","2008款 2.0T 自动 前驱,opelgd",
						"2008款 2.8T 自动 四驱,opelge","2009款 2.8T 自动 OPC四驱,opelgf"
					],
			
			"英速亚三厢":["2008款 1.6 自动 前驱,opelha","2008款 1.6T 自动 前驱,opelhb","2008款 1.8 自动 前驱,opelhc","2008款 2.0T 自动 前驱,opelhd",
						"2008款 2.8T 自动 四驱,opelhe","2008款 2.8T 自动 前驱,opelhf"
					],
			
			"英速亚旅行版":["2009款 2.8T 自动 四驱,opelia","2009款 1.6 自动 前驱,opelib","2009款 1.6T 自动 前驱,opelic","2009款 1.8 自动 前驱,opelid",
						   "2009款 2.0T 自动 前驱,opelie","2009款 2.8T 自动 OPC四驱,opelif","2013款 2.0T 自动 豪华型前驱,opelig",
						   "2013款 2.0T 自动 运动型四驱,opelih"
					],
			
			"赛飞利":["2006款 1.8 自动 前驱,opelja","2006款 1.8 手动 前驱,opeljb","2006款 1.9T 自动 柴油,opeljc","2010款 1.8 自动,opeljd",
					 "2013款 1.4T 自动 舒适型5座,opelje","2013款 1.4T 自动 豪华型7座,opeljf"
					],
			
			"雅特两厢":["2005款 1.8 自动 豪华型五门,opelma","2005款 1.8 自动 GTC五门,opelmb","2005款 2.0T 自动 (200HP)GTC三门,opelmc",
					"2005款 1.8 自动 GTC三门,opelmd","2005款 1.8 自动 舒适型五门,opelme","2006款 1.8 自动 GTC三门,opelmf",
					"2007款 1.8 自动 五门,opelmg","2007款 1.8 自动 全景风挡版GTC三门,opelmh","2009款 1.6T 自动 五门,opelmi",
					"2009款 1.6 手动 五门,opelmj","2009款 1.4T 自动 五门,opelmk","2009款 1.4T 手动 (87HP)五门,opelml",
					"2009款 1.4T 手动 (100HP)五门,opelmn","2010款 1.8 自动 GTC三门,opelmo","2010款 1.8 自动 五门,opelmp",
					"2010款 1.8 自动 全景风挡版GTC三门,opelmq","2014款 1.4T 自动 运动型GTC三门,opelmr","2014款 1.4T 自动 舒适型GTC三门,opelms"
					],
			
			"雅特三厢":["2009款 1.8 自动 A+标准版,opelna","2009款 1.8 自动 A+豪华版,opelnb"
					],
			
			"雅特敞篷车":["2007款 1.8 自动 ,opeloa","2007款 1.6 自动 ,opelob","2007款 1.6T 自动 ,opeloc","2007款 2.0T 自动 (200HP),opelod",
					 "2007款 2.0T 自动 (170HP),opeloe","2013款 1.4T 自动 豪华型7座,opelof"
					],
			
			"麦瑞纳":["2013款 1.4T 自动 舒适型前驱,opelpa","2013款 1.4T 自动 豪华型前驱,opelpb"
					],
			
// "帕加尼"		
			"Zonda":["2005款 7.3 手动 F,pajianiaa","2008款 7.3 自动 Cinque,pajianiab","2009款 7.3 自动 Cinque Roadster,pajianiac"
					],
				
//	"起亚"
			
			
			"起亚K2":["2011款 三厢 1.6L AT Premium,kiaaa","2011款 三厢 1.4L MT GLS,kiaab","2011款 三厢 1.4L AT GLS,kiaac",
					"2011款 三厢 1.4L MT GL,kiaad","2011款 三厢 1.4L MT TOP,kiaae","2012款 两厢 1.6L AT Premium,kiaaf",
					"2012款 两厢 1.4L MT GLS,kiaag","2012款 两厢 1.4L AT GLS,kiaah","2012款 两厢 1.4L MT GLS纪念版,kiaai",
					"2012款 两厢 1.6L AT Premium纪念版,kiaaj","2012款 三厢 1.4L MT GL纪念版,kiaak","2012款 三厢 1.4L MT GLS纪念版,kiaal",
					"2012款 三厢 1.4L AT GLS纪念版,kiaam","2012款 三厢 1.4L MT TOP纪念版,kiaan","2012款 三厢 1.4L AT TOP纪念版,kiaao",
					"2012款 三厢 1.6L AT Premium纪念版,kiaap","2012款 三厢 1.6L AT Premium ECO纪念版,kiaaq"
					],
			
			
			"秀尔":["2010款 1.6L MT GL,kiaba","2010款 1.6L AT GL,kiabb","2010款 1.6L MT GLS,kiabc","2010款 1.6L AT GLS,kiabd",
					"2010款 1.6L AT Premium,kiabe","2012款 1.6L MT GL,kiabf","2012款 1.6L AT GLS,kiabg","2012款 1.6L AT Premium,kiabh",
					"2013款 1.6L AT Premium,kiabi"
					],
			
			
			"赛拉图":["2005款 1.6L AT GL,kiaca","2005款 1.6L MT GL,kiacb","2005款 1.8L MT GLS,kiacc","2005款 1.6L MT GLS,kiacd",
					"2005款 1.6L AT GLS,kiace","2005款 1.8L MT SPORTS,kiacf","2006款 1.6L MT GL,kiacg","2006款 1.6L AT GL,kiach",
					"2006款 1.6L MT GLS,kiaci","2006款 1.6L AT GLS,kiacj","2006款 1.8L MT GLS,kiack","2007款 1.6L MT GLS,kiacl",
					"2007款 1.6L MT GL,kiacm","2007款 1.6L AT GL,kiacn","2007款 1.6L AT GLS,kiaco","2008款 欧风 1.6 MT GLS,kiacp",
					"2008款 欧风 1.6 AT GLS,kiacq","2008款 欧风 1.6 MT GL,kiacr","2008款 欧风 1.6 AT GL,kiacs","2010款 1.6L MT GL,kiact",
					"2010款 1.6 AT GL,kiacu","2010款 1.6L MT GLS,kiacv","2010款 1.6L AT GLS,kiacw","2012款 1.6L MT GL,kiacx"
					],
			
			
			"福瑞迪":["2009款 1.6L MT GLS,kiada","2009款 1.6L AT GLS,kiadb","2009款 1.6L MT GL,kiadc","2009款 1.6L AT GL,kiadd","2009款 1.6L AT Premium,kiade",
					"2011款 1.6L MT GL,kiadf","2011款 1.6L AT GL,kiadg","2011款 1.6L MT GLS,kiadh","2011款 1.6L AT GLS,kiadi","2011款 1.6L AT Premium,kiadj",
					"2012款 1.6L MT GL 纪念版,kiadk","2012款 1.6L MT GLS 纪念版,kiadl","2012款 1.6L AT Premium 纪念版,kiadm","2014款 1.6L MT GL,kiadn",
					"2014款 1.6L AT Premium Special,kiado"
					],
			
			
			"起亚K3":["2013款 1.6L 自动GL,kiaea","2013款 1.6L 手动GL,kiaeb","2013款 1.6L 手动GLS,kiaec","2013款 1.6L 自动GLS,kiaed","2013款 1.6L 自动DLX,kiaee",
					"2013款 1.6L 自动Premium,kiaef","2015款 1.6L 手动GLS,kiaeg","2015款 1.6L 自动GLS,kiaeh","2015款 1.6L 手动GL,kiaei"
					],
					
			
			"起亚K4":["2014款 1.8L 自动GLS,kiafa","2014款 1.6T 自动Turbo,kiafb"
				],
			
					
			"起亚K5":["2011款 2.4L 自动Premium,kiaga","2011款 2.0L 自动Premium,kiagb","2011款 2.0L 自动GL,kiagc","2011款 2.0L 自动GLS,kiagd",
					"2011款 2.0L 自动TOP,kiage","2011款 2.4L 自动TOP,kiagf","2011款 2.0L 自动DLX,kiagg","2012款 2.0L 自动GL,kiagh",
					"2012款 2.0L 自动GLS,kiagi","2012款 2.0L 自动DLX,kiagj","2012款 2.0L 自动DLX-2,kiagk","2012款 2.0L 自动Premium,kiagl",
					"2012款 2.0L 自动DLX特别版,kiagm","2014款 2.0T 自动T-Special,kiagn","2014款 2.0L 自动GL,kiago","2014款 2.0L 自动GLS,kiagp",
					"2014款 2.0L 自动LUXURY,kiagq","2014款 2.0L 自动PREMIUM 2,kiagr","2014款 2.0L 自动PREMIUM,kiags","2014款 2.0T 自动T-PREMIUM,kiagt",
					"2015款 2.0L 自动LUX,kiagu"
					],
			
			
			"狮跑":["2007款 2.7L 自动四驱版GLS,kiaha","2007款 2.0L 手动两驱版GL,kiahb","2007款 2.0L 自动两驱版GL,kiahc","2007款 2.0L 手动两驱版GLS,kiahd",
					"2007款 2.0L 自动两驱版GLS,kiahe","2007款 2.0L 手动四驱版GLS,kiahf","2009款 2.0L 手动两驱版GL,kiahg","2009款 2.0L 自动两驱版GL,kiahh",
					"2009款 2.0L 手动两驱版GLS,kiahi","2009款 2.0L 自动两驱版GLS,kiahj","2009款 2.0L 手动四驱版GLS,kiahk","2011款 2.0L 手动两驱版GL,kiahl",
					"2011款 2.0L 自动两驱版GL,kiahm","2011款 2.0L 手动两驱版GLS,kiahn","2011款 2.0L 自动两驱版GLS,kiaho","2011款 2.0L 手动四驱版GLS,kiahp",
					"2012款 2.0L 手动两驱版GL,kiahq","2012款 2.0L 自动两驱版GL,kiahr","2012款 2.0L 手动两驱版GLS,kiahs","2012款 2.0L 自动两驱版GLS,kiaht",
					"2012款 2.0L 手动四驱版GLS,kiahu","2013款 2.0L 手动两驱版GL,kiahv","2013款 2.0L 自动两驱版GL,kiahw","2013款 2.0L 手动两驱版GLS,kiahx",
					"2013款 2.0L 自动两驱版GLS,kiahy","2013款 2.0L 自动两驱版DLX,kiahz"
					],
			
			
			"智跑":["2011款 2.0L 手动两驱版GL,kiaia","2011款 2.0L 自动两驱版GL,kiaib","2011款 2.0L 手动两驱版GLS,kiaic","2011款 2.0L 自动两驱版GLS,kiaid",
					"2011款 2.0L 自动两驱版Premium,kiaie","2011款 2.4L 自动四驱版GLS,kiaif","2011款 2.4L 自动四驱版Premium,kiaig","2012款 2.4L 自动四驱版Premium,kiaih",
					"2012款 2.0L 手动两驱版GL,kiaii","2012款 2.0L 自动两驱版GL,kiaij","2012款 2.0L 自动两驱版GLS,kiaik","2012款 2.0L 自动四驱版GLS,kiail",
					"2012款 2.0L 自动两驱版NAVI,kiaim","2012款 2.4L 自动四驱版GLS,kiain","2014款 2.0L 自动两驱版GL,kiaio","2014款 2.0L 自动两驱版GLS,kiaip",
					"2014款 2.0L 自动两驱版DLX,kiaiq","2014款 2.0L 自动两驱版Premium Special,kiair","2015款 2.0L 自动两驱版DLX,kiais","2015款 2.0L 自动两驱版GLS,kiait"
					],
			
			
			"千里马":["2005款 1.6L MT GLS,kiaja","2005款 1.6L AT天窗版GLS,kiajb","2005款 1.3L AT GL,kiajc","2005款 1.3L MT GL,kiajd",
					"2005款 1.3L MT DLX,kiaje","2005款 1.6L MT GL,kiajf","2006款 1.3L MT纪念版,kiajg","2006款 1.3L AT纪念版,kiajh"
					],
							
				
			"锐欧":["2007款 1.4L MT GLS,kiaka","2007款 1.4L MT GL,kiakb","2007款 1.4L AT GLS,kiakc","2007款 1.6L MT GLS,kiakd",
					"2007款 1.4L MT,kiake"
					],
			
			
			"远舰":["2005款 2.0L MT GL,kiala","2005款 2.0L AT GL,kialb","2006款 2.0L 手动尊贵版GLS-2,kialc"
				],
			
			
			"嘉华":["2006款 2.7L GL,kiama","2006款 2.7L TOP,kiamb","2006款 3.5L 尊贵版,kiamc","2006款 3.5L 顶级版TOP,kiamd"
				],
			
			
			"凯尊":["2011款 2.4L 尊贵型,kiana","2011款 2.4L 商务型,kianb","2011款 2.4L 精英型,kianc","2012款 2.4L 商务型,kiand","2013款 2.4L 商务天窗型 国V,kiane"
				],
				
			
			"索兰托":["2005款 2.4L AWD,kiaoa","2006款 3.8L 汽油豪华版,kiaob","2006款 3.8L 汽油舒适版,kiaoc","2009款 2.4L 至尊版,kiaod","2009款 2.4L 豪华版,kiaoe",
					"2010款 2.2T 柴油豪华版,kiaof","2012款 2.4L 汽油至尊版,kiaog","2012款 2.4L 汽油豪华版,kiaoh","2012款 2.4L 汽油舒适版,kiaoi",
					"2013款 2.4L 5座汽油豪华版,kiaoj","2013款 2.4L 7座汽油豪华版,kiaok","2013款 2.4L 5座汽油舒适版,kiaol","2013款 2.4L 5座汽油豪华版 国IV,kiaom"
					],
			
			
			"霸锐":["2008款 3.8L 豪华版 国IV,kiapa","2013款 3.8L 豪华版 国V,kiapb"
				],
			
			
			"佳乐":["2007款 2.0L 7座豪华版,kiaqa","2007款 2.0L 7座舒适版,kiaqb","2007款 2.0L 5座舒适版,kiaqc","2007款 2.0L 5座豪华版,kiaqd","2007款 2.0L 5座标准版,kiaqe",
					"2007款 2.0L 7座标准版,kiaqf","2011款 2.0L 7座自动舒适版,kiaqg","2011款 2.0L 5座自动标准版,kiaqh","2011款 2.0L 7座自动标准版,kiaqi",
					"2011款 1.6L 5座自动豪华版,kiaqj","2013款 2.0L 7座自动舒适版 国V,kiaqk"
					],
			
			
			"起亚VQ":["2011款 2.7 舒适版,kiara","2011款 2.7 豪华版,kiarb"
				],
			
			
			"速迈":["2011款 1.6AT 运动版,kiasa"
				],
			
			
			"欧菲莱斯":["2008款 2.7 尊贵版,kiata"
				],
		
//"奇瑞"	
			
			
			"奇瑞A1":["2007款 1.3 手动 舒适型,qiruiaa","2007款 1.3 手动 豪华型,qiruiab","2009款 1.3 手动 舒适型,qiruiac","2009款 1.3 手动 实力型,qiruiad",
				"2009款 1.3 手动 豪华型,qiruiae","2009款 1.3 自动 舒适型,qiruiaf","2011款 1.3 自动 征途版,qiruiag","2011款 1.3 自动 逍遥版,qiruiah",
				"2011款 1.0 手动 征途版,qiruiai","2011款 1.0 手动 逍遥版,qiruiaj","2011款 1.0 手动 奇幻版,qiruiak","2011款 1.0 手动 传奇版,qiruial"
				],
			
			
			"奇瑞A3":["2008款 三厢  1.6 手动 精英型,qiruiba","2008款 三厢  1.6 手动 舒适型,qiruibb","2008款 三厢  1.6 手动 标准型,qiruibc","2008款 三厢  1.8 手动 华贵型,qiruibd","2008款 三厢  1.8 手动 驾驭型,qiruibe",
				"2009款 三厢  2.0 自动 精英型,qiruibf","2009款 三厢  2.0 自动 标准型,qiruibg","2009款 两厢  2.0 自动 精英型,qiruibobh","2009款 两厢  2.0 自动 标准型,qiruibpbi",
				"2009款 两厢  1.8 手动 华贵型,qiruibj","2009款 两厢  1.8 手动 驾驭型,qiruibk","2009款 两厢  1.6 手动 精英型,qiruibl","2009款 两厢  1.6 手动 标准型,qiruibm",
				"2010款 三厢  1.6 手动 尊贵型,qiruibn","2010款 三厢  1.6 手动 进取型,qiruibo","2010款 三厢  1.8 自动 尊贵型改款 三厢 ,qiruibp","2010款 三厢  1.8 自动 进取型改款 三厢 ,qiruibq",
				"2010款 三厢  1.8 自动 尊贵型,qiruibr","2010款 三厢  1.8 自动 进取型,qiruibs","2010款 三厢  1.6 手动 尊贵型改款 三厢 ,qiruibt","2010款 三厢  1.6 手动 进取型改款 三厢 ,qiruibu",
				"2010款 三厢  2.0 自动 尊贵型,qiruibv","2010款 三厢  1.8 手动 尊贵型,qiruibw","2010款 三厢  1.6 手动 豪华型,qiruibx","2010款 三厢  1.6 手动 进取型,qiruiby",
				"2010款 两厢  1.6 手动 尊贵型,qiruibz","2010款 两厢  1.6 手动 进取型,qiruibaa","2010款 两厢  1.8 自动 尊贵型改款 两厢 ,qiruibab","2010款 两厢  1.8 自动 进取型改款 两厢 ,qiruibfbac",
				"2010款 两厢  1.8 自动 尊贵型,qiruibad","2010款 两厢  1.8 自动 进取型,qiruibae","2010款 两厢  1.6 手动 尊贵型改款 两厢 ,qiruibaf","2010款 两厢  1.6 手动 进取型改款 两厢 ,qiruibag",
				"2010款 两厢  2.0 自动 尊贵型,qiruibah","2010款 两厢  1.8 手动 尊贵型,qiruibai","2010款 两厢  1.6 手动 豪华型,qiruibaj",
				"2010款 两厢  1.6 手动 进取型,qiruibak","2012款 两厢  1.6 自动 尊贵型,qiruibal","2012款 两厢  1.6 自动 进取型,qiruibam",
				"2012款 三厢  1.6 自动 尊贵型,qiruiban","2012款 三厢  1.6 自动 进取型,qiruibao"
				],
 
			
			
			"奇瑞A5":["2006款 1.6 手动 豪华型,qiruica","2006款 2.0 手动 华贵型,qiruicb","2006款 2.0 手动 精英型,qiruicc","2006款 2.0 手动 标准型,qiruicd",
				"2006款 1.6 手动 舒适型,qiruice","2006款 1.6 手动 实力型,qiruicf","2007款 1.6 手动 LPG油气混合,qiruicg","2007款 1.6 手动 CNG油气混合,qiruich",
				"2008款 1.5 手动 华贵型,qiruici","2008款 1.5 手动 豪华型,qiruicj","2008款 1.5 手动 标准型,qiruick","2008款 2.0 手动,qiruicl",
				"2008款 1.6 手动 豪华型,qiruicm",
				"2008款 1.6 手动 舒适型,qiruicn","2008款 1.6 手动 实力型,qiruico","2008款 1.6 手动 CNG油气混合,qiruicp","2008款 2.0 自动 豪华型,qiruicq",
				"2008款 1.8 手动 舒适型,qiruicr","2008款 1.8 手动 豪华型,qiruics","2008款 1.5 手动 精英型,qiruict","2009款 1.5 手动 精英型,qiruicu",
				"2009款 1.5 手动 标准型,qiruicv","2009款 1.6 手动 BSG豪华型 油电混合,qiruicw","2009款 1.6 手动 BSG舒适型 油电混合,qiruicx",
				"2009款 1.6 手动 BSG实力型 油电混合,qiruicy","2010款 1.6 手动,qiruivz","2009款 1.6 手动 ,qiruicaa","2009款 1.6 手动 CNG油气混合,qiruicab",
				"2009款 1.5 手动 豪华型,qiruicac","2009款 1.5 手动 华贵型,qiruicad"
				],
			
			"奇瑞E3":["2013款 1.5 手动 尊尚型,qiruida","2013款 1.5 手动 智尚型,qiruidb","2013款 1.5 手动 风尚型,qiruidc","2013款 1.5 手动 趣尚型,qiruidd",
				"2015款 1.5 手动 实尚版,qiruide","2015款 1.5 手动 智尚版,qiruidf","2015款 1.5 手动 风尚版,qiruidg","2015款 1.5 手动 趣尚版,qiruidh"
				],
			
			
			"奇瑞E5":["2011款 1.8 自动 卓悦型,qiruiea","2011款 1.8 自动 优悦型,qiruieb","2011款 1.5 手动 卓悦型,qiruiec","2011款 1.5 手动 智悦型,qiruied",
				"2011款 1.5 手动 优悦型,qiruiee","2011款 1.5 手动 新悦型,qiruief","2012款 1.5 手动 卓悦型,qiruieg","2012款 1.5 手动 智悦型,qiruieh",
				"2012款 1.5 手动 优悦型,qiruiei","2012款 1.5 手动 新悦型,qiruiej","2012款 1.5 手动 数字奥运版,qiruiek",
				"2013款 1.6 手动 出租版都市型 CNG油气混合,qiruiel","2013款 1.6 手动 出租版经济型 CNG油气混合,qiruiem","2013款 1.8 自动 运动型,qiruien",
				"2014款 1.8 自动 卓悦型,qiruieo","2014款 1.8 自动 优悦型,qiruiep","2014款 1.5 手动 卓悦型,qiruieq","2014款 1.5 手动 智悦型,qiruier",
				"2014款 1.5 手动 优悦型,qiruies"
				],
			
			"奇瑞eQ":["2015款 豪华型 纯电动,qiruifa","2015款 舒适型 纯电动,qiruifb"
				],
   
			"奇瑞QQ3":["2005款 两厢  0.8 手动 豪华型,qiruiga","2005款 两厢  0.8 手动 舒适型,qiruigb","2005款 两厢  0.8 手动 实力型,qiruigc","2005款 两厢  0.8 手动 贺岁版,qiruigd",
				"2005款 两厢  0.8 手动 标准型,qiruige","2005款 两厢  0.8 手动 基本型,qiruigf","2005款 两厢  1.1 手动 实力型助力转向,qiruigg","2005款 两厢  1.1 手动 标准型助力转向,qiruigh",
				"2005款 两厢  0.8 自动 EZ-drive豪华型,qiruigi","2005款 两厢  0.8 自动 EZ-drive舒适型,qiruigj","2005款 两厢  1.1 手动 豪华型,qiruigk","2005款 两厢  1.1 手动 舒适型,qiruigl",
				"2005款 两厢  1.1 手动 实力型,qiruigm","2005款 两厢  1.1 手动 标准型,qiruign","2006款 两厢  1.1 手动 舒适型,qiruigo","2006款 两厢  1.1 手动 豪华型,qiruigp",
				"2006款 两厢  1.1 手动 标准型,qiruigq","2006款 两厢  0.8 手动 贺岁型,qiruigr","2006款 两厢  0.8 自动 豪华型,qiruigs","2006款 两厢  0.8 自动 标准型,qiruigt",
				"2006款 两厢  0.8 自动 EZ小精灵豪华型,qiruigu","2006款 两厢  0.8 自动 EZ小精灵舒适型,qiruigv","2006款 两厢  0.8 自动 EZ小精灵标准型,qiruigw",
				"2006款 两厢  0.8 手动 小康生活豪华型,qiruigx","2006款 两厢  0.8 手动 小康生活标准型,qiruigy","2006款 两厢  0.8 手动 幸福家庭标准型,qiruigz",
				"2006款 两厢  1.1 手动 知识精英标准型,qiruigaa","2006款 两厢  1.1 手动 幸福家庭豪华型,qiruigab","2006款 两厢  1.1 手动 知识精英舒适型,qiruigac",
				"2006款 两厢  0.8 自动 都市丽人标准型,qiruigad","2006款 两厢  0.8 自动 都市丽人舒适型,qiruigae","2006款 两厢  0.8 自动 都市丽人豪华型,qiruigaf",
				"2006款 两厢  0.8 手动 专业致富标准型,qiruigag","2006款 两厢  1.1 手动 知识精英豪华型,qiruigah","2006款 两厢  0.8 手动 幸福家庭舒适型,qiruigai",
				"2006款 两厢  0.8 手动 小康生活舒适型,qiruigaj","2006款 两厢  0.8 手动 精英型,qiruigak","2006款 两厢  0.8 手动 豪华型,qiruigal","2006款 两厢  0.8 手动 标准型,qiruigam",
				"2006款 两厢  1.1 手动 实力型,qiruigan","2006款 两厢  0.8 手动 实力型,qiruigao","2006款 两厢  0.8 自动 实力型,qiruigap","2006款 两厢  1.1 手动 精英型,qiruigaq",
				"2006款 两厢  0.8 自动 精英型,qiruigar","2006款 两厢  0.8 手动 舒适型,qiruigas","2006款 两厢  0.8 自动 舒适型,qiruigat","2006款 两厢  0.8 手动 基本型,qiruigau",
				"2007款 两厢  1.1 自动 启航型,qiruigav","2007款 两厢  0.8 自动 舒适型,qiruigaw","2007款 两厢  0.8 自动 实力型,qiruigax","2007款 两厢  0.8 自动 精英型,qiruigay",
				"2007款 两厢  0.8 自动 标准型,qiruigaz","2007款 两厢  1.1 自动 豪华型,qiruigba","2007款 两厢  1.1 自动 舒适型,qiruigbb","2007款 两厢  1.1 手动 舒适型,qiruigbc",
				"2007款 两厢  1.1 手动 豪华型,qiruigbd","2007款 两厢  1.1 手动 标准型,qiruigbe","2007款 两厢  0.8 手动 舒适型,qiruigbf","2007款 两厢  0.8 手动 基本型,qiruigbg",
				"2007款 两厢  0.8 手动 标准型,qiruigbh","2009款 两厢  1.1 手动 启航型,qiruigbi","2009款 两厢  1.0 手动,qiruigbj","2009款 两厢  1.1 手动,qiruigbk",
				"2009款 两厢  0.8 手动 限量型,qiruigbl","2009款 两厢  0.8 手动 舒适型,qiruigbm","2009款 两厢  0.8 手动 基本型,qiruigbn","2009款 两厢  1.1 自动 启航型,qiruigbo",
				"2009款 两厢  0.8 手动 启航型,qiruigbp","2010款 两厢  1.0 自动 快乐巡航版,qiruigbq","2010款 两厢  1.0 自动 自由巡航版,qiruigbr",
				"2011款 两厢  0.8 手动 领航版,qiruigbs","2011款 两厢  0.8 手动 冠军版,qiruigbt","2011款 两厢  1.0 手动 启航型,qiruigbu","2011款 两厢  1.0 手动 领航型,qiruigbv",
				"2011款 两厢  1.0 自动 自由巡航型,qiruigbw","2011款 两厢  1.0 自动 快乐巡航型,qiruigbx","2011款 两厢  1.0 自动 给力巡航型,qiruigby","2011款 两厢  0.8 手动 启航型,qiruigbz",
				"2011款 两厢  0.8 手动 领航型,qiruigca","2011款 两厢  0.8 手动 基本型,qiruigcb","2011款 两厢  0.8 手动 冠军型,qiruigcc",
				"2012款 两厢  0.8 手动 暑期限量版型,qiruigcd","2012款 两厢  EV启航升级版选装空调 纯电动,qiruigce","2012款 两厢  EV运动版 纯电动,qiruigcf","2012款 两厢  EV启航升级版 纯电动,qiruigcg",
				"2012款 两厢  1.0 手动 时尚版启航型,qiruigah","2012款 两厢  1.0 手动 时尚版冠军型,qiruigai","2012款 两厢  1.0 手动 时尚版爱尚型,qiruigaj","2012款 两厢  1.0 手动 运动版启航型,qiruigak",
				"2012款 两厢  1.0 手动 运动版爱尚型,qiruigal","2012款 两厢  1.0 自动 时尚版启航型,qiruigam","2012款 两厢  1.0 自动 时尚版爱尚型,qiruigan","2012款 两厢  0.8 手动 梦想版梦想型,qiruigao",
				"2013款 两厢  1.0 自动 巡航版,qiruigap","2013款 两厢  1.0 手动 时尚版,qiruigaq","2013款 两厢  1.0 手动 快乐版,qiruigar","2013款 两厢  1.0 手动 活力版,qiruigas"
				],
			
			
			"奇瑞QQ6":["2006款 三厢  1.3 手动 标准型,qiruiha","2006款 三厢  1.1 手动 基本型,qiruihb","2007款 三厢  1.3 手动 基本型,qiruihc",
					"2007款 三厢  1.3 手动 舒适型,qiruihd","2007款 三厢  1.3 手动 豪华型,qiruihe","2007款 三厢  1.1 手动 舒适型,qiruihf",
					"2007款 三厢  1.1 手动 豪华型,qiruihg","2007款 三厢  1.1 手动 标准型,qiruihi","2009款 三厢  1.0 手动 舒适型,qiruihj",
					"2009款 三厢  1.0 手动 豪华型,qiruihk","2009款 三厢  1.0 手动 标准型,qiruihl","2010款 三厢  1.3 自动 自由巡航版,qiruihm"
					],
					
			"奇瑞QQme":["2009款 1.3 手动 欢心型,qiruiia","2009款 1.3 手动 欢乐型,qiruiib","2009款 1.3 手动 欢动型,qiruiic",
					"2010款 1.3 自动 自由巡航版,qiruiid","2010款 1.3 自动 快乐巡航版,qiruiie"
					],		
				
			
			"奇瑞V5":["2006款 2.0 手动 精英版7座,qiruija","2006款 2.0 手动 华贵版7座,qiruijb","2006款 2.4 自动 精英版7座,qiruijc","2006款 2.4 自动 华贵版7座,qiruijd",
				"2007款 2.0 手动 舒适版7座,qiruije","2007款 2.4 自动 舒适版7座,qiruijf"
				], 
			
			
			"东方之子":["2005款 2.0 自动 华贵型,qiruika","2005款 2.0 自动 标准型,qiruikb","2005款 2.0 手动 豪华型,qiruikc","2005款 2.0 自动 尊贵型,qiruikd",
						"2005款 2.4 自动 豪华型,qiruike",
						"2005款 2.4 自动 旗舰版,qiruikf","2005款 2.0 手动 基本型,qiruikg","2005款 2.4 自动 旗舰型,qiruikh","2006款 2.0 自动 尊贵型,qiruiki",
						"2006款 2.0 自动 华贵型,qiruikj",
						"2006款 2.0 自动 标准型,qiruikk","2006款 2.4 自动 华贵型,qiruikl","2006款 2.0 手动 实力型,qiruikm","2006款 2.0 手动 精英型,qiruikn",
						"2006款 2.0 手动 舒适型,qiruiko",
						"2006款 2.0 手动 基本型,qiruikp","2007款 1.8 手动 舒适型,qiruikq","2007款 1.8 手动 豪华型,qiruikr","2007款 2.0 手动 油气混合,qiruiks",
						"2007款 1.8 手动 舒适型 CNG油气混合,qiruikt","2007款 1.8 手动 豪华型 CNG油气混合,qiruiku","2008款 2.0 自动 舒适型,qiruikv","2008款 2.0 自动 豪华型,qiruikw",
						"2008款 1.8 手动 舒适型,qiruikx","2008款 1.8 手动 豪华型,qiruiky","2008款 2.0 手动 舒适型,qiruikz","2008款 2.0 手动 豪华型,qiruikaa",
						"2009款 1.8 手动 舒适型 CNG油气混合,qiruikab","2009款 1.8 手动 豪华型 CNG油气混合,qiruikac",
						"2010款 1.9T 手动 舒适型 柴油,qiruikad","2010款 1.9T 手动 豪华型 柴油,qiruikae",
						"2012款 1.8 手动 俊雅版增配型,qiruikaf","2012款 2.0 自动 豪雅版,qiruikag","2012款 2.0 自动 尊雅版,qiruikah","2012款 2.0 自动 智雅版,qiruikai",
						"2012款 2.0 自动 典雅版,qiruikaj","2012款 2.0 手动 智雅版,qiruikak","2012款 2.0 手动 典雅版,qiruikal","2012款 1.8 手动 俊雅版,qiruikam"
						],

			"旗云":["2005款 1.6 手动 标准型,qiruila","2005款 1.6 自动 无间版,qiruilb","2006款 1.6 手动,qiruilc","2006款 1.3 手动,qiruild",
					"2006款 1.6 手动 LPG油气混合,qiruile",
					"2006款 1.6 手动 CNG油气混合,qiruilf","2006款 1.6 手动 F标准型,qiruilg",
					"2006款 1.6 手动 豪华型,qiruilh","2006款 1.6 手动 舒适型,qiruili","2006款 1.6 手动 旗舰型,qiruilj","2006款 1.6 手动 标准型,qiruilk",
					"2006款 1.6 自动 舒适型,qiruilm",
					"2006款 1.6 自动 旗云之星舒适型,qiruiln","2006款 1.6 自动 旗云之星旗舰型,qiruilo","2006款 1.6 手动 基本运动型,qiruilp","2006款 1.6 手动 豪华运动型,qiruilq",
					"2006款 1.6 手动 旗舰运动型,qiruilr","2006款 1.6 手动 舒适运动型,qiruils","2006款 1.6 手动 豪华型都市版,qiruilt","2006款 1.6 手动 旗舰型都市版,qiruilu",
					"2006款 1.6 手动 舒适型都市版,qiruilv","2006款 1.6 手动 基本型都市版,qiruilw","2006款 1.6 自动 无间版,qiruilx","2006款 1.6 手动 精英型,qiruily",
					"2006款 1.6 手动 F旗舰型,qiruilz","2006款 1.6 手动 F舒适型,qiruilaa","2006款 1.6 手动 F豪华型,qiruilab","2006款 1.6 手动 F出租型,qiruilac",
					"2006款 1.6 手动 基本型,qiruilad",
					"2006款 1.6 自动 旗舰型,qiruilae","2007款 1.3 手动 基本型,qiruilaf","2007款 1.3 手动 豪华型,qiruilag","2007款 1.6 手动 豪华型,qiruilah",
					"2007款 1.6 手动 舒适型,qiruilai",
					"2007款 1.6 手动 标准型,qiruilaj","2007款 1.3 手动 舒适型,qiruilak","2007款 1.3 手动 标准型,qiruilal","2008款 1.6 手动 旗云之星,qiruilam",
					"2008款 1.6 手动,qiruilan",
					"2008款 1.6 手动 CNG油气混合,qiruilao","2008款 1.6 自动 舒适型,qiruilap","2008款 1.3 手动 CNG油气混合,qiruilaq",
					"2008款 1.5 手动 舒适型,qiruilar","2008款 1.6 手动 舒适型,qiruilas","2008款 1.6 手动 豪华型,qiruilat","2008款 1.5 手动 标准型,qiruilau",
					"2008款 1.3 手动 舒适型,qiruilav",
					"2008款 1.3 手动 标准型,qiruilaw","2008款 1.5 手动 豪华型,qiruilax","2009款 1.5 手动 CNG油气混合,qiruilay","2009款 1.6 手动 CNG油气混合,qiruilaz",
					"2009款 1.6 手动 ,qiruilba","2009款 1.5 手动 基本型,qiruilbb","2009款 1.5 手动 实力型,qiruilbc"
					],
			
			"旗云1":["2010款 1.3 手动 豪华型,qiruima","2010款 1.3 自动 舒适型,qiruimb","2010款 1.0 手动 舒适型,qiruimc","2010款 1.0 手动 豪华型,qiruimd","2010款 1.0 手动 标准型,qiruime",
					"2012款 1.0 手动 豪华型,qiruimf","2012款 1.0 手动 标准型,qiruimg","2012款 1.3 自动 舒适型,qiruimh","2012款 1.0 手动 数智导航版,qiruimi","2012款 1.0 手动 舒适型,qiruimj",
					"2012款 1.0 手动 数智版,qiruimk"
					],
			
			"旗云2":["2010款 1.6 手动 尊贵型,qiruina","2010款 1.6 手动 舒适型,qiruinb","2010款 1.6 手动 实力型,qiruinc","2010款 1.6 手动 豪华型,qiruind",
					"2010款 1.6 手动 标准型,qiruine",
					"2012款 1.5 手动 尊贵型,qiruinf","2012款 1.5 手动 舒适型,qiruing","2012款 1.5 手动 实力型,qiruinh","2012款 1.6 手动 尊贵型 油气混合,qiruini",
					"2012款 1.6 手动 豪华型 油气混合,qiruinj","2012款 1.6 手动 舒适型 油气混合,qiruink","2012款 1.6 手动 标准型 油气混合,qiruinl",
					"2012款 1.6 手动 实力型 油气混合,qiruinm",
					"2012款 1.5 手动 尊贵型,qiruinn","2012款 1.5 手动 舒适型,qiruino","2012款 1.5 手动 实力型,qiruinp","2012款 1.5 手动 豪华型,qiruinq",
					"2012款 1.5 手动 标准型,qiruinr",
					"2012款 1.5 手动 豪华型,qiruins","2012款 1.5 手动 标准型,qiruint"
					], 
			
			
			"旗云3":["2010款 1.6 手动 舒适型 油气混合,qiruioa","2010款 1.8 手动 豪华型,qiruiob","2010款 1.8 手动 精英型,qiruioc","2010款 1.6 自动 油气混合,qiruiod",
					"2010款 1.5 手动 精英型,qiruioe","2010款 1.5 手动 舒适型,qiruiof","2010款 1.5 手动 华贵型,qiruiog","2010款 2.0 自动 豪华型,qiruioh"
					],
			
			
			"旗云5":["2012款 1.8 自动 导航版,qiruipa","2012款 1.8 自动 巡航版,qiruipb","2012款 1.8 手动 豪华版,qiruipc","2012款 1.8 手动 舒适版,qiruipd",
					"2012款 1.8 手动 精英版,qiruipe"
					],
			
			"爱卡":["2013款 2.5T 手动 标准型 柴油,qiruiqa","2013款 2.2 手动 舒适型,qiruiqb","2013款 2.2 手动 标准型,qiruiqc"],    
			
			
			"瑞虎":["2005款 2.0 手动 精英型前驱,qiruira","2005款 2.4 自动 标准型前驱,qiruirb","2005款 2.4 手动 豪华型前驱,qiruirc","2005款 2.4 手动 舒适型前驱,qiruird",
					"2005款 2.4 自动 精英型前驱,qiruire","2005款 2.0 手动 标准型前驱,qiruirf","2006款 1.6 手动 实力型前驱,qiruirg",
					"2007款 2.0 自动 瑞虎3豪华型前驱,qiruirh","2007款 1.8 手动 瑞虎3精英型前驱,qiruiri","2007款 1.8 手动 瑞虎3舒适型前驱,qiruirj",
					"2007款 1.8 手动 瑞虎3豪华型前驱,qiruirk",
					"2007款 1.6 手动 瑞虎3舒适型前驱,qiruirl","2007款 2.0 自动 豪华型前驱,qiruirm","2007款 2.0 自动 舒适型前驱,qiruirn",
					"2007款 1.6 手动 瑞虎3豪华型前驱,qiruiro",
					"2007款 2.0 自动 瑞虎3舒适型前驱,qiruirp","2006款 2.4 手动 豪华型四驱,qiruirq","2006款 2.4 手动 舒适型四驱,qiruirr","2006款 1.6 手动 舒适型前驱,qiruirs",
					"2008款 2.0 手动 瑞虎3四驱,qiruirt","2008款 1.6 手动 瑞虎3精英型前驱,qiruiru",
					"2009款 2.0 手动 瑞虎3经典版舒适型四驱,qiruirv","2009款 2.0 手动 瑞虎3经典版豪华型四驱,qiruirw","2009款 1.8 手动 瑞虎3舒适型前驱,qiruirx",
					"2009款 1.8 手动 瑞虎3豪华型前驱,qiruiry","2009款 2.0 手动 舒适型四驱,qiruirz","2009款 2.0 手动 豪华型四驱,qiruiraa","2009款 2.0 手动 前驱,qiruirab",
					"2009款 1.8 手动 前驱,qiruirac","2009款 1.8 自动 前驱,qiruirad","2009款 2.0 手动 瑞虎3舒适型四驱,qiruirae","2009款 2.0 手动 瑞虎3豪华型四驱,qiruiraf",
					"2009款 2.0 自动 瑞虎3豪华型前驱,qiruirag","2009款 1.6 手动 瑞虎3舒适型前驱,qiruirah","2009款 2.0 自动 瑞虎3舒适型前驱,qiruirai",
					"2009款 1.6 手动 瑞虎3豪华型前驱,qiruiraj",
					"2010款 1.6T 手动 舒适型前驱,qiruirak","2010款 1.6T 手动 豪华型前驱,qiruiral","2010款 2.0 自动 DR舒适型前驱,qiruiram","2010款 2.0 自动 DR豪华型前驱,qiruiran",
					"2010款 1.8 手动 DR舒适型前驱,qiruirao","2010款 1.8 手动 DR豪华型前驱,qiruirap","2010款 1.8 手动 大师版豪华型前驱,qiruiraq",
					"2010款 1.8 手动 大师版舒适型前驱,qiruirar",
					"2011款 1.6 手动 精英版舒适型前驱,qiruiras","2011款 1.6 手动 精英版豪华型前驱,qiruirat","2011款 1.8 手动 舒适型前驱,qiruirau",
					"2011款 1.8 手动 豪华型前驱,qiruirav","2011款 1.8 自动 舒适型前驱,qiruiraw","2011款 1.8 自动 豪华型前驱,qiruirax","2011款 1.6 手动 舒适型前驱,qiruiray",
					"2011款 1.6 手动 豪华型前驱,qiruiraz","2011款 1.6 手动 S舒适型前驱,qiruirba","2011款 1.6 手动 S豪华型前驱,qiruirbb",
					"2012款 1.6 自动 精英版豪华型前驱,qiruirbc","2012款 1.6 自动 精英版舒适型前驱,qiruirbd","2012款 2.0 自动 DR欧版尊贵型前驱,qiruirbe",
					"2012款 1.8 手动 DR欧版精英型前驱,qiruirbf","2012款 2.0 手动 精英版豪华型四驱,qiruirbg","2012款 1.8 自动 精英版舒适型前驱,qiruirbh",
					"2012款 1.8 自动 精英版豪华型前驱,qiruirbi","2012款 1.6T 手动 S精英版舒适型前驱,qiruirbj","2012款 1.6T 手动 S精英版豪华型前驱,qiruirbk",
					"2012款 1.6 手动 精英版舒适型前驱,qiruirbl","2012款 1.6 手动 精英版豪华型前驱,qiruirbm","2012款 1.6 手动 经典版舒适型前驱,qiruirbn",
					"2012款 1.6 手动 经典版豪华型前驱,qiruirbo","2013款 1.6 手动 贺岁版前驱,qiruirbp"
					],
			
			"瑞虎3":["2014款 1.6 自动 魔力尊尚版,qiruisa","2014款 1.6 自动 魔力智尚版,qiruisb","2014款 1.6 自动 魔力风尚版,qiruisc","2014款 1.6 手动 魔力尊尚版,qiruisd",
					"2014款 1.6 手动 魔力智尚版,qiruise","2014款 1.6 手动 魔力风尚版,qiruisf","2014款 1.6 自动 尊尚版,qiruisg","2014款 1.6 自动 智尚版,qiruish",
					"2014款 1.6 自动 风尚版,qiruisi","2014款 1.6 手动 尊尚版,qiruisj","2014款 1.6 手动 智尚版,qiruisk","2014款 1.6 手动 风尚版,qiruisl",
					"2015款 1.6 自动 智尚纪念版,qiruism","2015款 1.6 手动 智尚纪念版,qiruisn","2015款 1.6 自动 尊尚周年运动版,qiruiso","2015款 1.6 自动 智尚周年运动版,qiruisp",
					"2015款 1.6 自动 风尚周年运动版,qiruisq","2015款 1.6 手动 尊尚周年运动版,qiruisr","2015款 1.6 手动 智尚周年运动版,qiruiss","2015款 1.6 手动 风尚周年运动版,qiruist"
					],

			
			"瑞虎5":["2014款 2.0 自动 智云版,qiruita","2014款 2.0 手动 智云版,qiruitb","2014款 2.0 自动 家臻版,qiruitc","2014款 2.0 自动 家尊版,qiruitd",
					"2014款 2.0 自动 家悦版,qiruite","2014款 2.0 手动 家尊版,qiruitf","2014款 2.0 手动 家悦版,qiruitg","2014款 2.0 手动 家享版,qiruith",
					"2015款 1.5T 手动 智云版,qiruiti","2015款 1.5T 手动 家尊版,qiruitj",
					"2015款 1.5T 手动 家享版,qiruitk","2015款 1.5T 手动 家悦版,qiruitl","2016款 1.5T 手动 卓越版,qiruitm","2016款 1.5T 手动 家悦版,qiruitn",
					"2016款 1.5T 手动 家享版,qiruito","2016款 2.0 自动 荣耀版,qiruitp","2016款 2.0 自动 卓越版,qiruitq",
					"2016款 2.0 自动 家尊版,qiruitr","2016款 2.0 自动 家悦版,qiruits","2016款 2.0 手动 家悦版,qiruitt","2016款 2.0 手动 家享版,qiruitu"
					],
			
			
			"艾瑞泽3":["2015款 1.5 自动 够酷版,qiruiva","2015款 1.5 手动 够酷版,qiruivb","2015款 1.5 自动 够劲版,qiruivc","2015款 1.5 手动 够劲版,qiruivd","2015款 1.5 自动 够炫版,qiruive",
						"2015款 1.5 自动 够型版,qiruivf","2015款 1.5 手动 够炫版,qiruivg","2015款 1.5 手动 够型版,qiruivh","2015款 1.5 手动 够真版,qiruivi","2015款 1.5 手动 够惠版,qiruivj"
						],
     
			"艾瑞泽7":["2013款 1.6 自动 致尊版,qiruiva","2013款 1.6 自动 致享版,qiruivb","2013款 1.6 自动 致尚版,qiruivc","2013款 1.6 自动 致领版,qiruivd",
						"2013款 1.6 手动 致享版,qiruive","2013款 1.6 手动 致尚版,qiruivf","2013款 1.6 手动 致领版,qiruivg","2013款 1.6 手动 致真版,qiruivh",
						"2014款 1.6 自动 智云魔力版,qiruivi","2014款 1.6 自动 致享魔力版,qiruivj","2014款 1.6 自动 致尚魔力版,qiruivk","2014款 1.6 手动 智云魔力版,qiruivl",
						"2014款 1.6 手动 致享魔力版,qiruivm","2014款 1.6 手动 致尚魔力版,qiruivn","2014款 1.6 手动 智云版,qiruivo","2014款 1.6 自动 智云版,qiruivp",
						"2014款 1.6 手动 致诚版,qiruivq","2015款 1.5T 手动 致尊版,qiruivr","2015款 1.5T 手动 致享版,qiruivs","2015款 1.5T 手动 致尚版,qiruivt",
						"2015款 1.5T 手动 致领版,qiruivu","2015款 1.6 自动 致享版,qiruivv","2015款 1.6 自动 致尚版,qiruivw","2015款 1.6 自动 致领版,qiruivx",
						"2015款 1.6 手动 致享版,qiruivy","2015款 1.6 手动 致尚版,qiruivz","2015款 1.6 手动 致领版,qiruivaa"
						],

			"艾瑞泽M7":["2015款 2.0 自动 宽享版232座,qiruiwa","2015款 2.0 自动 宽悦版232座,qiruiwb","2015款 1.8 手动 宽悦版232座,qiruiwc","2015款 1.8 手动 宽享版232座,qiruiwd",
						"2015款 1.8 手动 宽适版232座,qiruiwe","2015款 2.0 自动 宽享版223座,qiruiwf","2015款 2.0 自动 宽悦版223座,qiruiwg","2015款 1.8 手动 宽享版223座,qiruiwh",
						"2015款 1.8 手动 宽悦版223座,qiruiwi","2015款 1.8 手动 宽适版223座,qiruiwj"
						],
						
			"风云":["2008款 1.6 手动 4代,qiruixa"],			

     		"风云2":["2010款 两厢  1.5 手动 尊贵型增配版,qiruiya","2010款 两厢  1.5 手动 实力型增配版,qiruiyb","2010款 两厢  1.5 手动 进取型增配版,qiruiyc","2010款 两厢  1.5 手动 豪华型增配版,qiruiyd",
					"2010款 两厢  1.5 手动 尊贵型,qiruiye","2010款 两厢  1.5 手动 实力型,qiruiyf","2010款 两厢  1.5 手动 进取型,qiruiyg","2010款 两厢  1.5 手动 豪华型,qiruiyh",
					"2010款 三厢  1.5 手动 进取型,qiruiyi","2010款 三厢  1.5 手动 尊贵型,qiruiyj","2010款 三厢  1.5 手动 实力型,qiruiyk","2010款 三厢  1.5 手动 豪华型,qiruiyl",
					"2012款 两厢  1.5 手动 锐意版,qiruiym","2013款 两厢  1.5 自动 畅意版,qiruiyn","2013款 两厢  1.5 自动 锐意版,qiruiyo","2013款 两厢  1.5 手动 畅意版,qiruiyp",
					"2013款 两厢  1.5 手动 锐意版,qiruiyq","2013款 两厢  1.5 手动 快意版,qiruiyr","2013款 三厢  1.5 自动 锐意版,qiruiys","2013款 三厢  1.5 自动 畅意版,qiruiyt",
					"2013款 三厢  1.5 手动 畅意版,qiruiyu","2013款 三厢  1.5 手动 锐意版,qiruiyv","2013款 三厢  1.5 手动 快意版,qiruiyw","2014款 两厢  1.5 手动 新青年版,qiruiyx",           
					"2015款 两厢  1.5 手动 超值版,qiruiyy","2015款 两厢  1.5 手动 新意版,qiruiyz","2015款 两厢  1.5 手动 新锐版,qiruiyaa"
					],	
			
			
//日产			

		
			"启辰D50":["2012款 1.6 自动 尊贵版,richanaa","2012款 1.6 自动 舒适版,richanab","2012款 1.6 手动 尊贵版,richanac","2012款 1.6 手动 舒适版,richanad",
						"2013款 1.6 自动 北斗导航版,richanae","2013款 1.6 自动 时尚版,richanaf","2013款 1.6 自动 豪华版,richanag","2013款 1.6 手动 北斗导航版,richanah",
						"2013款 1.6 手动 豪华版,richanai","2013款 1.6 手动 时尚版,richanaj","2014款 1.6 自动 精彩版,richanak","2014款 1.6 手动 精彩版,richanal",
						"2015款 1.6 自动 北斗导航版,richanam","2015款 1.6 手动 北斗导航版,richanan","2015款 1.6 自动 豪华版,richanao","2015款 1.6 手动 豪华版,richanap",
						"2015款 1.6 自动 时尚版,richanaq","2015款 1.6 手动 时尚版,richanar"
						],
			
			"启辰R30":["2014款 1.2 手动 尊享版,richanba","2014款 1.2 手动 优享版,richanbb","2014款 1.2 手动 舒享版,richanbc","2014款 1.2 手动 易享版,richanbd"],
			
			"启辰R50":["2012款 1.6 自动 尊贵版,richanca","2012款 1.6 自动 舒适版,richancb","2012款 1.6 手动 尊贵版,richancc","2012款 1.6 手动 舒适版,richancd",
					"2013款 1.6 自动 时尚版,richance","2013款 1.6 自动 豪华版,richancf","2013款 1.6 自动 北斗导航版,richancg","2013款 1.6 手动 北斗导航版,richanch",
					"2013款 1.6 手动 豪华版,richanci","2013款 1.6 手动 时尚版,richancj","2014款 1.6 手动 精彩版,richanck","2014款 1.6 自动 精彩版,richancl",
					"2015款 1.6 自动 北斗导航版,richancm","2015款 1.6 手动 北斗导航版,richancn","2015款 1.6 自动 豪华版,richanco","2015款 1.6 手动 豪华版,richancp",
					"2015款 1.6 自动 时尚版,richancq","2015款 1.6 手动 时尚版,richancr"
					],
								
			"启辰R50X":["2013款 1.6 自动 北斗导航版,richanda","2013款 1.6 自动,richandb","2013款 1.6 手动,richandc","2013款 1.6 手动 北斗导航版,richandd",
						"2015款 1.6 自动 潮流版,richande","2015款 1.6 手动 潮流版,richandf","2015款 1.6 手动 北斗导航版,richandg","2015款 1.6 自动 北斗导航版,richandh",
						"2015款 1.6 自动 豪华版,richandi","2015款 1.6 手动 豪华版,richandj"
						],
									
			"启辰T70":["2015款 1.6 手动 睿行版,richanea","2015款 2.0 自动 睿享版,richaneb","2015款 2.0 自动 睿趣版,richanec","2015款 2.0 自动 睿行版,richaned",
						"2015款 2.0 手动 睿享版,richanee","2015款 2.0 手动 睿趣版,richanef"
						],
			
			"启辰晨风":["2015款 6.6KW 领航版 纯电动,richanfa","2015款 6.6KW 领风版 纯电动,richanfb","2015款 3.6KW 领风版 纯电动,richanfc"],
			
			"天籁":["2005款 3.5 自动 350JM-VIP,richanga","2006款 3.5 自动 350JM-VIP旗舰型,richangb","2006款 2.0 自动 200JK豪华型,richangc","2006款 2.3 自动 230JM科技型,richangd",
					"2006款 2.3 自动 230JM尊贵型,richange","2006款 2.3 自动 230JK豪华型天窗版,richangf","2006款 2.3 自动 230JK豪华型,richangg",
					"2007款 3.5 自动 350JM-VIP旗舰型,richangh","2007款 3.5 自动 350JM旗舰型,richangi","2007款 2.0 自动 200JK豪华型天窗版,richangj",
					"2007款 2.0 自动 200JK豪华型,richangk","2007款 2.3 自动 230JM科技型,richangl","2007款 2.3 自动 230JM尊贵型,richangm",
					"2007款 2.3 自动 230JK豪华型天窗版,richangn","2007款 2.3 自动 230JK豪华型,richango","2007款 2.3 自动 230JK舒适型天窗版,richangp","2007款 2.3 自动 230JK舒适型,richangq",
					"2008款 2.5 自动 XV尊雅导航版,richangr","2008款 2.0 自动 XL舒适版,richangs","2008款 2.5 自动 XL领先版,richangt","2008款 3.5 自动 XV尊驭版,richangu",
					"2008款 3.5 自动 XV-VIP至尊版,richangv","2008款 2.5 自动 XL-Res剧院版,richangw","2008款 2.0 自动 XL-Opt豪华版,richangx",
					"2008款 2.0 自动 XE标准版,richangy","2008款 2.5 自动 XV尊雅版,richangz","2009款 2.5 自动 XV-VIP尊享版,richangaa","2009款 2.0 自动 精英版,richangab","2009款 2.5 自动 RX酷动版,richangac",
					"2010款 2.5 自动 XL周年纪念版,richangad","2010款 2.0 自动 XL周年纪念版,richangae",
					"2011款 3.5 自动 XV-VIP尊领版,richangaf","2011款 2.5 自动 XV尊雅版,richangag","2011款 2.5 自动 XV-VIP尊尚版,richangah","2011款 2.5 自动 XL荣耀版,richangai",
					"2011款 2.5 自动 XL领先版,richangaj","2011款 2.5 自动 XL-NAVI智领版,richangak","2011款 2.0 自动 XL舒适版,richangal","2011款 2.0 自动 XL荣耀版,richangam",
					"2011款 2.0 自动 XL-NAVI智尚版,richangan","2011款 2.0 自动 XE标准版,richangao","2012款 2.5 自动 XV云安全版,richangap",
					"2012款 2.5 自动 XV-VIP云安全版,richangaq","2012款 2.5 自动 XL智享版,richangar","2012款 2.5 自动 XL-NAVI云安全版,richangas","2012款 2.0 自动 XL智享版,richangat",
					"2013款 2.5 自动 XL-Upper NAVI Tech尊贵版,richangau","2013款 2.5 自动 XL-NAVI Tech智享版,richangav","2013款 2.5 自动 XL-NAVI豪华版,richangaw",
					"2013款 2.5 自动 XL领先版,richangax","2013款 2.0 自动 XL-NAVI智领版,richangay","2013款 2.0 自动 XL舒适版,richangaz","2013款 2.0 自动 XE时尚版,richangba",
					"2014款 2.5 自动 公爵XV荣耀版,richangbb","2014款 2.5 自动 XL-Upper科技版,richangbc","2014款 2.0 自动 XL-Upper科技版,richangbd",
					"2014款 2.0 自动 公爵XV-NAVI豪华特别版,richangbe","2014款 2.0 自动 公爵XV精英特别版,richangbf","2014款 2.5 自动 公爵XV VIP尊领版,richangbg",
					"2014款 2.5 自动 公爵XV-NAVI-FES尊尚版,richangbh","2014款 2.5 自动 公爵XV尊雅版,richangbi","2014款 2.5 自动 公爵XV尊享版,richangbj",
					"2015款 2.5 自动 公爵XV-VIP欧冠尊领版,richangbk","2015款 2.5 自动 公爵XV-NAVI-FES欧冠尊尚版,richangbl","2015款 2.5 自动 公爵XV欧冠尊雅版,richangbm",
					"2015款 2.5 自动 公爵XV欧冠荣耀版,richangbn","2015款 2.5 自动 XL-UpperNAVI Tech欧冠尊贵版,richangbo","2015款 2.0 自动 XL-Upper欧冠科技版,richangbp",
					"2015款 2.0 自动 XL-Sporty欧冠运动版,richangbq","2015款 2.0 自动 XE-Sporty欧冠运动版,richangbr","2015款 2.5 自动 XL-NAVI Tech欧冠智享版,richangbs",
					"2015款 2.5 自动 XL-Upper欧冠科技版,richangbt"
					],
			
			"东风奇骏":["2008款 2.5 自动 XV旗舰型四驱,richanha","2008款 2.5 手动 XL豪华型四驱,richanhb","2008款 2.5 自动 XL豪华型四驱,richanhc",
					"2008款 2.0 手动 XE舒适型四驱,richanhd","2008款 2.0 自动 XE舒适型四驱,richanhe",
					"2010款 2.5 自动 XV至尊型四驱,richanhf","2010款 2.5 自动 XV旗舰型四驱,richanhg","2010款 2.5 手动 XL豪华型四驱,richanhh",
					"2010款 2.5 自动 XL豪华型四驱,richanhi","2010款 2.0 手动 XE舒适型四驱,richanhj","2010款 2.0 自动 XE舒适型四驱,richanhk",
					"2012款 2.5 自动 XV尊享版四驱,richanhl","2012款 2.5 自动 XV至尊版四驱,richanhm","2012款 2.5 自动 XV旗舰版四驱,richanhn",
					"2012款 2.5 手动 XL豪华版四驱,richanho","2012款 2.5 自动 XL豪华版四驱,richanhp","2012款 2.0 手动 XE舒适版四驱,richanhq",
					"2012款 2.0 自动 XE舒适版四驱,richanhr",
					"2014款 2.0 手动 XE时尚版两驱,richanhs","2014款 2.0 自动 XE智驱版四驱,richanht","2014款 2.0 自动 XL智领版两驱,richanhu",
					"2014款 2.0 自动 XL舒适版两驱,richanhv","2014款 2.0 自动 XE时尚版两驱,richanhw","2014款 2.5 自动 XV-NAVI至尊版四驱,richanhx",
					"2014款 2.5 自动 XL尊享版四驱,richanhy","2014款 2.5 自动 XL豪华版四驱,richanhz","2014款 2.5 自动 XL领先版四驱,richanhaa",
					"2015款 2.5 自动 舒适型MAX版四驱,richanhab","2015款 2.0 自动 舒适型MAX版两驱,richanhac"
					],
			
			"楼兰":["2011款 3.5 自动 四驱,richania","2013款 3.5 自动 XV-VIP四驱,richanib","2013款 3.5 自动 XV荣耀版四驱,richanic",
					"2015款 2.5T 自动 XV旗舰版四驱 油电混合,richanid","2015款 2.5T 自动 XL尊雅版两驱 油电混合,richanie","2015款 2.5T 自动 XE尊享版两驱 油电混合,richanif",
					"2015款 2.5 自动 XV豪雅版两驱,richanig","2015款 2.5 自动 XV豪华版两驱,richanih","2015款 2.5 自动 XL-NAVI智领版两驱,richanii",
					"2015款 2.5 自动 XL-NAVI智享版两驱,richanij","2015款 2.5 自动 XL智尚版两驱,richanik","2015款 2.5 自动 XE精英版两驱,richanil"
					],
			
			
			"玛驰":["2010款 1.5 手动 XL易炫版,richanja","2010款 1.5 手动 XE易型版,richanjb","2010款 1.5 自动 XV易智版,richanjc","2010款 1.5 自动 XL易炫版,richanjd",
					"2011款 1.5 手动 XL酷动SPORTY版,richanje","2015款 1.5 自动 XV易智版,richanjf","2015款 1.5 自动 XL易炫版,richanjg","2015款 1.5 自动 XE易型版,richanjh",
					"2015款 1.5 手动 XL易炫版,richanji","2015款 1.2 手动 XE易享版,richanjj"
					],
					
			"蓝鸟":["2005款 2.0 手动 豪华型,richanka","2005款 2.0 自动 尊贵导航限量版,richankb","2005款 2.0 自动 尊贵舒适型,richankc","2005款 2.0 自动 旗舰型,richankd",
					"2005款 2.0 自动 豪华型,richanke","2016款 1.6 自动 高能版,richankf","2016款 1.6 自动 智酷版,richankg","2016款 1.6 自动 炫酷版,richankh",
					"2016款 1.6 手动 炫酷版,richanki","2016款 1.6 手动 时尚型,richankj"
					],
					
			"轩逸":["2005款 2.0 自动 XE,richanla","2006款 1.6 自动 XE,richanlb","2006款 2.0 自动 XV导航版,richanlc","2006款 2.0 自动 XL天窗版,richanld",
					"2006款 2.0 自动 XV,richanle","2006款 2.0 自动 XL,richanlf","2006款 2.0 自动 XE,richanlg",
					"2007款 1.6 自动 XE豪华版,richanlh","2007款 1.6 自动 XE舒适版,richanli","2007款 2.0 自动 XV尊贵版NAVI,richanlj","2007款 2.0 自动 XV尊贵版DVD,richanlk",
					"2007款 2.0 自动 XL科技天窗版,richanll","2007款 2.0 自动 XL科技版,richanlm","2007款 2.0 自动 XL豪华版,richanln","2007款 2.0 自动 XE舒适版,richanlo",
					"2008款 2.0 自动 XL豪华版,richanlp","2008款 1.6 自动 XE舒适版,richanlq","2008款 2.0 自动 XL科技版,richanlr","2008款 2.0 自动 XL科技版,richanls",
					"2008款 2.0 自动 XL科技天窗版,richanlt","2008款 2.0 自动 XV尊贵版,richanlu","2008款 2.0 自动 XE舒适版,richanlv","2008款 1.6 自动 XL豪华版,richanlw",
					"2008款 2.0 自动 XL致酷版,richanlx",
					"2009款 2.0 自动 XL豪华天窗版,richanly","2009款 1.6 手动 XL豪华天窗版,richanlz","2009款 1.6 手动 XE舒适版,richanlaa","2009款 1.6 自动 XL豪华天窗版,richanlab",
					"2009款 1.6 自动 XE舒适版,richanlac","2009款 2.0 自动 XL科技天窗版,richanlad","2009款 2.0 自动 XL科技版,richanlae","2009款 2.0 自动 XL豪华版,richanlaf",
					"2012款 1.8 手动 XL豪华版,richanlag","2012款 1.6 自动 XL豪华版,richanlah","2012款 1.6 手动 XL豪华版,richanlai","2012款 1.8 自动 XE舒适版,richanlaj",
					"2012款 1.6 手动 XE舒适版,richanlak","2012款 1.6 自动 XE舒适版,richanlal","2012款 1.6 自动 XL豪华版经典款,richanlam","2012款 1.6 手动 XL豪华版经典款,richanlan",
					"2012款 1.6 自动 XE舒适版经典款,richanlao","2012款 1.6 手动 XE舒适版经典款,richanlap","2012款 1.8 自动 XV尊享版,richanlaq","2012款 1.8 自动 XL豪华版,richanlar",
					"2012款 1.8 手动 XL豪华增配版,richanlas","2014款 1.6 自动 XV尊享版,richanlat"
					],
			
			"逍客":["2008款 1.6 手动 16G前驱,richanma","2008款 2.0 自动 20XV FOUR龙四驱,richanmb","2008款 2.0 自动 20X FOUR虎四驱,richanmc",
					"2008款 2.0 自动 20X雷前驱,richanmd","2008款 2.0 自动 20S炎运动加长型前驱,richanme","2008款 2.0 自动 20S火前驱,richanmf","2008款 2.0 手动 20S火前驱,richanmg",
					"2010款 2.0 手动 20S火前驱,richanmh","2010款 2.0 自动 20XV FOUR龙四驱,richanmi","2010款 2.0 自动 20X FOUR虎四驱,richanmj",
					"2010款 2.0 自动 20X雷前驱,richanmk","2010款 2.0 自动 20S火前驱,richanml","2011款 2.0 手动 XL火前驱,richanmm","2011款 2.0 自动 XV龙四驱,richanmn","2011款 2.0 自动 XV虎四驱,richanmo",
					"2011款 2.0 自动 XV雷前驱,richanmp","2011款 2.0 自动 XL火前驱,richanmq","2011款 1.6 手动 XE风前驱,richanmr",
					"2012款 2.0 手动 XL火前驱,richanms","2012款 2.0 自动 XV龙四驱,richanmt","2012款 2.0 自动 XV虎四驱,richanmu","2012款 2.0 自动 XV雷前驱,richanmv",
					"2012款 2.0 自动 XL火前驱,richanmw","2012款 1.6 手动 XE风前驱,richanmx","2013款 2.0 自动 XV炫前驱,richanmy",
					"2015款 2.0 自动 XV酷炫前驱,richanmz","2015款 2.0 自动 XV酷雷前驱,richanmaa","2015款 2.0 自动 XL酷火前驱,richanmab",
					"2015款 2.0 手动 XL酷火前驱,richanmac","2016款 2.0 自动 XV旗舰版TOP,richanmad","2016款 2.0 自动 XV尊享版NAVI,richanmae","2016款 2.0 自动 XV豪华版Prem,richanmaf",
					"2016款 2.0 自动 XL领先版,richanmag","2016款 2.0 自动 XL精英版,richanmah","2016款 1.2T 自动 XE时尚版,richanmai","2016款 1.2T 手动 XE时尚版,richanmaj"
					],
			
			"阳光":["2005款 2.0 自动 冠军车纪念版,richanna","2005款 2.0 自动 纪念版,richannb","2005款 2.0 自动 LS导航版,richannc",
					"2005款 2.0 手动 E,richannd","2005款 2.0 自动 LS,richanne","2005款 2.0 自动 LE,richannf","2005款 2.0 自动 E,richanng",
					"2011款 1.5 手动 XV尊贵版,richannh","2011款 1.5 手动 XL豪华版,richanni","2011款 1.5 手动 XE舒适版,richannj",
					"2011款 1.5 自动 XV尊贵版,richannk","2011款 1.5 自动 XL豪华版,richannl","2011款 1.5 自动 XE舒适版,richannm",
					"2012款 1.5 自动 XV尊贵版,richannn","2012款 1.5 手动 XL豪华版,richanno","2012款 1.5 手动 XE舒适版,richannp",
					"2014款 1.5 自动 XV尊贵版,richannq","2014款 1.5 手动 XL豪华版,richannr","2014款 1.5 自动 XL豪华版,richanns",
					"2014款 1.5 手动 XE精英版,richannt","2014款 1.5 自动 XE精英版,richannu","2014款 1.5 自动 XE舒适版,richannv",
					"2014款 1.5 手动 XE舒适版,richannw","2015款 1.5 自动 XE大师版,richannx","2015款 1.5 手动 XE大师版,richanny"
					],
			
			"颐达":["2005款 1.6 自动 JS,richanoa","2005款 1.6 手动 J,richanob","2005款 1.6 自动 JE,richanoc","2005款 1.6 自动 J,richanod",
					"2006款 1.6 手动 J时尚型,richanoe","2006款 1.6 自动 J时尚型,richanof","2006款 1.6 自动 JS导航版,richanog","2006款 1.6 自动 JS,richanoh",
					"2006款 1.6 手动 J,richanoi","2006款 1.6 手动 JE天窗版,richanoj","2006款 1.6 自动 JE,richanok","2006款 1.6 自动 J,richanol",
					"2007款 1.6 自动 JS尊贵型NAVI,richanom","2007款 1.6 自动 JS豪华型,richanon","2007款 1.6 手动 J时尚型,richanoo","2007款 1.6 手动 JE智能型,richanop",
					"2007款 1.6 自动 JE智能型,richanoq","2007款 1.6 自动 J时尚型,richanor",
					"2008款 1.6 自动 JS尊贵型NAVI,richanos","2008款 1.6 自动 JS豪华型,richanot","2008款 1.6 手动 J时尚型,richanou","2008款 1.6 手动 JE智能型,richanov",
					"2008款 1.6 自动 JE智能型,richanow","2008款 1.6 自动 J时尚型,richanox",
					"2009款 1.6 自动 JS尊贵型NAVI,richanoy","2009款 1.6 自动 JS豪华型,richanoz","2009款 1.6 手动 J时尚型,richanoaa","2009款 1.6 手动 JE智能型,richanoab",
					"2009款 1.6 自动 JE智能型,richanoac","2009款 1.6 自动 J时尚型,richanoad","2009款 1.6 自动 JE科技型,richanoae",
					"2010款 1.6 手动 J时尚型,richanoaf","2010款 1.6 手动 JE智能型,richanoag"
					],
			
			"骊威":["2007款 1.6 自动 GX劲锐版标准型,richanpa","2007款 1.6 自动 GX劲锐版智能型,richanpb","2007款 1.6 手动 GX劲锐版标准型,richanpc",
					"2007款 1.6 手动 GX劲锐版智能型,richanpd","2007款 1.6 自动 GS超能型,richanpe","2007款 1.6 手动 GS超能型,richanpf","2007款 1.6 自动 GE全能型,richanpg",
					"2007款 1.6 手动 GE全能型,richanph","2007款 1.6 自动 G多能型,richanpi","2007款 1.6 手动 G多能型,richanpj",
					"2008款 1.6 手动 GT炫能型,richanpk","2008款 1.6 自动 GX劲锐版智能型,richanpl","2008款 1.6 手动 GX劲锐版智能型,richanpm",
					"2008款 1.6 自动 GT炫能型,richanpn","2008款 1.6 手动 GC多能加强型,richanpo","2008款 1.6 自动 GC多能加强型,richanpp",
					"2008款 1.6 手动 GX劲锐版标准型,richanpq","2008款 1.6 自动 GX劲锐版标准型,richanpr",
					"2009款 1.6 手动 GX劲锐版智能型,richanps","2009款 1.6 手动 GX劲锐版标准型,richanpt","2009款 1.6 自动 GX劲锐版智能型,richanpu",
					"2009款 1.6 自动 GX劲锐版标准型,richanpv","2009款 1.6 手动 GT炫能型,richanpw","2009款 1.6 自动 GT炫能型,richanpx",
					"2010款 1.8 手动 XE劲逸版标准型,richanpy","2010款 1.8 自动 XE劲逸版标准型,richanpz","2010款 1.6 手动 GX劲锐版标准型,richanpaa",
					"2010款 1.6 自动 GX劲锐版标准型,richanpab","2010款 1.6 手动 GV劲锐版智能型,richanpac","2010款 1.6 自动 GV劲锐版智能型,richanpad",
					"2010款 1.6 自动 GV劲锐版顶配型,richanpae","2010款 1.6 手动 GS劲悦版超能型,richanpaf","2010款 1.6 自动 GS劲悦版炫能型,richanpag",
					"2010款 1.6 自动 GS劲悦版超能型,richanpah","2010款 1.6 手动 GI劲悦版全能型,richanpai","2010款 1.6 自动 GI劲悦版全能型,richanpaj",
					"2013款 1.6 自动 XV劲锐版豪华型,richanpak","2013款 1.6 手动 XV劲锐版豪华型,richanpal","2013款 1.6 自动 XL劲锐版舒适型,richanpam",
					"2013款 1.6 手动 XL劲锐版舒适型,richanpan","2013款 1.6 自动 XL豪华版,richanpao","2013款 1.6 手动 XL豪华版,richanpap","2013款 1.6 自动 XE舒适版,richanpaq",
					"2013款 1.6 手动 XE舒适版,richanpar","2015款 1.6 自动 XV劲锐酷咖版,richanpas","2015款 1.6 自动 XL豪华真皮版,richanpat"
					],
			
			"骏逸":["2006款 1.8 手动 XE标准型,richanqa","2006款 1.8 自动 XV尊贵型,richanqb","2006款 1.8 自动 XL舒适型,richanqc",
					"2006款 1.8 自动 XL时尚型,richanqd","2006款 1.8 自动 XE标准型,richanqe","2007款 1.8 自动 XV尊贵型,richanqf",
					"2007款 1.8 自动 XL舒适型,richanqg","2007款 1.8 自动 XL时尚型,richanqh",
					"2007款 1.8 自动 XE标准型,richanqi","2007款 1.8 手动 XE标准型,richanqj","2008款 1.8 手动 XL舒适型,richanqk"
					],
								
			"骐达":["2005款 1.6 自动 GS,richanra","2005款 1.6 手动 G,richanrb","2005款 1.6 自动 GE,richanrc","2005款 1.6 自动 G,richanrd",
					"2006款 1.6 手动 G,richanre","2006款 1.6 手动 GE天窗版,richanrf","2006款 1.6 自动 GE,richanrg","2006款 1.6 自动 G,richanrh",
					"2006款 1.6 自动 GS导航版,richanri","2006款 1.6 自动 GS,richanrj",
					"2008款 1.6 自动 豪华型,richanrk","2008款 1.6 自动 GS科技型,richanrl","2008款 1.6 自动 GE智能型,richanrm","2008款 1.6 手动 GE智能型,richanrn",
					"2008款 1.6 自动 G时尚型,richanro","2008款 1.6 手动 G时尚型,richanrp","2008款 1.6 自动 GS尊贵型NAVI,richanrq",
					"2009款 1.6 自动 尊贵NISMO运动版,richanrr","2009款 1.6 自动 豪华NISMO运动版,richanrs","2009款 1.6 自动 智能NISMO运动版,richanrt",
					"2009款 1.6 手动 智能NISMO运动版,richanru","2009款 1.6 自动 时尚NISMO运动版,richanrv","2009款 1.6 手动 时尚NISMO运动版,richanrw",
					"2010款 1.6 手动 G时尚型,richanrx","2010款 1.6 手动 GE智能型,richanry",
					"2011款 1.6 手动 智能型,richanrz","2011款 1.6 手动 舒适型,richanraa","2011款 1.6 自动 智能型,richanrab","2011款 1.6 自动 舒适型,richanrac",
					"2011款 1.6 自动 豪华型,richanrad","2011款 1.6T 手动 致酷型,richanrae","2011款 1.6T 手动 炫动型,richanraf","2011款 1.6T 自动 致酷型,richanrag",
					"2011款 1.6T 自动 炫动型,richanrah","2012款 1.6T 自动 极速限量版,richanrai","2013款 1.6 自动 酷咖型,richanraj",
					"2014款 1.6 手动 智能型,richanrak","2014款 1.6 手动 舒适型,richanral","2014款 1.6 自动 智能型,richanram","2014款 1.6 自动 舒适型,richanran",
					"2014款 1.6 自动 豪华型,richanrao","2014款 1.6 自动 酷咖型,richanrap"
					],
			
			"NV200":["2010款 1.6 手动 尊贵型,richansa","2010款 1.6 手动 舒适型,richansb","2010款 1.6 手动 基本型,richansc","2010款 1.6 手动 豪华型,richansd",
					"2010款 1.6 手动 标准型,richanse","2011款 1.6 手动 尊雅型,richansf","2013款 1.6 手动 尊贵型,richansg","2013款 1.6 手动 尊雅型,richansh",
					"2013款 1.6 手动 豪华型,richansi",
					"2015款 1.6 自动 尊雅型,richansj","2015款 1.6 自动 尊享型,richansk","2015款 1.6 自动 尊贵型,richansl","2015款 1.6 自动 豪华型,richansm",
					"2014款 1.6 自动 尊雅型,richansn","2014款 1.6 自动 尊贵型,richanso","2014款 1.6 自动 尊享型,richansp","2014款 1.6 自动 豪华型,richansq",
					"2014款 1.6 手动 尊享型,richansr","2014款 1.6 手动 尊雅型,richanss","2014款 1.6 手动 尊贵型,richanst","2014款 1.6 手动 豪华型,richansu"
					],
			
			"俊风":["2014款 1.3 手动 舒适型7座,richanta","2014款 1.3 手动 标准型7座,richantb"],
			
			"多功能商用车":["2006款 2.4 手动 四驱,richanua","2006款 2.4 手动 豪华型后驱,richanub","2006款 2.4 手动 标准型四驱,richanuc",
							"2006款 2.4 手动 标准型后驱,richanud","2007款 2.4 手动 四驱,richanue"
							],
			
			
			"奥丁":["2007款 2.5T 手动 豪华型四驱 柴油,richanva","2007款 2.5T 手动 豪华型后驱 柴油,richanvb","2007款 2.4 手动 行政版后驱,richanvc",
					"2007款 2.4 手动 豪华型后驱,richanvd","2007款 2.4 自动 豪华型后驱,richanve","2007款 3.0T 手动 豪华型四驱 柴油,richanvf",
					"2007款 3.0T 手动 豪华型后驱 柴油,richanvg","2007款 3.2T 手动 豪华型后驱 柴油,richanvh","2007款 3.2T 手动 豪华型四驱 柴油,richanvi",
					"2009款 2.4 手动 行政版后驱,richanvj","2009款 2.4 手动 豪华型后驱 ,richanvk","2009款 3.2T 手动 四驱 柴油,richanvl",
					"2013款 2.5T 手动 豪华版四驱 柴油,richanvm","2013款 2.5T 手动 豪华版后驱 柴油,richanvn","2013款 2.4 手动 行政版后驱,richanvo",
					"2013款 2.4 手动 豪华版后驱,richanvp"
					],
			
			"帅客":["2010款 1.6 手动 舒适型7座,richanwa","2010款 1.6 手动 实用型7座,richanwb","2010款 1.6 手动 豪华型7座,richanwc","2010款 1.6 手动 舒适型5-7座,richanwd",
					"2010款 1.6 手动 实用型5-7座,richanwe","2010款 1.6 手动 豪华型5-7座,richanwf","2011款 1.5 手动 舒适型7座,richanwg","2011款 1.5 手动 标准型7座,richanwh",
					"2011款 1.5 手动 标准型5座,richanwi",
					"2012款 1.6 手动 豪华型7座,richanwj","2012款 1.6 手动 实用型7座,richanwk","2012款 1.6 手动 舒适型7座,richanwl","2012款 2.0 自动 旗舰型7座,richanwm",
					"2012款 2.0 自动 豪华型7座,richanwn","2012款 2.0 手动 舒适型7座,richanwo","2012款 2.0 手动 豪华型7座,richanwp","2012款 1.5 手动 舒适型7座,richanwq",
					"2012款 1.5 手动 标准型7座,richanwr","2013款 1.5 手动 标准型7座,richanws","2013款 1.6 手动 商用型5座,richanwt","2013款 1.5 手动 标准型5座,richanwu",
					"2014款 1.6 手动 舒适型7座,richanwv","2014款 1.6 手动 豪华型7座,richanww","2014款 1.5 手动 豪华型7座,richanwx","2014款 1.5 手动 舒适型7座,richanwy",
					"2014款 1.5 手动 标准型7座,richanwz","2014款 1.5 手动 标准型5座,richanwaa",
					"2016款 1.6 手动 舒适型7座,richanwab","2016款 1.6 手动 豪华型7座,richanwac","2016款 1.5 手动 豪华型7座,richanwad","2016款 1.5 手动 舒适型7座,richanwae",
					"2016款 1.5 手动 标准型7座,richanwaf"
					],

			
			"帕拉丁":["2006款 2.4 手动 XE超豪华型四驱,richanxa","2006款 2.4 手动 XE超豪华型后驱,richanxb","2006款 2.4 手动 XE豪华型四驱,richanxc",
					"2006款 3.3 自动 SE超豪华型四驱,richanxd",
					"2006款 3.3 自动 SE标准型四驱,richanxe","2006款 2.4 手动 XE豪华型后驱,richanxf","2006款 2.4 手动 XE标准型四驱,richanxg",
					"2006款 2.4 手动 XE标准型后驱,richanxh",
					"2007款 2.4 手动 XE超豪华型四驱,richanxi","2007款 2.4 手动 XE豪华型后驱,richanxj","2007款 2.4 手动 XE超豪华型后驱,richanxk",
					"2007款 2.4 手动 XE标准型后驱,richanxl",
					"2007款 2.4 手动 XE豪华型四驱,richanxm","2007款 2.4 手动 XE标准型四驱,richanxn","2008款 2.4 手动 XE标准型四驱,richanxo",
					"2008款 2.4 手动 XE标准型后驱,richanxp",
					"2013款 2.4 手动 XE行政型四驱,richanxq","2013款 2.4 手动 XE舒适型四驱,richanxr","2013款 2.4 手动 XE豪华型四驱,richanxs",
					"2013款 2.4 手动 XE标准型四驱,richanxt",
					"2013款 2.4 手动 XE行政型后驱,richanxu","2013款 2.4 手动 XE舒适型后驱,richanxv","2013款 2.4 手动 XE豪华型后驱,richanxw",
					"2013款 2.4 手动 XE标准型后驱,richanxx",
					"2013款 2.4 手动 XE豪华型纪念版四驱,richanxy","2013款 2.4 手动 XE标准型纪念版四驱,richanxz","2013款 2.4 手动 XE标准型纪念版后驱,richanxaa"
					],
			
			"帕拉骐":["2009款 2.5T 手动 标准型双排四驱 柴油,richanya","2009款 2.5T 手动 标准型单排四驱 柴油,richanyb","2009款 2.4 手动 高级型双排四驱,richanyc",
					"2009款 2.4 手动 标准型双排四驱,richanyd"
					],
			
			"御轩":["2008款 2.5 自动 旗舰型,richanza","2008款 2.5 自动 豪华型,richanzb","2008款 2.0 手动 豪华型,richanzc","2008款 2.0 手动 标准型,richanzd",
					"2009款 2.5 自动 旗舰型,richanze","2009款 2.5 自动 豪华型,richanzf","2009款 2.0 手动 行政型,richanzg","2009款 2.0 手动 豪华型,richanzh",
					"2009款 2.0 手动 标准型,richanzi"
					],
			
			"风度MX6":["2015款 2.0 自动 全能版四驱,richanaaa","2015款 2.0 自动 全能版两驱,richanaab","2015款 2.0 自动 梦想版四驱,richanaac",
					"2015款 2.0 自动 卓越版四驱,richanaad","2015款 2.0 自动 梦想版前驱,richanaae","2015款 2.0 自动 卓越版前驱,richanaaf",
					"2015款 2.0 手动 领航版四驱,richanaag","2015款 2.0 手动 领航版前驱,richanaah"
					],
			
			"350Z":["2005款 3.5 手动 (300HP),richanaba","2005款 3.5 手动 敞篷(306HP),richanabb","2008款 3.5 手动,richanabc"],
			
			"370Z":["2009款 3.7 手动,richanaca","2009款 3.7 自动,richanacb","2013款 3.7 自动,richanacc"],
			
			"GT-R":["2007款 3.8 自动 (485HP),richanada","2009款 3.8T 自动,richanadb","2010款 3.8T 自动 Premium Edition,richanadc",
					"2011款 3.8T 自动,richanadd","2012款 3.8T 自动 Premium Edition,richanade","2013款 3.8T 自动 Premium Edition,richanadf",
					"2014款 3.8T 自动 Premium Edition黑色内饰,richanadg","2014款 3.8T 自动 Premium Edition棕红内饰,richanadh",
					"2015款 3.8T 自动 Nismo,richanadi","2015款 3.8T 自动 黑金刚版,richanadj","2015款 3.8T 自动 特别版,richanadk",
					"2015款 3.8T 自动 时尚米白内饰版,richanadl","2015款 3.8T 自动 豪华棕红内饰版,richanadm","2015款 3.8T 自动 动感酷黑内饰版,richanadn"
					],
			
			"Juke":["2010款 1.6T 手动,richanaea","2010款 1.6 手动,richanaeb"],
			
			"Murano":["2011款 敞篷版 3.5 自动 敞篷四驱,richanafa","2008款 3.5 自动,richanafb"],
			
			"奇骏(进口)":["2008款 2.5 自动 SLE豪华俊朗型,richanaha","2008款 2.5 自动 SLE豪华型,richanahb","2008款 2.5 自动 LE舒适型,richanahc"],
			
			"奥蒂玛":["2007款 2.5 自动,richanaia","2007款 3.5 自动,richanaib"],
			
			"西玛":["2005款 3.0 自动,richanaka","2005款 4.5 自动,richanakb","2008款 3.0 自动,richanakc","2008款 4.5 自动,richanakd"],
			
			"贵士":["2005款 3.5 自动,richanala","2006款 3.5 自动,richanalb","2009款 3.5 自动,richanalc","2012款 3.5 自动 SL,richanald","2015款 3.5 自动 SL,richanale"],
			
			"途乐":["2012款 5.6 自动 旗舰版,richanama","2014款 5.6 自动 LE,richanamb","2016款 5.6 自动 LE,richanamc"],
			
			
			"风雅":["2009款 2.5 自动 250,richanapa","2009款 3.5 自动 350,richanapb","2009款 3.7 自动 370,richanapc"],
			
			"马克西马":["2009款 3.5 自动 S,richanaqa"],
			
			"骐达(进口)":["2006款 三厢 1.5 自动,richanara","2006款 三厢 1.6 手动,richanarb","2006款 三厢 1.8 手动,richanarc"],
	
//"荣威"
			
			"荣威350":["2010款 1.5 自动 讯达版,rongweiaa","2010款 1.5 自动 讯逸版,rongweiab","2010款 1.5 自动 讯豪版,rongweiac","2010款 1.5 手动 讯智版,rongweiad",
				"2010款 1.5 手动 讯驰版,rongweiae","2011款 1.5 手动 讯智版,rongweiaf","2011款 1.5 手动 讯捷版,rongweiag","2011款 1.5 手动 讯驰版,rongweiah",
				"2011款 1.5 自动 讯悦版,rongweiai","2011款 1.5 自动 讯逸版,rongweiaj","2011款 1.5 自动 讯豪版,rongweiak","2011款 1.5 自动 讯达版,rongweial",
				"2013款 1.5 自动 350S讯达版,rongweiam","2013款 1.5 手动  350C讯捷版,rongweian","2013款 1.5 自动 350C讯悦版,rongweiao",
				"2013款 1.5 自动 350D讯豪版,rongweiap","2013款 1.5 手动  350S讯驰版,rongweiaq","2014款 1.5T 自动 劲尚版,rongweiar",
				"2014款 1.5T 自动 劲逸版,rongweias","2014款 1.5T 手动 劲锐版,rongweiat","2014款 1.5 自动 讯豪版,rongweiau",
				"2014款 1.5 自动 讯悦版,rongweiav","2014款 1.5 手动 讯捷版,rongweiaw","2014款 1.5 自动 讯达版,rongweiax","2014款 1.5 手动 讯驰版,rongweiay",
				"2015款 1.5 手动 尊享版,rongweiaz","2015款 1.5 手动 豪华天窗版,rongweiaaa","2015款 1.5 自动 尊享版,rongweiaab","2015款 1.5 自动 豪华天窗版,rongweiaac"
				],
			
			
			"荣威550":["2008款 1.8T 手动 S品锐版,rongweiba","2008款 1.8T 自动 S品智版,rongweibb","2008款 1.8T 自动 G品仕版,rongweibc","2008款 1.8T 自动 D品臻版,rongweibd",
				"2008款 1.8T 手动 D品逸版,rongweibe","2008款 1.8 手动 启逸版,rongweibf","2008款 1.8 手动 启悦版,rongweibg","2009款 1.8 自动 启臻版,rongweibh",
				"2009款 1.8 自动 启智版,rongweibi","2010款 1.8 自动 贺岁版,rongweibj","2010款 1.8T 自动 G品仕版,rongweibk","2010款 1.8T 自动 D品臻版,rongweibl",
				"2010款 1.8T 手动 D品逸版,rongweibm","2010款 1.8 自动 启臻版,rongweibn","2010款 1.8 自动 启智版,rongweibo","2010款 1.8 手动 启逸版,rongweibp",
				"2010款 1.8 手动 启悦版,rongweibq",
				"2012款 1.8 手动 S启逸版,rongweibr","2012款 1.8 手动 S超值版,rongweibs","2012款 1.8 自动 S启臻版,rongweibt","2012款 1.8 自动 S超值版,rongweibu",
				"2012款 1.8T 自动 G品仕版,rongweibv","2012款 1.8T 手动 D品逸版,rongweibw","2012款 1.8T 自动 D品臻版,rongweibx","2012款 1.8 手动 启悦版,rongweiby",
				"2012款 1.8 自动 启智版,rongweibz","2012款 1.8T 自动 G世博限量版,rongweibaa","2012款 1.8 手动 世博限量版,rongweibab","2010款 1.8 手动 贺岁版,rongweibac",
				"2013款 1.8T 自动 G品仕版,rongweibad","2013款 1.8T 自动 D品臻版,rongweibae","2013款 1.8 自动 经典豪华版,rongweibaf","2013款 1.8 自动 经典风尚版,rongweibag",
				"2013款 1.8 手动 经典豪华版,rongweibah","2013款 1.8 手动 经典风尚版,rongweibai","2013款 1.8T 手动 D品逸版,rongweibaj","2013款 1.8 自动 S启臻版,rongweibak",
				"2013款 1.8 自动 启智版,rongweibal","2013款 1.8 手动 S启逸版,rongweibam","2013款 1.8 手动 启悦版,rongweiban",
				"2014款 1.5 自动 旗舰版 油电混合,rongweibao","2014款 1.5 自动 豪华版 油电混合,rongweibap","2014款 1.8 自动 S智选版,rongweibaq",
				"2014款 1.8 手动 S智选版,rongweibar","2015款 1.5 自动 旗舰版 油电混合,rongweibas","2015款 1.5 自动 豪华版 油电混合,rongweibat"
				],
			
			
			"荣威750":["2006款 2.5 自动 D典雅版,rongweica","2006款 2.5 自动 E,rongweicb","2007款 2.5 自动 D典雅版,rongweicc","2007款 2.5 自动 FL豪雅版,rongweicd",
				"2007款 2.5 自动 E贵雅版,rongweice",
				"2008款 1.8T 自动 i铂雅版,rongweicf","2008款 1.8T 自动 超值限量版,rongweicg","2008款 2.5 自动 FL典雅版,rongweich","2008款 2.5 自动 E贵雅版,rongweici",
				"2008款 2.5 自动 i睿雅版,rongweicj","2008款 2.5 自动 D周年版,rongweick","2008款 2.5 自动 i斯诺克大师版,rongweicl","2008款 1.8T 自动 EX祺雅版,rongweicm",
				"2008款 1.8T 自动 D商雅版,rongweicn","2008款 1.8T 自动 S迅雅版,rongweico",                  
				"2009款 1.8T 自动 EX祺雅版NAVI ,rongweicp","2009款 1.8T 自动 D商雅版NAVI ,rongweicq","2009款 1.8T 手动 D商雅版NAVI ,rongweicr","2009款 1.8T 自动 S迅雅版,rongweics",
				"2009款 2.5 自动 FL豪雅版NAVI ,rongweict","2009款 2.5 自动 E贵雅版NAVI ,rongweicu","2009款 1.8T 手动 S迅雅版,rongweicv","2010款 2.5 自动 i斯诺克大师版,rongweicw",
				"2011款 1.8T 自动 EX祺雅版NAVI ,rongweicx","2011款 2.5 自动 E贵雅版NAVI ,rongweicy","2011款 1.8T 手动 D商雅版 NAVI ,rongweicz","2011款 1.8T 自动 D商雅版NAVI ,rongweicaa",
				"2012款 1.8T 自动 油电混合,rongweicab","2011款 1.8T 手动 S迅雅版,rongweicac","2011款 1.8T 自动 S迅雅版,rongweicad","2011款 2.5 自动 FL豪雅版NAVI ,rongweicae"
				],	
			
			"荣威950":["2012款 2.0 自动 舒适版,rongweida","2012款 2.0 自动 典雅版,rongweidb","2012款 3.0 自动 旗舰版,rongweidc","2012款 2.4 自动 豪华行政版,rongweidd",
					"2012款 2.4 自动 豪华版,rongweide","2015款 1.8T 自动 精英版,rongweidf","2015款 2.0T 自动 旗舰版,rongweidg","2015款 2.0T 自动 豪华行政版,rongweidh",
					"2015款 1.8T 自动 豪华版,rongweidi","2015款 1.8T 自动 典雅版,rongweidj"
					],
			
			"荣威E50":["2013款 基本型 纯电动,rongweiea"],
			
			"荣威W5":["2011款 3.2 自动 尊域版四驱,rongweifa","2011款 3.2 自动 锐域版四驱,rongweifb","2011款 1.8T 手动 驰域版后驱,rongweifc",
				"2011款 1.8T 自动 胜域版后驱,rongweifd","2011款 1.8T 自动 豪域版四驱,rongweife","2012款 3.2 自动 尊域版四驱,rongweiff",
				"2012款 1.8T 自动 行政版四驱,rongweifg","2012款 1.8T 自动 豪域版四驱,rongweifh","2013款 1.8T 自动 胜域版后驱,rongweifi",
				"2013款 1.8T 手动 驰域版后驱,rongweifj","2013款 1.8T 自动 豪域版四驱,rongweifk","2013款 3.2 自动 尊域版四驱,rongweifl",
				"2013款 3.2 自动 锐域版四驱,rongweifm","2013款 1.8T 自动 行政版四驱,rongweifn","2013款 1.8T 手动 驰域特装版后驱,rongweifo",
				"2013款 1.8T 自动 豪域特装版四驱,rongweifp","2013款 1.8T 自动 胜域特装版后驱,rongweifq","2014款 1.8T 手动 驰域特装版前驱,rongweifr",
				"2014款 1.8T 自动 豪域特装版四驱,rongweifs","2014款 1.8T 自动 胜域特装版后驱,rongweift"
				],
				
//"瑞麒"	
			
			
			"瑞麒G3":["2012款 1.6 手动 实力型,ruilinaa","2012款 1.6 手动 舒适型,ruilinab","2012款 1.6 手动 豪华型,ruilinac","2012款 1.6 手动 旗舰型,ruilinad",
				  "2012款 1.6 自动 舒适型,ruilinae","2012款 1.6 自动 豪华型,ruilinaf","2012款 1.6 自动 旗舰型,ruilinag"
					],
			
			"瑞麒G5":["2010款 2.0T 手动 梅西版舒适型,ruilinba","2010款 2.0T 手动 梅西版尊贵型,ruilinbc","2010款 2.0T 手动 梅西版豪华型,ruilinbd",
				"2010款 2.0T 自动 旗舰型,ruilinbe","2010款 2.0T 手动 尊贵型,ruilinbf","2010款 2.0T 手动 豪华型,ruilinbg","2011款 2.0T 自动 旗舰型,ruilinbh",
				"2012款 2.0T 自动 尊享型,ruilinbi","2012款 2.0T 自动 豪华型,ruilinbj","2012款 2.0 手动 尊享型,ruilinbk","2012款 2.0 手动 舒适型,ruilinbl",
				"2012款 2.0 手动 豪华型,ruilinbm","2012款 2.0 自动 尊享型,ruilinbn","2012款 2.0 自动 豪华型,ruilinbo","2011款 2.0T 自动 豪华型,ruilinbp"
				],
			
			
			"瑞麒G6":["2011款 2.0T 自动 豪华型,ruilinca","2011款 2.0T 自动 旗舰型,ruilincb","2011款 2.0T 自动 舒适型,ruilincc",
			      "2011款 1.8T 自动 政府特供,ruilincd"
					],
			
			"瑞麒M1":["2009款 1.3 手动 精英型,ruilinda","2009款 1.3 手动 尊贵型,ruilindb","2009款 1.3 手动 豪华型,ruilindc","2009款 1.3 手动 舒适型,ruilindd",
				"2009款 1.3 手动 标准型,ruilinde","2010款 1.0 手动 豪华型,ruilindf","2010款 1.3 自动 舒适型,ruilindg","2010款 1.3 自动 尊贵型,ruilindh",
				"2010款 1.3 自动 豪华型,ruilindi","2010款 1.0 手动 尊贵型,ruilindj","2010款 1.0 手动 舒适型,ruilindk","2010款 1.0 手动 基本型,ruilindl",
				"2011款 1.3 手动 豪华型,ruilindm","2011款 1.3 自动 豪华型,ruilindn","2011款 1.0 手动 舒适型,ruilindo","2011款 1.0 手动 实力型,ruilindp",
				"2011款 1.0 手动 基本型,ruilindq","2011款 1.0 手动 豪华型,ruilindr","2011款 1.0 手动 EV5 纯电动,ruilinds","2011款 1.0 手动 EV4 纯电动,ruilindt",
				"2011款 1.0 手动 EV3 纯电动,ruilindu","2011款 1.0 手动 EV2 纯电动,ruilindv","2011款 1.0 手动 EV1 纯电动,ruilindw",
				"2011款 1.0 手动 智尊型,ruilindx","2011款 1.0 手动 智尚型,ruilindy","2011款 1.0 手动 智惠型,ruilindz", "2013款 智能版 纯电动,ruilindaa"
				],
			
			"瑞麒M5":["2010款 1.3 自动 豪华型,ruilinea","2010款 1.3 手动 豪华型,ruilineb","2010款 1.3 手动 基本型,ruilinec"
					],
			
			"瑞麒X1":["2010款 1.3 手动 豪华型,ruilinfa","2010款 1.3 手动 舒适型,ruilinfb","2010款 1.3 手动 尊贵型,ruilinfc","2010款 1.3 手动 进化版舒适型,ruilinfd",
				"2010款 1.3 手动 精英型,ruilinfe","2010款 1.3 手动 进化版豪华型,ruilinff","2010款 1.3 自动 豪华型,ruilinfg",
				"2011款 1.3 手动 豪华型,ruilinfh","2011款 1.3 手动 舒适型,ruilinfi","2011款 1.3 自动 豪华型,ruilinfj","2011款 1.3 手动 舒享型,ruilinfk",
				"2012款 1.5 手动 运动型,ruilinfl","2012款 1.5 手动 豪华型,ruilinfm","2012款 1.5 手动 舒享型,ruilinfn","2012款 1.5 手动 舒适型,ruilinfo",
				"2012款 1.3 自动 豪华型,ruilinfp"
				],
			
			
			"派拉蒙":["2010款 2.0T 自动 G6加长礼宾车,ruilinga"],
			
			
//"陕汽通家"		

			"福家":["2010款 1.3 手动 标准型5座6400A,sqtjiaaa","2014款 1.0 手动 标准型5座6400B,sqtjiaab",
					"2014款 1.3 手动 标准型5座6400A,sqtjiaac","2014款 1.3 手动 舒适型5座6400A,sqtjiaad",
					"2014款 1.3 手动 豪华型5座6400A,sqtjiaae"
					],
			
			"通家和瑞":["2010款 1.0 手动,sqtjiaba"],
			
//"双环"			
			
			"双环SCEO":["2005款 2.8T 手动 超豪华型后驱 柴油,shuanghuangaa","2005款 2.8T 手动 豪华型后驱 柴油,shuanghuangab","2005款 2.4 自动 超豪华型四驱4G64S4M,shuanghuangac",
					"2005款 2.4 自动 豪华型四驱4G64S4M,shuanghuangad","2005款 2.4 自动 超豪华型后驱4G64S4M,shuanghuangae","2005款 2.4 自动 豪华型后驱4G64S4M,shuanghuangaf",
					"2005款 2.4 手动 豪华型四驱4G64S4M,shuanghuangag","2005款 2.4 手动 豪华型四驱4G63S4M,shuanghuangah","2005款 2.4 手动 超豪华型四驱4G69S4N,shuanghuangai",
					"2005款 2.4 手动 超豪华型四驱4G64S4M,shuanghuangaj","2005款 2.4 手动 超豪华型四驱4G63S4M,shuanghuangak","2005款 2.4 手动 豪华型后驱4G64S4M,shuanghuangal",
					"2005款 2.4 手动 豪华型后驱4G63S4M,shuanghuangam","2005款 2.4 手动 超豪华型后驱4G69,shuanghuangan","2005款 2.4 手动 超豪华型后驱4G64S4M,shuanghuangao",
					"2005款 2.4 手动 超豪华型后驱4G63S4M,shuanghuangap","2005款 2.4 手动 顶级型四驱,shuanghuangaq","2005款 2.4 手动 超豪华型四驱,shuanghuangar",
					"2005款 2.4 手动 超豪华型后驱,shuanghuangas","2005款 2.4 手动 豪华型后驱,shuanghuangat","2005款 2.4 手动 豪华型四驱,shuanghuangau",
					"2006款 2.4 手动 超豪华型四驱4G64S4M,shuanghuangav","2006款 2.4 手动 豪华型四驱4G64S4M,shuanghuangaw","2006款 2.4 手动 豪华型后驱4G64S4M,shuanghuangax",
					"2006款 2.4 手动 超豪华型后驱4G64S4M,shuanghuangay","2007款 2.0 手动 超豪华型后驱4G63,shuanghuangaz","2007款 2.0 手动 豪华型后驱4G63,shuanghuangaaa",
					"2008款 2.4 手动 豪华型四驱4G69,shuanghuangaab","2008款 2.4 手动 豪华型后驱4G69,shuanghuangaac","2008款 2.4 手动 超豪华型四驱4G69,shuanghuangaad",
					"2008款 2.4 手动 超豪华型后驱4G69,shuanghuangaae","2008款 2.4 手动 豪华型四驱4G69S4N,shuanghuangaaf","2008款 2.4 手动 超豪华型四驱4G69S4N,shuanghuangaag",
					"2008款 2.4 手动 豪华型后驱4G69S4N,shuanghuangaah","2008款 2.4 手动 超豪华型后驱4G69S4N,shuanghuangaai","2008款 2.5T 手动 超豪华型四驱 柴油,shuanghuangaaj",
					"2008款 2.5T 手动 豪华型四驱 柴油,shuanghuangaak","2008款 2.5T 手动 超豪华型后驱 柴油,shuanghuangaal","2008款 2.5T 手动 豪华型后驱 柴油,shuanghuangaam",
					"2009款 2.0 手动 豪华型后驱,shuanghuangaan","2009款 2.4 自动 豪华型后驱4G69,shuanghuangaao","2009款 2.4 自动 超豪华型后驱4G69,shuanghuangaap",
					"2009款 2.4 自动 超豪华型后驱4G69S4N,shuanghuangaaq","2009款 2.4 自动 豪华型后驱4G69S4N,shuanghuangaar",                 
					"2011款 2.5T 手动 标准型四驱 柴油,shuanghuangaas","2011款 2.5T 手动 豪华型四驱 柴油,shuanghuangaat","2011款 2.5T 手动 超豪华型四驱 柴油,shuanghuangaau",
					"2011款 2.5T 手动 标准型后驱 柴油,shuanghuangaav","2011款 2.5T 手动 豪华型后驱 柴油,shuanghuangaaw","2011款 2.5T 手动 超豪华型后驱 柴油,shuanghuangaax",
					"2011款 2.4 手动 豪华型后驱,shuanghuangaay","2011款 2.4 手动 超豪华型后驱,shuanghuangaaz","2011款 2.4 手动 标准型后驱,shuanghuangaba","2011款 2.4 自动 豪华型后驱,shuanghuangabb",
					"2011款 2.4 自动 超豪华型后驱,shuanghuangabc","2011款 2.4 手动 豪华型四驱,shuanghuangabd","2011款 2.4 手动 超豪华型四驱,shuanghuangabe","2011款 2.4 手动 标准型四驱,shuanghuangabf",
					"2011款 2.4 自动 豪华型四驱,shuanghuangabg","2011款 2.4 自动 超豪华型四驱,shuanghuangabh","2011款 2.0 手动 豪华型后驱,shuanghuangabi","2011款 2.0 手动 超豪华型后驱,shuanghuangabj",
					"2011款 2.0 手动 标准型后驱4G63,shuanghuangabk","2011款 2.0 自动 豪华型后驱,shuanghuangabl","2011款 2.0 自动 超豪华型后驱,shuanghuangabm","2011款 2.0 手动 标准型后驱,shuanghuangabn"
					],
			
			
			"来宝S-RV":["2005款 2.4 手动 超豪华型四驱,shuanghuangba"],
			
			"来旺":["2002款 2.2 手动 豪华型,shuanghuangca"],
			
			"小贵族":["2007款 1.0 手动 华贵型,shuanghuangda","2007款 1.0 手动 金贵型,shuanghuangdb",
					  "2007款 1.1 手动 标准型,shuanghuangdc","2007款 1.1 手动 尊贵Ⅰ型,shuanghuangdd",
					  "2007款 1.1 手动 尊贵Ⅱ型,shuanghuangde"
					],
			
//"双龙"
	
			"主席":["2006款 3.2 自动 豪华型,shuanglongaa","2006款 3.2 自动 尊享型,shuanglongab","2006款 2.8 自动 尊享型,shuanglongac","2006款 2.8 自动 优雅型,shuanglongad",
					"2006款 2.8 自动 豪华型,shuanglongae","2007款 3.2 自动 加长型,shuanglongaf","2008款 3.2 自动 加长型,shuanglongag","2008款 2.8 自动 加长型,shuanglongah",
					"2008款 3.2 自动 豪华型,shuanglongai","2008款 3.2 自动 尊贵型,shuanglongaj","2008款 2.8 自动 尊贵型,shuanglongak","2008款 2.8 自动 典雅型,shuanglongal",
					"2008款 2.8 自动 豪华型,shuanglongam","2012款 2.8 自动 标准型,shuanglongan","2012款 3.6 自动 CW700L加长型,shuanglongao"
					],
			
			
			                  
			
			
			"享御":["2006款 2.0T 自动 豪华型四驱 柴油,shuanglongba","2006款 2.0T 自动 豪华型后驱 柴油,shuanglongbb","2006款 2.0T 自动 标准型后驱 柴油,shuanglongbc",
					"2007款 2.3 自动 豪华型后驱,shuanglongbd","2007款 2.0T 自动 超豪华型四驱 柴油,shuanglongbe","2007款 2.0T 自动 豪华型后驱 柴油,shuanglongbf",
					"2007款 2.0T 自动 标准型后驱 柴油,shuanglongbg","2010款 2.0T 自动 逸享世博导航版四驱 柴油,shuanglongbh","2010款 2.3 自动 乐享导航版后驱,shuanglongbi",
					"2010款 2.3 自动 畅享导航版四驱,shuanglongbj","2010款 2.0T 自动 乐享导航版后驱 柴油,shuanglongbk","2010款 2.0T 自动 畅享导航版四驱 柴油,shuanglongbl",
					"2011款 2.0T 自动 豪华导航版四驱 柴油,shuanglongbm","2011款 2.0T 自动 精英导航版四驱 柴油,shuanglongbn","2011款 2.0T 自动 豪华导航版后驱 柴油,shuanglongbo",
					"2011款 2.0T 自动 精英导航版后驱 柴油,shuanglongbp"
					],                  
			 
			 
			
			
			"柯兰多":["2011款 2.0T 手动 舒适导航版前驱 柴油,shuanglongca","2011款 2.0T 自动 精英导航版四驱 柴油,shuanglongcb","2011款 2.0T 自动 豪华导航版四驱 柴油,shuanglongcc",
					"2011款 2.0T 自动 精英导航版前驱 柴油,shuanglongcd","2011款 2.0T 自动 豪华导航版前驱 柴油,shuanglongce","2013款 2.0 自动 豪华导航版四驱,shuanglongcf",
					"2013款 2.0 自动 豪华导航版前驱,shuanglongcg","2013款 2.0 自动 精英版前驱,shuanglongch","2013款 2.0 手动 舒适版前驱,shuanglongci","2014款 2.0 手动 舒适导航版前驱,shuanglongcj",
					"2014款 2.0 自动 典藏版前驱,shuanglongck","2014款 2.0 自动 豪华导航版四驱,shuanglongcl","2014款 2.0 自动 豪华导航版前驱,shuanglongcm","2014款 2.0 自动 精英导航版前驱,shuanglongcn",
					"2015款 2.0 自动 致尊版四驱,shuanglongco","2015款 2.0 自动 致享版四驱,shuanglongcp","2015款 2.0 自动 致纯版四驱,shuanglongcq","2015款 2.0 自动 致尊版前驱,shuanglongcr",
					"2015款 2.0 自动 致享版前驱,shuanglongcs","2015款 2.0 自动 致纯版前驱,shuanglongct","2015款 2.0 手动 致纯版前驱,shuanglongcu","2015款 2.0 手动 致真版前驱,shuanglongcv"
					],
			                  
			
			
			
			"爱腾":["2006款 2.3 自动 超豪华型四驱,shuanglongda","2006款 2.0T 自动 超豪华型四驱 柴油,shuanglongdb","2006款 2.0T 手动 超豪华型四驱 柴油,shuanglongdc",
					"2006款 2.0T 自动 豪华型后驱 柴油,shuanglongdd","2006款 2.0T 自动 标准型后驱 柴油,shuanglongde","2006款 2.0T 手动 标准型后驱 柴油,shuanglongdf",
					"2007款 2.3 自动 豪华型后驱,shuanglongdg","2007款 2.3 自动 超豪华型四驱,shuanglongdh","2007款 2.0T 自动 超豪华型四驱 柴油,shuanglongdi","2007款 2.0T 手动 超豪华型四驱 柴油,shuanglongdj",
					"2007款 2.0T 自动 豪华型后驱 柴油,shuanglongdk","2007款 2.0T 自动 标准型后驱 柴油,shuanglongdl","2007款 2.0T 手动 标准型后驱 柴油,shuanglongdm",                  
					"2009款 2.3 自动 超豪华型四驱,shuanglongdn","2009款 2.0T 自动 超豪华型四驱 柴油,shuanglongdo","2009款 2.3 自动 豪华型后驱,shuanglongdp",
					"2009款 2.0T 自动 豪华型后驱 柴油,shuanglongdq","2009款 2.3 自动 标准型后驱,shuanglongdr","2009款 2.0T 手动 标准型后驱 柴油,shuanglongds",
					"2010款 2.3 自动 真爱版后驱,shuanglongdt","2010款 2.3 自动 深爱导航版后驱,shuanglongdu","2010款 2.0T 自动 深爱版后驱 柴油,shuanglongdv","2010款 2.3 自动 挚爱导航版四驱,shuanglongdw",
					"2010款 2.3 自动 真爱导航版后驱,shuanglongdx","2010款 2.0T 自动 挚爱导航版四驱 柴油,shuanglongdy","2010款 2.0T 自动 真爱导航版后驱 柴油,shuanglongdz",
					"2011款 2.0T 自动 四驱精英型柴油版,shuanglongdaa","2011款 2.0T 自动 四驱豪华型柴油版,shuanglongdab","2011款 2.3 自动 豪华版四驱,shuanglongdac","2011款 2.3 自动 精英版后驱,shuanglongdad",
					"2011款 2.3 手动 舒适版后驱,shuanglongdae","2011款 2.0T 自动 豪华版四驱 柴油,shuanglongdaf","2011款 2.0T 自动 豪华版后驱 柴油,shuanglongdag","2011款 2.0T 自动 精英版四驱 柴油,shuanglongdah",
					"2011款 2.0T 自动 精英版后驱 柴油,shuanglongdai","2014款 2.0T 自动 豪华版四驱 柴油,shuanglongdaj","2014款 2.0T 自动 精英版后驱 柴油,shuanglongdak",
					"2014款 2.0T 手动 进取版后驱 柴油,shuanglongdal","2014款 2.3 自动 豪华版四驱,shuanglongdam","2014款 2.3 自动 精英版后驱,shuanglongdan",
					"2014款 2.3 自动 舒适版后驱,shuanglongdao","2014款 2.3 手动 进取版后驱,shuanglongdap"
					],
			
			 
			 
			 
			
			"路帝":["2005款 3.2 自动 标准型7座,shuanglongea","2005款 3.2 自动 超豪华型7座,shuanglongeb","2005款 3.2 自动 豪华型7座,shuanglongec",
					"2007款 3.2 自动 舒适型7座,shuanglonged",
					"2008款 3.2 自动 豪华型7座后驱,shuanglongee","2008款 3.2 自动 超豪华型7座四驱,shuanglongef","2008款 2.7T 自动 豪华型7座后驱 柴油,shuanglongeg",
					"2008款 2.7T 自动 超豪华型7座后驱 柴油,shuanglongeh","2008款 2.7T 自动 标准型7座后驱 柴油,shuanglongei","2014款 2.0T 自动 豪华导航版四驱 柴油,shuanglongej",
					"2014款 2.0T 自动 豪华导航版后驱 柴油,shuanglongek","2014款 2.0T 自动 精英导航版后驱 柴油,shuanglongel"
					],
					                 
			                  
			
			"雷斯特":["2006款 3.2 自动 超豪华型7座,shuanglongfa","2006款 3.2 自动 超豪华导航型7座,shuanglongfb","2006款 3.2 自动 豪华型7座,shuanglongfc",
					"2006款 2.7T 自动 超豪华型5座柴油,shuanglongfd","2006款 2.7T 自动 豪华型5座柴油,shuanglongfe",
					"2010款 2.7T 自动 特尊旗般版四驱 柴油,shuanglongff","2010款 2.7T 自动 特睿导航版四驱 柴油,shuanglongfg",                  
					"2011款 2.0T 自动 豪华导航版四驱 柴油,shuanglongfh","2011款 2.0T 自动 精英导航版后驱 柴油,shuanglongfi",
					"2011款 2.7T 自动 豪华导航版四驱 柴油,shuanglongfj","2011款 3.2 自动 豪华导航版四驱,shuanglongfk",
					"2014款 2.7T 自动 W豪华导航版7座四驱 柴油,shuanglongfl","2014款 2.0T 自动 W豪华导航版7座四驱 柴油,shuanglongfm",
					"2014款 2.0T 自动 W豪华导航版7座后驱 柴油,shuanglongfn","2014款 2.0T 自动 W豪华导航版四驱 柴油,shuanglongfo",
					"2014款 2.0T 自动 W精英导航版后驱 柴油,shuanglongfp","2014款 2.7T 自动 W豪华导航版四驱 柴油,shuanglongfq"
					],

			
//"斯柯达"	
			
			"明锐":["2007款 2.0 手动 逸致版,skodaaa","2007款 2.0 自动 逸仕版,skodaab","2007款 2.0 手动 逸仕版,skodaac","2007款 1.8T 自动 逸尊版,skodaad",
					"2007款 1.8T 自动 逸仕版,skodaae",
					"2007款 1.6 自动 逸致版,skodaaf","2007款 1.6 自动 逸仕版,skodaag","2007款 1.6 手动 逸致版,skodaah","2007款 1.6 手动 逸仕版,skodaai",
					"2008款 2.0 手动 逸仕版,skodaaj",
					"2008款 2.0 自动 逸仕版,skodaak","2008款 1.8T 手动 夺金版,skodaal","2008款 2.0 手动 奥运限量版,skodaam","2008款 1.8T 自动 逸尊导航版,skodaan",
					"2009款 2.0 手动 逸俊版,skodaao","2009款 2.0 自动 逸俊版,skodaap","2009款 1.8T 手动 限量版,skodaaq","2009款 1.6 自动 逸尊版,skodaar",
					"2009款 1.6 手动 逸尊版,skodaas","2009款 1.8T 自动 逸尊版,skodaat","2009款 1.8T 自动 逸仕版,skodaau","2009款 2.0 自动 逸仕版,skodaav",
					"2009款 2.0 手动 逸仕版,skodaaw","2009款 1.6 自动 逸仕版,skodaax","2009款 1.6 手动 逸仕版,skodaay","2009款 1.6 自动 逸致版,skodaaz",
					"2009款 1.6 手动 逸致版,skodaaaa",
					"2010款 1.4T 自动 Greenline,skodaaab","2010款 1.8T 自动 逸尊版,skodaaac","2010款 1.8T 自动 逸俊版,skodaaad","2010款 1.4T 自动 逸尊版,skodaaae",
					"2010款 1.4T 自动 逸俊版,skodaaaf","2010款 1.4T 手动 逸俊版,skodaaag","2010款 2.0 手动 逸俊版,skodaaah","2010款 2.0 自动 逸俊版,skodaaai",
					"2010款 1.6 自动 逸尊版,skodaaaj","2010款 1.6 手动 逸俊版,skodaaak","2010款 1.6 自动 逸俊版,skodaaal","2010款 1.6 手动 逸致版,skodaaam",
					"2010款 1.6 自动 逸致版,skodaaan",
					"2012款 1.6 自动 逸致版,skodaaao","2012款 1.4T 自动 Greenline2,skodaaap","2012款 1.4T 自动 逸尊版,skodaaaq","2012款 1.4T 自动 逸俊版,skodaaar",
					"2012款 2.0 手动 逸杰版,skodaaas","2012款 2.0 自动 逸杰版,skodaaat","2012款 1.8T 自动 逸尊版,skodaaau","2012款 1.8T 自动 逸俊版,skodaaav",
					"2012款 1.6 手动 逸致版,skodaaaw","2012款 1.6 手动 逸俊版,skodaaax","2012款 1.6 手动 逸杰版,skodaaay","2012款 1.6 自动 逸俊版,skodaaaz",
					"2012款 1.6 自动 逸杰版,skodaaba","2012款 1.4T 手动 逸俊版,skodaabb","2013款 1.4T 自动 逸致版,skodaabc","2013款 1.8T 自动 逸尊版,skodaabd",
					"2013款 1.8T 自动 逸俊版,skodaabe","2013款 1.4T 自动 Greenline2,skodaabf",
					"2013款 1.4T 自动 逸尊版,skodaabg","2013款 1.4T 自动 逸俊版,skodaabh","2013款 1.4T 手动 逸俊版,skodaabi","2013款 2.0 自动 逸杰版,skodaabj",
					"2013款 2.0 手动 逸杰版,skodaabk","2013款 1.6 自动 逸俊版,skodaabl","2013款 1.6 手动 逸俊版,skodaabm","2013款 1.6 自动 逸杰版,skodaabn",
					"2013款 1.6 自动 逸致版,skodaabo","2013款 1.6 手动 逸杰版,skodaabp","2013款 1.6 手动 逸致版,skodaabq","2013款 1.6 自动 五周年典藏版,skodaabr",
					"2014款 2.0 手动 逸杰版,skodaabs","2014款 2.0 自动 逸杰版,skodaabt","2014款 1.8T 自动 逸尊版,skodaabu","2014款 1.8T 自动 逸俊版,skodaabv",
					"2014款 1.6 自动 逸俊版,skodaabw","2014款 1.6 自动 逸杰版,skodaabx","2014款 1.6 自动 逸致版,skodaaby","2014款 1.6 手动 逸俊版,skodaabz",
					"2014款 1.6 手动 逸杰版,skodaaca","2014款 1.6 手动 逸致版,skodaacb","2014款 1.4T 自动 逸尊版,skodaacc","2014款 1.4T 自动 GreenLine,skodaacd",
					"2014款 1.4T 手动 逸俊版,skodaace","2014款 1.4T 自动 逸俊版,skodaacf","2014款 1.4T 自动 逸致版,skodaacg",
					"2015款 1.6 自动 经典逸杰版,skodaach","2015款 1.6 手动 经典逸杰版,skodaaci","2015款 1.6 自动 经典逸致版,skodaacj",
					"2015款 1.6 手动 经典逸致版,skodaack","2015款 1.6 自动 逸致版,skodaacl","2015款 1.6 自动 逸杰版,skodaacm",
					"2015款 1.6 自动 逸俊版,skodaacn","2015款 1.4T 自动 逸致版,skodaaco","2015款 1.4T 自动 逸俊版,skodaacp",
					"2015款 1.4T 自动 逸尊版,skodaacq","2015款 1.4T 手动 逸俊版,skodaacr","2015款 1.6 手动 逸俊版,skodaacs",
					"2015款 1.6 手动 逸杰版,skodaact","2015款 1.6 手动 逸致版,skodaacu","2016款 1.4T 自动 280TSI尊行版,skodaacv",
					"2016款 1.4T 自动 280TSI智行版,skodaacw","2016款 1.4T 自动 280TSI前行版,skodaacx","2016款 1.6 自动 智行版,skodaacy",
					"2016款 1.6 手动 智行版,skodaacz","2016款 1.6 自动 创行版,skodaada","2016款 1.6 手动 创行版,skodaadb",
					"2016款 1.6 自动 前行版,skodaadc","2016款 1.6 手动 前行版,skodaadd"
					],
			
			
			"晶锐":["2008款 1.6 自动 晶享版,skodaba","2008款 1.4 自动 晶享版,skodabb","2008款 1.6 手动 晶灵版,skodabc","2008款 1.4 手动 晶灵版,skodabd",
					"2008款 1.4 手动 晶致版,skodabe","2009款 1.6 自动 晶灵版,skodabf","2009款 1.4 自动 晶灵版,skodabg","2009款 1.4 自动 晶致版,skodabh",
					"2010款 1.6 自动 晶享版,skodabi","2010款 1.6 自动 酷黑版,skodabj","2010款 1.6 手动 酷黑版,skodabk","2010款 1.6 手动 晶灵版,skodabl",
					"2010款 1.6 自动 晶灵版,skodabm","2011款 1.6 手动 Sport版,skodabn","2011款 1.6 自动 酷黑版,skodabo","2011款 1.6 自动 晶享版,skodabp",
					"2011款 1.6 手动 酷黑版,skodabq","2011款 1.4 自动 酷炫版,skodabr","2011款 1.4 手动 酷炫版,skodabs","2011款 1.4 自动 晶灵版,skodabt",
					"2011款 1.4 手动 晶灵版,skodabu","2011款 1.4 自动 晶致版,skodabv","2011款 1.4 手动 晶致版,skodabw",
					"2012款 1.6 手动 酷黑版,skodabx","2012款 1.6 自动 酷黑版,skodaby","2012款 1.6 自动 五周年典藏版,skodabz","2012款 1.6 手动 Sport版,skodabaa",
					"2012款 1.6 自动 Sport版,skodabab","2012款 1.6 自动 晶享版,skodabac","2012款 1.4 手动 晶致版,skodabad","2012款 1.4 手动 晶灵版,skodabae",
					"2012款 1.4 自动 晶灵版,skodabaf","2012款 1.4 自动 晶致版,skodabag","2013款 1.6 手动 酷黑版,skodabah","2013款 1.6 自动 酷黑版,skodabai",
					"2013款 1.6 自动 晶享版,skodabaj","2013款 1.6 自动 Sport版,skodabak","2013款 1.6 手动 Sport版,skodabal",
					"2014款 1.6 自动 Monte Carlo版,skodabam","2014款 1.6 自动 晶享版,skodaban","2014款 1.6 自动 Sport版,skodabao","2014款 1.6 手动 Sport版,skodabap",
					"2014款 1.4 自动 晶灵版,skodabaq","2014款 1.4 手动 晶灵版,skodabar","2014款 1.4 自动 晶致版,skodabas","2014款 1.4 手动 晶致版,skodabat",
					"2015款 1.6 自动 智行版,skodabau","2015款 1.6 自动 运动版,skodabav","2015款 1.6 手动 运动版,skodabaw","2015款 1.6 自动 创行版,skodabax",
					"2015款 1.4 自动 创行版,skodabay","2015款 1.4 手动 创行版,skodabaz","2015款 1.4 自动 前行版,skodabba","2015款 1.4 手动 前行版,skodabbb"
					],
			
			
			"欧雅":["2004款 2.0 自动,skodaca"],
			
			"速派":["2013款 1.8T 手动 名仕版,skodada","2013款 1.4T 自动 绅仕版,skodadb","2013款 1.4T 自动 名仕版,skodadc","2013款 2.0T 自动 尊仕版,skodadd",
				"2013款 1.8T 自动 雅仕版,skodade","2013款 1.8T 自动 名仕版,skodadf","2013款 1.4T 手动 绅仕版,skodadg","2013款 1.4T 自动 绿动版,skodadh",
				"2015款 1.8T 手动 绅仕版,skodadi","2016款 2.0T 自动 380旗舰版,skodadj","2016款 2.0T 自动 380尊行版,skodadk",
				"2016款 1.8T 自动 330智行版,skodadl","2016款 1.8T 自动 330创行版,skodadm",
				"2016款 1.4T 自动 280创行版,skodadn","2016款 1.4T 自动 280前行版,skodado","2016款 1.4T 手动 280前行版,skodadp"
				],
			
			"昊锐":["2009款 2.0T 自动 优雅版,skodaea","2009款 1.8T 自动 尊雅版,skodaeb","2009款 1.8T 手动 智雅版,skodaec","2009款 2.0T 自动 旗舰版,skodaed",
					"2009款 1.4T 手动 智雅版,skodaee","2009款 1.8T 自动 贵雅版,skodaef","2009款 1.8T 自动 智雅版,skodaeg","2009款 1.8T 自动 优雅版,skodaeh",
					"2010款 1.4T 手动 智雅版,skodaei","2010款 1.4T 自动 优雅版,skodaej","2010款 1.4T 自动 智雅版,skodaek","2010款 2.0T 自动 尊雅版,skodael",
					"2010款 2.0T 自动 贵雅版,skodaem","2012款 1.4T 自动 大客户Fleet3,skodaen","2012款 1.8T 自动 大客户Fleet2,skodaeo",
					"2012款 1.8T 手动 大客户Fleet1,skodaep","2012款 1.4T 自动 五周年典藏版,skodaeq",
					"2012款 1.8T 手动 优雅版,skodaer","2012款 1.8T 自动 优雅版,skodaes","2012款 1.8T 自动 贵雅版,skodaet","2012款 1.4T 手动 智雅版,skodaeu",
					"2012款 1.4T 自动 智雅版,skodaev","2012款 1.4T 自动 优雅版,skodaew","2012款 2.0T 自动 尊雅版,skodaex","2012款 2.0T 自动 典雅版,skodaey",
					"2012款 1.8T 自动 贵雅版油气混合,skodaez","2012款 1.8T 自动 优雅版油气混合,skodaeaa","2012款 1.8T 手动 优雅版油气混合,skodaeab",
					"2012款 1.4T 自动 优雅版油气混合,skodaeac","2012款 1.4T 自动 智雅版油气混合,skodaead","2012款 1.4T 手动 智雅版油气混合,skodaeae",
					"2013款 1.4T 自动 智雅版,skodaeaf","2013款 1.4T 自动 优雅版,skodaeag","2013款 1.4T 手动 智雅版,skodaeah","2013款 1.4T 自动 GreenLine2,skodaeai"
					],
								
			"Yeti":["2014款 1.4T 手动 探索版前驱,skodafa","2014款 1.4T 手动 极地版前驱 带外挂备胎,skodafb","2014款 1.4T 自动 野驱版前驱 带外挂备胎,skodafc",
					"2014款 1.4T 自动 极地版前驱 带外挂备胎,skodafd","2014款 1.4T 自动 探索版前驱,skodafe","2014款 1.4T 自动 魅影版前驱,skodaff",
					"2014款 1.4T 自动 炫彩版前驱,skodafg","2014款 1.4T 手动 炫彩版前驱,skodafh","2014款 1.6 手动 炫彩版前驱,skodafi",
					"2014款 1.8T 自动 野驱版四驱,skodafj","2014款 1.8T 自动 极地版四驱,skodafk","2014款 1.8T 自动 野驱版四驱 带外挂备胎,skodafl",
					"2014款 1.8T 自动 极地版四驱 带外挂备胎,skodafm","2014款 1.4T 手动 极地版前驱,skodafn",
					"2016款 1.8T 自动 尊行版前驱,skodafo","2016款 1.4T 自动 尊行版前驱,skodafp","2016款 1.4T 自动 创行版前驱,skodafq",
					"2016款 1.4T 自动 前行版前驱,skodafr","2016款 1.4T 手动 前行版前驱,skodafs","2016款 1.6 手动 前行版前驱,skodaft"
					], 
			
			"明锐RS":["2010款 2.0T 自动,skodaga"],
			
			
			"法比亚":["2000款 三厢 1.4 手动 (75HP),skodaha"],
			
			"晶锐Scout":["2012款 1.6 自动 跨界版Scout,skodaia","2012款 1.6 自动 跨界版Scout CLS,skodaib","2014款 1.6 自动 跨界版Scout,skodaic"],
			
			"昊锐旅行车":["2013款 2.0T 自动 舒适版,skodaja","2013款 2.0T 自动 L&K限量版,skodajb"],
			
			"昕锐":["2013款 1.6 自动 智选版,skodaka","2013款 1.6 自动 乐选版,skodakb","2013款 1.4 手动 乐选版,skodakc","2013款 1.4 手动 智选版,skodakd",
					"2013款 1.6 自动 优选版,skodake","2013款 1.6 手动 优选版,skodakf","2013款 1.6 手动 智选版,skodakg","2013款 1.6 手动 乐选版,skodakh",          
					"2015款 1.4 手动 智选版,skodaki","2015款 1.4 手动 乐选版,skodakj","2015款 1.6 自动 智选版,skodakk","2015款 1.6 自动 优选版,skodakl",
					"2015款 1.6 自动 乐选版,skodakm","2015款 1.6 手动 优选版,skodakn","2015款 1.6 手动 智选版,skodako","2015款 1.6 手动 乐选版,skodakp",                  
					"2016款 1.6 自动 尊行版,skodakq","2016款 1.6 手动 尊行版,skodakr","2016款 1.6 自动 创行版,skodaks","2016款 1.6 手动 创行版,skodakt",
					"2016款 1.6 自动 前行版,skodaku","2016款 1.6 手动 前行版,skodakv","2016款 1.4 手动 创行版,skodakw","2016款 1.4 手动 前行版,skodakx"
					],

			"速派(进口)":["2005款 1.8T 手动 豪华型,skodala","2005款 1.8T 手动 标准型,skodalb","2005款 1.8T 手动 舒适型,skodalc"],
			
			"Yeti(进口)":["2013款 1.8T 自动 尊享版,skodama","2013款 1.8T 自动 尊贵版,skodamb"],
			
			"速尊旅行版":["2014款 2.0T 自动 尊贵版,skodana","2014款 2.0T 自动 L&K限量版,skodanb"],
			
			"昕动":["2014款 1.4T 自动 致享版,skodaoa","2014款 1.4T 自动 舒享版,skodaob","2014款 1.4T 自动 乐享版,skodaoc","2014款 1.6 自动 致享版,skodaod",
					"2014款 1.6 自动 乐享版,skodaoe","2014款 1.6 自动 悦享版,skodaof","2014款 1.6 手动 悦享版,skodaog","2014款 1.6 手动 乐享版,skodaoh",
					"2016款 1.4T 自动 230TSI智行版,skodaoi","2016款 1.6 自动 智行版,skodaoj","2016款 1.6 自动 创行版,skodaok","2016款 1.6 手动 创行版,skodaol",
					"2016款 1.6 自动 前行版,skodaom","2016款 1.6 手动 前行版,skodaon","2016款 1.4 手动 前行版,skodaoo"
					],
                  
			
			"明锐旅行版":["2015款 1.8T 自动 逸享版,skodapa"," 2015款 1.8T 自动 逸臻版,skodapb"],
			
//"Smart"		
			"ForTwo":["2007款 1.0 自动 Brabus,smartaa","2007款 1.0 自动 (84HP)敞篷版,smartab","2007款 1.0 自动 (84HP),smartac","2007款 1.0 自动 (71HP),smartad",
					"2009款 1.0 自动 敞篷标准版,smartae","2009款 1.0 自动 敞篷版Style,smartaf","2009款 1.0 自动 硬顶版Pure,smartag","2009款 1.0 自动 硬顶版Style,smartah",
					"2009款 1.0 自动 硬顶标准版,smartai",
					"2010款 1.0 自动 硬顶pure虎年特别版,smartaj","2010款 1.0 自动 硬顶标准虎年特别版,smartak","2010款 1.0 自动 敞篷标准虎年特别版,smartal",
					"2010款 1.0 自动 硬顶Style虎年特别版,smartam","2010款 1.0 自动 敞篷Style虎年特别版,smartan","2010款 1.0 自动 敞篷亚光灰限量版,smartao",
					"2010款 1.0 自动 硬顶亚光灰限量版,smartap",
					"2011款 1.0T 自动 硬顶燃橙版,smartaq","2011款 1.0T 自动 敞篷燃橙版,smartar","2011款 1.0T 自动 硬顶激情版,smartas",
					"2011款 1.0T 自动 敞篷激情版,smartat","2011款 1.0T 自动 博速特别版,smartau","2011款 1.0 自动 MHD硬顶燃橙版,smartav","2011款 1.0 自动 MHD硬顶激情版,smartaw",
					"2011款 1.0 自动 MHD硬顶标准版,smartax","2011款 1.0 自动 MHD科比特别版,smartay","2011款 1.0 自动 MHD敞篷燃橙版,smartaz","2011款 1.0 自动 MHD敞篷激情版,smartaaa",
					"2011款 1.0T 自动 兔年限量版,smartaab","2011款 1.0 自动 MHD兔年特别版,smartaac",
					"2012款 1.0 自动 硬顶舒适版,smartaad","2012款 1.0 自动 硬顶标准版,smartaae","2012款 1.0T 自动 敞篷激情版,smartaaf","2012款 1.0 自动 敞篷激情版,smartaag",
					"2012款 1.0 自动 硬顶激情版,smartaah","2012款 1.0 自动 MHD,smartaai","2012款 1.0 自动 Brabus敞篷,smartaaj","2012款 1.0 自动 Brabus,smartaak",
					"2012款 1.0T 自动 敞篷激情烈焰特别版,smartaal","2012款 1.0T 自动 硬顶激情烈焰版,smartaam","2012款 1.0 自动 MHD敞篷激情烈焰特别版,smartaan",
					"2012款 1.0 自动 MHD硬顶激情烈焰特别版,smartaao","2012款 1.0T 自动 博速Xclusive,smartaap","2012款 1.0T 自动 硬顶激情领航版,smartaaq",
					"2012款 1.0T 自动 敞篷激情领航版,smartaar","2012款 1.0T 自动 敞篷激情版,smartaas","2012款 1.0T 自动 硬顶激情版,smartaat",
					"2012款 1.0 自动 MHD龙年限量特别版,smartaau","2012款 1.0T 自动 流光灰特别版,smartaav",
					"2013款 1.0T 自动 硬顶激情版,smartaaw","2013款 1.0T 自动 敞篷城市游侠特别限量版,smartaax","2013款 1.0T 自动 硬顶城市游侠特别限量版,smartaay",
					"2013款 1.0 自动 MHD敞篷城市游侠特别限量版,smartaaz","2013款 1.0 自动 MHD硬顶城市游侠特别限量版,smartaba","2013款 1.0 自动 MHD新年特别版,smartabb",
					"2013款 1.0T 自动 敞篷冰炫特别版,smartabc","2013款 1.0T 自动 硬顶冰炫特别版,smartabd","2013款 1.0 自动 MHD硬顶冰炫特别版,smartabe",
					"2014款 1.0T 自动 敞篷城市光波激情版,smartabf","2014款 1.0 自动 MHD敞篷城市光波激情版,smartabg",
					"2014款 1.0T 自动 硬顶城市光波激情版,smartabh","2014款 1.0 自动 MHD硬顶城市光波激情版,smartabi","2014款 1.0T 自动 敞篷激情版BoConcept,smartabj",
					"2014款 1.0 自动 MHD敞篷激情版BoConcept,smartabk","2014款 1.0T 自动 硬顶激情版BoConcept,smartabl",
					"2014款 1.0 自动 MHD硬顶激情版BoConcept,smartabm","2014款 纯电动,smartabn","2014款 1.0 自动 后驱硬顶新年特别版,smartabo",
					"2014款 1.0 自动 硬顶巧克力特别版,smartabp",
					"2015款 1.0 自动 硬顶激情版,smartabq","2015款 1.0 自动 硬顶灵动版,smartabr","2015款 1.0T 自动 炫闪特别版,smartabs",
					"2015款 1.0 自动 MHD炫闪特别版,smartabt","2015款 1.0 自动 MHD舒适畅游版,smartabu","2015款 1.0 自动 硬顶五周年纪念版,smartabv",
					"2015款 1.0 自动 MHD硬顶新年特别版,smartabw","2015款 1.0 自动 MHD紫夜限量版,smartabx"
					],
			
//"世爵"	
			
			
			"C12":["2006款 6.0 手动 Laturbie,shijueaa","2007款 6.0 手动 Zagato特别版,shijueab"],
			
			"C8":["2009款 4.2 手动 Aileron,shijueba","2009款 4.2 自动 Aileron,shijuebb",
				  "2009款 4.2 手动 Laviolette LM85限量版,shijuebc","2009款 4.2 手动 Spyder SWB,shijuebd",
				  "2010款 4.2 自动 Aileron,shijuebe"
				],
			
			"D12":["2006款 6.0 自动 Peking to Paris,shijueca"],
			
			"D8":["2010款 6.2 手动 Peking to Paris,shijueda"],
			
			
//"三菱"

			"欧蓝德(进口)":["2005款 3.0 自动 四驱,mitsubishiba","2006款 3.0 自动 EX精英GT版四驱,mitsubishibb","2007款 3.0 自动 前驱,mitsubishibc",
					"2007款 3.0 自动 EX精英GT版四驱,mitsubishibd","2007款 2.0T 手动 四驱 柴油,mitsubishibe","2007款 2.4 自动 四驱,mitsubishibf",
					"2007款 3.0 自动 EX豪华型四驱,mitsubishibg","2007款 2.4 自动 EX时尚版四驱,mitsubishibh","2007款 2.4 自动 EX舒适版前驱,mitsubishibi",
					"2007款 3.0 自动 EX顶级型四驱,mitsubishibj","2007款 3.0 自动 EX标准型四驱,mitsubishibk","2008款 3.0 自动 EX精英GT版四驱,mitsubishibl",
					"2009款 3.0 自动 精英版四驱,mitsubishibm","2009款 3.0 自动 豪华版四驱,mitsubishibn","2009款 2.4 自动 时尚版四驱,mitsubishibo",
					"2009款 2.4 自动 舒适版前驱,mitsubishibp",
					"2010款 3.0 自动 劲界精英GT版四驱,mitsubishibq","2010款 3.0 自动 劲界豪华版四驱,mitsubishibr","2010款 2.4 自动 劲界时尚版四驱,mitsubishibs",
					"2010款 2.4 自动 劲界舒适版前驱,mitsubishibt","2010款 2.0 自动 EX劲界运动版四驱,mitsubishibu","2010款 2.0 自动 EX劲界休闲版前驱,mitsubishibv",
					"2010款 2.4 自动 RX限量版前驱,mitsubishibw","2011款 2.4 自动 劲界时尚版四驱,mitsubishibx",
					"2012款 3.0 自动 EX劲界精英GT版四驱,mitsubishiby","2012款 3.0 自动 EX劲界豪华导航版四驱,mitsubishibz","2012款 2.4 自动 EX劲界时尚导航版四驱,mitsubishibaa",
					"2012款 2.0 自动 EX劲界运动导航版四驱,mitsubishibab","2012款 2.0 自动 EX劲界都市导航版四驱,mitsubishibac",
					"2013款 2.4 自动 精英GT版7座四驱,mitsubishibad","2013款 2.4 自动 豪华导航版7座四驱,mitsubishibae","2013款 2.4 自动 豪华导航版5座四驱,mitsubishibaf",
					"2013款 2.0 自动 都市导航版5座四驱,mitsubishibag","2013款 2.0 自动 运动导航版5座前驱,mitsubishibah",
					"2014款 2.4 自动 精英GT超值版7座四驱,mitsubishibai","2014款 2.4 自动 豪华超值版7座四驱,mitsubishibaj","2014款 2.4 自动 豪华超值版5座四驱,mitsubishibak",
					"2014款 2.0 自动 都市超值版5座四驱,mitsubishibal","2014款 2.0 自动 运动超值版5座前驱,mitsubishibam",
					"2016款 2.4 自动 旗舰GT版7座四驱,mitsubishiban","2016款 2.4 自动 精英版7座四驱,mitsubishibao","2016款 2.4 自动 精英版5座四驱,mitsubishibap",
					"2016款 2.0 自动 舒适版5座前驱,mitsubishibaq"
					],
			
			"帕杰罗(进口)":["2005款 3.8 手动 GLS,mitsubishica","2005款 3.0 自动 GLX,mitsubishicb","2005款 3.0 自动 GLS,mitsubishicc","2005款 3.8 自动 GLS,mitsubishicd",
					"2007款 3.8 自动 GLS,mitsubishice","2007款 3.8 手动 GLS,mitsubishicf","2007款 3.0 手动 GL,mitsubishicg","2007款 3.0 手动 GLX,mitsubishich",
					"2007款 3.0 自动 GLX,mitsubishici","2007款 3.0 手动 GLS,mitsubishicj","2007款 3.0 自动 GLS,mitsubishick",
					"2008款 3.0 自动 GLS Navi,mitsubishicl","2008款 3.0 手动 GLX,mitsubishicm","2008款 3.0 自动 GLX,mitsubishicn","2008款 3.0 手动 GLS,mitsubishico",
					"2008款 3.0 自动 GLS,mitsubishicp","2008款 3.0 手动 GL,mitsubishicq","2011款 3.0 手动 GLX,mitsubishicr","2011款 3.0 手动 GLS,mitsubishics",
					"2011款 3.0 手动 GL,mitsubishict","2011款 3.0 自动 GLX,mitsubishicu","2011款 3.0 自动 GLS,mitsubishicv"
					],
			
			"君阁":["2009款 2.0 自动 旗舰型7座,mitsubishida","2009款 2.0 自动 豪华型7座,mitsubishidb","2009款 2.0 手动 豪华型7座,mitsubishidc","2009款 2.0 手动 经典型5座,mitsubishidd",
					"2010款 2.0 手动 经典型7座,mitsubishide","2010款 2.0 手动 经典型增配版5座,mitsubishidf","2010款 2.0 手动 豪华型增配版7座,mitsubishidg",
					"2010款 2.0 手动 经典型5座,mitsubishidh","2010款 2.0 手动 豪华型7座,mitsubishidi","2010款 2.0 自动 旗舰型7座,mitsubishidj",
					"2010款 2.0 自动 豪华型7座,mitsubishidk","2011款 2.0 自动 旗舰型7座,mitsubishidl","2011款 2.0 自动 豪华型7座,mitsubishidm"
					],
			
			"戈蓝":["2007款 2.4 自动 精英型,mitsubishiea","2007款 2.4 自动 尊贵型,mitsubishieb","2007款 2.4 自动 旗舰型,mitsubishiec",
					"2009款 2.0 自动 铭仕型,mitsubishied","2009款 2.0 自动 精锐型,mitsubishiee","2009款 2.4 自动 旗舰型韵动版,mitsubishief",
					"2009款 2.4 自动 尊贵型韵动版,mitsubishieg","2009款 2.4 自动 旗舰型升级版,mitsubishieh","2009款 2.4 自动 尊贵型升级版,mitsubishiei",
					"2012款 2.0 自动 精锐版,mitsubishiej","2012款 2.0 自动 铭仕版,mitsubishiek","2012款 2.4 自动 尊贵升级版,mitsubishiel",
					"2012款 2.4 自动 旗舰升级版,mitsubishiem"
					],
			
			"翼神":["2010款 1.8 自动 致尚型,mitsubishifa","2010款 1.8 手动 致尚型,mitsubishifb","2010款 1.8 手动 时尚版睿智型,mitsubishifc",
					"2010款 2.0 自动 运动版旗舰型,mitsubishifd",
					"2010款 2.0 手动 运动版豪华型,mitsubishife","2010款 1.8 自动 时尚版豪华型,mitsubishiff","2010款 1.8 自动 时尚版舒适型,mitsubishifg",
					"2010款 1.8 手动 时尚版舒适型,mitsubishifh",
					"2011款 1.8 自动 致尚版限量款,mitsubishifi","2011款 1.8 自动 时尚版舒适型,mitsubishifj","2011款 1.8 手动 贺岁版舒适型,mitsubishifk",
					"2011款 1.8 手动 魅力版,mitsubishifl","2011款 2.0 自动 运动版旗舰型,mitsubishifm","2011款 2.0 手动 致炫版舒适型,mitsubishifn",
					"2011款 2.0 手动 致炫版豪华型,mitsubishifo","2011款 2.0 自动 致炫版旗舰型,mitsubishifp","2011款 1.8 手动 时尚版舒适型,mitsubishifq",
					"2011款 1.8 手动 时尚版睿智型,mitsubishifr","2011款 1.8 手动 致尚版豪华型,mitsubishifs","2011款 1.8 自动 豪华型致尚版,mitsubishift",
					"2012款 1.8 手动 时尚版舒适型,mitsubishifu","2012款 1.8 手动 时尚版睿智型,mitsubishifv","2012款 2.0 自动 致炫版旗舰型,mitsubishifw",
					"2012款 1.8 自动 时尚版舒适型,mitsubishifx","2012款 1.8 自动 经典黑白版,mitsubishify","2012款 1.8 手动 致尚版豪华型,mitsubishifz",
					"2012款 1.8 手动 贺岁版舒适型,mitsubishifaa","2012款 2.0 自动 运动版旗舰型,mitsubishifab","2012款 2.0 手动 致炫版舒适型,mitsubishifac",
					"2012款 2.0 手动 致炫版豪华型,mitsubishifad","2012款 1.8 自动 贺岁版舒适型,mitsubishifae","2012款 1.8 自动 致尚版豪华型,mitsubishifaf",
					"2012款 1.8 自动 魅力版,mitsubishifag","2012款 1.8 手动 魅力版,mitsubishifah","2012款 1.8 手动 贺岁版天窗型,mitsubishifai",
					"2012款 1.8 自动 贺岁版天窗型,mitsubishifaj",
					"2013款 1.8 自动 时尚版舒适型,mitsubishifak","2013款 1.8 自动 致尚版豪华型,mitsubishifal","2013款 1.8 手动 时尚版舒适型,mitsubishifam",
					"2013款 1.8 手动 时尚版睿智型,mitsubishifan","2013款 1.8 手动 致尚版豪华型,mitsubishifao","2013款 1.6 手动 风尚版豪华型,mitsubishifap",
					"2013款 1.6 手动 风尚版经典型,mitsubishifaq","2013款 2.0 自动 运动版旗舰型,mitsubishifar","2013款 2.0 自动 致炫版旗舰型,mitsubishifas",
					"2013款 2.0 手动 致炫版豪华型,mitsubishifat","2013款 2.0 手动 致炫版舒适型,mitsubishifau","2013款 2.0 自动 经典黑白版,mitsubishifav",
					"2014款 1.8 自动 竞速版舒适型,mitsubishifaw","2014款 1.8 手动 竞速版舒适型,mitsubishifax",
					"2015款 1.8 手动 魅影版,mitsubishifay","2015款 1.8 自动 魅影版,mitsubishifaz","2015款 2.0 自动 旗舰版,mitsubishifba","2015款 1.8 自动 致尚版,mitsubishifbb",
					"2015款 1.8 手动 致尚版,mitsubishifbc","2015款 1.8 自动 时尚版,mitsubishifbd","2015款 1.6 手动 时尚版,mitsubishifbe"
					],
			
			"菱绅":["2006款 2.4 手动 豪华型7座,mitsubishiga","2006款 2.4 手动 经典型7座,mitsubishigb","2006款 2.4 自动 经典型7座,mitsubishigc",
					"2006款 2.4 自动 尊贵型6-7座,mitsubishigd","2006款 2.4 自动 豪华型7座,mitsubishige"
					],
			
			"蓝瑟":["2006款 1.6 自动 豪华型运动型,mitsubishiha","2006款 1.6 手动 LPG油气混合,mitsubishihb","2006款 1.6 自动 豪华型带OBD运动型,mitsubishihc",
					"2006款 1.6 自动 舒适型,mitsubishihd","2006款 1.6 手动 舒适型,mitsubishihe","2006款 1.6 自动 豪华型,mitsubishihf","2006款 1.6 手动 豪华型,mitsubishihg",
					"2007款 1.6 手动 LPG油气混合,mitsubishihh","2007款 1.6 手动 CNG油气混合,mitsubishihi","2007款 1.6 自动 豪华型运动版,mitsubishihj",
					"2007款 1.6 手动 舒适型运动版,mitsubishihk","2008款 1.6 手动 舒适型炫动版,mitsubishihl","2008款 1.6 手动 豪华型炫动版,mitsubishihm",
					"2008款 1.6 自动 舒适型炫动版,mitsubishihn","2008款 1.6 自动 豪华型炫动版,mitsubishiho",
					"2010款 1.6 手动 SXI舒适型卓越版,mitsubishihp","2010款 1.6 手动 SXI舒适型运动版,mitsubishihq","2010款 1.6 自动 SXI舒适型运动版,mitsubishihr",
					"2010款 1.6 手动 EXI豪华型卓越版,mitsubishihs","2010款 1.6 手动 EXI豪华型运动版,mitsubishiht","2010款 1.6 自动 EXI豪华型卓越版,mitsubishihu",
					"2012款 1.6 手动 SEi舒适版 油气混合,mitsubishihv","2012款 1.6 手动 SXI EXi豪华版,mitsubishihw","2012款 1.6 手动 SXI SEi舒适版,mitsubishihx",
					"2012款 1.6 手动 SXI EXi豪华版 油气混合,mitsubishihy","2015款 1.6 手动 乐购版,mitsubishihz","2015款 1.6 手动 S Design,mitsubishihaa"
					],
			
			"风迪思":["2014款 1.8 自动 旗舰型,mitsubishiia","2014款 1.8 自动 豪华型,mitsubishiib","2014款 1.8 手动 舒适型,mitsubishiic",
					"2014款 1.6 手动 舒适型,mitsubishiid","2014款 1.6 手动 经典型,mitsubishiie"
					],
			
			"ASX劲炫":["2011款 2.0 自动 炫悦版前驱,mitsubishija","2011款 2.0 自动 炫逸版前驱,mitsubishijb","2011款 2.0 自动 炫动版前驱,mitsubishijc",
						"2011款 2.0 自动 劲尚版四驱,mitsubishijd","2011款 2.0 自动 劲酷版四驱,mitsubishije","2012款 2.0 自动 炫悦导航版前驱,mitsubishijf",
						"2012款 2.0 自动 炫逸导航版前驱,mitsubishijg","2012款 2.0 自动 劲尚导航版四驱,mitsubishijh","2012款 2.0 自动 劲酷导航版四驱,mitsubishiji"
						],

			
			"欧蓝德":["2006款 2.4 手动 四驱,mitsubishika","2006款 2.4 自动 前驱,mitsubishikb","2006款 2.4 手动 前驱,mitsubishikc",
						"2006款 2.4 自动 四驱,mitsubishikd","2007款 2.4 手动 时尚版前驱,mitsubishike"
					],
			
			"蓝瑟(进口)":["2007款 EX 2.4 自动 豪华运动版,mitsubishila","2007款 EX 2.0 自动 舒适版,mitsubishilb","2007款 EX 2.0 自动 时尚运动版,mitsubishilc","2007款 EX 2.0 自动 时尚版,mitsubishild",
						"2007款 EX 2.4 自动 ES,mitsubishile","2007款 EX 2.4 自动 VRX,mitsubishilf","2007款 EX 2.4 自动 VR,mitsubishilg","2007款 EX 2.0 自动,mitsubishilh",
						"2007款 EX 1.8 自动 ES,mitsubishili","2007款 EX 1.8 自动 VRX,mitsubishilj","2007款 EX 1.8 自动 VR,mitsubishilk","2007款 2.4 手动 GTS,mitsubishilkl"   
						],
			
			"帕杰罗":["2005款 3.8 手动 GLS,mitsubishima","2005款 3.0 自动 GLX,mitsubishimb","2005款 3.0 自动 GLS,mitsubishimc","2005款 3.8 自动 GLS,mitsubishimd",
					"2007款 3.8 自动 GLS,mitsubishime","2007款 3.8 手动 GLS,mitsubishimf","2007款 3.0 手动 GL,mitsubishimg","2007款 3.0 手动 GLX,mitsubishimh",
					"2007款 3.0 自动 GLX,mitsubishimi","2007款 3.0 手动 GLS,mitsubishimj","2007款 3.0 自动 GLS,mitsubishimk",
					"2008款 3.0 自动 GLS Navi,mitsubishiml","2008款 3.0 手动 GLX,mitsubishimm","2008款 3.0 自动 GLX,mitsubishimn","2008款 3.0 手动 GLS,mitsubishimo",
					"2008款 3.0 自动 GLS,mitsubishimp","2008款 3.0 手动 GL,mitsubishimq","2011款 3.0 手动 GLX,mitsubishimr","2011款 3.0 手动 GLS,mitsubishims",
					"2011款 3.0 手动 GL,mitsubishimt","2011款 3.0 自动 GLX,mitsubishimu","2011款 3.0 自动 GLS,mitsubishimv"
					],
			
			"蓝瑟翼豪陆神":["2005款 2.0T 自动,mitsubishina","2005款 2.0T 手动 IX,mitsubishinb","2006款 2.0T 手动 IX,mitsubishinc","2008款 2.0T 自动 X BBS版,mitsubishind",
							"2008款 2.0T 自动 X ENKEI版,mitsubishine","2008款 2.0T 手动 X,mitsubishinf","2008款 2.0T 自动 SST,mitsubishing","2008款 2.0T 自动,mitsubishinh"
							],

			
			"劲炫":["2013款 2.0 自动 精英版前驱,mitsubishioa","2013款 2.0 手动 舒适版前驱,mitsubishiob","2013款 2.0 自动 尊贵版四驱,mitsubishioc","2013款 2.0 自动 旗舰版四驱,mitsubishiod",
					"2013款 2.0 自动 豪华版前驱,mitsubishioe","2013款 1.6 手动 标准版前驱,mitsubishiof","2014款 2.0 自动 超越版前驱,mitsubishiog","2015款 2.0 自动 尊贵版四驱,mitsubishioh",
					"2015款 2.0 自动 精英版前驱,mitsubishioi","2015款 2.0 自动 丝绸之路纪念版,mitsubishioj"
					],

			
			"帕杰罗劲畅":["2011款 3.0 自动 运动导航版,mitsubishipa","2011款 3.0 自动 豪华导航版,mitsubishipb"],
			

//	"斯巴鲁"	
			
		"BRZ":["2013款 2.0 手动 豪华型,subaruaa","2013款 2.0 自动 豪华型,subaruab"],	
		
		"XV":["2012款 2.0 自动 豪华导航版,subaruba","2012款 2.0 自动 豪华版,subarubb","2012款 2.0 自动 精英导航版,subarubc",
			"2012款 2.0 自动 精英版,subarubd","2012款 2.0 自动 舒适导航版,subarube","2012款 2.0 自动 舒适版,subarubf",
			"2014款 2.0 自动 豪华导航版,subarubg","2014款 2.0 自动 豪华版,subarubh","2014款 2.0 自动 精英导航版,subarubi",
			"2014款 2.0 自动 精英版,subarubj","2014款 2.0 自动 舒适导航版,subarubk","2014款 2.0 自动 舒适版,subarubl",                  
			"2015款 2.0 自动 豪华导航版,subarubm","2015款 2.0 自动 豪华版,subarubn","2015款 2.0 自动 精英导航版,subarubo",
			"2015款 2.0 自动 精英版,subarubp","2015款 2.0 自动 特装运动版,subarubq"
			],
			
		"傲虎":["2005款 3.0 自动 R,subaruca","2005款 2.5 自动,subarucb","2006款 2.5 手动,subarucc","2006款 3.0 自动 R,subarucd","2006款 2.5 自动,subaruce",
			"2009款 3.6 自动 R,subarucf","2009款 2.5 手动,subarucg","2009款 2.5 自动,subaruch","2010款 3.6 自动 R豪华导航版,subaruci",
			"2010款 2.5 自动 豪华导航版,subarucj","2010款 3.6 自动 R豪华版,subaruck","2010款 2.5 自动 豪华版,subarucl",                  
			"2012款 2.5 自动 运动版,subarucm","2012款 2.5 自动 豪华版,subarucn","2012款 3.6 自动 R豪华版,subaruco",                  
			"2013款 3.6 自动 R豪华导航版,subarucp","2013款 3.6 自动 R豪华版,subarucq","2013款 2.5 自动 运动版导航版,subarucr",
			"2013款 2.5 自动 运动版,subarucs","2013款 2.5 自动 豪华导航版,subaruct","2013款 2.5 自动 豪华版,subarucu",
			"2014款 2.5 自动 雅尚尊贵版,subarucv","2014款 2.5 自动 雅尚智能版,subarucw","2014款 2.5 自动 雅尚运动版,subarucx",
			"2014款 2.5 自动 雅尚精英版,subarucy","2014款 3.6 自动 R豪华导航版,subarucz","2014款 3.6 自动 R豪华版,subarucaa",
			"2015款 2.5 自动 豪华导航版,subarucab","2015款 2.5 自动 经典版,subarucac",
			"2016款 2.0T 自动 尊贵版,subarucad","2016款 2.5 自动 运动导航版,subarucae","2016款 2.5 自动 豪华导航版,subarucaf",
			"2016款 2.5 自动 经典版,subarucag","2015款 2.0T 自动 尊贵版,subarucah","2015款 2.5 自动 运动导航版,subarucai"
			],	
			
		"力狮":["2005款 3.0 自动 R,subaruda","2005款 2.5 自动,subarudb","2005款 2.0 自动 R,subarudc","2006款 2.0 自动 R,subarudd","2006款 3.0 手动 R,subarude",
				"2006款 3.0 自动 R,subarudf",
				"2006款 2.0 手动 R,subarudg","2006款 2.0 手动 R,subarudh","2006款 2.0 自动 R,subarudi","2006款 3.0 自动 R,subarudj","2006款 3.0 手动 R,subarudk",
				"2008款 3.0 自动 R,subarudl","2008款 2.5 手动,subarudm","2008款 2.5 自动,subarudn","2008款 2.5T 手动 Spec.B,subarudo","2008款 2.5T 手动,subarudp",
				"2008款 2.5T 自动,subarudq","2009款 2.5 手动,subarudr","2009款 2.5T 手动,subaruds","2009款 2.0 手动,subarudt","2009款 2.5T 自动,subarudu",
				"2009款 2.5 自动,subarudv","2009款 2.0 手动,subarudw","2010款 2.0 手动 运动导航版,subarudx","2010款 2.5 自动 豪华导航版,subarudy",
				"2010款 2.5 自动 豪华版,subarudz","2010款 2.5T 自动 豪华导航版,subarudaa","2010款 2.5T 自动 豪华版,subarudab",
				"2010款 2.0 自动 豪华导航版,subarudac","2010款 2.0 自动 豪华版,subarudad","2010款 2.0 自动 豪华导航版,subarudae","2010款 2.0 自动 豪华版,subarudaf",
				"2013款 2.5T 自动 豪华导航版,subarudag","2013款 2.5T 自动 豪华版,subarudah","2013款 2.5 自动 X精英版,subarudai","2013款 2.5 自动 X豪华导航版,subarudaj",
				"2013款 2.5 自动 X豪华版,subarudak","2014款 2.5T 自动 GT雅尚运动版,subarudal","2014款 2.5 自动 豪华版,subarudam","2014款 2.5 自动 雅尚智能版,subarudan",
				"2014款 2.5 自动 雅尚精英版,subarudao","2016款 2.0T 自动 极致版,subarudap","2016款 2.0T 自动 经典版,subarudaq","2016款 2.5 自动 荣耀版,subarudar",
				"2016款 2.5 自动 风尚版,subarudas"
				],
		
		"森林人":["2005款 2.5T 自动 XT,subaruea","2005款 2.0 手动 X,subarueb","2005款 2.5T 手动 XT,subaruec","2005款 2.0 自动 X,subarued","2006款 2.5T 自动 XT豪华版,subaruee",
				"2007款 2.5T 自动 XT豪华版,subaruef","2008款 2.5T 自动 XT豪华导航版,subarueg","2008款 2.5T 自动 XT豪华版,subarueh","2008款 2.0 自动 XS豪华导航版,subaruei",
				"2008款 2.0 自动 XS豪华版,subaruej","2008款 2.0 手动 XS豪华版,subaruek","2008款 2.0 手动 X舒适版,subaruel","2009款 2.5 自动 XS豪华版,subaruem",
				"2009款 2.5 自动 XS豪华导航版,subaruen","2010款 2.5T 自动 XT豪华导航版,subarueo","2010款 2.5T 自动 XT豪华版,subaruep","2010款 2.0 自动 XS豪华导航版,subarueq",
				"2010款 2.0 自动 XS豪华版,subaruer","2010款 2.0 手动 XS豪华版,subarues","2010款 2.0 手动 X舒适版,subaruet","2010款 2.5 自动 XS豪华版,subarueu",
				"2010款 2.5 自动 XS豪华导航版,subaruev","2010款 2.5T 自动 XT十万同辉纪念版,subaruew","2010款 2.5 自动 XS十万同辉纪念版,subaruex",
				"2010款 2.0 自动 XS十万同辉运动纪念版,subaruey","2010款 2.0 自动 XS十万同辉纪念版,subaruez","2010款 2.0 自动 XS运动特别导航版,subarueaa",
				"2010款 2.0 自动 XS运动特别版,subarueab","2011款 2.5T 自动 S-EDITION豪华导航版,subarueac","2011款 2.5T 自动 S-EDITION豪华版,subaruead",
				"2011款 2.5T 自动 XT豪华导航版,subarueae","2011款 2.5T 自动 XT豪华版,subarueaf","2011款 2.5 自动 XS豪华导航版,subarueag","2011款 2.5 自动 XS豪华版,subarueah",
				"2011款 2.0 自动 XS豪华导航版,subarueai","2011款 2.0 自动 XS豪华版,subarueaj","2011款 2.0 自动 XS舒适版,subarueak",
				"2011款 2.0 手动 XS豪华版,subarueal","2011款 2.0 手动 X舒适版,subarueam",
				"2012款 2.5 自动 15周年纪念运动版XS,subaruean","2012款 2.5 自动 15周年纪念豪华版XS,subarueao","2012款 2.0 手动 15周年纪念豪华版XS,subarueap",
				"2012款 2.5T 自动 S-EDITION豪华导航版,subarueaq","2012款 2.5T 自动 S-EDITION豪华版,subaruear","2012款 2.5T 自动 XT豪华导航版,subarueas",
				"2012款 2.5T 自动 XT豪华版,subarueat","2012款 2.5 自动 XS运动导航版,subarueau","2012款 2.5 自动 XS运动版,subarueav","2012款 2.5 自动 XS豪华导航版,subarueaw",
				"2012款 2.5 自动 XS豪华版,subarueax","2012款 2.0 手动 XS豪华版,subarueay","2012款 2.0 自动 XS舒适版,subarueaz","2012款 2.0 自动 XS豪华导航版,subarueba",
				"2012款 2.0 自动 XS豪华版,subaruebb","2012款 2.0 手动 X舒适版,subaruebc",
				"2013款 2.0T 自动 尊贵导航版,subaruebd","2013款 2.5 自动 尊贵导航版,subaruebe","2013款 2.5 自动 尊贵版,subaruebf",
				"2013款 2.5 自动 豪华导航版,subaruebg","2013款 2.5 自动 豪华版,subaruebh","2013款 2.5 自动 精英导航版,subaruebi",
				"2013款 2.5 自动 精英版,subaruebj","2013款 2.0 自动 豪华导航版,subaruebk","2013款 2.0 自动 豪华版,subaruebl",
				"2013款 2.0 手动 豪华版,subaruebm","2013款 2.0 自动 舒适版,subaruebn",
				"2015款 2.5 自动 豪华悦享版,subaruebo","2015款 2.5 自动 特装纪念版,subaruebp","2015款 2.0 自动 特装纪念版,subaruebq"
				],
		
		"翼豹":["2005款 三厢  2.0 手动 R,subarufa","2005款 三厢  1.5 手动,subarufb","2005款 三厢  2.5T 手动 WRX,subarufc","2005款 三厢  2.0 自动 R,subarufd",
				"2005款 三厢  2.5T 手动 WRX STi,subarufe","2005款 三厢  2.5 手动 WRX,subaruff","2005款 旅行版  2.5 手动 WRX,subarufg",
				"2005款 旅行版  2.0 手动 R,subarufh","2005款 旅行版  2.0 自动 R,subarufi","2006款 三厢  2.0T 手动 WRX STi,subarufj",
				"2006款 三厢  2.0T 手动 WRX,subarufk","2006款 旅行版  2.0T 手动 WRX,subarufl","2007款 三厢  2.5T 手动,subarufm","2007款 三厢  2.5T 手动 WRX,subarufn",                  
				"2007款 两厢  2.5T 手动 WRX STi标准版,subarufo","2007款 两厢  2.5T 手动 WRX STi强化版,subarufp","2007款 两厢  2.5T 手动,subarufq",
				"2007款 两厢  2.0 手动,subarufr","2007款 两厢  1.5 手动,subarufs","2008款 三厢  2.0 手动 R,subaruft","2008款 三厢  2.0 自动 R运动导航版,subarufu",
				"2008款 三厢  2.0 自动 R运动版,subarufv","2008款 三厢  2.0 自动,subarufw","2008款 三厢  2.5T 手动 WRX STi,subarufx",
				"2008款 两厢  2.0 手动 R,subarufy","2008款 两厢  2.0 自动 R,subarufz","2008款 两厢  2.0 自动 R运动版,subarufaa","2009款 两厢  2.5T 手动 WRX STI,subarufab",
				"2010款 三厢  2.0 自动 XV豪华版,subarufac","2010款 三厢  2.0 自动 XV标准版,subarufad","2010款 两厢  2.0 自动 XV自动标准版,subarufae",
				"2010款 两厢  2.0 自动 XV自动豪华版,subarufaf","2011款 三厢  2.5T 手动 WRX,subarufag","2011款 三厢  2.5T 手动 WRX STi,subarufah",
				"2013款 三厢  2.0 自动,subarufai","2013款 三厢  2.0 手动,subarufaj","2013款 两厢  2.0 自动,subarufak","2013款 两厢  2.0 手动,subarufal",
				"2014款 三厢  2.5T 手动 WRX STI,subarufam"
				],

		
		"驰鹏":["2005款 3.0 自动,subaruga","2007款 3.0 自动,subarugb","2007款 3.6 自动,subarugc","2008款 3.6 自动,subarugd",
				"2011款 3.6 自动 基本型7座,subaruge"
				],
				
//	"萨博"	
			
		"9月3日":["2008款 三厢  2.8T 自动 TurboX,saabaa","2008款 三厢  2.8T 自动 Aero,saabab","2008款 三厢  2.0T 自动 linear,saabac",
				"2008款 三厢  2.8T 自动 TurboX限量版,saabad","2008款 三厢  2.0T 自动 vector,saabae","2008款 三厢  2.8T 手动 Aero运动版,saabaf",
				"2008款 三厢  2.0T 自动 运动版sport,saabag","2008款 三厢  2.0T 自动 运动版comfort,saabah",
				"2008款 三厢  2.8T 自动 Aero运动版,saabai","2008款 三厢  2.0T 自动 运动版touring,saabaj","2008款 旅行车  2.0T 自动 vector,saabak",
				"2009款 三厢  2.0T 自动 运动版sport,saabal","2009款 三厢  2.0T 自动 XWD运动版,saabam","2009款 三厢  2.0T 自动 运动版comfort,saaban",
				"2009款 三厢  2.0T 自动 运动版touring,saabao","2009款 旅行车  2.0T 手动 (200HP)BioPower 油电混合,saabap","2009款 旅行车  2.0T 自动 sport,saabaq",
				"2009款 旅行车  2.0T 自动 comfort,saabar","2009款 旅行车  2.0T 自动 Touring,saabas", 
				"2009款 敞篷车  2.0T 自动 SportCombi sport,saabat","2009款 敞篷车  2.0T 自动 SportCombi vector,saabau",
				"2009款 敞篷车  2.0T 自动 SportCombi comfort,saabav",
				"2009款 敞篷车  2.0T 自动 SportCombi touring,saabaw","2009款 敞篷车  2.0T 自动 SportCombi Aero,saabax"
				],
					
		"9月5日":["2005款 旅行车 2.0T 自动 SportCombi,saabba",
				"2005款 三厢  2.0T 自动,saabbb","2005款 三厢  2.0T 自动 BioPower 油电混合,saabbc","2005款 三厢  2.3T 自动 Vector,saabbd",
				"2005款 三厢  2.3T 自动 Arc,saabbe","2005款 三厢  2.3T 自动 Aero,saabbf","2005款 三厢  2.3T 自动 BioPower 油电混合,saabbg",
				"2010款 三厢  2.8T 自动,saabbh","2009款 三厢  2.0T 自动 Griffin Edition,saabbi"
				],
	
//"天马"
			
		
		"英雄":["2007款 2.0 手动 标准型后驱,tianmaaa",
				"2007款 2.0 手动 豪华型后驱,tianmaab","2007款 2.4 手动 标准型后驱,tianmaac","2007款 2.4 手动 豪华型后驱,tianmaad",
				"2007款 2.8T 手动 标准型后驱 柴油,tianmaae","2007款 2.8T 手动 豪华型后驱 柴油,tianmaaf"
				],
			
		"风锐":["2005款 2.8T 手动 豪华型后驱 柴油,tianmaba",
				"2005款 2.8T 手动 超豪华型后驱 柴油,tianmabb","2005款 2.2 手动 超豪华型后驱,tianmabc","2005款 2.2 手动 豪华型后驱,tianmabd",
				"2005款 2.4 手动 超豪华型后驱,tianmabe","2005款 2.4 手动 豪华型后驱,tianmabf",
				"2006款 2.4 手动 超豪华型后驱,tianmabg","2006款 2.4 手动 豪华型后驱,tianmabh"
				],	
			
		"骏驰":["2005款 2.2 手动 超豪华型后驱,tianmaca",
				"2005款 2.8T 手动 超豪华型后驱 柴油,tianmacb"
				],	
			
//"通宝"	
			
		"宗申通宝":["2007款 1.05 手动,tongbaoaa",
				"2007款 1.05 手动 升级版,tongbaoab"
				],
			
  
//  "天津一汽"
		
		
		"夏利A+":["2005款 1.0 自动 三缸,tjyqba","2005款 1.0 手动 三缸加长型,tjyqbb","2006款 1.0 手动 三缸,tjyqbc",                  
				"2006款 1.0 手动 三缸,tjyqbd","2006款 1.0 手动 三缸加长型,tjyqbe","2006款 1.0 手动 三缸三厢 CNG油气混合,tjyqbf",
				"2006款 1.0 手动 三缸三厢,tjyqbg","2006款 1.0 手动 三缸两厢,tjyqbh","2006款 1.4 手动 四缸两厢,tjyqbi",
				"2006款 1.0 手动 普通型三缸三厢,tjyqbj","2006款 1.0 手动 普通型三缸两厢,tjyqbk","2006款 1.4 手动 四缸三厢,tjyqbl",
				"2007款 1.0 手动 三缸三厢加长版 CNG油气混合,tjyqbm","2008款 1.0 手动 三缸三厢无空调,tjyqbn",
				"2008款 1.0 手动 三缸两厢无空调,tjyqbo","2008款 1.0 手动 三缸三厢,tjyqbp","2008款 1.0 手动 三缸两厢,tjyqbq",
				"2010款 1.0 手动 三缸三厢,tjyqbr","2010款 1.0 手动 三缸三厢无空调,tjyqbs","2010款 1.0 手动 三缸两厢无空调,tjyqbt",
				"2010款 1.0 手动 三缸两厢,tjyqbu","2011款 1.0 手动 三缸两厢无空调,tjyqbv","2011款 1.0 手动 三缸两厢,tjyqbw"
				],          

			
		"夏利N3":["2005款 两厢  1.1 手动 三缸基本型 助力,tjyqca","2005款 两厢  1.3 手动 四缸基本型,tjyqcb","2005款 两厢  1.0 手动 三缸,tjyqcc",
				"2005款 两厢  1.1 手动 三缸基本型,tjyqcd","2005款 两厢  1.3 手动 四缸基本型 助力,tjyqce","2005款 三厢  1.3 手动 四缸基本型,tjyqcf",
				"2005款 三厢  1.3 手动 四缸助力,tjyqcg","2006款 两厢  1.4 手动,tjyqch","2006款 两厢  1.0 手动 三缸,tjyqci","2006款 两厢  1.1 手动 三缸,tjyqcj",
				"2006款 两厢  1.3 手动 四缸,tjyqck","2006款 三厢  1.4 手动 三厢,tjyqcl","2006款 三厢  1.0 手动 三缸,tjyqcm","2006款 三厢  1.1 手动 三缸普通型,tjyqcn",
				"2006款 三厢  1.3 手动 四缸,tjyqco","2005款 三厢  1.0 手动 三缸,tjyqcp","2005款 三厢  1.1 手动 三缸基本型,tjyqcq","2007款 两厢  1.0 手动,tjyqcr",
				"2007款 三厢  1.0 手动 三厢,tjyqcs","2007款 三厢  1.3 手动 四缸,tjyqct","2007款 三厢  1.3 手动 四缸加长版,tjyqcu",
				"2007款 三厢  1.1 手动 三缸加长版,tjyqcv","2008款 两厢  1.0 手动 三缸助力,tjyqcw","2008款 两厢  1.0 手动 三缸,tjyqcx",
				"2008款 两厢  1.4 手动 四缸助力,tjyqcy","2008款 三厢  1.0 手动 三缸助力,tjyqcz","2008款 三厢  1.0 手动 三缸,tjyqcaa",
				"2008款 三厢  1.4 手动 四缸助力,tjyqcab","2010款 三厢  1.0 手动,tjyqcac","2010款 三厢  1.0 手动 助力,tjyqcad",
				"2011款 两厢  1.0 手动 三缸助力,tjyqcae","2011款 两厢  1.0 手动 三缸,tjyqcaf",
				"2012款 三厢  1.0 手动 舒适型,tjyqcag","2012款 三厢  1.0 手动 基本型,tjyqcah","2012款 三厢  1.0 手动 实用型,tjyqcai",                  
				"2012款 两厢  1.0 手动 标准型,tjyqcaj","2012款 两厢  1.0 手动 实用型,tjyqcak"
				],
	
			
		
		"夏利N5":["2010款 1.0 手动 标准型,tjyqda","2010款 1.3 手动 豪华型 气囊,tjyqdb","2010款 1.3 手动 豪华型,tjyqdc","2010款 1.3 手动 标准型,tjyqdd",
				"2011款 1.0 手动 标准型,tjyqde","2011款 1.3 手动 标准型,tjyqdf","2011款 1.3 手动 豪华型 气囊,tjyqdg","2011款 1.3 手动 豪华型,tjyqdh",
				"2014款 1.3 手动 智能节油豪华型,tjyqdi","2014款 1.3 手动 豪华型,tjyqdj","2014款 1.3 手动 舒适型,tjyqdk","2014款 1.3 手动 标准型,tjyqdl",
				"2014款 1.0 手动 舒适型,tjyqdm","2014款 1.0 手动 标准型,tjyqdn"
				],
		
		
		"夏利N7":["2013款 1.3 手动 智能节油尊贵型,tjyqea","2013款 1.3 手动 尊贵型,tjyqeb","2013款 1.3 手动 豪华型,tjyqec","2013款 1.3 手动 舒适型,tjyqed",
				"2014款 1.3 手动 运动尊贵型,tjyqee","2014款 1.3 手动 运动豪华型,tjyqef","2014款 1.3 手动 运动舒适型铝轮圈,tjyqeg",
				"2014款 1.3 手动 运动舒适型,tjyqeh","2015款 1.3 自动 舒适型,tjyqei","2015款 1.3 自动 尊贵型,tjyqej","2015款 1.3 自动 豪华型,tjyqek"
				],
		
		
		                  
		"威乐":["2005款 1.3 手动,tjyqfa","2006款 1.6 手动 豪华型,tjyqfb","2006款 1.5 自动 豪华型,tjyqfc","2006款 1.5 手动 豪华型,tjyqfd",
				"2008款 1.5 自动 豪华型,tjyqfe","2008款 1.5 手动 尊贵型,tjyqff","2008款 1.5 手动 豪华型,tjyqfg","2012款 1.5 手动 豪华型,tjyqfh"
				],
		
		
		                  
		
		"威姿":["2007款 1.3 自动 豪华型天窗版,tjyqga","2007款 1.3 自动 豪华型,tjyqgb","2007款 1.3 手动 豪华型,tjyqgc","2008款 1.3 手动 豪华型,tjyqgd",
				"2011款 1.3 手动 豪华型,tjyqge"
				],
                  	
		"威志":["2006款 三厢  1.5 手动,tjyqha","2006款 三厢  1.6 手动,tjyqhb","2007款 三厢  1.5 手动,tjyqhc","2007款 三厢  1.5 手动 豪华型 CNG油气混合,tjyqhd",
				"2007款 三厢  1.5 手动 豪华型,tjyqhe","2007款 三厢  1.6 手动 豪华型,tjyqhf","2007款 两厢  1.4 手动 豪华型,tjyqhg",
				"2007款 两厢  1.3 手动 豪华型,tjyqhh","2007款 两厢  1.3 手动 基本型,tjyqhi","2008款 三厢  1.3 手动 精英型,tjyqhj",
				"2008款 三厢  1.3 手动 基本型,tjyqhk","2008款 三厢  1.5 手动 豪华型,tjyqhl","2008款 两厢  1.3 手动 精英型,tjyqhm",
				"2008款 两厢  1.3 手动 豪华型,tjyqhn","2009款 两厢  1.5 手动 精英型,tjyqho","2009款 两厢  1.5 自动 旗舰型,tjyqhp",
				"2009款 两厢  1.3 手动 精英型,tjyqhq","2009款 三厢  1.5 手动 标准型,tjyqhr","2009款 三厢  1.5 手动 舒适型,tjyqhs",
				"2009款 三厢  1.5 手动 贺岁版,tjyqht","2009款 三厢  1.5 自动 旗舰型,tjyqhu","2009款 三厢  1.5 手动 精英型,tjyqhv",
				"2010款 三厢  1.5 手动 CNG油气混合,tjyqhw","2011款 三厢  1.5 手动 舒适型,tjyqhx","2011款 三厢  1.5 手动 精英型,tjyqhy",
				"2011款 三厢  1.5 手动 标准型,tjyqhz","2011款 三厢  1.5 手动 标准型铝轮,tjyqhaa","2011款 三厢  1.5 自动 旗舰型,tjyqhab"
				],	
		
		"威志V2":["2010款 1.3 自动 旗舰型,tjyqia","2010款 1.3 手动 豪华型,tjyqib","2010款 1.3 手动 舒适型,tjyqic",
				"2012款 1.3 自动 旗舰型,tjyqid","2012款 1.3 手动 豪华型,tjyqie"
				],
		
		"威志V5":["2015款 1.5 自动 精英型,tjyqja","2015款 1.5 自动 进取型,tjyqjb","2015款 1.5 自动 标准型,tjyqjc",
				"2014款 1.5 手动 精英型,tjyqjd","2014款 1.5 手动 进取型,tjyqje","2014款 1.5 手动 标准型,tjyqjf",
				"2013款 1.5 手动 精英型,tjyqjg","2013款 1.5 手动 进取型,tjyqjh","2013款 1.5 手动 标准型,tjyqji",
				"2012款 1.5 手动 精英型,tjyqjj","2012款 1.5 手动 进取型,tjyqjk","2012款 1.5 自动 旗舰型,tjyqjl",
				"2012款 1.5 手动 标准型,tjyqjm"
				],
		
		"骏派D60":["2015款 1.5 手动 技术型,tjyqka","2015款 1.5 手动 豪华导航版,tjyqkb","2015款 1.8 自动 尊贵型,tjyqkc",
				"2015款 1.8 自动 豪华型,tjyqkd","2015款 1.8 自动 舒适型,tjyqke","2015款 1.5 手动 尊贵型,tjyqkf",
				"2015款 1.5 手动 豪华型,tjyqkg","2015款 1.5 手动 舒适型,tjyqkh","2015款 1.5 手动 标准型,tjyqki"
				],
		
// "特斯拉"
		"MODEL S":["2014款 S60纯电动,teslaaa","2014款 P85纯电动,teslaab","2014款 S85纯电动,teslaac",			
					"2015款 P90D纯电动,teslaad","2015款 90D纯电动,teslaae","2015款 S90纯电动,teslaaf","2015款 S70纯电动,teslaag","2015款 S85纯电动,teslaah",
					"2015款 S60纯电动,teslaai","2015款 85D纯电动,teslaaj","2015款 60D纯电动,teslaak"
					],

//"威麟"

		"威麟H3":["2011款 2.0T 手动 豪华型,weilinaa"
				],
	
		"威麟H5":["2009款 2.0T 手动 14座,weilinba","2010款 2.0T 手动 豪华型14座,weilinbb","2010款 2.0T 手动 舒适型14座,weilinbc",			
			  "2011款 2.0T 手动 商务豪华型8座,weilinbd","2011款 2.0T 手动 商务舒适型9座,weilinbe"
					],	
			
		"威麟V5":["2007款 2.0 手动 豪华型,weilinca","2008款 1.8 手动 豪华型,weilincb","2009款 2.0 自动 豪华型,weilincc","2009款 1.9T 手动 舒适型 柴油,weilincd",
			"2009款 1.9T 手动 豪华型 柴油,weilince","2009款 2.0 自动 豪华型,weilincf","2009款 2.0 手动 舒适型,weilincg","2009款 2.0 手动 豪华型,weilinch",
			"2009款 1.8 手动 豪华型,weilinci","2009款 1.8 手动 舒适型,weilincj","2012款 2.0 手动 舒适型,weilinck","2012款 2.0 手动 豪华型,weilincl",
			"2012款 2.0 自动 豪华型,weilincm","2012款 1.9T 手动 舒适型 柴油,weilincn","2012款 1.9T 手动 豪华型 柴油,weilinco",
			"2012款 1.8 手动 舒适型,weilincp","2012款 1.8 手动 豪华型,weilincq"
			],	
			
			
		"威麟X5":["2010款 2.0T 手动 舒适型四驱,weilinda","2010款 2.0T 手动 精英型四驱,weilindb",
			"2011款 2.0T 手动 精英型四驱,weilindc","2011款 2.0T 手动 豪华型四驱,weilindd","2011款 2.0T 手动 舒适型四驱,weilinde",
			"2011款 2.0T 手动 舒适型后驱,weilindf","2011款 2.0T 手动 精英型后驱,weilindg","2011款 2.0T 手动 标准型后驱,weilindh"                
			],	
			
//"威兹曼"
			
		"威兹曼GT":["2008款 5.0 自动 MF5,weizimanaa","2008款 4.4T 手动 MF5,weizimanab",
			"2012款 4.4T 自动 MF4,weizimanac","2012款 4.0 自动 MF4,weizimanad","2012款 4.4T 自动 MF5,weizimanae"               
			],	
			
		"威兹曼敞篷车":["2009款 4.4T 手动 MF4,weizimanba","2009款 4.0 自动 MF4,weizimanbb",
			"2009款 4.4T 手动 MF5,weizimanbc","2009款 4.0 手动 MF4,weizimanbd","2009款 4.8 手动 MF4,weizimanbe",
			"2009款 5.0 自动 MF5,weizimanbf","2012款 4.4T 自动 MF4,weizimanbg","2012款 4.0 自动 MF4,weizimanbh","2012款 4.4T 自动 MF5,weizimanbi"                   
			],	
			
//"沃尔沃"	
		
		"S40":["2006款 2.5T 自动 T5,volvoaa","2006款 2.4 自动,volvoab","2007款 2.4 自动 智雅版,volvoac","2007款 2.5T 自动,volvoad",
				"2007款 2.4 自动 智尚版,volvoae","2008款 2.4 自动 R-Design,volvoaf",
				"2009款 2.0 自动 智尊版,volvoag","2009款 2.0 自动 智雅版,volvoah","2009款 2.0 自动 智尚版,volvoai","2009款 2.0 手动 标准版,volvoaj",
				"2009款 2.5T 自动 个性运动版R-Design,volvoak","2009款 2.4 自动 智雅限量版,volvoal",
				"2010款 2.0 手动 标准版,volvoam","2010款 2.0 自动 智尊版,volvoan","2010款 2.0 自动 智雅版,volvoao","2010款 2.0 自动 智尚版,volvoap",
				"2011款 2.0 自动 智尊版,volvoaq","2011款 2.0 自动 智雅版,volvoar","2011款 2.0 自动 智尚版,volvoas",
				"2012款 2.0 自动 尊享版,volvoat","2012款 2.0 自动 典雅版,volvoau"
				],	
			
			
		"S80L":["2009款 3.0T 自动 T6智雅版四驱,volvoba","2009款 3.0T 自动 T6智尊版四驱,volvobb","2009款 2.5T 自动 智尊版,volvobc",
				"2009款 2.5T 自动 智雅版,volvobd","2009款 2.5T 自动 智尚版,volvobe",
				"2010款 2.5T 自动 智尚版,volvobf","2010款 3.0T 自动 T6智雅版四驱,volvobg","2010款 3.0T 自动 T6智尊版四驱,volvobh",
				"2010款 2.5T 自动 智尊版,volvobi","2010款 2.5T 自动 智雅版,volvobj",                  
				"2011款 3.0T 自动 T6智尊版四驱,volvobk","2011款 3.0T 自动 T6智雅版四驱,volvobl","2011款 2.0T 自动 T4智尊版,volvobm",
				"2011款 2.0T 自动 T4智雅版,volvobn","2011款 2.0T 自动 T4智尚版,volvobo",
				"2012款 3.0T 自动 T6智尊版四驱,volvobp","2012款 3.0T 自动 T6智雅版四驱,volvobq","2012款 2.0T 自动 T5智尊版,volvobr",
				"2012款 2.0T 自动 T5智雅版,volvobs","2012款 2.0T 自动 T4智尚版,volvobt",
				"2014款 2.0T 自动 T5智逸版,volvobu","2014款 2.0T 自动 T5智雅版,volvobv","2014款 2.0T 自动 T5智尊行政版,volvobw",
				"2014款 3.0T 自动 T6智尊行政版四驱,volvobx",
				"2015款 2.0T 自动 T5智尊行政版,volvoby","2015款 2.0T 自动 T5智逸版,volvobz","2015款 2.0T 自动 T5智雅版,volvobaa"
				],	
			
		"C30":["2006款 2.4 自动 R-Design,volvofa","2008款 2.4 自动,volvofb","2010款 2.4 自动 个性运动版,volvofc","2010款 2.0 自动 个性运动版,volvofd",
				"2010款 2.0 自动 智雅版,volvofe","2010款 2.0 自动 智尚版,volvoff","2011款 2.0 自动 R-Design,volvofg","2011款 2.0 自动 智雅版,volvofh",
				"2011款 2.0 自动 智尚版,volvofi","2013款 2.0 自动 炫动版,volvofj","2012款 2.0 自动 Aktiv炫动版,volvofk",
				"2012款 2.0 自动 Aktiv乐动版,volvofl","2012款 2.0 自动 Aktiv,volvofm"
				],	
							
			
		"C70":["2006款 2.4 自动,volvoga","2006款 2.5T 自动 T5,volvogb","2007款 2.5T 自动 T5,volvogc","2008款 2.4 自动,volvogd",
				"2010款 2.5T 自动 T5,volvoge","2010款 2.4 自动,volvogf"
				],
		
		
		"S40(进口)":	["2007款 2.4 自动 ,volvoha"              
			],	
			
			
		"S60":["2005款 2.0T 自动 ,volvoia","2005款 2.4 自动 (140HP),volvoib","2005款 2.3T 自动 T5,volvoic","2005款 2.5T 自动 四驱,volvoid",
				"2005款 2.5T 自动 ,volvoie","2005款 2.4 自动 (170HP),volvoif","2008款 2.5T 自动 四驱,volvoig","2008款 2.4T 自动 T5四驱,volvoih",
				"2011款 3.0T 自动 T6智尊版四驱,volvoii","2011款 3.0T 自动 T6智雅版四驱,volvoij","2011款 2.0T 自动 T5智尊版,volvoik",
				"2011款 2.0T 自动 T5智雅版,volvoil","2011款 2.0T 自动 T5舒适版,volvoim","2011款 2.0T 自动 T5智尚版,volvoin",
				"2012款 1.6T 自动 智雅版Drive E,volvoio","2012款 1.6T 自动 智尚版Drive E,volvoip","2012款 1.6T 自动 舒适版Drive E,volvoiq",
				"2012款 2.0T 自动 T5智尚版,volvoir","2012款 2.0T 自动 T5舒适版,volvois",
				"2013款 2.0T 自动 进取版,volvoit","2013款 2.0T 自动 T5智尚版,volvoiu","2013款 2.0T 自动 T5舒适版,volvoiv","2013款 2.0T 自动 T5智雅版,volvoiw",
				"2013款 2.0T 自动 T5智尊版,volvoix","2013款 3.0T 自动 T6智尊版四驱,volvoiy",
				"2014款 2.0T 自动 T5个性运动版改款,volvoiz","2014款 3.0T 自动 T6个性运动版四驱,volvoiaa","2014款 2.0T 自动 T5个性运动版,volvoiab",
				"2014款 2.0T 自动 T5智雅版,volvoiac","2014款 2.0T 自动 T5智逸版,volvoiad","2015款 3.0T 自动 T6个性运动版四驱,volvoiae",
				"2015款 2.0T 自动 T5个性运动版,volvoiaf"
				],
  	
			
			
		"S80":["2006款 3.0 自动 T6四驱,volvoka","2006款 4.4 自动 四驱,volvokb","2006款 3.2 自动,volvokc","2007款 2.5T 自动 智雅版,volvokd","2008款 2.5T 自动 智尚版,volvoke",
			"2008款 3.0T 自动 T6智雅版四驱,volvokf","2008款 3.0T 自动 T6智尊版四驱,volvokg","2008款 2.5T 自动 智尊版,volvokh","2008款 2.5T 自动 智雅版,volvoki",
			"2009款 4.4 自动 四驱,volvokj","2009款 3.2 自动,volvokk","2009款 3.0T 自动 T6四驱,volvokl"
			],
	
			
			
		"V40":["2013款 2.0T 自动 T5智尊版,volvoma","2013款 2.0T 自动 智雅版,volvomb","2013款 2.0T 自动 智逸版,volvomc","2013款 2.0T 自动 智尚版,volvomd",
				"2014款 2.0T 自动 T5 Cross Country智尊版四驱,volvome","2014款 2.0T 自动 T5 Cross Country智雅版四驱,volvomf",
				"2014款 2.0T 自动 Cross Country智逸版,volvomg","2014款 2.0T 自动 T5智尊版,volvomh","2014款 2.0T 自动 智雅版,volvomi",
				"2014款 2.0T 自动 智逸版,volvomj","2014款 2.0T 自动 智尚版,volvomk",                  
				"2015款 2.0T 自动 T5 Cross Country智尊版四驱,volvoml","2015款 2.0T 自动 T5 Cross Country智雅版四驱,volvomm",
				"2015款 2.0T 自动 Cross Country智逸版,volvomn","2015款 2.0T 自动 T5智尊个性运动版,volvomo","2015款 1.6T 自动 智雅个性运动版,volvomp",
				"2015款 1.6T 自动 智雅版,volvomq","2015款 1.6T 自动 智逸版,volvomr","2015款 1.6T 自动 智尚版,volvoms",
				"2016款 2.0T 自动 T4个性运动版,volvomt","2016款 2.0T 自动 T4智雅版,volvomu","2016款 1.5T 自动 T3智逸版,volvomv","2016款 1.5T 自动 T3智尚版,volvomw"
				],
				   	
			
			
		"V60":["2012款 3.0T 自动 T6个性运动版四驱,volvona","2012款 3.0T 自动 T6运动版四驱,volvonb",
			"2013款 2.0T 自动 T5智尊版,volvonc","2013款 2.0T 自动 T5智雅版,volvond","2013款 2.0T 自动 T5舒适版,volvone","2013款 2.0T 自动 T5智尚版,volvonf",                  
			"2014款 2.0T 自动 T5个性运动版改款,volvong","2014款 2.0T 自动 T5智雅版改款,volvonh","2014款 2.0T 自动 T5智逸版改款,volvoni",
			"2014款 2.0T 自动 T5 Fashion Deive E,volvonj","2014款 3.0T 自动 T6个性运动版四驱,volvonk","2014款 2.0T 自动 T5个性运动版,volvonl",
			"2015款 2.0T 自动 T5智逸个性运动版,volvonm","2015款 2.0T 自动 T5智雅版,volvonn","2015款 2.0T 自动 T5智逸版,volvono",
			"2015款 3.0T 自动 T6个性运动版四驱,volvonp","2015款 2.0T 自动 T5智雅个性运动版,volvonq","2016款 2.5T 自动 T6 Cross Country  四驱,volvonr"
			],	
			
			
		"XC60(进口)":["2009款 3.0T 自动 T6智尊版四驱,volvooa","2009款 3.0T 自动 T6智雅版四驱,volvoob","2010款 3.0T 自动 T6舒适版四驱,volvooc",
				"2010款 2.0T 自动 智尊版前驱,volvood","2010款 2.0T 自动 智雅版前驱,volvooe","2010款 2.0T 自动 个性运动版前驱,volvoof",
				"2010款 2.0T 自动 舒适版前驱,volvoog","2010款 2.0T 自动 T5海洋竞赛版前驱,volvooh","2010款 3.0T 自动 T6个性运动版四驱,volvooi",
				"2011款 3.0T 自动 T6个性运动版四驱,volvooj","2011款 3.0T 自动 T6智雅版四驱,volvook","2011款 3.0T 自动 T6智尊版四驱,volvool",
				"2011款 2.0T 自动 智尊版前驱,volvoom","2011款 2.0T 自动 个性运动版前驱,volvoon","2011款 2.0T 自动 智雅版前驱,volvooo",
				"2011款 2.0T 自动 舒适版前驱,volvoop","2011款 3.0T 自动 T6智尊版四驱,volvooq",
				"2012款 2.0T 自动 T5环球帆船赛纪念版前驱,volvoor","2012款 3.0T 自动 T6舒适版四驱,volvoos","2012款 2.0T 自动 T5海洋竞赛版前驱,volvoot",
				"2012款 2.0T 自动 个性运动版前驱,volvoou","2012款 2.0T 自动 智尊版前驱,volvoov","2012款 2.0T 自动 智雅版前驱,volvoow",
				"2012款 2.0T 自动 舒适版前驱,volvoox","2012款 3.0T 自动 T6个性运动版四驱,volvooy","2012款 3.0T 自动 T6智雅版四驱,volvooz",
				"2013款 3.0T 自动 T6舒适版四驱,volvooaa","2013款 2.0T 自动 T5智尊版前驱,volvooab","2013款 2.0T 自动 T5智雅版前驱,volvooac",
				"2013款 2.0T 自动 T5舒适版前驱,volvooad","2013款 2.0T 自动 T5智尚版前驱,volvooae","2013款 3.0T 自动 T6智雅版四驱,volvooaf",
				"2013款 3.0T 自动 T6个性运动版四驱,volvooag",
				"2014款 2.0T 自动 T5个性运动版改款,volvooah","2014款 2.0T 自动 T5智雅版改款,volvooai","2014款 2.0T 自动 T5智逸版改款,volvooaj",
				"2014款 2.0T 自动 T5 Fashiong Drive E,volvooak","2014款 3.0T 自动 T6个性运动版,volvooal","2014款 2.0T 自动 T5个性运动版,volvooam",
				"2014款 3.0T 自动 T6智雅版四驱,volvooan","2014款 3.0T 自动 T6智逸版四驱,volvooao","2014款 3.0T 自动 T6智尚版四驱,volvooap",
				"2014款 2.0T 自动 T5智雅版前驱,volvooaq","2014款 2.0T 自动 T5智逸版前驱,volvooar","2014款 2.0T 自动 T5智尚版前驱,volvooas"
				],
				
		"XC90":["2005款 4.4 自动,volvopa","2006款 3.2 自动,volvopb","2006款 2.5T 自动,volvopc","2006款 4.4 自动 行政版5座,volvopd","2006款 4.4 自动 行政版7座,volvope",
				"2007款 2.5T 自动,volvopf","2007款 2.5T 手动,volvopg","2007款 3.2 自动,volvoph","2007款 4.4 自动,volvopi","2007款 3.2 自动 运动版,volvopj",
				"2008款 4.4 自动 Inscription典藏版,volvopk","2009款 2.5T 自动,volvopl","2009款 3.2 自动,volvopm","2009款 2.5T 自动 运动版,volvopn",
				"2009款 2.5T 自动 航海版,volvopo","2009款 4.4 自动 行政版,volvopp","2009款 3.2 自动 运动版,volvopq","2010款 2.5T 自动,volvopr",
				"2010款 3.2 自动,volvops","2010款 4.4 自动 行政版,volvopt","2010款 3.2 自动 运动版,volvopu","2010款 2.5T 自动 运动版,volvopv",
				"2011款 2.5T 自动,volvopw","2011款 3.2 自动,volvopx","2011款 3.2 自动 北欧行政版,volvopy","2011款 3.2 自动 R-Design,volvopz",
				"2011款 2.5T 自动 T5北欧行政版,volvopaa","2011款 2.5T 自动 T5北欧豪华版,volvopab","2011款 2.5T 自动 T5 R-Design北欧个性运动版,volvopac",
				"2011款 3.2 自动 R-Design北欧个性运动版,volvopad","2011款 2.5T 自动 R-Design,volvopae",
				"2012款 3.2 自动 北欧个性运动版,volvopaf","2012款 3.2 自动 北欧行政版,volvopag","2012款 2.5T 自动 北欧个性运动版,volvopah",
				"2012款 2.5T 自动 北欧行政版,volvopai","2012款 2.5T 自动 北欧豪华版,volvopaj","2012款 2.5T 自动 T5北欧行政版改款,volvopak",
				"2012款 2.5T 自动 T5北欧豪华版改款,volvopal",                  
				"2013款 2.5T 自动 T5行政版,volvopam","2013款 2.5T 自动 T5豪华版,volvopan","2013款 2.5T 自动 T5行政升级版,volvopao",
				"2013款 2.5T 自动 T5豪华升级版,volvopap","2015款 2.0T 自动 T6智尊版5座,volvopaq","2015款 2.0T 自动 T6智逸版5座,volvopar",
				"2015款 2.0T 自动 T6智雅版5座,volvopas","2015款 2.0T 自动 T6智尊版7座,volvopat",
				"2015款 2.0T 自动 T6智雅版7座,volvopau","2015款 2.0T 自动 T6智逸版7座,volvopav","2015款 2.0T 自动 T6限量版7座,volvopaw"
				],	
		
		"S60L":["2014款 2.0T 自动 T5智越版,volvoqa","2014款 2.0T 自动 T5智驭版,volvoqb","2014款 2.0T 自动 智远版,volvoqc","2014款 2.0T 自动 智进版,volvoqd",
				"2014款 2.0T 自动 智行版,volvoqe","2015款 2.0T 自动 T6智越版E驱混动,volvoqf","2015款 2.0T 自动 T6智驭版E驱混动,volvoqg",
				"2015款 2.0T 自动 T5智越版,volvoqh","2015款 2.0T 自动 T5智驭版,volvoqi","2015款 2.0T 自动 T5 Comfort智远版,volvoqj",
				"2015款 2.0T 自动 智远版,volvoqk","2015款 2.0T 自动 智进版,volvoql","2015款 2.0T 自动 智行版,volvoqm",
				"2016款 2.0T 自动 T5智越版,volvoqn","2016款 2.0T 自动 T5智驭版,volvoqo","2016款 2.0T 自动 T4智远版,volvoqp","2016款 1.5T 自动 T3智行版,volvoqq"
				],
		
		"XC Classic":["2014款 2.5T 自动 T5行政版四驱,volvora","2014款 2.5T 自动 T5豪华版四驱,volvorb"],
		
		"XC60":["2015款 2.0T 自动 T5智远版Drive E,volvosa","2015款 2.5T 自动 T6智越版四驱,volvosb","2015款 2.0T 自动 T5智驭版四驱,volvosc",
				"2015款 2.0T 自动 T5智远版四驱,volvosd","2015款 2.0T 自动 T5智进版Drive E,volvose","2015款 2.0T 自动 T5智行版Drive E,volvosf"
				],
				   

// "新凯"		
			
		"凯胜":["2011款 2.4 手动 一代标准型,xinkaiaa","2013款 2.4 手动 二代标准型,xinkaiab","2013款 2.0 手动 二代标准型,xinkaiac"],
			
			
		"新凯之星":["2005款 2.8T 手动 后驱 柴油,xinkaiba","2006款 2.2 手动 后驱,xinkaibb",
					"2006款 2.8T 手动 后驱 柴油,xinkaibc","2008款 2.2 手动 后驱,xinkaibd",
					"2009款 2.8T 手动 后驱 柴油,xinkaibe"
				],
			
		"新凯靓星":["2006款 2.2 手动 后驱,xinkaica"],	
			
			
		"都市之星":["2005款 2.2 手动 后驱,xinkaida","2006款 2.8T 手动 后驱 柴油,xinkaidb",
					"2006款 2.2 手动 后驱,xinkaidc","2008款 2.2 手动 后驱,xinkaidd",
					"2008款 2.8T 手动 后驱 柴油,xinkaide","2009款 2.8T 手动 后驱 柴油,xinkaidf"
				],	
			
			
//  "西雅特"
			
		"伊比飒":["2013款 1.2T 自动 Style,seataa","2013款 1.4T 自动 FR,seatab","2013款 1.4T 自动 FR旅行版,seatac"
				],
			
		"利昂":["2012款 1.8T 自动 FR,seatba","2012款 2.0T 自动 Cupra,seatbb"
				],
			
		"欧悦博":["2013款 1.8T 自动 豪华版,seatca","2013款 1.8T 自动 豪华导航版,seatcb","2013款 2.0T 自动 至尊版,seatcc"
				],	
			
			


//"现代"

			"Trajet":["2005款 2.7 自动,hyundaiaa"],
			
			"劳恩斯":["2008款 4.6 自动,hyundaica","2008款 3.8 自动 顶级版,hyundaicb","2008款 3.3 自动 舒适版,hyundaicc","2008款 3.3 自动 豪华版,hyundaicd",
					"2008款 3.3 自动 顶级版,hyundaice","2012款 3.3 自动 旗舰版,hyundaicf","2012款 3.3 自动 尊贵版,hyundaicg","2012款 3.0 自动 旗舰版,hyundaich",
					"2012款 3.0 自动 尊贵版,hyundaici","2012款 3.0 自动 豪华版,hyundaicj"
					],
			
			"劳恩斯酷派":["2009款 3.8 手动 豪华版,hyundaida","2009款 3.8 自动 旗舰版,hyundaidb","2009款 2.0T 手动 酷动版,hyundaidc","2009款 2.0T 自动 靓雅版,hyundaidd",
						"2009款 2.0T 自动 豪华版,hyundaide","2012款 3.8 自动 Brembo版,hyundaidf","2012款 2.0T 自动 Brembo版,hyundaidg","2012款 2.0T 自动 豪华版,hyundaidh","2012款 2.0T 自动 靓雅版,hyundaidi",
						"2012款 2.0T 手动 酷动版,hyundaidj"
						],

			
			"捷恩斯":["2015款 3.3 自动 旗舰版四驱,hyundaiga","2015款 3.3 自动 旗舰版后驱,hyundaigb","2015款 3.3 自动 豪华版四驱,hyundaigc","2015款 3.3 自动 豪华版后驱,hyundaigd",
					"2015款 3.0 自动 旗舰版四驱,hyundaige","2015款 3.0 自动 旗舰版后驱,hyundaigf","2015款 3.0 自动 豪华版四驱,hyundaigg","2015款 3.0 自动 豪华版后驱,hyundaigh",
					"2015款 3.0 自动 精英版四驱,hyundaigi","2015款 3.0 自动 精英版后驱,hyundaigj","2015款 3.0 自动 商务版四驱,hyundaigk","2015款 3.0 自动 商务版后驱,hyundaigl"
					],
			
			"格锐":["2013款 3.3 自动 旗舰版7座四驱,hyundaiha","2013款 3.3 自动 豪华版7座四驱,hyundaihb","2013款 3.0 自动 旗舰版7座,hyundaihc",
					"2013款 3.0 自动 旗舰版6座,hyundaihd","2013款 3.0 自动 豪华版7座,hyundaihe","2013款 3.0 自动 豪华版6座,hyundaihf",
					"2013款 3.0 自动 舒适版7座,hyundaihg","2013款 3.0 自动 舒适版6座,hyundaihh","2013款 2.2T 自动 旗舰版7座 柴油,hyundaihi",
					"2013款 2.2T 自动 旗舰版6座 柴油,hyundaihj","2013款 2.2T 自动 豪华版7座 柴油,hyundaihk","2013款 2.2T 自动 豪华版6座 柴油,hyundaihl",
					"2013款 2.2T 自动 舒适版7座 柴油,hyundaihm","2013款 2.2T 自动 舒适版6座 柴油,hyundaihn",
					"2015款 2.2T 自动 智领限量版7座 柴油,hyundaiho","2015款 2.0T 自动 棕色内饰旗舰版7座,hyundaihp","2015款 2.0T 自动 旗舰版7座,hyundaihq",
					"2015款 2.0T 自动 豪华版7座,hyundaihr","2015款 2.0T 自动 舒适版7座,hyundaihs","2015款 2.0T 自动 领先版7座,hyundaiht"
					],
			
			"维拉克斯":["2007款 3.8 自动 豪华导航版,hyundaila","2007款 3.8 自动 豪华版,hyundailb","2007款 3.8 自动 舒适版,hyundailc",
						"2012款 3.8 自动 舒适版,hyundaild","2012款 3.8 自动 豪华版,hyundaile"
						],
			
			"美佳":["2006款 1.6 自动 DVD,hyundaima"],
			
			"新胜达":["2006款 2.7 自动 舒适型7座四驱,hyundaina","2006款 2.7 自动 顶级型7座四驱,hyundainb","2007款 2.4 自动 至尊型7座四驱,hyundainc",
					"2007款 2.4 自动 豪华型7座四驱,hyundaind","2007款 2.4 自动 舒适型7座四驱,hyundaine","2008款 2.7 自动 豪华型7座四驱,hyundainf",
					"2010款 2.4 自动 至尊版七座四驱,hyundaing","2010款 2.4 自动 豪华版七座四驱,hyundainh","2010款 2.4 自动 舒适版七座四驱,hyundaini",
					"2011款 2.4 自动 至尊型7座四驱,hyundainj","2011款 2.4 自动 豪华型7座四驱,hyundaink","2011款 2.4 自动 舒适型7座四驱,hyundainl",
					"2011款 2.4 自动 至尊型7座前驱,hyundainm","2011款 2.4 自动 至尊型5座前驱,hyundainn","2011款 2.4 自动 舒适型7座前驱,hyundaino",
					"2011款 2.4 自动 舒适型5座前驱,hyundainp","2011款 2.4 自动 豪华型7座前驱,hyundainq","2011款 2.4 自动 豪华型5座前驱,hyundainr",
					"2013款 3.0 自动 尊享版改款四驱 欧Ⅴ,hyundains","2013款 3.0 自动 舒适版改款四驱 欧Ⅴ,hyundaint","2013款 3.0 自动 舒适导航版改款四驱 欧Ⅴ,hyundainu",
					"2013款 3.0 自动 旗舰版改款四驱 欧Ⅴ,hyundainv","2013款 3.0 自动 豪华版改款四驱 欧Ⅴ,hyundainw","2013款 3.0 自动 尊享版7座四驱 欧Ⅴ,hyundainx",
					"2013款 3.0 自动 舒适导航版7座四驱 欧Ⅴ,hyundainy","2013款 3.0 自动 舒适版7座四驱 欧Ⅴ,hyundainz","2013款 3.0 自动 旗舰版7座四驱 欧Ⅴ,hyundainaa",
					"2013款 3.0 自动 豪华版7座四驱 欧Ⅴ,hyundainab","2013款 3.0 自动 尊享版改款四驱 欧Ⅳ,hyundainac","2013款 3.0 自动 舒适导航版改款四驱 欧Ⅳ,hyundainad",
					"2013款 3.0 自动 舒适版改款四驱 欧Ⅳ,hyundainae","2013款 3.0 自动 旗舰版改款四驱 欧Ⅳ,hyundainaf","2013款 3.0 自动 豪华版改款四驱 欧Ⅳ,hyundainag",
					"2013款 3.0 自动 舒适导航版7座四驱 欧Ⅳ,hyundainah","2013款 3.0 自动 舒适导航版7座前驱,hyundainai","2013款 3.0 自动 舒适版7座前驱,hyundainaj",
					"2013款 3.0 自动 尊享版7座四驱 欧Ⅳ,hyundainak","2013款 3.0 自动 舒适导航版5座前驱,hyundainal","2013款 3.0 自动 舒适版7座四驱 欧Ⅳ,hyundainam",
					"2013款 3.0 自动 舒适版5座前驱,hyundainan","2013款 3.0 自动 豪华版7座四驱 欧Ⅳ,hyundainao","2013款 3.0 自动 旗舰版7座四驱 欧Ⅳ,hyundainap",
					"2014款 2.2T 自动 旗舰版7座四驱 柴油,hyundainaq","2014款 2.2T 自动 尊享版7座四驱 柴油,hyundainar","2014款 2.2T 自动 豪华版7座四驱 柴油,hyundainas",
					"2014款 2.2T 自动 舒适导航版7座四驱 柴油,hyundainat","2014款 2.2T 自动 舒适版7座四驱 柴油,hyundainau"
					],
			
			"辉翼":["2011款 2.4 自动 舒适版,hyundaioa","2011款 2.4 自动 领航版,hyundaiob","2011款 2.4 自动 豪华版,hyundaioc"],
			
			"酷派":["2007款 2.7 自动,hyundaiqa","2007款 2.0 自动,hyundaiqb","2004款 2.7 自动,hyundaiqc"],
			
			"雅尊":["2006款 3.3 自动 舒适版,hyundaira","2006款 3.3 自动 顶级版,hyundairb","2006款 2.7 自动 顶级版,hyundairc","2006款 2.4 自动 舒适版,hyundaird",
					"2006款 2.7 自动 豪华版,hyundaire","2006款 3.3 自动 豪华版,hyundairf","2011款 2.4 自动 豪华版,hyundairg","2011款 2.4 自动 旗舰版,hyundairh",
					"2011款 2.4 自动 舒适版,hyundairi","2011款 2.4 自动 尊贵版,hyundairj","2011款 3.0 自动 旗舰版,hyundairk","2011款 3.0 自动 尊贵版,hyundairl"
					],
					
			"胜达":["2013款 2.0T 自动 时尚型前驱,hyundaiaga","2013款 2.0T 自动 顶级型7座四驱,hyundaiagb","2013款 2.0T 自动 尊贵型四驱,hyundaiagc",
					"2013款 2.0T 自动 尊贵型前驱,hyundaiagd","2013款 2.0T 自动 时尚型四驱,hyundaiage","2013款 2.4 自动 尊贵型四驱,hyundaiagf",
					"2013款 2.4 自动 智能型四驱,hyundaiagg","2013款 2.4 自动 智能型前驱,hyundaiagh","2013款 2.0T 自动 顶级型四驱,hyundaiagi",
					"2013款 2.0T 自动 尊贵型四驱,hyundaiagj","2013款 2.0T 自动 时尚型四驱,hyundaiagk","2013款 2.4 自动 尊贵型四驱,hyundaiagl",
					"2013款 2.4 自动 智能型四驱,hyundaiagm","2013款 2.4 自动 舒适型四驱,hyundaiagn","2013款 2.4 自动 舒适型前驱,hyundaiago",
					"2013款 2.4 手动 舒适型前驱,hyundaiagp","2015款 2.0T 自动 智能型四驱,hyundaiagq","2015款 2.0T 自动 智能型前驱,hyundaiagr",
					"2015款 2.0T 自动 舒适型前驱,hyundaiags","2015款 2.4 自动 智能型四驱,hyundaiagt","2015款 2.4 自动 智能型前驱,hyundaiagu",
					"2015款 2.4 自动 舒适型前驱,hyundaiagv","2015款 2.4 手动 舒适型前驱,hyundaiagw",
					"2015款 2.0T 自动 顶级型四驱,hyundaiagx","2015款 2.0T 自动 智能型7座四驱,hyundaiagy"
					],
					
			"雅科仕"	:["2009款 5.0 自动 加长版,hyundaisa","2009款 4.6 自动 尊享版,hyundaisb","2009款 4.6 自动 豪华版,hyundaisc","2009款 3.8 自动 尊享版,hyundaisd",
					"2009款 3.8 自动 豪华版,hyundaise","2012款 5.0 自动 加长尊贵版,hyundaisf","2012款 5.0 自动 加长旗舰版,hyundaisg",
					"2012款 3.8 自动 加长尊贵版,hyundaish","2012款 3.8 自动 加长旗舰版,hyundaisi","2012款 5.0 自动 豪华版,hyundaisj",
					"2012款 5.0 自动 尊享版,hyundaisk","2012款 3.8 自动 尊享版,hyundaisl","2012款 3.8 自动 豪华版,hyundaism",
					"2014款 5.0 自动 GDi加长旗舰版,hyundaisn","2014款 5.0 自动 GDi加长尊享版,hyundaiso","2014款 3.8 自动 GDi加长旗舰版,hyundaisp",
					"2014款 3.8 自动 GDi加长尊享版,hyundaisq","2014款 5.0 自动 GDi尊享版,hyundaisr","2014款 5.0 自动 GDi豪华版,hyundaiss",
					"2014款 3.8 自动 GDi尊享版,hyundaist","2014款 3.8 自动 GDi豪华版,hyundaisu"
					],
			
			"飞思":["2012款 1.6 自动 豪华版,hyundaita","2012款 1.6T 自动 旗舰版,hyundaitb","2012款 1.6T 自动 豪华版,hyundaitc","2012款 1.6T 自动 尊享版,hyundaitd",
					"2012款 1.6T 手动 舒适版,hyundaite",
					"2011款 1.6 手动 舒适版,hyundaitf","2011款 1.6 手动 豪华版,hyundaitg","2011款 1.6 自动 尊享版,hyundaith","2011款 1.6 自动 旗舰版,hyundaiti",
					"2015款 1.6T 自动 旗舰版,hyundaitj","2015款 1.6T 自动 豪华版,hyundaitk","2015款 1.6T 自动 尊享版,hyundaitl","2015款 1.6T 手动 舒适版,hyundaitm"
					],
	
			
			"i30":["2009款 2.0 自动 尊享型,hyundaiua","2009款 2.0 手动 尊享型,hyundaiub","2009款 1.6 自动 豪享型,hyundaiuc","2009款 1.6 手动 豪享型,hyundaiud",
					"2009款 1.6 自动 劲享型,hyundaiue","2009款 1.6 手动 劲享型,hyundaiuf","2009款 1.6 自动 舒享型,hyundaiug","2009款 1.6 手动 舒享型,hyundaiuh"
					],
						
			
			"ix25":["2015款 2.0 自动 DLX尊贵型四驱,hyundaiva","2015款 2.0 自动 GLX领先型四驱,hyundaivb","2015款 2.0 自动 GLS智能型前驱,hyundaivc",
					"2015款 1.6 自动 DLX尊贵型前驱,hyundaivd","2015款 1.6 自动 GLS智能型前驱,hyundaive","2015款 1.6 自动 GS时尚型前驱,hyundaivf",
					"2015款 1.6 手动 GS时尚型前驱,hyundaivg"
					],	
			
			"ix35":["2010款 2.4 自动 GLS领航版四驱,hyundaiwa","2010款 2.0 手动 GL新锐版前驱,hyundaiwb","2010款 2.4 自动 GLS尊贵导航版前驱,hyundaiwc",
					"2010款 2.0 自动 GLS尊贵导航版四驱,hyundaiwd","2010款 2.0 自动 GLS精英天窗版前驱,hyundaiwe","2010款 2.4 自动 GLS尊贵版前驱,hyundaiwf",
					"2010款 2.0 自动 GLS尊贵版四驱,hyundaiwg","2010款 2.0 自动 GLS精英版前驱,hyundaiwh","2010款 2.0 自动 GL新锐版前驱,hyundaiwi",                  
					"2012款 2.4 自动 GLS领航版四驱,hyundaiwj","2012款 2.4 自动 GLS尊贵版前驱,hyundaiwk","2012款 2.0 手动 GL新锐版前驱,hyundaiwl",
					"2012款 2.0 自动 GL尊贵型四驱,hyundaiwm","2012款 2.0 自动 GL新锐版前驱,hyundaiwn","2012款 2.0 自动 GLS精英版前驱,hyundaiwo",
					"2012款 2.4 自动 GLS尊贵导航版前驱,hyundaiwp",
					"2013款 2.4 自动 GLX领先型四驱,hyundaiwq","2013款 2.0 自动 GLX领先型前驱,hyundaiwr","2013款 2.4 自动 GLX领先型前驱,hyundaiws",
					"2013款 2.4 自动 GLS智能型四驱,hyundaiwt","2013款 2.0 手动 GL舒适型前驱,hyundaiwu","2013款 2.0 自动 GL舒适型四驱,hyundaiwv",
					"2013款 2.0 自动 GL舒适型前驱,hyundaiww","2013款 2.0 自动 GLS智能型四驱,hyundaiwx","2013款 2.0 自动 GLS智能型前驱,hyundaiwy",
					"2015款 2.4 自动 GLX领先型四驱,hyundaiwz","2015款 2.4 自动 GLX领先型前驱,hyundaiwaa","2015款 2.0 自动 GLX领先型前驱,hyundaiwab",
					"2015款 2.0 自动 GLS智能型四驱,hyundaiwac","2015款 2.0 自动 GLS智能型前驱,hyundaiwad","2015款 2.0 自动 GL舒适型前驱,hyundaiwae",
					"2015款 2.0 手动 GL舒适型前驱,hyundaiwaf"
					],	
			
			"伊兰特"	:["2005款 三厢 1.6 手动 豪华型GLS,hyundaixa","2005款 三厢 1.6 手动 标准型GL,hyundaixb","2005款 三厢 1.6 自动 豪华型GLS,hyundaixc",
					"2005款 三厢 1.8 手动 豪华型GLS,hyundaixd","2005款 三厢 1.8 自动 豪华型GLS,hyundaixe","2005款 三厢 1.6 自动 标准型GL,hyundaixf",
					"2006款 三厢 1.6 自动 标准型GL,hyundaixg","2006款 三厢 1.6 手动 标准型GL,hyundaixh","2006款 三厢 1.6 手动 豪华型GLS,hyundaixi",
					"2006款 三厢 1.6 自动 豪华型GLS,hyundaixj","2006款 三厢 1.8 手动 豪华型GLS,hyundaixk","2006款 三厢 1.8 自动 豪华型GLS,hyundaixl",
					"2006款 三厢 1.6 自动 舒适型GL,hyundaixm","2006款 两厢 1.6 自动 豪华运动型,hyundaixn","2006款 两厢 1.6 手动 豪华运动型,hyundaixo",
					"2007款 三厢 1.8 手动 豪华型GLS,hyundaixp","2007款 三厢 1.8 自动 豪华型GLS,hyundaixq","2007款 三厢 1.6 手动 标准型GL,hyundaixr",
					"2007款 三厢 1.6 手动 舒适型GL,hyundaixs","2007款 三厢 1.6 自动 舒适型GL,hyundaixt","2007款 三厢 1.6 自动 豪华型GLS,hyundaixu",
					"2007款 三厢 1.6 手动 豪华型GLS,hyundaixv","2007款 两厢 1.6 自动 豪华运动型,hyundaixw","2007款 两厢 1.6 手动 豪华运动型,hyundaixx",
					"2008款 三厢 1.6 手动 豪华贵雅版GL,hyundaixy","2008款 三厢 1.6 自动 贵雅版GL,hyundaixz","2008款 三厢 1.6 自动 清新版GL,hyundaixaa",
					"2008款 三厢 1.6 手动 贵雅版GL,hyundaixab","2008款 三厢 1.6 手动 CNG油气混合,hyundaixac","2008款 三厢 1.6 手动 LPG油气混合,hyundaixad",
					"2008款 三厢 1.6 手动 清新版GL,hyundaixae","2008款 三厢 1.6 手动 优全版GL,hyundaixaf","2008款 三厢 1.6 自动 优全版GL,hyundaixag",
					"2008款 两厢 1.6 自动 豪华运动型,hyundaixah","2008款 两厢 1.6 手动 豪华运动型,hyundaixai",
					"2010款 三厢 1.6 手动 舒适型真皮座椅天窗版GL,hyundaixaj","2010款 三厢 1.6 手动 舒适型天窗版GL,hyundaixak","2010款 三厢 1.6 手动 舒适型GL,hyundaixal",
					"2010款 三厢 1.6 自动 舒适型GL,hyundaixam","2010款 三厢 1.6 手动 CNG油气混合,hyundaixan",
					"2011款 三厢 1.6 自动 舒适型GL油气混合 ,hyundaixao","2011款 三厢 1.6 手动 舒适型真皮座椅天窗版GL,hyundaixap","2011款 三厢 1.6 手动 舒适型天窗版GL,hyundaixaq",
					"2011款 三厢 1.6 手动 舒适型GL,hyundaixas","2011款 三厢 1.6 自动 舒适型真皮座椅天窗版GL,hyundaixat","2011款 三厢 1.6 自动 舒适型天窗版GL,hyundaixau",
					"2011款 三厢 1.6 自动 舒适型GL,hyundaixav","2011款 三厢 1.6 手动 CNG油气混合,hyundaixaw",
					"2013款 三厢 1.6 手动 CNG油气混合,hyundaixax"
					],
	
			
			"名图":["2014款 1.8 手动 舒适型,hyundaiya","2014款 2.0 自动 旗舰型,hyundaiyb","2014款 2.0 自动 至尊型,hyundaiyc","2014款 1.8 自动 尊贵型,hyundaiyd",
					"2014款 1.8 自动 智能型,hyundaiye","2014款 1.8 自动 舒适型,hyundaiyf"
					],
			
			"名驭":["2009款 1.8 手动 舒适型油气混合,hyundaiza","2009款 1.8 手动 豪华型油气混合,hyundaizb","2009款 1.8 手动 舒适型,hyundaizc","2009款 1.8 手动 豪华型,hyundaizd",
					"2009款 2.0 手动 舒适型,hyundaize","2009款 2.0 手动 豪华型,hyundaizf","2009款 2.0 自动 尊贵型,hyundaizg","2009款 2.0 自动 舒适型,hyundaizh",
					"2009款 2.0 自动 导航型,hyundaizi","2009款 1.8 手动 舒适型,hyundaizj","2009款 1.8 手动 豪华型,hyundaizk","2009款 1.8 自动 尊贵型,hyundaizl",
					"2009款 1.8 自动 舒适型,hyundaizm","2009款 1.8 手动 舒适型油气混合,hyundaizn","2009款 1.8 手动 豪华型油气混合,hyundaizo",
					"2009款 2.0 手动 舒适型油气混合,hyundaizp","2009款 2.0 手动 豪华型油气混合,hyundaizq"
					],
			
			"御翔":["2006款 2.0 自动 豪华型,hyundaiaaa","2006款 2.4 自动 顶级型,hyundaiaab","2006款 2.4 自动 舒适型,hyundaiaac","2006款 2.4 自动 豪华型,hyundaiaad",
					"2007款 2.0 手动 舒适型,hyundaiaae","2007款 2.4 自动 顶级型,hyundaiaaf","2007款 2.4 自动 豪华型,hyundaiaag","2007款 2.0 自动 豪华型,hyundaiaah",
					"2007款 2.0 自动 尊贵型,hyundaiaai","2008款 2.0 手动 舒适型,hyundaiaaj","2008款 2.4 自动 豪华型,hyundaiaak","2008款 2.4 自动 顶级型,hyundaiaal",
					"2008款 2.0 自动 尊贵型,hyundaiaam","2008款 2.0 自动 豪华型,hyundaiaan","2009款 2.0 手动 舒适型,hyundaiaao"
					],	
			
			"悦动":["2007款 1.6 自动,hyundaiaba","2007款 1.6 手动,hyundaiabb",
					"2008款 1.6 自动 豪华型导航版,hyundaiabc","2008款 1.6 手动 舒适型,hyundaiabd","2008款 1.6 自动 舒适型,hyundaiabe","2008款 1.8 手动 豪华型,hyundaiabf",
					"2008款 1.6 手动 豪华型,hyundaiabg","2008款 1.8 自动 豪华型,hyundaiabh","2008款 1.6 自动 豪华型,hyundaiabi",
					"2009款 1.8 自动 豪华型导航版,hyundaiabj","2009款 1.8 手动 豪华型导航版,hyundaiabk","2009款 1.6 自动 豪华型导航版,hyundaiabl",
					"2009款 1.6 手动 豪华型真皮导航版,hyundaiabm","2009款 1.6 手动 豪华型导航版,hyundaiabn","2009款 1.6 手动 豪华型真皮版,hyundaiabo",
					"2009款 1.6 自动 舒适型天窗版,hyundaiabp",
					"2010款 1.8 自动 豪华型,hyundaiabq","2010款 1.8 手动 豪华型,hyundaiabr","2010款 1.6 手动 舒适型,hyundaiabs","2010款 1.6 手动 豪华型,hyundaiabt",
					"2010款 1.6 自动 舒适型,hyundaiabu","2010款 1.6 自动 豪华型,hyundaiabv",
					"2011款 1.6 自动 顶级型,hyundaiabw","2011款 1.8 手动 豪华型,hyundaiabx","2011款 1.8 手动 顶级型,hyundaiaby","2011款 1.8 自动 豪华型,hyundaiabz",
					"2011款 1.8 自动 顶级型,hyundaiabaa","2011款 1.6 手动 舒适型,hyundaiabab","2011款 1.6 手动 豪华型,hyundaiabac","2011款 1.6 手动 顶级型,hyundaiabad",
					"2011款 1.6 自动 舒适型,hyundaiabae","2011款 1.6 自动 豪华型,hyundaiabaf",
					"2015款 1.6 自动 豪华型,hyundaiabag","2015款 1.6 手动 豪华型,hyundaiabah","2015款 1.6 自动 舒适型,hyundaiabai","2015款 1.6 手动 舒适型,hyundaiabaj"
					],

			
			"朗动":["2012款 1.6 手动 GLX领先型,hyundaiaca","2012款 1.6 自动 DLX尊贵型,hyundaiacb","2012款 1.6 自动 GLX领先型,hyundaiacc","2012款 1.6 自动 GS时尚型,hyundaiacd",
					"2012款 1.8 手动 DLX尊贵型,hyundaiace","2012款 1.8 自动 DLX尊贵型,hyundaiacf","2012款 1.6 手动 GS时尚型,hyundaiacg",
					"2013款 1.6 自动 DLX尊贵型,hyundaiach","2013款 1.6 自动 GLX领先型,hyundaiaci",
					"2015款 1.8 自动 DLX尊贵型,hyundaiacj","2015款 1.8 手动 DLX尊贵型,hyundaiack","2015款 1.6 自动 DLX尊贵型,hyundaiacl","2015款 1.6 自动 GLX领先型,hyundaiacm",
					"2015款 1.6 自动 GS时尚型,hyundaiacn","2015款 1.6 手动 GLX领先型,hyundaiaco","2015款 1.6 手动 GS时尚型,hyundaiacp"
					],	
			
			"瑞奕":["2014款 1.6 自动 TOP顶级型,hyundaiada","2014款 1.4 自动 TOP顶级型,hyundaiadb","2014款 1.4 自动 GLX领先型,hyundaiadc",
					"2014款 1.4 手动 GLX领先型,hyundaiadd","2014款 1.4 手动 GL舒适型,hyundaiade"
					],
			
			"瑞纳":["2010款 三厢 1.4 自动 GL标准型,hyundaiaea","2010款 三厢 1.6 自动 GLS尊贵型,hyundaiaeb","2010款 三厢 1.6 自动 Blue版,hyundaiaec",
					"2010款 三厢 1.4 手动 GS舒适型,hyundaiaed","2010款 三厢 1.4 手动 GL标准型,hyundaiaee","2010款 三厢 1.4 自动 GS舒适型,hyundaiaef",
					"2010款 三厢 1.4 自动 GT时尚型,hyundaiaeg","2010款 三厢 1.4 自动 GLS豪华型,hyundaiaeh",
					"2010款 三厢 三厢 1.4 自动 BLUE型,hyundaiaei","2010款 三厢 1.4 手动 GLS豪华型,hyundaiaej","2010款 三厢 1.4 手动 BLUE,hyundaiaek",
					"2011款 两厢 1.4 手动 STYLE型,hyundaiael","2011款 两厢 1.4 手动 GLS豪华型,hyundaiaem","2011款 两厢 1.6 自动 GLS尊贵型,hyundaiaen",
					"2011款 两厢 1.4 自动 GS舒适型,hyundaiaeo","2011款 两厢 1.4 自动 GT时尚型,hyundaiaep","2011款 两厢 1.4 自动 GLS豪华型,hyundaiaeq",
					"2011款 两厢 1.4 自动 STYLE型,hyundaiaer","2011款 两厢 1.4 手动 GS舒适型,hyundaiaes",
					"2011款 两厢 1.4 手动 GL标准型,hyundaiaet","2013款 三厢 1.4 自动 GL标准型,hyundaiaeu",
					"2014款 三厢 1.6 自动 TOP顶级型,hyundaiaev","2014款 三厢 1.6 自动 GLX领先型,hyundaiaew","2014款 三厢 1.4 自动 TOP顶级型,hyundaiaex",
					"2014款 三厢 1.4 手动 TOP顶级型,hyundaiaey","2014款 三厢 1.4 自动 GLX领先型,hyundaiaez","2014款 三厢 1.4 自动 GLS智能型,hyundaiaeaa",
					"2014款 三厢 1.4 手动 GLS智能型,hyundaiaeab","2014款 三厢 1.4 自动 GS时尚型,hyundaiaeac","2014款 三厢 1.4 手动 GS时尚型,hyundaiaead"
					],
			
			"索纳塔":["2006款 2.0 手动 舒适型GLS LPG油气混合,hyundaiafa","2006款 2.0 手动 舒适型GLS ,hyundaiafb","2006款 2.0 手动 标准型GL ,hyundaiafc",
					"2005款 2.0 手动 标准型GL LPG油气混合,hyundaiafd","2005款 2.0 自动 豪华型GLS ,hyundaiafe","2005款 2.0 手动 舒适型GLS LPG油气混合,hyundaiaff",
					"2005款 2.0 自动 标准型GL ,hyundaiafg","2005款 2.0 手动 舒适型GLS ,hyundaiafh","2005款 2.0 手动 标准型GL ,hyundaiafu","2005款 2.5 自动 尊贵型GOLD ,hyundaiafj",
					"2006款 2.0 手动 标准型GL LPG油气混合,hyundaiafk","2006款 2.0 自动 豪华型GLS ,hyundaiafl","2006款 2.0 自动 标准型GL ,hyundaiafm",
					"2007款 2.0 自动 GL ,hyundaiafn","2007款 2.0 手动 GL ,hyundaiafo","2008款 2.0 手动 标准型GL CNG油气混合,hyundaiafp",
					"2009款 2.7 自动 GLS ,hyundaiafq","2009款 2.0 自动 标准型至尊版GL ,hyundaiafr","2009款 2.0 自动 标准型优雅版GL ,hyundaiafs",
					"2009款 2.0 自动 标准型宜人版GL ,hyundaiaft","2009款 2.0 手动 标准型优全版GL ,hyundaiafu","2009款 2.0 手动 标准型贵雅版GL ,hyundaiafv",
					"2011款 2.0 自动 GS时尚版,hyundaiafw","2011款 2.0 自动 GLS领先版,hyundaiafx","2011款 2.0 自动 GLX豪华版,hyundaiafy","2011款 2.4 自动 TOP顶级版,hyundaiafz",
					"2011款 2.4 自动 DLX尊贵版,hyundaiafaa","2011款 2.0 自动 TOP顶级版,hyundaiafab","2011款 2.0 自动 LUX至尊版,hyundaiafac","2011款 2.0 自动 DLX尊贵版,hyundaiafad",
					"2011款 2.0 手动 GS时尚版,hyundaiafae","2011款 2.0 自动 DLX豪华版,hyundaiafaf","2011款 2.0 自动 GS时尚版,hyundaiafag","2011款 2.0 自动 GLS领先版,hyundaiafah",
					"2013款 2.0 自动 LUX至尊版,hyundaiafai","2013款 2.0 自动 TOP顶级版,hyundaiafaj","2013款 2.4 自动 TOP顶级版,hyundaiafak","2013款 2.4 自动 DLX尊贵版,hyundaiafal",
					"2013款 2.4 自动 GLX豪华版,hyundaiafam","2013款 2.4 自动 GLS领先版,hyundaiafan","2013款 2.0 自动 DLX尊贵版,hyundaiafao","2013款 2.0 自动 GLX豪华版,hyundaiafap",
					"2013款 2.0 自动 GLS领先版,hyundaiafaq","2013款 2.0 自动 GS时尚版,hyundaiafar",
					"2014款 2.4 自动 TOP顶级版,hyundaiafas","2014款 2.4 自动 GLX豪华版,hyundaiafat","2014款 2.4 自动 GLS领先版,hyundaiafau","2014款 2.0 自动 DLX尊贵版,hyundaiafav",
					"2014款 2.0 自动 GLX豪华版,hyundaiafaw","2014款 2.0 自动 GLS领先版,hyundaiafax",
					"2015款 1.6T 自动 GX舒适型,hyundaiafay","2015款 2.0 自动 GLS智能型,hyundaiafaz","2015款 1.6T 自动 DLX尊贵型,hyundaiafba","2015款 1.6T 自动 GLX领先型,hyundaiafbb",
					"2015款 1.6T 自动 GLS智能型,hyundaiafbc","2015款 1.6T 自动 GS时尚型,hyundaiafbd","2015款 2.4 自动 TOP旗舰型,hyundaiafbe","2015款 2.4 自动 LUX至尊型,hyundaiafbf",
					"2015款 2.4 自动 DLX尊贵型,hyundaiafbg"
					],
			
			"途胜":["2005款 2.7 自动 GLS豪华型四驱 ,hyundaiaha","2005款 2.0 手动 GLS豪华型四驱,hyundaiahb",
					"2006款 2.0 手动 GLS 豪华型四驱,hyundaiahc","2006款 2.7 自动 GLS豪华型四驱 ,hyundaiahd","2006款 2.0 手动 GL时尚型前驱,hyundaiahe",
					"2006款 2.0 手动 GL舒适型前驱 ,hyundaiahf",
					"2007款 2.0 自动 GL舒适型前驱 ,hyundaiahg","2007款 2.0 手动 GL舒适型前驱 ,hyundaiahh","2007款 2.0 自动 GL舒适天窗型前驱 ,hyundaiahi",
					"2007款 2.0 手动 GLS豪华型四驱 ,hyundaiahj","2007款 2.0 手动 GL时尚型前驱 ,hyundaiahk","2007款 2.7 自动 GLS豪华型四驱 ,hyundaiahl",
					"2008款 2.0 自动 GL舒适型前驱,hyundaiahm","2008款 2.0 自动 GL舒适天窗型前驱 ,hyundaiahn",
					"2009款 2.0 手动 GLS豪华型四驱 ,hyundaiaho","2009款 2.0 手动 GL时尚型前驱 ,hyundaiahp","2009款 2.0 手动 GL舒适型前驱 ,hyundaiahq",
					"2009款 2.7 自动 GLS豪华型四驱 ,hyundaiahr","2009款 2.0 自动 GL舒适天窗型前驱 ,hyundaiahs",
					"2012款 1.6 手动 GL CNG油气混合,hyundaiaht","2013款 2.0 手动 舒适型前驱,hyundaiahu","2013款 2.0 手动 时尚型前驱,hyundaiahv",
					"2013款 2.0 自动 舒适型前驱,hyundaiahw","2013款 2.0 手动 尊贵型四驱,hyundaiahx","2013款 2.0 手动 豪华型四驱,hyundaiahy",
					"2015款 1.6T 自动 旗舰型四驱,hyundaiahz","2015款 1.6T 自动 尊贵型四驱,hyundaiahaa","2015款 1.6T 自动 领先型前驱,hyundaiahab",
					"2015款 1.6T 自动 舒适型前驱,hyundaiahac","2015款 1.6T 自动 智能型前驱,hyundaiahad","2015款 2.0 自动 智能型前驱,hyundaiahae",
					"2015款 2.0 手动 舒适型前驱,hyundaiahaf","2015款 2.0 自动 舒适型前驱,hyundaiahag"
					],	
			
			"雅绅特":["2006款 1.6 自动 豪华型,hyundaiaia","2006款 1.4 自动 豪华型,hyundaiaib","2006款 1.4 手动 基本型,hyundaiaic","2006款 1.4 手动 舒适型,hyundaiaid",
					"2006款 1.4 自动 舒适型,hyundaiaie","2007款 1.4 手动 舒适型,hyundaiaif","2007款 1.4 手动 基本型,hyundaiaig","2007款 1.4 自动 舒适型,hyundaiaih",
					"2007款 1.4 自动 豪华型,hyundaiaii","2007款 1.4 手动 时尚型,hyundaiaij","2007款 1.6 自动 豪华型,hyundaiaik","2007款 1.4 手动 豪华型,hyundaiail",
					"2007款 1.4 自动 尊贵型,hyundaiaim","2008款 1.6 自动 豪华型,hyundaiain","2008款 1.4 手动 舒适型,hyundaiaio","2008款 1.4 手动 豪华型,hyundaiaip",
					"2008款 1.4 自动 舒适型,hyundaiaiq","2008款 1.4 自动 豪华型,hyundaiair","2009款 1.4 手动 简配E3型,hyundaiais","2009款 1.4 手动 简配E2型,hyundaiait",
					"2009款 1.4 手动 简配E1型,hyundaiaiu","2009款 1.4 自动 舒适型双色版,hyundaiaiv","2009款 1.4 手动 舒适型双色版,hyundaiaiw",
					"2009款 1.4 自动 限量导航版,hyundaiaix","2009款 1.4 手动 限量导航版,hyundaiaiy","2011款 1.4 自动 豪华型,hyundaiaiz",
					"2011款 1.4 自动 舒适型,hyundaiaiaa","2011款 1.4 自动 尊贵型,hyundaiaiab","2011款 1.4 手动 舒适型,hyundaiaiac",
					"2011款 1.4 手动 豪华型,hyundaiaiad"
					],

			
			"领翔":["2009款 2.0 手动 GLS豪华型,hyundaiaja","2009款 2.0 自动 GL舒适型,hyundaiajb","2009款 2.0 自动 TOP顶级型,hyundaiajc","2009款 2.4 自动 TOP顶级型,hyundaiajd",
					"2009款 2.4 自动 DLX尊贵型,hyundaiaje","2009款 2.4 自动 GLS豪华型,hyundaiajf","2009款 2.0 自动 DLX尊贵型,hyundaiajg","2009款 2.0 自动 GLS豪华型,hyundaiajh",
					"2009款 2.0 手动 GL舒适型,hyundaiaji"
					],

//"雪铁龙"			

			"C2":["2006款 1.6 自动 EX,citroenaa","2006款 1.6 手动 EX,citroenab","2006款 1.6 自动 SX,citroenac","2006款 1.6 手动 SX,citroenad","2006款 1.4 手动 SX,citroenae",
				"2007款 1.6 自动 SX VTS运动版,citroenaf","2007款 1.6 手动 SX VTS运动版,citroenag","2007款 1.4 手动 SX VTS运动版,citroenah",
				"2008款 1.6 自动 SX VTS运动版,citroenai","2008款 1.6 手动 SX VTS运动版,citroenaj","2008款 1.4 手动 SX VTS运动版,citroenak",
				"2008款 1.6 自动 EX,citroenal","2008款 1.6 手动 EX,citroenam","2008款 1.6 自动 SX,citroenan","2008款 1.6 手动 SX,citroenao","2008款 1.4 手动 SX,citroenap",
				"2009款 1.6 自动 SX酷精灵版,citroenaq","2009款 1.6 自动 SX酷睿版,citroenar","2009款 1.6 自动 SX酷智版,citroenas","2009款 1.6 手动 SX酷精灵版,citroenat",
				"2009款 1.6 手动 SX酷睿版,citroenau","2009款 1.6 手动 SX酷智版,citroenav","2009款 1.4 手动 SX酷精灵版,citroenaw","2009款 1.4 手动 SX酷智版,citroenax",
				"2009款 1.4 手动 SX酷睿版,citroenay","2010款 1.4 手动 运动型,citroenaz","2010款 1.4 手动 舒适型,citroenaaa",
				"2012款 1.6 自动 运动型,citroenaab","2012款 1.4 手动 运动型,citroenaac","2012款 1.4 手动 舒适型,citroenaad"
				],
							
			
			"C3-XR":["2015款 1.6T 自动 旗舰型,citroenba","2015款 1.6T 自动 智能型,citroenbb","2015款 1.6T 自动 先锋型,citroenbc","2015款 1.6 自动 智能型,citroenbd",
					"2015款 1.6 自动 先锋型,citroenbe","2015款 1.6 手动 先锋型,citroenbf","2015款 1.6 自动 时尚型,citroenbg","2015款 1.6 手动 时尚型,citroenbh"
					],
			
			"C4L":["2013款 1.8 自动 劲驰版,citroenca","2013款 1.6 手动 劲享节能版,citroencb","2013款 1.6 手动 劲享增配版,citroencc","2013款 1.6T 自动 劲驰版,citroencd",
					"2013款 1.8 自动 劲智版,citroence","2013款 1.8 手动 劲智版,citroencf","2013款 1.8 自动 劲享版,citroencg","2013款 1.8 手动 劲享版,citroench",
					"2013款 1.6T 自动 劲智版,citroenci","2013款 1.6 手动 劲智版,citroencj","2013款 1.6T 自动 劲享版,citroenck","2013款 1.6 手动 劲享版,citroencl",
					"2014款 1.8 自动 车载互联劲智型,citroencm","2014款 1.8 自动 车载互联劲驰型,citroencn","2014款 1.6T 自动 车载互联劲驰型,citroenco",
					"2014款 1.6T 自动 车载互联劲智型,citroencp","2014款 1.8 手动 车载互联劲智型,citroencq","2014款 1.8 手动 智驱版劲智型,citroencr",
					"2014款 1.8 手动 智驱版劲享型,citroencs","2014款 1.6T 自动 智驱版劲智型,citroenct","2014款 1.8 自动 智驱版劲智型,citroencu",
					"2014款 1.8 自动 智驱版劲享型,citroencv",
					"2015款 1.6T 自动 旗舰版,citroencw","2015款 1.6T 自动 尊贵版,citroencx","2015款 1.2T 自动 尊贵版,citroency","2015款 1.2T 自动 豪华版,citroencz",
					"2015款 1.2T 手动 豪华版,citroencaa","2015款 1.8 自动 尊贵版,citroencab","2015款 1.8 自动 豪华版,citroencac","2015款 1.8 自动 领先版,citroencad",
					"2015款 1.8 手动 领先版,citroencae"
					],
			
			"C5":["2010款 3.0 自动 旗舰型,citroenda","2010款 2.3 自动 尊贵型,citroendb","2010款 2.3 自动 尊雅型导航版,citroendc","2010款 2.3 自动 尊雅型,citroendd",
				"2010款 2.0 自动 舒适型,citroende","2010款 2.0 手动 舒适型,citroendf",
				"2011款 3.0 自动 东方之旅旗舰型,citroendg","2011款 2.3 自动 东方之旅豪华型,citroendh","2011款 2.3 自动 东方之旅尊贵型,citroendi",
				"2011款 2.3 自动 东方之旅尊驭型,citroendj","2011款 2.3 自动 东方之旅尊雅型,citroendk","2011款 2.0 自动 东方之旅舒适型,citroendl",
				"2011款 2.0 手动 东方之旅舒适型,citroendm","2011款 3.0 自动 旗舰型,citroendn","2011款 2.3 自动 豪华型,citroendo","2011款 2.3 自动 尊贵型,citroendp",
				"2011款 2.3 自动 尊驭型,citroendq","2011款 2.3 自动 尊雅型,citroendr","2011款 2.0 自动 舒适型,citroends","2011款 2.0 手动 舒适型,citroendt",
				"2012款 2.3 自动 20周年纪念版,citroendu","2012款 3.0 自动 旗舰型,citroendv","2012款 2.3 自动 尊驭型,citroendw","2012款 2.3 自动 尊贵型,citroendx",
				"2012款 2.3 自动 豪华型,citroendy","2012款 2.0 手动 舒适版,citroendz","2012款 2.0 自动 尊享型,citroendaa",
				"2013款 2.3 自动 悦尚版,citroendab","2013款 2.0 自动 悦尚版,citroendac","2013款 2.3 自动 悦享版,citroendad","2013款 2.0 自动 悦享版,citroendae",
				"2013款 3.0 自动 旗舰型,citroendaf","2013款 2.3 自动 豪华型,citroendag","2013款 2.3 自动 尊贵型,citroendah","2013款 2.3 自动 尊驭型,citroendai",
				"2013款 2.0 自动 尊享型,citroendaj","2013款 2.0 手动 舒适型,citroendak",
				"2014款 2.3 自动 尊贵型,citroendal","2014款 2.3 自动 豪华型,citroendam","2014款 1.6T 自动 尊贵型,citroendan","2014款 1.6T 自动 尊享型,citroendao",
				"2014款 2.0 自动 尊悦型,citroendap","2014款 2.0 自动 尊享型,citroendaq",
				"2016款 1.8T 自动 豪华型,citroendar","2016款 1.8T 自动 尊贵型,citroendas","2016款 1.8T 自动 尊享型,citroendat","2016款 1.6T 自动 尊悦型,citroendau"
				],
			
			"世嘉":["2007款 两厢 2.0 手动 VTS,citroenea","2007款 两厢 2.0 自动,citroeneaa","2007款 两厢 1.6 手动,citroeneaaa","2007款 两厢 1.6 自动,citroeneaaaa",
					"2008款 三厢 1.6 手动,citroeneb","2008款 三厢 1.6 自动,citroenebb","2009款 两厢 1.6 手动 乐享型,citroenebbb","2009款 两厢 1.6 手动 乐尚型,citroenebbbb",
					"2009款 两厢 2.0 手动 豪华天窗型VTS,citroenec","2009款 两厢 2.0 手动 ESP版豪华天窗型VTS,citroenecc",
					"2009款 两厢 2.0 手动 夺冠版天窗型,citroeneccc","2009款 两厢 2.0 自动 豪华天窗型,citroenecccc","2009款 两厢 2.0 自动 导航版豪华天窗型,citroened",
					"2009款 两厢 1.6 手动 时尚天窗型,citroenedd","2009款 两厢 1.6 自动 时尚天窗型,citroeneddd","2009款 两厢 1.6 自动 舒适天窗型,citroenedddd",
					"2009款 两厢 1.6 手动 舒适天窗型,citroenee","2009款 两厢 2.0 手动 夺冠版,citroeneee","2009款 两厢 2.0 手动 ESP版豪华型VTS,citroeneeee",
					"2009款 两厢 2.0 手动 豪华型VTS,citroeneeeeef","2009款 两厢 1.6 自动 时尚型,citroeneff","2009款 两厢 2.0 自动 豪华型,citroenefff",
					"2009款 两厢 2.0 自动 导航版豪华型,citroeneffff","2009款 两厢 1.6 自动 舒适型,citroeneg","2009款 两厢 1.6 手动 舒适型,citroenegg",
					"2009款 两厢 1.6 手动 时尚型,citroeneggg","2009款 三厢 2.0 自动 时尚型,citroenegggg","2009款 三厢 2.0 自动 豪华天窗型,citroeneh",
					"2009款 三厢 2.0 自动 豪华型,citroenehh","2009款 三厢 2.0 自动 舒适型,citroenehhh","2009款 三厢 2.0 手动 舒适型,citroenei",
					"2009款 三厢 2.0 手动 时尚型,citroeneii","2009款 三厢 1.6 自动 舒适型,citroeneiii","2009款 三厢 1.6 自动 时尚型,citroenej",
					"2009款 三厢 1.6 手动 舒适型,citroenejj","2009款 三厢 1.6 手动 时尚型,citroenejjj","2010款 两厢 1.6 手动 尚乐版,citroenejjjj",
					"2010款 两厢 2.0 自动 时尚型,citroenek","2010款 两厢 2.0 手动 夺冠炫酷升级型,citroenekk","2010款 两厢 1.6 自动 限量版音乐型,citroenekkk",
					"2010款 两厢 1.6 手动 限量版音乐型,citroenekkkk","2010款 两厢 1.6 手动 炫酷版舒适型,citroenel","2010款 两厢 1.6 手动 炫酷版时尚型,citroenell",
					"2010款 三厢 1.6 手动 尚乐版,citroenelll","2010款 三厢 1.6 自动 尚乐版,citroenellll",
					"2011款 两厢 2.0 自动 乐骋型,citroenem","2011款 两厢 2.0 自动 乐炫型,citroenemm","2011款 两厢 2.0 手动 劲雅型,citroenemmm","2011款 两厢 2.0 手动 博雅型,citroenemmmm",
					"2011款 两厢 2.0 自动 劲雅型,citroenen","2011款 两厢 2.0 自动 博雅型,citroenenn","2011款 两厢 1.6 手动 悦雅型,citroenennn","2011款 两厢 1.6 手动 炫雅型,citroenennnn",
					"2011款 两厢 1.6 手动 冠军版,citroeneo","2011款 两厢 1.6 自动 悦雅型,citroeneoo","2011款 两厢 1.6 自动 炫雅型,citroeneooo","2011款 两厢 1.6 自动 冠军版,citroeneoooo",
					"2011款 三厢 2.0 手动 尊享型,citroenep","2011款 三厢 2.0 手动 锐尚型,citroenepp","2011款 三厢 2.0 自动 锐尚型,citroeneppp","2011款 三厢 2.0 自动 豪华型,citroenepppp",
					"2011款 三厢 1.6 手动 舒适型,citroeneq","2011款 三厢 1.6 手动 时尚型,citroeneqq","2011款 三厢 1.6 手动 冠军版,citroeneqqq","2011款 三厢 1.6 自动 舒适型,citroeneqqqq",
					"2011款 三厢 1.6 自动 时尚型,citroener","2011款 三厢 1.6 自动 冠军版,citroenerr",
					"2012款 两厢 1.6 自动 乐享型,citroenerrr","2012款 两厢 1.6 自动 乐尚型,citroenerrrr","2012款 两厢 2.0 自动 乐骋型,citroenes","2012款 两厢 2.0 手动 乐骋型,citroeness",
					"2012款 两厢 2.0 自动 乐炫型,citroenesss","2012款 两厢 2.0 手动 乐炫型,citroenessss","2012款 两厢 1.6 手动 乐享型,citroenet","2012款 两厢 1.6 手动 乐尚型,citroenett",
					"2012款 三厢 2.0 自动 品驭型,citroenettt","2012款 三厢 2.0 自动 品悦型,citroeneu","2012款 三厢 2.0 手动 品悦型,citroeneuu","2012款 三厢 1.6 自动 品享型,citroeneuuu",
					"2012款 三厢 1.6 自动 品尚型,citroenev","2012款 三厢 1.6 手动 品尚型,citroenevv","2012款 三厢 2.0 手动 贺岁版,citroenevvv","2012款 三厢 2.0 自动 贺岁版,citroenew",
					"2013款 三厢 1.6 自动 品尚型,citroeneww","2013款 三厢 1.6 自动 炫音版,citroenewww","2013款 三厢 1.6 手动 品尚型CNG,citroenex","2012款 三厢 1.6 手动 品享型,citroenexx",
					"2013款 两厢 1.6 手动 乐享型,citroenexxx","2013款 两厢 1.6 手动 乐尚型,citroeney","2013款 两厢 1.6 自动 乐享型,citroeneyy","2013款 两厢 1.6 自动 乐尚型,citroeneyyy",
					"2013款 三厢 2.0 手动 品悦型,citroenez","2013款 三厢 2.0 自动 品悦型,citroenezz","2013款 三厢 1.6 手动 品尚型,citroenezzz","2013款 三厢 1.6 自动 品享型,citroeneab",
					"2014款 三厢 1.6 手动 车载互联版,citroeneac","2014款 三厢 1.6 自动 车载互联版,citroenead","2014款 三厢 2.0 自动 品悦型VTS版,citroeneae",
					"2014款 三厢 2.0 手动 品悦型VTS版,citroeneaf","2014款 三厢 1.6 自动 品享型VTS版,citroeneag","2014款 三厢 1.6 手动 品享型VTS版,citroeneah",
					"2014款 三厢 1.6 自动 品尚型VTS版,citroeneai","2014款 三厢 1.6 手动 品尚型VTS版,citroeneaj"
					],
			
			"凯旋":["2006款 2.0 自动 旗舰型,citroenfa","2006款 2.0 自动 尊贵型,citroenfb","2006款 2.0 手动 尊贵型,citroenfc",
					"2007款 2.0 自动 精英型,citroenfd","2007款 2.0 手动 精英型,citroenfe",
					"2008款 2.0 手动 尊贵型,citroenff","2008款 2.0 自动 尊贵型,citroenfg","2008款 2.0 自动 旗舰型导航版,citroenfh","2008款 2.0 自动 旗舰型,citroenfi",
					"2008款 2.0 手动 精英型,citroenfj","2008款 2.0 自动 精英型,citroenfk",
					"2010款 2.0 手动 科技型,citroenfl","2010款 2.0 手动 经典型,citroenfm","2010款 2.0 自动 科技型,citroenfn","2010款 2.0 自动 经典型,citroenfo"
					],
   
			
			"富康":["2005款 1.4 手动 EDC油气混合,citroenga","2005款 1.4 手动 RDN油气混合,citroengb","2005款 1.4 手动 EDN油气混合,citroengc",
					"2005款 1.6 自动 新自由人舒适型16V,citroengd","2005款 1.6 手动 新自由人舒适型16V,citroenge","2005款 1.6 自动 AXC新自由人舒适型8V,citroengf",
					"2005款 1.4 手动 简约版,citroengg","2005款 1.6 自动 AXC新自由人舒适型16V,citroengh","2005款 1.6 手动 AXC新自由人舒适型16V,citroengi",
					"2005款 1.6 自动 AXC1新自由人舒适型8V,citroengj","2005款 1.6 手动 AXC新自由人舒适型8V,citroengk",
					"2007款 1.6 自动 新自由人舒适型16V,citroengl","2007款 1.6 手动 新自由人舒适型16V,citroengm","2007款 1.6 手动 AXC新自由人舒适型16V,citroengn",
					"2007款 1.6 自动 AXC新自由人舒适型16V,citroengo","2008款 1.4 手动 RPC,citroengp"
					],
			
			"毕加索":["2006款 2.0 自动,citroenva","2006款 1.6 手动 16V,citroenvb","2007款 1.6 手动 舒适型,citroenvc",               
					"2007款 2.0 自动 豪华型天窗版,citroenvd","2007款 2.0 自动 豪华型,citroenve","2007款 1.6 手动 舒适型天窗版,citroenvf",
					"2015款 1.6T 自动 豪华型科技选装包7座,citroenvg","2015款 1.6T 自动 豪华型科技选装包5座,citroenvh","2015款 1.6T 自动 豪华型7座,citroenvi",
					"2015款 1.6T 自动 时尚型7座,citroenvj","2015款 1.6T 自动 豪华型5座,citroenvk","2015款 1.6T 自动 时尚型5座,citroenvl"
					],
			
			"爱丽舍":["2005款 三厢 1.6 自动,citroenha","2005款 三厢 1.6 自动 VIP赛纳座椅版,citroenhb","2005款 三厢 1.6 手动 VIP,citroenhc",
					"2005款 三厢 1.6 手动,citroenhd","2005款 三厢 1.6 手动 VIP赛纳座椅版,citroenhe","2005款 三厢 1.6 自动 8V,citroenhf",
					"2005款 三厢 1.6 自动 VIP,citroenhg","2005款 三厢 1.6 手动 X 8V,citroenhh",
					"2006款 三厢 1.4 手动,citroenhi","2006款 三厢 1.6 手动 8V LPG油气混合 ,citroenhj","2006款 三厢 1.6 手动,citroenhk","2006款 三厢 1.6 自动,citroenhl",
					"2006款 三厢 1.6 手动 8V CNG油气混合,citroenhm",
					"2007款 三厢 1.6 手动 油气混合,citroenhn","2007款 三厢 1.6 自动 油气混合,citroenho","2007款 三厢 1.6 自动,citroenhp","2007款 三厢 1.6 手动,citroenhq",
					"2007款 三厢 1.6 自动 VIP出租版,citroenhr","2008款 三厢 1.6 自动 CNG油气混合,citroenhs","2008款 三厢 1.6 手动 CNG油气混合,citroenht",
					"2008款 三厢 1.6 自动 舒适型,citroenhu","2008款 三厢 1.6 手动 舒适型,citroenhv","2008款 三厢 1.6 自动 豪华型,citroenhw",
					"2008款 三厢 1.6 手动 豪华型,citroenhx","2008款 三厢 1.6 手动 标准型,citroenhy","2008款 两厢 1.6 手动,citroenhz","2008款 两厢 1.6 自动,citroenhaa",
					"2009款 两厢 1.6 自动 舒适型,citroenhab","2009款 两厢 1.6 手动 豪华型,citroenhac","2009款 两厢 1.6 手动 标准型,citroenhad",
					"2009款 三厢 1.6 手动 10XA4APSA发动机 油气混合CNG ,citroenhae","2009款 三厢 1.6 手动 CNG油气混合,citroenhaf","2009款 三厢 1.6 手动 出租型8V CNG油气混合 ,citroenhag",
					"2009款 三厢 1.6 手动 标准型8V CNG油气混合,citroenhah","2009款 三厢 1.6 自动 标准型,citroenhai",
					"2010款 三厢 1.6 手动 尊贵型,citroenhaj","2010款 三厢 1.6 手动 科技型,citroenhak","2010款 三厢 1.6 自动 科技型,citroenhal",
					"2011款 三厢 1.6 手动 科技型 CNG油气混合,citroenham","2011款 三厢 1.6 手动 尊贵型,citroenhan","2011款 三厢 1.6 手动 科技型,citroenhao",
					"2011款 三厢 1.6 自动 科技型,citroenhap","2011款 两厢 1.6 手动 尊贵型,citroenhaq","2011款 两厢 1.6 手动 科技型,citroenhas","2011款 两厢 1.6 自动 科技型,citroenhat",
					"2012款 三厢 1.6 手动 科技型 CNG油气混合,citroenhau","2012款 三厢 1.6 自动 尊贵型,citroenhav","2012款 三厢 1.6 自动 科技型,citroenhaw","2012款 三厢 1.6 手动 尊贵型,citroenhax",
					"2012款 三厢 1.6 手动 科技型,citroenhay",
					"2013款 三厢 1.6 手动 科技天窗型 CNG油气混合,citroenhaz","2013款 三厢 1.6 手动 科技型 CNG油气混合,citroenhba","2013款 三厢 1.6 自动 尊贵型,citroenhbb",
					"2013款 三厢 1.6 自动 尊贵天窗型,citroenhbc","2013款 三厢 1.6 手动 尊贵天窗型,citroenhbd","2013款 三厢 1.6 手动 尊贵型,citroenhve","2013款 三厢 1.6 自动 科技型,citroenhbf",
					"2013款 三厢 1.6 自动 科技天窗型,citroenhbg","2013款 三厢 1.6 手动 科技天窗型,citroenhbh","2013款 三厢 1.6 手动 科技型,citroenhbi",
					"2014款 三厢 1.6 手动 时尚型WTCC纪念版,citroenhbj","2014款 三厢 1.6 手动 舒适型WTCC纪念版,citroenhbk","2014款 三厢 1.6 自动 时尚型WTCC纪念版,citroenhbl",
					"2014款 三厢 1.6 自动 豪华型WTCC纪念版,citroenhbm","2014款 三厢 1.6 手动 时尚型天窗版 CNG油气混合,citroenhbn","2014款 三厢 1.6 手动 时尚型 CNG油气混合,citroenhbo",
					"2014款 三厢 1.6 手动 舒适型,citroenhbp","2014款 三厢 1.6 手动 时尚型,citroenhbq","2014款 三厢 1.6 自动 舒适型,citroenhbr","2014款 三厢 1.6 自动 时尚型,citroenhbs",
					"2014款 三厢 1.6 自动 豪华型,citroenhbt",
					"2015款 三厢 1.6 自动 质尚版豪华型,citroenhbu","2015款 三厢 1.6 自动 质尚版舒适型,citroenhbv","2015款 三厢 1.6 自动 质尚版时尚型,citroenhbw",
					"2015款 三厢 1.6 手动 质尚版舒适型,citroenhbx","2015款 三厢 1.6 手动 质尚版时尚型,citroenhby","2015款 三厢 1.6 自动 舒适型WTCC纪念版,citroenhbz"
					],

			
			"赛纳":["2005款 2.0 自动 舒适型,citroenia","2005款 2.0 自动 豪华型,citroenib","2005款 2.0 手动 豪华型,citroenic","2005款 2.0 手动 舒适型,citroenid"],
			
			"C4":["2009款 1.6T 自动 豪华GPS版,citroenja","2009款 1.6T 自动 豪华版,citroenjb","2009款 1.6T 自动 VTS,citroenjc"],
			
			"C4 Aircross":["2012款 2.0 自动 豪华版四驱,citroenka","2012款 2.0 自动 豪华版前驱,citroenkb","2012款 2.0 自动 舒适版四驱,citroenkc","2012款 2.0 自动 舒适版前驱,citroenkd",
							"2013款 2.0 自动 豪华版四驱,citroenke","2013款 2.0 自动 豪华版前驱,citroenkf","2013款 2.0 自动 舒适版四驱,citroenkg","2013款 2.0 自动 舒适版前驱,citroenkh",
							"2013款 2.0 自动 进取版前驱,citroenki"
							],
			
			"C4毕加索":["2007款 2.0 自动,citroenla","2009款 2.0 自动 舒适型,citroenlb","2009款 2.0 自动 豪华型,citroenlc"],
			
			"C5(进口)":["2007款 3.0 自动 豪华型,citroena","2007款 3.0 自动 标准型,citroenb","2007款 2.0 自动 豪华型,citroenc",
						"2007款 2.0 自动 标准型,citroend","2008款 3.0 自动,citroene"
						],
			
			"C6":["2005款 3.0 自动,citroenma","2008款 3.0 自动 旗舰型,citroenmb","2008款 3.0 自动 豪华型,citroenmc"
				],
			
			"DS3":["2012款 敞篷 1.6 自动 至尊版,citroenna","2012款 敞篷 1.6 自动 风尚版,citroennb","2012款 敞篷 1.6 自动 时尚版,citroennc",
					"2013款 两厢 1.6 自动 至尊版,citroennd","2013款 两厢 1.6 自动 风尚版,citroenne","2013款 两厢 1.6 自动 时尚版,citroennf"
					],
			
			"DS4":["2012款 1.6T 自动 风尚版,citroenoa","2012款 1.6T 自动 雅致版,citroenob"],
			
			"DS5":["2014款 两厢 1.6T 自动 卓越版THP160,citroenpa","2014款 两厢 1.6T 自动 豪华版THP160,citroenpb","2014款 两厢 1.6T 自动 雅致版THP160,citroenpc",
					"2014款 两厢 1.6T 自动 尊享版THP200,citroenpd","2014款 两厢 1.6T 自动 豪华版THP200,citroenpe","2014款 两厢 1.6T 自动 雅致版THP200,citroenpf",
					"2015款 两厢 1.8T 自动 旗舰版THP200,citroenpg","2015款 两厢 1.8T 自动 尊享版THP200,citroenph","2015款 两厢 1.8T 自动 豪华版THP200,citroenpi",
					"2015款 两厢 1.6T 自动 雅致版THP160,citroenpj","2015款 两厢 1.6T 自动 豪华版THP160,citroenpk"
					],
			
			"DS 5LS":["2014款 1.6T 自动 尊享版THP200,citroensa","2014款 1.6T 自动 豪华版THP200,citroensb","2014款 1.6T 自动 豪华版THP160,citroensc",
					"2014款 1.6T 自动 雅致版THP160,citroensd","2014款 1.8 自动 雅致版VTi140,citroense","2014款 1.8 自动 舒适版VTi140,citroensf",
					"2014款 1.8 手动 舒适版VTi140,citroensg",
					"2015款 1.6T 自动 60周年限量版THP160,citroensh","2015款 1.6T 自动 尊享版THP200,citroensi","2015款 1.6T 自动 豪华版THP200,citroensj",
					"2015款 1.6T 自动 豪华版THP160,citroensk","2015款 1.6T 自动 雅致版THP160,citroensl","2015款 1.6T 自动 风尚版THP160,citroensm",
					"2015款 1.6T 自动 舒适版THP160,citroensn","2015款 1.6T 自动 基础版THP160,citroenso"
					],
			
			"DS5(进口)":["2012款 1.6T 自动 尊享版,citroenpta","2012款 1.6T 自动 豪华版,citroenptb","2012款 1.6T 自动 雅致版,citroenptc"],
			
			"DS6":["2014款 1.6T 自动 舒适版THP160,citroenua","2014款 1.6T 自动 尊享版THP200,citroenub","2014款 1.6T 自动 豪华版THP200,citroenuc",
					"2014款 1.6T 自动 尊享版THP160,citroenud","2014款 1.6T 自动 豪华版THP160,citroenue","2014款 1.6T 自动 雅致版THP160,citroenuf",
					"2016款 1.8T 自动 尊享版THP200,citroenug","2016款 1.8T 自动 60周年限量版THP200,citroenuh","2016款 1.8T 自动 豪华版THP200,citroenui",
					"2016款 1.6T 自动 尊享版THP160,citroenuj","2016款 1.6T 自动 豪华版THP160,citroenuk","2016款 1.6T 自动 雅致版THP160,citroenul",
					"2016款 1.6T 自动 舒适版THP160,citroenum"
					],
					
			"雪铁龙C4世嘉":["2016款 1.6T 自动 旗舰型,citroenva","2016款 1.2T 自动 旗舰型,citroenvb","2016款 1.2T 自动 豪华型,citroenvc",
							"2016款 1.2T 手动 舒适型,citroenvd","2016款 1.6 自动 舒适型,citroenve","2016款 1.6 自动 豪华型,citroenvf","2016款 1.6 手动 豪华型,citroenvg","2016款 1.6 手动 舒适型,citroenvh"
							],		

//
//"雪佛兰"

			"乐风":["2006款 1.4 自动 SE,chevroletaa","2006款 1.4 手动 SE,chevroletab","2006款 1.6 自动 SX,chevroletac","2006款 1.6 手动 SX,chevroletad",
					"2006款 1.6 手动 SX豪华型,chevroletae","2006款 1.6 自动 SX豪华型,chevroletaf","2006款 1.4 自动 SE舒适型,chevroletag",
					"2006款 1.4 手动 SE舒适型,chevroletah","2006款 1.4 手动 SL基本型,chevroletai",
					"2007款 1.4 自动 SE,chevroletaj","2007款 1.4 手动 SE,chevroletak","2007款 1.6 自动 SX,chevroletal","2007款 1.6 手动 SX,chevroletam",
					"2007款 1.4 手动 SL劲黑内饰型,chevroletan","2007款 1.6 手动 SX豪华型,chevroletao","2007款 1.6 自动 SX豪华型,chevroletap",
					"2007款 1.4 手动 SL基本型,chevroletaq","2007款 1.4 自动 SL基本型,chevroletar","2007款 1.4 手动 SE舒适型,chevroletas",
					"2007款 1.4 自动 SE舒适型,chevroletat",
					"2008款 1.4 手动 SE活力限量型,chevroletau","2008款 1.4 手动 SL劲黑内饰型,chevroletav","2008款 1.4 手动 基本型,chevroletaw",
					"2008款 1.4 手动 SE舒适型,chevroletax","2008款 1.4 自动 SL基本型,chevroletay","2008款 1.4 自动 SE舒适型,chevroletaz",
					"2008款 1.6 自动 SX豪华型,chevroletaaa","2008款 1.6 手动 SX豪华型,chevroletaab","2009款 1.6 手动 SX魅动版,chevroletaac",
					"2009款 1.6 自动 SX格调版,chevroletaad","2009款 1.4 手动 SE风度版,chevroletaae","2009款 1.4 自动 SE风尚版,chevroletaaf",
					"2009款 1.2 手动 SL基本型,chevroletaag","2009款 1.6 自动 SX豪华导航型,chevroletaah","2009款 1.6 自动 SX豪华型,chevroletaai",
					"2009款 1.6 手动 SX豪华型,chevroletaaj","2009款 1.6 手动 SX豪华导航型,chevroletaak","2009款 1.4 自动 SE舒适型,chevroletaal",
					"2009款 1.4 手动 SE舒适型,chevroletaam","2009款 1.4 自动 SL基本型,chevroletaan","2009款 1.4 手动 SL基本型,chevroletaao",
					"2009款 1.2 手动 SL高效型,chevroletaap"
					],
					
			"乐骋":["2005款 1.4 自动 SX豪华型,chevroletba","2005款 1.4 手动 SE舒适型,chevroletbb","2005款 1.4 自动 SE舒适型,chevroletbc",
					"2006款 1.4 自动 SX豪华型,chevroletbd","2006款 1.4 手动 SX豪华型,chevroletbe","2006款 1.4 手动 SL基本型,chevroletbf",
					"2007款 1.2 手动 SL,chevroletbg","2007款 1.4 自动 SX豪华型,chevroletbh","2007款 1.4 自动 SE舒适型,chevroletbi","2007款 1.4 手动 SE,chevroletbj",
					"2007款 1.6 自动 SX,chevroletbk","2007款 1.6 手动 SE,chevroletbl","2007款 1.4 手动 SX豪华型,chevroletbm",
					"2008款 1.4 自动 SX豪华型,chevroletbn","2008款 1.4 手动 SX豪华型,chevroletbo","2008款 1.2 手动 SL高效型,chevroletbp","2008款 1.4 自动 SE舒适型,chevroletbq",
					"2008款 1.2 手动 SL基本型,chevroletbr","2008款 1.4 手动 SE舒适型,chevroletbs","2008款 1.6 自动 SX豪华型,chevroletbt","2008款 1.6 自动 SX劲黑内饰型,chevroletbu",
					"2008款 1.6 手动 SX劲黑内饰型,chevroletbv","2008款 1.6 手动 SX豪华型,chevroletbw",
					"2009款 1.6 自动 SX豪华型,chevroletbx","2009款 1.6 自动 SX劲黑内饰型,chevroletby","2009款 1.6 手动 SX劲黑内饰型,chevroletbz","2009款 1.4 自动 SE舒适型,chevroletbaa",
					"2009款 1.4 手动 SE舒适型,chevroletbab","2009款 1.2 手动 SL劲黑内饰型,chevroletbac","2009款 1.2 手动 SL高效型,chevroletbad"
					],
			
			"创酷":["2014款 1.4T 自动 变形金刚限量版四驱,chevroletca","2014款 1.4T 自动 旗舰型四驱,chevroletcb","2014款 1.4T 自动 豪华型前驱,chevroletcc",
					"2014款 1.4T 自动 舒适型前驱,chevroletcd","2014款 1.4T 手动 舒适型前驱,chevroletce","2016款 1.4T 自动 旗舰型四驱,chevroletcf",
					"2016款 1.4T 自动 豪华型前驱,chevroletcg","2016款 1.4T 自动 舒适型前驱天窗版,chevroletch",
					"2016款 1.4T 手动 舒适型前驱天窗版,chevroletci"
					],
			
			"景程":["2005款 2.0 自动 SX豪华型,chevroletda","2005款 2.0 自动 SE舒适型,chevroletdb","2006款 2.0 手动 SX豪华型,chevroletdc","2006款 2.0 自动 SX豪华型,chevroletdd",
					"2006款 2.0 手动 SE舒适型,chevroletde","2006款 2.0 自动 SE舒适型,chevroletdf","2006款 2.0 自动 SL基本型,chevroletdg","2006款 2.0 手动 SL基本型,chevroletdh",
					"2007款 2.0 手动 行政版,chevroletdi","2007款 2.0 自动 SE舒适型,chevroletdj","2007款 2.0 手动 SE舒适型,chevroletdk","2007款 2.0 手动 SX豪华型,chevroletdl",
					"2007款 2.0 自动 SX豪华型,chevroletdm","2008款 2.0 手动 行政版,chevroletdn","2008款 2.0 手动 SX豪华型,chevroletdo","2008款 2.0 手动 SE舒适型,chevroletdp",
					"2008款 2.0 自动 SX豪华型,chevroletdq","2008款 2.0 自动 SE舒适型,chevroletdr","2009款 2.0 自动 SE智能型,chevroletds",
					"2010款 1.8 手动 SX豪华版,chevroletdt","2010款 1.8 手动 SX豪华导航版,chevroletdu","2010款 1.8 自动 SX豪华导航版,chevroletdv",
					"2010款 1.8 自动 SX豪华版,chevroletdw","2010款 1.8 自动 SE舒适版,chevroletdx","2010款 1.8 手动 SE舒适版,chevroletdy",
					"2010款 1.8 手动 SL致真版,chevroletdz",
					"2011款 1.8 手动 SX豪华导航版,chevroletdaa","2011款 1.8 手动 SX豪华版,chevroletdab","2011款 1.8 自动 SX豪华导航版,chevroletdac",
					"2011款 1.8 自动 SX豪华版,chevroletdad","2011款 1.8 手动 SL致真版,chevroletdae","2011款 1.8 手动 SE舒适版炫黑内饰,chevroletdaf",
					"2011款 1.8 手动 SE舒适版,chevroletdag","2011款 1.8 自动 SE舒适版炫黑内饰,chevroletdah","2011款 1.8 自动 SE舒适版,chevroletdai",
					"2011款 1.8 自动 SX豪华炫黑内饰版,chevroletdaj",
					"2013款 1.8 自动 SX行政版,chevroletdak","2013款 1.8 自动 SX豪华版,chevroletdal","2013款 1.8 手动 SX豪华版,chevroletdam","2013款 1.8 自动 SE舒适版,chevroletdan",
					"2013款 1.8 手动 SE舒适版,chevroletdao","2013款 1.8 手动 SL致真版,chevroletdap"
					],
			
			"爱唯欧":["2011款 两厢 1.4 手动 SE,chevroletea","2011款 两厢 1.4 手动 SL,chevroleteb","2011款 两厢 1.6 自动 SX,chevroletec","2011款 两厢 1.4 自动 SL,chevroleted",
					"2011款 两厢 1.4 自动 SE,chevroletee","2011款 两厢 1.6 手动 SX,chevroletef","2011款 三厢 1.4 手动 SE,chevroleteg","2011款 三厢 1.4 手动 SL,chevroleteh",
					"2011款 三厢 1.6 自动 SX,chevroletei","2011款 三厢 1.4 自动 SL,chevroletej","2011款 三厢 1.4 自动 SE,chevroletek","2011款 三厢 1.6 手动 SX,chevroletel",
					"2013款 两厢 1.6 自动 SX风尚影音版,chevroletem","2013款 两厢 1.6 手动 SX风尚影音版,chevroleten","2013款 三厢 1.6 自动 SX风尚影音版,chevroleteo",
					"2013款 三厢 1.6 手动 SX风尚影音版,chevroletep","2013款 三厢 1.4 自动 时尚天窗版,chevroleteq","2014款 两厢 1.6 自动 SX风尚版,chevroleter",
					"2014款 两厢 1.4 自动 SE畅悠版,chevroletes","2014款 两厢 1.4 手动 SE乐悠版,chevroletet","2014款 两厢 1.4 自动 SL时尚版,chevroleteu",
					"2014款 两厢 1.4 手动 SL舒享版,chevroletev","2014款 两厢 1.4 手动 SL舒适版,chevroletew","2014款 三厢 1.4 自动 SL时尚天窗版,chevroletex",
					"2014款 三厢 1.6 自动 SX风尚版,chevroletey","2014款 三厢 1.4 自动 SE畅悠版,chevroletez","2014款 三厢 1.4 手动 SE乐悠版,chevroleteaa",
					"2014款 三厢 1.4 自动 SL时尚版,chevroleteab","2014款 三厢 1.4 手动 SL舒享版,chevroleteac","2014款 三厢 1.4 手动 SL舒适版,chevroletead"
					],
			
			"科帕奇":["2012款 2.4 自动 城市版5座前驱,chevroletfa","2012款 2.4 自动 旗舰版7座四驱,chevroletfb","2012款 2.4 自动 豪华版5座四驱,chevroletfc",
					"2012款 2.4 自动 豪华导航版7座四驱,chevroletfd","2012款 2.4 自动 舒适导航版5座四驱,chevroletfe","2012款 2.4 自动 城市导航版5座前驱,chevroletff",
					"2013款 2.4 自动 豪华版5座四驱,chevroletfg","2013款 2.4 自动 城市版7座前驱,chevroletfh",
					"2014款 2.4 自动 旗舰版7座四驱,chevroletfi","2014款 2.4 自动 城市版7座前驱,chevroletfj","2014款 2.4 自动 城市版5座前驱,chevroletfk",
					"2014款 2.4 自动 豪华版5座四驱,chevroletfl","2015款 2.4 自动 豪华版5座四驱,chevroletfm","2015款 2.4 自动 城市版5座前驱,chevroletfn",
					"2015款 2.4 自动 旗舰版7座四驱,chevroletfo","2015款 2.4 自动 城市版7座前驱,chevroletfp"
					],
			
			"科鲁兹":["2009款 三厢 1.6 手动 SL,chevroletga","2009款 三厢 1.6 自动 SL,chevroletgb","2009款 三厢 1.8 自动 SX,chevroletgc","2009款 三厢 1.8 自动 SE,chevroletgd",
					"2009款 三厢 1.6 自动 SE,chevroletge","2009款 三厢 1.6 手动 SE,chevroletgf",
					"2011款 三厢 1.6 手动 SE WTCC,chevroletgg","2011款 三厢 1.6T 手动 变形金刚版,chevroletgh","2011款 三厢 1.6 自动 SE变形金刚版,chevroletgi",
					"2011款 三厢 1.6 手动 SE变形金刚版,chevroletgj","2011款 三厢 1.6 手动 SL,chevroletgk","2011款 三厢 1.6 自动 SE,chevroletgl",
					"2011款 三厢 1.6 手动 SE,chevroletgm",
					"2011款 三厢 1.8 自动 SE,chevroletgn","2011款 三厢 1.8 自动 SX,chevroletgo","2011款 三厢 1.6T 手动 ,chevroletgp","2011款 三厢 1.6 手动 SL天地版,chevroletgq",
					"2011款 三厢 1.6 自动 天地版,chevroletgr",
					"2012款 三厢 1.6T 手动 ,chevroletgs","2012款 三厢 1.8 自动 SX,chevroletgt","2012款 三厢 1.6 手动 SL天地版,chevroletgy","2012款 三厢 1.6 手动 SL,chevroletgv",
					"2012款 三厢 1.6 自动 SL天地版,chevroletgw","2012款 三厢 1.8 自动 SE,chevroletgx","2012款 三厢 1.6 手动 SE,chevroletgy","2012款 三厢 1.6 自动 SE,chevroletgz",
					"2013款 三厢 1.8 自动 SX,chevroletgaa","2013款 三厢 1.8 自动 SE,chevroletgab","2013款 三厢 1.6 自动 SL天地版,chevroletgac","2013款 三厢 1.6 自动 SE,chevroletgad",
					"2013款 三厢 1.6 手动 SE,chevroletgae","2013款 三厢 1.6 手动 SL天地版,chevroletgaf","2013款 三厢 1.6 手动 SL天窗版 ,chevroletgag","2013款 三厢 1.6 手动 SL ,chevroletgah",
					"2013款 三厢 1.6 自动 SE WTCC版,chevroletgai","2013款 三厢 1.8 自动 SX WTCC版,chevroletgaj","2013款 三厢 1.8 自动 SE WTCC版,chevroletgak",
					"2013款 三厢 1.6 手动 SE WTCC版,chevroletgal","2014款 三厢 1.6 自动 SL百万纪念版,chevroletgam","2014款 三厢 1.6 手动 SL百万纪念版,chevroletgan",
					"2013款 两厢 1.6T 自动 旗舰型,chevroletgao","2013款 两厢 1.6 自动 豪华型,chevroletgap","2013款 两厢 1.6 手动 豪华型,chevroletgaq",
					"2015款 三厢 1.5 自动 经典版SE,chevroletgar","2015款 三厢 1.5 手动 经典版SE,chevroletgas","2015款 三厢 1.5 手动 经典版SL,chevroletgat","2015款 三厢 1.4T 自动 旗舰版,chevroletgau",
					"2015款 三厢 1.4T 自动 精英版,chevroletgav","2015款 三厢 1.4T 手动 精英版,chevroletgaw","2015款 三厢 1.5 自动 豪华版,chevroletgax","2015款 三厢 1.5 手动 精英版,chevroletgay",
					"2015款 三厢 1.5 自动 时尚导航版,chevroletgaz","2015款 三厢 1.5 手动 时尚版,chevroletgba","2015款 两厢 1.6 自动 舒适版,chevroletgbb","2015款 两厢 1.6 手动 舒适版,chevroletgbc",
					"2016款 三厢 1.4T 自动 旗舰版,chevroletgbd","2016款 三厢 1.4T 自动 豪华版,chevroletgbe","2016款 三厢 1.5 自动 豪华版,chevroletgbf","2016款 三厢 1.5 手动 精英版,chevroletgbg",
					"2016款 三厢 1.5 自动 时尚天窗版,chevroletgbh","2016款 三厢 1.5 手动 时尚版,chevroletgbi"
					],
			
			"赛欧":["2005款 三厢 1.6 自动 SE舒适型,chevroletha","2005款 三厢 1.6 手动 SE舒适型,chevrolethb","2005款 三厢 1.6 手动 SL基本型,chevrolethc",
					"2005款 两厢 1.6 自动 SRV SX豪华型,chevrolethd","2005款 两厢 1.6 手动 SRV SE舒适型,chevrolethe","2005款 两厢 1.6 手动 SRV SL基本型,chevrolethf",
					"2009款 三厢 1.2 自动,chevrolethg","2009款 三厢 1.2 手动,chevrolethh","2009款 三厢 1.4 自动,chevrolethi","2009款 三厢 1.4 手动,chevrolethj",
					"2010款 两厢 1.2 自动 理想版,chevrolethk","2010款 两厢 1.2 手动 理想版,chevrolethl","2010款 两厢 1.4 手动 理想版,chevrolethm",
					"2010款 两厢 1.2 手动 时尚版,chevrolethn","2010款 两厢 1.4 自动 优逸版,chevroletho","2010款 两厢 1.4 手动 优逸版,chevrolethp","2010款 两厢 1.2 手动 温馨版,chevrolethq",
					"2010款 三厢 1.4 自动 优逸版,chevrolethr","2010款 三厢 1.2 手动 理想增配版,chevroleths","2010款 三厢 1.4 手动 理想版,chevroletht",
					"2010款 三厢 1.4 手动 优逸版,chevrolethu","2010款 三厢 1.2 手动 优逸版,chevrolethv","2010款 三厢 1.2 手动 温馨版,chevrolethw",
					"2010款 三厢 1.2 手动 时尚版,chevrolethx","2010款 三厢 1.2 手动 理想版,chevrolethy",
					"2010款 三厢 1.2 自动 理想版,chevrolethz","2011款 两厢 1.4 手动 幸福版,chevrolethaa","2011款 两厢 1.4 自动 幸福版,chevrolethab",
					"2011款 三厢 1.4 手动 幸福版,chevrolethac","2011款 三厢 1.4 自动 幸福版,chevrolethad",
					"2013款 两厢 1.2 手动 温馨版,chevrolethae","2013款 两厢 1.4 手动 优逸版,chevrolethaf","2013款 三厢 1.4 手动 理想版,chevrolethag",
					"2013款 三厢 1.4 手动 优逸版,chevrolethah","2013款 三厢 1.2 自动 理想版,chevrolethai","2013款 三厢 1.2 手动 SX优逸版,chevrolethaj",
					"2013款 三厢 1.2 手动 幸福版,chevrolethak","2013款 三厢 1.2 手动 温馨版,chevrolethal","2013款 三厢 1.2 手动 时尚版,chevroletham",
					"2013款 三厢 1.2 手动 理想版,chevrolethan",
					"2013款 三厢 1.4 手动 优逸幸福版,chevrolethao","2013款 三厢 1.4 手动 理想幸福版,chevrolethap","2013款 三厢 1.2 手动 时尚幸福版,chevrolethaq",
					"2013款 三厢 1.4 自动 优逸版,chevrolethar","2013款 三厢 1.4 手动 幸福版Ⅱ,chevrolethas"
					],
			
			"迈锐宝":["2012款 2.0 自动 豪华版,chevroletia","2012款 2.0 自动 舒适版,chevroletib","2012款 2.0 自动 经典版,chevroletic","2012款 2.0 自动 豪华纪念版,chevroletid",
					"2012款 2.0 自动 舒适纪念版,chevroletie","2012款 2.4 自动 旗舰版,chevroletif","2012款 2.4 自动 豪华版,chevroletig",
					"2013款 2.4 自动 旗舰版,chevroletih","2013款 2.4 自动 豪华版,chevroletii","2013款 2.0 自动 豪华版,chevroletij","2013款 2.0 自动 舒适版,chevroletik",
					"2013款 2.0 自动 经典版,chevroletil","2013款 1.6T 自动 舒适版,chevroletim","2013款 1.6T 自动 豪华版,chevroletin",
					"2014款 2.4 自动 旗舰版,chevroletio","2014款 2.4 自动 豪华版,chevroletip","2014款 2.0 自动 舒适版,chevroletiq","2014款 2.0 自动 豪华版,chevroletir",
					"2014款 1.6T 自动 舒适版,chevroletis","2014款 1.6T 自动 豪华版,chevroletit","2014款 1.6T 手动 舒适版,chevroletiu",
					"2016款 2.4 自动 旗舰版,chevroletiv","2016款 2.4 自动 豪华版,chevroletiw","2016款 2.0 自动 豪华型,chevroletix","2016款 1.6T 自动 豪华型,chevroletiy",
					"2016款 2.0 自动 舒适型,chevroletiz","2016款 1.6T 自动 舒适版,chevroletiaa"
					],
			
			"乐驰":["2005款 1.0 手动,chevroletja",
					"2006款 0.8 手动,chevroletjb","2006款 1.0 手动 基本型,chevroletjc","2006款 1.0 手动,chevroletjd","2006款 0.8 手动 实用型,chevroletje",
					"2006款 0.8 手动 舒适型,chevroletjf","2006款 0.8 手动 基本型,chevroletjg","2006款 1.0 手动 标准型,chevroletjh","2006款 0.8 手动 标准型,chevroletji",
					"2006款 0.8 手动 时尚型,chevroletjj","2006款 1.0 手动 舒适型,chevroletjk","2006款 0.8 自动 舒适型,chevroletjl",
					"2008款 1.0 手动,chevroletjm","2008款 0.8 手动 舒适型,chevroletjn","2008款 0.8 手动 时尚型,chevroletjo","2008款 0.8 手动 标准型,chevroletjp",
					"2008款 0.8 手动 豪华型,chevroletjq","2008款 0.8 手动 舒适Ⅱ型,chevroletjr","2008款 1.0 手动 豪华型,chevroletjs","2008款 1.0 手动 舒适型,chevroletjt",
					"2008款 0.8 自动 舒适型,chevroletju","2009款 1.2 手动 时尚型,chevroletjv","2009款 1.2 手动 舒适型,chevroletjw","2009款 1.2 手动 标准型,chevroletjx",
					"2009款 1.0 手动 豪华型,chevroletjy","2009款 1.0 手动 舒适型,chevroletjz","2009款 0.8 手动,chevroletjaa",
					"2010款 1.2 手动 运动版时尚型,chevroletjab","2010款 1.2 手动 运动版活力型,chevroletjac","2010款 1.2 手动 运动版优越型,chevroletjad",
					"2010款 1.2 手动 优越型,chevroletjae","2010款 1.2 手动 时尚型,chevroletjaf","2010款 1.2 手动 活力型,chevroletjag","2010款 1.0 手动 优越型,chevroletjah",
					"2010款 1.0 手动 时尚型,chevroletjai","2010款 1.0 手动 活力型,chevroletjaj"
					],
			
			"克尔维特":["2008款 Couple 6.2 手动 ,chevroletma","2008款 ZR1 6.2 手动,chevroletmb","2008款 Z06 7.0 手动,chevroletmc","2008款 敞篷 6.2 手动 敞篷,chevroletmd",
						"2009款 敞篷 6.2 手动 Grand Sport,chevroletme","2010款 Z06 7.0 手动 限量版Carbon,chevroletmf","2012款 敞篷 7.0 手动 珍藏版,chevroletmg",
						"2015款 Couple 6.2 自动 C7,chevroletmh"
						],

			"斯帕可":["2011款 1.0 自动 SE,chevroletna","2011款 1.0 手动 SE,chevroletnb"],
			
			"沃蓝达":["2012款 1.4 自动,chevroletoa"],
			
			"特使":["2008款 6.0 自动 ,chevroletpa"],
			
			"科帕奇":["2012款 2.4 自动 城市版5座前驱,chevroletqa","2012款 2.4 自动 旗舰版7座四驱,chevroletqb","2012款 2.4 自动 豪华版5座四驱,chevroletqc",
					"2012款 2.4 自动 豪华导航版7座四驱,chevroletqd","2012款 2.4 自动 舒适导航版5座四驱,chevroletqe","2012款 2.4 自动 城市导航版5座前驱,chevroletqf",
					"2013款 2.4 自动 豪华版5座四驱,chevroletqg","2013款 2.4 自动 城市版7座前驱,chevroletqh",
					"2014款 2.4 自动 旗舰版7座四驱,chevroletqi","2014款 2.4 自动 城市版7座前驱,chevroletqj","2014款 2.4 自动 城市版5座前驱,chevroletqk",
					"2014款 2.4 自动 豪华版5座四驱,chevroletql","2015款 2.4 自动 豪华版5座四驱,chevroletqm","2015款 2.4 自动 城市版5座前驱,chevroletqn","2015款 2.4 自动 旗舰版7座四驱,chevroletqo",
					"2015款 2.4 自动 城市版7座前驱,chevroletqp"
					],
			
			"科迈罗":["2009款 6.2 自动 (426HP),chevroletra","2009款 6.2 手动 (426HP),chevroletrb","2009款 3.6 自动 (323HP),chevroletrc","2009款 3.6 手动 (323HP),chevroletrd",
					"2009款 6.2 自动 SS L99,chevroletre","2009款 6.2 自动 SS LS3 ,chevroletrf","2009款 3.6 自动,chevroletrg","2011款 3.6 自动 传奇性能版,chevroletrh",
					"2012款 3.6 自动 传奇性能版,chevroletri","2012款 6.2 自动 ZL1(550HP),chevroletrj","2012款 6.2 手动 ZL1(550HP),chevroletrk",
					"2012款 3.6 自动 变形金刚限量版,chevroletrl","2015款 3.6 自动 RS限量版,chevroletrm"
					],
			
			"蒙特卡洛":["2005款 3.5 自动,chevroletta","2005款 5.3 自动,chevrolettb","2005款 3.9 自动,chevrolettc"
						],
			
			"豪放":["2007款 5.3 自动 五门版四驱,chevroletta"],
	
//"永源"
			
		"永源五星":["2012款 1.1 手动 基本型7座,yongyuanaa","2012款 1.1 手动 舒适型7座,yongyuanab","2012款 1.1 手动 豪华型7座,yongyuanac"
				],	
			
		"猎鹰":["2013款 1.6 手动 标准型前驱,yongyuanba","2013款 1.6 手动 舒适型前驱,yongyuanbb",
			    "2013款 1.6 手动 豪华型前驱,yongyuanbc","2013款 1.8 自动 豪华型前驱,yongyuanbd",
				"2013款 1.8 自动 舒适型前驱,yongyuanbe","2013款 1.8 自动 标准型前驱,yongyuanbf",
				"2013款 2.0 手动 标准型前驱,yongyuanbg","2013款 2.0 手动 舒适型前驱,yongyuanbh",
				"2013款 2.0 手动 豪华型前驱,yongyuanbi"
				],		
			
		"飞碟UFO":["2005款 2.0 手动 基本型前驱,yongyuanca","2005款 2.4 手动 基本型前驱,yongyuancb","2006款 1.6 手动 标准型前驱,yongyuancc",
				"2007款 1.6 手动 风景线标准型前驱,yongyuancd",
				"2008款 1.6 手动 风景线钻石版前驱,yongyuance","2008款 1.6 手动 风景线蓝钻版前驱,yongyuancf","2008款 1.6 手动 风景线精英版前驱,yongyuancg",
				"2008款 2.0 手动 庄威钻石版前驱,yongyuanch","2008款 2.0 手动 庄威蓝钻版前驱,yongyuanci","2008款 2.0 手动 庄威精英版前驱,yongyuancj",
				"2008款 2.4 手动 庄威钻石版前驱,yongyuanck","2008款 2.4 手动 庄威蓝钻版前驱,yongyuancl","2008款 2.4 手动 庄威精英版前驱,yongyuancm",
				"2009款 1.8 自动 A380 5门基本型前驱,yongyuancn","2009款 2.0 手动 A380钻石版前驱,yongyuanco","2009款 2.0 手动 A380精英版前驱,yongyuancp",
				"2009款 1.6 手动 A380钻石版前驱,yongyuancq","2009款 1.6 手动 A380精英版前驱,yongyuancr","2011款 1.6 自动 A380钻石版前驱,yongyuancs",
				"2013款 1.6 手动 A380 5门限量版前驱,yongyuanct","2013款 2.0 手动 A380 3门豪华版前驱,yongyuancu","2013款 2.0 手动 A380 3门舒适版前驱,yongyuancv",
				"2013款 2.0 手动 A380 3门标准版前驱,yongyuancw","2013款 1.8 自动 A380 3门豪华版前驱,yongyuancx","2013款 1.8 自动 A380 3门舒适版前驱,yongyuancy",
				"2013款 1.8 自动 A380 3门标准版前驱,yongyuancz","2013款 1.6 手动 A380 3门豪华版前驱,yongyuancaa","2013款 1.6 手动 A380 3门舒适版前驱,yongyuancab",
				"2013款 1.6 手动 A380 3门标准版前驱,yongyuancac","2013款 2.0 手动 A380 5门豪华版前驱,yongyuancad","2013款 2.0 手动 A380 5门舒适版前驱,yongyuancae",
				"2013款 2.0 手动 A380 5门标准版前驱,yongyuancaf","2013款 1.8 自动 A380 5门标准版前驱,yongyuancag","2013款 1.8 自动 A380 5门舒适版前驱,yongyuancah",
				"2013款 1.8 自动 A380 5门豪华版前驱,yongyuancai","2013款 1.6 手动 A380 5门豪华版前驱,yongyuancaj","2013款 1.6 手动 A380 5门舒适版前驱,yongyuancak",
				"2013款 1.6 手动 A380 5门标准版前驱,yongyuancal","2015款 1.5 手动 A380十周年纪念版,yongyuancam","2015款 1.5 手动 新A380精英版前驱,yongyuancan",
				"2015款 1.5 手动 新A380超值版前驱,yongyuancao"
				],	
			
// "一汽"		
			
			"特锐":["2005款 1.3 自动 周年版后驱,yiqiaa","2005款 1.3 手动 周年版后驱,yiqiab"
				],	
			
			"自由风":["2006款 2.2 手动 公务版基本型9座,yiqiba","2006款 2.2 手动 公务版基本型7座,yiqibb","2006款 2.2 手动 公务版豪华型9座,yiqibc",
					"2006款 2.2 手动 公务版豪华型7座,yiqibd","2006款 2.2 手动 公务版标准型9座,yiqibe","2006款 2.4 手动 旗舰版标准型7座,yiqibf",
					"2006款 2.2 手动 公务版标准型7座,yiqibg","2006款 2.0 手动 尊贵版基本型7座,yiqibh","2006款 2.0 手动 尊贵版基本型9座,yiqibi",
					"2006款 2.0 手动 尊贵版超豪华型7座,yiqibj","2006款 2.0 手动 尊贵版豪华型9座,yiqibk","2006款 2.0 手动 尊贵版标准型7座,yiqibl",
					"2006款 2.0 手动 尊贵版标准型9座,yiqibm","2006款 2.4 手动 旗舰版基本型7座,yiqibn","2006款 2.4 手动 旗舰版基本型9座,yiqibo",
					"2006款 2.4 手动 旗舰版豪华型7座,yiqibp","2006款 2.4 手动 旗舰版豪华型9座,yiqibq","2006款 2.4 手动 旗舰版标准型9座,yiqibr",
					"2006款 2.4 手动 旗舰版超豪华型7座,yiqibs","2006款 2.0 手动 尊贵版豪华型7座,yiqibt","2008款 2.0 手动 尊贵版基本型9座,yiqibu",
					"2008款 2.0 手动 尊贵版基本型7座,yiqibv","2008款 2.0 手动 尊贵版豪华型9座,yiqibw","2008款 2.0 手动 尊贵版豪华型7座,yiqibx",
					"2008款 2.0 手动 尊贵版超豪华型7座,yiqiby","2008款 2.0 手动 尊贵版标准型9座,yiqibz","2008款 2.0 手动 尊贵版标准型7座,yiqibaa",                  
					"2009款 2.4 手动 尊享版,yiqibab","2009款 2.4 手动 II双骏版 油气混合,yiqibac","2009款 2.0 手动 II利业版基本型,yiqibad",
					"2009款 2.0 手动 II利业版豪华型,yiqibae","2009款 2.0 手动 II利业版标准型,yiqibaf","2009款 2.0 手动 II创享版标准型,yiqibag",
					"2009款 1.8T 手动 II 柴油,yiqibah","2009款 2.0 手动 II创享版豪华型,yiqibai","2009款 2.0 手动,yiqibaj"
					],
			
			"森雅M80":["2007款 1.5 手动 CL 5座,yiqica","2007款 1.5 自动,yiqicb","2007款 1.5 自动 CX 5座,yiqicc","2007款 1.5 手动 CL 7座,yiqicd","2007款 1.5 自动 CL 7座,yiqice",
					"2007款 1.5 自动 CX 7座,yiqicf","2007款 1.5 自动 CL 5座,yiqicg","2007款 1.3 手动 CC 5座,yiqich","2008款 1.5 自动 CX 5座,yiqici","2008款 1.5 自动 CX 7座,yiqicj","2008款 1.5 自动 CL 5座,yiqick","2008款 1.5 自动 CL 7座,yiqicl",
					"2008款 1.5 手动 CL 5座,yiqicm","2008款 1.5 手动 CL 7座,yiqicn","2008款 1.3 手动 CC 5座,yiqico","2009款 1.5 手动 CL 5座,yiqicp",
					"2009款 1.5 手动 CL 7座,yiqicq","2009款 1.3 手动 CC 5座,yiqicr","2009款 1.5 自动 CX 5座,yiqics","2009款 1.5 自动 CX 7座,yiqict","2009款 1.5 自动 CL 5座,yiqicu","2009款 1.5 自动 CL 7座,yiqicv",
					"2010款 1.5 手动 CL 5座,yiqicw","2010款 1.5 手动 CC 5座,yiqicx","2010款 1.5 手动 CL 7座,yiqicy","2010款 1.3 手动 CC 7座,yiqicz",
					"2010款 1.5 自动 CX 5座,yiqicaa","2012款 1.5 手动 超值版7座,yiqicab","2012款 1.3 手动 7座,yiqicac","2012款 1.3 手动 5座,yiqicad",
					"2012款 1.3 手动 超值版5座,yiqicae","2012款 1.3 手动 超值版7座,yiqicaf","2012款 1.3 手动 CC 5座,yiqicag","2012款 1.5 手动 超值版5座,yiqicah"
					],
			
			"森雅S80":["2011款 1.3 手动 舒适型5座,yiqida","2011款 1.5 自动 豪华型7座,yiqidb","2011款 1.5 手动 豪华型7座,yiqidc","2011款 1.5 手动 舒适型5座,yiqidd",
					"2011款 1.5 手动 豪华型5座,yiqide","2011款 1.5 手动 都市型5座,yiqidf","2011款 1.5 手动 导航型5座,yiqidg","2011款 1.5 自动 精英型5座,yiqidh",
					"2011款 1.5 自动 豪华型5座,yiqidi","2011款 1.5 自动 导航型5座,yiqidj","2011款 1.3 手动 舒适型7座,yiqidk","2011款 1.5 手动 舒适型7座,yiqidl",
					"2011款 1.5 手动 都市型7座,yiqidm","2011款 1.5 自动 精英型7座,yiqidn","2011款 1.5 自动 导航型7座,yiqido","2011款 1.5 自动 精英导航型5座,yiqidp",
					"2013款 1.5 手动 时尚豪华型5座,yiqidq","2013款 1.5 手动 时尚舒适型5座,yiqidr","2013款 1.5 手动 都市时尚型5座,yiqids",
					"2013款 1.5 自动 时尚型7座,yiqidt","2013款 1.5 自动 时尚豪华型5座,yiqidu","2013款 1.5 自动 时尚型5座,yiqidv","2013款 1.5 自动 时尚豪华型7座,yiqidw",
					"2013款 1.5 手动 时尚舒适型7座,yiqidx","2013款 1.5 手动 时尚豪华型7座,yiqidy","2013款 1.5 手动 都市时尚型7座,yiqidz",                  
					"2014款 1.5 自动 至尊导航版5座,yiqidaa","2014款 1.5 自动 至尊导航版7座,yiqidab","2014款 1.5 自动 运动版7座,yiqidac",
					"2014款 1.5 自动 运动版5座,yiqidad","2014款 1.5 手动 运动舒适C版5座,yiqidae","2014款 1.5 手动 运动舒适C版7座,yiqidaf",
					"2014款 1.5 手动 运动舒适B版7座,yiqidag","2014款 1.5 手动 运动舒适B版5座,yiqidah","2014款 1.5 手动 运动都市C版5座,yiqidai",
					"2014款 1.5 手动 运动都市C版7座,yiqidaj","2014款 1.5 手动 运动都市B版7座,yiqidak","2014款 1.5 手动 运动都市B版5座,yiqidal",
					"2014款 1.5 手动 经典版7座,yiqidam","2014款 1.5 手动 经典版5座,yiqidan","2014款 1.3 手动 经典版7座,yiqidao","2014款 1.3 手动 经典版5座,yiqidap",
					"2015款 1.5 自动 都市版7座,yiqidaq","2015款 1.5 自动 都市版5座,yiqidar","2015款 1.5 手动 都市版7座,yiqidas","2015款 1.5 手动 都市版5座,yiqidat",
					"2015款 1.3 手动 标准版7座,yiqidau","2015款 1.3 手动 标准版5座,yiqidav"
					],
			
			"欧朗":["2012款 三厢  1.5 自动 尊贵型,yiqiea","2012款 三厢  1.5 自动 豪华型,yiqieb","2012款 三厢  1.5 自动 舒适型,yiqiec","2012款 三厢  1.5 手动 豪华型,yiqied",
					"2012款 三厢  1.5 手动 舒适型,yiqiee","2012款 三厢  1.5 手动 基本型,yiqief",
					"2014款 两厢  1.5 自动 舒适型,yiqieg","2014款 两厢  1.5 自动 豪华型,yiqieh","2014款 两厢  1.5 手动 豪华型,yiqiei","2014 1.5 手动 舒适型,yiqiej"
					],

			
//"英菲尼迪"		
			
			"英菲尼迪ESQ":["2014款 1.6 自动 率性版前驱,infinitiaa","2014款 1.6 自动 率臻版前驱,infinitiab","2014款 1.6T 自动 率动版四驱,infinitiac"
				],	
			
			"英菲尼迪EX":["2007款 3.5 自动 EX35后驱,infinitiba",
				"2008款 3.5 自动 EX35风华版四驱,infinitibb","2008款 3.7 自动 EX37四驱,infinitibc","2008款 3.5 自动 EX35四驱,infinitibd",
				"2008款 3.5 自动 EX35风尚版四驱,infinitibe","2009款 3.5 自动 EX35风华版四驱,infinitibf","2009款 3.5 自动 EX35风尚版四驱,infinitibg",
				"2010款 2.5 自动 EX25优雅版后驱,infinitibh","2010款 2.5 自动 EX25尊雅版后驱,infinitibi","2010款 3.5 自动 EX35四驱,infinitibj",
				"2011款 3.5 自动 EX35四驱,infinitibk","2011款 2.5 自动 EX25优雅版后驱,infinitibl","2011款 2.5 自动 EX25尊雅版后驱,infinitibm",
				"2012款 2.5 自动 EX25尊雅版四驱,infinitibn","2012款 2.5 自动 EX25优雅版四驱,infinitibo","2012款 3.7 自动 EX37四驱,infinitibp",
				"2012款 2.5 自动 EX25优雅版后驱,infinitibq","2012款 2.5 自动 EX25尊雅版后驱,infinitibr",
				"2013款 3.7 自动 EX37四驱,infinitibs","2013款 2.5 自动 EX25尊雅版四驱,infinitibt","2013款 2.5 自动 EX25优雅版四驱,infinitibu",
				"2013款 2.5 自动 EX25尊雅版后驱,infinitibv","2013款 2.5 自动 EX25优雅版后驱,infinitibw"
				],
    
			
			"英菲尼迪FX":["2007款 3.5 自动 FX35超越版,infinitica","2007款 3.5 自动 FX35标准版,infiniticb","2007款 4.5 自动 FX45巅峰版,infiniticc",
				"2009款 5.0 自动 FX50巅峰版,infiniticd","2009款 3.5 自动 FX35标准版,infinitice","2009款 3.5 自动 FX35超越版,infiniticf",                  
				"2010款 5.0 自动 FX50巅峰版,infiniticg","2010款 3.5 自动 FX35超越版,infinitich","2010款 3.5 自动 FX35标准版,infinitici",
				"2011款 5.0 自动 FX50巅峰版,infiniticj","2011款 5.0 自动 FX50金尚巅峰版,infinitick","2011款 3.5 自动 FX35金尚超越版,infiniticl",
				"2011款 3.5 自动 FX35金尚标准版,infiniticm","2011款 3.5 自动 FX35超越版,infiniticn","2011款 3.5 自动 FX35标准版,infinitico",
				"2013款 5.0 自动 FX50巅峰版,infiniticp","2013款 3.7 自动 FX37五周年限量版,infiniticq","2013款 3.7 自动 FX37超越版,infiniticr",
				"2013款 3.7 自动 FX37标准版,infinitics"
				],
			
			"英菲尼迪G":["2006款 三厢  3.5 自动 G35后驱,infinitida","2007款 三厢  3.5 自动 G35至尊版后驱,infinitidb","2007款 三厢  3.5 自动 G35标准版后驱,infinitidc",
				"2009款 敞篷版  3.7 自动 G37四驱,infinitidd","2009款 Couple 3.7 自动 G37(351HP)后驱,infinitid09a",
				"2009款 三厢  3.7 自动 G37后驱,infinitide","2010款 三厢  2.5 自动 G25运动版后驱,infinitidf","2010款 三厢  2.5 自动 G25豪华运动版后驱,infinitidg",
				"2010款 三厢  3.7 自动 G37后驱,infinitidh","2010款 Couple 3.7 自动 G37(351HP)后驱,infinitid10a",
				"2010款 敞篷版  3.7 自动 G37硬顶红木饰版后驱,infinitidi","2010款 敞篷版  3.7 自动 G37硬顶紫檀木版后驱,infinitidj",
				"2010款 敞篷版  3.7 自动 G37硬顶合金版后驱,infinitidk","2011款 Couple 3.7 自动 G37(348HP)后驱,infinitid11a",
				"2012款 敞篷版  3.7 自动 G37红木版后驱,infinitidl","2012款 敞篷版  3.7 自动 G37后驱,infinitidm","2012款 敞篷版  3.7 自动 G37(343HP)IPL后驱,infinitidn",
				"2012款 Couple 3.7 自动 G37(351HP)后驱,infinitid12a",
				"2013款 敞篷版  3.7 自动 G37红木版后驱,infinitido","2013款 敞篷版  3.7 自动 G37后驱,infinitidp",
				"2013款 三厢  3.7 自动 G37后驱,infinitidq","2013款 三厢  2.5 自动 G25运动版后驱,infinitidr","2013款 三厢  2.5 自动 G25豪华运动版后驱,infinitids",
				"2013款 三厢  2.5 自动 G25STC版限量版后驱,infinitidt","2013款 Couple 3.7 自动 G37,infinitidu"
				],
			
			"英菲尼迪JX":["2013款 3.5 自动 JX35卓越版前驱,infinitiea","2013款 3.5 自动 JX35全能版四驱,infinitieb"
				],
			
			"英菲尼迪M":["2005款 3.5 自动 M35后驱,infinitifa","2005款 4.5 自动 M45四驱,infinitifb","2005款 3.5 自动 M35四驱,infinitifc","2005款 4.5 自动 M45后驱,infinitifd",
				"2008款 3.5 自动 M35尊尚版后驱,infinitife","2008款 3.5 自动 M35标准版后驱,infinitiff","2009款 3.5 自动 M35后驱,infinitifg",     
				"2010款 3.7 自动 M37后驱,infinitifh","2011款 3.7 自动 M37后驱,infinitifi","2011款 2.5 自动 M25舒适型后驱,infinitifj",
				"2011款 2.5 自动 M25奢华型后驱,infinitifk","2011款 2.5 自动 M25豪华型后驱,infinitifl",
				"2012款 3.5 自动 M35HL奢华版后驱 油电混合,infinitifm","2012款 3.5 自动 M35HL豪华版后驱 油电混合,infinitifn","2012款 2.5 自动 M25L奢华版后驱,infinitifo",
				"2012款 2.5 自动 M25L豪华版后驱,infinitifp","2012款 2.5 自动 M25L雅致版后驱,infinitifq","2012款 2.5 自动 M25L舒适版后驱,infinitifr"
				],
			
			"英菲尼迪Q50":["2014款 3.5 自动 旗舰版 油电混合,infinitiga","2014款 3.5 自动 豪华运动版 油电混合,infinitigb","2014款 3.7 自动 豪华运动版,infinitigc",
					"2014款 3.7 自动 豪华版,infinitigd","2014款 3.7 自动 舒适版,infinitige","2014款 2.0T 自动 豪华运动版,infinitigf",
					"2014款 2.0T 自动 豪华版,infinitigg","2014款 2.0T 自动 舒适版,infinitigh"
					],
								
			"英菲尼迪Q60":["2013款 3.7 自动,infinitiha"
				],
			
			"英菲尼迪Q60S":["2013款 3.7 自动,infinitiia"
				],
			
			"英菲尼迪Q70L":["2013款 3.5 自动 奢华版油电混合,infinitija","2013款 3.5 自动 豪华版油电混合,infinitijb","2013款 2.5 自动 奢华版,infinitijc",
					"2013款 2.5 自动 豪华版,infinitijd","2013款 2.5 自动 雅致版,infinitije","2013款 2.5 自动 舒适版,infinitijf",        
					"2015款 3.5 自动 奢华版油电混合,infinitijg","2015款 3.5 自动 豪华版油电混合,infinitijh","2015款 2.5 自动 奢华版,infinitiji",
					"2015款 2.5 自动 豪华版,infinitijj","2015款 2.5 自动 悦享版,infinitijk","2015款 2.5 自动 精英版,infinitijl"
					],  
			
			"英菲尼迪QX":["2008款 5.6 自动 四驱,infinitika","2011款 5.6 自动 四驱,infinitikb","2013款 5.6 自动 四驱,infinitikc"
				],
			
			"英菲尼迪QX50(进口)":["2013款 2.5 自动 优雅版后驱,infinitila","2013款 2.5 自动 尊雅版后驱,infinitilb","2013款 2.5 自动 优雅版四驱,infinitilc",
						"2013款 2.5 自动 尊雅版四驱,infinitild","2013款 3.7 自动 四驱,infinitile"
						],
						
			"英菲尼迪QX60":["2013款 3.5 自动 卓越版前驱,infinitima","2013款 3.5 自动 全能版四驱,infinitimb","2015款 2.5T 自动 卓越版前驱 油电混合,infinitimc",
						"2015款 2.5T 自动 全能版四驱 油电混合,infinitimd"
						],
						
			"英菲尼迪QX70":["2013款 3.5 自动 标准版,infinitina","2013款 3.5 自动 超越版,infinitinb","2013款 3.7 自动 标准版,infinitinc",
					"2013款 3.7 自动 超越版,infinitind","2013款 5.0 自动 巅峰版,infinitine","2015款 3.7 自动 绝影版,infinitinf"
						],			
			
			"英菲尼迪QX80":["2013款 5.6 自动,infinitioa"
				],
				
				
			"英菲尼迪Q50L":["2015款 2.0T 自动 舒适版,infinitipa","2015款 2.0T 自动 悦享版,infinitipb","2015款 2.0T 自动 运动版,infinitipc",
					"2015款 2.0T 自动 豪华版,infinitipd","2015款 2.0T 自动 豪华运动版,infinitipe"
					],	
				
			"英菲尼迪QX50":["2015款 2.5 自动 舒适版,infinitiqa","2015款 2.5 自动 悦享版,infinitiqb","2015款 2.5 自动 豪华版,infinitiqc",
					"2015款 2.5 自动 尊享版,infinitiqd"
					],	
				
				
//"英伦"
			
			"英伦SC3":["2012款 1.3 手动 基本型,englonaa","2012款 1.3 手动 标准型,englonab","2012款 1.3 手动 舒适型,englonac",
					"2012款 1.3 手动 豪华型,englonad"
					],	
			
			"英伦SC5":["2011款 1.5 手动 尚酷版,englonba","2011款 1.5 手动 炫酷版A型,englonbb","2011款 1.5 手动 炫酷版B型,englonbc",
					"2011款 1.0 手动,englonbd","2011款 1.5 手动 炫酷版,englonac","2011款 1.5 手动 豪华版,englonbe"
					],	
			
			"英伦SC6":["2013款 1.5 手动 舒适型,englonca","2013款 1.5 手动 精英型,engloncb","2013款 1.5 手动 尊贵型,engloncc",
					"2013款 1.5 手动 进取型,engloncd"
					],	
			
			"英伦SC7":["2010款 1.5 手动 舒适型,englonda","2010款 1.5 手动 基本型,englondb","2010款 1.5 手动 标准型,englondc","2010款 1.8 手动 舒适型,englondd",
				"2010款 1.8 手动 基本型,englonde","2010款 1.8 手动 豪华型,englondf","2010款 1.8 手动 标准型,englondg","2011款 1.5 手动 标准型,englondh",
				"2011款 1.8 手动 舒适型,englondi","2011款 1.8 手动 基本型,englondj","2011款 1.8 手动 标准型,englondk","2011款 1.5 手动 舒适型,englondl",
				"2011款 1.5 手动 基本型,englondm","2013款 1.5 手动 舒适型,englondn","2013款 1.5 手动 超值型,englondo","2013款 1.5 手动 甲醇M100,englondp",
				"2013款 1.5 手动 尊贵型,englondq","2013款 1.5 手动 精英型,englondr",
				"2013款 1.5 手动 进取型,englonds","2013款 1.5 手动 超悦型,englondt","2013款 1.8 自动 尊贵型,englondu","2013款 1.8 自动 精英型,englondv"
				],
			
			"英伦SX7":["2013款 2.4 自动 尊贵型前驱,englonea","2013款 2.0 手动 尊贵型前驱,engloneb","2013款 2.0 手动 精英型前驱,englonec","2013款 1.8 手动 尊贵型前驱,engloned",
				"2013款 1.8 手动 精英型前驱,englonee","2013款 1.8 手动 进取型前驱,englonef","2014款 2.4 自动 尊贵型前驱,engloneg","2014款 2.0 手动 尊贵型前驱,engloneh",
				"2014款 2.0 自动 豪华型前驱,englonei","2014款 1.8 手动 尊贵型前驱,englonej","2014款 1.8 手动 精英型前驱,englonek","2014款 1.8 手动 进取型前驱,englonel",
				"2014款 2.0 自动 尊贵型前驱,englonem","2014款 2.0 自动 进取型前驱,englonen","2014款 2.0 手动 进取型前驱,engloneo"
				],
 
			
			"英伦TX4":["2008款 2.4 手动,englonfa","2008款 2.5T 自动 柴油,englonfb","2008款 2.5T 手动 柴油,englonfc","2009款 2.4 手动 基本型,englonfd",
			       "2009款 2.5T 手动 柴油,englonfe","2009款 2.5T 自动 柴油,englonff","2009款 2.4 手动 标配型,englonfg",
			       "2010款 2.4 手动 油电混合,englonfh","2012款 2.4 自动 定制商务型,englonfi"
					],

//"野马			
			
			"野马F10":["2011款 1.5 手动 豪华型,yemaaa","2011款 1.5 手动 尊贵型,yemaab","2011款 1.5 手动,yemaac","2011款 1.6 自动,yemaad",
			       "2012款 1.5 手动,yemaae","2012款 1.6 自动,yemaaf"
					],
			
			"野马F12":["2012款 1.5 手动 基本型前驱,yemaba","2012款 1.6 自动 基本型前驱,yemabb",
						"2014款 1.5 手动 前驱,yemabc","2014款 1.6 自动 前驱,yemabd"
					],
			
			"野马F16":["2014款 1.5 手动 精英版,yemaca","2014款 1.6 手动 精英版,yemacb",
						"2014款 1.6 自动 豪华版,yemacc"
					],
			
			"野马F99":["2009款 1.5 手动 豪华型前驱,yemada","2009款 1.5 手动 基本型前驱,yemadb","2009款 1.5 手动 旗舰型前驱,yemadc",
					  "2009款 1.5 手动 舒适型前驱,yemadd","2009款 1.5 手动 尊贵型前驱,yemade"
					],
			
			"野马T70":["2015款 1.8 手动 舒适型,yemaea","2015款 1.8 手动 精英型,yemaeb","2015款 1.8 手动 豪华型,yemaec",
					  "2015款 1.8T 自动 优雅型,yemaed","2015款 1.8T 自动 睿智型,yemaee",
					  "2015款 1.8 手动 精英型改款,yemaef","2015款 1.8T 自动 优雅型改款,yemaeg"
					],
			
//"英致"
			
			"英致G3":["2014款 1.5 手动 致惠型,yingzhiaa","2014款 1.5 手动 致雅型,yingzhiab","2014款 1.5 手动 致豪型,yingzhiac",
					  "2014款 1.5 手动 致尊型,yingzhiad","2015款 1.5 自动 致豪型,yingzhiae",
					  "2015款 1.5 自动 致尊型,yingzhiaf"
					],
			
			
			"英致737":["2015款 1.5 手动 标准版6-7座,yingzhiba","2015款 1.5 手动 舒适版6-7座,yingzhibb","2015款 1.5 手动 精英版6-7座,yingzhibc",
					  "2015款 1.5 手动 豪华导航版6-7座,yingzhibd","2015款 1.5 手动 互联网版6-7座,yingzhibe"
					],
			
//"中誉"
			
			"凌特":["2008款 2.7T 自动 豪华版 柴油,zhongyuaa",
					  "2008款 2.7T 手动 标准版 柴油,zhongyuab"
					],
			
			"威霆":["2008款 3.2 自动 加长商务型7座,zhongyuba",
					  "2008款 3.2 自动 商务型7座,zhongyubb","2011款 2.5 自动 vito3,zhongyubc"
					],
			
			"假日风情":["2007款 2.7T 自动 柴油,zhongyuca"
					],
			
			"迷你巴士":["2007款 3.5 自动,zhongyuda"
					],
			
	
// "中顺"		
		"世纪":["2005款 2.2 手动 豪华型14座,zhongshunaa","2005款 2.2 手动 标准型14座,zhongshunab",
				"2005款 2.2 手动 普通型14座,zhongshunac","2005款 2.5T 手动 11座 柴油,zhongshunad",
				"2006款 2.7T 手动 经典型11座 柴油,zhongshunae","2006款 2.7T 手动 经典型6座 柴油,zhongshunaf",
				"2006款 2.2 手动 经典型7座,zhongshunag","2006款 2.7T 手动 经典型9座 柴油,zhongshunah",
				"2006款 2.7T 手动 普通型6座 柴油,zhongshunai","2006款 2.2 手动 普通型7座,zhongshunaj",
				"2006款 2.7T 手动 普通型9座 柴油,zhongshunak","2006款 2.7T 手动 实用Ⅰ型11座 柴油,zhongshunal",
				"2006款 2.7T 手动 实用Ⅰ型6座 柴油,zhongshunam","2006款 2.2 手动 实用Ⅰ型7座,zhongshunan",
				"2006款 2.7T 手动 实用Ⅰ型9座 柴油,zhongshunao","2006款 2.7T 手动 实用Ⅱ型11座 柴油,zhongshunap",
				"2006款 2.7T 手动 实用Ⅱ型6座 柴油,zhongshunaq","2006款 2.2 手动 实用Ⅱ型7座,zhongshunar",
				"2006款 2.7T 手动 实用Ⅱ型7座 柴油,zhongshunas","2006款 2.7T 手动 实用Ⅱ型9座 柴油,zhongshunat",
				"2006款 2.7T 手动 实用Ⅲ型6座 柴油,zhongshunau","2006款 2.2 手动 实用Ⅲ型7座,zhongshunav",
				"2006款 2.7T 手动 实用Ⅲ型7座 柴油,zhongshunaw","2006款 2.7T 手动 实用Ⅲ型9座 柴油,zhongshunax",
				"2006款 2.7T 手动 实用Ⅲ型11座 柴油,zhongshunay","2006款 2.2 手动 实用Ⅲ型11座,zhongshunaz",
				"2006款 2.7T 手动 普通型11座 柴油,zhongshunaaa","2006款 2.0 手动 经典型7座,zhongshunaab",
				"2006款 2.0 手动 实用Ⅰ型7座,zhongshunaac","2006款 2.0 手动 普通型7座,zhongshunaad",
				"2006款 2.0 手动 实用Ⅱ型7座,zhongshunaae","2006款 2.0 手动 实用Ⅲ型7座,zhongshunaaf",
				"2006款 2.2 手动 经典型11座,zhongshunaag","2006款 2.2 手动 普通型11座,zhongshunaah",
				"2006款 2.2 手动 实用Ⅰ型11座,zhongshunaai","2006款 2.2 手动 实用Ⅱ型11座,zhongshunaaj",
				"2006款 2.2 手动 普通型14座,zhongshunaak","2006款 2.2 手动 实用Ⅱ型14座,zhongshunaal",
				"2006款 2.2 手动 实用Ⅲ型14座,zhongshunaam","2006款 2.2 手动 经典型6座,zhongshunaan",
				"2006款 2.2 手动 普通型6座,zhongshunaao","2006款 2.2 手动 实用Ⅰ型6座,zhongshunaap",
				"2006款 2.2 手动 实用Ⅱ型6座,zhongshunaaq","2006款 2.2 手动 实用Ⅲ型6座,zhongshunaar",
				"2006款 2.2 手动 实用Ⅲ型9座,zhongshunaas","2006款 2.2 手动 实用Ⅱ型9座,zhongshunaat",
				"2006款 2.2 手动 实用Ⅰ型9座,zhongshunaau","2006款 2.2 手动 普通型9座,zhongshunaav",
				"2006款 2.2 手动 经典型9座,zhongshunaaw","2006款 2.7T 手动 经典型7座 柴油,zhongshunaax",
				"2006款 2.7T 手动 普通型7座 柴油,zhongshunaay","2006款 2.7T 手动 实用Ⅰ型7座 柴油,zhongshunaaz",
				"2006款 2.0 手动 11座,zhongshunaba","2006款 2.0 手动 6座,zhongshunaba","2006款 2.0 手动 14座,zhongshunabb",
				"2006款 2.0 手动 9座,zhongshunabc","2006款 2.5T 手动 6-9座 柴油,zhongshunabd",
				"2006款 2.2 手动 11-14座,zhongshunabe","2007款 2.0 手动 经典型高顶7座,zhongshunabf",
				"2007款 2.0 手动 实用Ⅰ型高顶7座,zhongshunabg","2007款 2.0 手动 普通型高顶7座,zhongshunabh",
				"2007款 2.0 手动 实用Ⅱ型高顶7座,zhongshunabi","2007款 2.0 手动 实用Ⅲ型高顶7座,zhongshunabj",
				"2007款 2.2 手动 经典型高顶11座,zhongshunabk","2007款 2.2 手动 实用Ⅰ型高顶11座,zhongshunabl",
				"2007款 2.2 手动 实用Ⅱ型高顶11座,zhongshunabm","2007款 2.2 手动 实用Ⅲ型高顶11座,zhongshunabn",
				"2007款 2.0T 手动 6-9座 柴油,zhongshunabo","2008款 2.2 手动 6-9座,zhongshunabp","2009款 2.2 手动 11座,zhongshunabq"
				],
			
//"中兴"
			"中兴C3":["2014款 1.5 手动 启航版,zhongxingaa",
				  "2014款 1.5 手动 商务版,zhongxingab","2014款 1.5 手动 厂庆版,zhongxingac"
					],
			
			"无限":["2010款 2.0 手动 后驱,zhongxingba",
				  "2013款 2.0 手动 改款,zhongxingbb"
					],  
			
// "中华"		
			
			"H220":["2014款 1.5 自动 精英型,zhonghuaaa","2014款 1.5 自动 天窗型,zhonghuaab","2014款 1.5 手动 精英型,zhonghuaac","2014款 1.5 手动 酷悦型,zhonghuaad","2014款 1.5 手动 舒适型,zhonghuaae"
					],
			
			
			"H230":["2012款 1.5 自动 精英型,zhonghuaba","2012款 1.5 自动 天窗型,zhonghuabb","2012款 1.5 手动 精英型,zhonghuabc",
				  "2012款 1.5 手动 酷悦型,zhonghuabd","2012款 1.5 手动 舒适型,zhonghuabe","2012款 纯电动舒适型,zhonghuabf"
					],
					
			"H320":["2013款 1.5 自动 舒适型,zhonghuaca","2013款 1.5 自动 豪华型,zhonghuacb","2013款 1.5 手动 豪华型,zhonghuacc","2013款 1.5 手动 舒适型,zhonghuacd"
					],
			
			
			"H330":["2013款 1.5 手动 舒适型,zhonghuada","2013款 1.5 自动 舒适型,zhonghuadb","2013款 1.5 手动 豪华型,zhonghuadc",
				  "2013款 1.5 自动 豪华型,zhonghuadd","2014款 1.5 手动 油气混合,zhonghuade","2015款 1.5 手动 智享版,zhonghuadf"
					],
			
			"H530":["2012款 1.6 自动 智能型,zhonghuaea","2012款 1.5T 自动 豪华型,zhonghuaeb","2012款 1.5T 自动 精英型,zhonghuaec","2012款 1.5T 手动 舒适型,zhonghuaed",
					"2012款 1.6 手动 舒适型,zhonghuaee","2012款 1.6 手动 豪华型,zhonghuaef","2012款 1.6 手动 标准型,zhonghuaeg","2012款 1.6 自动 舒适型,zhonghuaeh",
					"2012款 1.6 自动 豪华型,zhonghuaei","2012款 1.5T 手动 豪华型,zhonghuaej","2012款 1.5T 自动 尊贵型,zhonghuaek","2012款 1.5T 自动 智能型,zhonghuael",
					"2013款 1.6 手动 舒适天窗型,zhonghuaem","2013款 1.6 自动 舒适天窗型,zhonghuaen","2013款 1.6 自动 智能型,zhonghuaeo","2013款 1.5T 自动 精英型,zhonghuaep",
					"2013款 1.6 自动 智能型油气混合,zhonghuaeq","2013款 1.6 自动 舒适天窗型油气混合,zhonghuaer",
					"2014款 1.5T 自动 智能型,zhonghuaes","2014款 1.5T 自动 豪华型,zhonghuaet","2014款 1.5T 自动 精英型,zhonghuaeu","2014款 1.5T 手动 豪华型,zhonghuaev",
					"2014款 1.5T 手动 舒适型,zhonghuaew","2014款 1.6 自动 豪华型,zhonghuaex","2014款 1.6 自动 舒适型,zhonghuaey","2014款 1.6 手动 豪华型,zhonghuaez",
					"2014款 1.6 手动 舒适型,zhonghuaeaa"
					],  
			
			"中华V5":["2012款 1.5T 自动 尊贵型前驱,zhonghuafa","2012款 1.5T 自动 豪华型前驱,zhonghuafb","2012款 1.5T 手动 运动型前驱,zhonghuafc",
					"2012款 1.5T 手动 豪华型前驱,zhonghuafd","2012款 1.5T 自动 豪华型四驱,zhonghuafe","2012款 1.5T 自动 尊贵型四驱,zhonghuaff",
					"2012款 1.5T 自动 运动型前驱,zhonghuafg","2012款 1.6 自动 尊贵型前驱,zhonghuafh","2012款 1.6 手动 舒适型前驱,zhonghuafi","2012款 1.6 手动 豪华型前驱,zhonghuafj",
					"2012款 1.6 自动 舒适型前驱,zhonghuafk","2012款 1.6 自动 豪华型前驱,zhonghuafl","2014款 1.5T 手动 豪华型前驱,zhonghuafm","2014款 1.5T 自动 运动型前驱,zhonghuafn",
					"2014款 1.5T 手动 运动型前驱,zhonghuafo","2014款 1.6 自动 豪华型前驱,zhonghuafp","2014款 1.6 自动 舒适型前驱,zhonghuafq","2014款 1.6 手动 豪华型前驱,zhonghuafr",
					"2014款 1.6 手动 舒适型前驱,zhonghuafs","2015款 1.6 手动 智享版前驱,zhonghuaft","2014款 1.5T 自动 尊贵型四驱,zhonghuafu","2014款 1.5T 自动 豪华型四驱,zhonghuafv",
					"2014款 1.5T 自动 豪华型前驱,zhonghuafw"
					],
								
			"华晨中华":["2004款 2.4 自动 豪华型,zhonghuaga"],
			
			
			"中华V3":["2015款 1.5 自动 智能型,zhonghuaha","2015款 1.5 手动 智能型,zhonghuahb","2015款 1.5T 自动 旗舰型,zhonghuahc",
					"2015款 1.5T 自动 精英型,zhonghuahd","2015款 1.5T 手动 都市型,zhonghuahe",
					"2015款 1.5 自动 精英型,zhonghuahf","2015款 1.5 自动 舒适型,zhonghuahg","2015款 1.5 手动 酷悦型,zhonghuahh",
					"2015款 1.5 手动 精英型,zhonghuahi","2015款 1.5 手动 舒适型,zhonghuahj","2015款 1.5 手动 基本型,zhonghuahk"
					],
			
			"中华豚":["2015款 1.3 手动 灵动版,zhonghuaia","2015款 1.3 手动 耀动版,zhonghuaib","2015款 1.3 手动 驭动版,zhonghuaic"
					],
			
			"尊驰":["2005款 2.4 手动 豪华型,zhonghuaja","2005款 2.0 手动 舒适型,zhonghuajb","2005款 2.4 自动 尊贵型,zhonghuajc","2005款 2.0 自动 豪华型,zhonghuajd",
					"2005款 2.0 自动 舒适型,zhonghuaje","2005款 2.0 手动 标准型,zhonghuajf","2005款 2.0 手动 豪华型,zhonghuajg",                  
					"2006款 1.8T 自动,zhonghuajh","2006款 1.8 手动,zhonghuaji","2006款 2.4 手动 豪华型,zhonghuajj","2006款 1.8 手动 舒适型,zhonghuajk",
					"2006款 1.8 手动 经典型,zhonghuajl","2006款 1.8 手动 豪华型,zhonghuajm","2006款 2.0 自动 豪华型,zhonghuajn","2006款 2.0 自动 舒适型,zhonghuajo",                  
					"2007款 2.0 手动 LPG油气混合,zhonghuajp","2007款 1.8T 自动 行政型,zhonghuajq","2007款 1.8T 自动 旗舰型,zhonghuajr","2007款 1.8T 自动 豪华型,zhonghuajs",
					"2007款 2.0 手动 舒适型,zhonghuajt",
					"2007款 2.0 手动 豪华型,zhonghuaju","2007款 1.8T 手动 标准型,zhonghuajv","2007款 1.8T 手动 豪华型,zhonghuajw","2007款 1.8T 手动 舒适型,zhonghuajx",
					"2008款 1.8 手动 白金型,zhonghuajy","2008款 2.0 自动,zhonghuajz","2008款 1.8T 手动 标准型,zhonghuajaa","2008款 1.8 手动 经典型,zhonghuajab",
					"2008款 2.0 手动 豪华型,zhonghuajac",
					"2008款 1.8 手动 豪华型,zhonghuajad","2008款 2.0 手动 舒适型,zhonghuajae","2008款 1.8 手动 舒适型,zhonghuajaf","2008款 1.8T 手动 豪华型,zhonghuajag",
					"2008款 1.8T 自动 豪华型,zhonghuajah",
					"2008款 1.8T 自动 旗舰型,zhonghuajai","2008款 1.8T 自动 行政型,zhonghuajaj","2008款 1.8T 手动 舒适型,zhonghuajak","2008款 1.8T 自动 舒适型,zhonghuajal",
					"2008款 1.8 手动 豪华型LPG油气混合,zhonghuajam","2008款 1.8 手动 舒适型LPG油气混合,zhonghuajan","2008款 1.8 手动 经典型LPG油气混合,zhonghuajao",
					"2009款 1.8T 自动 舒适型,zhonghuajap","2009款 1.8T 自动 行政型,zhonghuajaq","2009款 1.8T 自动 旗舰型,zhonghuajar","2009款 1.8T 自动 豪华型,zhonghuajas",
					"2009款 2.0 自动 舒适型,zhonghuajat","2009款 1.8T 手动 限量运动型,zhonghuajau","2009款 2.0 自动 豪华型,zhonghuajav","2009款 1.8T 手动 豪华型,zhonghuajaw",
					"2009款 1.8T 手动 舒适型,zhonghuajax",
					"2010款 2.0 自动 舒适型,zhonghuajay","2010款 2.0 自动 豪华型,zhonghuajaz","2010款 2.0 手动 舒适型,zhonghuajba","2010款 2.0 手动 豪华型,zhonghuajbb",                 
					"2011款 1.8T 自动 舒适型,zhonghuajbc","2011款 2.0 自动 豪华型,zhonghuajbd","2011款 2.0 自动 舒适型,zhonghuajbe","2011款 2.0 手动 豪华型,zhonghuajbf",
					"2011款 2.0 手动 舒适型,zhonghuajbg","2011款 1.8T 手动 舒适型,zhonghuajbh","2011款 1.8T 手动 豪华型,zhonghuajbi","2011款 1.8T 自动 行政型,zhonghuajbj",
					"2011款 1.8T 自动 旗舰型,zhonghuajbk","2011款 1.8T 自动 豪华型,zhonghuajbl"
					],
			
			"酷宝":["2009款 1.8T 自动 运动型GT,zhonghuaka","2009款 1.8T 手动 运动型GT,zhonghuakb",
					"2008款 1.8 手动 时尚型,zhonghuakc","2008款 1.8 手动 经典型,zhonghuakd","2008款 1.8T 手动 时尚型,zhonghuake","2008款 1.8T 自动 运动型,zhonghuakf",
					"2008款 1.8T 自动 时尚型,zhonghuakg","2008款 1.8T 手动 运动型,zhonghuakh"
					],
			
			"骏捷":["2005款 2.0 手动 舒适型,zhonghuala","2005款 2.0 手动 豪华型,zhonghualb","2006款 1.8T 自动 尊贵型,zhonghualc","2006款 1.6 手动 标准型,zhonghuald",
					"2006款 1.8 手动 舒适型,zhonghuale","2006款 2.0 自动 豪华型,zhonghualf","2006款 1.8 手动 豪华型,zhonghualg","2006款 2.0 自动 舒适型,zhonghualh",
					"2006款 1.6 手动 舒适型,zhonghuali","2007款 1.6 手动 舒适型,zhonghualj","2007款 1.8T 自动 尊贵型,zhonghualk","2007款 1.6 手动 豪华型,zhonghuall",
					"2007款 1.8 手动 豪华型,zhonghualm","2007款 1.8 手动 舒适型,zhonghualn",
					"2008款 1.6 手动 舒适型,zhonghualo","2008款 1.6 手动 夺冠型,zhonghualp","2008款 1.6 手动 炫酷型,zhonghualq","2008款 1.8 手动 舒适型,zhonghualr",
					"2008款 1.8 手动 豪华型,zhonghuals",
					"2008款 1.8T 手动 标准型,zhonghualt","2008款 1.8T 手动 舒适型,zhonghualu","2008款 1.6 手动 豪华型,zhonghualv","2008款 1.8T 手动 豪华型,zhonghualw",
					"2008款 1.8T 自动 尊贵型,zhonghualx",
					"2009款 1.6 手动 炫酷型,zhonghualy","2009款 1.6 手动 舒适型,zhonghualz","2009款 1.6 手动 豪华型,zhonghualaa",
					"2010款 1.6 手动 油气混合,zhonghualab","2010款 1.6 手动 新锐型,zhonghualac","2010款 1.8T 手动 豪华型,zhonghualad","2010款 1.8T 自动 尊贵型,zhonghualae",
					"2010款 1.8 手动 舒适型,zhonghualaf","2010款 1.8 手动 豪华型,zhonghualag","2010款 1.8 自动 豪华型,zhonghualah","2010款 1.6 手动 舒适型,zhonghualai",
					"2010款 1.6 手动 豪华型,zhonghualaj",
					"2012款 1.6 手动 珍藏型油气混合,zhonghualak","2015款 1.6 手动 智享型,zhonghualal","2015款 1.6 手动 经济型,zhonghualam"
					],
			
			"骏捷Cross":["2010款 1.5 手动 舒适型,zhonghuama","2010款 1.5 手动 豪华型,zhonghuamb","2010款 1.5 手动 飞炫版,zhonghuamc",
						"2010款 1.5 自动 舒适型,zhonghuamd","2010款 1.5 自动 豪华型,zhonghuame","2010款 1.5 自动 飞炫版,zhonghuamf","2009款 1.5 手动 豪华型,zhonghuamg","2009款 1.6 自动 豪华型,zhonghuamh"
						],
			
			
			
			
			"骏捷FRV":["2008款 1.3 手动 豪华型,zhonghuana","2008款 1.3 手动 舒适型,zhonghuanb","2008款 1.6 手动 舒适型,zhonghuanc","2008款 1.6 自动 豪华型,zhonghuand",
					"2008款 1.6 手动 豪华型,zhonghuane","2008款 1.6 自动 尊贵型,zhonghuanf","2010款 1.5 手动 舒适型,zhonghuang","2010款 1.5 手动 豪华型,zhonghuanh",
					"2010款 1.5 自动 舒适型,zhonghuani","2010款 1.5 自动 豪华型,zhonghuanj","2010款 1.3 手动 舒适型,zhonghuank","2010款 1.3 手动 豪华型,zhonghuanl"
					],
			
			"骏捷FSV":["2009款 1.6 自动 舒适型,zhonghuaoa","2009款 1.6 自动 豪华型,zhonghuaob","2009款 1.5 手动 舒适型,zhonghuaoc","2009款 1.5 手动 豪华型,zhonghuaod",
					"2010款 1.5 自动 运动版豪华型,zhonghuaoe","2010款 1.5 自动 舒适型,zhonghuaof","2010款 1.5 手动 经典型,zhonghuaog","2010款 1.5 手动 舒适型,zhonghuaoh",
					"2010款 1.5 手动 运动版豪华型,zhonghuaoi","2010款 1.5 手动 精英型,zhonghuaoj","2010款 1.5 自动 豪华型,zhonghuaok",                  
					"2011款 1.5 手动 舒适型油气混合,zhonghuaol","2011款 1.5 自动 舒适型油气混合,zhonghuaom","2011款 1.5 自动 豪华型,zhonghuaon",
					"2011款 1.5 自动 舒适型,zhonghuaoo","2011款 1.5 自动 油电混合型,zhonghuaop","2011款 1.6 自动 油气混合,zhonghuaoq","2011款 1.5 自动 油气混合,zhonghuaor",
					"2011款 1.5 手动 舒适型,zhonghuaos","2011款 1.5 手动 精英型,zhonghuaot","2011款 1.5 手动 经典型,zhonghuaou","2011款 1.5 手动 新锐版基本型,zhonghuaov",
					"2011款 1.5 自动 新锐版舒适型,zhonghuaow","2011款 1.5 自动 新锐版豪华型,zhonghuaox"
					],
			
			"骏捷Wagon":["2009款 2.0 自动 豪华型,zhonghuapa","2009款 1.8T 自动 尊贵型,zhonghuapb","2009款 1.8T 手动 豪华型,zhonghuapc","2009款 1.8 手动 豪华型,zhonghuapd",
						"2009款 1.8 手动 舒适型,zhonghuape","2010款 2.0 自动 豪华型,zhonghuapf","2011款 2.0 手动 舒适型,zhonghuapg","2011款 2.0 手动 豪华型,zhonghuaph","2011款 2.0 自动 豪华型,zhonghuapi",
						"2011款 1.8T 手动 豪华型,zhonghuapj","2011款 1.8T 自动 尊贵型,zhonghuapk"
						],
							
			
//众泰
		"2008":["2006款 1.3 手动 标准型,zhongtaiaa","2006款 1.3 手动 经济型,zhongtaiab","2006款 1.3 手动 时尚型,zhongtaiac","2006款 1.6 手动 标准型,zhongtaiad","2006款 1.6 手动 时尚型,zhongtaiae","2007款 1.6 手动 标准型,zhongtaiaf","2007款 1.6 手动 时尚型,zhongtaiag","2007款 1.3 手动 标准型,zhongtaiah","2007款 1.3 手动 经济型,zhongtaiai","2007款 1.3 手动 时尚型,zhongtaiaj","2007款 1.5 手动 标准型,zhongtaiak","2007款 1.5 手动 时尚型,zhongtaial","2008款 1.3 手动 经济型,zhongtaiam","2008款 1.3 手动 标准型,zhongtaian","2008款 1.3 手动 时尚型,zhongtaiao","2008款 1.5 手动 标准型,zhongtaiap","2008款 1.5 手动 时尚型,zhongtaiaq","2009款 1.3 手动 小康型,zhongtaiar","2009款 EV纯电动车,zhongtaias","2010款 1.3 手动 实用型,zhongtaiat","2010款 1.3 手动 标准型,zhongtaiau","2010款 1.3 手动 舒适型,zhongtaiav","2010款 1.5 手动 舒适型,zhongtaiax"],
				"梦迪博朗":["2009款 1.6 手动 精锐型,zhongtaiba","2009款 1.6 手动 精锐型油气混合,zhongtaibb","2009款 1.6 手动 精锐增配型,zhongtaibc"],
		
		"5008":["2008款 1.3 手动 标准型,zhongtaica","2008款 1.3 手动 时尚型,zhongtaicb","2008款 1.5 手动 标准型,zhongtaicc","2008款 1.5 手动 时尚型,zhongtaicd","2009款 轻型客车纯电动,zhongtaice","2009款 1.5 自动 乐睿,zhongtaicf","2009款 1.3 自动 乐睿,zhongtaicg","2010款 1.3 自动 乐睿舒适型,zhongtaich","2010款 1.3 自动 乐睿标准型,zhongtaici","2010款 1.3 自动 乐睿豪华型,zhongtaicj","2010款 1.5 自动 乐睿舒适型,zhongtaick","2010款 1.5 自动 乐睿豪华型,zhongtaicl","2010款 1.3 手动 乐睿标准型,zhongtaicm","2010款 1.3 手动 乐睿豪华型,zhongtaicn","2010款 1.3 手动 乐睿舒适型,zhongtaico","2010款 1.5 手动 乐睿豪华型,zhongtaicp","2010款 1.5 手动 乐睿舒适型,zhongtaicq","2010款 1.3 自动 乐睿标准增配型,zhongtaicr","2010款 1.3 自动 乐睿豪华增配型,zhongtaics","2010款 1.3 自动 乐睿舒适增配型,zhongtaict","2011款 轻型客车纯电动,zhongtaicu"],
				"M300朗悦":["2009款 1.6 手动,zhongtaida","2010款 1.6 手动 基本型5座,zhongtaidb","2010款 1.6 手动 基本型6座,zhongtaidc","2010款 1.6 手动 豪华型5座,zhongtaidd","2010款 1.6 手动 豪华型6座,zhongtaide","2010款 1.6 手动 尊贵型5座,zhongtaidf","2010款 1.6 手动 标准型5座,zhongtaidg","2010款 1.6 手动 标准型6座,zhongtaidh","2010款 1.6 手动 尊贵型6座,zhongtaidi"],
		
		"朗骏Z200":["2011款 1.5 手动 豪华型,zhongtaiea","2011款 1.5 手动 精英型,zhongtaieb","2011款 1.5 手动 舒适型,zhongtaiec","2011款 1.5 自动 豪华型,zhongtaied","2011款 1.5 自动 精英型,zhongtaiee","2011款 1.5 自动 舒适型,zhongtaief","2011款 1.3 手动 舒适型,zhongtaieg","2011款 1.3 手动 实用型,zhongtaieh","2011款 1.3 手动 豪华型,zhongtaiei","2011款 1.3 手动 精英型,zhongtaiej","2013款 1.3 手动 舒适型,zhongtaiek","2013款 1.3 手动 实用型,zhongtaiel","2013款 1.3 手动 精英型,zhongtaiem"],
		
		"郎朗Z200HB":["2011款 1.5 手动 豪华型,zhongtaifa","2011款 1.3 手动 科技型,zhongtaifb","2011款 1.3 手动 豪华型,zhongtaifc","2011款 1.3 手动 舒适型,zhongtaifd","2011款 1.5 自动 豪华型,zhongtaife","2011款 1.5 自动 科技型,zhongtaiff","2011款 1.5 自动 尊贵型,zhongtaifg","2011款 1.3 手动 尊贵型,zhongtaifh","2011款 1.5 自动 舒适型,zhongtaifi","2011款 1.5 手动 科技型,zhongtaifj","2011款 1.5 手动 舒适型,zhongtaifk","2011款 1.5 手动 尊贵型,zhongtaifl"],
		
		"Z300":["2012款 1.5 手动 舒适型,zhongtaiga","2012款 1.5 手动 精英型,zhongtaigb","2012款 1.5 手动 尊贵型,zhongtaigc","2013款 1.6 自动 精英型,zhongtaigd","2013款 1.5 手动 豪华型,zhongtaige","2013款 1.6 自动 豪华型,zhongtaigf","2013款 1.6 自动 尊贵型,zhongtaigg","2014款 1.5 手动 都市版豪华型,zhongtaigh","2014款 1.5 手动 驾值版舒适型,zhongtaigi","2014款 1.6 自动 都市版尊贵型,zhongtaigj","2014款 1.5 手动 都市版尊贵型,zhongtaigk","2014款 1.5 手动 驾值版豪华型,zhongtaigl","2014款 1.5 手动 驾值版精英型,zhongtaigm","2014款 1.5 手动 新视界版豪华型,zhongtaign","2014款 1.5 手动 新视界版时尚型,zhongtaigo","2014款 1.5 手动 新视界版尊贵型,zhongtaigp","2014款 1.6 自动 都市版豪华型,zhongtaigq","2014款 1.6 自动 都市版精英型,zhongtaigr","2014款 1.6 自动 新视界版豪华型,zhongtaigs","2014款 1.6 自动 新视界版尊贵型,zhongtaigt","2016款 1.5 手动 驾值版感恩型,zhongtaigu","2016款 1.5 手动 新视界版感恩型,zhongtaigv"],
		
		"T200":["2013款 1.3 手动 经典型,zhongtaiha","2013款 1.3 手动 都市型,zhongtaihb","2013款 1.3 手动 精英型,zhongtaihc","2013款 1.5 手动 精英型,zhongtaihd","2014款 1.5 自动 精英型,zhongtaihe"],
		
		"V10":["2012款 1.2 手动 实用型,zhongtaiia","2012款 1.2 手动 标准型,zhongtaiib","2012款 1.2 手动 舒适型,zhongtaiic"],
		
		"Z100":["2013款 1.0 手动 标准型,zhongtaija","2013款 1.0 手动 舒适型,zhongtaijb","2013款 1.0 手动 精英型,zhongtaijc"],
		
		"T600":["2014款 1.5T 手动 精英型,zhongtaika","2014款 1.5T 手动 豪华型,zhongtaikb","2014款 1.5T 手动 尊荣型,zhongtaikc","2014款 1.5T 手动 旗舰型,zhongtaikd","2014款 2.0T 自动 豪华型,zhongtaike","2014款 2.0T 自动 尊贵型,zhongtaikf","2014款 2.0T 自动 旗舰型,zhongtaikg","2014款 2.0T 手动 尊贵型,zhongtaikh","2014款 2.0T 手动 旗舰型,zhongtaiki","2014款 1.5T 手动 尊贵型,zhongtaikj","2015款 1.5T 手动 尊贵型汽车之家定制版,zhongtaikk","2015款 1.5T 手动 旗舰型汽车之家定制版,zhongtaikl","2015款 1.5T 手动 精英型,zhongtaikm","2015款 1.5T 手动 豪华型,zhongtaikn","2015款 1.5T 手动 尊贵型,zhongtaiko","2015款 1.5T 手动 旗舰型,zhongtaikp","2015款 2.0T 手动 尊贵型,zhongtaikq","2015款 2.0T 手动 旗舰型,zhongtaikr","2015款 2.0T 自动 豪华型,zhongtaiks","2015款 2.0T 自动 尊贵型,zhongtaikt","2015款 2.0T 自动 旗舰型,zhongtaiku","2015款 2.0T 手动 家用版豪华型,zhongtaikv","2015款 2.0T 手动 家用版尊贵型,zhongtaikw","2015款 2.0T 手动 家用版精英型,zhongtaikx"],
		
		"知豆":["2014款 标准型纯电动,zhongtaila"],
		
		"云100":["2014款 基本型纯电动,zhongtaima"],
		
		"Z500":["2015款 1.5T 手动 精英型,zhongtaina","2015款 1.5T 手动 豪华型,zhongtainb","2015款 1.5T 手动 尊贵型,zhongtainc","2015款 1.5T 自动 豪华型,zhongtaind","2015款 1.5T 自动 尊贵型,zhongtaine"],
		
		"大迈X5":["2015款 1.5T 手动 精英型,zhongtaioa","2015款 1.5T 手动 豪华型,zhongtaiob","2015款 1.5T 手动 旗舰型,zhongtaioc","2015款 1.5T 手动 尊贵型,zhongtaiod","2015款 1.5T 手动 至尊型,zhongtaioe","2015款 1.5T 自动 掌柜型,zhongtaiof","2015款 1.5T 自动 地主型,zhongtaiog","2015款 1.5T 自动 知县型,zhongtaioh","2015款 1.5T 自动 总督型,zhongtaioi","2015款 1.5T 自动 丞相型,zhongtaioj"],
				
			
//"中欧"
		"尊逸":["2011款 3.5 自动 C系列豪华全景玻璃,zhongouaa"],			
			
			
			
			
			
			
			
			
			
	
};