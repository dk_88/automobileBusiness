<?php
	if (!session_id()) session_start();
	//判断城市
	if(!isset($_GET['city'])) {
		header("Location: buy?city=hangzhou");
	}
	
	//默认情况的话将从ip地址获取到城市,前期的话用杭州
	
?>
<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>

<!--根据筛选条件来显示相应的车辆-->
  <script src="js/car_brand_data.js"></script>
   <script>
		//获取品牌
		function show_brand(container) {
			var content ='';
			var brand = '';
			for(var s in brand_a) {
					if(s=="A") {
						
						content+='<div class="buycarmore-brand-left"><div class="buycarmore-all-brand-list"><span>'+s+'</span><div class="buycarmore-list-brand"><ul>';
						$(brand_a[s]).each(function(i,dom){
							aArray = dom.split(',');
							q1 = aArray[0];
							q2 = aArray[1];
							content+='<li><a href="'+q2+'" onclick="javascript:car_search('+"'brand'"+',\''+q2+'\');return false">'+q1+'</a></li>';
						});
						content+='</ul></div></div>';
					}
					else if(s=="N") {
						content+='</div><div class="buycarmore-brand-right"><div class="buycarmore-all-brand-list"> <span>'+s+'</span><div class="buycarmore-list-brand"><ul>';
						$(brand_a[s]).each(function(i,dom){
							aArray = dom.split(',');
							q1 = aArray[0];
							q2 = aArray[1];
							content+='<li><a href="'+q2+'" onclick="javascript:car_search('+"'brand'"+',\''+q2+'\');return false">'+q1+'</a></li>';
						});
						content+='</ul></div></div>';
					}
					else {
						content+='<div class="buycarmore-all-brand-list" ><span>'+s+'</span><div class="buycarmore-list-brand"><ul>';
							$(brand_a[s]).each(function(i,dom){
							aArray = dom.split(',');
							q1 = aArray[0];
							q2 = aArray[1];
							content+='<li><a href="'+q2+'" onclick="javascript:car_search('+"'brand'"+',\''+q2+'\');return false">'+q1+'</a></li>';
							});
							content+='</ul></div></div>';
						}
					}
					content+='</div>';
					$(container).html(content);	
		};
		$(function(){
			show_brand(".buycarmore-brand");
			});
		
	</script>
<?php
	$city = isset($_GET['city'])?$_GET['city']:"hangzhou";
	$brand = isset($_GET['brand'])?$_GET['brand']:"";
	$class = isset($_GET['class'])?$_GET['class']:"";
	$price = isset($_GET['price'])?$_GET['price']:"";
	$filter = isset($_GET['filter'])?$_GET['filter']:"";
	//排序
	$sort = isset($_GET['sort'])?$_GET['sort']:"";
	$seq = isset($_GET['seq'])?$_GET['seq']:"asc";
	//分页参数
	$showrow = 12; //一页显示的行数
	$curpage = empty($_GET['page']) ? 1 : $_GET['page']; //当前的页,还应该处理非数字的情况
	
	
	//去掉当前url中的page
	$current_url = $_SERVER['QUERY_STRING'];
	$str_ex = explode("&",$current_url);
	if(strstr($current_url,"page")===false) {
		$url = "?".$current_url."&page={page}";
	}
	else {
		$str_page = preg_replace("/(\&page=\d+)/","&page={page}",$current_url);//已经存在page的情况下将page进行替换
		$url = "?".$str_page;
	}
	
	
	$conn = db_connect();
	$conn->query("set names utf8");
	$query = "select id,brand_name,current_price,new_price,plate_date,driving_distance,img_src from ".$car_dataset." where sell_city='".$city."'";
	if($brand!="") {
		$query.=" and car_brand='".$brand."'";
	}
	if($class!="") {
		$query.=" and car_class='".$class."'";
	
	}
	if($price!="") {
		switch($price) {
			case "p1":
				$query.=" and current_price<3";
				break;
			case "p2":
				$query.=" and current_price>2.999 and current_price<5.001";
				break;
			case "p3":
				$query.=" and current_price>4.999 and current_price<10.001";
				break;
			case "p4":
				$query.=" and current_price>9.999 and current_price<15.001";
				break;
			case "p5":
				$query.=" and current_price>14.999 and current_price<20.001";
				break;
			case "p6":
				$query.=" and current_price>19.999 and current_price<30.001";
				break;
			case "p7":
				$query.=" and current_price>29.999 and current_price<40.001";
				break;
			case "p8":
				$query.=" and current_price>39.999";
				break;
			default:
				;
		}	
	}
	if($filter!="") {
		$str_arr = explode("_",$filter);
		if($str_arr[0]!=0) {
			//车龄
			switch($str_arr[0]) {
				case "1":
					$query.=" and coty=1";
					break;
				case "2":
					$query.=" and coty=2";
					break;
				case "3":
					$query.=" and coty=3";
					break;
				case "4":
					$query.=" and coty=4";
					break;
				case "5":
					$query.=" and coty=5";
					break;
				default:
				;
			}
		}
		if($str_arr[1]!=0) {
			//里程
			switch($str_arr[1]) {
				case "1":
					$query.=" and driving_distance<3";
					break;
				case "2":
					$query.=" and driving_distance>2.999 and driving_distance<6.001";
					break;
				case "3":
					$query.=" and driving_distance>5.999 and driving_distance<10.001";
					break;
				case "4":
					$query.=" and driving_distance>9.999 and driving_distance<15.001";
					break;
				case "5":
					$query.=" and driving_distance>14.999 and driving_distance<20.001";
					break;
				case "6":
					$query.=" and driving_distance>20.001";
					break;
				default:
				;
			}
		}
		//变速箱
		if($str_arr[2]!=0) {
			switch($str_arr[2]) {
				case "1":
					$query.=" and gearbox = 1";
					break;
				case "2":
					$query.=" and gearbox = 2";
					break;
				default:
				;
			}
		}
		//排量
		if($str_arr[3]!=0) {
			switch($str_arr[3]) {
				case "1":
					$query.=" and displacement = 1";
					break;
				case "2":
					$query.=" and displacement = 2";
					break;
				case "3":
					$query.=" and displacement = 3";
					break;
				case "4":
					$query.=" and displacement = 4";
					break;
				case "5":
					$query.=" and displacement = 5";
					break;
				case "6":
					$query.=" and displacement = 6";
					break;
				case "7":
					$query.=" and displacement = 7";
					break;
				default:
				;
			}
		}
		//排放标准
		if($str_arr[4]!=0) {
			switch($str_arr[4]) {
				case "1":
					$query.=" and e_standard = 1";
					break;
				case "2":
					$query.=" and e_standard = 2";
					break;
				case "3":
					$query.=" and e_standard = 3";
					break;
				case "4":
					$query.=" and e_standard = 4";
					break;
				
				default:
				;
			}
		}
		//车辆颜色
		if($str_arr[5]!=0) {
			switch($str_arr[5]) {
				case "1":
					$query.=" and carcolor = 1";
					break;
				case "2":
					$query.=" and carcolor = 2";
					break;
				case "3":
					$query.=" and carcolor = 3";
					break;
				case "4":
					$query.=" and carcolor = 4";
					break;
				case "5":
					$query.=" and carcolor = 5";
					break;
				case "6":
					$query.=" and carcolor = 6";
					break;
				case "7":
					$query.=" and carcolor = 7";
					break;
				case "8":
					$query.=" and carcolor = 8";
					break;
				case "9":
					$query.=" and carcolor = 9";
					break;
				default:
				;
			}
		}
		
	}
	//只显示正在出售的车
	$query.=" and state = 1";
	if($sort!="") {
		switch($sort) {
			case "price":
				$query.=" order by current_price ".$seq;
				break;
			case "coty":
				$query.=" order by coty ".$seq;
				break;
			case "distance":
				$query.=" order by driving_distance ".$seq;
				break;
			default:;
		}
	}

	
	$result = $conn->query($query);
	//记录总的条数
	$total=$result->num_rows;
	if (!empty($_GET['page']) && $total != 0 && $curpage > ceil($total / $showrow))
		$curpage = ceil($total_rows / $showrow);
	//实现分页,根据sort是否存在来设置不同的order条件
	
	$query .= $sort==""?" order by id desc limit " . ($curpage - 1) * $showrow . ",$showrow;":",id desc limit " . ($curpage - 1) * $showrow . ",$showrow;";
	$result = $conn->query($query);
	//获取当前用户的收藏记录
	if(isset($_SESSION['user_id'])) {
		$result1 = $conn->query("select collect_cars from ".$users_info." where user_id='".$_SESSION['user_id']."'");
		$row1 = $result1->fetch_assoc();
		$user_collect = $row1['collect_cars'];
	}
	else
		$user_collect = "";
	
	//echo $query." ".$total." ";
//	while ($row = $result->fetch_assoc()) {
//		echo $row['driving_distance']."<br >";
//	}
?>

<script type="text/javascript" src="js/choice.js"></script>
<script type="text/javascript" src="js/collect.js"></script>
<script>
	function car_search(name,value,more) {
		
		//more参数用来记录filter当前的类别,以及记录排序的状态,0表示升序，1表示降序
		var url = window.location.href;//获取当前url
		
		url = url.replace(/(\&page=\d+)/,""); //去掉page
		var para = url.match(/([^\/]*\/){3}([^\/]*)/)[2];//获取当前地址下的各个参数
		
		var paras = para.split("&");
		//如果
		if(name!="sort") {
			for(i=0;i<paras.length;i++) {
					
					if(paras[i].indexOf("sort")!=-1) {
						//查找位置remove掉
						paras.splice(i,1);
					}
					if(paras[i].indexOf("seq")!=-1) {
						//查找位置remove掉
						paras.splice(i,1);
					}
			
				}
			para = paras.join("&");
		}
		//如果是选择品牌的话
		if(name=="filter") {
			//对filter进行特殊处理
			var filter_arr = [0,0,0,0,0,0];
			//不存在的情况
			if(para.indexOf(name)==-1) {
				filter_arr[more]=value;
				value = filter_arr.join("_");
				para+="&"+name+"="+value;//新的地址
				window.location.href=para;
			}
			//已经存在的情况下需要对其进行改变
			else {
				for(i=0;i<paras.length;i++) {
					
					if(paras[i].indexOf(name)!=-1) {
						//查找到位置并替换
						var paras_po=paras[i].split("_");
						if(more==0) {
							paras_po[0]="filter="+value;
						}
						else {
							paras_po[more]=value;
						}
						paras[i]= paras_po.join("_");
					}
				}
				para = paras.join("&");
				window.location.href=para;
			}
		}
		
		else {
			
			if(name=="brand") {
				//去掉车系的选项
				for(i=0;i<paras.length;i++) {
					
					if(paras[i].indexOf("class")!=-1) {
						
							//查找到并删除车系选项
							paras.splice(i,1);
					}
			
				}
				
			}
			//判断该参数是否已经存在
			if(para.indexOf(name)==-1) {
				//不存在的情况
				if(value!="all") {
					para+="&"+name+"="+value;//新的地址
					//如果是sort的话，根据more的值来改变当前的href
					if(name=="sort") {
						para = para+(more==0?"&seq=desc":"&seq=asc");
					}
					window.location.href=para;
				}
				else
					window.location.href=url;
			}
			else {
				//如果已存在，则修改相应的参数
				//若为sort
				
				for(i=0;i<paras.length;i++) {
					
					if(paras[i].indexOf(name)!=-1) {
						//查找到位置并替换
						if(value!="all") {
							paras[i]=name+"="+value;
						}
						else
							//若value值为all，则remove掉
							paras.splice(i,1);
					}
			
				}
				if(name=="sort") {
					for(i=0;i<paras.length;i++) {
					
						if(paras[i].indexOf("seq")!=-1) {
							//查找到位置并替换
							if(value!="all")
								paras[i]=(more==0?"seq=desc":"seq=asc");
							else
								paras.splice(i,1);
				
						}
					}
				}
				
				para = paras.join("&");
				window.location.href=para;
			}
		}
	}
	//处理筛选栏的高亮
	$(function(){
		var brand_name = "<?php echo $brand;?>";
		var brand_class = "<?php echo $class;?>";
		var car_price = "<?php echo $price;?>";
		var filter = "<?php echo $filter==""?"0_0_0_0_0_0":$filter;?>";
		var filter_str =filter.split("_");
		var car_year = filter_str[0];
		var car_distance = filter_str[1];
		var car_gearbox = filter_str[2];
		var displacement = filter_str[3];
		var e_standard = filter_str[4];
		var car_color = filter_str[5];
		
		if(brand_name=="") {
			$("#a1 a").eq(0).parent().addClass("buycar-active");
		}
		else {
			//添加高亮
			$("#a1 a[href="+brand_name+"]").parent().addClass("buycar-active");
			$(".buycarmore-brand a[href="+brand_name+"]").parent().addClass("buycar-active");
			
			//将新的车系筛选出来并放置于a2中
			var car_strad = $(".buycarmore-brand a[href="+brand_name+"]").html();
			
			var arrs = brand_class_ad[car_strad];
			
			var class_content = '';
			class_content+='<ul><li><a href="all" onclick="javascript:car_search('+"'class'"+','+"'all'"+');return false">不限</a></li>';
			$(arrs).each(function(i,dom){
				aArray = dom.split(',');
				q1 = aArray[0];
				q2 = aArray[1];
				class_content+='<li><a href="'+q2+'" onclick="javascript:car_search('+"'class'"+',\''+q2+'\');return false">'+q1+'</a></li>';
			});
			class_content+='</ul>';
			$("#a2").html(class_content);
			//将该筛选限制放置到已选条件中
			$(".buycar-condition-list").append('<li id="a_0" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'brand'"+','+"'all'"+');return false">'+car_strad+'</a></li>');
		//	alert($("#a1 a[href="+brand_name+"]").html());
		}
		if(brand_class=="") {
			$("#a2 a").eq(0).parent().addClass("buycar-active");
			
		}
		else {
			$("#a2 a[href="+brand_class+"]").parent().addClass("buycar-active");
			$(".buycar-condition-list").append('<li id="a_1" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'class'"+','+"'all'"+');return false">'+$("#a2 a[href="+brand_class+"]").html()+'</a></li>');
		}
		if(car_price=="") {
			$("#a3 a").eq(0).parent().addClass("buycar-active");
		}
		else {
			$("#a3 a[href="+car_price+"]").parent().addClass("buycar-active");
			$(".buycar-condition-list").append('<li id="a_2" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'price'"+','+"'all'"+');return false">'+$("#a3 a[href="+car_price+"]").html()+'</a></li>');
		}
		$("#a5 li").eq(car_year).addClass("buycar-active");
		if(car_year!=0) {
			$(".buycar-condition-list").append('<li id="a_3" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'filter'"+','+"'0'"+',0);return false">'+$("#a5 a[href="+car_year+"]").html()+'</a></li>');
		}
		$("#a6 li").eq(car_distance).addClass("buycar-active");
		if(car_distance!=0) {
			$(".buycar-condition-list").append('<li id="a_4" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'filter'"+','+"'0'"+',1);return false">'+$("#a6 a[href="+car_distance+"]").html()+'</a></li>');
		}
		$("#a7 li").eq(car_gearbox).addClass("buycar-active");
		if(car_gearbox!=0) {
			$(".buycar-condition-list").append('<li id="a_5" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'filter'"+','+"'0'"+',2);return false">'+$("#a7 a[href="+car_gearbox+"]").html()+'</a></li>');
		}
		$("#a8 li").eq(displacement).addClass("buycar-active");
		if(displacement!=0) {
			$(".buycar-condition-list").append('<li id="a_6" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'filter'"+','+"'0'"+',3);return false">'+$("#a8 a[href="+displacement+"]").html()+'</a></li>');
		}
		$("#a9 li").eq(e_standard).addClass("buycar-active");
		if(e_standard!=0) {
			$(".buycar-condition-list").append('<li id="a_7" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'filter'"+','+"'0'"+',4);return false">'+$("#a9 a[href="+e_standard+"]").html()+'</a></li>');
		}
		$("#a10 li").eq(car_color).addClass("buycar-active");
		if(car_color!=0) {
			$(".buycar-condition-list").append('<li id="a_8" class="buycar-active"><a href="all" onclick="javascript:car_search('+"'filter'"+','+"'0'"+',5);return false">'+$("#a10 a[href="+car_color+"]").html()+'</a></li>');
		}
		if($(".buycar-condition-list li").length==0)
			$(".buycar-font").after('<div class="buycar-condition">暂时没有选择过滤条件！</div>');
	})
	
	
		
		
	
</script>
<!--banner部分-->
<div class="buy-carbanner"></div>
<div class="buycar-choice"> 
       <div class="buycar-container" id="buycar_result">
            <div class="buycar-font">已选条件：</div>
            <!--<div class="buycar-condition">暂时没有选择过滤条件！</div>-->
            <ul class="buycar-condition-list"></ul>
            <div class="buycar-condition-clear"><a href="buy?city=hangzhou">全部清除</a></div>
       </div>      
         <div class="buycar-bg">
       <div class="buycar-brand clearfix">
           <strong class="buycar-brand-font">品牌</strong>

         	<div  class="buycar-brand-item" id="a1">
                <ul>
                    <li><a href="all" onclick="javascript:car_search('brand','all');return false">不限</a></li>
                    <li><a href="dazhong" onclick="javascript:car_search('brand','dazhong');return false">大众</a></li>
                    <li><a href="bmw" onclick="javascript:car_search('brand','bmw');return false">宝马</a></li>
                    <li><a href="benz" onclick="javascript:car_search('brand','benz');return false">奔驰</a></li>
                    <li><a href="ford" onclick="javascript:car_search('brand','ford');return false">福特</a></li>
                    <li><a href="biaozhi" onclick="javascript:car_search('brand','biaozhi');return false">标致</a></li>
                    <li><a href="audi" onclick="javascript:car_search('brand','audi');return false">奥迪</a></li>
                    <li><a href="hyundai" onclick="javascript:car_search('brand','hyundai');return false">现代</a></li>
                    <li><a href="chevrolet" onclick="javascript:car_search('brand','chevrolet');return false">雪佛兰</a></li>
                    <li><a href="mazda" onclick="javascript:car_search('brand','mazda');return false">马自达</a></li>
                    <li><a href="byd" onclick="javascript:car_search('brand','byd');return false">比亚迪</a></li>
                    <li><a href="richan" onclick="javascript:car_search('brand','richan');return false">日产</a></li>
                    <li><a href="kia" onclick="javascript:car_search('brand','kia');return false">起亚</a></li>
                    <li><a href="bieke" onclick="javascript:car_search('brand','bieke');return false">别克</a></li>
                    <li><a href="skoda" onclick="javascript:car_search('brand','skoda');return false">斯柯达</a></li>
                    <li><a href="bentian" onclick="javascript:car_search('brand','bentian');return false">本田</a></li>
                    <li><a href="landrove" onclick="javascript:car_search('brand','landrove');return false">路虎</a></li>
                    <li><a href="geely" onclick="javascript:car_search('brand','geely');return false">吉利</a></li>
                    <li><a href="mini" onclick="javascript:car_search('brand','mini');return false">迷你</a></li>
                    <li><a href="qirui" onclick="javascript:car_search('brand','qirui');return false">奇瑞</a></li>
                </ul>
           	  </div>
       		<div style="float:left; width:50px; height:35px;">
       			<a href="#" id="buycarmore"><strong>更多</strong><i class="fa fa-angle-double-down buycar-angledown"></i></a>
       		</div>
  	  </div>
        
		
  		 <div class="buycar-brand clearfix">
         <strong class="buycar-brand-font">车系</strong>
        		 <div  class="buycar-brand-item" id="a2">
                	<ul>
                        <li><a href="all" onclick="javascript:car_search('class','all');return false">不限</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=ford&class=fordc';?>">福克斯</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bmw&class=bmwa';?>">宝马3系</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bmw&class=bmwb';?>">宝马5系</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=benz&class=benzb';?>">奔驰E级</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=byd&class=bydf';?>">比亚迪F0</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=dazhong&class=dazhonga';?>">POLO</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=skoda&class=skodaa';?>">明锐</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=biaozhi&class=biaozhij';?>">标致307</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=rongwei&class=rongweib';?>">荣威550</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=hyundai&class=hyundaiab1';?>">悦动</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=biaozhi&class=biaozhid';?>">标致408</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=richan&class=richang';?>">天籁</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=chevrolet&class=chevroletg';?>">科鲁兹</a></li>
           				<li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=qirui&class=qiruig';?>">奇瑞QQ3</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=audi&class=audiq5';?>">奥迪Q5</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=landrove&class=landroved';?>">揽胜</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bjqc&class=bjqcq';?>">绅宝D60</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bentian&class=bentianf';?>">雅阁</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=geely&class=geelyd';?>">吉利SC3</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=hyundai&class=hyundaiac1';?>">朗动</a></li>
                        <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=kia&class=kiaf';?>">起亚K4</a></li>
                        
                	</ul>
           	  	</div> 
  	   </div>
       
       
       
  		 <div class="buycar-brand clearfix">
         <strong class="buycar-brand-font">价格</strong>
        		 <div  class="buycar-brand-item" id="a3">
                	<ul>
                       <li><a href="p0" onclick="javascript:car_search('price','all');return false">不限</a></li>
                       <li><a href="p1" onclick="javascript:car_search('price','p1');return false">3万以下</a></li>
                       <li><a href="p2" onclick="javascript:car_search('price','p2');return false">3-5万</a></li>
                       <li><a href="p3" onclick="javascript:car_search('price','p3');return false">5-10万</a></li>
                       <li><a href="p4" onclick="javascript:car_search('price','p4');return false">10-15万</a></li>
                       <li><a href="p5" onclick="javascript:car_search('price','p5');return false">15-20万</a></li>
                       <li><a href="p6" onclick="javascript:car_search('price','p6');return false">20-30万</a></li>
                       <li><a href="p7" onclick="javascript:car_search('price','p7');return false">30-40万</a></li>
                       <li><a href="p8" onclick="javascript:car_search('price','p8');return false">40万以上</a></li>
                	</ul>
           	  	</div>
  	   	</div>
       

       <div class="buycar-more">
          	  <strong class="buycar-brand-font">更多</strong>
               
                 

              <div class="buycar-filter clearfix">车龄<i class="fa fa-caret-down buycardown"></i>
                  <div class="buycar-wrapper" id="a5">
                        <ul>
							<li><a href="0" onclick="javascript:car_search('filter','0',0);return false">不限</a></li>
                            <li><a href="1" onclick="javascript:car_search('filter','1',0);return false">1年以内</a></li>
                            <li><a href="2" onclick="javascript:car_search('filter','2',0);return false">1-3年</a></li>
                            <li><a href="3" onclick="javascript:car_search('filter','3',0);return false">3-5年</a></li>
                            <li><a href="4" onclick="javascript:car_search('filter','4',0);return false">5-8年</a></li>
                            <li><a href="5" onclick="javascript:car_search('filter','5',0);return false">8年以上</a></li>
                        </ul>
                    </div>
                </div>
               
               <div class="buycar-filter clearfix">里程<i class="fa fa-caret-down buycardown"></i>
                     <div class="buycar-wrapper" id="a6">
                            <ul>
                                <li><a href="0" onclick="javascript:car_search('filter','0',1);return false">不限</a></li>
                                <li><a href="1" onclick="javascript:car_search('filter','1',1);return false">3万公里以下</a></li>
                                <li><a href="2" onclick="javascript:car_search('filter','2',1);return false">3-6万公里</a></li>
                                <li><a href="3" onclick="javascript:car_search('filter','3',1);return false">6-10万公里</a></li>
                                <li><a href="4" onclick="javascript:car_search('filter','4',1);return false">10-15万公里</a></li>
                                <li><a href="5" onclick="javascript:car_search('filter','5',1);return false">15-20万公里</a></li>
                                <li><a href="6" onclick="javascript:car_search('filter','6',1);return false">20万公里以上</a></li>
                            </ul>
                        </div>
                </div>
                 
             <div class="buycar-filter clearfix">变速箱<i class="fa fa-caret-down buycardown"></i>
                     <div class="buycar-wrapper" id="a7">
                            <ul>
                               <li><a href="0" onclick="javascript:car_search('filter','0',2);return false">不限</a></li>
                               <li><a href="1" onclick="javascript:car_search('filter','1',2);return false">手动</a></li>
                               <li><a href="2" onclick="javascript:car_search('filter','2',2);return false">自动</a></li>
                            </ul>
                        </div>
                 </div>
                 
                 <div class="buycar-filter clearfix">排量<i class="fa fa-caret-down buycardown"></i>
                     <div class="buycar-wrapper" id="a8">
                            <ul>
                                <li><a href="0" onclick="javascript:car_search('filter','0',3);return false">不限</a></li>
                                <li><a href="1" onclick="javascript:car_search('filter','1',3);return false">1.0L及以下</a></li>
                                <li><a href="2" onclick="javascript:car_search('filter','2',3);return false">1.1L-1.6L</a></li>
                                <li><a href="3" onclick="javascript:car_search('filter','3',3);return false">1.7L-2.0L</a></li>
                                <li><a href="4" onclick="javascript:car_search('filter','4',3);return false">2.1L-2.5L</a></li>
                                <li><a href="5" onclick="javascript:car_search('filter','5',3);return false">2.6L-3.0L</a></li>
                                <li><a href="6" onclick="javascript:car_search('filter','6',3);return false">3.1L-4.0L</a></li>
                                <li><a href="7" onclick="javascript:car_search('filter','7',3);return false">4.0L以上</a></li>
                            </ul>
                        </div>
                 </div>
                 
                 
                 <div class="buycar-filter clearfix">排放标准<i class="fa fa-caret-down buycardown"></i>
                     <div class="buycar-wrapper" id="a9" >
                            <ul>
                                <li><a href="0" onclick="javascript:car_search('filter','0',4);return false">不限</a></li>
                                <li><a href="1" onclick="javascript:car_search('filter','1',4);return false">国五</a></li>
                                <li><a href="2" onclick="javascript:car_search('filter','2',4);return false">国四及以上</a></li>
                                <li><a href="3" onclick="javascript:car_search('filter','3',4);return false">国三及以上</a></li>
                                <li><a href="4" onclick="javascript:car_search('filter','4',4);return false">国二及以上</a></li>
                            </ul>
                        </div>
                 </div>
                 
                 <div class="buycar-filter clearfix">车辆颜色<i class="fa fa-caret-down buycardown"></i>
                     <div class="buycar-wrapper buycar-color-wrapper" id="a10">
                            <ul>
                                  <li><a href="0" onclick="javascript:car_search('filter','0',5);return false">不限</a></li>
                                  <li><a href="1" onclick="javascript:car_search('filter','1',5);return false">黑色</a></li>
                                  <li><a href="2" onclick="javascript:car_search('filter','2',5);return false">白色</a></li>
                                  <li><a href="3" onclick="javascript:car_search('filter','3',5);return false">银色</a></li>
                                  <li><a href="4" onclick="javascript:car_search('filter','4',5);return false">灰色</a></li>
                                  <li><a href="5" onclick="javascript:car_search('filter','5',5);return false">红色</a></li>
                                  <li><a href="6" onclick="javascript:car_search('filter','6',5);return false">蓝色</a></li>
                                  <li><a href="7" onclick="javascript:car_search('filter','7',5);return false">黄色</a></li>
                                  <li><a href="8" onclick="javascript:car_search('filter','8',5);return false">金色</a></li>
                                  <li><a href="9" onclick="javascript:car_search('filter','9',5);return false">其他</a></li>
                            </ul>
                        </div>
                </div>

       </div>
    
   
    <div id="buycarmore-b" class="buycarmore-brand">
               
                
            </div>
    
    
   </div>    
</div>
 				
    <div class="buycar-result">
   		
        <div class="buycar-orderby">
            <ul>
                <li><a href="all" onclick="javascript:car_search('sort','all',0);return false" class="a-native">默认排序</a></li>
               
				   
					   <li class="buycara"><a href="price" onclick="javascript:car_search('sort','price',<?php if($sort=="price"&&$seq=="asc") echo 0; else echo 1;?>);return false" <?php echo $sort=="price"?'class="buycara-current"':'';?>>按价格&nbsp;<i class="fa fa-arrow-<?php if($sort=="price"&&$seq=="asc") echo "up"; else echo "down";?>"></i></a></li>
                       <li class="buycara"><a href="price" onclick="javascript:car_search('sort','coty',<?php if($sort=="coty"&&$seq=="asc") echo 0; else echo 1;?>);return false" <?php echo $sort=="coty"?'class="buycara-current"':'';?>>按车龄&nbsp;<i class="fa fa-arrow-<?php if($sort=="coty"&&$seq=="asc") echo "up"; else echo "down";?>"></i></a></li>
                       <li class="buycara"><a href="price" onclick="javascript:car_search('sort','distance',<?php if($sort=="distance"&&$seq=="asc") echo 0; else echo 1;?>);return false" <?php echo $sort=="distance"?'class="buycara-current"':'';?>>按里程&nbsp;<i class="fa fa-arrow-<?php if($sort=="distance"&&$seq=="asc") echo "up"; else echo "down";?>"></i></a></li>
				  
			  
                
                
            </ul>
        </div> 
         <div class="buycar-result-a">已经为您找到<span><?php echo $total;?></span>辆车</div>   
   </div>
  
   
   
   
   
   <div class="buycar-list">
       <?php
	       if($result->num_rows==0) {
			   echo '<div class="buycar-none"><img src="'.$SER_ADD.'/images/shopping_cart.png" />对不起，没有找到符合条件的二手车~</div>';
		   }
		   else {
	       $show_cardetail = '<ul class="buycar-car-list">';
		   while ($row = $result->fetch_assoc()) {
			   $img_src=explode(";",$row['img_src']);
			   $img_src[0]=$img_src[0]==""?"upload/lost.jpg":$img_src[0];
			   $show_cardetail.='<a href="'.$SER_ADD.'/buycar_detail?id='.$row['id'].'"><li><img src="../cheyuzhou_bg/'.$img_src[0].'"><p>'.$row['brand_name'].'</p><span>'.$row['current_price'].'万</span><div class="buycar-newprice">当前款新车价格：<span>¥'.$row['new_price'].'</span>(含税)</div><div class="buycar-line"></div><div class="buycar-detail">上牌 '.$row['plate_date'].' | 里程 '.$row['driving_distance'].'万公里</div></li></a>';
		   }
		   $show_cardetail.='</ul>';
		   
		   echo $show_cardetail;
		   }
	   ?>
               
        </ul>              
     </div>
     
     <div class="buycar-pages">
         <?php
             //显示分页
			 $page = new page($total, $showrow, $curpage, $url, 2);
			 echo $page->myde_write();
                    
          ?>
     </div>
     
<?php
	
	require_once('footer.php'); 
?>
