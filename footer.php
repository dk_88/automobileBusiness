<?php
	require_once('configure/parameter.php');
?>
  <!--底部-->
    <div class="footer">
    
          <div class="index-promise">
            <ul>
                <li>
                    <span>专业顾问陪同看车</span>
                    <p>1对1全程陪你找车看车验车</p>
                </li>
                <li>
                    <span>极致检测标准</span>
                    <p>最真实的车况检测结果</p>
                </li>
                <li>
                    <span>2年/4万公里售后质保</span>
                    <p>部分部件保额保修，省时省心</p>
                </li>
                <li>
                    <span>一站式代办服务</span>
                    <p>过户、上牌、一条龙服务</p>
                </li>
            </ul>
         </div>
    
    	<div class="footer-contain">
        	<ul class="footer-contain-left">
            	<li>
                    <ul>
                        <li><strong>关于我们</strong></li>
                        <li><a href="<?php echo $SER_ADD;?>/about">平台简介</a></li>
                        <li><a href="<?php echo $SER_ADD;?>/join_us">加入我们</a></li>
                        <li><a href="<?php echo $SER_ADD;?>/contact">联系我们</a></li>
                        <li><a href="<?php echo $SER_ADD;?>/news">车宇宙资讯</a></li>
                    </ul>
                </li>
                
                <li>
                    <ul>
                        <li><strong>交易流程</strong></li>
                        <li><a href="<?php echo $SER_ADD;?>/service">购车流程</a></li>
                        <li><a href="<?php echo $SER_ADD;?>/sell_car">卖车流程</a></li>
                        <li><a href="<?php echo $SER_ADD;?>/transferprocess">过户流程</a></li>
                    </ul>
                </li>
                
                <li>
                    <ul>
                        <li><strong>服务保障</strong></li>
                        <li><a href="<?php echo $SER_ADD;?>/problems">常见问题</a></li>
                        <li><a href="<?php echo $SER_ADD;?>/buyguide">购车指南</a></li>
                    </ul>
                </li>
               
            </ul> 
            
            <div class="footer-contain-img">
            	<img src="<?php echo $SER_ADD;?>/images/ewm.png" />
            	<strong>关注我们</strong>
                <p>随时随地查看好车</p>
				<p>精品二手车，品质更放心</p>
            </div>
            
            <div class="footer-contain-tel">
            	<img src="<?php echo $SER_ADD;?>/images/tel-ico.png" />
                <strong>400-0975-588</strong>
                <p>免费咨询（咨询、建议、投诉）</p>
				<p>周一至周五 8：00-18：00</p>
            </div>   
        </div>
    </div>
	
    <div class="bottom-version">Copyright © 2015, yuzhouche.com. All Rights Reserved. 浙ICP备 14041345号</div>
</body>
</html>