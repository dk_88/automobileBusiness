<?php
	if (!session_id()) session_start();
	//判断城市
	$city = isset($_GET['city'])?$_GET['city']:"hangzhou";
	
	//默认情况的话将从ip地址获取到城市,前期的话用杭州
	
?>
<?php
	require_once('configure/parameter.php');
	require_once('header.php');
	
?>
<!--各个页面中用来随时调用的模态对话框-->
<div class="model-dialog" id="model_dialog_clue">
	
</div>
<div class="fullbg"></div>
<script type="text/javascript" src="js/area.js"></script>
<script>
	function password_get_code() {
		
		
		var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
		 $.ajax({
				  type:"POST",
				  url:"configure/phonemessage_send.php",   //接入短信发送接口
				  cache:false,
				  data:{
					  "phone":$("#find_mobile").val(),
					  "img_code":$("#img_code").val(),
					  
					  },
				  beforeSend:function(){
					  $(".passwordcenter-message").html("验证码获取中....");
					  $(".passwordcenter-message").attr("disabled","disabled");
					  if(!/^[\u4e00-\u9fa5][\u4e00-\u9fa5][\u4e00-\u9fa5]?[\u4e00-\u9fa5]?$/.test($("#username").val())){
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">负责人姓名有误!</p>';
						  show_clue(content);
						  $(".passwordcenter-message").html("获取验证码").removeAttr("disabled");
						  return false;
					  }
					  else if(!/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test($("#find_mobile").val())) {
						  //必须把图片验证码放到php里面去
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">输入的手机号有误!</p>';
						  show_clue(content);
						  $(".passwordcenter-message").html("获取验证码").removeAttr("disabled");
						  return false;
					  }
					  
					  },
				  success:function(data){
					  if(data==0) {
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">输入的图片验证码有误!</p>';
						  $(".passwordcenter-message").html("获取验证码"); 
						  $(".passwordcenter-message").removeAttr("disabled");
					  }
					  else {
						 content+= '<p class="helpbuy-demand-success helpbuy-demand-right">短信验证码已发送，请查收!</p>';
					  	 time($(".passwordcenter-message"));
						 $("#checkImg").attr("src","validationCode.php?num="+Math.random());
					  }
					  show_clue(content);
					   },
				  
				  });
	}
		  
		  $(function(){
			 
			 $("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
	   $("#checkImg").click(function() {
		  $("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
		   });
		   
			  $("#login-form-business").validate({
	
				rules: {
				"cpass": {
                    equalTo: "#password"
                },
				img_code:{
				   remote:{
					   type:"get",
					   url:"configure/img_code_validate.php",
					   dataType:"json",
					   data:{
						   img_code:function(){return $('#img_code').val();}
					   }
				   },
			   },
		 		},
				
				
                messages: {
					 	cpass: {
                    			equalTo: "*两次输入密码不一致"
                				},
						img_code: {
							remote:"* 输入的验证码有误",
					   },
					 },
	  submitHandler: function (form) {
		  var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
		  $.ajax({
			  type:"POST",
			  url:"configure/business_check.php",
			  cache:false,
			  data:{
				  user_name:$("#username").val(),
				  user_id:$("#find_mobile").val(),
				  passwd:$("#password").val(),
				  company:$("#companyname").val(),
				  dealer_type:$(".examine-select option:selected").val(),
				  address:$("#s_province option:selected").val()+$("#s_city option:selected").val()+$("#s_county option:selected").val()+$("#address").val(),
				  },
			  beforeSend:function(){
					 
				   $(".login-submit").html("申请中...").attr('disabled',"true");
						  
					  },
			  success:function(data) {
				 
				  //申请成功
				  if(data==1) {
					window.location.href="examinenext";
				  }
				  //尚有未处理的工单
				  else if(data==2) {
					   content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">请勿反复提交申请!</p>';
					   show_clue(content);
				  	   $(".login-submit").html("立即申请").removeAttr('disabled');
					 
				  }
				  //手机验证码有误
				 else if(data==3) {
					   content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">短信验证码有误!</p>';
					   $("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
					   $("#img_code").val("");
					   $("#sms").val("");
					   show_clue(content);
				       $(".login-submit").html("立即申请").removeAttr('disabled');					   
				 }
				 //手机已经注册的情况
				 else if(data==4){
					 content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">该手机号已注册!</p>';
					 show_clue(content);
				  	 $(".login-submit").html("立即申请").removeAttr('disabled');
				 }
				  else {
				  	content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">系统忙，请稍后重试!</p>';
				  	show_clue(content);
				    $(".login-submit").html("立即申请").removeAttr('disabled');
				  }
			  },
			  });
	  },
	  
	});
			  })
	</script>
     <div class="header-three-bg">
    	<div class="header-logo">
        	<a href="<?php echo $SER_ADD;?>/index"><img src="images/index_logo.png" /></a>
        </div>
    </div>
     <div class="navbar-container">
           <div class="nav-bar">
                <ul>
                    
                    <?php
						
						$links = array();
						$links[]=array("首页","index.php","index");//或者这边直接改成根据ip地址获取
						$links[]=array("买车","buy_car.php","buy?city=".$city);
						$links[]=array("卖车","sell_car.php","sell_car");
						$links[]=array("帮买","helpbuy.php","helpbuy");
						$links[]=array("服务保障","service.php","service");
						$self_page = basename($_SERVER['PHP_SELF']);
						foreach($links as $link){
							printf('<a href="%s"><li %s>%s</li></a>' , $link[2],$self_page==$link[1]?'class="navbar-current"':'class=""',$link[0]);
								
							}
					?>
                                 
                </ul>
            </div>
      </div>
  <div class="examine-bg">
    	 <div class="examine-processbg">
            <ul class="examine-process">
                <li class="examine-native">填写公司账号信息</li>
                <li>待审核状态</li>
                <li>审核结果</li>
            </ul>
   		</div>

   <!---->
    <div class="examine-processlist">
    	<form class="login-form examine-form" id="login-form-business">
          <div class="examine-group">
              <div class="examine-title">实际负责人姓名</div>
              <input type="username"  id="username" name="username" class="examine-control" required>
          </div>
          <div class="examine-messge">* 请填写店主、企业法人、实际门店负责人姓名</div>
          
          
          <div class="examine-group">
              <div class="examine-title">手机号码</div>
              <input type="mobile" id="find_mobile" name="mobile" class="examine-control" required>
          </div>
          <div class="examine-messge">* 手机号将作为登录的用户名</div>
          
          <div class="examine-group">
          	  <div class="examine-title">图形验证码</div>                                                                              
                  <input type="img_code" required class="examine-control examinecode" name="img_code" id="img_code">
                  <img class="helpbuy-img-code examine-img-code" id="checkImg" />
   
          </div>
  
          <div class="examine-group">
              <div class="examine-title">短信验证码</div>
              <input type="code" id="sms" name="sms" class="examine-control examinecode" required>
              <button class="examine-message passwordcenter-message" onclick="password_get_code()">获取验证码</button>
          </div>
          <div class="examine-group">
              <div class="examine-title">登录密码</div>
              <input type="password" name="password" id="password" class="examine-control" required>
              </div>
          <div class="examine-messge">*密码必须同时包含数字和字母（8-20位）</div>
          
          
          <div class="examine-group">
              <div class="examine-title">确认密码</div>
              <input type="password" name="cpass" id="cpass" class="examine-control" required>
          </div>
       
          
          <div class="examine-group">
              <div class="examine-title">公司名称</div>
              <input type="companyname" name="companyname" class="examine-control" id="companyname" required>
          </div>
          
          <div class="examine-group">
              <div class="examine-title">经销商类型</div>
              <select name="dealertype" class="examine-control examine-select" >
                  <option value="1">市场内经营</option>
                  <option value="2">市场外独立展厅</option>
                  <option value="3">4S店</option>
              </select>
          </div>
          
          <div class="examine-group">
              <div class="examine-title">经销商地址</div>
              <select id="s_province" name="s_province" class="examine_province"></select>  
              <select id="s_city" name="s_city" class="examine_city"></select>
              <select id="s_county" name="s_county" class="examine_city"></select>
          	  <script type="text/javascript">_init_area();</script>
          </div>
          <div class="examine-group">
          <div class="examine-title">&nbsp;</div>
          <input type="address" name="address" class="examine-control" id="address" required placeholder="详细地址"> 
          </div>
          
          
          <div class="examine-messge">* 请详细填写公司地址，以便审核人员进行核实</div>
          
          <div class="examine-submit"><input type="submit" class="login-submit" value="立即申请" ></div>
          <div class="examine-messge">如有疑问，可拨打电话<u>400695578</u></div>
          
        </form>
    </div>
    </div>
<?php
	
	require_once('footer.php'); 
?>