<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
?>
 <script>
    	$(function(){
			$(".password-input li:even").addClass("evenwidth")
			$(".password-input li:odd").addClass("oddwidth");
			//获取图片验证码
			$("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
	   		$("#checkImg").click(function() {
		  		$("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
			});
			//表格验证
			
			$("#retrievepasswordForm").validate({
			errorPlacement: function(error, element) {                               
                             
 		
		error.appendTo(element.parent());
 },
 			rules: {
			   img_code:{
				   remote:{
					   type:"get",
					   url:"configure/img_code_validate.php",
					   dataType:"json",
					   data:{
						   img_code:function(){return $('#img_code').val();}
					   }
				   },
			   },
			   mobile: {
				   //检验该手机号是否已注册
				    remote:{
					   type:"get",
					   url:"configure/user_id_exist.php",
					   dataType:"json",
					   data:{
						   mobile:function(){return $('#find_mobile').val();}
					   }
				   },
			   },
		   },
		   messages: {
		    img_code: {
			    remote:"* 输入的验证码有误！点击图片可重新获取",
		   },
		    mobile: {
				remote:"* 该手机号暂未注册，请直接注册！",
			},
		   },
			
			submitHandler: function(form) {
				
				 //验证通过后 的js代码写在这里
				 var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				 $.ajax({
					 //进行最后的验证，并把新的密码写入到该用户名中
					 type:"POST",
			  		 url:"configure/change_password.php",
			  		 cache:false,
					 data:{
						 "user_id":$("#find_mobile").val(),
						 "mobile_code":$("#find_code").val(),
						 "passwd":$("#find_password").val(),
					 },
					 	 
			  		 beforeSend:function(){
						 $(".passwordcenter-login").html("提交中...").attr('disabled',"true");
					 },
				     success:function(data) {
						 
						 if(data==1) {
						 content+= '<p class="helpbuy-demand-success">新密码已生效，请妥善保管!</p><p class="helpbuy-around"><a href="javascript:show_model(0)" >马上去登录<<</a></p>';
						 }
						 //手机号暂未注册的情况
						 else if(data==2) {
							 content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">该手机号暂未注册!</p>';
						 }
						 //手机验证码出错
						 else if(data==3){
							 content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">手机验证码有误，请重新输入!</p>';
						 }
						 //其他
						 else {
							 content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">系统繁忙，请稍候再试!</p>';
						 }
						 show_clue(content);
						 $(".passwordcenter-login").html("完成").removeAttr("disabled");
					 },
				 });
			}
		})
			})
			//判断是否能够获取手机验证码
	function password_get_code(){
		 var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
		 $.ajax({
				  type:"POST",
				  url:"configure/phonemessage_send.php",   //接入短信发送接口
				  cache:false,
				  data:{
					  "phone":$("#find_mobile").val(),
					  "img_code":$("#img_code").val(),
					  
					  },
				  beforeSend:function(){
					  $(".passwordcenter-message").html("验证码获取中....");
					  $(".passwordcenter-message").attr("disabled","disabled");
					  if(!/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test($("#find_mobile").val())) {
						  //必须把图片验证码放到php里面去
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">输入的手机号有误!</p>';
						  show_clue(content);
						  $(".passwordcenter-message").html("获取验证码").removeAttr("disabled");
						  return false;
					  }
					  
					  },
				  success:function(data){
					  if(data==0) {
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">输入的图片验证码有误!</p>';
						  $(".passwordcenter-message").html("获取验证码");
						  $(".passwordcenter-message").removeAttr("disabled");
					  }
					  else {
						 content+= '<p class="helpbuy-demand-success helpbuy-demand-right">短信验证码已发送，请查收!</p>';
					  	 time($(".passwordcenter-message"));
						 $("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
					  }
					  show_clue(content);
					   },
				  
				  });
	}
    
    </script>
<div class="password-title">首页>找回密码</div>
     <div class="password-content">
         <form class="password-form" id="retrievepasswordForm">
            <ul class="password-input">
                <li>手机号码：</li>
                <li><input id="find_mobile" type="mobile" class="form-control" name="mobile" required="required" /></li>
            </ul>
            
            <ul class="password-input">
                <li>图形验证码：</li>
                <li>
                    <div class="password-input-code"><input type="img_code" class="form-control" required="required" name="img_code" id="img_code" /></div>
                    <div class="passwordcenter-code"><img class="helpbuy-img-code" id="checkImg"></div>
                </li>
            </ul>
            
            <ul class="password-input">
                <li>手机验证码：</li>
                <li>
                    <div class="password-input-code"><input type="code" class="form-control" required="required" name="code" id="find_code"/></div>
                    <div class="passwordcenter-code">
                        <button class="passwordcenter-message" onClick="password_get_code()" >获取验证码</button>
                    </div>
                </li>
            </ul>
            
            <ul class="password-input">
                <li>新密码：</li>
                <li><input type="password" class="form-control"  required="required" name="password" id="find_password" /></li>
            </ul>
            
            <!--<ul class="password-input">
                <li>确认密码：</li>
                <li><input type="password" class="form-control" required="required" name="confirm-password" id="confirm-password" /></li>
            </ul>
-->
            <ul class="password-input">
                <li>&nbsp;</li>
                <li><button class="form-control password-done passwordcenter-login" type="submit">完成</button> </li>  
            </ul>
         </form>	
     </div>
</div> 

<?php
	require_once('footer.php');
?>