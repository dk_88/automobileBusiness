<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
 <script type="text/javascript" src="js/limitstandard.js"></script>	
<div class="introduction-bg"></div>

        <div class="introduction-item">
            <div class="introduction-left">
                <div class="introduction-left-top"><i class="fa fa-files-o"></i>车宇宙百科</div>
                <?php require_once('problems_navbar.php');?>
            </div>
            
            <div class="introduction-right">
                 <div class="introduction-right-locatin">车宇宙百科>迁入标准</div>
                <div class="introduction-right-content">
                	<div class="limitstandard">
                    	<strong>按地区选择:</strong>
                         <select class="areaselect" id="province" name="province" >
                         	<option value="0">浙江省</option>
                            <option value="1">北京市</option>
                            <option value="2">天津市</option>
                            <option value="3">上海市</option>
                            <option value="5">重庆市</option>
                            <option value="6">云南省</option>
                            <option value="7">广东省</option>
                            <option value="8">江苏省</option>
                            <option value="9">河北省</option>
                            <option value="10">山西省</option>
                            <option value="11">内蒙古自治区</option>
                            <option value="12">辽宁省</option>
                            <option value="13">吉林省</option>
                            <option value="14">黑龙江省</option>
                            <option value="15">福建省</option>
                            <option value="16">江西省</option>
                            <option value="17">山东省</option>
                            <option value="18">河南省</option>
                            <option value="19">湖北省</option>
                            <option value="20">湖南省</option>
                            <option value="21">广西壮族自治区</option>
                            <option value="22">海南省</option>
                            <option value="23">四川省</option>
                            <option value="24">贵州省</option>
                            <option value="25">西藏自治区</option>
                            <option value="26">陕西省</option>
                            <option value="27">青海省</option>
                            <option value="28">安徽省</option>
                            <option value="29">甘肃省</option>
                            <option value="30">宁夏自治区</option>
                            <option value="31">新疆自治区</option>
                        </select>
                        <div class="result">
                            <strong>搜索结果:</strong>
                            <span id="prov">浙江省</span>
                        </div>    
                    </div>
                     <ul  id="carstander" class="carstander">
                            <li>杭州：国4排放标准;不符合标准的不得转籍</li>
                            <li>嘉兴：国4排放标准;不符合标准的不得转籍</li>
                            <li>金华：国4排放标准;不符合标准的不得转籍</li>
                            <li>丽水：国4排放标准;不符合标准的不得转籍</li>
                            <li>湖州：国4排放标准;不符合标准的，需买方写保证</li>
                            <li>衢州：国4排放标准;不符合标准的不得转籍</li>
                            <li>台州：国4排放标准;不符合标准的不得转籍</li>
                            <li>绍兴：国4排放标准;不符合标准的不得转籍</li>
                            <li>舟山：国3排放标准;不符合标准的，需买方写保证</li>
                            <li>宁波：国4排放标准;不符合标准的不得转籍</li>
                            <li>温州：国4排放标准;不符合标准的不得转籍</li>
                     </ul>
          
                </div>    
            </div>                                                                      
        </div>
 
	<script type="text/javascript">
		$(function(){
		   $("#province").change(function(){
			 var me = $(this);
			 var class_content = '';
			 var class_string=$("#province option:selected").text();
			 if(me.val()!='') {
				 var arr = carstander[class_string];
				$(arr).each(function(i,dom){
				  var aArray = dom.split(','),
				  q1=aArray[0];
				  class_content+='<li>'+q1+'</li>';
				  });
				  $("#carstander").html(class_content);
				  $("#prov").text(class_string);
			 }
			 else{
				  $("#carstander").html("搜索结果");
				 }
			 });

		});

       </script>
<!--底部-->
<?php
	require_once('footer.php');
?>