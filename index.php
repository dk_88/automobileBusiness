<?php
	require_once('configure/parameter.php');
	require_once('header.php');
	require_once('configure/db_fns.php');
	require_once('navbar.php');
?>


<script src="js/car_brand_data.js"></script>
<script type="text/javascript">

 
  $(function() {
      //banner
      $('.banner').flexslider({
          directionNav: true,
          pauseOnAction: false
      });

      //获取品牌
	  show_brand(".buycarmore-brand-a");
      show_brand(".buycarmore-brand");
      $(".index-brand_more").hover(function(){
          $(".buycarmore-brand-a").show();
          },function(){
          $(".buycarmore-brand-a").hide();
      });

      
      //推荐车款切换
      $(".index-car-navbar li").mouseenter(function(){
          $(this).addClass("index-car-current");
          $(".index-car-navbar li").not(this).removeClass("index-car-current");
          var list_order=$('.index-car-order').index(this);
          $(".index-car-sell-tab").hide().eq(list_order).show();
          });
      //锚点设置
      $(".index-sell-brand-list li").click(function(){
  
          $(".index-brand-con1-right").scrollTop(100);
          });

          $(".index-sell-brand").click(function(event){
          //取消事件冒泡  
          event.stopPropagation();  
          $(".index-sell-brand-con1").toggle();
           return false;
          })
          
          //点击空白处隐藏弹
       $(document).click(function(event){
            var _con = $(".index-sell-brand-con1");   // 设置目标区域
            if(!_con.is(event.target) && _con.has(event.target).length == 0){ // Mark 1
              //$('#buycarmore-b').slideUp('slow');   //滑动消失
              $(".index-sell-brand-con1").hide();          //淡出消失
            }
      });
          
      //卖车部分汽车品牌显示
      show_brand(".index-brand-con1-right",0);
      //买车栏汽车品牌展示
      show_brand(".index-brand-detail",1);
      //买家栏车型展示

      });

  
//卖车界面得到相应的汽车牌子
  function get_brand(brand_name) {
      $(".index-sell-brand-content").val($(brand_name).html());
      $(".index-sell-brand-con1").hide();
      
  }
  
  function show_brand(container,type) {
      var content ='';
      var brand = '';
      //卖车栏
      if(type==0) {
          for(var s in brand_a) {
              content+= '<span class="index-brand-flag">'+s+'</span>';
              $(brand_a[s]).each(function(i,dom){
                  var aArray = dom.split(','),
                  q1 = aArray[0],
                  q2 = aArray[1];
                  content+='<p class="brand-item"><a href="javascript:void(0)" onClick="get_brand(this)">'+q1+'</a></p>';
                  });
          }
      }
      //买车栏
      else {
          for(var s in brand_a) {
              if(s=="A") {
                  content+='<div class="index-brand-left"><div class="index-brand-item"><div class="index-brand-title">'+s+'</div><div class="index-brand-name">';
                  
                  $(brand_a[s]).each(function(i,dom){
                      aArray = dom.split(',');
                      q1 = aArray[0];
                      q2 = aArray[1];
                      content+='<a href="buy?city=hangzhou&brand='+q2+'">'+q1+'</a>';
                  });
                  content+='</div><div class="clear"></div></div>';
              }
              else if(s=="M") {
                  content+='</div><div class="index-brand-right"><div class="index-brand-item"><div class="index-brand-title">'+s+'</div><div class="index-brand-name">';
                  $(brand_a[s]).each(function(i,dom){
                      aArray = dom.split(',');
                      q1 = aArray[0];
                      q2 = aArray[1];
                      content+='<a href="buy?city=hangzhou&brand='+q2+'">'+q1+'</a>';
                  });
                  content+='</div><div class="clear"></div></div>';
              }
              else {
                  content+='<div class="index-brand-item"><div class="index-brand-title">'+s+'</div><div class="index-brand-name">';
                  $(brand_a[s]).each(function(i,dom){
                  aArray = dom.split(',');
                  q1 = aArray[0];
                  q2 = aArray[1];
                  content+='<a href="buy?city=hangzhou&brand='+q2+'">'+q1+'</a>';
                  });
                  content+='</div><div class="clear"></div></div>';
              }
          }
          content+='</div>';

      }
      
      $(container).html(content);	
  }
  
  
  function show_class(container) {
      var content ='';
      
      for(var s in car_type) {
          content+='<div class="index-model-class"><div class="index-model-title"><span>'+s+'</span></div><div class="index-model-name">';
          $(car_type[s]).each(function(i,dom){
              var aArray = dom.split(','),
              q1 = aArray[0],
              q2 = aArray[1];
              content+='<a href="buy?city=hangzhou&class='+q2+'">'+q1+'</a>';
              });
          content+='</div><div class="clear"></div></div>';
          
      }

      $(container).html(content);	
  } 
</script>
<?php
	//获取服务人数和已售车辆
	$conn = db_connect();
	$conn->query("set names utf8");
	$result = $conn->query('select id from '.$users);
	//保存服务人数
	$service_num=$result->num_rows;
	
	//对服务人数进行处理
	for($i=0,$service_arr=array(0,0,0,0,0,0),$flag=1;$flag>0;$i++) {
		$service_arr[$i]=$service_num%(10^($i+1));
		$flag=intval(floor($service_num/(10^($i))));
	}
	$result1 = $conn->query('select id from '.$car_dataset.' where state=2');
	//保存已售车辆数
	$sell_num = $result1->num_rows;
	
?>
  <div class="body-bg">	
    <!-- 轮播-->

    <div class="banner">
        <ul class="slides">
            <li style="background:url(<?php echo $SER_ADD;?>/images/banner1.jpg) 50% 0 no-repeat;"></li>
            <li style="background:url(<?php echo $SER_ADD;?>/images/banner2.jpg) 50% 0 no-repeat;"></li>
            <li style="background:url(<?php echo $SER_ADD;?>/images/banner3.jpg) 50% 0 no-repeat;"></li>
            <li style="background:url(<?php echo $SER_ADD;?>/images/banner4.jpg) 50% 0 no-repeat;"></li>
        </ul>
	</div>
	
      <div class="index-contain-all">
          <!-- 左侧-->
              <div class="index-contain-left">
              <div class="index-contain-left-top"><img src="<?php echo $SER_ADD;?>/images/index_y.png" /><span>价格</span></div>
              <ul class="index-contain-left-topa">
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city;?>">不限</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p1';?>">3万以下</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p2';?>">3-5万</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p3';?>">5-10万</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p4';?>">10-15万</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p5';?>">15-20万</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p6';?>">20-30万</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p7';?>">30-40万</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p8';?>">40万以上</a></li>
              </ul>
              <div class="index_line"></div>
              
              <div class="index-contain-left-top index-brand_more">
              		<img src="<?php echo $SER_ADD;?>/images/index_car.png" /><span>品牌 <i class="fa fa-angle-right"></i></span>
               		<div class="buycarmore-brand-a"></div>
              </div>
              <ul class="index-contain-left-topa">
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=dazhong';?>">大众</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bwm';?>">宝马</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=audi';?>">奥迪</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=benz';?>">奔驰</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=yiqi';?>">一汽</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=zhongxing';?>">中兴</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=changfeng';?>">长丰</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bentian';?>">本田</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=geely';?>">吉利</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=zhonghua';?>">中华</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=qirui';?>">奇瑞</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=hyundai';?>">现代</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=byd';?>">比亚迪</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=mazda';?>">马自达</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=chevrolet';?>">雪佛兰</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=rollsroyce';?>">劳斯莱斯</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=ferray';?>">法拉利</a></li> 
                  
              </ul>
				
              <div class="index_line"></div>
              
              <div class="index-contain-left-top"><img src="<?php echo $SER_ADD;?>/images/index_lc.png" /><span>车系</span></div>
              <ul class="index-contain-left-topa">
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=ford&class=fordc';?>">福克斯</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bmw&class=bmwb';?>">宝马5系</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=benz&class=benzb';?>">奔驰E级</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=rongwei&class=rongweib';?>">荣威550</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=dazhong&class=dazhongd';?>">朗逸</a></li>
                  <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=biaozhi&class=biaozhid';?>">标致408</a></li>
              </ul>  
          </div>
          
         
         <!-- 右侧-->
         <div class="index-contain-right">
              <div class="index-contain-righttop"><strong>目前平台已成功售出二手车</strong></div>
              <div class="index-contain-rightnum">
                  <div class="numberRun4">
                  		<span>0</span>
                        <span>0</span>
                        <span>0</span>
                        <span>6</span>
                        <span>5</span>
                        <span>2</span>
                  </div>
                  <p></p>
                  <p>销售额共计<span>7819</span>万元人民币</p>
              </div>

              <div class="index_line_dashed"></div>
              <div class="index-contain-rightsell">
                  <h2>出售爱车</h2>
                  <div class="index-sell-brand">
                    <input id="brand_name" type="text" value="车辆品牌" placeholder="车辆品牌" class="index-sell-brand-content" readonly></input>
                    <span class="index-sell-triangle"></span>
                  </div>
                  
                  <div class="index-sell-brand-con1">
                      <div class="index-brand-con1-right"></div>
                  </div>
                  
                  <form id="index-help-sell">
                      <div class="index-sell-num">
                          <input type="mobile" class="form-control required" placeholder="手机号" id="appointment_mobile">
                      </div>
                     <button class="appoint-now">马上预约</button>
                   </form>
                   <div id="summary" class="messageBox"></div>
                   
                   <div class="index-sell-all"><a href="<?php echo $SER_ADD;?>/appraise">估价计算器 <i class="fa fa-angle-double-right"></i></a></div> 
              </div>
         </div>
    	</div>
  		

      <div class="index-part">
      		<span>精品豪车馆</span>
            <p>LUXURY CAR</p>            
      </div>
      
      <div class="index-hc-list">
      		<div class="index-hc-list-right-d">
            	<a href="<?php echo $SER_ADD.'/buy?city='.$city;?>"><img src="<?php echo $SER_ADD;?>/images/haoche_1.png" alt="车宇宙豪车馆" title="车宇宙豪车馆" /></a>
            </div>

        	<div class="index-hc-list-right-a">
            	<ul>
                	
            		<a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bmw';?>">
                        <li class="index-hc-list-right-atop">
                        	<span>宝马精品馆</span>
                            <p>高速、稳定、舒适</p>
                            <img src="<?php echo $SER_ADD;?>/images/bmw1.png" />
                        </li>
                    </a>
                	<a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=porsche';?>">
                    	<li class="index-hc-list-right-abottom">
                        	<span>保时捷精品馆</span>
                            <p>速度、美感、跑车</p>
                            <img src="<?php echo $SER_ADD;?>/images/bsj1.png" />
                        </li>
                    </a>
                </ul>
            </div>
            
            <div class="index-hc-list-right-b">
            	<ul>
            		<a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=audi';?>">
                        <li class="index-hc-list-right-btop">
                        	<span>奥迪精品馆</span>
                            <p>稳健、气派、品质</p>
                            <img src="<?php echo $SER_ADD;?>/images/aodi1.png" />
                        </li>
                    </a>
                	<a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=benz';?>">
                    	<li class="index-hc-list-right-bbottom">
                        	<span>奔驰精品馆</span>
                            <p>舒适、高档、细致</p>
                            <img src="<?php echo $SER_ADD;?>/images/bc1.png" />
                        </li>
                    </a>
                </ul>
            </div>
            
            <div class="index-hc-list-right-c">
            	<ul>
            		<a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=dazhong';?>">
                        <li class="index-hc-list-right-ctop">
                        	<span>大众精品馆</span>
                            <p>安全、稳定、性价比</p>
                            <img src="<?php echo $SER_ADD;?>/images/dz1.png" />
                        </li>
                    </a>
                	<a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=landrove';?>">
                    	<li class="index-hc-list-right-cbottom">
                        	<span>路虎精品馆</span>
                            <p>精良、体面、拉风</p>
                            <img src="<?php echo $SER_ADD;?>/images/lh1.png" />
                        </li>
                    </a>
                </ul>
            </div>
      
      </div>
      
      
      <div class="index-offer">
      	<ul>
            	<li>
                    <img src="<?php echo $SER_ADD;?>/images/offer_1.png" alt="今日推荐" title="今日推荐" />
                    <div class="index-go-a"><strong>今&nbsp;日&nbsp;推&nbsp;荐</strong></div>
            	</li>
            
            <a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=audi&class=audia8';?>">
            	<li>
                    <img src="<?php echo $SER_ADD;?>/images/offer_2.png" alt="豪华动感 奥迪A8 " title="豪华动感 奥迪A8 " />
                    <span>豪华动感 奥迪A8</span>
                    <p>二手新品体验</p>
                    <div class="index-go-b"><strong>GO&nbsp;></strong></div>
            	</li>
            </a>
            
            <a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bmw&class=bmww';?>">
            	<li>
                    <img src="<?php echo $SER_ADD;?>/images/offer_3.png" alt="现代动感 宝马M6" title="现代动感 宝马M6" />
                    <span>现代动感 宝马M6</span>
                    <p>二手新品体验</p>
                    <div class="index-go-c"><strong>GO&nbsp;></strong></div>
            	</li>
            </a>
            
            <a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=dazhong&class=dazhongg';?>">
            	<li>
                    <img src="<?php echo $SER_ADD;?>/images/offer_4.png" alt="帕萨特 大众汽车" title="帕萨特 大众汽车" />
                    <span>帕萨特 大众汽车</span>
                    <p>二手新品体验</p>
                    <div class="index-go-d"><strong>GO&nbsp;></strong></div>
            	</li>
            </a>
        </ul>
      </div>
     
     </div> 
      
      <div class="index-car-sell">
        <ul class="index-car-navbar">
        	<!--大于40w豪华座驾；15-20w小资专车；5-10w经济代步-->
        	<a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p8';?>"><li class="index-car-order index-car-current">豪华座驾</li></a>
            <a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p5';?>"><li class="index-car-order">小资专车</li></a>
            <a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p3';?>"><li class="index-car-order">经济代步</li></a>
            <div class="index-car-more"><a href="buy?city=hangzhou">查看更多&nbsp;<img src="<?php echo $SER_ADD;?>/images/more.png" /></a></div>
        </ul>
    	
        <div class="index-car-sell-tab">
            <div class="index-car-navbar-left"><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p8';?>"><img src="<?php echo $SER_ADD;?>/images/hh.jpg"/></a></div>
            <ul class="index-car-list">
            	<?php
					//豪华座驾
					$result = $conn->query("select * from ".$car_dataset." where sell_city='".$city."' and state=1 and current_price>50.001 order by id desc limit 6");
					$num_result = $result->num_rows;
					$str = '';
					if(!$result||$num_result==0) {
						$str.='<p>对不起，暂时无此类别的车辆！</p>';
					}
					else {
						for($i=0; $i < $num_result; $i++) {
							$row = $result->fetch_assoc();
							$img_src = explode(";",$row['img_src']);
							$img_src = $img_src[0]==""?"../cheyuzhou_bg/upload/lost.jpg":"../cheyuzhou_bg/".$img_src[0];
							$str.='<a href="'.$SER_ADD.'/buycar_detail?id='.$row['id'].'"><li><img src="'.$SER_ADD.'/'.$img_src.'" /><p>'.$row['brand_name'].'</p><span>¥'.$row['current_price'].'万</span><div class="index-newprice">当前款新车价格：<span>¥'.$row['new_price'].'万</span>(含税)</div><div class="index-line"></div><div class="index-newprice">上牌 '.$row['plate_date'].' ｜ 里程 '.$row['driving_distance'].'万公里</div></li></a>';
						}
					}
					echo $str;
				?>
                
            </ul>
        </div>
        
        <div class="index-car-sell-tab index-car-navbar-none">
            <div class="index-car-navbar-left"><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p5';?>"><img src="<?php echo $SER_ADD;?>/images/xz.jpg"/></a></div>
        	<ul class="index-car-list">
        	<?php
				//小资专车
				$result = $conn->query("select * from ".$car_dataset." where sell_city='".$city."' and state=1 and current_price>20.001 and current_price<49.999 order by id desc limit 6");
				$num_result = $result->num_rows;
				$str = '';
				if(!$result||$num_result==0) {
					$str.='<p>对不起，暂时无此类别的车辆！</p>';
				}
				else {
					for($i=0; $i < $num_result; $i++) {
						$row = $result->fetch_assoc();
						$img_src = explode(";",$row['img_src']);
						$img_src = $img_src[0]==""?"../cheyuzhou_bg/upload/lost.jpg":"../cheyuzhou_bg/".$img_src[0];
						$str.='<a href="'.$SER_ADD.'/buycar_detail?id='.$row['id'].'"><li><img src="'.$SER_ADD.'/'.$img_src.'" /><p>'.$row['brand_name'].'</p><span>¥'.$row['current_price'].'万</span><div class="index-newprice">当前款新车价格：<span>¥'.$row['new_price'].'万</span>(含税)</div><div class="index-line"></div><div class="index-newprice">上牌 '.$row['plate_date'].' ｜ 里程 '.$row['driving_distance'].'万公里</div></li></a>';
					}
				}
				echo $str;
			?>

        	</ul>
        </div>
        
        <div class="index-car-sell-tab index-car-navbar-none">
            <div class="index-car-navbar-left"><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p3';?>"><img src="<?php echo $SER_ADD;?>/images/jj.jpg"/></a></div>
        	<ul class="index-car-list">
        	<?php
				//经济代步
				$result = $conn->query("select * from ".$car_dataset." where sell_city='".$city."' and state=1 and current_price<19.999 order by id desc limit 6");
				$num_result = $result->num_rows;
				$str = '';
				if(!$result||$num_result==0) {
					$str.='<p>对不起，暂时无此类别的车辆！</p>';
				}
				else {
					for($i=0; $i < $num_result; $i++) {
						$row = $result->fetch_assoc();
						$img_src = explode(";",$row['img_src']);
						$img_src = $img_src[0]==""?"../cheyuzhou_bg/upload/lost.jpg":"../cheyuzhou_bg/".$img_src[0];
						$str.='<a href="'.$SER_ADD.'/buycar_detail?id='.$row['id'].'"><li><img src="'.$SER_ADD.'/'.$img_src.'" /><p>'.$row['brand_name'].'</p><span>¥'.$row['current_price'].'万</span><div class="index-newprice">当前款新车价格：<span>¥'.$row['new_price'].'万</span>(含税)</div><div class="index-line"></div><div class="index-newprice">上牌 '.$row['plate_date'].' ｜ 里程 '.$row['driving_distance'].'万公里</div></li></a>';
					}
				}
				echo $str;
			?>
        	</ul>
        </div>
    </div>
    
    <div class="index-help"><a href="<?php echo $SER_ADD;?>/helpbuy"><img src="<?php echo $SER_ADD;?>/images/index_help.png"  alt="知心帮买" title="知心帮买"/></a></div>
    
<?php
	require_once('footer.php');
?>