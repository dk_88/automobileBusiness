<?php
	require_once('configure/parameter.php');
	require_once('header.php');
	require_once('configure/db_fns.php');
	require_once('navbar.php');
?>
 <div class="price_background">
    	<div class="login_item">
            <div class="pricesult_dialog">
            	<ul class="pricesult">
                	<li class="pricesultula">
                    	<ul>
                        	<li class="pricesult-lia">
                            	<img src="images/audi.jpg" />
                            </li>
                        	<li class="pricesult-lib">
                            	<span class="spana">
                                  奥迪A3 2015款 奥迪A3 Limousine35 TFSI 手动进取型 杭州
                                 </span>
                                  <p>
                                  	 <span>排放标准：国5</span>
                                     <span>变速箱：手动排量：1.4t</span>
                                  </p>
                                  <p>
                                  	<span>表显里程：2万</span>
                                    <span>上牌时间：2015年10月</span>
                                  </p>
                               
                            </li>
                        </ul>
                    </li>
                	
                    <li><h5>成交价格区间：</h5></li>
                    
                    <li class="est-pic-result">
                    	<div class="est-pic-line"></div>
                        
                        <div class="est-interval">
                            <div class="minPrice">
                                <i class="num-radius"></i>
                                <span><b>8.24</b>万</span>
                            </div>
                            <div class="maxPrice">
                                <i class="num-radius"></i>
                                <span><b>8.96</b>万</span>
                            </div>
                            <div class="est-interval-line"></div>
                    	</div>
                    </li>
                    
                    <li class="pricesult-pcli">
                    	<span>推荐价：<strong>11.12</strong>万</span>
                    	<span class="pricesult-pclispan">新车指导价：<b>19.09</b>万</span>
                    </li>
                    
                    
                     <li class="pricesult-f5">
                    	<a href="#"><i class="fa fa-repeat"></i>&nbsp;再估一次</a>
                    </li>
                    
                    <li class="pricesult-bt">
                    	<button type="button" class="pricesult-f5bta">我要买车</button>
                        <button type="button" class="pricesult-f5bta pricesult-f5btb">我要卖车</button>
                    </li>
                </ul>
        	</div>
        </div>
    </div>
<?php 
	require_once('footer.php');
?>