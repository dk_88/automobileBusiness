<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
 <div class="introduction-bg"></div>

        <div class="introduction-item">
            <div class="introduction-left">
                <div class="introduction-left-top"><i class="fa fa-files-o"></i>车宇宙百科</div>
                <?php require_once('problems_navbar.php');?>
            </div>
            
            <div class="introduction-right">
                <div class="introduction-right-locatin">车宇宙百科>过户流程</div>
                <div class="introduction-right-content">
                        <div class="transferprocess-img" >
                    	<img src="images/transferprocess_1.jpg" />
                    	</div>
                    <div class="transferprocess-content">
                    	
                    	<p class="transferprocess-content-title">简要概述</p>
                    	<p></p>
                    	<p class="transferprocess-contentone">
                    		二手车的买卖流程，大致可分为5个步骤：确认购买车辆（验车）→二手车评估（车辆评估书）→买卖交易（开发票）→过户→保险过户（税费变更）。买卖交易以及税费变更也可以引申为过户的一部分，所以这里主要讲后三个步骤。
      						买二手车在交易环节中除了要看好车况，剩下最重要的环节就是办理过户手续。
                    	</p>
                    	
                    	<br />
                    	<p class="transferprocess-content-title">需要的材料</p>
                    	<p></p>
                    	<p class="transferprocess-contentone">
                    		首先，先列举一下那些在过户时候需要提前准备的材料。这个准备过程还是比较繁琐的，所以一定不要大意，以免白跑一趟。当然，车子也是要开去的。
                    	</p>
                    	<p>
                    		卖方（原车主）：<br />
						      1、原车主身份证原件及复印件/代理人身份证原件及复印件；<br />
						      2、机动车登记证书原件及复印件；<br />
						      3、机动车行驶证原件及复印件；<br />
						      4、原始购车发票（红联）或上次过户发票（红联）原件及复印件；<br />
						      5、购置税完税证明；<br />
						      6、卖方是单位则需要组织机构代码证书原件及复印件（带公章）。
						      <br /><br />
						         买方（新车主）：<br />
						      1、新车主身份证原件及复印件/代理人身份证原件及复印件；<br />
						      2、机动车注册、转移、注销登记表/转入申请表（一般市场、车管所都有）；<br />
						      3、如果所在城市有限购，还需准备中签结果原件及复印件；<br />
						      4、如外地户口上当地牌照还需准备暂住证；<br />
						      5、买方是单位则需要组织机构代码证书原件及复印件（带公章）。  
                    	</p>
                    	<br />
                    	
                    	<p class="transferprocess-content-title">手续及流程</p>
                    	<p></p>
                    	<p>
                    		1、进入过户大厅后，出示相关材料领取《旧机动车买卖合同》，并进行填写。该合同一式三份，卖方一份，买方一份，工商部门一份。
							     这个时候，就开始办理过户了，如果嫌麻烦，可以找协助办理引导员（俗称黄牛）进行代办，服务费一般也就50元，当然，其实流程并非特别复杂，自己有时间也就办了，能省则省嘛。
							<br />
							2、将车开到过户验车处，进行验车、拓号、拆牌、照相处理，并填写《检查记录表》。之后便可将车停到停车场，去过户大厅办理手续。这一过程要缴纳"拓号费"。
							<br />
							3、到过户大厅收费窗口排队，缴纳"过户费"（年限越长，排量越小，过户费越便宜）。
							<br />
							4、去"转移受理"窗口办理相关手续。在办理之前需要填写《机动车注册、转移、注销登记表/转入申请表》。
							<br />
							5、办完"转移受理"，拿到回执单，就需要到下一个窗口缴纳"上牌费"了。
							<br />
							6、之后的流程表就是选号，拿新的车牌、新的行驶本、登记证书，以及提车了。
                    	</p>
                    	
                    	
                    	
                    	
                    </div>

                </div>
            </div>
            
        </div>
 

          
    <!--底部-->
<?php
	require_once('footer.php');
?>