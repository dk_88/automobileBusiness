<?php
	if (!session_id()) session_start();
	//判断城市
	$city = isset($_GET['city'])?$_GET['city']:"hangzhou";
	
	//默认情况的话将从ip地址获取到城市,前期的话用杭州
	
?>
<?php
	require_once('header.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	$user_phone = isset($_GET['phone'])?$_GET['phone']:"";
	$type = isset($_GET['type'])?$_GET['type']:"";
	$conn = db_connect();
	$conn->query("set names utf8");
	$query = "select * from ".$dealer_info." where user_id='".$user_phone."'";
	if($type!="") {
		$query.=" and apply_schedule='".$type."'";
	}
	$query.=" order by id desc";
	$result = $conn->query($query);
	
?>
 <div class="header-three-bg">
    	<div class="header-logo">
        	<a href="<?php echo $SER_ADD;?>/index"><img src="images/index_logo.png" /></a>
        </div>
    </div>
     <div class="navbar-container">
           <div class="nav-bar">
                <ul>
                    
                    <?php
						
						$links = array();
						$links[]=array("首页","index.php","index");//或者这边直接改成根据ip地址获取
						$links[]=array("买车","buy_car.php","buy?city=".$city);
						$links[]=array("卖车","sell_car.php","sell_car");
						$links[]=array("帮买","helpbuy.php","helpbuy");
						$links[]=array("服务保障","service.php","service");
						$self_page = basename($_SERVER['PHP_SELF']);
						foreach($links as $link){
							printf('<a href="%s"><li %s>%s</li></a>' , $link[2],$self_page==$link[1]?'class="navbar-current"':'class=""',$link[0]);
								
							}
					?>
                                 
                </ul>
            </div>
      </div>

   
  <div class="examine-bg">
    	 <div class="examine-processbg">
            <ul class="examine-processc">
                <li>填写公司账号信息</li>
                <li>待审核状态</li>
                <li class="examine-native">审核结果</li>
            </ul>
   		</div>

	<div class="examine-result">
    	<ul class="examine-result-left">
        	<li class="examine-result-lefta"><img src="<?php echo $SER_ADD;?>/images/search.png" />申请进度查询</li>
        	<a href="examineresult?phone=<?php echo $user_phone;?>"><li <?php if($type=="") echo 'class="examine-result-active"';?>>全部</li></a>
            <a href="examineresult?type=2&phone=<?php echo $user_phone;?>"><li <?php if($type==2) echo 'class="examine-result-active"';?>>申请成功</li></a>
        	<a href="examineresult?type=1&phone=<?php echo $user_phone;?>"><li <?php if($type==1) echo 'class="examine-result-active"';?>>审核中</li></a>
            <a href="examineresult?type=3&phone=<?php echo $user_phone;?>"><li style="border-bottom:1px solid #dddddd;" <?php if($type==3) echo 'class="examine-result-active"';?>>申请失败</li></a>
        </ul>
        
        <div class="examine-result-right">
            <div class="examine-result-location">
                <a href="<?php echo $SER_ADD;?>/index">车宇宙首页</a>>>
                <a href="<?php echo $SER_ADD;?>/businesslogin">经销商申请</a>>>
                <span>申请进度查询</span>
            </div>
            <?php
				if($result->num_rows==0) {
					echo '<div class="examine-no-result">对不起，没有查询到相应的申请工单！</div>';
				}
				else {
					//存放申请详情的数组
					$content_detail = array();
					$content = '<table class="examine-result-list"><tr class="examine-result-lista"><td>申请时间</td><td>申请进度</td><td>申请详情</td><td>备注</td></tr>';
					$i=0;
					while($row=$result->fetch_assoc()) {
						
						array_push($content_detail,''.$row['user_name'].','.$row['user_id'].','.$row['company'].','.$dealertype_change[$row['dealer_type']].','.$row['address'].'');
						$content.='<tr><td>'.$row['Ctime'].'</td><td>'.$apply_change[$row['apply_schedule']].'</td><td><a href="javascript:void(0)" title="查看详情" onclick="show_detail(\''.$content_detail[$i].'\')">查看详情</a></td><td>'.$row['fail_reason'].'</td></tr>';
						$i++;
					}
					$content.='</table>';
					echo $content;
					
				}
			?>
             
                <div class="examine-result-content">
                    <div class="examine-result-contentbg">
                    	申请详情
                        <img src="images/close.png" alt="关闭" title="关闭" />
                    </div>
                    <ul class="examine-result-show">
                        <li>负责人姓名：</li>
                        <li>张三</li>
                        <li>手机号码：</li>
                        <li>15157115149</li>
                        <li>公司名称：</li>
                        <li>明道车业</li>
                        <li>经销商类型：</li>
                        <li>市场内经营</li>
                        <li>经销商地址：</li>
                        <li>浙江省杭州市凤起东路189号浙江省杭州市凤起东路浙江省杭州市凤起东路</li>
                    </ul>
               
            </div>
           
         </div>   
    </div>
    
    
  	<div class="clear"></div>

    
       
        <script type="text/jscript">  
				$(function(){   
				//表格变色
					$(".examine-result-list tr").attr("background-color", "#f7f7f7"); //为单数行表格设置背景颜色   
					$(".examine-result-list tr:even").css("background-color", "#ffffff"); //为双数行表格设置背颜色素
				//查看详情
					$(".examine-result-content li").css("width", "240px"); 
					$(".examine-result-content li:even").css("width", "100px"); 

				}); 
				

            	$(".examine-result-list tr a").click(function(event){
                //取消事件冒泡  
                event.stopPropagation();  
                $(".examine-result-content").show();
                 return false;
                })
                //消失	
				$(".examine-result-contentbg img").click(function(){
					$(".examine-result-content").hide();
				})
				 //点击空白处隐藏弹
				 $(document).click(function(event){
					  var _con = $(".examine-result-content");   // 设置目标区域
					  if(!_con.is(event.target) && _con.has(event.target).length ==0){ // Mark 1
						$(".examine-result-content").hide();          //淡出消失   
					  }
				});

				
				
				
				function show_detail(content) {
					var info_content = content.split(",");
					
					for(i=0;i<info_content.length;i++) {
						$(".examine-result-show>li").eq(i*2+1).html(info_content[i]);
					}
					<!--alert(info_content[0]);-->
					$(".examine-result-bg").css("visibility","visible");
				}
     		 </script> 
</div>
<?php
	
	require_once('footer.php'); 
?>