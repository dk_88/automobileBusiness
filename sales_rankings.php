<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
 <script type="text/javascript">
     $(function(){
          $(".tablea tr:even").css("background","#f4f4f4");
		  $(".tablea tr.allnx").css("background","#3a3a3a");   
    })
   </script>
<div class="introduction-bg"></div>

        <div class="introduction-item">
            <div class="introduction-left">
                <div class="introduction-left-top"><i class="fa fa-files-o"></i>车宇宙百科</div>
                <?php require_once("problems_navbar.php");?>
            </div>
            
            <div class="introduction-right">
                <div class="introduction-right-locatin">车宇宙百科>销量排行</div>
                <div class="introduction-right-content">
                    <table class="tablea">
                    	<tr class="allnx">
                        	<td colspan="3">全国销量排行TOP10</td>
                        </tr>
                        
                        <tr class="allnxa">
                        	<td>排名</td>
                        	<td>车型</td>
                            <td>2016年1月销量</td>
                        </tr>
                        
                        <tr>
                        	<td  class="ordera">1</td>
                        	<td>大众朗逸</td>
                            <td>48387</td>
                        </tr>
                        
                        <tr>
                        	<td class="ordera">2</td>
                        	<td>大众捷达</td>
                            <td>35254</td>
                        </tr>
                        
                        <tr>
                        	<td class="ordera">3</td>
                        	<td>别克英朗</td>
                            <td>34728</td>
                        </tr>
                        
                        <tr>
                        	<td class="orderb">4</td>
                        	<td>大众速腾</td>
                            <td>31253</td>
                        </tr>
                        
                        <tr>
                        	<td class="orderb">5</td>
                        	<td>大众桑塔纳</td>
                            <td>31075</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">6</td>
                        	<td>丰田卡罗拉</td>
                            <td>24555</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">7</td>
                        	<td>雪佛兰科鲁兹</td>
                            <td>24292</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">8</td>
                        	<td>福特福睿斯</td>
                            <td>24275</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">9</td>
                        	<td>大众高尔夫</td>
                            <td>22874</td>
                        </tr>
                    	                     
                         <tr>
                         	<td class="orderb">10</td>
                        	<td>大众帕萨特</td>
                            <td>22803</td>
                        </tr>
                    
                    </table>
                    
                    
                     <table class="tablea tableb">
                    	<tr class="allnx">
                        	<td colspan="3">平台销量排行TOP10</td>
                        </tr>
                        
                        <tr class="allnxa">
                        	<td>排名</td>
                        	<td>车型</td>
                            <td>2016年1月销量</td>
                        </tr>
                        
                        <tr>
                        	<td  class="ordera">1</td>
                        	<td>大众朗逸</td>
                            <td>48387</td>
                        </tr>
                        
                        <tr>
                        	<td class="ordera">2</td>
                        	<td>大众捷达</td>
                            <td>35254</td>
                        </tr>
                        
                        <tr>
                        	<td class="ordera">3</td>
                        	<td>别克英朗</td>
                            <td>34728</td>
                        </tr>
                        
                        <tr>
                        	<td class="orderb">4</td>
                        	<td>大众速腾</td>
                            <td>31253</td>
                        </tr>
                        
                        <tr>
                        	<td class="orderb">5</td>
                        	<td>大众桑塔纳</td>
                            <td>31075</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">6</td>
                        	<td>丰田卡罗拉</td>
                            <td>24555</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">7</td>
                        	<td>雪佛兰科鲁兹</td>
                            <td>24292</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">8</td>
                        	<td>福特福睿斯</td>
                            <td>24275</td>
                        </tr>
                        
                         <tr>
                         	<td class="orderb">9</td>
                        	<td>大众高尔夫</td>
                            <td>22874</td>
                        </tr>
                    	                     
                         <tr>
                         	<td class="orderb">10</td>
                        	<td>大众帕萨特</td>
                            <td>22803</td>
                        </tr>
                    
                    </table>
                    
                    <div class="cankao">*全国销量排行榜数据来源于汽车销量网数据，仅供参考。</div>
                    
                </div>
            </div>
            
        </div>
 

          

<?php
	require_once('footer.php');
?>