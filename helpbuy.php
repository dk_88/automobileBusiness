<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');

?>
<!--js效果-->
<script type="text/javascript">

   $(function(){
	   	//图片验证码刷新
	   $("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
	   $("#checkImg").click(function() {
		  $("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
		   });
	   
		$("#help-buyform").validate({
			 rules: {
			   img_code:{
				   remote:{
					   type:"get",
					   url:"configure/img_code_validate.php",
					   dataType:"json",
					   data:{
						   img_code:function(){return $('#img_code').val();}
					   }
				   },
			   },
		   },
		   messages: {
		    img_code: {
			   remote:"* 输入的验证码有误！点击图片可重新获取",
		   }
		   },
		   submitHandler: function (form) {
			  var demand_text = $("#adviser_text").val();
			  var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
			  if(demand_text=="填写您的购车需求，如：7万以内，空间大的德系车。或：就想找宝马530旅行版等") {
				  demand_text = "该用户未填写购车需求！";
			  }
			  var user_id = $("#mobile").val();
			  $("#checkImg").attr("src","configure/validationCode.php?num="+Math.random());
			  $.ajax({
				  type:"POST",
			  	  url:"configure/helpbuy_check.php",
			      cache:false,
				  data:{
					  "demand_text":demand_text,
					  "user_id":user_id,
				  },
				  beforeSend:function(){
					 
				   $(".help-buy-sell").html("提交中...").attr('disabled',"true");
						  
					  },
				  success:function(data) {
					  
					  //提交成功
					  if(data==1) {
						  //调用模态对话框,提示成功
						  content+= '<p class="helpbuy-demand-success">成功接收到您的需求</p><p class="helpbuy-contact">您的私人帮买顾问会第一时间与您取得联系~</p><p class="helpbuy-around"><a href="buy_car.php">您也可以到处逛逛<<</a></p>';
						 
					  }
					 //失败
					  else if(data==0){
						  //调用模态对话框,提示失败
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">提交失败~请稍候重试!</p>';
						  
					  }
					  
					   //同一ip/session在同一时间内提交超过2次则会出现验证码
					  else {
						  content+= '<p class="helpbuy-demand-success">成功接收到您的需求</p><p class="helpbuy-contact">您的私人帮买顾问会第一时间与您取得联系~</p><p class="helpbuy-around"><a href="buy_car.php">您也可以到处逛逛<<</a></p>';
						  $(".img-code-check").show();
						  
					  }
					  show_clue(content);
					  $(".help-buy-sell").html("提交").removeAttr('disabled');
					  
					  $("#img_code").val("");
					  $("#mobile").val("");
					  },
			  });
		   }
			})
	});	
	

	function bfocus(ma){ 
		if(ma.value=="填写您的购车需求，如：7万以内，空间大的德系车。或：就想找宝马530旅行版等 "){ 
		ma.value=""; 
		ma.style.color = '#000';
		} 
		} 
	function bonblur(ma){ 
		if(ma.value==''){ 
		ma.value="填写您的购车需求，如：7万以内，空间大的德系车。或：就想找宝马530旅行版等 "; 
		ma.style.color='#999'; 
		} 
		} 

   </script>

 <!-- banner-->
	  <div class="helpbuy-banner">
      		<div class="help-buy-message">
                <div class="help-buy-message-list">
                    <div class="help-buy-message-a">帮您选车</div>
                    <form id="help-buyform" class="help-buyform-css" action="#" method="post">
                        <div class="help-buyform-text">
                            <textarea class="txt-area required" id="adviser_text"  onfocus="bfocus(this)" onblur="bonblur(this)" name="comment">填写您的购车需求，如：7万以内，空间大的德系车。或：就想找宝马530旅行版等 </textarea>
                        </div>
                        
                        <div class="help-buyform-input">
                            <input type="mobile" class="form-control required" id="mobile" name="mobile"  placeholder="手机号码(必填)">  
                        </div>
                        
                        <div class="help-buyform-input">
                        	<img class="helpbuy-img-code" id="checkImg" src="">
                            <input type="img_code" class="form-control required ma-con" id="img_code" name="img_code"  placeholder="验证码">  					
                                 
                                						
                        </div>
    	
                        <button class="help-buy-sell"  retype="submit">提交</button>  
                    </form>
                    <div class="help-buy-consult">免费咨询：400-695-578</div> 
                </div>
        	</div>
      </div>	
    
	  <ul class="helpbuy-bg">
      	<li>
        	<span>第一步 提交需求</span>
            <p>填写帮买需求、预留号码</p>
        </li>
      	<li>
        	<span>第二步 确定车源</span>
            <p>根据客户需求、推荐相应车款</p>
        </li>
        
        <li>
        	<span>第三步 线下交易</span>
            <p>顾问陪同线下车辆交易</p>
        </li>
        
        <li>
        	<span>第四步 完成交易</span>
            <p>安全、省心、快捷完成交易</p>
        </li>
      </ul>		
    

      <div class="helpbuy-list">
          <div class="helpbuy-title">
                <span>TEAM</span>
                <p>帮买团队</p>
                <p class="ht-line">__</p>
          </div>
      </div> 

      <div class="helpbuy-manlist">
          <ul>
              <li>
                  <img src="images/face1.png" />
                  <p class="bp1">大帅哥</p>
                  <p class="bp2">宇宙人称：张大帅
                    <br>
                    擅长推荐：中高端车型
                  </p>
                  <p class="bp3">
                    15年二手车从业经验，精通车辆鉴定、估值。成功为上百位买家精准推荐合适的车型，超越预期的找到梦想之车。
                 </p>
              </li>
              <li>
                  <img src="images/face1.png" />
                  <p class="bp1">大帅哥</p>
                  <p class="bp2">宇宙人称：张大帅
                    <br>
                    擅长推荐：中高端车型
                  </p>
                  <p class="bp3">
                    15年二手车从业经验，精通车辆鉴定、估值。成功为上百位买家精准推荐合适的车型，超越预期的找到梦想之车。
                 </p>
              </li>

              <li>
                  <img src="images/face1.png" />
                  <p class="bp1">大帅哥</p>
                  <p class="bp2">宇宙人称：张大帅
                    <br>
                    擅长推荐：中高端车型
                  </p>
                  <p class="bp3">
                    15年二手车从业经验，精通车辆鉴定、估值。成功为上百位买家精准推荐合适的车型，超越预期的找到梦想之车。
                 </p>
              </li>

              <li>
                  <img src="images/face1.png" />
                  <p class="bp1">大帅哥</p>
                  <p class="bp2">宇宙人称：张大帅
                    <br>
                    擅长推荐：中高端车型
                  </p>
                  <p class="bp3">
                    15年二手车从业经验，精通车辆鉴定、估值。成功为上百位买家精准推荐合适的车型，超越预期的找到梦想之车。
                 </p>
              </li>
          </ul>

      </div>   	
<?php
	require_once('footer.php');
?>