<?php
	require_once('configure/parameter.php');
	require_once('header.php');
	require_once('configure/db_fns.php');
	require_once('navbar.php');
?>
 <script type="text/javascript" src="js/new_file.js"></script>	
 <script type="text/javascript" src="js/areab.js"></script>	
	 <div class="price_background">
    	<div class="login_item">
            <div class="price_dialog" id="price_out_model">
            	<div class="price_name_tab">
                 	<span>填写您的车辆基本信息</span>
                    <p>信息越准确，我们为您估算的价格越精确</p>
                </div>
    
                <form class="price-form" id="price-form">
                  <div class="price-tableall">	
                  <ul class="price-tablea">
                   	 <li>
                     	<ul>
                    		<li class="price-a">汽车品牌</li>
                            <li class="price-b">
                                <select class="form-control" title="请选择汽车品牌" id="car_brand" name="car_brand" required>
                                <option value="">请选择汽车品牌</option>
                                <option value="anchi">安驰</option><option value="audi">奥迪</option><option value="astonmartin">阿斯顿马丁</option><option value="acs">AC Schnitzer</option><option value="alfa">阿尔法罗密欧</option><option value="barbus">巴博斯</option><option value="baolong">宝龙</option><option value="porsche">保时捷</option><option value="bieke">别克</option><option value="bmw">宝马</option><option value="benz">奔驰</option><option value="beiqi">北汽制造</option><option value="bentian">本田</option><option value="bjqc">北京汽车</option><option value="benteng">奔腾</option><option value="byd">比亚迪</option><option value="baojun">宝骏</option><option value="binli">宾利</option><option value="bjd">布加迪</option><option value="biaozhi">标致</option><option value="bqhs">北汽幻速</option><option value="changfeng">长丰</option><option value="changan">长安</option><option value="changcheng">长城</option><option value="changhe">昌河</option><option value="daoqi">道奇</option><option value="dayu">大宇</option><option value="dadi">大迪</option><option value="dongnan">东南</option><option value="dazhong">大众</option><option value="datong">大通</option><option value="dihao">帝豪</option><option value="dongfeng">东风</option><option value="fsike">菲斯克</option><option value="futian">福田</option><option value="fyate">菲亚特</option><option value="fudi">福迪</option><option value="fengtian">丰田</option><option value="ford">福特</option><option value="ferray">法拉利</option><option value="mitsuoka">光冈</option><option value="gmc">GMC</option><option value="guangqi">广汽</option><option value="guanzhi">观致</option><option value="gumpert">GUMPERT</option><option value="hanma">悍马</option><option value="huatai">华泰</option><option value="heibao">黑豹</option><option value="huanghai">黄海</option><option value="hafei">哈飞</option><option value="flag">红旗</option><option value="huaxiang">华翔</option><option value="huabei">华北</option><option value="haima">海马</option><option value="huizhong">汇众</option><option value="huapu">华普</option><option value="huayang">华阳</option><option value="hengtian">恒天</option><option value="hangtian">航天</option><option value="haige">海格</option><option value="huasong">华颂</option><option value="jinlong">金龙</option><option value="jincheng">金程</option><option value="geely">吉利</option><option value="jinbei">金杯</option><option value="jianghuai">江淮</option><option value="jiao">吉奥</option><option value="jiangling">江铃</option><option value="jeep">吉普</option><option value="jiangnan">江南</option><option value="jiulong">九龙</option><option value="jiebao">捷豹</option><option value="kairui">开瑞</option><option value="chrysler">克莱斯勒</option><option value="koenigsegg">柯尼赛格</option><option value="kers">卡尔森</option><option value="cadillac">凯迪拉克</option><option value="kaibaihe">凯佰赫</option><option value="ktm">KTM</option><option value="kawei">卡威</option><option value="kaima">凯马汽车</option><option value="kaiyi">凯翼</option><option value="kangdi">康迪</option><option value="renault">雷诺</option><option value="lianhua">莲花</option><option value="lexus">雷克萨斯</option><option value="lutesi">路特斯</option><option value="lincoln">林肯</option><option value="suzuki">铃木</option><option value="lifan">力帆</option><option value="landrove">路虎</option><option value="lamborghini">兰博基尼</option><option value="rollsroyce">劳斯莱斯</option><option value="lufeng">陆风</option><option value="ludiship">陆地方舟</option><option value="luofu">罗孚</option><option value="mogen">摩根</option><option value="mini">迷你</option><option value="maybach">迈巴赫</option><option value="maikailun">迈凯伦</option><option value="mingjue">名爵</option><option value="mazda">马自达</option><option value="maserati">玛莎拉蒂</option><option value="meiya">美亚</option><option value="nazhijie">纳智捷</option><option value="nanqi">南汽</option><option value="ouge">讴歌</option><option value="opel">欧宝</option><option value="pajiani">帕加尼</option><option value="kia">起亚</option><option value="qirui">奇瑞</option><option value="richan">日产</option><option value="rongwei">荣威</option><option value="ruilin">瑞麒</option><option value="sqtjia">陕汽通家</option><option value="shuanghuang">双环</option><option value="shuanglong">双龙</option><option value="skoda">斯柯达</option><option value="smart">Smart</option><option value="shijue">世爵</option><option value="mitsubishi">三菱</option><option value="subaru">斯巴鲁</option><option value="saab">萨博</option><option value="tianma">天马</option><option value="tongbao">通宝</option><option value="tjyq">天津一汽</option><option value="tesla">特斯拉</option><option value="weilin">威麟</option><option value="weiziman">威兹曼</option><option value="volvo">沃尔沃</option><option value="xinkai">新凯</option><option value="seat">西雅特</option><option value="hyundai">现代</option><option value="citroen">雪铁龙</option><option value="chevrolet">雪佛兰</option><option value="yongyuan">永源</option><option value="yiqi">一汽</option><option value="infiniti">英菲尼迪</option><option value="englon">英伦</option><option value="yema">野马</option><option value="yingzhi">英致</option><option value="zhongyu">中誉</option><option value="zhongshun">中顺</option><option value="zhongxing">中兴</option><option value="zhonghua">中华</option><option value="zhongtai">众泰</option><option value="zhongou">中欧</option>
                                </select>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <ul>
                                <li class="price-a">汽车系列</li>
                                <li class="price-b">
                                <select class="form-control" id="car_class" title="请选择汽车系列" name="car_class" required >
                                    <option value="">请选择汽车系列</option>
                                </select>
                                </li>
                            </ul>
                        </li>
                  
                  
                        <li>
                            <ul>
                                <li class="price-a">汽车型号</li>
                                <li class="price-b">
                                 <select class="form-control" id="car_style" title="请选择汽车型号" name="car_style" required>
                                    <option value="">请选择汽车型号</option>
                                 </select>
                                </li>
                            </ul> 
                       </li> 
                   </ul>
                   </div>
                   
                  	<ul class="price-tableb">
                   			<li>
                            	<ul>
                            	<li class="price-a">上牌时间</li>
                                <li class="price-ale">
                                	<select class="form-control"  title="请选择上牌年" id="timea" name="timea" required >
                                    	<option value="">请选择上牌年</option>
                                        <option value="1">2016年</option>
                                        <option value="2">2015年</option>
                                        <option value="3">2014年</option>
                                        <option value="4">2013年</option>
                                        <option value="5">2012年</option>
                                        <option value="6">2011年</option>
                                        <option value="7">2010年</option>
                                        <option value="8">2009年</option>
                                        <option value="9">2008年</option>
                                        <option value="10">2007年</option>
                                        <option value="11">2006年</option>
                                        <option value="12">2005年</option>
                                        <option value="13">2004年</option>
                                        <option value="14">2003年</option>
                                        <option value="15">2002年</option>
                                        <option value="16">2001年</option>
                                        <option value="17">2000年</option>
                                    </select>
                                </li>
                                <li class="price-ari">
                                	<select class="form-control" title="请选择上牌月" required id="timeb" name="timeb">
                                    	<option value="">请选择上牌月</option>
                                        <option value="1">1月</option>
                                        <option value="2">2月</option>
                                        <option value="3">3月</option>
                                        <option value="4">4月</option>
                                        <option value="5">5月</option>
                                        <option value="6">6月</option>
                                        <option value="7">7月</option>
                                        <option value="8">8月</option>
                                        <option value="9">9月</option>
                                        <option value="10">10月</option>
                                        <option value="11">11月</option>
                                        <option value="12">12月</option>
                                    </select>
                                
                                </li>
                                </ul>
                            </li>
                            
                          
                            <li>
                            	<ul>
                                    <li class="price-a">所在地区</li>
                                    <li class="price-ale">
                                         <select class="form-control" id="province" name="p_province" title="请选择省份"  onchange="getcity()" required>
                                           <option value="">请选择省份</option>
                                           <option value="北京市">北京市 </option>
                                           <option value="广东省">广东省 </option>
                                           <option value="上海市">上海市 </option>
                                           <option value="天津市">天津市 </option>
                                           <option value="重庆市">重庆市 </option>
                                           <option value="辽宁省">辽宁省</option>
                                           <option value="江苏省">江苏省</option>
                                           <option value="湖北省">湖北省</option>
                                           <option value="四川省">四川省</option>
                                           <option value="陕西省">陕西省</option>
                                           <option value="河北省">河北省</option>
                                           <option value="山西省">山西省</option>
                                           <option value="河南省">河南省</option>
                                           <option value="吉林省">吉林省</option>
                                           <option value="黑龙江省">黑龙江省</option>
                                           <option value="内蒙古自治区">内蒙古自治区</option>
                                           <option value="山东省">山东省</option>
                                           <option value="安徽省">安徽省</option>
                                           <option value="浙江省">浙江省</option>
                                           <option value="福建省">福建省</option>
                                           <option value="湖南省">湖南省</option>
                                           <option value="广西省">广西省</option>
                                           <option value="江西省">江西省</option>
                                           <option value="贵州省">贵州省</option>
                                           <option value="云南省">云南省</option>
                                           <option value="西藏自治区">西藏自治区</option>
                                           <option value="海南省">海南省</option>
                                           <option value="甘肃省">甘肃省</option>
                                           <option value="宁夏自治区">宁夏自治区</option>
                                           <option value="青海省">青海省</option>
                                           <option value="新疆自治区">新疆自治区</option>
                                           <option value="香港">香港</option>
                                           <option value="澳门">澳门</option>
                                           <option value="台湾">台湾</option>
                                           <option value="海外">海外</option>
                                           <option value="其他">其他</option>
                                         </select> 
                                    </li>
                                    <li class="price-ari">
                                         <select id="city" name="p_city" class="form-control" title="请选择县/市" required>
                                            <option value="">请选择县/市</option>
                                        </select> 
                                    </li>
								</ul>
                            </li>
                            
                            <li>
                            	<ul>
                                    <li class="price-a">行驶里程</td>
                                    <li class="price-d">
                                        <input type="number" class="form-control" min="0" required/>
                                    </li><span class="wan"><strong>万公里</strong></span>
                                </ul>
                            </li>
                        
                             <li class="tabtrb">
                                 <button type="submit" class="price-submit" id="price-submit">我要估价</button>
                            </li>
                            
                        </ul>

                </form>
               
        	</div>
        </div>
    </div>
    
    
    <!--底部-->
    <?php
		require_once('footer.php');
    ?>
 <script>
 	$(function(){
		 
		 //初始化车的品牌、车系、车型
		 var content='';
		 var brand_arr = '';
		
		  //改变车系栏
		 if($("#car_brand option:selected").val()!="") {
			  var class_arr = brand_class[$("#car_brand option:selected").text()];
			  $(class_arr).each(function(i,dom){
				  var aArray = dom.split(','),
				  q1=aArray[0],
				  q2=aArray[1];
				  content+='<option value="'+q2+'">'+q1+'</option>';
				  });
			  $("#car_class").append(content);
		 }
		 
		
		 //改变车型
		 content='';
		  if($("#car_class option:selected").val()!="") {
			  var style_arr = brand_style[$("#car_class option:selected").text()];
			  $(style_arr).each(function(i,dom){
				  var aArray1 = dom.split(','),
				  r1=aArray1[0],
				  r2=aArray1[1];
				  content+='<option value="'+r2+'">'+r1+'</option>';
				  });
			  $("#car_style").append(content);
		 }
		 
		 //车牌变动时
		 $("#car_brand").change(function(){
			
			 var me = $(this);
			 var class_content = '<option value="">车系</option>';
			 var class_string=$("#car_brand option:selected").text();
			 if(me.val()!='') {
				 var arr = brand_class[class_string];
				 $(arr).each(function(i,dom){
					  var aArray1 = dom.split(','),
					  q1=aArray1[0],
					  q2=aArray1[1];
					  class_content+='<option value="'+q2+'">'+q1+'</option>';
				  });
				  $("#car_class").html(class_content);
				  $("#car_style").html('<option value="">车型</option>');
			 }
			 });
	     //车系变动时
		 $("#car_class").change(function(){
			 var me = $(this);
			 var class_content = '<option value="">车型</option>';
			 var class_string=$("#car_class option:selected").text();
			 if(me.val()!='') {
				 var arr = brand_style[class_string];
				 $(arr).each(function(i,dom){
					  var aArray1 = dom.split(','),
					  q1=aArray1[0],
					  q2=aArray1[1];
					  class_content+='<option value="'+q2+'">'+q1+'</option>';
				  });
				  $("#car_style").html(class_content);
			 }
			 });

				
			
			$("#price-form").validate();

		
		});
		
		
		
		
 </script>