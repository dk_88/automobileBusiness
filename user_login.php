<?php
	require_once('configure/parameter.php');
	require_once('header.php');
	
?>
  <script type="text/javascript">
	 	$(function(){
			
			$(".login_name_taba").click(function(){
				$(".login_name_taba").removeClass("border-styleb").css("border-bottom","1px solid #149bdb")
				$(".login_name_tabb").addClass("border-styleb").removeClass("border-stylea");
				$(".register-form").removeClass("register_style").addClass("login_register_form");
				$(".login-form").removeClass("login_style");
				});
			
			$(".login_name_tabb").click(function(){
				$(".login_name_tabb").removeClass("border-styleb").addClass("border-stylea")
				$(".login_name_taba").css("border-bottom","none").addClass("border-styleb").removeClass("border-stylea")
				$(".register-form").addClass("register_style").removeClass("login_register_form");
				$(".login-form").addClass("login_style");
				});
			
			
		});
     
    </script>
 <div class="header-three-bg">
    	<div class="header-logo">
        	<a href="<?php echo $SER_ADD;?>/index"><img src="images/index_logo.png" /></a>
        </div>
    </div>
    
    
	 <div class="login_background">
    	<div class="login_item">
            <div class="login_dialog" id="login_out_model">
            	<div class="login_name_tab">
                 	<div class="login_name_taba">会员登录</div>
                    <div class="login_name_tabb">用户注册</div>
                </div>
                
                
             <!--登录框-->
                <form class="login-form" id="login-form">
                    <div class="input-gp">
                    	<div class="input-group-a">手机号:</div>
                        <div class="input-group-b">
                        	<input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="login-phone" required>	
                        </div>
                        
                    </div>
                    
                    <div class="input-gp">
                         <div class="input-group-a">密&nbsp;&nbsp;&nbsp;码:</div>
                         <div class="input-group-b">
                        	<input type="password" class="form-control " placeholder="请输入密码" name="password" id="login-password" required>   
                          </div>
                    </div>
                    
                    <div class="input-gp">
                    	<input type="checkbox" class="login-footer-left" />&nbsp;下次自动登录
                    	<span class="login-footer-right"><a href="retrievepassword.php">忘记密码?</a></span>
                    </div> 
                        
                    <div class="login-form-submit">
                        <button type="submit" class="form-control login-submit">登录</button>
                    </div>
                    
                </form>
                <!--注册框-->
                <form class="register-form login_register_form" id="register-form">
                    <div class="input-gp">
                    	<div class="input-group-a">手机号:</div>
                    	<div class="input-group-b">
                        <input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="register-phone" required>
                        </div>
                    </div>
                    <div class="input-gp">
                    	<div class="input-group-a">密码:</div>
                    	<div class="input-group-b">
                        <input type="password" class="form-control" placeholder="输入密码：6-20位字母和数字" name="password" id="register-password" required>
                        </div>
                    </div>
                     
                    <div class="input-gp">
                    	<div class="input-group-a">验证码:</div>
                        <div class="input-group-c">
                        	<input type="code" class="form-control" placeholder="手机验证码" name="code" id="login-code" required> 
                        </div>
                        <div class="input-group-d">
                        	<button class="btn btn-default" onClick="get_code()" >获取验证码</button>
                        </div>
                    </div> 
                    <div class="login-form-submit">
                        <button type="submit" class="form-control register-submit">注册</button>
                       
                    </div>
                </form>
        	</div>
        </div>
    </div>
    <?php
		require_once('footer.php'); 
	?>