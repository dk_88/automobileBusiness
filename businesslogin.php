<?php
	require_once('header.php');
	require_once('configure/parameter.php');
?>
<script>
		$(function(){
		$(".businesslogin_b").click(function(){
			var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog_sp()"></i></div><form class="login-form" action="examineresult" method="get"><div class="input-group"><input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="login-phone" required></div><div class="input-group"><button type="submit" class="login-submit">审查进度查询</button></div></form>';
			
			show_clue(content);
			});
		$("#login-form-business").validate({
	
	
	  submitHandler: function (form) {
		  var user_id = $("#login-phone").val();
		  $.ajax({
			  type:"POST",
			  url:"../cheyuzhou_bg/login_judge.php",
			  cache:false,
			  data:{
				  "username":user_id,
				  "passwd":$("#login-password").val(),
				  },
			  beforeSend:function(){
					 
				   $(".login-submit").html("登录中...").attr('disabled',"true");
						  
					  },
			  success:function(data) {
				 
				  //登录成功
				  if(data==1) {
					
					  window.location.href="../cheyuzhou_bg/index_bg.php";
				  }
				  //账号不存在的情况
				  else if(data==2) {
					  $("#login-phone").after('<lable class="error register-exist">* 手机号或密码错误！</lable>');
					 
				  }
				  //密码错误
				  //else if(data==3) {
//					  $("#login-password").after('<lable class="error register-exist">* 密码错误！</lable>');
//				  }
				  else
				  	alert("暂时无法连接服务器，请稍后再试");
				  $(".login-submit").html("登录").removeAttr('disabled');
				  
			  },
			  });
	  },
	  
	});
		})
</script>
<!--各个页面中用来随时调用的模态对话框-->
<div class="model-dialog" id="model_dialog_clue">
	
</div>
<!--头部-->
    <div class="fullbg"></div>
  <div class="header-three-bg">
    	<div class="header-logo">
        	<a href="index.php"><img src="images/index_logo.png" /></a>
        </div>
    </div>
    
    
	 <div class="shop_login_background">
    	<div class="login_item">
        	<div class="login-bt">
            	<ul>
                	<a href="<?php echo $SER_ADD;?>/examine"><li>免费入驻</li></a>
                    <a href="javascript:void(0)" class="businesslogin_b"><li>查看申请进度</li></a>
                </ul>
            </div>		
            
            <div class="login_dialog" id="login_out_model">
            	<div class="shop_name_tab">
                 	商家登录入口
                </div>
                  
             <!--登录框-->
                <form class="login-form" id="login-form-business">
                    <div class="input-gp">
                    	<div class="input-group-a">手机号:</div>
                        <div class="input-group-b">
                        	<input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="login-phone" required>	
                        </div>
                        
                    </div>
                    
                    <div class="input-gp">
                         <div class="input-group-a">密&nbsp;&nbsp;&nbsp;码:</div>
                         <div class="input-group-b">
                        	<input type="password" class="form-control " placeholder="请输入密码" name="password" id="login-password" required>   
                          </div>
                    </div>
                    
                    <div class="input-gp">
                    	<input type="checkbox" class="login-footer-left" />&nbsp;下次自动登录
                    	<span class="login-footer-right"><a href="retrievepassword.php">忘记密码?</a></span>
                    </div> 
                        
                    <div class="login-form-submit">
                        <button type="submit" class="form-control login-submit">登录</button>
                    </div>
                    
                </form>
                <!--注册框-->
                <form class="register-form login_register_form" id="register-form">
                    <div class="input-gp">
                    	<div class="input-group-a">手机号:</div>
                    	<div class="input-group-b">
                        <input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="register-phone" required>
                        </div>
                    </div>
                    <div class="input-gp">
                    	<div class="input-group-a">密码:</div>
                    	<div class="input-group-b">
                        <input type="password" class="form-control" placeholder="输入密码：6-20位字母和数字" name="password" id="register-password" required>
                        </div>
                    </div>
                     
                    <div class="input-gp">
                    	<div class="input-group-a">验证码:</div>
                        <div class="input-group-c">
                        	<input type="code" class="form-control" placeholder="手机验证码" name="code" id="login-code" required> 
                        </div>
                        <div class="input-group-d">
                        	<button class="btn btn-default" onClick="get_code()" >获取验证码</button>
                        </div>
                    </div> 
                    <div class="login-form-submit">
                        <button type="submit" class="form-control register-submit">注册</button>
                       
                    </div>
                </form>
        	</div>
        </div>
    </div>
    
<?php
	require_once('footer.php');
?>