<?php
	if (!session_id()) session_start();
	if(!isset($_SESSION['user_id'])) {
		//未登录
		return 3;
	}
	require_once('web_fns.php');
	$user_id = $_SESSION['user_id'];
	$car_id = $_POST['car_id'];
	$check_result = cancel_order($user_id,$car_id,$car_dataset,$orders_table,$users_info);
	switch($check_result) {
		//取消成功
		case 1:
			echo "1";
			break;
		//该预约单已不在，提示请刷新重试
		case 2:
			echo "2";
			break;
		//查询发生错误
		default:
			echo "0";
	}
?>