<?php
	require_once("web_fns.php");
	require_once("parameter.php");
	if (!session_id()) session_start();
	if(!isset($_SESSION['user_id'])) {
		echo 2;
	}
	else {
		$old_password = $_POST['old_password'];
		$new_password = $_POST['new_password'];
		$check_result = user_changepassword($users,$_SESSION['user_id'],$old_password,$new_password);
		switch($check_result) {
			//修改成功
			case 1:
				echo "1";
				break;
			//输入的原密码有误
			case 3:
				echo "3";
				break;
			//查询发生错误
			default:
				echo "0";
		}
	}
?>