<?php
	require_once('users_fns.php'); 
	//包含连接数据库信息
	require_once('db_fns.php');
	//参数设置
	require_once('parameter.php');
	$user_id = trim($_POST['user_id']);
	$passwd = trim($_POST['passwd']);
	$check_result = register_check($user_id,$passwd,$users,$users_info);
	switch($check_result) {
		//注册成功
		case 1:
			echo "1";
			break;
		//该账号已存在
		case 2:
			echo "2";
			break;
		//查询发生错误
		default:
			echo "0";
	}
?>