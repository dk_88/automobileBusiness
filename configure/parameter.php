<?php
	$GLOBALS['SER_ADD']='http://www.yuzhouche.com';//服务器地址
	$GLOBALS['users']='users';//用户保存用户的账号/密码
	$GLOBALS['users_info']='users_info';//用于保存用户的基本信息
	$GLOBALS['car_dataset']='car_dataset';//保存二手车基本信息
	$GLOBALS['orders_table']='orders';  //
	$GLOBALS['admin_users']='admin_users';//经销商/管理员列表
	$GLOBALS['dealer_info']='dealer_info';//
	$city_change = array("hangzhou"=>"杭州","shenzhen"=>"深圳","shanghai"=>"上海");
	$standard_change = array("0"=>"未知","1"=>"国五","2"=>"国四及以上","3"=>"国三及以上","4"=>"国二及以上");
	$transfer_num_change = array("0"=>"未知","1"=>"一手车","2"=>"二手车","3"=>"三手车","4"=>"次数大于3次");
	$property_change = array("0"=>"未知","1"=>"非营运车","2"=>"营运车");
	$car_procedure_change = array("0"=>"未知","1"=>"代办","2"=>"非代办");
	$maintenance_change = array("0"=>"未知","1"=>"4S店定期","2"=>"非4S店定期");
	$gearbox_change = array("0"=>"未知","1"=>"手动","2"=>"自动");
	$carcolor_change = array("0"=>"未知","1"=>"黑色","2"=>"白色","3"=>"银色","4"=>"灰色","5"=>"红色","6"=>"蓝色","7"=>"黄色","8"=>"金色","9"=>"其他");
	$e_standard_change = array("0"=>"未知","1"=>"国五","2"=>"国四及以上","3"=>"国三及以上","4"=>"国二及以上");
	//申请状态转化
	$apply_change = array("1"=>"审核中","2"=>"申请成功","3"=>"申请失败");
	//经销商类型转化
	$dealertype_change = array("1"=>"市场内经营","2"=>"市场外独立展厅","3"=>"4S店");
?>