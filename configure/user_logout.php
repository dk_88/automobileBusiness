<?php
	require_once('parameter.php');
	session_start();
	unset($_SESSION['user_id']);
	session_destroy();
	header("Location: ".$SER_ADD."/user_login");
	exit;
?>