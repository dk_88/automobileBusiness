<?php
	require_once('header.php');
?>
<div class="model-dialog" id="model_dialog_clue">
	
</div>
<div class="fullbg"></div>
    <!--用来切换的模态对话框-->
    <div class="model-dialog" id="login_out_model">
    	<!--登录框-->
         <form class="login-form" id="login-form">
        	<div class="model-dialog-close">
            	<span>登录</span>
        		<i class="fa fa-times" onClick="close_dialog()"></i>
        	</div>
        	<div class="input-group">
                <input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="login-phone" required>
            </div>
            
            <div class="input-group">
                <input type="password" class="form-control" placeholder="请输入密码" name="password" id="login-password" required>
            </div>
                
            <div class="login-form-footer">
            	<button type="submit" class="form-control login-submit">登录</button>
                <span class="login-footer-left"><a href="retrievepassword.php">忘记密码 <i class="fa fa-angle-double-right"></i></a></span>
                <span class="login-footer-right">还没有车宇宙账号？<a href="javascript:void(0)" class="login-register">点击注册<i class="fa fa-angle-double-right"></i></a></a></span>
            </div>
            
        </form>
        <form class="register-form" id="register-form">
        	<div class="model-dialog-close">
            	<span>注册</span>
        		<i class="fa fa-times" onClick="close_dialog()"></i>
        	</div>
        	<div class="input-group">
            	
                <input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="register-phone" required>
            </div>
            <div class="input-group">
            	
                <input type="password" class="form-control" placeholder="输入密码：6-20位字母和数字" name="password" id="register-password" required>
            </div>
             
            <div class="input-group">
                
                <button class="btn btn-default register-phone-code" onClick="get_code()" >获取验证码</button>
                <input type="code" class="form-control register-code" placeholder="输入手机验证码" name="code" id="login-code" required>
                
            </div> 
            <div class="login-form-footer">
            	<button type="submit" class="form-control register-submit">注册</button>
                <div class="login-footer-exist">已有账号？<a href="javascript:void(0)" class="register-login">返回登录<i class="fa fa-angle-double-right"></i></a></a></div>
            </div>
        </form>
    </div>
<script>
    	$(function(){
			//伸缩展开	
				$(".bbsindex-themer a").click(function(){
					var t=$(this).text();
					if(t=="收起"){	
						$(".bbsindex-theme").css("height","49px");
						$(this).text("展开");
						$(".bbsindex-themer img").attr("src","<?php echo $SER_ADD;?>/images/xjt.png");
					}
					else{
						$(".bbsindex-theme").css("height","81px"); 
						$(this).text("收起");
						$(".bbsindex-themer img").attr("src","<?php echo $SER_ADD;?>/images/sjt.png");
						}
				});
				
			//选择		
				$(".bbsindex-title ul a").click(function(){
					 $(this).children('li').addClass('allnative');
					 $(this).siblings().children('li').removeClass('allnative');
					})			
		
			
			//消失
			
			$("#themeclick").click(function(event){
				$(".themeallnone").toggle();
				return false;
				});
				
			$(document).click(function(event){
                  var _con = $(".themeallnone");   // 设置目标区域
                  if(!_con.is(event.target) && _con.has(event.target).length == 0){ // Mark 1
                    $(".themeallnone").hide();
                  }
				});	
				
				
			//字数统计
			 $("#quickpost-input").keyup(function(){//表示输入之后不再输入的时候触发的事件
				var len=$("#quickpost-input").val().length;
				$("font").remove();
				$("color").remove();
				if(len>80)
				{
					$("#quickpost-input").after("<font color=\"red\">超过范围</font>");
					$("#quickpost-input").attr({maxlength:"80"});
					$("#quickpost-input").css("color","red");
				}
				else
				{
					$("#quickpost-input").css("color","");
				}
			
			  $("#shownum").html("<span id=\"shownum\">"+len+"</span>");
			
		  })
		  
	}) 
    
    </script>   

<a class="head-adv" href="<?php echo $SER_ADD;?>/helpbuy">
        	<img src="<?php echo $SER_ADD;?>/images/advti.jpg" />
        </a>

        <div class="bbsindex-title">
        	<ul class="bbsindex-theme">
            	<a href="#"><li class="allnative">全部</li></a>
            	
                <a href="#"><li>新手攻略<span>123</span></li></a>
                <a href="#"><li>卖车分享<span>123</span></li></a>
                <a href="#"><li>买车分享<span>123</span></li></a>
                <a href="#"><li>好车推荐<span>123</span></li></a>
                <a href="#"><li>汽车大咖<span>123</span></li></a>
                <a href="#"><li>车友生活<span>123</span></li></a>
                <a href="#"><li>快报<span>123</span></li></a>
                <a href="#"><li>以车会友<span>123</span></li></a>
                <a href="#"><li>自驾游&amp;聚会<span>123</span></li></a> 
                <a href="#"><li>车主故事【专栏】<span>123</span></li></a>
                <a href="#"><li>吐槽<span>123</span></li></a>
             </ul>
			 <div class="bbsindex-themer"><img src="<?php echo $SER_ADD;?>/images/sjt.png" /><a href="#">收起</a></div>

            
            <div class="bbsindex-list">
           			<div class="bbsindex-listtitle">
                    <ul class="themeall">
                        <li id="themeclick"><a href="javascript:void(0);">全部主题</a><i class="fa fa-caret-down"></i></li>
                        <li><a href="#">最新</a></li>
                        <li><a href="#">热门</a></li>
                        <li><a href="#">热帖</a></li>
                        <li><a href="#">精华</a></li> 
                    </ul>
                     <dl class="themeallnone">
                            	<dt></dt>
                            	<a href="#"><dd>全部主题</dd></a>
                                <a href="#"><dd>投票</dd></a>
                                <a href="#"><dd>活动</dd></a>
                     </dl>
                     <div class="newtime">发帖/最新回复时间</div>   
                     <div class="author">作者/最新回复</div>
                </div>
                 <div class="bbsindex-listcontent">
                	<div class="bbsindex-listcw">
                        <ul class="bbs-listcwtop">
                            <li class="bbswa"><span class="offtop">置顶</span></li>
                            <li class="bbswb">
                            	<span>[新手攻略]</span> 
                                <a href="#">2015车宇宙社区【年度大盘点】 多亏有你，精彩乐不停…</a>
                            </li>
                            <li class="bbswc"><i class="facea"></i>愤怒的小鸟</li>
                            <li class="bbswd">2016-03-08 18:38</li>
                        </ul>
                        
                         <ul class="bbs-listcwtop bbs-liststyle">
                            <li class="bbswa"><span class="indexnum">1073</span></li>
                            <li class="bbswb">
                            	<span>[新手攻略]</span> 
                                <a href="#">2015车宇宙社区【年度大盘点】 多亏有你，精彩乐不停…</a>
                                <p class="bbs-ip">我一直买买一辆SUV，但是只有5万左右的钱，想问一下有这个价</p>
                            </li>
                            <li class="bbswc">
                            	<i class="faceb"></i>愤怒的小鸟
                           		<p class="bbs-ipt"><i class="facec"></i>小七</p>
                            </li>
                            <li class="bbswd">2016-03-08 18:38</li>
                        </ul>
                        
                       
                         <ul class="bbs-listcwtop bbs-liststyle">
                            <li>版块主题<i class="faced" onclick=""></i></li>
                        </ul>
                        
                        <ul class="bbs-listcwtop bbs-liststyle">
                            <li class="bbswa"><span class="indexnum">1073</span></li>
                            <li class="bbswb">
                            	<span>[新手攻略]</span> 
                                <a href="#">2015车宇宙社区【年度大盘点】 多亏有你，精彩乐不停…</a>
                                <p class="bbs-ip">我一直买买一辆SUV，但是只有5万左右的钱，想问一下有这个价</p>
                            </li>
                            <li class="bbswc">
                            	<i class="faceb"></i>愤怒的小鸟
                           		<p class="bbs-ipt"><i class="facec"></i>小七</p>
                            </li>
                            <li class="bbswd">2016-03-08 18:38</li>
                        </ul>
                        
                        
                        <ul class="bbs-listcwtop bbs-liststyle">
                            <li class="bbswa"><span class="indexnum">1073</span></li>
                            <li class="bbswb">
                            	<span>[新手攻略]</span> 
                                <a href="#">2015车宇宙社区【年度大盘点】 多亏有你，精彩乐不停…</a>
                                <p class="bbs-ip">我一直买买一辆SUV，但是只有5万左右的钱，想问一下有这个价</p>
                            </li>
                            <li class="bbswc">
                            	<i class="faceb"></i>愤怒的小鸟
                           		<p class="bbs-ipt"><i class="facec"></i>小七</p>
                            </li>
                            <li class="bbswd">2016-03-08 18:38</li>
                        </ul>
                        
                        
                        <ul class="bbs-listcwtop bbs-liststyle">
                            <li class="bbswa"><span class="indexnum">1073</span></li>
                            <li class="bbswb">
                            	<span>[新手攻略]</span> 
                                <a href="#">2015车宇宙社区【年度大盘点】 多亏有你，精彩乐不停…</a>
                                <p class="bbs-ip">我一直买买一辆SUV，但是只有5万左右的钱，想问一下有这个价</p>
                            </li>
                            <li class="bbswc">
                            	<i class="faceb"></i>愤怒的小鸟
                           		<p class="bbs-ipt"><i class="facec"></i>小七</p>
                            </li>
                            <li class="bbswd">2016-03-08 18:38</li>
                        </ul>
                        
                         <div class="buycar-pages">
                             <div id="page">
                                 <p>首页</p>
                                 <p>上一页</p>
                                 <a href="?city=hangzhou&amp;brand=benz&amp;page=1" title="第1页" class="cur">1</a>
                                 <a href="#" title="第2页">2</a>
                                 <p>下一页</p>
                                 <p>尾页</p>
                             </div>     
                        </div>
                        
                	</div>
            	</div>
                <div class="quickpost">
                	<div class="bm_h">快速发帖</div>
                    <div class="bm_c">
                        <select class="quickpost-select">
                            <option>选择分类主题</option>
                            <option>卖车分享</option>
                            <option>买车分享</option>
                            <option>好车推荐</option>
                            <option>汽车大咖</option>
                            <option>车友生活</option>
                            <option>车主故事【专栏】</option>
                            <option>自驾游&聚会</option>
                            <option>随便聊聊</option>
                            <option> 吐槽</option>
                        </select>
                        <input type="text"  class="quickpost-input"  name="quickpost-input" id="quickpost-input" maxlength="80" />
                        你可以输入<span id="shownum">0</span>/80字符
                    </div>
                    
                    
                    <div class="bm_d">
                    	<div class="loginreg">
                            <a href="">登录</a>
                            <a href="">注册</a>
                        </div>    
                    </div>
                    
                    <div class="bm_e">
                    	<button type="submit">发表帖子</button>
                    </div>
                </div>
            </div>
      </div>
<?php 
	require_once('../footer.php');
?>