<?php 
	require_once('../configure/parameter.php');
	require_once('../configure/db_fns.php');
	require_once('../configure/web_fns.php');
?>
<!DOCTYPE html>
<html lang="zh-CN"><head>
	<meta charset="utf-8">
   	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $SER_ADD;?>/images/favicon.ico" />
    <link rel="stylesheet" href="<?php echo $SER_ADD;?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $SER_ADD;?>/css/index.css">
    <link rel="stylesheet" href="<?php echo $SER_ADD;?>/css/bbsindex.css">
    <script type="text/javascript" src="<?php echo $SER_ADD;?>/js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $SER_ADD;?>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo $SER_ADD;?>/js/index.js"></script>
    <script type="text/javascript" src="<?php echo $SER_ADD;?>/js/unslider.js"></script>
    <script src="<?php echo $SER_ADD;?>/js/animateBackground-plugin.js"></script>
    <script src="<?php echo $SER_ADD;?>/js/jquery.validate.js"></script>
    <script src="<?php echo $SER_ADD;?>/js/messages_zh.js"></script>
    <script src="<?php echo $SER_ADD;?>/js/placeholder.js"></script>
    <script type="text/javascript" src="<?php echo $SER_ADD;?>/js/fly.js"></script>
    <script type="text/javascript" src="<?php echo $SER_ADD;?>/js/requestAnimationFrame.js"></script> 
    <script type="text/javascript" src="<?php echo $SER_ADD;?>/js/jquery.flexslider-min.js"></script>
    <link rel="stylesheet" href="<?php echo $SER_ADD;?>/fontawesome/css/font-awesome.min.css">
    <title>车宇宙</title>
</head>

<body>
	<!--论坛头部--->
	<div class="bbsindex-headbg">
    	<ul class="bbsindex-head">
        	<li>
			<?php
				if(!isset($_SESSION['user_id'])) {
					echo '<a href="javascript:show_model(0)">Hi,请登录</a>';
				}
				else {
					echo '<p class="bbsindex-nickname">Hi,'.$_SESSION['user_id'].'</p>';
				}
			?>
            </li>
            <!--<li class="bbbs-fr"><a href="#">网站导航<i class="fa fa-caret-down"></i></a></li>-->
            <!--<li class="bbbs-fr"><a href="">个人中心</a></li>-->
            <li class="bbbs-fr"><a href="<?php echo $SER_ADD;?>/bbs/bbs">论坛首页</a></li> 
            <li class="bbbs-fr"><a href="<?php echo $SER_ADD;?>">车宇宙首页</a></li>
        </ul>
    </div>
	
    <div class="headwhole-bg">
        <div class="bbsindex-logo">
        	<a href="<?php echo $SER_ADD;?>/bbs/bbs"><img src="<?php echo $SER_ADD;?>/images/head.png" /></a>
            <div class="bbsindex-logoip">
            	<input class="form-control" placeholder="请输入搜索内容"/>
                <a href="javascript:void(0);"><i class="fa fa-search"></i></a>
            </div>
        </div>
    </div>  
    