<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
	require_once('configure/db_data.php');
?>
 <?php
	$conn = db_data_connect();
	$conn->query("set names utf8");
	//分页参数
	$showrow = 12; //一页显示的行数
	$curpage = empty($_GET['page']) ? 1 : $_GET['page']; //当前的页,还应该处理非数字的情况
	$result1 = $conn->query("select * from news where class='0'");
	$total=$result1->num_rows;
	if (!empty($_GET['page']) && $total != 0 && $curpage > ceil($total / $showrow))
	$curpage = ceil($total / $showrow); //当前页数大于最后页数，取最后一页
	//去掉当前url中的page
	$url = $SER_ADD."/news?page={page}";
	$query = "select * from news where class='0' order by id desc limit " . ($curpage - 1) * $showrow . ",$showrow;";
	$result = $conn->query($query);
	
?>
<script>
	$(function(){
			$(".introduction-right-content img").parent().addClass("news-img");
			
		})
</script>
 <div class="introduction-bgb"></div>

        <div class="introduction-item">
            <div class="introduction-left">
                <div class="introduction-left-top"><i class="fa fa-files-o"></i>关于我们</div>
               	<?php
					require_once('about_navbar.php');
					
				?>
 
            </div>
            
            <div class="introduction-right">
            	<?php
					$content='';
					if(!isset($_GET['id'])) {
						
						$content.='<div class="introduction-right-locatin">关于我们>新闻动态</div><div class="introduction-right-content"><ul class="cnewsa-list">';
						while($row = $result->fetch_assoc()) {
							$date = explode(' ',$row['date']);
							$content.='<li><a href="'.$SER_ADD.'/news?id='.$row['id'].'">'.$row['title'].'</a><span>'.$date[0].'</span></li>';
						}
						$content.='</ul></div><div class="buycar-pages">';
						echo $content;
						//显示分页
						$page = new page($total, $showrow, $curpage, $url, 2);
			 			echo $page->myde_write();
					}
					else {
						$result = $conn->query("select * from news where id=".$_GET['id']."");
						$row = $result->fetch_assoc();
						$content.='<div class="introduction-rl-title">'.$row['title'].'<p>发布日期：'.$row['date'].'</p></div><div class="introduction-right-content">'.$row['content'].'</div></div>';
						echo $content;
					}
				?>    
            </div>     
      </div>
 </div>
 
		
          
    <!--底部-->
<?php 
	require_once('footer.php');
?>