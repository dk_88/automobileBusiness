<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
        <div class="introduction-bgb"></div>

        <div class="introduction-item">
            <div class="introduction-left">
                <div class="introduction-left-top"><i class="fa fa-files-o"></i>关于我们</div>
                <?php require_once('about_navbar.php');?>
 
            </div>
            
            <div class="introduction-right">
                <div class="introduction-right-locatin">关于我们>平台简介</div>
                <div class="introduction-right-content">
                	<p style="text-indent:2em">
                    车宇宙，是一个以二手车交易为基础的一站式汽车消费服务平台，致力于满足经销商及 个人买车卖车等需求。自平台运营之初，就受到了广大用户的爱戴。海量的二手车信息，方便的在线预约系统， 专业的上门估价服务，可靠的售后担保业务，给每一位用户带来安全可靠的二手车交易服务。
                	</p>
                </div>    
            </div>
            
        </div>
<?php
	require_once('footer.php');
?>