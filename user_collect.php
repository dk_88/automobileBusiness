<?php
	if (!session_id()) session_start();
	if(!isset($_SESSION['user_id'])) {
		 header("Location: user_login");
		 exit;
	}
?>
<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
<?php
	$conn = db_connect();
	$conn->query("set names utf8");
	$result = $conn->query("select * from ".$users_info." where user_id='".$_SESSION['user_id']."'");
	if($result) {
		$row = $result->fetch_assoc();
		$collect_cars = $row['collect_cars'];
	}
	
?>
<script>
	function cancel_collect(bt,car_id) {
		var collect_type = 1;
		$.ajax({
			type:"POST",
			url:"configure/car_collect.php",
			cache:false,
			data:{"car_id":car_id,
				  "collect_type":collect_type,
			},
			success:function(data) {
				//失败
				if(data==0) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">删除失败！请刷新后重试~</p>';
					show_clue(content);
				}
				//未登录
				else if(data==2) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">未登录！请刷新后重试~</p>';
					show_clue(content);
				}
				//成功
				else if(data==1) {
					
					$(bt).parents("li").remove();
					}
					//若为空，则显示为空的代码
					
					if($(".tab_box-divb").find('li').length==0) {
						$(".tab_box").html('<div class="release-box-right-prompt">您还没有收藏过任何二手车<a href="buy?city=<?php echo $city;?>"> 快去看看！</a></div>');
					}
			},
	  	});
		 
	}
</script>
<div class="usercenter-box">
            <div class="usercenter-box-left">
                <div class="usercenter-box-left-face">
                    <div class="usercenter-box-left-faceimg"><img src="<?php echo $row['img_src']==""?'images/askface.png':$row['img_src'];?>" /></div>
                    <div class="usercenter-box-left-font"><?php echo $row['nickname']==""?$row['user_id']:$row['nickname']; ?></div>
                </div>
                
                <div class="usercenter-box-left-ul">
                    <ul>
                        <?php
							require_once('usercenter_navbar.php');
						?>
                        <!--<a href="#">
                        	<li>
                            	<i class="usercentera"></i>
                            	我的预约
                       		 </li>
                        </a>

                        <a href="usercenter.html"  class="usercenter-active">
                            <li>
                                <i class="usercenterb"></i>
                                我的收藏
                             </li>
                        </a>

                        <a href="usercenter.html">
                            <li>
                                <i class="usercenterc"></i>
                               我发布的车辆
                             </li>
                        </a>

                        <a href="usercenter.html">
                            <li>
                                <i class="usercenterd"></i>
                                我的购物车
                             </li>
                        </a>

                        <a href="usercenter.html">
                            <li>
                                <i class="usercentere"></i>
                               浏览历史
                             </li>
                        </a>

                        <a href="usercenter.html">
                            <li>
                                <i class="usercenterf"></i>
                                我的账户
                             </li>
                        </a>-->
                    </ul>

                </div>
            </div>
            
            <div class="usercenter-box-right" id="tab">
                 <ul class="tab_menu">
                    <li class="useselected useleft">我的收藏</li>
                    <!--<li>待付款</li>
                    <li>已付款</li>
                    <li>交易关闭</li>
                    <li>交易完成</li>-->
                </ul>

				
                <div class="tab_box">
                	
                    <div class="tab_box-divb">
                    	<?php
							if($collect_cars=="") {
								echo '<div class="release-box-right-prompt">您还没有收藏过任何二手车<a href="buy?city='.$city.'"> 快去看看！</a></div>';
							}
							else {
								$result = $conn->query("select * from ".$car_dataset." where id in(".$collect_cars.") order by field (id,".$collect_cars.") desc");
								$num_result = $result->num_rows;
								$collect_content = '<ul>';
								while($row = $result->fetch_assoc()) {
									if($row['img_src']=="") {
									  $img_src = "../cheyuzhou_bg/upload/lost.jpg";
								  	}
								  	else {
								  		$arr = explode(";",$row['img_src']);
								  		$img_src = $arr[0]; 
								  	}
									$collect_content.='<a href="'.$SER_ADD.'/buycar_detail?id='.$row['id'].'"><li><img src="../cheyuzhou_bg/'.$img_src.'" /><span class="collect-name">'.$row['brand_name'].'</span><p class="collect-newp"><span>￥'.$row['current_price'].'万</span><span class="collect-new">新车价￥'.$row['new_price'].'万</span></p><p>上牌 '.$row['plate_date'].'｜ 里程 '.$row['driving_distance'].'万公里</p><div class="collect-button-cancel" onclick="cancel_collect(this,\''.$row['id'].'\');return false"><i class="redfs"></i>&nbsp;&nbsp;&nbsp;&nbsp;取消收藏</div></li></a>';
								}
								$collect_content.= '</ul>';
								echo $collect_content;
							}
						  	
						  	
						  
						?>
                  	
                    </div>
                  <!--  <div class="hidea"></div>
                    	<div class="hidea"></div>
                        <div class="hidea"></div>
                        <div class="hidea"></div>
                     -->
                </div>
           </div>
           
       </div>
<?php 
	require_once("footer.php");
?>