<?php
	if (!session_id()) session_start();
	if(!isset($_SESSION['user_id'])) {
		//跳转到登陆页面
		 header("Location: user_login");
		 exit;
	}
?>
<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
<script type="text/javascript">
 $(function(){
	 //计算数量
	function GetCount() {
		
			var conts = 0;
			var aa = 0;
			$("#playList input[name=newslist]").each(function () {
				if (this.checked == true) {
					for (var i = 0; i < $(this).length; i++) {
						//总价计算2
						conts += parseInt($(this).parents("ul").children("li").find('.tot').html());
						//alert(conts);
						//数量计算
						aa += 1;
					}
				}
			});
			$("#shuliang").text(aa);
			$("#zong1").html((conts).toFixed(2));
				//判断显示的车辆地方是否为0
				var numb = $("#shuliang").html();
				if(numb == 0){
					$("#jz2").hide();
					$("#jz1").show();
					$(".reverse").prop("checked",false);
					
				}
				
				else {
					if(numb==$("#playList input[name=newslist]").length) {
						$(".reverse").prop("checked",true);
					}
					else {
						$(".reverse").prop("checked",false);
					}
					$("#jz1").hide();
					$("#jz2 ").show();	
				}
}
	//宽度
	$(".shopcart-contain-title li:eq(0)").css({"width":"100px","text-align":"left"}) 
	$(".shopcart-contain-title li:eq(1)").css("width","520px");
		$(".shopcart-contain-title li:eq(2)").css("width","214px");
		$(".shopcart-contain-title li:eq(3)").css("width","214px");   
		
		$(".shopcart-bottom li:eq(0)").css({"width":"100px","text-align":"left","margin-left":"0"});
		$(".shopcart-bottom li:eq(1)").css("float","right");
		$(".shopcart-bottom li:eq(2)").css("float","right");
		$(".shopcart-bottom li:eq(3)").css("float","right");
		
			$("input[type=checkbox]").each(function(){
				$(this).attr("checked", true);
				})
		
			// 全选/取消全部 
			$(".reverse").click(function () {
				if ($(this).prop("checked")) {
					$(".reverse").prop("checked",true);
					$("#playList input[name=newslist]").each(function() { 
						$(this).prop("checked",true);
					});
				}
				else{
					
				$(".reverse").prop("checked",false);
				$("#playList input[name=newslist]").each(function() { 
					$(this).prop("checked", false);

				});
				 
				}
				GetCount();
			});

					
			$("#playList input[name=newslist]").click(function () {
				
				GetCount();
			});
			
			////总价计算1
			
			var len = $(".tot");
			var num = 0;
			for (var i = 0; i < len.length; i++) {
				num = parseInt(num) + parseInt($(len[i]).html());
			}
			$("#zong1").text(parseInt(num).toFixed(2));
			$("#shuliang").text(len.length);
			
		

})

//删除车辆
function sidebar_goods_delete(car_id) {
	var r = confirm("确定移出该车？");
	if(r==true) {
		$.ajax({
		type:"POST",
		url:"configure/cart_goods_delete.php",
		cache:false,
		data:{"car_id":car_id,
		  },
		success:function(data) {
			var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
			//成功
			if(data==1) {
				//删除成功，重新加载页面
				 window.location.reload();
			}
			//未登录
			else if(data==2) {
				show_model(0);
			}
			//
			//删除失败
			else if(data==0) {
				content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">删除失败！请刷新后重试~</p>';
				show_clue(content);
				
			}
			//该车子已经不在购物车了
			else if(data==3) {
				content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">该二手车早已不在购物车啦~</p>';
				show_clue(content);
			}
		},
	});
	}
	
}
/*用户收藏*/
function car_collect(obj) {
	var collect_type = 0;
	$.ajax({
			  type:"POST",
			  url:"configure/car_collect.php",
			  cache:false,
			  data:{"car_id":$(obj).val(),
			  		"collect_type":collect_type,
			  },
			  
			  success:function(data) {
				  
				  var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				  //失败
				  
					  if(data==0) {
						  content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">该车子已经在收藏夹啦！</p>';
						  show_clue(content);
					  }
					  //未登录
					  else if(data==2) {  
						  show_model(0);
					  }
					  //成功
					  else if(data==1) {
						  $(".collect-img").attr('src','images/star_full.png');
						  sidebar_goods_delete($(obj).val());
					  }
			  },
		});
}

//未选中任何商品提交预约
function shopping_empty() {
	//调用对话框
	var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
	content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">对不起，您未勾选任何二手车！</p>';
	show_clue(content);
}
//点击确认提交购物车商品
function shopping_submit() {
	var car_id="";
	$("#playList input[name=newslist]").each(function () {
		if ($(this).prop("checked")) {
			
			car_id+=$(this).val()+',';
		}
	
	});
	car_id = car_id.substr(0,car_id.length-1)
	
	$.ajax({
		type:"POST",
		url:"configure/make_order_check.php",
		cache:false,
		data:{
				car_id:car_id,
					//如果是在购物车结算，则需要考虑是否登录的问题
				user_id:"",
				user_type:"2",
			},
		beforeSend:function(){
				$(".shopcart-bottomd").html("预约中...").addClass("shopcart-disable").attr('disabled',"true");
				},
		success:function(data){
			
				var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				if(data==1) {
					$(".shopcart-contain").html('<div class="shoppingcart-success"><img src="images/success.png" /><span>预约成功~请保持手机畅通!</span><div class="shoppingcart-around"><a href="buy?city=<?php echo $city;?>">车宇宙那么大~我还想逛逛</a></div></div>');
					$(".shopcart-bottom").remove();
					//content+= '<p class="helpbuy-demand-success">预约成功，请保持手机畅通~</p>';
//					show_clue(content);
				}
				
				//预约失败
				else if(data==0) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">预约失败，请刷新页面重试~</p>';
					show_clue(content);
				}
				//未登录
				else if(data==4) {
					show_model(0);
				}
				//该车已卖掉/下架
				else if(data==2) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">太不巧啦~您选中的二手车中已出售！</p>';
					show_clue(content);
					
				}
				//已经预约了某辆车
				else if(data==3) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">请勿重复预约，可至<a href="<?php echo $SER_ADD;?>/user_order">个人中心</a>查看已预约的车！</p>';
					show_clue(content);
				}
				$(".shopcart-bottomd").html("预约中...").removeClass("shopcart-disable").removeAttr('disabled');
			
			},
	});
//	$.ajax({
//			type:"POST",
//			url:"configure/make_order_check.php",
//			cache:false,
//			data:{
//				car_id:$(".cardetails-buttonab").val(),
//				user_id:$("#make_order_mobile").val(),
//				user_type:"1",
//			},
//			beforeSend:function(){
//				
//				$(".make_order_submit").html("预约中...").attr('disabled',"true");
//				  
//			  },
//			success:function(data){
//				
//				var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
//				$(".fullbg,#model_dialog_order").hide(); 
//				//预约成功
//				if(data==1) {
//					content+= '<p class="helpbuy-demand-success">预约成功，请保持手机畅通~</p>';
//					show_clue(content);
//				}
//				
//				//预约失败
//				else if(data==0) {
//					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">预约失败，刷新页面重试~</p>';
//					show_clue(content);
//				}
//				//该车已卖掉/下架
//				else if(data==2) {
//					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">太不巧啦~该二手车已卖出！</p>';
//					show_clue(content);
//					
//				}
//				else if(data==3) {
//					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">请勿重复预约该车！可至个人中心查看</p>';
//					show_clue(content);
//				}
//				$(".make_order_submit").html("确认预约").removeAttr('disabled');
//			}
//		});
}

		
</script>
<?php
	$conn = db_connect();
	$conn->query("set names utf8");
	$result = $conn->query("select shopping_cart from ".$users_info." where user_id='".$_SESSION['user_id']."'");
	$cart_content = "";
	if(!$result) {
		$cart_content = '<div class="shoppingcart-empty"><img src="images/shopping_cart.png" />糟糕~系统好像在开小差！刷新一下试试吧~</div>';
	}
	else {
		$row = $result->fetch_assoc();
		$car_id = $row['shopping_cart'];
		if($car_id == '') {
			$cart_content = '<div class="shoppingcart-empty">您的购物车空空如也~赶快<a href="buy?city=hangzhou" class="shoppingcart-buy">去挑一辆心爱的车</a>吧~</div>';
		}
		else {
			$result1 = $conn->query("select id,brand_name,plate_date,driving_distance,e_standard,sell_city,current_price,new_price,img_src from ".$car_dataset." where id in(".$car_id.") order by field (id,".$car_id.") asc");
			$num_result = $result1->num_rows;
			$price_total = 0;
			$cart_content = '<ul class="shopcart-contain-title"><li><input type="checkbox"  class="reverse" />&nbsp;&nbsp;全选&nbsp;&nbsp;</li><li>车辆信息</li><li>价格</li><li>操作</li></ul>';
			while($row1=$result1->fetch_assoc()) {
				$arr = explode(";",$row1['img_src']);
				$img_src = $arr[0];
				$img_src = $img_src==""?"../cheyuzhou_bg/upload/lost.jpg":"../cheyuzhou_bg/".$img_src;
				$price_total +=$row1['current_price'];
				$cart_content.='<ul class="shopcart-contain-list"><li class="shopcartcla"><input type="checkbox" name="newslist" value="'.$row1['id'].'" /></li><li class="shopcartclb"><img src="'.$img_src.'" /><div class="shopcart-font"><span><a href="'.$SER_ADD.'/buycar_detail?id='.$row1['id'].'">'.$row1['brand_name'].'</a></span><p>上牌时间：'.$row1['plate_date'].'   表显里程:'.$row1['driving_distance'].'万公里<br />排放标准：'.$standard_change[$row1['e_standard']].'   销售城市：'.$city_change[$row1['sell_city']].'</p></div> </li><li class="shopcartclc"><span>￥<span class="tot">'.$row1['current_price'].'</span>万</span><p>新车价￥'.$row1['new_price'].'万</p></li><li class="shopcartcld"><span class="splid"><button type="submit" onclick="car_collect(this)" value="'.$row1['id'].'">移入收藏夹</button></span><p class="splidp"><a href="javascript:void(0)" onclick="sidebar_goods_delete(\''.$row1['id'].'\')">删除</a></p></li></ul>';
			}
			
		}
	}
?>

 <div class="shopcart-title">
    	购物车&nbsp;&nbsp;
    </div>    
    	
    <div id="playList">    
        <div class="shopcart-contain">
            <?php echo $cart_content;?>
        </div> 
        
        <ul class="shopcart-bottom">
        	<li><input type="checkbox"  class="reverse" />&nbsp;&nbsp;全选</li>
            <li><!--无样式--><div id="jz1" onclick="shopping_empty()">确认预约</div>
                <button class="shopcart-bottomd" id="jz2" onclick="shopping_submit()">确认预约</button></li>
            <li>合计(不含运费)￥:<span id="zong1">0</span>万元</li>
            <li>共计<span id="shuliang">0</span>辆车</li>
        </ul>
	</div>
<?php
	require_once("footer.php");
?>