<?php
	if (!session_id()) session_start();
	//判断城市
	$city = isset($_GET['city'])?$_GET['city']:"hangzhou";
	
	//默认情况的话将从ip地址获取到城市,前期的话用杭州
	
?>
<?php
	require_once('configure/web_fns.php');
	require_once('configure/parameter.php');
?>
<script>
 function AddFavorite(sURL, sTitle)
		{
			try
			{
				window.external.addFavorite(sURL, sTitle);
			}
			catch (e)
			{
				try
				{
					window.sidebar.addPanel(sTitle, sURL, "");
				}
				catch (e)
				{
					alert("加入收藏失败，请使用Ctrl+D进行添加");
				}
			}
		}

</script>
<div class="model-dialog" id="model_dialog_clue">
	
</div>
<div class="fullbg"></div>
    <!--用来切换的模态对话框-->
    <div class="model-dialog" id="login_out_model">
    	<!--登录框-->
         <form class="login-form" id="login-form">
        	<div class="model-dialog-close">
            	<span>登录</span>
        		<i class="fa fa-times" onClick="close_dialog()"></i>
        	</div>
        	<div class="input-group">
                <input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="login-phone" required>
            </div>
            
            <div class="input-group">
                <input type="password" class="form-control" placeholder="请输入密码" name="password" id="login-password" required>
            </div>
                
            <div class="login-form-footer">
            	<button type="submit" class="form-control login-submit">登录</button>
                <span class="login-footer-left"><a href="retrievepassword.php">忘记密码 <i class="fa fa-angle-double-right"></i></a></span>
                <span class="login-footer-right">还没有车宇宙账号？<a href="javascript:void(0)" class="login-register">点击注册<i class="fa fa-angle-double-right"></i></a></a></span>
            </div>
            
        </form>
        <form class="register-form" id="register-form">
        	<div class="model-dialog-close">
            	<span>注册</span>
        		<i class="fa fa-times" onClick="close_dialog()"></i>
        	</div>
        	<div class="input-group">
            	
                <input type="mobile" class="form-control" placeholder="请输入11位手机号" name="phone" id="register-phone" required>
            </div>
            <div class="input-group">
            	
                <input type="password" class="form-control" placeholder="输入密码：6-20位字母和数字" name="password" id="register-password" required>
            </div>
             
            <div class="input-group">
                
                <button class="btn btn-default register-phone-code" onClick="get_code()" >获取验证码</button>
                <input type="code" class="form-control register-code" placeholder="输入手机验证码" name="code" id="login-code" required>
                
            </div> 
            <div class="login-form-footer">
            	<button type="submit" class="form-control register-submit">注册</button>
                <div class="login-footer-exist">已有账号？<a href="javascript:void(0)" class="register-login">返回登录<i class="fa fa-angle-double-right"></i></a></a></div>
            </div>
        </form>
    </div>
	 <?php
	 	//获取购物车的信息
		$cart_content = "";
		$num_result = 0;
		$price_total = 0;
		if(!isset($_SESSION['user_id'])) {
			$cart_content = '<div class="ibar_plugin_content"><div class="shopcart_more_face"><img src="images/face.png" /></div></div><div class="shopcart_state"><a href="user_login"><span>登录</span></a></div>';
		}
		else {
		$conn = db_connect();
		$conn->query("set names utf8");
		$result = $conn->query("select shopping_cart from ".$users_info." where user_id='".$_SESSION['user_id']."'");
		
		if(!$result) {
			$cart_content = '<div class="shoppingcart-empty sidebar-shoppingcart-empty">糟糕~系统好像在开小差！刷新一下试试吧~</div>';
		}
		else {
			$row = $result->fetch_assoc();
			$car_id = $row['shopping_cart'];
			if($car_id == '') {
				$cart_content = '<div class="shoppingcart-empty sidebar-shoppingcart-empty">您的购物车空空如也~<br />赶快<a href="buy?city=hangzhou" class="shoppingcart-buy">去挑一辆心爱的车</a>吧~</div>';
			}
			else {
				$result1 = $conn->query("select id,brand_name,current_price,img_src from ".$car_dataset." where id in(".$car_id.") order by field (id,".$car_id.") asc");
				$num_result = $result1->num_rows;
				
				
				while($row1=$result1->fetch_assoc()) {
					$arr = explode(";",$row1['img_src']);
					$img_src = $arr[0];
					$img_src = $img_src==""?"upload/lost.jpg":$img_src;
					$price_total +=$row1['current_price'];
					$cart_content.='<li><div class="cart_item_pic"><a href="buycar_detail?id='.$row1['id'].'"><img src="../cheyuzhou_bg/'.$img_src.'" /></a></div><div class="cart_item_desc"><a href="buycar_detail?id='.$row1['id'].'" class="cart_item_name">'.$row1['brand_name'].'</a><span class="cart_price">￥'.$row1['current_price'].'万元</span></div><div class="cart_item_delete"><a href="javascript:void(0)" onclick="sidebar_goods_delete(\''.$row1['id'].'\')">删除</a></div></li>';
				}
				
			}
		  
		}
		}
     ?>
    
   
    <div class="header">
    
    	<div class="header-bg">
        	<div class="header-container">
            	<ul class="header-container-left">
                	<li>
                        <img src="images/index_location.png" />
                        <a href="index?city=hangzhou" class="header-city">杭州&nbsp;<i class="fa fa-angle-down"></i></a>
                    </li>
                    <li>|</li>
                    <?php if(isset($_SESSION['user_id'])) {
							$user_id=$_SESSION['user_id'];
							echo '<li id="lia"><a href="'.$SER_ADD.'/user_order" class="login_register_bt">Hi,'.$user_id.'<i class="fa fa-sort-desc"></i></a><div class="mine-center"><a href="'.$SER_ADD.'/user_order">个人中心</a><a href="'.$SER_ADD.'/configure/user_logout.php?id='.$user_id.'">退出登录</a><div></li>
';
						}
						else {
							echo '<li><a href="javascript:show_model(0);" class="login_register_bt">Hi,请登录</a></li><li><a href="javascript:show_model(1)" class="login_register_bt">注册</a></li>';
						}
					?>
                	               
                </ul>
            	
                <div class="header-city-container">
            		<p class="header-city-explain">目前只支持杭州地区，其他城市将陆续开放，敬请期待！</p>
            	</div>
                
                
            	<ul class="header-container-right">
                   <!-- <li><a href="javascript:void(0)" onclick="AddFavorite(window.location,document.title)" ><i class="fa fa-star"></i>收藏夹</a></li>-->
                	<li><a href="<?php echo $SER_ADD;?>/shopping_cart"><i class="fa fa-shopping-cart"></i>购物车<span class="shopping_number"><?php echo $num_result;?></span></a></li>
                    <li>|</li>
                    <li>
                        <a href="index?city=hangzhou">网站导航&nbsp;<i class="fa fa-angle-down"></i></a>
                    </li>
                    <li><a href="<?php echo $SER_ADD;?>/businesslogin">经销商合作<i class="fa fa-arrow-circle-o-right"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
	
  
	
     <!--logo、搜索部分-->
     <div class="header-three-color"> 
         <div class="header-three-bg">
            <div class="header-logo">
                <a href="<?php echo $SER_ADD;?>/index"><img src="<?php echo $SER_ADD;?>/images/index_logo.png" /></a>
            </div>
            
            <div class="header-search-all">
                <div class="header-search">
                    <input type="text" class="header-input" placeholder="请输入品牌、车系搜索" aria-describedby="sizing-addon1" value="<?php echo isset($_GET['searchtitle'])?$_GET['searchtitle']:'';?>">
                    <button class="header-btn" type="button" onclick="car_navbar_search('<?php echo $city;?>')">搜索</button>
                </div>
                
                <ul class="header-search-title">
                    <li>价格：</li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p1';?>">3万以下</a></li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p4';?>">10-15万</a></li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p7';?>">30-40万</a></li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&price=p8';?>">40万以上</a></li>
                    <li>&nbsp;&nbsp;|&nbsp;&nbsp;</li>
                    <li>品牌：</li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=dazhong';?>">大众</a></li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=bmw';?>">宝马</a></li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=audi';?>">奥迪</a></li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=benz';?>">奔驰</a></li>
                    <li><a href="<?php echo $SER_ADD.'/buy?city='.$city.'&brand=fengtian';?>">丰田</a></li>
                </ul>
            </div>
            
           <div class="header-tel">
                <span>400-0975-588</span>
                <p>在线时间：8:00-18:00</p>
            </div>
        </div>
    
	</div>
      <div class="navbar-container">
           <div class="nav-bar">
                <ul>
                    
                    <?php
						if(basename($_SERVER['PHP_SELF'])=='index.php')
							echo '<li class="nav-bar-a">全部车辆分类</li>';
						$links = array();
						$links[]=array("首页","index.php","index");//或者这边直接改成根据ip地址获取
						$links[]=array("买车","buy_car.php","buy?city=".$city);
						$links[]=array("卖车","sell_car.php","sell_car");
						$links[]=array("帮买","helpbuy.php","helpbuy");
						//$links[]=array("论坛","../disz/upload/forum.php?mod=forumdisplay&fid=2","../disz/upload/forum.php?mod=forumdisplay&fid=2");
						$links[]=array("服务保障","service.php","service");
						$self_page = basename($_SERVER['PHP_SELF']);
						foreach($links as $link){
							printf('<a href="%s"><li %s>%s</li></a>' , $link[2],$self_page==$link[1]?'class="navbar-current"':'class=""',$link[0]);
								
							}
					?>
                                 
                </ul>
            </div>
      </div>
         <!--右侧侧边栏-->
      <!--右侧贴边导航quick_links.js控制-->
	
     <div class="mui-mbar-tabs">

                <div class="quick_links_panel">
                    <div id="quick_links" class="quick_links">
                    	<ul>
                            <li id="shopCart" class="moaa"><a href="#" class="message_list"><i class="message"></i></a> 
                                 <div class="shopcart_span" id="end">购物车</div>
                                 	<span class="cart_num"><?php echo $num_result;?></span>
                            </li>
                                
                             <li><a target="_self" href="javascript:void(0)" onclick="window.open('http://chat16.live800.com/live800/chatClient/chatbox.jsp?companyID=607375&configID=134325&jid=2704577628','_blank','scrollbars=0,resizable=0,width=590,height=500;');return false;"><i class="setting"></i></a>
                                <div class="mp_tooltip">
                                    客服中心<i class="icon_arrow_right_black"></i>
                                 </div>
                            </li>
                            
                             <li><a href="javascript:void(0)" class="mpbtn_histroy"><i class="view"></i></a>
                                <div class="mp_tooltip" style="width:180px;left:-180px; height:50px; line-height:1.8; padding:5px 5px; text-align:left;">
                                   客服热线：400-0975-588<br />
                                   服务时间：9：00-22：00
                                   <i class="icon_arrow_right_black"></i>
                                 </div>
                            </li>
                       </ul>  
                    </div>
                    
                     <div class="quick_toggle">
                     	<ul>
                            
                            <li><a href="javascript:void(0)"><i class="mpbtn_qrcode"></i></a>
                                <div class="mp_qrcode" style="display: none;">
                                  <img src="images/erweima2.jpg" width="100" height="100" />
                                  <i class="icon_arrow_white"></i>
                                </div>
                            </li>
                            <li><a href="javascript:scroll(0,0)" class="return_top"><i class="top"></i></a></li>
                         </ul>   
                    </div>
                </div>
                
                <div class="shopcart_more">
                	<div class="ibar_plugin_content">
                        <div class="ibar_cart_group ibar_cart_product">
                          	<ul class="ibar_car_list">
                              <?php
							     
								  echo $cart_content;
							  ?>
                             </ul>
                       <!--     <ul>
                            <li>
                                <div class="cart_item_pic"><a href="#"><img src="../cheyuzhou_bg/upload/1a0e1e01cf55e6fdd760c016a0be4a60.jpg" /></a></div>
                                <div class="cart_item_desc"><a href="#" class="cart_item_name">夏季透气真皮鞋反绒男士休闲鞋韩版磨砂驾车鞋子</a>
                                <span class="cart_price">￥700.00</span>
                                </div>
                                 <div class="cart_item_delete"><a href="#">删除</a></div>
                            </li>
                            
                             <li>
                                <div class="cart_item_pic"><a href="#"><img src="images/xiez.jpg" /></a></div>
                                <div class="cart_item_desc"><a href="#" class="cart_item_name">夏季透气真皮鞋反绒男士休闲鞋韩版磨砂驾车鞋子</a>
                                <span class="cart_price">￥700.00</span>
                                </div>
                                 <div class="cart_item_delete"><a href="#">删除</a></div>
                            </li>
                            
                             <li>
                                <div class="cart_item_pic"><a href="#"><img src="images/xiez.jpg" /></a></div>
                                <div class="cart_item_desc"><a href="#" class="cart_item_name">夏季透气真皮鞋反绒男士休闲鞋韩版磨砂驾车鞋子</a>
                                <span class="cart_price">￥700.00</span>
                                </div>
                                 <div class="cart_item_delete"><a href="#">删除</a></div>
                            </li>
                            
                             <li>
                                <div class="cart_item_pic"><a href="#"><img src="images/xiez.jpg" /></a></div>
                                <div class="cart_item_desc"><a href="#" class="cart_item_name">夏季透气真皮鞋反绒男士休闲鞋韩版磨砂驾车鞋子</a>
                                <span class="cart_price">￥700.00</span>
                                </div>
                                 <div class="cart_item_delete"><a href="#">删除</a></div>
                            </li>
                   		 </ul>-->
                        </div>
                       <?php
					       if(isset($_SESSION['user_id'])) {
							   echo '<div class="cart_handler"><div class="cart_handler_header"><span class="cart_handler_left">共<span class="cart_price cart_number">'.$num_result.'</span>辆车</span><span class="cart_handler_right">￥<span class="cart_handler_detail">'.$price_total.'</span>万元</span></div><p><a href="shopping_cart" class="cart_go_btn" target="_blank">去购物车结算</a></p></div>';
						   }
					   ?>
                      
                	</div>
           	 </div>

    </div>
     
    
    <script type="text/javascript">

			$(function(){
			//购物车
			$(".moaa").click(function (event){
				 event.stopPropagation();
				 
				 $(".shopcart_more").toggle();
				$(".moaa").toggleClass("shopcart_background");
				
				$(this).children("span").toggleClass("shopcart_numbg");
				//alert(shopcart_numbg)
				//css("background", "white")
                 return false;
				});
			
			$(document).click(function(event){
                  var _con = $(".shopcart_more");   // 设置目标区域
                  if(!_con.is(event.target) && _con.has(event.target).length == 0){ // Mark 1
                    $(".shopcart_more").hide();
					$(".moaa").removeClass("shopcart_background");  
					$(".moaa").children("span").removeClass("shopcart_numbg");
					         //淡出消失
                  }
				});
			

			
			//头部注销
			$("#lia").hover(function(){
				$(".mine-center").show();
				},
				function(){
				$(".mine-center").hide();
			});

			
	 		$(".quick_links_panel li").mouseenter(function() {
                $(this).children("span").addClass("bbq1").removeClass("cart_num");
                $(this).children(".mp_tooltip").css("visibility", "visible");
                $(this).children(".ibar_login_box").css("display", "block");
            });
            $(".quick_links_panel li").mouseleave(function() {
				$(this).children("span").addClass("cart_num").removeClass("bbq1");
                $(this).children(".mp_tooltip").css("visibility", "hidden");
                $(this).children(".ibar_login_box").css("display", "none");
            });
			
			
			
            $(".quick_toggle li").mouseover(function() {
                $(this).children(".mp_qrcode").show();
            });
            $(".quick_toggle li").mouseleave(function() {
                $(this).children(".mp_qrcode").hide();
            });
			
			});
	//删除车辆
	function sidebar_goods_delete(car_id) {
		$.ajax({
			type:"POST",
			url:"configure/cart_goods_delete.php",
			cache:false,
			data:{"car_id":car_id,
			  },
			success:function(data) {
				var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				//成功
				if(data==1) {
					//删除成功，重新加载页面
					 window.location.reload();
				}
				//未登录
				else if(data==2) {
					show_model(0);
				}
				//
				//删除失败
				else if(data==0) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">删除失败！请刷新后重试~</p>';
				    show_clue(content);
					
				}
				//该车子已经不在购物车了
				else if(data==3) {
					content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">该二手车早已不在购物车啦~</p>';
				    show_clue(content);
				}
			},
		});
	}
    </script>