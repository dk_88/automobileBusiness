<?php
//	if(!isset($_GET['id'])||$_GET['id']=="") {
//		 header("Location: 404");
//		 exit;
//	}
?>
<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
	
	$sort = isset($_GET['sort'])?$_GET['sort']:"";
	$seq = isset($_GET['seq'])?$_GET['seq']:"asc";
	$sellcity = isset($_GET['sellcity'])?$_GET['sellcity']:"";
	//获取当前用户的收藏记录
	if(isset($_SESSION['user_id'])) {
		$result1 = $conn->query("select collect_cars from ".$users_info." where user_id='".$_SESSION['user_id']."'");
		$row1 = $result1->fetch_assoc();
		$user_collect = $row1['collect_cars'];
	}
	else
		$user_collect = "";
	//分页参数
	$showrow = 12; //一页显示的行数
	$curpage = empty($_GET['page']) ? 1 : $_GET['page']; //当前的页,还应该处理非数字的情况
	
	
	//去掉当前url中的page
	$current_url = $_SERVER['QUERY_STRING'];
	$str_ex = explode("&",$current_url);
	if(strstr($current_url,"page")===false) {
		$url = "?".$current_url."&page={page}";
	}
	else {
		$str_page = preg_replace("/(\&page=\d+)/","&page={page}",$current_url);//已经存在page的情况下将page进行替换
		$url = "?".$str_page;
	}
	$conn = db_connect();
	$conn->query("set names utf8");
	$result = $conn->query("select * from ".$admin_users." where username='".$_GET['id']."'");
	$row = $result->fetch_assoc();
	
	//获取该店铺车辆的信息
	$query = "select * from ".$car_dataset." where owner_phone='".$_GET['id']."'";
	if($sort!="") {
		switch($sort) {
			case "price":
				$query.=" order by current_price ".$seq;
				break;
			case "coty":
				$query.=" order by coty ".$seq;
				break;
			case "distance":
				$query.=" order by driving_distance ".$seq;
				break;
			default:;
		}
	}
	//记录总的条数
	$result2 = $conn->query("select * from ".$car_dataset." where owner_phone='".$_GET['id']."'");
	$total=$result2->num_rows;
	if (!empty($_GET['page']) && $total != 0 && $curpage > ceil($total / $showrow))
		$curpage = ceil($total_rows / $showrow);
	//实现分页,根据sort是否存在来设置不同的order条件
	
	$query .= $sort==""?" order by id desc limit " . ($curpage - 1) * $showrow . ",$showrow;":",id desc limit " . ($curpage - 1) * $showrow . ",$showrow;";
	$result1 = $conn->query($query);
	
?>


    <div class="businessshop-list">
    	 <ul>
         	<li class="businessshop-lista"><img src="images/busshop.jpg" /><i class="businessshopico"></i></li>
         	<li class="businessshop-listb">
            	 <dl>
                 	<dt><?php echo $row['postname'];?></dt>
                    <dd><i class="businesicoa"></i>地址：<?php echo $row['address'];?></dd>
                    <dd><i class="businesicob"></i>联系方式：<?php echo $_GET['id'];?></dd>
                    <dd><i class="businesicoc"></i>销售城市：
					<?php 
						if(!isset($city_change[$sellcity]))
							echo "未知";
						else 
							echo $city_change[$sellcity];
					?>
                	</dd>
                 </dl>
            </li>
            <li  class="businessshop-listc">
            	 <dl>
                 	<dt></dt>
                    <dd>评分：暂无数据</dd>
                    <dd>正在出售：<?php echo $total;?>辆</dd>
                    <dd>
                    	<span>商家资质：</span>
                        <div class="ddimg">
                            <img src="images/cxshop.png"  alt="诚信商家"   title="诚信商家"/>
                            <img src="images/yzshop.png"  alt="优质商家"  title="优质商家"/>
                        </div>
                    </dd>
                 </dl>
            
            </li>
         </ul>
    </div>  

<script type="text/javascript">
	setInterval(function(){
			$(".businessshop-button img").fadeOut(800).fadeIn(800);
		},800);
	function car_search(name,value,more) {
		//more参数用来记录filter当前的类别,以及记录排序的状态,0表示升序，1表示降序
		var url = window.location.href;//获取当前url
		
		url = url.replace(/(\&page=\d+)/,""); //去掉page
		var para = url.match(/([^\/]*\/){4}([^\/]*)/)[2];//获取当前地址下的各个参数
		
		var paras = para.split("&");
		if(para.indexOf(name)==-1) {
				//不存在的情况
				if(value!="all") {
					para+="&"+name+"="+value;//新的地址
					//如果是sort的话，根据more的值来改变当前的href
					if(name=="sort") {
						para = para+(more==0?"&seq=desc":"&seq=asc");
					}
					window.location.href=para;
					
				}
				else
					window.location.href=url;
		}
		else {
					for(i=0;i<paras.length;i++) {
						if(paras[i].indexOf(name)!=-1) {
						//查找到位置并替换
						if(value!="all") {
							paras[i]=name+"="+value;
						}
						else
							//若value值为all，则remove掉
							paras.splice(i,1);
					}
						if(paras[i].indexOf("seq")!=-1) {
							//查找到位置并替换
							if(value!="all")
								paras[i]=(more==0?"seq=desc":"seq=asc");
							else
								paras.splice(i,1);
				
						}
					}
					para = paras.join("&");
				window.location.href=para;
				}
				
				
	}
</script>
<div class="buycar-result">
        <div class="buycar-orderby">
            <ul>
                <li><a href="all" onclick="javascript:car_search('sort','all',0);return false">默认排序</a></li>
               
				   
					   <li class="buycara"><a href="price" onclick="javascript:car_search('sort','price',<?php if($sort=="price"&&$seq=="asc") echo 0; else echo 1;?>);return false" <?php echo $sort=="price"?'class="buycara-current"':'';?>>按价格&nbsp;<i class="fa fa-arrow-<?php if($sort=="price"&&$seq=="asc") echo "up"; else echo "down";?>"></i></a></li>
                       <li class="buycara"><a href="price" onclick="javascript:car_search('sort','coty',<?php if($sort=="coty"&&$seq=="asc") echo 0; else echo 1;?>);return false" <?php echo $sort=="coty"?'class="buycara-current"':'';?>>按车龄&nbsp;<i class="fa fa-arrow-<?php if($sort=="coty"&&$seq=="asc") echo "up"; else echo "down";?>"></i></a></li>
                       <li class="buycara"><a href="price" onclick="javascript:car_search('sort','distance',<?php if($sort=="distance"&&$seq=="asc") echo 0; else echo 1;?>);return false" <?php echo $sort=="distance"?'class="buycara-current"':'';?>>按里程&nbsp;<i class="fa fa-arrow-<?php if($sort=="distance"&&$seq=="asc") echo "up"; else echo "down";?>"></i></a></li>
            </ul>
        </div> 
        
        <div class="buycar-result-a">已经为您找到<span><?php echo $total;?></span>辆车</div>   
   </div>
  
   
   <div class="buycar-list">
       <?php
	       $show_cardetail = '<ul class="buycar-car-list">';
		   while ($row = $result1->fetch_assoc()){
			   $img_src=explode(";",$row['img_src']);
			   $img_src[0]=$img_src[0]==""?"upload/lost.jpg":$img_src[0];
			   $show_cardetail.='<a href="'.$SER_ADD.'/buycar_detail?id='.$row['id'].'"><li><img src="../cheyuzhou_bg/'.$img_src[0].'"><p>'.$row['brand_name'].'</p><span>¥'.$row['current_price'].'万</span><div class="buycar-newprice">当前款新车价格：<span>¥'.$row['new_price'].'万</span></div><div class="buycar-line"></div><div class="buycar-detail">上牌 '.$row['plate_date'].' ｜ 里程'.$row['driving_distance'].'万公里</div></li></a>';
		   }
		   $show_cardetail.='</ul>';
		   echo $show_cardetail;
       ?>
    	
     </div>
      <div class="buycar-pages">
         <?php
             //显示分页
			 $page = new page($total, $showrow, $curpage, $url, 2);
			 echo $page->myde_write();
                    
          ?>
     </div>
<?php

	require_once('footer.php'); 
?> 
