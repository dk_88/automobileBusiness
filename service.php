<?php
	require_once('header.php');
	
?>
<link rel="stylesheet" href="css/jquery.fullPage.css">
<script src="js/jquery.fullPage.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script>
$(function(){
	$('.section1').find('.img1').animate({opacity:'1'},3000)
	$('.section1').find('h3').animate({left:'0'},500,function(){
		$('.section1').find('.paction').animate({left:'0'},500)
		});
	
	$('#dowebok').fullpage({
		sectionsColor: ['#3d96cf', '#4a6bcd', '#3d96cf', '#4a6bcd','#3d96cf','#194068'],
		'navigation': true,
		
		afterLoad: function(anchorLink, index){
			
			if(index == 2){
				$('.section2').find('h3').delay(500).animate({
					bottom: '-50'
				},500,function(){
					$('.section2').find('.paction').delay(500).animate({
					bottom: '-50'
					},500,'easeOutExpo')	
				});
			}
			if(index == 3){
				$('.section3').find('h3').delay(500).animate({
				right: '0'
				},500,function(){
					$('.section3').find('.paction').delay(500).animate({
					right: '0'
					},100,'easeOutExpo')

					$('.section3').find('.imgc1').animate({opacity:'1'},400,function(){
						$('.section3').find('.imgc2').animate({opacity:'1'},400,function(){
						$('.section3').find('.imgc3').animate({opacity:'1'},400,function(){
						$('.section3').find('.imgc4').animate({opacity:'1'},400,function(){
						$('.section3').find('.imgc5').animate({opacity:'1'},400,function(){
						$('.section3').find('.imgc6').animate({opacity:'1'},300,function(){
						$('.section3').find('.imgc7').animate({opacity:'1'},300)
						})
						})
						})
						})
						})
						})
					
				});
			}
			
			if(index == 4){
				$('.section4').find('h3').delay(500).animate({
					left: '0'
				},500,function(){
					$('.section4').find('.paction').delay(500).animate({
					bottom: '0'
					},500,'easeOutExpo')	
				});
			}
			
			if(index == 5){
				$('.section5').find('h3').delay(500).animate({
					bottom: '0'
				},500,function(){
					$('.section5').find('.paction').delay(500).animate({
					bottom: '0'
					},500,function(){
						$('.section5').find('.imge1').delay(500).animate({
							left: '0',
							bottom:'0'
							},1000,'easeOutExpo')	
						})
				});
			}
			
			if(index == 6){
				
				$('.section6').find('.imgf1').delay(500).animate({
				 left: '0',
				 bottom:'-300'
				},500)

			}
		},
		
	});
	
});
</script>
<div id="dowebok">
	
	<div class="section section1">
	
    <div class="head-bg">
    	<?php
			require_once('navbar.php');
		?>
   	
    </div>
    <div class="section1-bg">
     	<p>&nbsp;</p>
		<h3><span>海量  精选  好车</span> 任您挑选</h3>
        <p>&nbsp;</p>
		<p class="paction">—— 车宇宙二手车服务保障 ——</p>
        <img class="img1" src="images/sa1.png" />
	</div>
    </div>
    
	<div class="section section2">
		<h3>提供<span>最让人放心的</span>车源</h3>
         <p>&nbsp;</p>
		<p class="paction">车宇宙二手车服务保障</p>
	</div>
	<div class="section section3">
      		 <p>&nbsp;</p>
          	 <p>&nbsp;</p>
		<h3><span>贴心的</span> 买车服务</h3>
        	 <p>&nbsp;</p>
		<p class="paction">全程陪您找车、看车、验车，过户、上牌、一条龙服务</p>
         	<p>&nbsp;</p>
		    <p>&nbsp;</p>
            <div>
                <img class="imgc1" src="images/scc.jpg" />
                <img class="imgc2" src="images/scd.jpg" />
                <img class="imgc3" src="images/sce.jpg" />
            </div>
            <div>
                <img class="imgc6" src="images/sch.jpg" />
                <img class="imgc4" src="images/scf.jpg" />
                <img class="imgc5" src="images/scg.jpg" />
                <img class="imgc7" src="images/sci.jpg" />
            </div>
  
	</div>
    
	<div class="section section4">
    	 <p>&nbsp;</p>
         <p>&nbsp;</p>
		<h3><span>安全　透明</span> 交易</h3>
         <p>&nbsp;</p>
		<p class="paction">无商家加价、无交易差价，透明交易、安全放心</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
        <img class="imgd1" src="images/sdb.png" />
	</div>
    
    <div class="section section5">
     	<p>&nbsp;</p>
        <p>&nbsp;</p>
		<h3>让您的购车<span>无售后之忧</span></h3>
         <p>&nbsp;</p>
		<p class="paction">14天无理由退车、2年4万公里售后质保</p>
          <p>&nbsp;</p>
          	 <p>&nbsp;</p>
        <img class="imge1" src="images/seb.png" />
	</div>
    
    <div class="section section6">
    	<div class="section6-bg">
          <div class="go-state">
              <a href="buy?city=<?php echo $city?>"><div class="gotobuy">我要买车</div></a>
              <a href="sell_car"><div class="gotosell">我要卖车</div></a>
          </div>
          <img class="imgf1" src="images/sfb.png" />

        </div>
        <?php
			require_once('footer.php')
		?>
    
    </div>
</div>
