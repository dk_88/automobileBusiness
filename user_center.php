<?php
	if (!session_id()) session_start();
	if(!isset($_SESSION['user_id'])) {
		 header("Location: user_login");
		 exit;
	}
?>
<?php
	require_once('header.php');
	require_once('navbar.php');
	require_once('configure/db_fns.php');
	require_once('configure/parameter.php');
	require_once('paganation/page.class.php');
?>
<script src="js/area.js" type="text/javascript"></script>
<link href="css/cropper.min.css" rel="stylesheet"/>
<link href="css/main.css" rel="stylesheet"/>
<script src="js/cropper.min.js"></script>
<script src="js/main.js"></script>
<?php
	$conn = db_connect();
	$conn->query("set names utf8");
	$result = $conn->query("select * from ".$users_info." where user_id='".$_SESSION['user_id']."'");
	if($result) {
		$row = $result->fetch_assoc();
		$collect_cars = $row['collect_cars'];
	}
	
?>
 <div class="usercenter-box">
            <div class="usercenter-box-left">
                <div class="usercenter-box-left-face">
                    <div class="usercenter-box-left-faceimg"><img src="<?php echo $row['img_src']==""?'images/default.png':$row['img_src'];?>" /></div>
                    <div class="usercenter-box-left-font"><?php echo $row['nickname']==""?$row['user_id']:$row['nickname']; ?></div>
                </div>
                
                <div class="usercenter-box-left-ul">
                    <ul>
                    	<?php
							require_once('usercenter_navbar.php');
						?>
                    </ul>

                </div>
            </div>
            
           <div class="usercenter-box-right" id="tab">
                 <ul class="tab_menu">
                    <li class="useselected useleft">个人信息</li>
                    <li>账户安全</li>
                    <!--<li>已付款</li>
                    <li>交易关闭</li>
                    <li>交易完成</li>-->
                </ul>


                <div class="tab_box">
                <div class="tab_box-divb">
                	 	<div class="usercenter-box-right-infor-left" id="crop-avatar">
                    	<div class="usercenter-box-title">头像：</div>
            <div class="avatar-view" title="点击更改头像">
                <img src="<?php echo $row['img_src']==""?'images/default.png':$row['img_src'];?>" alt="Avatar"/>
               
            </div>
 			<div class="usercenter-box-right-infor-leftchangeimg">
                        	<p class="usercenter-box-right-infor-leftchangefont">点击图片修改头像</p>
                        </div>
            <!-- Cropping modal -->
            <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
                            <div class="modal-header">
                                <button class="close" data-dismiss="modal" type="button">&times;</button>
                                <h4 class="modal-title" id="avatar-modal-label">更换头像</h4>
                            </div>
                            <div class="modal-body">
                                <div class="avatar-body">

                                    <!-- Upload image and data -->
                                    <div class="avatar-upload">
                                        <input class="avatar-src" name="avatar_src" type="hidden"/>
                                        <input class="avatar-data" name="avatar_data" type="hidden"/>
                                        <label for="avatarInput">头像上传</label>
                                        <input class="avatar-input" id="avatarInput" name="avatar_file" type="file"/>
                                    </div>

                                    <!-- Crop and preview -->
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="avatar-wrapper"></div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="avatar-preview preview-lg"></div>
                                            <div class="avatar-preview preview-md"></div>
                                            <div class="avatar-preview preview-sm"></div>
                                        </div>
                                    </div>

                                    <div class="row avatar-btns">
                                       
                                        <div class="col-md-3">
                                            <button class="btn btn-primary btn-block avatar-save" type="submit">保存</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="modal-footer">
                              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div><!-- /.modal -->
                    
                    
                    	<form id="jsmineForm" class="jsmineForm">
                        <tbody>
                        	<table class="mine-detail">
                                <tr>
                                	<td class="mine-detaila">昵称：</td>
                                    <td><input type="nickname" class="mine-inputa mine-input" name="nickname" id="nickname" value="<?php echo $row['nickname'];?>"/></td>
                                </tr>
                               
                                <tr>
                                	<td class="mine-detaila">通讯地址：</td>
                                   
                                    <td>
                                    	<input type="text" class="mine-inputb mine-input" placeholder="详细地址" value="<?php echo $row['address'];?>" />
                                    </td>
                                </tr>
                                
                                
                                <tr>
                                	<td class="mine-detaila">&nbsp;</td>
                                    <td>
                                    	<button type="submit" class="minesub">保存</button>	
                                    </td>
                                </tr>
                                
                            </table>
                          </tbody>  
                        </form>
                    </div>
                    </div>
                  	<div class="hidea">
                            <tbody>
                                <table class="mine-countdetail">
                                     <tr>
                                     	<td class="countdetaila">
                                        	<div class="countdetaild"><i class="minea"></i><strong>登录密码</strong></div>
                                        </td>
                                        <td class="countdetailb"><span class="fontred">互联网帐号存在被盗风险，建议您定期更改密码以保护账户安全。</span></td>
                                        <td class="countdetailc"><a href="javascript:void(0);" class="minechangd changea">修改</a></td>
                                     </tr>  
                                     
                                       <tr>
                                     	<td class="countdetaila">
                                        	<div class="countdetaild"><i class="minea"></i><strong>手机验证</strong></div>
                                        </td>
                                        <td class="countdetailb">您验证的手机：<?php echo substr_replace($_SESSION['user_id'],"*****",3,5);?>  若已丢失或停用，请立即更换。</td>
                                        <td class="countdetailc"><a href="javascript:void(0);" class="minechangd changeb">修改</a></td>
                                     </tr>  
                                     
                                     
                                     <!--邮箱验证通过-->
                                     <!--<tr>
                                     	<td class="countdetaila">
                                        	<div class="countdetaild"><i class="minea"></i><strong>邮箱验证</strong></div>
                                        </td>
                                        <td class="countdetailb">验证后，可用于快速找回登录密码。</td>
                                        <td class="countdetailc"><a href="javascript:void(0);" class="minechangd changec">修改</a></td>
                                     </tr> 
                                     -->
                                     <?php
									 	if($row['user_email']=="") {
											echo '<tr><td class="countdetaila"><div class="countdetaild"><i class="mineb"></i><strong>邮箱验证</strong></div></td><td class="countdetailb">验证后，可用于快速找回登录密码。</td><td class="countdetailc"><a href="javascript:void(0);" class="minechangf changed">立即验证</a></td></tr>';
										}
										else {
											echo '<tr><td class="countdetaila"><div class="countdetaild"><i class="minea"></i><strong>邮箱验证</strong></div></td><td class="countdetailb">您验证的邮箱：'.substr_replace($row['user_email'],"*****",2,4).'</td><td class="countdetailc"><a href="javascript:void(0);" class="minechangd changec">修改</a></td></tr>';
										}
									 ?>
                                     
                                     
                                      <!--支付密码验证通过-->
                                      
                                      <!-- <tr>
                                     	<td class="countdetaila">
                                        	<div class="countdetaild"><i class="minea"></i><strong>支付密码</strong></div>
                                        </td>
                                        <td class="countdetailb">您还未设置支付密码，需设置后才能完成定金支付等功能。</td>
                                        <td class="countdetailc"><a href="javascript:void(0);" class="minechangd changee">修改</a></td>
                                      </tr>  -->
                                      
                                      <!-- <tr>
                                     	<td class="countdetaila">
                                        	<div class="countdetaild"><i class="mineb"></i><strong>支付密码</strong></div>
                                        </td>
                                        <td class="countdetailb">您还未设置支付密码，需设置后才能完成定金支付等功能。</td>
                                        <td class="countdetailc"><a href="javascript:void(0);" class="minechangf changef">立即设置</a></td>
                                     </tr>  -->
                                     
                                     <!--实名认证通过-->
                                   <!--  <tr>
                                     	<td class="countdetaila">
                                        	<div class="countdetaild"><i class="minea"></i><strong>实名认证</strong></div>
                                        </td>
                                        <td class="countdetailb">实名认证后，可通过实名信息找回支付密码，修改手机号等，提高账户安全性。</td>
                                        <td class="countdetailc"><a href="javascript:void(0);" class="minechangd">已认证</a></td>
                                     </tr>  -->
                                     
                                       <!--<tr>
                                     	<td class="countdetaila">
                                        	<div class="countdetaild"><i class="mineb"></i><strong>实名认证</strong></div>
                                        </td>
                                        <td class="countdetailb">实名认证后，可通过实名信息找回支付密码，修改手机号等，提高账户安全性。</td>
                                        <td class="countdetailc"><a href="javascript:void(0);" class="minechangf changeg">立即认证</a></td>
                                     </tr>  -->    
                                </table>
                            </tbody>     
                    </div>
                    <!--  	<div class="hidea"></div>
                        <div class="hidea"></div>
                        <div class="hidea"></div>
                     -->
                </div>
           </div>
       </div>
 <script type="text/javascript">
     $(function(){
        var  ua = $(".usercenter-box-left-ul ul a");
        ua.mouseover(function(){
            $(this).children("li").addClass('bgchange')
            $(this).children("li").children(".usercentera").addClass('usercenteraa').removeClass('usercentera')
            $(this).children("li").children(".usercenterb").addClass('usercenterba').removeClass('usercenterb')
            $(this).children("li").children(".usercenterc").addClass('usercenterca').removeClass('usercenterc')
            $(this).children("li").children(".usercenterd").addClass('usercenterda').removeClass('usercenterd')
            $(this).children("li").children(".usercentere").addClass('usercenterea').removeClass('usercentere')
            $(this).children("li").children(".usercenterf").addClass('usercenterfa').removeClass('usercenterf')
        });

         ua.mouseout(function(){
             $(this).children("li").removeClass('bgchange')
             $(this).children("li").children(".usercenteraa").removeClass('usercenteraa').addClass('usercentera')
             $(this).children("li").children(".usercenterba").removeClass('usercenterba').addClass('usercenterb')
             $(this).children("li").children(".usercenterca").removeClass('usercenterca').addClass('usercenterc')
             $(this).children("li").children(".usercenterda").removeClass('usercenterda').addClass('usercenterd')
             $(this).children("li").children(".usercenterea").removeClass('usercenterea').addClass('usercentere')
             $(this).children("li").children(".usercenterfa").removeClass('usercenterfa').addClass('usercenterf')
         }); 

       //选项卡
	    var $tab_li = $('#tab ul li');
		$tab_li.click(function(){
		$(this).addClass('useselected').siblings().removeClass('useselected');
		var index = $tab_li.index(this);
		$('div.tab_box > div').eq(index).show().siblings().hide();
		
		}); 
		
		
		//验证正则表达式
		$.validator.addMethod("nickname", function (value, element) {
			var nickname =/^[a-zA-Z0-9\u4e00-\u9fa5]{3,12}$/;
			return this.optional(element) || (nickname.test(value));
		}, '*昵称由3-12位字母、数字或中文组成');
		
		$.validator.addMethod("iden", function (value, element) {
		var isIDCard2=/^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/;
		return this.optional(element) || (isIDCard2.test(value));
		}, '*身份证格式不对');
		//验证个人信息并保存	
		$("#jsmineForm").validate({
			
			submitHandler: function (form) {
				var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				
				$.ajax({
				  type:"POST",
				  url:"configure/change_userinfo.php",
				  cache:false,
				  data:{
					  "nickname":$(".mine-inputa").val(),
					  "address":$(".mine-inputb").val(),
					  },
				  beforeSend:function(){
						 
					   $(".minesub").html("保存中...").attr('disabled',"true");
							  
						  },
				  success:function(data) {
					 if(data==1) {
						 
						 content+= '<p class="helpbuy-demand-success">信息保存成功！</p>';
						 show_clue(content);
						 
					 }
					 else if(data==0) {
						 content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">保存失败~请稍候重试!</p>';
						 show_clue(content);
					 }
					 else if(data==2) {
						 show_model(0);
					 }
					 $(".minesub").html("保存").removeAttr('disabled');
				  },
				})
			}
			});
		$("#countdetailForm").validate({
			rules:{
					samepassword: {
                    	equalTo: "#newpassword"
                	}
				 },
			
			messages:{
					samepassword: {
						equalTo: "*两次输入密码不一致"
					}
				},
			submitHandler: function (form) {
				var content = '<div class="model-dialog-close"><i class="fa fa-times" onClick="close_dialog()"></i></div>';
				
				$.ajax({
					type:"POST",
					url:"configure/user_changepassword.php",
					cache:false,
					data:{
						"old_password":$("#oldpassword").val(),
						"new_password":$("#newpassword").val(),
						},
					beforeSend:function(){
						   
						 $(".user-save1").html("保存中...").attr('disabled',"true");
								
							},
					success:function(data) {
						$(".user-save1").parents(".mmma").hide();
						
					    if(data==1) {
							//保存成功
							content+= '<p class="helpbuy-demand-success">密码修改成功！</p>';
							show_clue(content);
						   
					    }
					    else if(data==0) {
							//保存失败
						    content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">密码修改失败~请稍候重试!</p>';
						    show_clue(content);
					    }
						else if(data==3) {
							//保存失败
						    content+= '<p class="helpbuy-demand-success helpbuy-demand-wrong">旧密码输入有误!</p>';
						    show_clue(content);
					    }
					    else if(data==2) {
							//未登录
						    show_model(0);
					    }
					    $(".user-save1").html("保存").removeAttr('disabled');
					},
				  })
				
			}
			
			});	
		
		$("#countdetailFormb").validate({
			});	
			
		$("#countdetailFormc").validate({
			});	
             
		$("#countdetailFormcb").validate({
			});	
		
			       
		$("#countdetailFormd").validate({
			rules:{
					samepassword: {
                    	equalTo: "#newpasswordb"
                	}
				 },
			
			messages:{
					samepassword: {
						equalTo: "*两次输入密码不一致"
					}
				}
			
			});
		
		
		$("#countdetailFormdb").validate({
			});
		
		$("#countdetailForme").validate({
			});
		
		
		
		$(".changea").click(function(){
			$(".countdetailForm").show();
			});
		
		
		$(".changeb").click(function(){
			$(".countdetailFormb").show();
			});
			
		
		
		$(".changec").click(function(){
			$(".countdetailFormcb").show();
			});
		
		$(".changed").click(function(){
			$(".countdetailFormc").show();
			});
			
			
		$(".changee").click(function(){
			$(".countdetailFormdb").show();
			});
			
			
		$(".changef").click(function(){
			$(".countdetailFormd").show();
			});
			
			
		$(".changeg").click(function(){
			$(".countdetailForme").show();
			});
			

		
		$(".layerclose").click(function(){
			$(this).parents(".mmma").hide();
			})	
		
		
    })
	//获取新手机的验证码
	//修改绑定的手机号必须要更改数据库orders结构。
	function get_codes() {
		$.ajax({
			type:"POST",
			url:"test.php",   //接入短信接口
			cache:false,
			data:{
			"phone":$("#mobilea").val(),
			
			
			},
			beforeSend:function(){
			$("#phone-code").html("验证码获取中....");
			$("#phone-code").attr("disabled","disabled");
			if(!/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test($("#mobilea").val())) {
			  alert("获取验证码失败！输入的手机号不符合规定");
			  $("#phone-code").html("获取验证码");
			  $("#phone-code").removeAttr("disabled");
			  return false;
			}
			
			},
			success:function(data){
			 alert("获取成功,验证码已发送至您手机，请查收");
			 time($(".register-phone-code"));
			
			},
			
		});
 	}
	//获取email的验证码
	function get_email(){
		$.ajax({
			type:"POST",
			url:"",   //接入email接口
			cache:false,
			data:{
			"phone":$("#mobilea").val(),
			
			
			},
			beforeSend:function(){
			$("#phone-code").html("验证码获取中....");
			$("#phone-code").attr("disabled","disabled");
			if(!/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test($("#mobilea").val())) {
			  alert("获取验证码失败！输入的手机号不符合规定");
			  $("#phone-code").html("获取验证码");
			  $("#phone-code").removeAttr("disabled");
			  return false;
			}
			
			},
			success:function(data){
			 alert("获取成功,验证码已发送至您手机，请查收");
			 time($(".register-phone-code"));
			
			},
			
		});
	}
	function get_codes() {
		$.ajax({
			type:"POST",
			url:"test.php",   //接入短信接口
			cache:false,
			data:{
			"phone":$("#mobilea").val(),
			
			
			},
			beforeSend:function(){
			$("#phone-code").html("验证码获取中....");
			$("#phone-code").attr("disabled","disabled");
			if(!/^(0|86|17951)?(13[0-9]|15[012356789]|17[0678]|18[0-9]|14[57])[0-9]{8}$/.test($("#mobilea").val())) {
			  alert("获取验证码失败！输入的手机号不符合规定");
			  $("#phone-code").html("获取验证码");
			  $("#phone-code").removeAttr("disabled");
			  return false;
			}
			
			},
			success:function(data){
			 alert("获取成功,验证码已发送至您手机，请查收");
			 time($(".register-phone-code"));
			
			},
			
		});
 	}
 
	
	
   </script>
       
 <!-- 修改登录密码-->
     <form id="countdetailForm" class="countdetailForm mmma">
            <div class="minepass">
            	<div class="minestate-he">修改登录密码<i class="layerclose  fa fa-times"></i></div>
                <div class="minestate-pass">
                    <div class="minestate-label"><strong>旧密码</strong></div>
                    <input type="password"  class="minepass-input" required="required" name="oldpassword" id="oldpassword"/>
                </div>
                 <div class="minestate-pass">
                    <div class="minestate-label"><strong>新密码</strong></div>
                    <input type="password" class="minepass-input" required="required" name="newpassword" id="newpassword"/>
                </div>
                
                 <div class="minestate-pass">
                    <div class="minestate-label"><strong>确认密码</strong></div>
                    <input type="password" class="minepass-input" required="required" name="samepassword"/>
                </div>
                <button type="submit" class="minepass-submit user-save1">保存</button>
            </div>
      </form>

	<!-- 修改手机号-->
     <form id="countdetailFormb" class="countdetailFormb mmma">
        <div class="minepass">
            <div class="minestate-he">修改手机号<i class="layerclose fa fa-times"></i></div>
            <div class="minestate-pass">
                <div class="minestate-label"><strong>当前手机号码</strong></div>
                <input type="text"  class="minepass-input" value="<?php echo $_SESSION['user_id'];?>" readonly="readonly"/>
            </div>
             <div class="minestate-pass">
                <div class="minestate-label"><strong>新手机号</strong></div>
                <input type="mobile" class="minepass-input" required="required" name="mobilea" id="mobilea"/>
            </div>
            
             <div class="minestate-pass">
                <div class="minestate-label"><strong>验证码</strong></div>
                <button class="minepass-code" id="phone-code" onclick="get_codes()">获取验证码</button> 
                <input type="code" class="minepass-input minestatecode" required="required" name="codea" id="codea"/>
            </div>
            <div class="minestate-pass"> <span class="mine-order">注：修改成功后，请使用新手机号登录车宇宙。</span></div>
            
            <button type="submit" class="minepass-submit">保存</button>
        </div>
      </form>
      
     <!-- 设置邮箱-->
       <form id="countdetailFormc" class="countdetailFormc mmma">
            <div class="minepass">
            	<div class="minestate-he">设置邮箱<i class="layerclose  fa fa-times"></i></div>
                <div class="minestate-pass">
                    <div class="minestate-label"><strong>邮箱</strong></div>
                    <input type="email"  class="minepass-input" required="required" name="emaila"/>
                </div>
               <div class="minestate-pass">
                    <div class="minestate-label"><strong>验证码</strong></div>
                    <button class="minepass-code" id="email-code" onclick="get_email()">获取验证码</button> 
                    <input type="code" class="minepass-input minestatecode" required="required" name="codeb"/>
               </div>
                <button type="submit" class="minepass-submit">保存</button>
            </div>
      </form>
      
      <!-- 修改邮箱-->
       <form id="countdetailFormcb" class="countdetailFormcb mmma">
        <div class="minepass">
            <div class="minestate-he">修改邮箱<i class="layerclose fa fa-times"></i></div>
            <div class="minestate-pass">
                <div class="minestate-label"><strong>当前邮箱</strong></div>
                <input type="text"  class="minepass-input" value="<?php echo $row['user_email'];?>" readonly="readonly"/>
            </div>
             <div class="minestate-pass">
                <div class="minestate-label"><strong>新邮箱</strong></div>
                <input type="email" class="minepass-input" required="required" name="emailb"/>
            </div>
            
             <div class="minestate-pass">
                <div class="minestate-label"><strong>验证码</strong></div>
                <button class="minepass-code">获取验证</button> 
                <input type="code" class="minepass-input minestatecode" required="required" name="coded"/>
            </div>
            
            <button type="submit" class="minepass-submit">保存</button>
        </div>
      </form>
      
      <!-- 设置支付密码-->
       <form id="countdetailFormd" class="countdetailFormd mmma">
            <div class="minepass">
            	<div class="minestate-he">设置支付密码<i class="layerclose fa fa-times"></i></div>
                 <div class="minestate-pass">
                    <div class="minestate-label"><strong>密码</strong></div>
                    <input type="password" class="minepass-input" required="required" name="newpasswordb" id="newpasswordb"/>
                </div>
                
                 <div class="minestate-pass">
                    <div class="minestate-label"><strong>确认密码</strong></div>
                    <input type="password" class="minepass-input" required="required" name="samepassword"  id="samepassword"/>
                </div>
                <button type="submit" class="minepass-submit">保存</button>
            </div>
      </form>
      
       
       <!-- 修改支付密码-->
       
       <form id="countdetailFormdb" class="countdetailFormdb mmma">
            <div class="minepass">
            	<div class="minestate-he">修改支付密码<i class="layerclose fa fa-times"></i></div>
                <div class="minestate-pass">
                    <div class="minestate-label"><strong>手机号码</strong></div>
                    <input type="mobile"  class="minepass-input" required="required" name="mobiled"/>
                </div>
                 <div class="minestate-pass">
                    <div class="minestate-label"><strong>旧密码</strong></div>
                    <input type="password" class="minepass-input" required="required" name="newpass"/>
                </div>
                
                  <div class="minestate-pass">
                    <div class="minestate-label"><strong>验证码</strong></div>
                    <button class="minepass-code">获取验证</button> 
                    <input type="code" class="minepass-input minestatecode" required="required" name="codef"/>
                </div>
                <button type="submit" class="minepass-submit">保存</button>
            </div>
      </form>
       
       
       <!-- 实名认证-->
    	  <form id="countdetailForme" class="countdetailForme mmma">
            <div class="minepass">
            	<div class="minestate-he">实名认证<i class="layerclose fa fa-times"></i></div>
                <div class="minestate-pass">
                    <div class="minestate-label"><strong>真实姓名</strong></div>
                    <input type="username"  class="minepass-input" required="required" name="username"/>
                </div>
                 <div class="minestate-pass">
                    <div class="minestate-label"><strong>身份证号</strong></div>
                    <input type="iden" class="minepass-input" required="required" name="number"/>
                </div>
                <button type="submit" class="minepass-submit">保存</button>
            </div>
      </form>
   
<?php
	require_once("footer.php");
?>

          